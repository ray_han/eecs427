// Created by ihdl
module pc_functional(clk, br, stall, jmp, disp, resetn,scan_in,scan_en,RF_in,addr, scan_out,Rlink);
   input         clk,resetn,scan_en;
   input [9:0]   disp, RF_in;
   input 	 br, jmp,stall,scan_in;
   output [9:0]  Rlink;
   output reg [9:0] addr;
   output reg scan_out;
   reg [9:0] offset;

assign Rlink= addr+offset;


always @(posedge clk)begin
	if(scan_en)	scan_out<=addr[9];
	else 		scan_out<=0;
end

always @(posedge clk)begin
	if(scan_en==1)	addr<={addr[8:0],scan_in};
	else if(resetn==1)	
			addr<=0;
	else if(stall)	addr<=addr;
	else if(jmp==1)	addr<=RF_in;
	else		addr<=Rlink;
end

always @ (*) begin
	if(resetn)	offset=1;
	else if(br==1)	offset=disp;
	else		offset=1;
end



endmodule 
