* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT PC VDD VSS jmp br resetn scan_out<0> scan_out<1> scan_out<2> scan_en scan_in<0> scan_in<1> scan_in<2> scan_out<9> scan_out<4> scan_out<7> clk Rlink<0> scan_out<5> Rlink<2> scan_out<3>
+ scan_out<6> Rlink<3> RF_in<3> scan_in<3> Rlink<5> disp<5> disp<4> RF_in<6> Rlink<4> disp<6> scan_out<8> RF_in<8> Rlink<1> scan_in<7> scan_in<4> scan_in<5> Rlink<6> RF_in<9> scan_in<9> scan_in<8>
+ scan_in<6> Rlink<7> Rlink<9> Rlink<8> RF_in<0> RF_in<1> disp<0> RF_in<2> disp<1> disp<2> RF_in<4> disp<3> RF_in<5> RF_in<7> disp<7> disp<8> disp<9>
** N=1588 EP=57 IP=0 FDC=12
D0 1588 1576 diodenwx AREA=1.68925e-10 perim=0.00014614 t3well=0 $X=10460 $Y=10400 $D=474
D1 1588 1577 diodenwx AREA=2.81306e-10 perim=0.00014932 t3well=0 $X=10460 $Y=16010 $D=474
D2 1588 1578 diodenwx AREA=2.81306e-10 perim=0.00014932 t3well=0 $X=10460 $Y=23210 $D=474
D3 1588 1579 diodenwx AREA=2.81306e-10 perim=0.00014932 t3well=0 $X=10460 $Y=88010 $D=474
D4 1588 1580 diodenwx AREA=2.81116e-10 perim=0.00014948 t3well=0 $X=10460 $Y=44810 $D=474
D5 1588 1581 diodenwx AREA=2.80484e-10 perim=0.00014992 t3well=0 $X=10460 $Y=59210 $D=474
D6 1588 1582 diodenwx AREA=2.80926e-10 perim=0.00014964 t3well=0 $X=10460 $Y=30410 $D=474
D7 1588 1583 diodenwx AREA=2.81116e-10 perim=0.00014948 t3well=0 $X=10460 $Y=80810 $D=474
D8 1588 1584 diodenwx AREA=2.81116e-10 perim=0.00014948 t3well=0 $X=10460 $Y=37610 $D=474
D9 1588 1585 diodenwx AREA=2.80294e-10 perim=0.00015008 t3well=0 $X=10460 $Y=52010 $D=474
D10 1588 1586 diodenwx AREA=2.81116e-10 perim=0.00014948 t3well=0 $X=10460 $Y=73610 $D=474
D11 1588 1587 diodenwx AREA=2.81116e-10 perim=0.00014948 t3well=0 $X=10460 $Y=66410 $D=474
.ENDS
***************************************
