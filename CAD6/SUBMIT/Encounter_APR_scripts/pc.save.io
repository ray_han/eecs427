######################################################
#                                                    #
#  Cadence Design Systems, Inc.                      #
#  Cadence(R) Encounter(TM) IO Assignments           #
#                                                    #
######################################################

Version: 2



Offset: 10.4
Pin: clk N 2 0.2000 0.4000
Offset: 13.6
Pin: disp[0] N 2 0.2000 0.4000
Offset: 16.8
Pin: disp[1] N 2 0.2000 0.4000
Offset: 23.2
Pin: disp[2] N 2 0.2000 0.4000
Offset: 25.6
Pin: disp[3] N 2 0.2000 0.4000
Offset: 30
Pin: disp[4] N 2 0.2000 0.4000
Offset: 35.4
Pin: disp[5] N 2 0.2000 0.4000
Offset: 38.8
Pin: disp[6] N 2 0.2000 0.4000
Offset: 41.2
Pin: disp[7] N 2 0.2000 0.4000
Offset: 47.6
Pin: disp[8] N 2 0.2000 0.4000
Offset: 52
Pin: disp[9] N 2 0.2000 0.4000

Offset: 8
Pin: jmp W 3 0.2000 0.4000
Offset: 11.6
Pin: br W 3 0.2000 0.4000
Offset: 15.2
Pin: resetn W 3 0.2000 0.4000
Offset: 18.8
Pin: scan_out[0] W 3 0.2000 0.4000
Offset: 22.4
Pin: scan_out[1] W 3 0.2000 0.4000
Offset: 26
Pin: scan_out[2] W 3 0.2000 0.4000
Offset: 29.6
Pin: scan_out[3] W 3 0.2000 0.4000
Offset: 33.2
Pin: scan_out[4] W 3 0.2000 0.4000
Offset: 36.8 
Pin: scan_out[5] W 3 0.2000 0.4000
Offset: 40.4 
Pin: scan_out[6] W 3 0.2000 0.4000
Offset: 44 
Pin: scan_out[7] W 3 0.2000 0.4000
Offset: 47.6 
Pin: scan_out[8] W 3 0.2000 0.4000
Offset: 51.2 
Pin: scan_out[9] W 3 0.2000 0.4000
Offset: 54.8 
Pin: scan_en W 3 0.2000 0.4000
Offset: 58.4 
Pin: scan_in[0] W 3 0.2000 0.4000
Offset: 62 
Pin: scan_in[1] W 3 0.2000 0.4000
Offset: 64.4 
Pin: scan_in[2] W 3 0.2000 0.4000

Offset:  11.2
Pin: RF_in[0] S 2 0.2000 0.4000
Offset: 13.6
Pin: RF_in[1] S 2 0.2000 0.4000
Offset: 16.8
Pin: RF_in[2] S 2 0.2000 0.4000
Offset:  23
Pin: RF_in[3] S 2 0.2000 0.4000
Offset:  26.8
Pin: RF_in[4] S 2 0.2000 0.4000
Offset:  28.8
Pin: RF_in[5] S 2 0.2000 0.4000
Offset:  35.6
Pin: RF_in[6] S 2 0.2000 0.4000
Offset:  40
Pin: RF_in[7] S 2 0.2000 0.4000
Offset:  47.6
Pin: RF_in[8] S 2 0.2000 0.4000
Offset: 52
Pin: RF_in[9] S 2 0.2000 0.4000



Offset:  9
Pin: Rlink[0] E 3 0.2000 0.4000
Offset:   12.6
Pin: Rlink[1]  E 3 0.2000 0.4000
Offset:   16.2
Pin: Rlink[2]  E 3 0.2000 0.4000
Offset:  19.8
Pin: Rlink[3]  E 3 0.2000 0.4000
Offset:   23.4
Pin: Rlink[4]  E 3 0.2000 0.4000
Offset:  27
Pin: Rlink[5]  E 3 0.2000 0.4000
Offset:  30.6
Pin: Rlink[6]  E 3 0.2000 0.4000
Offset:  34.2
Pin: Rlink[7]  E 3 0.2000 0.4000
Offset:  37.8
Pin: Rlink[8]  E 3 0.2000 0.4000
Offset:  41.4
Pin: Rlink[9]  E 3 0.2000 0.4000
Offset:  45
Pin: scan_in[3] E 3 0.2000 0.4000
Offset:  48.6
Pin: scan_in[4]  E 3 0.2000 0.4000
Offset:  52.2
Pin: scan_in[5]  E 3 0.2000 0.4000
Offset:  55.8
Pin: scan_in[6]  E 3 0.2000 0.4000
Offset:  59.4
Pin: scan_in[7]  E 3 0.2000 0.4000
Offset:  62.8
Pin: scan_in[8]  E 3 0.2000 0.4000
Offset:  64
Pin: scan_in[9]  E 3 0.2000 0.4000
