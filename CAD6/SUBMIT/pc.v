//EESC427 Gropu2 CAD6

module pc(clk, br, jmp, disp, resetn,scan_in,scan_en,RF_in, scan_out,Rlink);
   input         clk,resetn,scan_en;
   input [9:0]  scan_in,disp, RF_in;
   input 	 br, jmp;
   output [9:0]  Rlink;
   output [9:0] scan_out;
   reg [9:0] offset,addr;

assign Rlink= addr+offset;

assign scan_out=addr;


always @(posedge clk)begin
	if(scan_en==1)	addr<=scan_in;
	else if(resetn==1)	
			addr<=0;
	else if(jmp==1)	addr<=RF_in;
	else		addr<=Rlink;
end

always @ (*) begin
	if(br==1)	offset<=disp;
	else		offset<=1;
end


endmodule 
