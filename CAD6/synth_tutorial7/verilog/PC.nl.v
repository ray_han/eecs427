/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06
// Date      : Wed Oct 24 23:00:40 2018
/////////////////////////////////////////////////////////////


module PC_DW01_add_1 ( A, B, CI, SUM, CO );
  input [9:0] A;
  input [9:0] B;
  output [9:0] SUM;
  input CI;
  output CO;
  wire   n1, n3, n5, n6, n7, n8, n9, n10, n12, n13, n15, n17, n18, n19, n20,
         n21, n22, n23, n24, n25, n26, n28, n31, n34, n36, n39, n40, n41, n44,
         n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58,
         n59, n60, n63, n65, n67, n68, n69, n107, n108, n109, n110, n111, n112,
         n113, n114, n115, n116, n117, n118, n119, n120, n121, n122, n123,
         n124, n125, n126;

  NOR2X6TR U84 ( .A(A[1]), .B(B[1]), .Y(n57) );
  NAND2X4TR U85 ( .A(A[1]), .B(B[1]), .Y(n58) );
  NOR2X8TR U86 ( .A(A[3]), .B(B[3]), .Y(n50) );
  NAND2X4TR U87 ( .A(A[3]), .B(B[3]), .Y(n51) );
  NAND2X6TR U88 ( .A(A[2]), .B(B[2]), .Y(n54) );
  NOR2X8TR U89 ( .A(A[2]), .B(B[2]), .Y(n53) );
  AND2XLTR U90 ( .A(n46), .B(n109), .Y(n115) );
  NOR2X6TR U91 ( .A(n44), .B(n39), .Y(n109) );
  CLKINVX2TR U92 ( .A(n34), .Y(n36) );
  NAND2X6TR U93 ( .A(n124), .B(n40), .Y(n34) );
  AND2X4TR U94 ( .A(n112), .B(n113), .Y(n25) );
  NOR2BX2TR U95 ( .AN(n109), .B(n28), .Y(n26) );
  OA21X1TR U96 ( .A0(n36), .A1(n28), .B0(n31), .Y(n113) );
  INVX2TR U97 ( .A(n56), .Y(n55) );
  NAND2X1TR U98 ( .A(A[7]), .B(B[7]), .Y(n24) );
  INVX4TR U99 ( .A(n18), .Y(n122) );
  NOR2X4TR U100 ( .A(A[5]), .B(B[5]), .Y(n39) );
  NAND2X4TR U101 ( .A(n111), .B(n20), .Y(n18) );
  NAND2X1TR U102 ( .A(n116), .B(n24), .Y(n22) );
  CLKINVX2TR U103 ( .A(n10), .Y(SUM[0]) );
  XOR2X1TR U104 ( .A(n55), .B(n8), .Y(SUM[2]) );
  OR2X4TR U105 ( .A(n115), .B(n34), .Y(n121) );
  CLKAND2X2TR U106 ( .A(n125), .B(n17), .Y(n107) );
  CLKAND2X2TR U107 ( .A(n114), .B(n31), .Y(n108) );
  OR2X4TR U108 ( .A(n122), .B(n107), .Y(n110) );
  OR2X4TR U109 ( .A(n39), .B(n45), .Y(n124) );
  NAND2X4TR U110 ( .A(n110), .B(n123), .Y(SUM[8]) );
  XNOR2X2TR U111 ( .A(n46), .B(n6), .Y(SUM[4]) );
  XNOR2X2TR U112 ( .A(n52), .B(n7), .Y(SUM[3]) );
  OR2X2TR U113 ( .A(A[8]), .B(B[8]), .Y(n125) );
  OR2X2TR U114 ( .A(n23), .B(n31), .Y(n116) );
  NAND2X2TR U115 ( .A(A[6]), .B(B[6]), .Y(n31) );
  XOR2X2TR U116 ( .A(n41), .B(n5), .Y(SUM[5]) );
  AOI21X2TR U117 ( .A0(n46), .A1(n117), .B0(n120), .Y(n41) );
  CLKINVX6TR U118 ( .A(n47), .Y(n46) );
  NOR2X2TR U119 ( .A(n53), .B(n50), .Y(n48) );
  CLKINVX2TR U120 ( .A(n53), .Y(n68) );
  OAI21X1TR U121 ( .A0(n55), .A1(n53), .B0(n54), .Y(n52) );
  NAND2X2TR U122 ( .A(A[4]), .B(B[4]), .Y(n45) );
  OR2XLTR U123 ( .A(A[4]), .B(B[4]), .Y(n117) );
  OR2X2TR U124 ( .A(n47), .B(n19), .Y(n111) );
  AOI21X2TR U125 ( .A0(n18), .A1(n125), .B0(n15), .Y(n13) );
  NAND2X2TR U126 ( .A(A[5]), .B(B[5]), .Y(n40) );
  OR2X4TR U127 ( .A(n118), .B(n119), .Y(n112) );
  OR2XLTR U128 ( .A(A[6]), .B(B[6]), .Y(n114) );
  NAND2X4TR U129 ( .A(A[0]), .B(B[0]), .Y(n60) );
  XOR2X4TR U130 ( .A(n121), .B(n108), .Y(SUM[6]) );
  NOR2X4TR U131 ( .A(A[7]), .B(B[7]), .Y(n23) );
  AOI21X2TR U132 ( .A0(n34), .A1(n21), .B0(n22), .Y(n20) );
  INVXLTR U133 ( .A(n50), .Y(n67) );
  NOR2X1TR U134 ( .A(A[4]), .B(B[4]), .Y(n44) );
  NAND2XLTR U135 ( .A(n63), .B(n24), .Y(n3) );
  INVX2TR U136 ( .A(n26), .Y(n119) );
  CLKINVX1TR U137 ( .A(n46), .Y(n118) );
  NAND2X2TR U138 ( .A(n122), .B(n107), .Y(n123) );
  NAND2X1TR U139 ( .A(n126), .B(n12), .Y(n1) );
  XOR2X4TR U140 ( .A(n13), .B(n1), .Y(SUM[9]) );
  NAND2X2TR U141 ( .A(n109), .B(n21), .Y(n19) );
  NOR2X2TR U142 ( .A(n28), .B(n23), .Y(n21) );
  AND2XLTR U143 ( .A(A[4]), .B(B[4]), .Y(n120) );
  INVXLTR U144 ( .A(n39), .Y(n65) );
  OAI21X4TR U145 ( .A0(n57), .A1(n60), .B0(n58), .Y(n56) );
  INVX2TR U146 ( .A(n17), .Y(n15) );
  OAI21X2TR U147 ( .A0(n50), .A1(n54), .B0(n51), .Y(n49) );
  NAND2XLTR U148 ( .A(n117), .B(n45), .Y(n6) );
  INVXLTR U149 ( .A(n23), .Y(n63) );
  AOI21X4TR U150 ( .A0(n56), .A1(n48), .B0(n49), .Y(n47) );
  NAND2BXLTR U151 ( .AN(n59), .B(n60), .Y(n10) );
  NOR2XLTR U152 ( .A(A[0]), .B(B[0]), .Y(n59) );
  OR2XLTR U153 ( .A(A[9]), .B(B[9]), .Y(n126) );
  XOR2X4TR U154 ( .A(n25), .B(n3), .Y(SUM[7]) );
  NAND2X1TR U155 ( .A(n67), .B(n51), .Y(n7) );
  NAND2X1TR U156 ( .A(n65), .B(n40), .Y(n5) );
  XOR2X1TR U157 ( .A(n9), .B(n60), .Y(SUM[1]) );
  NAND2X1TR U158 ( .A(n69), .B(n58), .Y(n9) );
  CLKINVX2TR U159 ( .A(n57), .Y(n69) );
  NAND2X1TR U160 ( .A(n68), .B(n54), .Y(n8) );
  NOR2X2TR U161 ( .A(A[6]), .B(B[6]), .Y(n28) );
  NAND2X1TR U162 ( .A(A[8]), .B(B[8]), .Y(n17) );
  NAND2X1TR U163 ( .A(A[9]), .B(B[9]), .Y(n12) );
endmodule


module PC ( clk, br, jmp, disp, resetn, scan_in, scan_en, RF_in, scan_out, 
        Rlink );
  input [9:0] disp;
  input [9:0] scan_in;
  input [9:0] RF_in;
  output [9:0] scan_out;
  output [9:0] Rlink;
  input clk, br, jmp, resetn, scan_en;
  wire   offset_9_0, offset_8_0, offset_7_0, offset_6_0, offset_5_0,
         offset_4_0, offset_3_0, offset_2_0, offset_1_0, offset_0_0, N6, N7,
         N8, N9, N10, N11, N12, N13, N14, N15, n150, n16, n17, n18, n19, n20,
         n21, n22, n23, n24, n25, n26, n27, n28, n30, n31, n32, n33, n34, n35,
         n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48;

  PC_DW01_add_1 add_11 ( .A(scan_out), .B({offset_9_0, offset_8_0, offset_7_0, 
        offset_6_0, n150, offset_4_0, offset_3_0, offset_2_0, offset_1_0, 
        offset_0_0}), .CI(1'b0), .SUM(Rlink) );
  DFFQX1TR addr_reg_9_0 ( .D(N15), .CK(clk), .Q(scan_out[9]) );
  DFFX4TR addr_reg_5_0 ( .D(N11), .CK(clk), .Q(scan_out[5]) );
  DFFHQX8TR addr_reg_6_0 ( .D(N12), .CK(clk), .Q(scan_out[6]) );
  DFFHQX2TR addr_reg_1_0 ( .D(N7), .CK(clk), .Q(scan_out[1]) );
  DFFHQX4TR addr_reg_4_0 ( .D(N10), .CK(clk), .Q(scan_out[4]) );
  DFFQX4TR addr_reg_0_0 ( .D(N6), .CK(clk), .Q(scan_out[0]) );
  DFFHQX4TR addr_reg_7_0 ( .D(N13), .CK(clk), .Q(scan_out[7]) );
  DFFHQX2TR addr_reg_3_0 ( .D(N9), .CK(clk), .Q(scan_out[3]) );
  DFFHQX2TR addr_reg_8_0 ( .D(N14), .CK(clk), .Q(scan_out[8]) );
  DFFX4TR addr_reg_2_0 ( .D(N8), .CK(clk), .Q(scan_out[2]) );
  AOI22X2TR U36 ( .A0(Rlink[6]), .A1(n24), .B0(RF_in[6]), .B1(n22), .Y(n40) );
  OAI2BB1X2TR U37 ( .A0N(scan_in[7]), .A1N(n21), .B0(n39), .Y(N13) );
  NOR2X4TR U38 ( .A(n31), .B(n32), .Y(n39) );
  OAI2BB1X4TR U39 ( .A0N(scan_in[8]), .A1N(n20), .B0(n42), .Y(N14) );
  NOR2X4TR U40 ( .A(n27), .B(n28), .Y(n42) );
  BUFX8TR U41 ( .A(offset_5_0), .Y(n150) );
  CLKAND2X12TR U42 ( .A(Rlink[7]), .B(n24), .Y(n31) );
  CLKAND2X2TR U43 ( .A(disp[5]), .B(n18), .Y(offset_5_0) );
  CLKAND2X2TR U44 ( .A(RF_in[8]), .B(n23), .Y(n28) );
  OAI2BB1X1TR U45 ( .A0N(scan_in[3]), .A1N(n21), .B0(n34), .Y(N9) );
  OAI2BB1X1TR U46 ( .A0N(scan_in[4]), .A1N(n20), .B0(n33), .Y(N10) );
  CLKINVX2TR U47 ( .A(RF_in[5]), .Y(n26) );
  AND2X2TR U48 ( .A(Rlink[8]), .B(n25), .Y(n27) );
  CLKBUFX2TR U49 ( .A(br), .Y(n16) );
  OAI2BB1X2TR U50 ( .A0N(scan_in[9]), .A1N(n20), .B0(n38), .Y(N15) );
  AOI22X2TR U51 ( .A0(Rlink[9]), .A1(n25), .B0(RF_in[9]), .B1(n22), .Y(n38) );
  CLKAND2X4TR U52 ( .A(disp[2]), .B(n17), .Y(offset_2_0) );
  BUFX20TR U53 ( .A(br), .Y(n17) );
  BUFX20TR U54 ( .A(br), .Y(n18) );
  CLKINVX2TR U55 ( .A(scan_en), .Y(n19) );
  CLKINVX2TR U56 ( .A(n19), .Y(n20) );
  CLKINVX2TR U57 ( .A(n19), .Y(n21) );
  CLKINVX2TR U58 ( .A(n46), .Y(n22) );
  CLKINVX2TR U59 ( .A(n46), .Y(n23) );
  CLKINVX2TR U60 ( .A(n46), .Y(n47) );
  CLKINVX2TR U61 ( .A(n44), .Y(n24) );
  CLKINVX2TR U62 ( .A(n44), .Y(n25) );
  AOI2BB2X1TR U63 ( .B0(Rlink[5]), .B1(n25), .A0N(n26), .A1N(n46), .Y(n41) );
  AOI22X1TR U64 ( .A0(Rlink[3]), .A1(n25), .B0(RF_in[3]), .B1(n23), .Y(n34) );
  AOI22X1TR U65 ( .A0(Rlink[4]), .A1(n24), .B0(RF_in[4]), .B1(n47), .Y(n33) );
  CLKINVX2TR U66 ( .A(n44), .Y(n48) );
  NAND2X2TR U67 ( .A(n30), .B(br), .Y(offset_0_0) );
  AND2X8TR U68 ( .A(disp[4]), .B(n17), .Y(offset_4_0) );
  CLKAND2X12TR U69 ( .A(disp[6]), .B(n17), .Y(offset_6_0) );
  OAI2BB1X1TR U70 ( .A0N(scan_in[2]), .A1N(scan_en), .B0(n35), .Y(N8) );
  OAI2BB1X1TR U71 ( .A0N(scan_in[1]), .A1N(scan_en), .B0(n36), .Y(N7) );
  OAI2BB1X1TR U72 ( .A0N(scan_in[6]), .A1N(n20), .B0(n40), .Y(N12) );
  OAI2BB1X2TR U73 ( .A0N(scan_in[5]), .A1N(n21), .B0(n41), .Y(N11) );
  AOI22X1TR U74 ( .A0(Rlink[2]), .A1(n48), .B0(RF_in[2]), .B1(n23), .Y(n35) );
  INVX12TR U75 ( .A(disp[0]), .Y(n30) );
  AND2X2TR U76 ( .A(disp[7]), .B(n17), .Y(offset_7_0) );
  AND2X2TR U77 ( .A(disp[3]), .B(n18), .Y(offset_3_0) );
  AND2X2TR U78 ( .A(disp[1]), .B(n18), .Y(offset_1_0) );
  AND2X2TR U79 ( .A(disp[8]), .B(n16), .Y(offset_8_0) );
  CLKAND2X2TR U80 ( .A(RF_in[7]), .B(n22), .Y(n32) );
  CLKAND2X2TR U81 ( .A(disp[9]), .B(n16), .Y(offset_9_0) );
  NAND3BX1TR U82 ( .AN(resetn), .B(n19), .C(n43), .Y(n44) );
  CLKINVX2TR U83 ( .A(jmp), .Y(n43) );
  NAND3BX1TR U84 ( .AN(scan_en), .B(jmp), .C(n45), .Y(n46) );
  AOI22X1TR U85 ( .A0(Rlink[1]), .A1(n48), .B0(RF_in[1]), .B1(n47), .Y(n36) );
  OAI2BB1X1TR U86 ( .A0N(scan_in[0]), .A1N(n21), .B0(n37), .Y(N6) );
  AOI22X1TR U87 ( .A0(Rlink[0]), .A1(n48), .B0(RF_in[0]), .B1(n47), .Y(n37) );
  CLKINVX4TR U88 ( .A(resetn), .Y(n45) );
endmodule

