###################################
# EECS 427 F15
# Changed by Jaeyoung Kim for SOC 14.2 compatibility
# Changed on October 15th 2015
# Encounter basic script
###################################


# Warning Message Suppressions
# Suppress ANTENNADIFFAREA
 suppressMessage ENCLF-201
# Surpress VIARULE GENERATE for turn-vias from LEF file
 suppressMessage ENCPP-557
# Surpess max_fanout of output/inout pin missing from library
 suppressMessage TECHLIB-436

set my_toplevel PC

# Setup design and create floorplan
source PC.globals
setDesignMode -process 130
init_design

# Initialize Floorplan
floorPlan -s 70 85 10.8 10.8 10.8 10.8

setFlipping f
redraw
fit

# Declare global VDD, VSS nets
clearGlobalNets
globalNetConnect VDD -type pgpin -pin VDD -inst *
globalNetConnect VDD -type tiehi
globalNetConnect VSS -type pgpin -pin VSS -inst *
globalNetConnect VSS -type tielo
applyGlobalNets

saveDesign initialized.enc

# Create Power Rings
addRing -nets {VDD VSS} -around each_block -center 1 -layer {top M3 bottom M3 left M2 right M2} -width {top 2 bottom 2 left 2 right 2} -spacing {top 1 bottom 1 left 1 right 1}

# Add Stripes (optional but always very recommended)
# Should add more stripes in other metal layers
setAddStripeMode -break_at block_ring -stacked_via_bottom_layer M3 -stacked_via_top_layer M5

addStripe -direction vertical -nets {VDD VSS}  -extend_to design_boundary -set_to_set_distance 24.3 -width 1.2 -spacing 10.8 -layer M4 -create_pins 1 -xleft_offset 0

saveDesign power.enc

# Place I/O pins in block boundary
loadIoFile ${my_toplevel}.save.io

# Then preplace the scan cells as jtag cells
#specifyJtag -hinst scan_chain0
#placeJtag -nrRow 1 -nrRowTop 3 -nrRowBottom 3 -nrRowLeft 6 -nrRowRight 3 -contour

# Place standard cells but count ONLY m1 and m2 as obstructions
setPlaceMode -congEffort High -modulePlan true
setTrialRouteMode -maxRouteLayer 3


setOptMode -allEndPoints true
placeDesign -prePlaceOpt
optDesign -preCTS

deletePlaceBlockage -all
addTieHiLo -cell "TIEHITR TIELOTR" -prefix tie 

redraw
saveDesign placed.enc
redraw

# Route power nets
# -noBlockPins -noPadRings replaced by -connect (unused for this time)
sroute -blockPinTarget nearestTarget

saveDesign power_routed.enc

# Run Clock Tree Generation
# Read the Encounter User Guide clkSynthesis commands to view syntax
setCTSMode -engine ck
createClockTreeSpec -output ${my_toplevel}.cts
specifyClockTree -file ${my_toplevel}.cts
ckSynthesis -rguide cts.rguide -report report.ctsrpt -macromodel report.ctsmdl -fix_added_buffers -forceReconvergent

#Optimize Design after CTS
optDesign -postCTS


# Output Results of CTS
trialRoute -highEffort -guide cts.rguide

# Add filler cells
addFiller -cell FILL8TR -prefix FILL -fillBoundary
addFiller -cell FILL4TR -prefix FILL -fillBoundary
addFiller -cell FILL2TR -prefix FILL -fillBoundary
addFiller -cell FILL1TR -prefix FILL -fillBoundary

# Connect all fixed VDD,VSS inputs to TIEHI/TIELO cells
globalNetConnect VDD -type tiehi
globalNetConnect VDD -type pgpin -pin VDD -override
globalNetConnect VSS -type tielo
globalNetConnect VSS -type pgpin -pin VSS -override

# Timing driven routing or timing optimization
setNanoRouteMode -drouteUseViaOfCut 2
setNanoRouteMode -drouteFixAntenna false
setNanoRouteMode -drouteSearchAndRepair true 
setNanoRouteMode -droutePostRouteSwapVia multicut
setNanoRouteMode -routeTopRoutingLayer 3
setNanoRouteMode -routeBottomRoutingLayer 1

globalDetailRoute

# Fix Antenna errors
# Set the top metal lower than the maximum level to avoid adding diodes
setNanoRouteMode -routeTopRoutingLayer 3
setNanoRouteMode -routeInsertDiodeForClockNets true
setNanoRouteMode -drouteFixAntenna true
setNanoRouteMode -routeAntennaCellName "ANTENNATR"
setNanoRouteMode -routeInsertAntennaDiode true
setNanoRouteMode -drouteSearchAndRepair true 

globalDetailRoute

# Save Desing
saveDesign routed.enc

# Output DEF, LEF and GDSII
set dbgLefDefOutVersion 5.7
#defout ../def/${my_toplevel}.def  -placement -routing 
lefout ../lef/${my_toplevel}.lef
setStreamOutMode -SEvianames ON
streamOut ../gds2/${my_toplevel}.gds2 -mapFile /afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/enc2gdsLM.map -libName eecs427artisan -structureName ${my_toplevel} -stripes 1 -units 1000 -mode ALL

#editSelect -nets clk
#deleteSelectedFromFPlan

saveNetlist -excludeLeafCell ../verilog/${my_toplevel}.apr.v

# Generate SDF
setExtractRCMode -engine postRoute -effortLevel high -relative_c_th 0.01 -total_c_th 0.01 -Reduce 0.0 -specialNet true
extractRC -outfile ${my_toplevel}.cap
rcOut -spf ${my_toplevel}.spf
rcOut -spef ${my_toplevel}.spef

setUseDefaultDelayLimit 10000
setDelayCalMode -engine aae -signoff true -ecsmType ecsmOnDemand
write_sdf ../sdf/${my_toplevel}.apr.sdf

# Run Geometry and Connection checks
verifyGeometry -reportAllCells -noOverlap -report ${my_toplevel}.geom.rpt
fixVia -minCut
# Meant for power vias that are just too small
verifyConnectivity -type all -noAntenna -report ${my_toplevel}.conn.rpt

puts "**************************************"
puts "*                                    *"
puts "* Encounter script finished          *"
puts "*                                    *"
puts "**************************************"
