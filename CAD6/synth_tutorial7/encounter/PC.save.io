######################################################
#                                                    #
#  Cadence Design Systems, Inc.                      #
#  Cadence(R) Encounter(TM) IO Assignments           #
#                                                    #
######################################################

Version: 2



Offset: 14.7
Pin: clk N 2 0.2000 0.4000
Offset: 20.7
Pin: disp[0] N 2 0.2000 0.4000
Offset: 26.7
Pin: disp[1] N 2 0.2000 0.4000
Offset: 32.7
Pin: disp[2] N 2 0.2000 0.4000
Offset: 38.7
Pin: disp[3] N 2 0.2000 0.4000
Offset: 46.7
Pin: disp[4] N 2 0.2000 0.4000
Offset: 52.7
Pin: disp[5] N 2 0.2000 0.4000
Offset: 56.7
Pin: disp[6] N 2 0.2000 0.4000
Offset: 62.7
Pin: disp[7] N 2 0.2000 0.4000
Offset: 68.7
Pin: disp[8] N 2 0.2000 0.4000
Offset: 76.7
Pin: disp[9] N 2 0.2000 0.4000

Offset: 11.5
Pin: jmp W 3 0.2000 0.4000
Offset: 16.5
Pin: br W 3 0.2000 0.4000
Offset: 21.5
Pin: resetn W 3 0.2000 0.4000
Offset: 26.5
Pin: scan_out[0] W 3 0.2000 0.4000
Offset: 31.5
Pin: scan_out[1] W 3 0.2000 0.4000
Offset: 36.5
Pin: scan_out[2] W 3 0.2000 0.4000
Offset: 41.5
Pin: scan_out[3] W 3 0.2000 0.4000
Offset: 46.5
Pin: scan_out[4] W 3 0.2000 0.4000
Offset: 51.5 
Pin: scan_out[5] W 3 0.2000 0.4000
Offset: 56.5 
Pin: scan_out[6] W 3 0.2000 0.4000
Offset: 61.5 
Pin: scan_out[7] W 3 0.2000 0.4000
Offset: 66.5 
Pin: scan_out[8] W 3 0.2000 0.4000
Offset: 71.5 
Pin: scan_out[9] W 3 0.2000 0.4000
Offset: 76.5 
Pin: scan_en W 3 0.2000 0.4000
Offset: 81.5 
Pin: scan_in[0] W 3 0.2000 0.4000
Offset: 86.5 
Pin: scan_in[1] W 3 0.2000 0.4000
Offset: 91.5 
Pin: scan_in[2] W 3 0.2000 0.4000

Offset:  13
Pin: RF_in[0] S 2 0.2000 0.4000
Offset: 19
Pin: RF_in[1] S 2 0.2000 0.4000
Offset: 25
Pin: RF_in[2] S 2 0.2000 0.4000
Offset:  31
Pin: RF_in[3] S 2 0.2000 0.4000
Offset:  37
Pin: RF_in[4] S 2 0.2000 0.4000
Offset:  43
Pin: RF_in[5] S 2 0.2000 0.4000
Offset:  49
Pin: RF_in[6] S 2 0.2000 0.4000
Offset:  55
Pin: RF_in[7] S 2 0.2000 0.4000
Offset:  61
Pin: RF_in[8] S 2 0.2000 0.4000
Offset: 67
Pin: RF_in[9] S 2 0.2000 0.4000



Offset:  13
Pin: Rlink[0] E 3 0.2000 0.4000
Offset:   18
Pin: Rlink[1]  E 3 0.2000 0.4000
Offset:   23
Pin: Rlink[2]  E 3 0.2000 0.4000
Offset:  28
Pin: Rlink[3]  E 3 0.2000 0.4000
Offset:   33
Pin: Rlink[4]  E 3 0.2000 0.4000
Offset:  38
Pin: Rlink[5]  E 3 0.2000 0.4000
Offset:  43
Pin: Rlink[6]  E 3 0.2000 0.4000
Offset:  48
Pin: Rlink[7]  E 3 0.2000 0.4000
Offset:  53
Pin: Rlink[8]  E 3 0.2000 0.4000
Offset:  58
Pin: Rlink[9]  E 3 0.2000 0.4000
Offset:  63
Pin: scan_in[3] E 3 0.2000 0.4000
Offset:  68
Pin: scan_in[4]  E 3 0.2000 0.4000
Offset:  73
Pin: scan_in[5]  E 3 0.2000 0.4000
Offset:  78
Pin: scan_in[6]  E 3 0.2000 0.4000
Offset:  83
Pin: scan_in[7]  E 3 0.2000 0.4000
Offset:  88
Pin: scan_in[8]  E 3 0.2000 0.4000
Offset:  93
Pin: scan_in[9]  E 3 0.2000 0.4000
