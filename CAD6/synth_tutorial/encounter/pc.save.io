######################################################
#                                                    #
#  Cadence Design Systems, Inc.                      #
#  Cadence(R) Encounter(TM) IO Assignments           #
#                                                    #
######################################################

Version: 2

Offset: 9 	  
Pin: clk      N	2 0.2000 0.4000
Offset: 12	  
Pin: Rlink[0] N	2 0.2000 0.4000
Offset: 15	  
Pin: Rlink[1] N	2 0.2000 0.4000
Offset: 18	  
Pin: Rlink[2] N	2 0.2000 0.4000
Offset: 21	  
Pin: Rlink[3] N	2 0.2000 0.4000
Offset: 24	  
Pin: Rlink[4] N	2 0.2000 0.4000
Offset: 27	  
Pin: Rlink[5] N	2 0.2000 0.4000
Offset: 30	  
Pin: Rlink[6] N	2 0.2000 0.4000
Offset: 33	  
Pin: Rlink[7] N	2 0.2000 0.4000
Offset: 36	  
Pin: Rlink[8] N	2 0.2000 0.4000
Offset: 39	 
Pin: Rlink[9] N	2 0.2000 0.4000
Offset: 42	  
Pin: resetn   N	2 0.2000 0.4000
Offset: 45	 
Pin: jmp      N	2 0.2000 0.4000
Offset: 48	  
Pin: stall    N	2 0.2000 0.4000
Offset: 51	  
Pin: br       N	2 0.2000 0.4000
Offset: 54	  
Pin: disp[0]  N	2 0.2000 0.4000
Offset: 57	  
Pin: disp[1]  N	2 0.2000 0.4000
Offset: 60	  
Pin: disp[2]  N	2 0.2000 0.4000
Offset: 63	  
Pin: disp[3]  N	2 0.2000 0.4000
Offset: 66	  
Pin: disp[4]  N	2 0.2000 0.4000
Offset: 69	  
Pin: disp[5]  N	2 0.2000 0.4000
Offset: 72	  
Pin: disp[6]  N	2 0.2000 0.4000
Offset: 75	  
Pin: disp[7]  N	2 0.2000 0.4000
Offset: 78	  
Pin: disp[8]  N	2 0.2000 0.4000
Offset: 81	  
Pin: disp[9]  N	2 0.2000 0.4000
Offset: 84	  
Pin: scan_in  N	2 0.2000 0.4000
Offset: 87	  
Pin: scan_en  N	2 0.2000 0.4000
Offset: 90	  
Pin: scan_out N	2 0.2000 0.4000

Offset:  9    
Pin: addr[0]  W   3 0.2000 0.4000
Offset:	 12   
Pin: addr[1]  W	3 0.2000 0.4000
Offset:	 15   
Pin: addr[2]  W	3 0.2000 0.4000
Offset:	 18   
Pin: addr[3]  W	3 0.2000 0.4000
Offset:	 21   
Pin: addr[4]  W	3 0.2000 0.4000
Offset:	 24   
Pin: addr[5]  W	3 0.2000 0.4000
Offset:	 27   
Pin: addr[6]  W	3 0.2000 0.4000
Offset:	 30   
Pin: addr[7]  W	3 0.2000 0.4000
Offset:	 33   
Pin: addr[8]  W	3 0.2000 0.4000
Offset:	 36   
Pin: addr[9]  W	3 0.2000 0.4000

Offset:  11.2	
Pin: RF_in[3] E 3 0.2000 0.4000
Offset:	 30.2   
Pin: RF_in[8] E	5 0.2000 0.4000
Offset:	 32.2   
Pin: RF_in[5] E	5 0.2000 0.4000
Offset:	 32.6   
Pin: RF_in[4] E	5 0.2000 0.4000
Offset:	 33     
Pin: RF_in[7] E	5 0.2000 0.4000
Offset:	 33.4   
Pin: RF_in[6] E	5 0.2000 0.4000
Offset:	 33.8   
Pin: RF_in[9] E	5 0.2000 0.4000
Offset:	 34.2   
Pin: RF_in[2] E	3 0.2000 0.4000
Offset:	 35.4   
Pin: RF_in[0] E	3 0.2000 0.4000
Offset:	 36.2   
Pin: RF_in[1] E	3 0.2000 0.4000




