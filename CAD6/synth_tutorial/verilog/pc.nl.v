/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Ultra(TM) in wire load mode
// Version   : O-2018.06
// Date      : Sun Dec  9 18:55:28 2018
/////////////////////////////////////////////////////////////


module pc ( clk, br, stall, jmp, disp, resetn, scan_in, scan_en, RF_in, addr, 
        scan_out, Rlink );
  input [9:0] disp;
  input [9:0] RF_in;
  output [9:0] addr;
  output [9:0] Rlink;
  input clk, br, stall, jmp, resetn, scan_in, scan_en;
  output scan_out;
  wire   n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n28, n29, n30, n31,
         n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45,
         n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59,
         n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73,
         n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87,
         n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100,
         n101, n102, n103, n104, n105, n106, n107, n108, n109, n110, n111,
         n112, n113, n114, n115, n116, n117, n118, n119, n120, n121, n122,
         n123, n124, n125, n126, n127, n128, n129, n130, n131, n132, n133,
         n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146;

  DFFQX4TR addr_reg_0_0 ( .D(n25), .CK(clk), .Q(addr[0]) );
  DFFQX4TR addr_reg_3_0 ( .D(n22), .CK(clk), .Q(addr[3]) );
  DFFQX4TR addr_reg_2_0 ( .D(n23), .CK(clk), .Q(addr[2]) );
  DFFTRX1TR scan_out_reg ( .D(addr[9]), .RN(scan_en), .CK(clk), .Q(scan_out)
         );
  DFFHQX1TR addr_reg_9_0 ( .D(n16), .CK(clk), .Q(addr[9]) );
  DFFQX4TR addr_reg_1_0 ( .D(n24), .CK(clk), .Q(addr[1]) );
  DFFHQX2TR addr_reg_4_0 ( .D(n21), .CK(clk), .Q(addr[4]) );
  DFFQX4TR addr_reg_5_0 ( .D(n20), .CK(clk), .Q(addr[5]) );
  DFFQX4TR addr_reg_6_0 ( .D(n19), .CK(clk), .Q(addr[6]) );
  DFFQX4TR addr_reg_7_0 ( .D(n18), .CK(clk), .Q(addr[7]) );
  DFFHQX1TR addr_reg_8_0 ( .D(n17), .CK(clk), .Q(addr[8]) );
  CLKBUFX2TR U40 ( .A(n102), .Y(n35) );
  CLKBUFX2TR U41 ( .A(n102), .Y(n34) );
  INVX2TR U42 ( .A(Rlink[3]), .Y(n117) );
  NAND2BX1TR U43 ( .AN(jmp), .B(n104), .Y(n102) );
  AOI21X2TR U44 ( .A0(n124), .A1(n52), .B0(n51), .Y(n56) );
  OAI21X1TR U45 ( .A0(n64), .A1(n63), .B0(n62), .Y(n65) );
  INVX2TR U46 ( .A(scan_en), .Y(n29) );
  NAND2X4TR U47 ( .A(n99), .B(addr[0]), .Y(n100) );
  NAND2X2TR U48 ( .A(n40), .B(addr[1]), .Y(n91) );
  NAND2X2TR U49 ( .A(n49), .B(addr[6]), .Y(n63) );
  BUFX8TR U50 ( .A(n39), .Y(n37) );
  CLKINVX1TR U51 ( .A(disp[4]), .Y(n45) );
  CLKINVX1TR U52 ( .A(n29), .Y(n31) );
  NAND2X1TR U53 ( .A(n53), .B(addr[7]), .Y(n62) );
  XOR2X1TR U54 ( .A(n93), .B(n100), .Y(Rlink[1]) );
  NAND2X4TR U55 ( .A(n38), .B(br), .Y(n39) );
  INVX2TR U56 ( .A(Rlink[5]), .Y(n132) );
  INVX2TR U57 ( .A(Rlink[6]), .Y(n135) );
  INVX2TR U58 ( .A(Rlink[7]), .Y(n138) );
  INVX3TR U59 ( .A(Rlink[9]), .Y(n146) );
  INVX2TR U60 ( .A(Rlink[8]), .Y(n141) );
  CLKINVX2TR U61 ( .A(Rlink[1]), .Y(n111) );
  INVX2TR U62 ( .A(n67), .Y(n50) );
  CLKINVX2TR U63 ( .A(n64), .Y(n54) );
  CLKINVX2TR U64 ( .A(n73), .Y(n74) );
  CLKINVX2TR U65 ( .A(n121), .Y(n122) );
  OR2X2TR U66 ( .A(n77), .B(addr[9]), .Y(n79) );
  CLKBUFX2TR U67 ( .A(n103), .Y(n142) );
  INVX2TR U68 ( .A(n29), .Y(n30) );
  INVX2TR U69 ( .A(n29), .Y(n32) );
  OAI211X2TR U70 ( .A0(n138), .A1(n34), .B0(n137), .C0(n136), .Y(n18) );
  OAI211X2TR U71 ( .A0(n141), .A1(n35), .B0(n140), .C0(n139), .Y(n17) );
  OAI211X1TR U72 ( .A0(n111), .A1(n102), .B0(n110), .C0(n109), .Y(n24) );
  XNOR2X2TR U73 ( .A(n124), .B(n83), .Y(Rlink[4]) );
  OAI21X2TR U74 ( .A0(n50), .A1(n60), .B0(n63), .Y(n51) );
  NAND2X1TR U75 ( .A(n54), .B(n62), .Y(n55) );
  NAND2X1TR U76 ( .A(n33), .B(RF_in[5]), .Y(n130) );
  INVX2TR U77 ( .A(n82), .Y(n123) );
  NAND2X1TR U78 ( .A(n33), .B(RF_in[9]), .Y(n144) );
  NAND2X1TR U79 ( .A(n143), .B(RF_in[6]), .Y(n133) );
  NAND2X1TR U80 ( .A(n143), .B(RF_in[2]), .Y(n112) );
  NAND2X1TR U81 ( .A(n143), .B(RF_in[7]), .Y(n136) );
  NAND2X1TR U82 ( .A(n33), .B(RF_in[1]), .Y(n109) );
  AOI22X1TR U83 ( .A0(n36), .A1(addr[9]), .B0(n32), .B1(addr[8]), .Y(n145) );
  AOI22X1TR U84 ( .A0(n142), .A1(addr[1]), .B0(n30), .B1(addr[0]), .Y(n110) );
  AOI22X1TR U85 ( .A0(n103), .A1(addr[2]), .B0(n31), .B1(addr[1]), .Y(n113) );
  OR2X2TR U86 ( .A(n71), .B(addr[8]), .Y(n75) );
  NAND2X1TR U87 ( .A(n77), .B(addr[9]), .Y(n78) );
  AOI22X1TR U88 ( .A0(n103), .A1(addr[7]), .B0(n31), .B1(addr[6]), .Y(n137) );
  NOR2X4TR U89 ( .A(n49), .B(addr[6]), .Y(n60) );
  NAND2X1TR U90 ( .A(n108), .B(RF_in[3]), .Y(n115) );
  NOR3BX2TR U91 ( .AN(stall), .B(resetn), .C(n32), .Y(n103) );
  BUFX6TR U92 ( .A(n39), .Y(n28) );
  NOR2X6TR U93 ( .A(n42), .B(addr[3]), .Y(n85) );
  NAND2X1TR U94 ( .A(n71), .B(addr[8]), .Y(n73) );
  NOR3X2TR U95 ( .A(resetn), .B(stall), .C(n30), .Y(n104) );
  XOR2X4TR U96 ( .A(n129), .B(n128), .Y(Rlink[5]) );
  NOR2BX4TR U97 ( .AN(disp[2]), .B(n37), .Y(n41) );
  NOR2X4TR U98 ( .A(n45), .B(n28), .Y(n47) );
  NAND2X2TR U99 ( .A(n48), .B(addr[5]), .Y(n126) );
  NOR2X4TR U100 ( .A(n48), .B(addr[5]), .Y(n125) );
  NOR2BX2TR U101 ( .AN(disp[5]), .B(n37), .Y(n48) );
  NAND2X2TR U102 ( .A(n92), .B(n91), .Y(n93) );
  OAI211X1TR U103 ( .A0(n146), .A1(n102), .B0(n145), .C0(n144), .Y(n16) );
  CLKINVX6TR U104 ( .A(n70), .Y(n124) );
  NAND2X4TR U105 ( .A(n41), .B(addr[2]), .Y(n95) );
  NAND2X2TR U106 ( .A(n47), .B(addr[4]), .Y(n121) );
  NOR2X4TR U107 ( .A(n82), .B(n125), .Y(n61) );
  AND2X1TR U108 ( .A(n101), .B(n100), .Y(Rlink[0]) );
  OR2X1TR U109 ( .A(n99), .B(addr[0]), .Y(n101) );
  CLKINVX1TR U110 ( .A(n90), .Y(n92) );
  CLKBUFX2TR U111 ( .A(n108), .Y(n33) );
  OAI211X1TR U112 ( .A0(n132), .A1(n34), .B0(n131), .C0(n130), .Y(n20) );
  OAI211X1TR U113 ( .A0(n135), .A1(n35), .B0(n134), .C0(n133), .Y(n19) );
  CLKBUFX2TR U114 ( .A(n103), .Y(n36) );
  NOR2BX2TR U115 ( .AN(disp[6]), .B(n39), .Y(n49) );
  NOR2BX2TR U116 ( .AN(disp[3]), .B(n37), .Y(n42) );
  OR2X2TR U117 ( .A(n39), .B(disp[0]), .Y(n99) );
  INVX16TR U118 ( .A(resetn), .Y(n38) );
  NOR2BX4TR U119 ( .AN(disp[1]), .B(n28), .Y(n40) );
  NOR2X4TR U120 ( .A(n40), .B(addr[1]), .Y(n90) );
  OAI21X4TR U121 ( .A0(n90), .A1(n100), .B0(n91), .Y(n84) );
  NOR2X8TR U122 ( .A(n41), .B(addr[2]), .Y(n94) );
  NOR2X2TR U123 ( .A(n94), .B(n85), .Y(n44) );
  NAND2X2TR U124 ( .A(n42), .B(addr[3]), .Y(n86) );
  OAI21X2TR U125 ( .A0(n85), .A1(n95), .B0(n86), .Y(n43) );
  AOI21X4TR U126 ( .A0(n84), .A1(n44), .B0(n43), .Y(n70) );
  NOR2X2TR U127 ( .A(n47), .B(addr[4]), .Y(n82) );
  CLKINVX2TR U128 ( .A(n61), .Y(n46) );
  NOR2X2TR U129 ( .A(n46), .B(n60), .Y(n52) );
  OAI21X4TR U130 ( .A0(n125), .A1(n121), .B0(n126), .Y(n67) );
  NOR2BX2TR U131 ( .AN(disp[7]), .B(n37), .Y(n53) );
  NOR2X4TR U132 ( .A(n53), .B(addr[7]), .Y(n64) );
  XOR2X4TR U133 ( .A(n56), .B(n55), .Y(Rlink[7]) );
  AOI21X4TR U134 ( .A0(n124), .A1(n61), .B0(n67), .Y(n59) );
  CLKINVX2TR U135 ( .A(n60), .Y(n57) );
  NAND2X1TR U136 ( .A(n57), .B(n63), .Y(n58) );
  XOR2X4TR U137 ( .A(n59), .B(n58), .Y(Rlink[6]) );
  NOR2X4TR U138 ( .A(n60), .B(n64), .Y(n66) );
  NAND2X2TR U139 ( .A(n61), .B(n66), .Y(n69) );
  AOI21X4TR U140 ( .A0(n67), .A1(n66), .B0(n65), .Y(n68) );
  OAI21X4TR U141 ( .A0(n70), .A1(n69), .B0(n68), .Y(n76) );
  NOR2BX1TR U142 ( .AN(disp[8]), .B(n28), .Y(n71) );
  NAND2X1TR U143 ( .A(n75), .B(n73), .Y(n72) );
  XNOR2X2TR U144 ( .A(n76), .B(n72), .Y(Rlink[8]) );
  AOI21X4TR U145 ( .A0(n76), .A1(n75), .B0(n74), .Y(n81) );
  NOR2BX1TR U146 ( .AN(disp[9]), .B(n28), .Y(n77) );
  NAND2X1TR U147 ( .A(n79), .B(n78), .Y(n80) );
  XOR2X4TR U148 ( .A(n81), .B(n80), .Y(Rlink[9]) );
  NAND2X1TR U149 ( .A(n123), .B(n121), .Y(n83) );
  INVX4TR U150 ( .A(n84), .Y(n98) );
  OAI21X4TR U151 ( .A0(n98), .A1(n94), .B0(n95), .Y(n89) );
  CLKINVX2TR U152 ( .A(n85), .Y(n87) );
  NAND2X1TR U153 ( .A(n87), .B(n86), .Y(n88) );
  XNOR2X1TR U154 ( .A(n89), .B(n88), .Y(Rlink[3]) );
  CLKINVX2TR U155 ( .A(n94), .Y(n96) );
  NAND2X1TR U156 ( .A(n96), .B(n95), .Y(n97) );
  XOR2X1TR U157 ( .A(n98), .B(n97), .Y(Rlink[2]) );
  CLKINVX2TR U158 ( .A(Rlink[0]), .Y(n107) );
  AOI22X1TR U159 ( .A0(n36), .A1(addr[0]), .B0(scan_en), .B1(scan_in), .Y(n106) );
  AND2X2TR U160 ( .A(n104), .B(jmp), .Y(n108) );
  NAND2X1TR U161 ( .A(n33), .B(RF_in[0]), .Y(n105) );
  OAI211X1TR U162 ( .A0(n107), .A1(n35), .B0(n106), .C0(n105), .Y(n25) );
  CLKBUFX2TR U163 ( .A(n108), .Y(n143) );
  CLKINVX2TR U164 ( .A(Rlink[2]), .Y(n114) );
  OAI211X1TR U165 ( .A0(n114), .A1(n34), .B0(n113), .C0(n112), .Y(n23) );
  AOI22X1TR U166 ( .A0(n142), .A1(addr[3]), .B0(n32), .B1(addr[2]), .Y(n116)
         );
  OAI211X1TR U167 ( .A0(n117), .A1(n35), .B0(n116), .C0(n115), .Y(n22) );
  CLKINVX2TR U168 ( .A(Rlink[4]), .Y(n120) );
  AOI22X1TR U169 ( .A0(n142), .A1(addr[4]), .B0(n31), .B1(addr[3]), .Y(n119)
         );
  NAND2X1TR U170 ( .A(n108), .B(RF_in[4]), .Y(n118) );
  OAI211X1TR U171 ( .A0(n120), .A1(n34), .B0(n119), .C0(n118), .Y(n21) );
  AOI21X4TR U172 ( .A0(n124), .A1(n123), .B0(n122), .Y(n129) );
  CLKINVX2TR U173 ( .A(n125), .Y(n127) );
  NAND2X1TR U174 ( .A(n127), .B(n126), .Y(n128) );
  AOI22X1TR U175 ( .A0(n142), .A1(addr[5]), .B0(n32), .B1(addr[4]), .Y(n131)
         );
  AOI22X1TR U176 ( .A0(n36), .A1(addr[6]), .B0(n31), .B1(addr[5]), .Y(n134) );
  AOI22X1TR U177 ( .A0(n36), .A1(addr[8]), .B0(scan_en), .B1(addr[7]), .Y(n140) );
  NAND2X1TR U178 ( .A(n143), .B(RF_in[8]), .Y(n139) );
endmodule

