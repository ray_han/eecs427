###################################
# EECS 427 F11
# Changed by ZhiYoong Foo for SOC 10.1 compatibility
# Changed on October 25th 2011
# Encounter basic script
###################################


# Warning Message Suppressions
# Suppress ANTENNADIFFAREA
suppressMessage ENCLF-200
# Surpress VIARULE GENERATE for turn-vias from LEF file
suppressMessage ENCPP-557
# Surpess max_fanout of output/inout pin missing from library
suppressMessage TECHLIB-436

set my_toplevel mult

# Setup design and create floorplan
loadConfig ./${my_toplevel}.conf 
#10.1 commits config automatically with loadconfig
#commitConfig
setCteReport

# Initialize Floorplan
floorPlan -s 68 64 10.8 10.8 10.8 10.8

setFlipping f
redraw
fit

# Declare global VDD, VSS nets
clearGlobalNets
globalNetConnect VDD -type pgpin -pin VDD -inst *
globalNetConnect VDD -type tiehi
globalNetConnect VSS -type pgpin -pin VSS -inst *
globalNetConnect VSS -type tielo
applyGlobalNets

saveDesign initialized.enc

# Create Power Rings
addRing -nets {VDD VSS} -around core -center 1 -layer_top M3 -layer_bottom M3 -layer_right M2 -layer_left M2 -width_top 2 -width_bottom 2 -width_left 2 -width_right 2 -spacing_top 1 -spacing_bottom 1 -spacing_right 1 -spacing_left 1
    
# Add Stripes (optional but always very recommended)
# Should add more stripes in other metal layers
addStripe -direction vertical -nets {VDD VSS}  -break_stripes_at_block_rings 1 -extend_to design_boundary -set_to_set_distance 24.0 -width 1.2 -spacing 10.8 -layer M4 -stacked_via_bottom_layer M3 -stacked_via_top_layer M5 -create_pins 1 -xleft_offset 0

saveDesign power.enc

# Place I/O pins in block boundary
loadIoFile ${my_toplevel}.save.io

# Then preplace the scan cells as jtag cells
#specifyJtag -hinst scan_chain0
#placeJtag -nrRow 1 -nrRowTop 3 -nrRowBottom 3 -nrRowLeft 6 -nrRowRight 3 -contour

# Place standard cells but count ONLY m1 and m2 as obstructions
setPlaceMode -congEffort High -modulePlan true -maxRouteLayer 3
placeDesign -inPlaceOpt -prePlaceOpt

deleteObstruction -all
addTieHiLo -cell "TIEHITR TIELOTR" -prefix tie 

redraw
saveDesign placed.enc
redraw

# Route power nets
# -noBlockPins -noPadRings replaced by -connect (unused for this time)
sroute -blockPinTarget nearestRingStripe

saveDesign power_routed.enc

# Run Clock Tree Generation
# Read the Encounter User Guide clkSynthesis commands to view syntax
#loadTimingCon ${my_toplevel}.sdc
#createClockTreeSpec -output ${my_toplevel}.cts -bufFootprint buf -invFootprint inv
#specifyClockTree -clkfile ${my_toplevel}.cts
#ckSynthesis -rguide cts.rguide -report report.ctsrpt -macromodel report.ctsmdl -fix_added_buffers -forceReconvergent

# Output Results of CTS
#trialRoute -highEffort -guide cts.rguide

# Add filler cells
addFiller -cell FILL8TR -prefix FILL -fillBoundary
addFiller -cell FILL4TR -prefix FILL -fillBoundary
addFiller -cell FILL2TR -prefix FILL -fillBoundary
addFiller -cell FILL1TR -prefix FILL -fillBoundary

# Connect all fixed VDD,VSS inputs to TIEHI/TIELO cells
globalNetConnect VDD -type tiehi
globalNetConnect VDD -type pgpin -pin VDD -override
globalNetConnect VSS -type tielo
globalNetConnect VSS -type pgpin -pin VSS -override

# Timing driven routing or timing optimization
setNanoRouteMode -drouteUseViaOfCut 2
setNanoRouteMode -drouteFixAntenna false
setNanoRouteMode -drouteSearchAndRepair true 
setNanoRouteMode -drouteOptimizeUseMultiCutVia true
setNanoRouteMode -routeTopRoutingLayer 3
setNanoRouteMode -routeBottomRoutingLayer 1

globalDetailRoute

# Fix Antenna errors
# Set the top metal lower than the maximum level to avoid adding diodes
setNanoRouteMode -routeTopRoutingLayer 3
setNanoRouteMode -routeInsertDiodeForClockNets true
setNanoRouteMode -drouteFixAntenna true
setNanoRouteMode -routeAntennaCellName "ANTENNATR"
#No Longer Used in 10.1
#setNanoRouteMode -routeInsertAntennaDiodeForClockNet true
setNanoRouteMode -routeInsertAntennaDiode true
setNanoRouteMode -drouteSearchAndRepair true 

globalDetailRoute

# Save Desing
saveDesign routed.enc

# Output DEF, LEF and GDSII
set dbgLefDefOutVersion 5.5
#defout ../def/${my_toplevel}.def  -placement -routing 
lefout ../lef/${my_toplevel}.lef
#streamOut ${my_toplevel}.gds2 -stripes 1 -units 1000 -mode ALL
setStreamOutMode -SEvianames ON
streamOut ../gds2/${my_toplevel}.gds2 -mapFile /afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/enc2gdsLM.map -libName eecs427artisan -structureName ${my_toplevel} -stripes 1 -units 1000 -mode ALL

#editSelect -nets clk
#deleteSelectedFromFPlan

saveNetlist -excludeLeafCell ../verilog/${my_toplevel}.apr.v

# Generate SDF
setExtractRCMode -detail -relative_c_th 0.01 -total_c_th 0.01 -Reduce 0.0 -rcdb tap.db -specialNet true
extractRC -outfile ${my_toplevel}.cap
rcOut -spf ${my_toplevel}.spf
rcOut -spef ${my_toplevel}.spef

setUseDefaultDelayLimit 10000
delayCal -sdf ../sdf/${my_toplevel}.apr.sdf

# Run Geometry and Connection checks
verifyGeometry -reportAllCells -viaOverlap -report ${my_toplevel}.geom.rpt
#Obsolete 
#fixMinCutVia 
fixVia -minCut
# Meant for power vias that are just too small
verifyConnectivity -type all -noAntenna -report ${my_toplevel}.conn.rpt

puts "**************************************"
puts "*                                    *"
puts "* Encounter script finished          *"
puts "*                                    *"
puts "**************************************"
