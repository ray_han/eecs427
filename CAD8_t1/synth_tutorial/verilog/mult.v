//EESC427 Gropu2 CAD8 revised

module mult(clk, instruIn, DIn, RsrcIn, pc, N, Z, F, C, DOut, ReadA, ReadB, WE, WEMaster, imm8_disp, DSEL, AluSEL, Amount, LuiSEL, FunISEL, MovSEL, SEL1, CIN, WEN, CEN, NOUT, ZOUT, FOUT, COUT, Br, Jmp);
	input			clk, N, Z, F, C;
	input [9:0]		pc;
	input [15:0]		instruIn, DIn, RsrcIn;
	output [7:0]		imm8_disp;
	output reg 		LuiSEL, SEL1, CIN, WEN, MovSEL, WEMaster, NOUT, ZOUT, FOUT, COUT, Br, Jmp;
	output reg [1:0]	DSEL, AluSEL, Amount, FunISEL;
	output reg [15:0]	DOut;
	output [15:0]		ReadA, ReadB, WE;
	output 			CEN;

//REG WIRE definition
	reg [3:0]	WEReg;
	reg [15:0] 	instruReg;
	wire[3:0]	opCode, opCodeExt, rSrc, rDest;

//Rtarget = Rsrc<9:0>
assign imm8_disp 	= instruReg[7:0];
assign rSrc 		= instruReg[3:0];
assign rDest 		= instruReg[11:8];
assign opCode 		= instruReg[15:12];
assign opCodeExt	= instruReg[7:4];
assign WE		= 16'h0001<<WEReg;
assign ReadA		= 16'h0001<<rSrc;
assign ReadB		= 16'h0001<<rDest;
// assign CEN		= 1'b0; //(check CEN truth table ????)


always @(posedge clk)begin//instruReg assignment
	instruReg	<= instruIn;
	WEReg 		<= rDest; // Delay one cycle for WB;
end
always@(posedge clk) begin//WEMaster
	if(opCode==4'b1100)					WEMaster <= 1'b0; //NO WB for Bcond 
	else if({opCode,opCodeExt[2:0]}=={4'b0100,3'b100})	WEMaster <= 1'b0; //NO WB for STOR, Jcond
	else if({opCode,opCodeExt} == {4'b0000,4'b1011} || opCode == 4'b1011)
								WEMaster <= 1'b0; //NO WB for CMP, CMPI
	else 							WEMaster <= 1'b1; //WB for OTHERS
end


always@(posedge clk)begin//FLAG REG
	NOUT <= N;
end

always@(posedge clk)begin//FLAG REG
	FOUT <= F;
end

always@(posedge clk)begin//FLAG REG
	ZOUT <= Z;
end

always@(posedge clk)begin//COUT REG
	COUT <= C;
end

/*
always@(*)begin//COUT REG
	Rtarget	= RsrcIn[9:0];
end
*/

//Br
always@(*)begin
	if (opCode == 4'b1100) begin
		case(rDest)
		4'b0000: begin //EQ 
			if (ZOUT == 1'b1) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b0001: begin //NE
			if (ZOUT == 1'b0) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b1101: begin //GE 
			if (ZOUT == 1'b1 || NOUT == 1'b1) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b0010: begin //CS 
			if (COUT == 1'b1) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b0011: begin //CC
			if (COUT == 1'b0) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b0110: begin //GT
			if (NOUT == 1'b1) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b0111: begin //LE
			if (NOUT == 1'b0) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b1000: begin //FS
			if (FOUT == 1'b0) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b1001: begin //FC
			if (FOUT == 1'b0) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b1100: begin //LT
			if (NOUT == 1'b0 || ZOUT == 1'b0) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b1110: Br = 1'b1;
		default: Br = 1'b0;
		endcase	
	end else Br = 1'b0;
end

//JMP
always@(*)begin
	if (opCode == 4'b0100) begin
		case(rDest)
		4'b0000: begin //EQ 
			if (ZOUT == 1'b1) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b0001: begin //NE
			if (ZOUT == 1'b0) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b1101: begin //GE 
			if (ZOUT == 1'b1 || NOUT == 1'b1) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b0010: begin //CS 
			if (COUT == 1'b1) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b0011: begin //CC
			if (COUT == 1'b0) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b0110: begin //GT
			if (NOUT == 1'b1) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b0111: begin //LE
			if (NOUT == 1'b0) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b1000: begin //FS
			if (FOUT == 1'b0) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b1001: begin //FC
			if (FOUT == 1'b0) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b1100: begin //LT
			if (NOUT == 1'b0 || ZOUT == 1'b0) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b1110: Jmp = 1'b1;
		default: Jmp = 1'b0;
		endcase	
	end else Jmp = 1'b0;
end

//MUX for ALU,Shifter,DMEM,MOV results

always@(*)begin//MUX(D, pc) -> D;
	if ({opCode,opCodeExt} == 8'b01001000)	DOut = DIn;//???????????
	else					DOut = {6'b000000, pc};
end

always@(*)begin//WEN
	if ({opCode,opCodeExt} == {4'b0100,4'b0100}) //STOR
		WEN	=1'b0;
	else 	WEN	=1'b1;
end

always@(*)begin//DSEL
	if(opCode == 4'b0100)				DSEL = 2'b00;//DMEM	
	else if(opCode == 4'b1111 || opCode == 4'b1000)	DSEL = 2'b01;//SHIFTER
	else						DSEL = 2'b10;
end

always@(*)begin//MovSEL
	if(opCode==4'b1101)	MovSEL = 1'b1;//MOVI
	else if({opCode,opCodeExt}=={4'b0000,4'b1101})
				MovSEL = 1'b1;//MOV
	else 			MovSEL = 1'b0;
end

always@(*)begin//FunISEL
	if({opCode,opCodeExt[3:2],opCodeExt[0]}=={4'b0000,3'b101})//CMP SUB
		FunISEL = 2'b10;
	else if({opCode[3:2],opCode[0]}==3'b101)//SUBi CMPi
		FunISEL	= 2'b11;
	else if(opCode==4'b0000)//ADD AND OR XOR MOV
		FunISEL = 2'b00;
	else 	FunISEL = 2'b01;
end

always@(*)begin//CIN
	if({opCode,opCodeExt[3:2],opCodeExt[0]}=={4'b0000,3'b101})//CMP SUB
		CIN = 1'b1;
	else if({opCode[3:2],opCode[0]}==3'b101)//SUBi CMPi
		CIN = 1'b1;	
	else 	CIN = 1'b0;
end

always@(*)begin//LuiSEL
	if(opCode==4'b1111)	LuiSEL = 1'b1;
	else 			LuiSEL = 1'b0;
end

always@(*)begin//Amount
	if({opCode,opCodeExt}=={4'b1000,4'b0100})//LSH
		Amount = 2'b00;
	else if({opCode,opCodeExt}=={4'b1000,4'b0000})//LSHI
		Amount = 2'b01;
	else 	Amount = 2'b10;//LUI
end

always@(*)begin//AluSEL
	if     (opCode==4'b0001||{opCode,opCodeExt}=={4'b0000,4'b0001})	AluSEL = 2'b00;//AND ANDi
	else if(opCode==4'b0010||{opCode,opCodeExt}=={4'b0000,4'b0010})	AluSEL = 2'b11;//OR ORi
	else if(opCode==4'b0011||{opCode,opCodeExt}=={4'b0000,4'b0011})	AluSEL = 2'b01;//XOR XORi
	else AluSEL = 2'b10;//ADD
end

always@(*)begin//SEL1
	if ((~imm8_disp[7]) | ((~opCode[3]) & (~opCode[2])) | (opCode[3] & opCode[2] & opCode[0]) == 1'b1)
		SEL1 = 1'b0;
	else	SEL1 = 1'b1;
end

endmodule

