/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Ultra(TM) in wire load mode
// Version   : O-2018.06
// Date      : Tue Nov 13 22:55:05 2018
/////////////////////////////////////////////////////////////


module mult ( clk, instruIn, DIn, RsrcIn, pc, N, Z, F, C, DOut, ReadA, ReadB, 
        WE, WEMaster, imm8_disp, DSEL, AluSEL, Amount, LuiSEL, FunISEL, MovSEL, 
        SEL1, CIN, WEN, CEN, NOUT, ZOUT, FOUT, COUT, Br, Jmp );
  input [15:0] instruIn;
  input [15:0] DIn;
  input [15:0] RsrcIn;
  input [9:0] pc;
  output [15:0] DOut;
  output [15:0] ReadA;
  output [15:0] ReadB;
  output [15:0] WE;
  output [7:0] imm8_disp;
  output [1:0] DSEL;
  output [1:0] AluSEL;
  output [1:0] Amount;
  output [1:0] FunISEL;
  input clk, N, Z, F, C;
  output WEMaster, LuiSEL, MovSEL, SEL1, CIN, WEN, CEN, NOUT, ZOUT, FOUT, COUT,
         Br, Jmp;
  wire   n172, n173, n174, n175, FunISEL_1_0, rDest_3_0, rDest_2_0, rDest_1_0,
         rDest_0_0, opCode_3_0, opCode_2_0, opCode_1_0, opCode_0_0, WEReg_3_0,
         WEReg_2_0, WEReg_1_0, WEReg_0_0, N49, n67, n68, n69, n70, n71, n72,
         n73, n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86,
         n87, n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100,
         n101, n102, n103, n105, n107, n108, n110, n111, n112, n113, n114,
         n115, n116, n117, n118, n120, n121, n122, n123, n124, n125, n126,
         n127, n128, n129, n130, n131, n132, n133, n134, n135, n136, n137,
         n138, n139, n140, n141, n142, n143, n144, n145, n146, n147, n148,
         n149, n150, n151, n152, n153, n154, n155, n156, n157, n158, n159,
         n160, n161, n162, n163, n164, n165, n166, n167, n168, n169, n170,
         n171;
  assign CIN = FunISEL_1_0;
  assign FunISEL[1] = FunISEL_1_0;

  DFFQX1TR ZOUT_reg ( .D(Z), .CK(clk), .Q(ZOUT) );
  DFFQX1TR NOUT_reg ( .D(N), .CK(clk), .Q(NOUT) );
  DFFQX1TR instruReg_reg_10_0 ( .D(instruIn[10]), .CK(clk), .Q(rDest_2_0) );
  DFFQX1TR instruReg_reg_7_0 ( .D(instruIn[7]), .CK(clk), .Q(imm8_disp[7]) );
  DFFQX1TR instruReg_reg_5_0 ( .D(instruIn[5]), .CK(clk), .Q(imm8_disp[5]) );
  DFFQX1TR instruReg_reg_4_0 ( .D(instruIn[4]), .CK(clk), .Q(imm8_disp[4]) );
  DFFQX1TR instruReg_reg_9_0 ( .D(instruIn[9]), .CK(clk), .Q(rDest_1_0) );
  DFFX1TR instruReg_reg_15_0 ( .D(instruIn[15]), .CK(clk), .Q(opCode_3_0), 
        .QN(n171) );
  DFFQXLTR instruReg_reg_0_0 ( .D(instruIn[0]), .CK(clk), .Q(n175) );
  DFFQXLTR WEReg_reg_0_0 ( .D(rDest_0_0), .CK(clk), .Q(WEReg_0_0) );
  DFFQXLTR instruReg_reg_13_0 ( .D(instruIn[13]), .CK(clk), .Q(opCode_1_0) );
  DFFQXLTR instruReg_reg_14_0 ( .D(instruIn[14]), .CK(clk), .Q(opCode_2_0) );
  DFFQXLTR WEReg_reg_3_0 ( .D(rDest_3_0), .CK(clk), .Q(WEReg_3_0) );
  DFFQXLTR instruReg_reg_3_0 ( .D(instruIn[3]), .CK(clk), .Q(n172) );
  DFFQXLTR WEReg_reg_1_0 ( .D(n86), .CK(clk), .Q(WEReg_1_0) );
  DFFQXLTR WEReg_reg_2_0 ( .D(rDest_2_0), .CK(clk), .Q(WEReg_2_0) );
  DFFQXLTR instruReg_reg_1_0 ( .D(instruIn[1]), .CK(clk), .Q(n174) );
  DFFQXLTR instruReg_reg_2_0 ( .D(instruIn[2]), .CK(clk), .Q(n173) );
  DFFQXLTR instruReg_reg_8_0 ( .D(instruIn[8]), .CK(clk), .Q(rDest_0_0) );
  DFFQX1TR instruReg_reg_6_0 ( .D(instruIn[6]), .CK(clk), .Q(imm8_disp[6]) );
  DFFQXLTR instruReg_reg_11_0 ( .D(instruIn[11]), .CK(clk), .Q(rDest_3_0) );
  DFFQXLTR WEMaster_reg ( .D(N49), .CK(clk), .Q(WEMaster) );
  DFFQXLTR COUT_reg ( .D(C), .CK(clk), .Q(COUT) );
  DFFQXLTR FOUT_reg ( .D(F), .CK(clk), .Q(FOUT) );
  DFFQXLTR instruReg_reg_12_0 ( .D(instruIn[12]), .CK(clk), .Q(opCode_0_0) );
  NOR2XLTR U148 ( .A(n157), .B(opCode_3_0), .Y(n132) );
  CLKBUFX2TR U149 ( .A(n162), .Y(n69) );
  INVX1TR U150 ( .A(n132), .Y(n146) );
  CLKBUFX2TR U151 ( .A(n159), .Y(n70) );
  NOR2XLTR U152 ( .A(n135), .B(n150), .Y(LuiSEL) );
  INVX1TR U153 ( .A(n157), .Y(n128) );
  CLKBUFX2TR U154 ( .A(imm8_disp[7]), .Y(n79) );
  INVX1TR U155 ( .A(imm8_disp[4]), .Y(n152) );
  INVX1TR U156 ( .A(imm8_disp[5]), .Y(n125) );
  OR2X1TR U157 ( .A(opCode_3_0), .B(opCode_2_0), .Y(n155) );
  INVX1TR U158 ( .A(ZOUT), .Y(n137) );
  INVX1TR U159 ( .A(NOUT), .Y(n136) );
  XNOR2XLTR U160 ( .A(n78), .B(n140), .Y(n141) );
  AOI22XLTR U161 ( .A0(n122), .A1(COUT), .B0(ZOUT), .B1(n87), .Y(n140) );
  INVX1TR U162 ( .A(rDest_2_0), .Y(n80) );
  INVX1TR U163 ( .A(n80), .Y(n81) );
  NAND3XLTR U164 ( .A(opCode_3_0), .B(opCode_0_0), .C(n110), .Y(n150) );
  INVX1TR U165 ( .A(opCode_1_0), .Y(n135) );
  INVX1TR U166 ( .A(n79), .Y(n149) );
  INVX1TR U167 ( .A(opCode_0_0), .Y(n151) );
  INVX1TR U168 ( .A(n88), .Y(n89) );
  INVX1TR U169 ( .A(n155), .Y(n68) );
  NAND3XLTR U170 ( .A(n151), .B(n135), .C(opCode_2_0), .Y(n157) );
  INVX1TR U171 ( .A(imm8_disp[6]), .Y(n88) );
  INVX1TR U172 ( .A(WEReg_3_0), .Y(n96) );
  AND2X1TR U173 ( .A(rDest_2_0), .B(n78), .Y(n67) );
  INVX1TR U174 ( .A(n156), .Y(n99) );
  AOI31XLTR U175 ( .A0(n81), .A1(n121), .A2(n145), .B0(n144), .Y(n158) );
  NAND2XLTR U176 ( .A(n125), .B(n152), .Y(n130) );
  NAND3BXLTR U177 ( .AN(n110), .B(n126), .C(opCode_3_0), .Y(n127) );
  INVX1TR U178 ( .A(n172), .Y(n107) );
  INVX1TR U179 ( .A(n82), .Y(n83) );
  INVX1TR U180 ( .A(n96), .Y(n98) );
  INVX1TR U181 ( .A(WEReg_2_0), .Y(n73) );
  INVX1TR U182 ( .A(WEReg_2_0), .Y(n82) );
  INVX1TR U183 ( .A(n96), .Y(n97) );
  INVX1TR U184 ( .A(n84), .Y(n85) );
  INVX1TR U185 ( .A(WEReg_1_0), .Y(n74) );
  INVX1TR U186 ( .A(WEReg_1_0), .Y(n84) );
  INVX1TR U187 ( .A(n94), .Y(n78) );
  INVX1TR U188 ( .A(n93), .Y(n95) );
  NAND2XLTR U189 ( .A(n80), .B(n77), .Y(n162) );
  INVX1TR U190 ( .A(n90), .Y(n91) );
  INVX1TR U191 ( .A(n90), .Y(n92) );
  INVX1TR U192 ( .A(n93), .Y(n94) );
  INVX1TR U193 ( .A(rDest_0_0), .Y(n93) );
  NAND2XLTR U194 ( .A(n121), .B(n95), .Y(n138) );
  INVX1TR U195 ( .A(n67), .Y(n72) );
  INVX1TR U196 ( .A(rDest_3_0), .Y(n90) );
  NAND2XLTR U197 ( .A(n81), .B(n95), .Y(n159) );
  INVX1TR U198 ( .A(n86), .Y(n87) );
  INVX1TR U199 ( .A(n67), .Y(n71) );
  INVX1TR U200 ( .A(n91), .Y(n77) );
  INVX1TR U201 ( .A(n173), .Y(n76) );
  INVX1TR U202 ( .A(n107), .Y(n108) );
  INVX1TR U203 ( .A(n173), .Y(n105) );
  INVX1TR U204 ( .A(n174), .Y(n75) );
  INVX1TR U205 ( .A(n174), .Y(n103) );
  INVX1TR U206 ( .A(n115), .Y(n156) );
  INVX1TR U207 ( .A(n99), .Y(n100) );
  INVX1TR U208 ( .A(n99), .Y(n101) );
  INVX1TR U209 ( .A(n99), .Y(n102) );
  INVX1TR U210 ( .A(n121), .Y(n161) );
  NAND4XLTR U211 ( .A(imm8_disp[7]), .B(imm8_disp[4]), .C(imm8_disp[6]), .D(
        n125), .Y(n124) );
  NAND3XLTR U212 ( .A(imm8_disp[5]), .B(n133), .C(n149), .Y(n134) );
  NAND3XLTR U213 ( .A(n111), .B(n151), .C(n68), .Y(n154) );
  NAND2BXLTR U214 ( .AN(LuiSEL), .B(n127), .Y(DSEL[0]) );
  INVX1TR U215 ( .A(n103), .Y(imm8_disp[1]) );
  INVX1TR U216 ( .A(n105), .Y(imm8_disp[2]) );
  INVX1TR U217 ( .A(n107), .Y(imm8_disp[3]) );
  INVX1TR U218 ( .A(n161), .Y(n86) );
  NOR4X1TR U219 ( .A(imm8_disp[6]), .B(FunISEL[0]), .C(n149), .D(n152), .Y(
        n148) );
  NOR2XLTR U220 ( .A(n89), .B(FunISEL[0]), .Y(n133) );
  CLKBUFX2TR U221 ( .A(opCode_2_0), .Y(n110) );
  INVX1TR U222 ( .A(n135), .Y(n111) );
  CLKBUFX2TR U223 ( .A(WEReg_0_0), .Y(n112) );
  CLKBUFX2TR U224 ( .A(WEReg_0_0), .Y(n113) );
  CLKBUFX2TR U225 ( .A(WEReg_0_0), .Y(n114) );
  OR4X1TR U226 ( .A(n89), .B(n149), .C(n146), .D(n130), .Y(n131) );
  INVX1TR U227 ( .A(n131), .Y(n115) );
  INVX1TR U228 ( .A(n131), .Y(n116) );
  INVX1TR U229 ( .A(n131), .Y(n117) );
  CLKBUFX2TR U230 ( .A(n175), .Y(n118) );
  CLKBUFX2TR U231 ( .A(n175), .Y(imm8_disp[0]) );
  CLKBUFX2TR U232 ( .A(n175), .Y(n120) );
  CLKBUFX2TR U233 ( .A(rDest_1_0), .Y(n121) );
  CLKBUFX2TR U234 ( .A(rDest_1_0), .Y(n122) );
  CLKBUFX2TR U235 ( .A(rDest_1_0), .Y(n123) );
  OAI22XLTR U236 ( .A0(n143), .A1(n142), .B0(n69), .B1(n141), .Y(n144) );
  NOR2XLTR U237 ( .A(n158), .B(n146), .Y(Jmp) );
  NOR2XLTR U238 ( .A(n132), .B(DSEL[0]), .Y(DSEL[1]) );
  NOR2XLTR U239 ( .A(n167), .B(n169), .Y(WE[15]) );
  NOR2XLTR U240 ( .A(n166), .B(n164), .Y(ReadA[9]) );
  NOR2XLTR U241 ( .A(n164), .B(n165), .Y(ReadA[13]) );
  NOR2XLTR U242 ( .A(opCode_0_0), .B(opCode_1_0), .Y(n126) );
  NAND2X1TR U243 ( .A(n126), .B(n68), .Y(FunISEL[0]) );
  OAI22XLTR U244 ( .A0(n111), .A1(n150), .B0(n124), .B1(FunISEL[0]), .Y(MovSEL) );
  OR3X1TR U245 ( .A(n79), .B(n130), .C(n127), .Y(Amount[1]) );
  NOR2XLTR U246 ( .A(n89), .B(Amount[1]), .Y(Amount[0]) );
  OR4X1TR U247 ( .A(n130), .B(n146), .C(n88), .D(n79), .Y(WEN) );
  NOR4XLTR U248 ( .A(n114), .B(WEReg_3_0), .C(n73), .D(n74), .Y(WE[6]) );
  NOR4XLTR U249 ( .A(imm8_disp[2]), .B(n172), .C(imm8_disp[0]), .D(n75), .Y(
        ReadA[2]) );
  NOR4XLTR U250 ( .A(n120), .B(n108), .C(imm8_disp[1]), .D(n76), .Y(ReadA[4])
         );
  NOR4XLTR U251 ( .A(imm8_disp[0]), .B(imm8_disp[3]), .C(n76), .D(n75), .Y(
        ReadA[6]) );
  OAI31XLTR U252 ( .A0(imm8_disp[5]), .A1(imm8_disp[4]), .A2(n88), .B0(n171), 
        .Y(n129) );
  NOR3X1TR U253 ( .A(n110), .B(n151), .C(n171), .Y(n147) );
  AOI222XLTR U254 ( .A0(n129), .A1(n128), .B0(imm8_disp[5]), .B1(n148), .C0(
        opCode_1_0), .C1(n147), .Y(N49) );
  AO22X1TR U255 ( .A0(n116), .A1(DIn[1]), .B0(n156), .B1(pc[1]), .Y(DOut[1])
         );
  AO22X1TR U256 ( .A0(n117), .A1(DIn[2]), .B0(n100), .B1(pc[2]), .Y(DOut[2])
         );
  AO22X1TR U257 ( .A0(n115), .A1(DIn[6]), .B0(n101), .B1(pc[6]), .Y(DOut[6])
         );
  AO22X1TR U258 ( .A0(n116), .A1(DIn[0]), .B0(n102), .B1(pc[0]), .Y(DOut[0])
         );
  AO22X1TR U259 ( .A0(n117), .A1(DIn[8]), .B0(n156), .B1(pc[8]), .Y(DOut[8])
         );
  AO22X1TR U260 ( .A0(n115), .A1(DIn[9]), .B0(n100), .B1(pc[9]), .Y(DOut[9])
         );
  AO22X1TR U261 ( .A0(n116), .A1(DIn[3]), .B0(n101), .B1(pc[3]), .Y(DOut[3])
         );
  AO22X1TR U262 ( .A0(n117), .A1(DIn[4]), .B0(n102), .B1(pc[4]), .Y(DOut[4])
         );
  AO22X1TR U263 ( .A0(n115), .A1(DIn[5]), .B0(n156), .B1(pc[5]), .Y(DOut[5])
         );
  AO22X1TR U264 ( .A0(n116), .A1(DIn[7]), .B0(n100), .B1(pc[7]), .Y(DOut[7])
         );
  NAND2X1TR U265 ( .A(n112), .B(WEReg_1_0), .Y(n167) );
  NAND2X1TR U266 ( .A(n83), .B(n98), .Y(n169) );
  NAND2X1TR U267 ( .A(n112), .B(n74), .Y(n168) );
  NOR2XLTR U268 ( .A(n168), .B(n169), .Y(WE[13]) );
  NAND2X1TR U269 ( .A(n97), .B(n73), .Y(n170) );
  NOR2XLTR U270 ( .A(n170), .B(n167), .Y(WE[11]) );
  NOR2XLTR U271 ( .A(n170), .B(n168), .Y(WE[9]) );
  NAND2X1TR U272 ( .A(n118), .B(imm8_disp[1]), .Y(n163) );
  NAND2X1TR U273 ( .A(imm8_disp[2]), .B(imm8_disp[3]), .Y(n165) );
  NOR2XLTR U274 ( .A(n163), .B(n165), .Y(ReadA[15]) );
  NAND2X1TR U275 ( .A(n118), .B(n75), .Y(n164) );
  NAND2X1TR U276 ( .A(n108), .B(n76), .Y(n166) );
  NOR2XLTR U277 ( .A(n166), .B(n163), .Y(ReadA[11]) );
  NAND2X1TR U278 ( .A(n91), .B(n80), .Y(n160) );
  NOR2XLTR U279 ( .A(n138), .B(n160), .Y(ReadB[11]) );
  OAI21XLTR U280 ( .A0(n155), .A1(n135), .B0(n134), .Y(AluSEL[0]) );
  NOR2XLTR U281 ( .A(n138), .B(n162), .Y(ReadB[3]) );
  OAI32XLTR U282 ( .A0(n78), .A1(NOUT), .A2(n92), .B0(n136), .B1(n94), .Y(n145) );
  OAI33XLTR U283 ( .A0(ZOUT), .A1(NOUT), .A2(n70), .B0(n137), .B1(n71), .B2(
        n136), .Y(n143) );
  NOR2XLTR U284 ( .A(n123), .B(FOUT), .Y(n139) );
  OAI211XLTR U285 ( .A0(rDest_2_0), .A1(n139), .B0(n92), .C0(n138), .Y(n142)
         );
  OR2X1TR U286 ( .A(n148), .B(n147), .Y(FunISEL_1_0) );
  NOR3BX1TR U287 ( .AN(n150), .B(n68), .C(n149), .Y(SEL1) );
  OAI31XLTR U288 ( .A0(n79), .A1(n89), .A2(n152), .B0(n151), .Y(n153) );
  NAND3XLTR U289 ( .A(n68), .B(n154), .C(n153), .Y(AluSEL[1]) );
  NOR2BX1TR U290 ( .AN(DIn[10]), .B(n101), .Y(DOut[10]) );
  NOR2BX1TR U291 ( .AN(DIn[11]), .B(n102), .Y(DOut[11]) );
  NOR2BX1TR U292 ( .AN(DIn[12]), .B(n131), .Y(DOut[12]) );
  NOR2BX1TR U293 ( .AN(DIn[13]), .B(n100), .Y(DOut[13]) );
  NOR2BX1TR U294 ( .AN(DIn[14]), .B(n101), .Y(DOut[14]) );
  NOR2BX1TR U295 ( .AN(DIn[15]), .B(n102), .Y(DOut[15]) );
  NOR3X1TR U296 ( .A(n158), .B(n171), .C(n157), .Y(Br) );
  NOR3X1TR U297 ( .A(n123), .B(n93), .C(n160), .Y(ReadB[9]) );
  NOR3X1TR U298 ( .A(n122), .B(n94), .C(n160), .Y(ReadB[8]) );
  NOR3X1TR U299 ( .A(n92), .B(n70), .C(n161), .Y(ReadB[7]) );
  NOR3X1TR U300 ( .A(rDest_3_0), .B(n72), .C(n87), .Y(ReadB[6]) );
  NOR3X1TR U301 ( .A(n121), .B(n91), .C(n70), .Y(ReadB[5]) );
  NOR3X1TR U302 ( .A(n123), .B(n92), .C(n71), .Y(ReadB[4]) );
  NOR3X1TR U303 ( .A(n95), .B(n161), .C(n69), .Y(ReadB[2]) );
  NOR3X1TR U304 ( .A(n122), .B(n78), .C(n162), .Y(ReadB[1]) );
  NOR3X1TR U305 ( .A(n161), .B(n70), .C(n77), .Y(ReadB[15]) );
  NOR3X1TR U306 ( .A(n87), .B(n71), .C(n77), .Y(ReadB[14]) );
  NOR3X1TR U307 ( .A(rDest_1_0), .B(n159), .C(n90), .Y(ReadB[13]) );
  NOR3X1TR U308 ( .A(n123), .B(n72), .C(n77), .Y(ReadB[12]) );
  NOR3X1TR U309 ( .A(rDest_0_0), .B(n87), .C(n160), .Y(ReadB[10]) );
  NOR3X1TR U310 ( .A(n122), .B(n95), .C(n69), .Y(ReadB[0]) );
  NOR3X1TR U311 ( .A(n120), .B(n174), .C(n166), .Y(ReadA[8]) );
  NOR3X1TR U312 ( .A(n108), .B(n105), .C(n163), .Y(ReadA[7]) );
  NOR3X1TR U313 ( .A(imm8_disp[3]), .B(n76), .C(n164), .Y(ReadA[5]) );
  NOR3X1TR U314 ( .A(n173), .B(n172), .C(n163), .Y(ReadA[3]) );
  NOR3X1TR U315 ( .A(imm8_disp[2]), .B(n108), .C(n164), .Y(ReadA[1]) );
  NOR3X1TR U316 ( .A(imm8_disp[0]), .B(n103), .C(n165), .Y(ReadA[14]) );
  NOR3X1TR U317 ( .A(n120), .B(imm8_disp[1]), .C(n165), .Y(ReadA[12]) );
  NOR3X1TR U318 ( .A(n120), .B(n166), .C(n75), .Y(ReadA[10]) );
  NOR4XLTR U319 ( .A(imm8_disp[2]), .B(imm8_disp[3]), .C(imm8_disp[0]), .D(
        imm8_disp[1]), .Y(ReadA[0]) );
  NOR3X1TR U320 ( .A(n114), .B(n85), .C(n170), .Y(WE[8]) );
  NOR3X1TR U321 ( .A(n97), .B(n82), .C(n167), .Y(WE[7]) );
  NOR3X1TR U322 ( .A(n98), .B(n73), .C(n168), .Y(WE[5]) );
  NOR4XLTR U323 ( .A(n113), .B(n97), .C(n85), .D(n73), .Y(WE[4]) );
  NOR3X1TR U324 ( .A(WEReg_2_0), .B(WEReg_3_0), .C(n167), .Y(WE[3]) );
  NOR4XLTR U325 ( .A(n83), .B(n98), .C(n114), .D(n74), .Y(WE[2]) );
  NOR3X1TR U326 ( .A(n83), .B(n97), .C(n168), .Y(WE[1]) );
  NOR3X1TR U327 ( .A(n113), .B(n84), .C(n169), .Y(WE[14]) );
  NOR3X1TR U328 ( .A(n114), .B(n85), .C(n169), .Y(WE[12]) );
  NOR3X1TR U329 ( .A(n113), .B(n170), .C(n74), .Y(WE[10]) );
  NOR4XLTR U330 ( .A(n83), .B(n98), .C(n113), .D(n85), .Y(WE[0]) );
endmodule

