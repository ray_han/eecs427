* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT DFF RSTn D VSS! VDD! Qb Q CLK
** N=179 EP=7 IP=0 FDC=34
M0 VSS! CLK 1 VSS! nfet L=1.2e-07 W=4e-07 AD=7.2e-14 AS=1.28e-13 PD=7.6e-07 PS=1.44e-06 NRD=0.45 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.44e-14 panw8=0 panw9=0 panw10=0 $X=540 $Y=1950 $D=25
M1 15 D VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=3.92e-14 AS=8.96e-14 PD=5.6e-07 PS=1.2e-06 NRD=0.5 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=6e-15 panw9=2.76e-14 panw10=3.36e-14 $X=540 $Y=3250 $D=25
M2 2 1 15 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.82e-14 AS=3.92e-14 PD=9.1e-07 PS=5.6e-07 NRD=1.125 NRS=0.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=6e-15 panw9=2.76e-14 panw10=3.36e-14 $X=940 $Y=3250 $D=25
M3 3 1 VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=7.2e-14 PD=1.44e-06 PS=7.6e-07 NRD=0.8 NRS=0.45 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.44e-14 panw8=0 panw9=0 panw10=0 $X=1020 $Y=1950 $D=25
M4 8 3 2 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=8.82e-14 PD=8.4e-07 PS=9.1e-07 NRD=1 NRS=1.125 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.47e-06 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=6e-15 panw9=2.76e-14 panw10=3.36e-14 $X=1690 $Y=3250 $D=25
M5 18 1 4 VSS! nfet L=1.2e-07 W=2.8e-07 AD=2.8e-14 AS=8.96e-14 PD=4.8e-07 PS=1.2e-06 NRD=0.357143 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.74e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=1.2e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.44e-14 panw8=0 panw9=0 panw10=0 $X=2000 $Y=2070 $D=25
M6 VSS! 5 18 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7e-14 AS=2.8e-14 PD=7.8e-07 PS=4.8e-07 NRD=0.892857 NRS=0.357143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=6.4e-07 sb=1.42e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=1.2e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.44e-14 panw8=0 panw9=0 panw10=0 $X=2320 $Y=2070 $D=25
M7 19 9 8 VSS! nfet L=1.2e-07 W=2.8e-07 AD=2.94e-14 AS=7.84e-14 PD=4.9e-07 PS=8.4e-07 NRD=0.375 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.91e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=6e-15 panw9=2.76e-14 panw10=3.36e-14 $X=2370 $Y=3250 $D=25
M8 VSS! RSTn 19 VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.88e-14 AS=2.94e-14 PD=7e-07 PS=4.9e-07 NRD=0.75 NRS=0.375 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.58e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=6e-15 panw9=2.76e-14 panw10=3.36e-14 $X=2700 $Y=3250 $D=25
M9 20 RSTn VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=2.8e-14 AS=7e-14 PD=4.8e-07 PS=7.8e-07 NRD=0.357143 NRS=0.892857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.26e-06 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=1.2e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.44e-14 panw8=0 panw9=0 panw10=0 $X=2940 $Y=2070 $D=25
M10 9 2 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.32e-14 AS=5.88e-14 PD=6.6e-07 PS=7e-07 NRD=0.678571 NRS=0.75 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=6e-15 panw9=2.76e-14 panw10=3.36e-14 $X=3240 $Y=3250 $D=25
M11 5 4 20 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.344e-13 AS=2.8e-14 PD=1.52e-06 PS=4.8e-07 NRD=1.71429 NRS=0.357143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.58e-06 sb=4.8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=1.2e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.44e-14 panw8=0 panw9=0 panw10=0 $X=3260 $Y=2070 $D=25
M12 4 3 9 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.512e-13 AS=5.32e-14 PD=1.64e-06 PS=6.6e-07 NRD=1.92857 NRS=0.678571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=5.4e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=6e-15 panw9=2.76e-14 panw10=3.36e-14 $X=3740 $Y=3250 $D=25
M13 VSS! 5 10 VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=8.96e-14 PD=6.4e-07 PS=1.2e-06 NRD=0.642857 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=1.2e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.44e-14 panw8=0 panw9=0 panw10=0 $X=4380 $Y=2070 $D=25
M14 Qb 10 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.04e-14 PD=1.2e-06 PS=6.4e-07 NRD=1.14286 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=1.2e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.44e-14 panw8=0 panw9=0 panw10=0 $X=4860 $Y=2070 $D=25
M15 Q 5 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=6e-15 panw9=2.76e-14 panw10=3.36e-14 $X=4900 $Y=3250 $D=25
M16 VDD! CLK 1 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.26e-13 panw8=4.8e-14 panw9=6.24e-14 panw10=1.8e-14 $X=540 $Y=510 $D=108
M17 16 D VDD! VDD! pfet L=1.2e-07 W=5.6e-07 AD=5.6e-14 AS=1.792e-13 PD=7.6e-07 PS=1.76e-06 NRD=0.178571 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.26e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=6e-15 panw6=1.2e-14 panw7=9.72e-14 panw8=4.8e-14 panw9=3.84e-14 panw10=0 $X=540 $Y=4930 $D=108
M18 2 3 16 VDD! pfet L=1.2e-07 W=5.6e-07 AD=1.04533e-13 AS=5.6e-14 PD=1.22667e-06 PS=7.6e-07 NRD=0.333333 NRS=0.178571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=6.4e-07 sb=9.8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=6e-15 panw6=1.2e-14 panw7=3e-14 panw8=5.92e-14 panw9=9.44e-14 panw10=0 $X=860 $Y=4930 $D=108
M19 3 1 VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=2.56e-13 AS=1.44e-13 PD=2.24e-06 PS=1.16e-06 NRD=0.4 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=3e-14 panw8=4.8e-14 panw9=1.584e-13 panw10=1.8e-14 $X=1020 $Y=510 $D=108
M20 8 1 2 VDD! pfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=5.22667e-14 PD=6.4e-07 PS=6.13333e-07 NRD=0.642857 NRS=0.666667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.12e-06 sb=1.28e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.44e-14 panw8=4.8e-14 panw9=4.8e-15 panw10=3.36e-14 $X=1340 $Y=5210 $D=108
M21 VDD! 9 8 VDD! pfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=5.04e-14 PD=6.4e-07 PS=6.4e-07 NRD=0.642857 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.6e-06 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.44e-14 panw8=4.8e-14 panw9=4.8e-15 panw10=1.68e-14 $X=1820 $Y=5210 $D=108
M22 17 3 4 VDD! pfet L=1.2e-07 W=5.6e-07 AD=5.6e-14 AS=1.792e-13 PD=7.6e-07 PS=1.76e-06 NRD=0.178571 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.19e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=4.8e-15 panw7=3e-14 panw8=4.8e-14 panw9=5.16e-14 panw10=0 $X=2000 $Y=510 $D=108
M23 8 RSTn VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.04e-14 PD=1.2e-06 PS=6.4e-07 NRD=1.14286 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.44e-14 panw8=4.8e-14 panw9=4.8e-15 panw10=0 $X=2300 $Y=5210 $D=108
M24 VDD! 5 17 VDD! pfet L=1.2e-07 W=5.6e-07 AD=1.53067e-13 AS=5.6e-14 PD=1.41333e-06 PS=7.6e-07 NRD=0.488095 NRS=0.178571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=6.4e-07 sb=8.7e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=4.8e-15 panw7=3e-14 panw8=4.8e-14 panw9=5.16e-14 panw10=0 $X=2320 $Y=510 $D=108
M25 5 RSTn VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=7.65333e-14 PD=6.4e-07 PS=7.06667e-07 NRD=0.642857 NRS=0.97619 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.26e-06 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=6e-15 panw8=4.32e-14 panw9=1.8e-14 panw10=0 $X=2940 $Y=510 $D=108
M26 9 2 VDD! VDD! pfet L=1.2e-07 W=5.6e-07 AD=1.19467e-13 AS=1.792e-13 PD=1.25333e-06 PS=1.76e-06 NRD=0.380952 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=6.5e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=6e-15 panw6=1.2e-14 panw7=3e-14 panw8=4.8e-14 panw9=3.84e-14 panw10=0 $X=3240 $Y=4930 $D=108
M27 VDD! 4 5 VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.04e-14 PD=1.2e-06 PS=6.4e-07 NRD=1.14286 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.74e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=6e-15 panw8=4.32e-14 panw9=1.8e-14 panw10=0 $X=3420 $Y=510 $D=108
M28 4 1 9 VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.512e-13 AS=5.97333e-14 PD=1.64e-06 PS=6.26667e-07 NRD=1.92857 NRS=0.761905 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8.2e-07 sb=5.4e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.44e-14 panw8=4.8e-14 panw9=4.8e-15 panw10=3.36e-14 $X=3740 $Y=5210 $D=108
M29 VDD! 5 10 VDD! pfet L=1.2e-07 W=5.6e-07 AD=1.008e-13 AS=1.792e-13 PD=9.2e-07 PS=1.76e-06 NRD=0.321429 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=4.8e-15 panw7=3e-14 panw8=4.8e-14 panw9=1.188e-13 panw10=0 $X=4380 $Y=510 $D=108
M30 Qb 10 VDD! VDD! pfet L=1.2e-07 W=5.6e-07 AD=1.792e-13 AS=1.008e-13 PD=1.76e-06 PS=9.2e-07 NRD=0.571429 NRS=0.321429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=4.8e-15 panw7=6.36e-14 panw8=8.16e-14 panw9=5.16e-14 panw10=0 $X=4860 $Y=510 $D=108
M31 Q 5 VDD! VDD! pfet L=1.2e-07 W=5.6e-07 AD=1.792e-13 AS=1.792e-13 PD=1.76e-06 PS=1.76e-06 NRD=0.571429 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=6e-15 panw6=1.2e-14 panw7=8.6e-14 panw8=5.92e-14 panw9=3.84e-14 panw10=0 $X=4900 $Y=4930 $D=108
D32 VSS! VDD! diodenwx AREA=1.09208e-11 perim=1.542e-05 t3well=0 $X=-120 $Y=-240 $D=474
D33 VSS! VDD! diodenwx AREA=1.02784e-11 perim=1.52e-05 t3well=0 $X=-120 $Y=4480 $D=474
.ENDS
***************************************
