* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT MUX_Shifter_1bit Rdest Shifter SEL Imm_16 SEL_BAR VSS! VDD!
** N=81 EP=7 IP=0 FDC=11
M0 3 SEL Imm_16 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=1.036e-13 PD=9.2e-07 PS=1.3e-06 NRD=1.14286 NRS=1.32143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.7e-07 sb=1.08e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=450 $Y=570 $D=25
M1 Rdest SEL_BAR 3 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=9.2e-07 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.13e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=450 $Y=1330 $D=25
M2 VSS! 3 7 VSS! nfet L=1.2e-07 W=4.2e-07 AD=8.61e-14 AS=1.344e-13 PD=8.3e-07 PS=1.48e-06 NRD=0.488095 NRS=0.761905 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.33e-06 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-15 panw9=0 panw10=0 $X=5130 $Y=470 $D=25
M3 Shifter 7 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=8.61e-14 PD=7.8e-07 PS=8.3e-07 NRD=0.428571 NRS=0.488095 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8.5e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-15 panw9=0 panw10=0 $X=5130 $Y=1000 $D=25
M4 VSS! 7 Shifter VSS! nfet L=1.2e-07 W=4.2e-07 AD=1.344e-13 AS=7.56e-14 PD=1.48e-06 PS=7.8e-07 NRD=0.761905 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.33e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-15 panw9=0 panw10=0 $X=5130 $Y=1480 $D=25
M5 3 SEL_BAR Imm_16 VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=1.036e-13 PD=9.2e-07 PS=1.3e-06 NRD=1.14286 NRS=1.32143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.7e-07 sb=1.08e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=3.36e-14 panw9=0 panw10=3.36e-14 $X=2210 $Y=570 $D=108
M6 Rdest SEL 3 VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=9.2e-07 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.13e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=3.36e-14 panw9=0 panw10=3.36e-14 $X=2210 $Y=1330 $D=108
M7 VDD! 3 7 VDD! pfet L=1.2e-07 W=7.2e-07 AD=1.58138e-13 AS=2.304e-13 PD=1.16945e-06 PS=2.08e-06 NRD=0.305051 NRS=0.444444 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.33e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=4.56e-14 panw8=8.88e-14 panw9=2.76e-14 panw10=1.344e-13 $X=3510 $Y=470 $D=108
M8 Shifter 7 VDD! VDD! pfet L=1.2e-07 W=9.3e-07 AD=1.674e-13 AS=2.04262e-13 PD=1.29e-06 PS=1.51055e-06 NRD=0.193548 NRS=0.236168 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7.30323e-07 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=2.322e-13 panw10=6.66e-14 $X=3510 $Y=1000 $D=108
M9 VDD! 7 Shifter VDD! pfet L=1.2e-07 W=9.3e-07 AD=2.976e-13 AS=1.674e-13 PD=2.5e-06 PS=1.29e-06 NRD=0.344086 NRS=0.193548 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.21032e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.17e-13 panw8=4.26e-14 panw9=2.76e-14 panw10=1.596e-13 $X=3510 $Y=1480 $D=108
D10 VSS! VDD! diodenwx AREA=7.358e-12 perim=1.086e-05 t3well=0 $X=1910 $Y=-300 $D=474
.ENDS
***************************************
