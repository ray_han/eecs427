* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT MUX_Shifter_1bit Rdest Shifter SEL Imm_16 SEL_BAR VSS! VDD! 20
** N=80 EP=8 IP=0 FDC=9
*.SEEDPROM
M0 VSS! 20 21 VSS! nfet L=1.2e-07 W=4.2e-07 AD=8.61e-14 AS=1.344e-13 PD=8.3e-07 PS=1.48e-06 NRD=0.488095 NRS=0.761905 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.33e-06 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-15 panw9=0 panw10=0 $X=5130 $Y=470 $D=25
M1 Shifter 21 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=8.61e-14 PD=7.8e-07 PS=8.3e-07 NRD=0.428571 NRS=0.488095 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8.5e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-15 panw9=0 panw10=0 $X=5130 $Y=1000 $D=25
M2 VSS! 21 Shifter VSS! nfet L=1.2e-07 W=4.2e-07 AD=1.344e-13 AS=7.56e-14 PD=1.48e-06 PS=7.8e-07 NRD=0.761905 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.33e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-15 panw9=0 panw10=0 $X=5130 $Y=1480 $D=25
M3 20 SEL_BAR Imm_16 VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=1.036e-13 PD=9.2e-07 PS=1.3e-06 NRD=1.14286 NRS=1.32143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.7e-07 sb=1.08e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=3.36e-14 panw9=0 panw10=3.36e-14 $X=2210 $Y=570 $D=108
M4 Rdest SEL 20 VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=9.2e-07 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.13e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=3.36e-14 panw9=0 panw10=3.36e-14 $X=2210 $Y=1330 $D=108
M5 VDD! 20 21 VDD! pfet L=1.2e-07 W=7.2e-07 AD=1.58138e-13 AS=2.304e-13 PD=1.16945e-06 PS=2.08e-06 NRD=0.305051 NRS=0.444444 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.33e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=4.56e-14 panw8=8.88e-14 panw9=2.76e-14 panw10=1.344e-13 $X=3510 $Y=470 $D=108
M6 Shifter 21 VDD! VDD! pfet L=1.2e-07 W=9.3e-07 AD=1.674e-13 AS=2.04262e-13 PD=1.29e-06 PS=1.51055e-06 NRD=0.193548 NRS=0.236168 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7.30323e-07 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=2.322e-13 panw10=6.66e-14 $X=3510 $Y=1000 $D=108
M7 VDD! 21 Shifter VDD! pfet L=1.2e-07 W=9.3e-07 AD=2.976e-13 AS=1.674e-13 PD=2.5e-06 PS=1.29e-06 NRD=0.344086 NRS=0.193548 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.21032e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.17e-13 panw8=4.26e-14 panw9=2.76e-14 panw10=1.596e-13 $X=3510 $Y=1480 $D=108
D8 VSS! VDD! diodenwx AREA=7.358e-12 perim=1.086e-05 t3well=0 $X=1910 $Y=-300 $D=474
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9 10 29
** N=30 EP=11 IP=40 FDC=20
*.SEEDPROM
M0 30 2 7 9 nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=1.036e-13 PD=9.2e-07 PS=1.3e-06 NRD=1.14286 NRS=1.32143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.7e-07 sb=1.08e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=4.08e-14 $X=450 $Y=570 $D=25
M1 6 4 30 9 nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=9.2e-07 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.13e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=4.08e-14 $X=450 $Y=1330 $D=25
X2 1 5 2 3 4 9 10 29 MUX_Shifter_1bit $T=-6000 0 0 0 $X=-6240 $Y=-300
X3 6 8 2 7 4 9 10 30 MUX_Shifter_1bit $T=0 0 0 0 $X=-240 $Y=-300
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 35
** N=36 EP=17 IP=46 FDC=42
*.SEEDPROM
M0 36 2 10 15 nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=1.036e-13 PD=9.2e-07 PS=1.3e-06 NRD=1.14286 NRS=1.32143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.7e-07 sb=1.08e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=4.08e-14 $X=-5550 $Y=570 $D=25
M1 9 4 36 15 nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=9.2e-07 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.13e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=4.08e-14 $X=-5550 $Y=1330 $D=25
X2 1 2 3 4 5 6 7 8 15 16 35 ICV_1 $T=-12000 0 0 0 $X=-18240 $Y=-300
X3 9 2 10 4 11 12 13 14 15 16 36 ICV_1 $T=0 0 0 0 $X=-6240 $Y=-300
.ENDS
***************************************
.SUBCKT MUX_Shifter Rdest<3> Imm_16<3> SH_MUX<3> Rdest<2> Imm_16<2> SH_MUX<2> Rdest<1> Imm_16<1> SH_MUX<1> Rdest<0> Imm_16<0> SH_MUX<0> Rdest<7> Imm_16<7> SH_MUX<7> Rdest<6> Imm_16<6> SH_MUX<6> Rdest<5> Imm_16<5>
+ SH_MUX<5> Rdest<4> Imm_16<4> SH_MUX<4> Rdest<11> Imm_16<11> SH_MUX<11> Rdest<10> Imm_16<10> SH_MUX<10> Rdest<9> Imm_16<9> SH_MUX<9> Rdest<8> Imm_16<8> SH_MUX<8> Rdest<15> Imm_16<15> SH_MUX<15> Rdest<14>
+ Imm_16<14> SH_MUX<14> Rdest<13> Imm_16<13> SH_MUX<13> Rdest<12> Imm_16<12> SH_MUX<12> VDD! SEL VSS!
** N=132 EP=51 IP=116 FDC=187
M0 2 1 VSS! VSS! nfet L=1.2e-07 W=4.7e-07 AD=8.46e-14 AS=1.02254e-13 PD=8.3e-07 PS=9.97317e-07 NRD=0.382979 NRS=0.462896 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.38043e-06 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=3.48e-14 $X=4280 $Y=1850 $D=25
M1 VSS! 1 2 VSS! nfet L=1.2e-07 W=4.7e-07 AD=1.504e-13 AS=8.46e-14 PD=1.58e-06 PS=8.3e-07 NRD=0.680851 NRS=0.382979 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.66383e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=3.48e-14 $X=4280 $Y=2330 $D=25
M2 1 53 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 AD=6.3e-14 AS=7.62222e-14 PD=7.1e-07 PS=8.44444e-07 NRD=0.514286 NRS=0.622222 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7.44e-07 sb=1.8e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=3.48e-14 $X=4400 $Y=850 $D=25
M3 VSS! 53 1 VSS! nfet L=1.2e-07 W=3.5e-07 AD=7.61463e-14 AS=6.3e-14 PD=7.42683e-07 PS=7.1e-07 NRD=0.621603 NRS=0.514286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.224e-06 sb=1.32e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=3.48e-14 $X=4400 $Y=1330 $D=25
M4 VSS! SEL 53 VSS! nfet L=1.2e-07 W=2.8e-07 AD=6.09778e-14 AS=8.96e-14 PD=6.75556e-07 PS=1.2e-06 NRD=0.777778 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=3.36e-14 $X=4470 $Y=320 $D=25
M5 129 1 Imm_16<3> VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=1.036e-13 PD=9.2e-07 PS=1.3e-06 NRD=1.14286 NRS=1.32143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.7e-07 sb=1.08e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=4.08e-14 $X=28470 $Y=970 $D=25
M6 Rdest<3> 2 129 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=9.2e-07 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.13e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=4.08e-14 $X=28470 $Y=1730 $D=25
M7 130 1 Imm_16<7> VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=1.036e-13 PD=9.2e-07 PS=1.3e-06 NRD=1.14286 NRS=1.32143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.7e-07 sb=1.08e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=4.08e-14 $X=52470 $Y=970 $D=25
M8 Rdest<7> 2 130 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=9.2e-07 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.13e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=4.08e-14 $X=52470 $Y=1730 $D=25
M9 131 1 Imm_16<11> VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=1.036e-13 PD=9.2e-07 PS=1.3e-06 NRD=1.14286 NRS=1.32143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.7e-07 sb=1.08e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=4.08e-14 $X=76470 $Y=970 $D=25
M10 Rdest<11> 2 131 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=9.2e-07 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.13e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=4.08e-14 $X=76470 $Y=1730 $D=25
M11 132 1 Imm_16<15> VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=1.036e-13 PD=9.2e-07 PS=1.3e-06 NRD=1.14286 NRS=1.32143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.7e-07 sb=1.08e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=100470 $Y=970 $D=25
M12 Rdest<15> 2 132 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=9.2e-07 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.13e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=100470 $Y=1730 $D=25
M13 VDD! SEL 53 VDD! pfet L=1.2e-07 W=4.8e-07 AD=1.04533e-13 AS=1.536e-13 PD=8.97778e-07 PS=1.6e-06 NRD=0.453704 NRS=0.666667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=7.32e-14 panw8=4.8e-14 panw9=5.16e-14 panw10=0 $X=510 $Y=320 $D=108
M14 1 53 VDD! VDD! pfet L=1.2e-07 W=6e-07 AD=1.08e-13 AS=1.30667e-13 PD=9.6e-07 PS=1.12222e-06 NRD=0.3 NRS=0.362963 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7.44e-07 sb=1.8e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3e-14 panw8=4.8e-14 panw9=1.38e-13 panw10=0 $X=510 $Y=850 $D=108
M15 VDD! 53 1 VDD! pfet L=1.2e-07 W=6e-07 AD=1.344e-13 AS=1.08e-13 PD=1.04e-06 PS=9.6e-07 NRD=0.373333 NRS=0.3 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.224e-06 sb=1.32e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3e-14 panw8=4.8e-14 panw9=6.6e-14 panw10=1.44e-13 $X=510 $Y=1330 $D=108
M16 2 1 VDD! VDD! pfet L=1.2e-07 W=9e-07 AD=1.62e-13 AS=2.016e-13 PD=1.26e-06 PS=1.56e-06 NRD=0.2 NRS=0.248889 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.26933e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=3e-14 panw8=4.8e-14 panw9=1.8e-13 panw10=3e-14 $X=510 $Y=1850 $D=108
M17 VDD! 1 2 VDD! pfet L=1.2e-07 W=9e-07 AD=2.88e-13 AS=1.62e-13 PD=2.44e-06 PS=1.26e-06 NRD=0.355556 NRS=0.2 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.57333e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.38e-13 panw8=4.8e-14 panw9=7.2e-14 panw10=3e-14 $X=510 $Y=2330 $D=108
D18 VSS! VDD! diodenwx AREA=6.5715e-12 perim=1.064e-05 t3well=0 $X=-240 $Y=-300 $D=474
X19 Rdest<3> 1 Imm_16<3> 2 SH_MUX<3> Rdest<2> Imm_16<2> SH_MUX<2> Rdest<1> Imm_16<1> SH_MUX<1> Rdest<0> Imm_16<0> SH_MUX<0> VSS! VDD! 129 ICV_2 $T=11200 400 1 180 $X=4960 $Y=100
X20 Rdest<7> 1 Imm_16<7> 2 SH_MUX<7> Rdest<6> Imm_16<6> SH_MUX<6> Rdest<5> Imm_16<5> SH_MUX<5> Rdest<4> Imm_16<4> SH_MUX<4> VSS! VDD! 130 ICV_2 $T=35200 400 1 180 $X=28960 $Y=100
X21 Rdest<11> 1 Imm_16<11> 2 SH_MUX<11> Rdest<10> Imm_16<10> SH_MUX<10> Rdest<9> Imm_16<9> SH_MUX<9> Rdest<8> Imm_16<8> SH_MUX<8> VSS! VDD! 131 ICV_2 $T=59200 400 1 180 $X=52960 $Y=100
X22 Rdest<15> 1 Imm_16<15> 2 SH_MUX<15> Rdest<14> Imm_16<14> SH_MUX<14> Rdest<13> Imm_16<13> SH_MUX<13> Rdest<12> Imm_16<12> SH_MUX<12> VSS! VDD! 132 ICV_2 $T=83200 400 1 180 $X=76960 $Y=100
.ENDS
***************************************
