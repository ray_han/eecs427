* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT Sign_Ext_right IMM_16 SEL_BAR SEL 4 5
** N=49 EP=5 IP=0 FDC=10
M0 4 6 7 4 nfet L=1.2e-07 W=3.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=2210 $D=97
M1 IMM_16 7 4 4 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=670 $Y=2730 $D=97
M2 4 7 IMM_16 4 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=670 $Y=3330 $D=97
M3 6 SEL_BAR 4 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=720 $Y=570 $D=97
M4 5 SEL 6 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=720 $Y=1130 $D=97
M5 5 6 7 5 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=2210 $D=189
M6 IMM_16 7 5 5 pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2730 $D=189
M7 5 7 IMM_16 5 pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=3330 $D=189
M8 6 SEL 4 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=570 $D=189
M9 5 SEL_BAR 6 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1130 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6
** N=6 EP=6 IP=10 FDC=20
X0 4 1 2 3 5 Sign_Ext_right $T=0 0 0 0 $X=-240 $Y=-300
X1 6 1 2 3 5 Sign_Ext_right $T=0 4000 0 0 $X=-240 $Y=3700
.ENDS
***************************************
.SUBCKT Sign_Ext_left Imm_16 Imm_8 3 4
** N=23 EP=4 IP=0 FDC=4
M0 Imm_16 6 3 3 nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=1020 $D=97
M1 3 Imm_8 6 3 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=670 $Y=490 $D=97
M2 Imm_16 6 4 4 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=1020 $D=189
M3 4 Imm_8 6 4 pfet L=1.2e-07 W=4.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2040 $Y=490 $D=189
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6
** N=7 EP=6 IP=10 FDC=8
X0 2 3 1 4 Sign_Ext_left $T=0 0 0 0 $X=-240 $Y=-180
X1 5 6 1 4 Sign_Ext_left $T=0 1600 0 0 $X=-240 $Y=1420
.ENDS
***************************************
.SUBCKT Sign_Ext SEL_one VSS! VDD! Imm_16<8> Imm_16<9> Imm_16<10> Imm_16<11> Imm_16<12> Imm_16<13> Imm_16<14> Imm_16<15> Imm_16<0> Imm_16<1> Imm_8<0> Imm_8<1> Imm_16<2> Imm_16<3> Imm_8<2> Imm_8<3> Imm_16<4>
+ Imm_16<5> Imm_8<4> Imm_8<5> Imm_16<6> Imm_16<7> Imm_8<6> Imm_8<7>
** N=73 EP=27 IP=52 FDC=124
M0 VSS! 3 1 VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=45450 $D=97
M1 1 3 VSS! VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=46010 $D=97
M2 VSS! 3 1 VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=46570 $D=97
M3 3 5 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=47130 $D=97
M4 VSS! 5 3 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=47690 $D=97
M5 5 SEL_one VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=48250 $D=97
M6 VDD! 3 1 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1890 $Y=45450 $D=189
M7 1 3 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1890 $Y=46010 $D=189
M8 VDD! 3 1 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1890 $Y=46570 $D=189
M9 3 5 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1890 $Y=47130 $D=189
M10 VDD! 5 3 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1890 $Y=47690 $D=189
M11 5 SEL_one VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2010 $Y=48250 $D=189
X12 1 3 VSS! Imm_16<8> VDD! Imm_16<9> ICV_1 $T=0 12800 0 0 $X=-240 $Y=12500
X13 1 3 VSS! Imm_16<10> VDD! Imm_16<11> ICV_1 $T=0 20800 0 0 $X=-240 $Y=20500
X14 1 3 VSS! Imm_16<12> VDD! Imm_16<13> ICV_1 $T=0 28800 0 0 $X=-240 $Y=28500
X15 1 3 VSS! Imm_16<14> VDD! Imm_16<15> ICV_1 $T=0 36800 0 0 $X=-240 $Y=36500
X16 VSS! Imm_16<0> Imm_8<0> VDD! Imm_16<1> Imm_8<1> ICV_2 $T=0 0 0 0 $X=-240 $Y=-180
X17 VSS! Imm_16<2> Imm_8<2> VDD! Imm_16<3> Imm_8<3> ICV_2 $T=0 3200 0 0 $X=-240 $Y=3020
X18 VSS! Imm_16<4> Imm_8<4> VDD! Imm_16<5> Imm_8<5> ICV_2 $T=0 6400 0 0 $X=-240 $Y=6220
X19 VSS! Imm_16<6> Imm_8<6> VDD! Imm_16<7> Imm_8<7> ICV_2 $T=0 9600 0 0 $X=-240 $Y=9420
.ENDS
***************************************
