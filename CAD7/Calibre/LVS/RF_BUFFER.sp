* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT RF_BUFFER BUFF_A QA BUFF_B QB VSS! VDD!
** N=77 EP=6 IP=0 FDC=16
M0 VSS! 6 QA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1010 $D=97
M1 QA 6 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1490 $D=97
M2 VSS! 6 QA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1970 $D=97
M3 VSS! BUFF_A 6 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2960 $D=97
M4 VSS! 8 QB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=780 $D=97
M5 QB 8 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=1260 $D=97
M6 VSS! 8 QB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=1740 $D=97
M7 VSS! BUFF_B 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=2680 $D=97
M8 VDD! 6 QA VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1490 $Y=1010 $D=189
M9 QA 6 VDD! VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1490 $Y=1490 $D=189
M10 VDD! 6 QA VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1490 $Y=1970 $D=189
M11 VDD! BUFF_A 6 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1850 $Y=2960 $D=189
M12 VDD! 8 QB VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=780 $D=189
M13 QB 8 VDD! VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1260 $D=189
M14 VDD! 8 QB VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1740 $D=189
M15 VDD! BUFF_B 8 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2680 $D=189
.ENDS
***************************************
