* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT MUX_Rsrc_1bit_BUFFER SEL_BAR<1> SEL_BAR<0> SEL<1> SEL<0> VSS! VDD!
** N=89 EP=6 IP=0 FDC=24
M0 VSS! 1 SEL_BAR<1> VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=440 $D=97
M1 SEL_BAR<1> 1 VSS! VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1000 $D=97
M2 VSS! 1 SEL_BAR<1> VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1560 $D=97
M3 1 8 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2120 $D=97
M4 VSS! 8 1 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2680 $D=97
M5 8 SEL<1> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3240 $D=97
M6 4 10 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5200 $Y=2120 $D=97
M7 VSS! 10 4 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5200 $Y=2680 $D=97
M8 VSS! 4 SEL_BAR<0> VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5220 $Y=440 $D=97
M9 SEL_BAR<0> 4 VSS! VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5220 $Y=1000 $D=97
M10 VSS! 4 SEL_BAR<0> VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5220 $Y=1560 $D=97
M11 10 SEL<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=3240 $D=97
M12 VDD! 1 SEL_BAR<1> VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1830 $Y=440 $D=189
M13 SEL_BAR<1> 1 VDD! VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1830 $Y=1000 $D=189
M14 VDD! 1 SEL_BAR<1> VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1830 $Y=1560 $D=189
M15 1 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1870 $Y=2120 $D=189
M16 VDD! 8 1 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1870 $Y=2680 $D=189
M17 8 SEL<1> VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=3240 $D=189
M18 VDD! 4 SEL_BAR<0> VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=440 $D=189
M19 SEL_BAR<0> 4 VDD! VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=1000 $D=189
M20 VDD! 4 SEL_BAR<0> VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=1560 $D=189
M21 4 10 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=2120 $D=189
M22 VDD! 10 4 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=2680 $D=189
M23 10 SEL<0> VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=3240 $D=189
.ENDS
***************************************
