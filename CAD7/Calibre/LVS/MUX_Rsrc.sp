* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT MUX_Rsrc_1bit SEL<1> SEL_BAR<1> SEL_BAR<0> SEL<0> ALU_B Rsrc VSS! VDD!
** N=86 EP=8 IP=0 FDC=18
M0 12 SEL<1> 10 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=540 $D=97
M1 11 SEL_BAR<1> 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1100 $D=97
M2 ALU_B 12 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2240 $D=97
M3 VSS! 12 ALU_B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2800 $D=97
M4 ALU_B 12 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3360 $D=97
M5 VSS! 12 ALU_B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3900 $D=97
M6 VSS! 10 11 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=2340 $D=97
M7 10 SEL<0> Imm_16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5250 $Y=540 $D=97
M8 Rsrc SEL_BAR<0> 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5250 $Y=1100 $D=97
M9 ALU_B 12 VDD! VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=2240 $D=189
M10 VDD! 12 ALU_B VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=2800 $D=189
M11 ALU_B 12 VDD! VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=3360 $D=189
M12 VDD! 12 ALU_B VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=3900 $D=189
M13 12 SEL_BAR<1> 10 VDD! pfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2050 $Y=540 $D=189
M14 11 SEL<1> 12 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1100 $D=189
M15 10 SEL_BAR<0> Imm_16 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=540 $D=189
M16 Rsrc SEL<0> 10 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=1100 $D=189
M17 VDD! 10 11 VDD! pfet L=1.2e-07 W=6.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=2340 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9 10
** N=11 EP=10 IP=18 FDC=36
X0 1 2 3 4 5 6 9 10 MUX_Rsrc_1bit $T=0 0 0 0 $X=-240 $Y=-160
X1 1 2 3 4 7 8 9 10 MUX_Rsrc_1bit $T=6000 0 0 0 $X=5760 $Y=-160
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11 12 13 14
** N=15 EP=14 IP=22 FDC=72
X0 1 2 3 4 5 6 7 8 13 14 ICV_1 $T=0 0 0 0 $X=-240 $Y=-160
X1 1 2 3 4 9 10 11 12 13 14 ICV_1 $T=12000 0 0 0 $X=11760 $Y=-160
.ENDS
***************************************
.SUBCKT MUX_Rsrc SEL<1> SEL<0> B<0> Rsrc<0> B<1> Rsrc<1> B<2> Rsrc<2> B<3> Rsrc<3> B<4> Rsrc<4> B<5> Rsrc<5> B<6> Rsrc<6> B<7> Rsrc<7> B<8> Rsrc<8>
+ B<9> Rsrc<9> B<10> Rsrc<10> B<11> Rsrc<11> B<12> Rsrc<12> B<13> Rsrc<13> B<14> Rsrc<14> B<15> Rsrc<15> VSS! VDD!
** N=121 EP=36 IP=60 FDC=312
M0 VSS! 1 2 VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=440 $D=97
M1 2 1 VSS! VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1000 $D=97
M2 VSS! 1 2 VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1560 $D=97
M3 1 40 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2120 $D=97
M4 VSS! 40 1 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2680 $D=97
M5 40 SEL<1> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3240 $D=97
M6 4 42 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5200 $Y=2120 $D=97
M7 VSS! 42 4 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5200 $Y=2680 $D=97
M8 VSS! 4 3 VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5220 $Y=440 $D=97
M9 3 4 VSS! VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5220 $Y=1000 $D=97
M10 VSS! 4 3 VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5220 $Y=1560 $D=97
M11 42 SEL<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=3240 $D=97
M12 VDD! 1 2 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1830 $Y=440 $D=189
M13 2 1 VDD! VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1830 $Y=1000 $D=189
M14 VDD! 1 2 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1830 $Y=1560 $D=189
M15 1 40 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1870 $Y=2120 $D=189
M16 VDD! 40 1 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1870 $Y=2680 $D=189
M17 40 SEL<1> VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=3240 $D=189
M18 VDD! 4 3 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=440 $D=189
M19 3 4 VDD! VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=1000 $D=189
M20 VDD! 4 3 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=1560 $D=189
M21 4 42 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=2120 $D=189
M22 VDD! 42 4 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=2680 $D=189
M23 42 SEL<0> VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=3240 $D=189
X24 1 2 3 4 B<0> Rsrc<0> B<1> Rsrc<1> B<2> Rsrc<2> B<3> Rsrc<3> VSS! VDD! ICV_2 $T=14400 0 0 0 $X=14160 $Y=-160
X25 1 2 3 4 B<4> Rsrc<4> B<5> Rsrc<5> B<6> Rsrc<6> B<7> Rsrc<7> VSS! VDD! ICV_2 $T=38400 0 0 0 $X=38160 $Y=-160
X26 1 2 3 4 B<8> Rsrc<8> B<9> Rsrc<9> B<10> Rsrc<10> B<11> Rsrc<11> VSS! VDD! ICV_2 $T=62400 0 0 0 $X=62160 $Y=-160
X27 1 2 3 4 B<12> Rsrc<12> B<13> Rsrc<13> B<14> Rsrc<14> B<15> Rsrc<15> VSS! VDD! ICV_2 $T=86400 0 0 0 $X=86160 $Y=-160
.ENDS
***************************************
