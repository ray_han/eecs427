* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT MUX_Shifter_1bit Rdest Shifter SEL Imm_16 SEL_BAR VSS! VDD!
** N=50 EP=7 IP=0 FDC=10
M0 8 SEL Imm_16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=570 $D=97
M1 Rdest SEL_BAR 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1330 $D=97
M2 VSS! 8 9 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=470 $D=97
M3 Shifter 9 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1000 $D=97
M4 VSS! 9 Shifter VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1480 $D=97
M5 8 SEL_BAR Imm_16 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=570 $D=189
M6 Rdest SEL 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1330 $D=189
M7 VDD! 8 9 VDD! pfet L=1.2e-07 W=7.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=470 $D=189
M8 Shifter 9 VDD! VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1000 $D=189
M9 VDD! 9 Shifter VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1480 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9 10
** N=10 EP=10 IP=14 FDC=20
X0 1 5 2 3 4 9 10 MUX_Shifter_1bit $T=-6000 0 0 0 $X=-6240 $Y=-300
X1 6 8 2 7 4 9 10 MUX_Shifter_1bit $T=0 0 0 0 $X=-240 $Y=-300
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
** N=16 EP=16 IP=20 FDC=40
X0 1 2 3 4 5 6 7 8 15 16 ICV_1 $T=-12000 0 0 0 $X=-18240 $Y=-300
X1 9 2 10 4 11 12 13 14 15 16 ICV_1 $T=0 0 0 0 $X=-6240 $Y=-300
.ENDS
***************************************
.SUBCKT MUX_Shifter Rdest<3> Imm_16<3> SH_MUX<3> Rdest<2> Imm_16<2> SH_MUX<2> Rdest<1> Imm_16<1> SH_MUX<1> Rdest<0> Imm_16<0> SH_MUX<0> Rdest<7> Imm_16<7> SH_MUX<7> Rdest<6> Imm_16<6> SH_MUX<6> Rdest<5> Imm_16<5>
+ SH_MUX<5> Rdest<4> Imm_16<4> SH_MUX<4> Rdest<11> Imm_16<11> SH_MUX<11> Rdest<10> Imm_16<10> SH_MUX<10> Rdest<9> Imm_16<9> SH_MUX<9> Rdest<8> Imm_16<8> SH_MUX<8> Rdest<15> Imm_16<15> SH_MUX<15> Rdest<14>
+ Imm_16<14> SH_MUX<14> Rdest<13> Imm_16<13> SH_MUX<13> Rdest<12> Imm_16<12> SH_MUX<12> VDD! SEL VSS!
** N=91 EP=51 IP=64 FDC=170
M0 2 1 VSS! VSS! nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4280 $Y=1850 $D=97
M1 VSS! 1 2 VSS! nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4280 $Y=2330 $D=97
M2 1 53 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=850 $D=97
M3 VSS! 53 1 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=1330 $D=97
M4 VSS! SEL 53 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4470 $Y=320 $D=97
M5 VDD! SEL 53 VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=320 $D=189
M6 1 53 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=850 $D=189
M7 VDD! 53 1 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=1330 $D=189
M8 2 1 VDD! VDD! pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=1850 $D=189
M9 VDD! 1 2 VDD! pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=2330 $D=189
X10 Rdest<3> 1 Imm_16<3> 2 SH_MUX<3> Rdest<2> Imm_16<2> SH_MUX<2> Rdest<1> Imm_16<1> SH_MUX<1> Rdest<0> Imm_16<0> SH_MUX<0> VSS! VDD! ICV_2 $T=11200 400 1 180 $X=4960 $Y=100
X11 Rdest<7> 1 Imm_16<7> 2 SH_MUX<7> Rdest<6> Imm_16<6> SH_MUX<6> Rdest<5> Imm_16<5> SH_MUX<5> Rdest<4> Imm_16<4> SH_MUX<4> VSS! VDD! ICV_2 $T=35200 400 1 180 $X=28960 $Y=100
X12 Rdest<11> 1 Imm_16<11> 2 SH_MUX<11> Rdest<10> Imm_16<10> SH_MUX<10> Rdest<9> Imm_16<9> SH_MUX<9> Rdest<8> Imm_16<8> SH_MUX<8> VSS! VDD! ICV_2 $T=59200 400 1 180 $X=52960 $Y=100
X13 Rdest<15> 1 Imm_16<15> 2 SH_MUX<15> Rdest<14> Imm_16<14> SH_MUX<14> Rdest<13> Imm_16<13> SH_MUX<13> Rdest<12> Imm_16<12> SH_MUX<12> VSS! VDD! ICV_2 $T=83200 400 1 180 $X=76960 $Y=100
.ENDS
***************************************
