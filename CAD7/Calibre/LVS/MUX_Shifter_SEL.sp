* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT MUX_Shifter_SEL_BUF SEL_OUT SEL_IN VDD! VSS!
** N=25 EP=4 IP=0 FDC=4
M0 VSS! 6 SEL_OUT VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=450 $D=97
M1 6 SEL_IN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=450 $D=97
M2 VDD! 6 SEL_OUT VDD! pfet L=1.2e-07 W=7.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=1750 $D=189
M3 6 SEL_IN VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT MUX_Shifter_SEL_1bit SEL<1> ALU_B Imm_8 SEL<0> Rsrc VSS! EIGHT VDD!
** N=104 EP=8 IP=0 FDC=22
M0 VSS! SEL<1> 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=450 $D=97
M1 ALU_B 14 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=5210 $D=97
M2 12 SEL<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=450 $D=97
M3 VSS! 14 ALU_B VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=5210 $D=97
M4 VSS! EIGHT 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=450 $D=97
M5 14 SEL<1> 10 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=5070 $D=97
M6 15 Imm_8 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=450 $D=97
M7 11 9 14 VSS! nfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2780 $Y=5230 $D=97
M8 11 SEL<0> 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=450 $D=97
M9 13 12 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=5270 $D=97
M10 VSS! Rsrc 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=450 $D=97
M11 VDD! SEL<1> 9 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=1950 $D=189
M12 ALU_B 14 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=3510 $D=189
M13 12 SEL<0> VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=1950 $D=189
M14 VDD! 14 ALU_B VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=3510 $D=189
M15 VDD! EIGHT 10 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=1950 $D=189
M16 14 9 10 VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=3510 $D=189
M17 15 Imm_8 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=1950 $D=189
M18 11 SEL<1> 14 VDD! pfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2880 $Y=3510 $D=189
M19 11 12 15 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=2210 $D=189
M20 13 SEL<0> 11 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=3510 $D=189
M21 VDD! Rsrc 13 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=1950 $D=189
.ENDS
***************************************
.SUBCKT MUX_Shifter_SEL Shifter_SEL<3> Imm_8<3> Rsrc<3> Shifter_SEL<2> Imm_8<2> Rsrc<2> Shifter_SEL<1> Imm_8<1> Rsrc<1> Shifter_SEL<0> Imm_8<0> Rsrc<0> SEL<0> SEL<1> VSS! VDD!
** N=19 EP=16 IP=42 FDC=96
X0 14 SEL<0> VDD! VSS! MUX_Shifter_SEL_BUF $T=0 1600 0 270 $X=-250 $Y=-180
X1 13 SEL<1> VDD! VSS! MUX_Shifter_SEL_BUF $T=6000 1600 1 270 $X=2510 $Y=-180
X2 13 Shifter_SEL<3> Imm_8<3> 14 Rsrc<3> VSS! VDD! VDD! MUX_Shifter_SEL_1bit $T=0 1600 1 90 $X=-240 $Y=1170
X3 13 Shifter_SEL<2> Imm_8<2> 14 Rsrc<2> VSS! VSS! VDD! MUX_Shifter_SEL_1bit $T=0 6400 1 90 $X=-240 $Y=5970
X4 13 Shifter_SEL<1> Imm_8<1> 14 Rsrc<1> VSS! VSS! VDD! MUX_Shifter_SEL_1bit $T=0 11200 1 90 $X=-240 $Y=10770
X5 13 Shifter_SEL<0> Imm_8<0> 14 Rsrc<0> VSS! VSS! VDD! MUX_Shifter_SEL_1bit $T=0 16000 1 90 $X=-240 $Y=15570
.ENDS
***************************************
