* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT MUX_Rsrc_1bit SEL<1> SEL_BAR<1> SEL_BAR<0> SEL<0> ALU_B Imm_16 Rsrc VSS! VDD!
** N=86 EP=9 IP=0 FDC=18
M0 7 SEL<1> 1 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=540 $D=97
M1 4 SEL_BAR<1> 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1100 $D=97
M2 ALU_B 7 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2240 $D=97
M3 VSS! 7 ALU_B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2800 $D=97
M4 ALU_B 7 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3360 $D=97
M5 VSS! 7 ALU_B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3900 $D=97
M6 VSS! 1 4 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=2340 $D=97
M7 1 SEL<0> Imm_16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5250 $Y=540 $D=97
M8 Rsrc SEL_BAR<0> 1 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5250 $Y=1100 $D=97
M9 ALU_B 7 VDD! VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=2240 $D=189
M10 VDD! 7 ALU_B VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=2800 $D=189
M11 ALU_B 7 VDD! VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=3360 $D=189
M12 VDD! 7 ALU_B VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=3900 $D=189
M13 7 SEL_BAR<1> 1 VDD! pfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2050 $Y=540 $D=189
M14 4 SEL<1> 7 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1100 $D=189
M15 1 SEL_BAR<0> Imm_16 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=540 $D=189
M16 Rsrc SEL<0> 1 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=1100 $D=189
M17 VDD! 1 4 VDD! pfet L=1.2e-07 W=6.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=2340 $D=189
.ENDS
***************************************
