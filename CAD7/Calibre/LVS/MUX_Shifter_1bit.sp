* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT MUX_Shifter_1bit Rdest Shifter SEL Imm_16 SEL_BAR VSS! VDD!
** N=51 EP=7 IP=0 FDC=10
M0 3 SEL Imm_16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=570 $D=97
M1 Rdest SEL_BAR 3 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1330 $D=97
M2 VSS! 3 7 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=470 $D=97
M3 Shifter 7 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1000 $D=97
M4 VSS! 7 Shifter VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1480 $D=97
M5 3 SEL_BAR Imm_16 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=570 $D=189
M6 Rdest SEL 3 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1330 $D=189
M7 VDD! 3 7 VDD! pfet L=1.2e-07 W=7.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=470 $D=189
M8 Shifter 7 VDD! VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1000 $D=189
M9 VDD! 7 Shifter VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1480 $D=189
.ENDS
***************************************
