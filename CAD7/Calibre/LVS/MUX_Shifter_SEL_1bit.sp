* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT MUX_Shifter_SEL_1bit SEL<1> ALU_B Imm_8 SEL<0> Rsrc VSS! EIGHT VDD!
** N=104 EP=8 IP=0 FDC=22
M0 VSS! SEL<1> 1 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=450 $D=97
M1 ALU_B 11 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=5210 $D=97
M2 7 SEL<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=450 $D=97
M3 VSS! 11 ALU_B VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=5210 $D=97
M4 VSS! EIGHT 4 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=450 $D=97
M5 11 SEL<1> 4 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=5070 $D=97
M6 14 Imm_8 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=450 $D=97
M7 6 1 11 VSS! nfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2780 $Y=5230 $D=97
M8 6 SEL<0> 14 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=450 $D=97
M9 10 7 6 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=5270 $D=97
M10 VSS! Rsrc 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=450 $D=97
M11 VDD! SEL<1> 1 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=1950 $D=189
M12 ALU_B 11 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=3510 $D=189
M13 7 SEL<0> VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=1950 $D=189
M14 VDD! 11 ALU_B VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=3510 $D=189
M15 VDD! EIGHT 4 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=1950 $D=189
M16 11 1 4 VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=3510 $D=189
M17 14 Imm_8 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=1950 $D=189
M18 6 SEL<1> 11 VDD! pfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2880 $Y=3510 $D=189
M19 6 7 14 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=2210 $D=189
M20 10 SEL<0> 6 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=3510 $D=189
M21 VDD! Rsrc 10 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=1950 $D=189
.ENDS
***************************************
