* SPICE NETLIST
***************************************

*.CALIBRE ABORT_INFO SOFTCHK
.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT 3rd_bit SH<2> SH<2>_BAR D 4 5 6 7 8
** N=50 EP=8 IP=0 FDC=8
*.SEEDPROM
M0 5 4 D 8 nfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=360 $D=3
M1 9 SH<2> 4 8 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1320 $D=3
M2 7 4 D 8 nfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4920 $Y=360 $D=3
M3 10 SH<2>_BAR 4 8 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=1320 $D=3
M4 6 4 D 6 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=360 $D=189
M5 9 SH<2>_BAR 4 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1320 $D=189
M6 6 4 D 6 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=360 $D=189
M7 10 SH<2> 4 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1320 $D=189
.ENDS
***************************************
.SUBCKT 4th_bit 1 2 3 4 5 6 7 8 9
** N=98 EP=9 IP=0 FDC=22
M0 10 1 5 9 nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=520 $D=97
M1 12 10 4 9 nfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=1520 $D=97
M2 11 12 4 9 nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=2460 $D=97
M3 4 12 11 9 nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=2940 $D=97
M4 3 11 4 9 nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=3420 $D=97
M5 8 11 3 9 nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=1520 $D=97
M6 3 11 8 9 nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=2000 $D=97
M7 8 11 3 9 nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=2480 $D=97
M8 3 11 8 9 nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=2960 $D=97
M9 8 11 3 9 nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=3440 $D=97
M10 10 2 7 9 nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5230 $Y=520 $D=97
M11 12 10 6 6 pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1760 $Y=1520 $D=189
M12 3 11 6 6 pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1790 $Y=3420 $D=189
M13 11 12 6 6 pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2460 $D=189
M14 6 12 11 6 pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2940 $D=189
M15 10 2 5 6 pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2150 $Y=520 $D=189
M16 10 1 7 6 pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
M17 6 11 3 6 pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1520 $D=189
M18 3 11 6 6 pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2000 $D=189
M19 6 11 3 6 pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2480 $D=189
M20 3 11 6 6 pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2960 $D=189
M21 6 11 3 6 pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3440 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25
** N=26 EP=25 IP=34 FDC=60
*.SEEDPROM
X0 1 2 9 23 7 8 10 24 3rd_bit $T=0 -1200 1 0 $X=-240 $Y=-3600
X1 1 2 12 26 10 11 13 24 3rd_bit $T=6000 -1200 1 0 $X=5760 $Y=-3600
X2 3 4 5 14 15 16 17 18 25 4th_bit $T=0 0 0 0 $X=-240 $Y=-200
X3 3 4 6 18 19 20 21 22 25 4th_bit $T=6000 0 0 0 $X=5760 $Y=-200
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
** N=42 EP=40 IP=50 FDC=120
*.SEEDPROM
X0 1 3 2 4 5 6 9 12 14 16 20 41 22 10 11 13 15 17 18 19
+ 21 23 38 39 40
+ ICV_1 $T=0 0 0 0 $X=-240 $Y=-3600
X1 1 3 2 4 7 8 22 25 27 29 33 35 36 23 24 26 28 30 31 32
+ 34 37 42 39 40
+ ICV_1 $T=12000 0 0 0 $X=11760 $Y=-3600
.ENDS
***************************************
.SUBCKT LVS_first SH3_BAR SH<3> E<0> E<1> E<2> E<3> E<4> E<5> E<6> E<7> E<8> E<9> E<10> E<11> E<12> E<13> E<14> E<15>
** N=192 EP=18 IP=160 FDC=499
X0 43 102 subc $X=61530 $Y=4770 $D=1
X1 45 102 subc $X=67530 $Y=4770 $D=1
X2 47 102 subc $X=73530 $Y=4770 $D=1
X3 17 102 subc $X=77510 $Y=4770 $D=1
X4 48 102 subc $X=79530 $Y=4770 $D=1
X5 50 102 subc $X=85530 $Y=4770 $D=1
X6 53 102 subc $X=91530 $Y=4770 $D=1
X7 56 102 subc $X=97530 $Y=4770 $D=1
X8 57 102 subc $X=1530 $Y=5770 $D=1
X9 72 102 subc $X=25530 $Y=5770 $D=1
X10 87 102 subc $X=49530 $Y=5770 $D=1
X11 61 102 subc $X=7530 $Y=5770 $D=1
X12 76 102 subc $X=31530 $Y=5770 $D=1
X13 92 102 subc $X=55530 $Y=5770 $D=1
X14 64 102 subc $X=13530 $Y=5770 $D=1
X15 79 102 subc $X=37530 $Y=5770 $D=1
X16 68 102 subc $X=19530 $Y=5770 $D=1
X17 83 102 subc $X=43530 $Y=5770 $D=1
M18 17 98 17 102 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=78300 $Y=6130 $D=3
X19 3 SH<3> 4 SH3_BAR E<0> E<1> E<2> E<3> 57 58 35 59 35 60 5 61 62 36 36 63
+ 6 64 65 37 66 37 67 7 68 69 38 38 70 8 71 72 73 99 102 43
+ ICV_2 $T=1670 4570 1 0 $X=1430 $Y=370
X20 3 SH<3> 4 SH3_BAR E<4> E<5> E<6> E<7> 72 73 39 74 39 75 9 76 77 40 40 78
+ 10 79 80 41 81 41 82 11 83 84 42 42 85 12 86 87 88 100 102 43
+ ICV_2 $T=25670 4570 1 0 $X=25430 $Y=370
X21 3 SH<3> 4 SH3_BAR E<8> E<9> E<10> E<11> 87 88 5 89 90 91 13 92 93 6 94 95
+ 14 43 43 7 44 44 96 15 45 45 8 46 46 16 97 47 47 101 102 43
+ ICV_2 $T=49670 4570 1 0 $X=49430 $Y=370
X22 3 SH<3> 4 SH3_BAR E<12> E<13> E<14> E<15> 47 47 9 17 17 17 17 48 48 10 49 49
+ 18 50 50 11 51 51 52 52 53 53 12 54 54 55 55 56 56 98 102 43
+ ICV_2 $T=73670 4570 1 0 $X=73430 $Y=370
*.CALIBRE WARNING SCONNECT SCONNECT conflict(s) detected by extraction in this cell. See extraction report for details.
.ENDS
***************************************
