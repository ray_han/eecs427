* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT 2rd_bit SH<1> SH<1>_BAR 3 4 5 6 7
** N=28 EP=7 IP=0 FDC=4
M0 5 SH<1> 3 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=520 $D=97
M1 7 SH<1>_BAR 3 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4990 $Y=520 $D=97
M2 5 SH<1>_BAR 3 6 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1930 $Y=520 $D=189
M3 7 SH<1> 3 6 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
.ENDS
***************************************
.SUBCKT Firstbit SH<0> SH<0>_BAR 3 4 5 6 7
** N=30 EP=7 IP=0 FDC=4
M0 5 SH<0> 3 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=520 $D=97
M1 7 SH<0>_BAR 3 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=520 $D=97
M2 5 SH<0>_BAR 3 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=520 $D=189
M3 7 SH<0> 3 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
.ENDS
***************************************
.SUBCKT 3rd_bit SH<2> SH<2>_BAR D 4 5 6 7
** N=48 EP=7 IP=0 FDC=8
M0 4 8 D 4 nfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=360 $D=97
M1 5 SH<2> 8 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1320 $D=97
M2 4 8 D 4 nfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4920 $Y=360 $D=97
M3 7 SH<2>_BAR 8 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=1320 $D=97
M4 6 8 D 6 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=360 $D=189
M5 5 SH<2>_BAR 8 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1320 $D=189
M6 6 8 D 6 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=360 $D=189
M7 7 SH<2> 8 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1320 $D=189
.ENDS
***************************************
.SUBCKT 4th_bit SH<3> SH3_BAR E VSS! 5 VDD! 7
** N=96 EP=7 IP=0 FDC=22
M0 8 SH<3> 5 VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=520 $D=97
M1 10 8 VSS! VSS! nfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=1520 $D=97
M2 9 10 VSS! VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=2460 $D=97
M3 VSS! 10 9 VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=2940 $D=97
M4 E 9 VSS! VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=3420 $D=97
M5 VSS! 9 E VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=1520 $D=97
M6 E 9 VSS! VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=2000 $D=97
M7 VSS! 9 E VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=2480 $D=97
M8 E 9 VSS! VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=2960 $D=97
M9 VSS! 9 E VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=3440 $D=97
M10 8 SH3_BAR 7 VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5230 $Y=520 $D=97
M11 10 8 VDD! VDD! pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1760 $Y=1520 $D=189
M12 E 9 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1790 $Y=3420 $D=189
M13 9 10 VDD! VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2460 $D=189
M14 VDD! 10 9 VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2940 $D=189
M15 8 SH3_BAR 5 VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2150 $Y=520 $D=189
M16 8 SH<3> 7 VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
M17 VDD! 9 E VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1520 $D=189
M18 E 9 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2000 $D=189
M19 VDD! 9 E VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2480 $D=189
M20 E 9 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2960 $D=189
M21 VDD! 9 E VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3440 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27
** N=27 EP=27 IP=56 FDC=76
X0 1 2 15 11 12 13 14 2rd_bit $T=0 -4400 1 0 $X=-240 $Y=-6330
X1 1 2 18 11 16 13 17 2rd_bit $T=6000 -4400 1 0 $X=5760 $Y=-6330
X2 3 4 14 11 19 13 20 Firstbit $T=0 -6400 1 0 $X=-240 $Y=-8270
X3 3 4 17 11 20 13 21 Firstbit $T=6000 -6400 1 0 $X=5760 $Y=-8270
X4 5 6 23 11 22 13 15 3rd_bit $T=0 -1200 1 0 $X=-240 $Y=-3600
X5 5 6 25 11 24 13 18 3rd_bit $T=6000 -1200 1 0 $X=5760 $Y=-3600
X6 7 8 9 11 26 13 23 4th_bit $T=0 0 0 0 $X=-240 $Y=-200
X7 7 8 10 11 27 13 25 4th_bit $T=6000 0 0 0 $X=5760 $Y=-200
.ENDS
***************************************
.SUBCKT LVS_test_wodriver SH3_BAR SH<3> A<0> A<1> A<2> A<3> A<4> A<5> A<6> A<7> A<8> A<9> A<10> A<11> A<12> A<13> A<14> E<0> E<1> E<2>
+ E<3> E<4> E<5> E<6> E<7> E<8> E<9> E<10> E<11> E<12> E<13> E<14> E<15> VSS! VDD! A<15>
** N=651 EP=36 IP=216 FDC=608
X0 3 5 8 7 4 6 SH<3> SH3_BAR E<0> E<1> VSS! VSS! VDD! 11 10 VSS! 15 14 VSS! A<0>
+ A<1> VSS! 9 VSS! 13 VDD! VDD!
+ ICV_1 $T=-42040 -8370 1 0 $X=-42280 $Y=-12570
X1 3 5 8 7 4 6 SH<3> SH3_BAR E<2> E<3> VSS! 11 VDD! 19 18 15 23 22 A<1> A<2>
+ A<3> VSS! 17 VSS! 21 VDD! VDD!
+ ICV_1 $T=-30040 -8370 1 0 $X=-30280 $Y=-12570
X2 3 5 8 7 4 6 SH<3> SH3_BAR E<4> E<5> VSS! 19 VDD! 27 26 23 31 30 A<3> A<4>
+ A<5> 10 25 14 29 VDD! VDD!
+ ICV_1 $T=-18040 -8370 1 0 $X=-18280 $Y=-12570
X3 3 5 8 7 4 6 SH<3> SH3_BAR E<6> E<7> VSS! 27 VDD! 35 34 31 38 37 A<5> A<6>
+ A<7> 18 33 22 40 VDD! VDD!
+ ICV_1 $T=-6040 -8370 1 0 $X=-6280 $Y=-12570
X4 3 5 8 7 4 6 SH<3> SH3_BAR E<8> E<9> VSS! 35 VDD! 42 41 38 45 44 A<7> A<8>
+ A<9> 26 76 30 77 9 13
+ ICV_1 $T=5960 -8370 1 0 $X=5720 $Y=-12570
X5 3 5 8 7 4 6 SH<3> SH3_BAR E<10> E<11> VSS! 42 VDD! 48 47 45 51 50 A<9> A<10>
+ A<11> 34 78 37 79 17 21
+ ICV_1 $T=17960 -8370 1 0 $X=17720 $Y=-12570
X6 3 5 8 7 4 6 SH<3> SH3_BAR E<12> E<13> VSS! 48 VDD! 53 81 51 55 83 A<11> A<12>
+ A<13> 41 80 44 82 25 29
+ ICV_1 $T=29960 -8370 1 0 $X=29720 $Y=-12570
X7 3 5 8 7 4 6 SH<3> SH3_BAR E<14> E<15> VSS! 53 VDD! 86 85 55 89 88 A<13> A<14>
+ A<15> 47 84 50 87 33 40
+ ICV_1 $T=41960 -8370 1 0 $X=41720 $Y=-12570
.ENDS
***************************************
