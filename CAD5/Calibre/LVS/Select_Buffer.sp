* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT Select_Buffer SH0 SH<0> SH0_BAR SH1_BAR SH<1> SH1 SH2 SH<2> SH2_BAR SH3_BAR SH<3> SH3 VSS! VDD!
** N=239 EP=14 IP=0 FDC=40
M0 VSS! SH0 2 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=720 $Y=490 $D=97
M1 SH<0> 2 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1240 $Y=490 $D=97
M2 VSS! 2 SH<0> VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1720 $Y=490 $D=97
M3 SH0_BAR SH<0> VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2240 $Y=490 $D=97
M4 VSS! SH<0> SH0_BAR VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2720 $Y=490 $D=97
M5 SH1_BAR SH<1> VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3200 $Y=490 $D=97
M6 VSS! SH<1> SH1_BAR VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3680 $Y=490 $D=97
M7 SH<1> 8 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4200 $Y=490 $D=97
M8 VSS! 8 SH<1> VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4680 $Y=490 $D=97
M9 8 SH1 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5200 $Y=490 $D=97
M10 VSS! SH2 10 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6280 $Y=490 $D=97
M11 SH<2> 10 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6800 $Y=490 $D=97
M12 VSS! 10 SH<2> VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7280 $Y=490 $D=97
M13 SH2_BAR SH<2> VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7800 $Y=490 $D=97
M14 VSS! SH<2> SH2_BAR VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8280 $Y=490 $D=97
M15 SH3_BAR SH<3> VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8760 $Y=490 $D=97
M16 VSS! SH<3> SH3_BAR VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9240 $Y=490 $D=97
M17 SH<3> 18 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9760 $Y=490 $D=97
M18 VSS! 18 SH<3> VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10240 $Y=490 $D=97
M19 18 SH3 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10760 $Y=490 $D=97
M20 VDD! SH0 2 VDD! pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=720 $Y=3910 $D=189
M21 SH<0> 2 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1240 $Y=3070 $D=189
M22 VDD! 2 SH<0> VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1720 $Y=3070 $D=189
M23 SH0_BAR SH<0> VDD! VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2240 $Y=2330 $D=189
M24 VDD! SH<0> SH0_BAR VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2720 $Y=2330 $D=189
M25 SH1_BAR SH<1> VDD! VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3200 $Y=2330 $D=189
M26 VDD! SH<1> SH1_BAR VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3680 $Y=2330 $D=189
M27 SH<1> 8 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4200 $Y=3070 $D=189
M28 VDD! 8 SH<1> VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4680 $Y=3070 $D=189
M29 8 SH1 VDD! VDD! pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5200 $Y=3910 $D=189
M30 VDD! SH2 10 VDD! pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6280 $Y=3910 $D=189
M31 SH<2> 10 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6800 $Y=3070 $D=189
M32 VDD! 10 SH<2> VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7280 $Y=3070 $D=189
M33 SH2_BAR SH<2> VDD! VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7800 $Y=2330 $D=189
M34 VDD! SH<2> SH2_BAR VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8280 $Y=2330 $D=189
M35 SH3_BAR SH<3> VDD! VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8760 $Y=2330 $D=189
M36 VDD! SH<3> SH3_BAR VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9240 $Y=2330 $D=189
M37 SH<3> 18 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9760 $Y=3070 $D=189
M38 VDD! 18 SH<3> VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10240 $Y=3070 $D=189
M39 18 SH3 VDD! VDD! pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10760 $Y=3910 $D=189
.ENDS
***************************************
