* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT 3rd_bit
** N=79 EP=0 IP=0 FDC=0
*.SEEDPROM
*.CALIBRE ISOLATED NETS: SH<2> SH<2>_BAR D
.ENDS
***************************************
.SUBCKT 2rd_bit
** N=52 EP=0 IP=0 FDC=0
*.SEEDPROM
*.CALIBRE ISOLATED NETS: SH<1> SH<1>_BAR
.ENDS
***************************************
.SUBCKT Data_Buffer
** N=100 EP=0 IP=0 FDC=0
*.SEEDPROM
*.CALIBRE ISOLATED NETS: E VSS! IN VDD!
.ENDS
***************************************
.SUBCKT 4th_bit
** N=52 EP=0 IP=0 FDC=0
*.SEEDPROM
*.CALIBRE ISOLATED NETS: SH3 SH3_BAR OUT VSS! IN1 VDD! IN2
.ENDS
***************************************
.SUBCKT ICV_1
** N=41 EP=0 IP=57 FDC=0
*.SEEDPROM
.ENDS
***************************************
.SUBCKT 1st_bit
** N=54 EP=0 IP=0 FDC=0
*.SEEDPROM
*.CALIBRE ISOLATED NETS: SH<0> SH<0>_BAR
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 151 152 153 154 155
** N=158 EP=32 IP=236 FDC=63
*.SEEDPROM
M0 14 156 11 11 nfet L=1.2e-07 W=6.3e-07 AD=2.016e-13 AS=2.016e-13 PD=1.9e-06 PS=1.9e-06 NRD=0.507937 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=2.4e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=450 $Y=-1940 $D=25
M1 156 1 12 11 nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.04e-14 panw9=1.32e-14 panw10=0 $X=450 $Y=-2900 $D=25
M2 14 156 11 11 nfet L=1.2e-07 W=6.3e-07 AD=2.016e-13 AS=2.016e-13 PD=1.9e-06 PS=1.9e-06 NRD=0.507937 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=2.4e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=4920 $Y=-1940 $D=25
M3 156 2 15 11 nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.04e-14 panw9=1.32e-14 panw10=0 $X=5270 $Y=-2900 $D=25
M4 17 157 11 11 nfet L=1.2e-07 W=6.3e-07 AD=2.016e-13 AS=2.016e-13 PD=1.9e-06 PS=1.9e-06 NRD=0.507937 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=2.4e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=6450 $Y=-1940 $D=25
M5 157 1 16 11 nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.04e-14 panw9=1.32e-14 panw10=0 $X=6450 $Y=-2900 $D=25
M6 17 157 11 11 nfet L=1.2e-07 W=6.3e-07 AD=2.016e-13 AS=2.016e-13 PD=1.9e-06 PS=1.9e-06 NRD=0.507937 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=2.4e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=10920 $Y=-1940 $D=25
M7 157 2 18 11 nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.04e-14 panw9=1.32e-14 panw10=0 $X=11270 $Y=-2900 $D=25
M8 15 3 19 11 nfet L=1.2e-07 W=5.6e-07 AD=1.792e-13 AS=1.792e-13 PD=1.76e-06 PS=1.76e-06 NRD=0.571429 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-14 panw8=2.4e-14 panw9=1.92e-14 panw10=0 $X=450 $Y=-5300 $D=25
M9 15 4 20 11 nfet L=1.2e-07 W=5.6e-07 AD=1.792e-13 AS=1.792e-13 PD=1.76e-06 PS=1.76e-06 NRD=0.571429 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.16e-14 panw8=2.4e-14 panw9=2.16e-14 panw10=0 $X=4990 $Y=-5300 $D=25
M10 18 3 21 11 nfet L=1.2e-07 W=5.6e-07 AD=1.792e-13 AS=1.792e-13 PD=1.76e-06 PS=1.76e-06 NRD=0.571429 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-14 panw8=2.4e-14 panw9=1.92e-14 panw10=0 $X=6450 $Y=-5300 $D=25
M11 18 4 22 11 nfet L=1.2e-07 W=5.6e-07 AD=1.792e-13 AS=1.792e-13 PD=1.76e-06 PS=1.76e-06 NRD=0.571429 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.16e-14 panw8=2.4e-14 panw9=2.16e-14 panw10=0 $X=10990 $Y=-5300 $D=25
M12 11 152 9 11 nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=2.24e-13 PD=1.06e-06 PS=2.04e-06 NRD=0.257143 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=4870 $Y=1260 $D=25
M13 9 152 11 11 nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=4870 $Y=1740 $D=25
M14 11 152 9 11 nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=4870 $Y=2220 $D=25
M15 9 152 11 11 nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=4870 $Y=2700 $D=25
M16 11 152 9 11 nfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.26e-13 PD=2.04e-06 PS=1.06e-06 NRD=0.457143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=4870 $Y=3180 $D=25
M17 151 6 14 11 nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=5230 $Y=260 $D=25
M18 158 154 11 11 nfet L=1.2e-07 W=7.3e-07 AD=2.336e-13 AS=2.336e-13 PD=2.1e-06 PS=2.1e-06 NRD=0.438356 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=6430 $Y=1260 $D=25
M19 155 158 11 11 nfet L=1.2e-07 W=5e-07 AD=9e-14 AS=1.6e-13 PD=8.6e-07 PS=1.64e-06 NRD=0.36 NRS=0.64 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.28e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=6430 $Y=2200 $D=25
M20 11 158 155 11 nfet L=1.2e-07 W=5e-07 AD=9.66667e-14 AS=9e-14 PD=8.83333e-07 PS=8.6e-07 NRD=0.386667 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=6430 $Y=2680 $D=25
M21 10 155 11 11 nfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.35333e-13 PD=2.04e-06 PS=1.23667e-06 NRD=0.457143 NRS=0.27619 m=1 par=1 nf=1 ngcon=1 psp=0 sa=9.88571e-07 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=6430 $Y=3160 $D=25
M22 154 5 24 11 nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=6430 $Y=260 $D=25
M23 20 8 26 11 nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=5270 $Y=-7300 $D=25
M24 22 7 26 11 nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=6450 $Y=-7300 $D=25
M25 14 156 13 13 pfet L=1.2e-07 W=6.3e-07 AD=2.016e-13 AS=2.016e-13 PD=1.9e-06 PS=1.9e-06 NRD=0.507937 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.56e-14 panw9=0 panw10=6e-15 $X=1860 $Y=-1940 $D=108
M26 156 2 12 13 pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.8e-14 panw8=1.56e-14 panw9=0 panw10=6e-15 $X=2210 $Y=-2900 $D=108
M27 14 156 13 13 pfet L=1.2e-07 W=6.3e-07 AD=2.016e-13 AS=2.016e-13 PD=1.9e-06 PS=1.9e-06 NRD=0.507937 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.56e-14 panw9=0 panw10=6e-15 $X=3510 $Y=-1940 $D=108
M28 156 1 15 13 pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.8e-14 panw8=1.56e-14 panw9=0 panw10=6e-15 $X=3510 $Y=-2900 $D=108
M29 17 157 13 13 pfet L=1.2e-07 W=6.3e-07 AD=2.016e-13 AS=2.016e-13 PD=1.9e-06 PS=1.9e-06 NRD=0.507937 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.56e-14 panw9=0 panw10=6e-15 $X=7860 $Y=-1940 $D=108
M30 157 2 16 13 pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.8e-14 panw8=1.56e-14 panw9=0 panw10=6e-15 $X=8210 $Y=-2900 $D=108
M31 17 157 13 13 pfet L=1.2e-07 W=6.3e-07 AD=2.016e-13 AS=2.016e-13 PD=1.9e-06 PS=1.9e-06 NRD=0.507937 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.56e-14 panw9=0 panw10=6e-15 $X=9510 $Y=-1940 $D=108
M32 157 1 18 13 pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.8e-14 panw8=1.56e-14 panw9=0 panw10=6e-15 $X=9510 $Y=-2900 $D=108
M33 15 4 19 13 pfet L=1.2e-07 W=5.6e-07 AD=1.792e-13 AS=1.792e-13 PD=1.76e-06 PS=1.76e-06 NRD=0.571429 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=9.6e-15 panw9=0 panw10=1.44e-14 $X=1930 $Y=-5300 $D=108
M34 15 3 20 13 pfet L=1.2e-07 W=5.6e-07 AD=1.792e-13 AS=1.792e-13 PD=1.76e-06 PS=1.76e-06 NRD=0.571429 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=7.2e-15 panw9=0 panw10=1.2e-14 $X=3510 $Y=-5300 $D=108
M35 18 4 21 13 pfet L=1.2e-07 W=5.6e-07 AD=1.792e-13 AS=1.792e-13 PD=1.76e-06 PS=1.76e-06 NRD=0.571429 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=9.6e-15 panw9=0 panw10=1.44e-14 $X=7930 $Y=-5300 $D=108
M36 18 3 22 13 pfet L=1.2e-07 W=5.6e-07 AD=1.792e-13 AS=1.792e-13 PD=1.76e-06 PS=1.76e-06 NRD=0.571429 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=7.2e-15 panw9=0 panw10=1.2e-14 $X=9510 $Y=-5300 $D=108
M37 153 151 13 13 pfet L=1.2e-07 W=7.3e-07 AD=2.336e-13 AS=2.336e-13 PD=2.1e-06 PS=2.1e-06 NRD=0.438356 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=1760 $Y=1260 $D=108
M38 9 152 13 13 pfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.35333e-13 PD=2.04e-06 PS=1.23667e-06 NRD=0.457143 NRS=0.27619 m=1 par=1 nf=1 ngcon=1 psp=0 sa=9.88571e-07 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.08e-13 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=1790 $Y=3160 $D=108
M39 152 153 13 13 pfet L=1.2e-07 W=5e-07 AD=9e-14 AS=1.6e-13 PD=8.6e-07 PS=1.64e-06 NRD=0.36 NRS=0.64 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.28e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=6e-14 $X=1990 $Y=2200 $D=108
M40 13 153 152 13 pfet L=1.2e-07 W=5e-07 AD=9.66667e-14 AS=9e-14 PD=8.83333e-07 PS=8.6e-07 NRD=0.386667 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=6.36e-14 panw10=0 $X=1990 $Y=2680 $D=108
M41 13 152 9 13 pfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=2.24e-13 PD=1.06e-06 PS=2.04e-06 NRD=0.257143 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=3510 $Y=1260 $D=108
M42 9 152 13 13 pfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=3510 $Y=1740 $D=108
M43 13 152 9 13 pfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=8.4e-14 $X=3510 $Y=2220 $D=108
M44 9 152 13 13 pfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=8.76e-14 panw10=0 $X=3510 $Y=2700 $D=108
M45 13 152 9 13 pfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.26e-13 PD=2.04e-06 PS=1.06e-06 NRD=0.457143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.08e-13 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=3510 $Y=3180 $D=108
M46 151 6 23 13 pfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=2150 $Y=260 $D=108
M47 151 5 14 13 pfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=3510 $Y=260 $D=108
M48 158 154 13 13 pfet L=1.2e-07 W=7.3e-07 AD=2.336e-13 AS=2.336e-13 PD=2.1e-06 PS=2.1e-06 NRD=0.438356 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=7760 $Y=1260 $D=108
M49 10 155 13 13 pfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.35333e-13 PD=2.04e-06 PS=1.23667e-06 NRD=0.457143 NRS=0.27619 m=1 par=1 nf=1 ngcon=1 psp=0 sa=9.88571e-07 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.08e-13 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=7790 $Y=3160 $D=108
M50 155 158 13 13 pfet L=1.2e-07 W=5e-07 AD=9e-14 AS=1.6e-13 PD=8.6e-07 PS=1.64e-06 NRD=0.36 NRS=0.64 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.28e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=6e-14 $X=7990 $Y=2200 $D=108
M51 13 158 155 13 pfet L=1.2e-07 W=5e-07 AD=9.66667e-14 AS=9e-14 PD=8.83333e-07 PS=8.6e-07 NRD=0.386667 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=6.36e-14 panw10=0 $X=7990 $Y=2680 $D=108
M52 13 155 10 13 pfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=2.24e-13 PD=1.06e-06 PS=2.04e-06 NRD=0.257143 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=9510 $Y=1260 $D=108
M53 10 155 13 13 pfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=9510 $Y=1740 $D=108
M54 13 155 10 13 pfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=8.4e-14 $X=9510 $Y=2220 $D=108
M55 10 155 13 13 pfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=8.76e-14 panw10=0 $X=9510 $Y=2700 $D=108
M56 13 155 10 13 pfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.26e-13 PD=2.04e-06 PS=1.06e-06 NRD=0.457143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.08e-13 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=9510 $Y=3180 $D=108
M57 154 6 24 13 pfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=8150 $Y=260 $D=108
M58 154 5 17 13 pfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=9510 $Y=260 $D=108
M59 20 8 25 13 pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=3.36e-14 panw10=3.36e-14 $X=2210 $Y=-7300 $D=108
M60 20 7 26 13 pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=3.36e-14 panw10=3.36e-14 $X=3510 $Y=-7300 $D=108
M61 22 8 26 13 pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=3.36e-14 panw10=3.36e-14 $X=8210 $Y=-7300 $D=108
M62 22 7 27 13 pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=3.36e-14 panw10=3.36e-14 $X=9510 $Y=-7300 $D=108
.ENDS
***************************************
.SUBCKT SHIFTER SEL<3> SEL<2> SEL<1> SEL<0> A<0> A<1> A<2> A<3> A<4> A<5> A<6> A<7> A<8> A<9> A<10> A<11> A<12> A<13> A<14> A<15>
+ E<0> E<1> E<2> E<3> E<4> E<5> E<6> E<7> E<8> E<9> E<10> E<11> E<12> E<13> E<14> E<15> VDD! VSS!
** N=1412 EP=38 IP=1136 FDC=666
M0 11 8 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 AD=2.178e-13 AS=2.75168e-13 PD=1.57e-06 PS=1.97777e-06 NRD=0.14876 NRS=0.187943 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.12e-06 sb=1.91074e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=4.8e-14 panw10=2.28e-14 $X=3500 $Y=2240 $D=25
M1 VSS! 8 11 VSS! nfet L=1.2e-07 W=1.21e-06 AD=2.178e-13 AS=2.178e-13 PD=1.57e-06 PS=1.57e-06 NRD=0.14876 NRS=0.14876 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.49421e-06 sb=1.73223e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=4.8e-14 panw10=2.28e-14 $X=3500 $Y=2720 $D=25
M2 5 9 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 AD=2.178e-13 AS=2.178e-13 PD=1.57e-06 PS=1.57e-06 NRD=0.14876 NRS=0.14876 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.73223e-06 sb=1.49421e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=4.8e-14 panw10=2.28e-14 $X=3500 $Y=3200 $D=25
M3 VSS! 9 5 VSS! nfet L=1.2e-07 W=1.21e-06 AD=2.75168e-13 AS=2.178e-13 PD=1.97777e-06 PS=1.57e-06 NRD=0.187943 NRS=0.14876 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.91074e-06 sb=1.12e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=4.8e-14 panw10=2.28e-14 $X=3500 $Y=3680 $D=25
M4 12 6 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 AD=2.178e-13 AS=2.75168e-13 PD=1.57e-06 PS=1.97777e-06 NRD=0.14876 NRS=0.187943 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.12e-06 sb=1.91074e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=3500 $Y=7800 $D=25
M5 VSS! 6 12 VSS! nfet L=1.2e-07 W=1.21e-06 AD=2.178e-13 AS=2.178e-13 PD=1.57e-06 PS=1.57e-06 NRD=0.14876 NRS=0.14876 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.49421e-06 sb=1.73223e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=3500 $Y=8280 $D=25
M6 10 7 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 AD=2.178e-13 AS=2.178e-13 PD=1.57e-06 PS=1.57e-06 NRD=0.14876 NRS=0.14876 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.73223e-06 sb=1.49421e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=3500 $Y=8760 $D=25
M7 VSS! 7 10 VSS! nfet L=1.2e-07 W=1.21e-06 AD=2.75168e-13 AS=2.178e-13 PD=1.97777e-06 PS=1.57e-06 NRD=0.187943 NRS=0.14876 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.91074e-06 sb=1.12e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=3500 $Y=9240 $D=25
M8 8 80 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 AD=1.368e-13 AS=1.80303e-13 PD=1.12e-06 PS=1.52e-06 NRD=0.236842 NRS=0.31216 m=1 par=1 nf=1 ngcon=1 psp=0 sa=5.93684e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-15 panw8=2.4e-14 panw9=4.8e-14 panw10=2.28e-14 $X=3950 $Y=1240 $D=25
M9 VSS! 80 8 VSS! nfet L=1.2e-07 W=7.6e-07 AD=1.72832e-13 AS=1.368e-13 PD=1.24223e-06 PS=1.12e-06 NRD=0.299225 NRS=0.236842 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.07368e-06 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-15 panw8=2.4e-14 panw9=4.8e-14 panw10=2.28e-14 $X=3950 $Y=1720 $D=25
M10 9 81 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 AD=1.368e-13 AS=1.72832e-13 PD=1.12e-06 PS=1.24223e-06 NRD=0.236842 NRS=0.299225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.07368e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-15 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=3950 $Y=4200 $D=25
M11 VSS! 81 9 VSS! nfet L=1.2e-07 W=7.6e-07 AD=1.80303e-13 AS=1.368e-13 PD=1.52e-06 PS=1.12e-06 NRD=0.31216 NRS=0.236842 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=5.93684e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-15 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=3950 $Y=4680 $D=25
M12 6 82 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 AD=1.368e-13 AS=1.80303e-13 PD=1.12e-06 PS=1.52e-06 NRD=0.236842 NRS=0.31216 m=1 par=1 nf=1 ngcon=1 psp=0 sa=5.93684e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-15 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=3950 $Y=6800 $D=25
M13 VSS! 82 6 VSS! nfet L=1.2e-07 W=7.6e-07 AD=1.72832e-13 AS=1.368e-13 PD=1.24223e-06 PS=1.12e-06 NRD=0.299225 NRS=0.236842 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.07368e-06 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-15 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=3950 $Y=7280 $D=25
M14 7 83 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 AD=1.368e-13 AS=1.72832e-13 PD=1.12e-06 PS=1.24223e-06 NRD=0.236842 NRS=0.299225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.07368e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-15 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=3950 $Y=9760 $D=25
M15 VSS! 83 7 VSS! nfet L=1.2e-07 W=7.6e-07 AD=1.80303e-13 AS=1.368e-13 PD=1.52e-06 PS=1.12e-06 NRD=0.31216 NRS=0.236842 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=5.93684e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-15 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=3950 $Y=10240 $D=25
M16 VSS! SEL<3> 80 VSS! nfet L=1.2e-07 W=4e-07 AD=9.48966e-14 AS=1.28e-13 PD=8e-07 PS=1.44e-06 NRD=0.593103 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=3.12e-14 panw10=2.28e-14 $X=4310 $Y=720 $D=25
M17 81 SEL<2> VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=9.48966e-14 PD=1.44e-06 PS=8e-07 NRD=0.8 NRS=0.593103 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=3.12e-14 panw10=1.68e-14 $X=4310 $Y=5200 $D=25
M18 VSS! SEL<1> 82 VSS! nfet L=1.2e-07 W=4e-07 AD=9.48966e-14 AS=1.28e-13 PD=8e-07 PS=1.44e-06 NRD=0.593103 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=3.12e-14 panw10=1.68e-14 $X=4310 $Y=6280 $D=25
M19 83 SEL<0> VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=9.48966e-14 PD=1.44e-06 PS=8e-07 NRD=0.8 NRS=0.593103 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=3.12e-14 panw10=1.68e-14 $X=4310 $Y=10760 $D=25
M20 VSS! 1373 1375 VSS! nfet L=1.2e-07 W=7.3e-07 AD=2.336e-13 AS=2.336e-13 PD=2.1e-06 PS=2.1e-06 NRD=0.438356 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=5630 $Y=2360 $D=25
M21 VSS! 1375 1374 VSS! nfet L=1.2e-07 W=5e-07 AD=1.6e-13 AS=9e-14 PD=1.64e-06 PS=8.6e-07 NRD=0.64 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=5630 $Y=1420 $D=25
M22 1374 1375 VSS! VSS! nfet L=1.2e-07 W=5e-07 AD=9e-14 AS=9.66667e-14 PD=8.6e-07 PS=8.83333e-07 NRD=0.36 NRS=0.386667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=5630 $Y=940 $D=25
M23 VSS! 1374 E<0> VSS! nfet L=1.2e-07 W=7e-07 AD=1.35333e-13 AS=2.24e-13 PD=1.23667e-06 PS=2.04e-06 NRD=0.27619 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=9.88571e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=5630 $Y=460 $D=25
M24 VDD! 8 1373 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=5630 $Y=3360 $D=25
M25 E<1> 1377 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.26e-13 PD=2.04e-06 PS=1.06e-06 NRD=0.457143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=16070 $Y=2360 $D=25
M26 VSS! 1377 E<1> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=16070 $Y=1880 $D=25
M27 E<1> 1377 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=16070 $Y=1400 $D=25
M28 VSS! 1377 E<1> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=16070 $Y=920 $D=25
M29 E<1> 1377 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=2.24e-13 PD=1.06e-06 PS=2.04e-06 NRD=0.257143 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=16070 $Y=440 $D=25
M30 17 11 1376 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=16430 $Y=3360 $D=25
M31 VSS! 7 15 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=5650 $Y=10920 $D=25
M32 A<1> 10 19 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=16470 $Y=10920 $D=25
M33 VSS! 1378 1380 VSS! nfet L=1.2e-07 W=7.3e-07 AD=2.336e-13 AS=2.336e-13 PD=2.1e-06 PS=2.1e-06 NRD=0.438356 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=17630 $Y=2360 $D=25
M34 VSS! 1380 1379 VSS! nfet L=1.2e-07 W=5e-07 AD=1.6e-13 AS=9e-14 PD=1.64e-06 PS=8.6e-07 NRD=0.64 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=17630 $Y=1420 $D=25
M35 1379 1380 VSS! VSS! nfet L=1.2e-07 W=5e-07 AD=9e-14 AS=9.66667e-14 PD=8.6e-07 PS=8.83333e-07 NRD=0.36 NRS=0.386667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=17630 $Y=940 $D=25
M36 VSS! 1379 E<2> VSS! nfet L=1.2e-07 W=7e-07 AD=1.35333e-13 AS=2.24e-13 PD=1.23667e-06 PS=2.04e-06 NRD=0.27619 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=9.88571e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=17630 $Y=460 $D=25
M37 VDD! 8 1378 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=17630 $Y=3360 $D=25
M38 E<3> 1382 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.26e-13 PD=2.04e-06 PS=1.06e-06 NRD=0.457143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=28070 $Y=2360 $D=25
M39 VSS! 1382 E<3> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=28070 $Y=1880 $D=25
M40 E<3> 1382 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=28070 $Y=1400 $D=25
M41 VSS! 1382 E<3> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=28070 $Y=920 $D=25
M42 E<3> 1382 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=2.24e-13 PD=1.06e-06 PS=2.04e-06 NRD=0.257143 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=28070 $Y=440 $D=25
M43 25 11 1381 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=28430 $Y=3360 $D=25
M44 A<1> 7 23 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=17650 $Y=10920 $D=25
M45 A<3> 10 27 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=28470 $Y=10920 $D=25
M46 VSS! 1383 1385 VSS! nfet L=1.2e-07 W=7.3e-07 AD=2.336e-13 AS=2.336e-13 PD=2.1e-06 PS=2.1e-06 NRD=0.438356 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=29630 $Y=2360 $D=25
M47 VSS! 1385 1384 VSS! nfet L=1.2e-07 W=5e-07 AD=1.6e-13 AS=9e-14 PD=1.64e-06 PS=8.6e-07 NRD=0.64 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=29630 $Y=1420 $D=25
M48 1384 1385 VSS! VSS! nfet L=1.2e-07 W=5e-07 AD=9e-14 AS=9.66667e-14 PD=8.6e-07 PS=8.83333e-07 NRD=0.36 NRS=0.386667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=29630 $Y=940 $D=25
M49 VSS! 1384 E<4> VSS! nfet L=1.2e-07 W=7e-07 AD=1.35333e-13 AS=2.24e-13 PD=1.23667e-06 PS=2.04e-06 NRD=0.27619 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=9.88571e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=29630 $Y=460 $D=25
M50 VDD! 8 1383 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=29630 $Y=3360 $D=25
M51 E<5> 1387 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.26e-13 PD=2.04e-06 PS=1.06e-06 NRD=0.457143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=40070 $Y=2360 $D=25
M52 VSS! 1387 E<5> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=40070 $Y=1880 $D=25
M53 E<5> 1387 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=40070 $Y=1400 $D=25
M54 VSS! 1387 E<5> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=40070 $Y=920 $D=25
M55 E<5> 1387 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=2.24e-13 PD=1.06e-06 PS=2.04e-06 NRD=0.257143 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=40070 $Y=440 $D=25
M56 33 11 1386 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=40430 $Y=3360 $D=25
M57 A<3> 7 31 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=29650 $Y=10920 $D=25
M58 A<5> 10 35 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=40470 $Y=10920 $D=25
M59 VSS! 1388 1390 VSS! nfet L=1.2e-07 W=7.3e-07 AD=2.336e-13 AS=2.336e-13 PD=2.1e-06 PS=2.1e-06 NRD=0.438356 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=41630 $Y=2360 $D=25
M60 VSS! 1390 1389 VSS! nfet L=1.2e-07 W=5e-07 AD=1.6e-13 AS=9e-14 PD=1.64e-06 PS=8.6e-07 NRD=0.64 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=41630 $Y=1420 $D=25
M61 1389 1390 VSS! VSS! nfet L=1.2e-07 W=5e-07 AD=9e-14 AS=9.66667e-14 PD=8.6e-07 PS=8.83333e-07 NRD=0.36 NRS=0.386667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=41630 $Y=940 $D=25
M62 VSS! 1389 E<6> VSS! nfet L=1.2e-07 W=7e-07 AD=1.35333e-13 AS=2.24e-13 PD=1.23667e-06 PS=2.04e-06 NRD=0.27619 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=9.88571e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=41630 $Y=460 $D=25
M63 VDD! 8 1388 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=41630 $Y=3360 $D=25
M64 E<7> 1392 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.26e-13 PD=2.04e-06 PS=1.06e-06 NRD=0.457143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=52070 $Y=2360 $D=25
M65 VSS! 1392 E<7> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=52070 $Y=1880 $D=25
M66 E<7> 1392 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=52070 $Y=1400 $D=25
M67 VSS! 1392 E<7> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=52070 $Y=920 $D=25
M68 E<7> 1392 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=2.24e-13 PD=1.06e-06 PS=2.04e-06 NRD=0.257143 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=52070 $Y=440 $D=25
M69 41 11 1391 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=52430 $Y=3360 $D=25
M70 A<5> 7 39 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=41650 $Y=10920 $D=25
M71 A<7> 10 43 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=52470 $Y=10920 $D=25
M72 VSS! 1393 1395 VSS! nfet L=1.2e-07 W=7.3e-07 AD=2.336e-13 AS=2.336e-13 PD=2.1e-06 PS=2.1e-06 NRD=0.438356 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=53630 $Y=2360 $D=25
M73 VSS! 1395 1394 VSS! nfet L=1.2e-07 W=5e-07 AD=1.6e-13 AS=9e-14 PD=1.64e-06 PS=8.6e-07 NRD=0.64 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=53630 $Y=1420 $D=25
M74 1394 1395 VSS! VSS! nfet L=1.2e-07 W=5e-07 AD=9e-14 AS=9.66667e-14 PD=8.6e-07 PS=8.83333e-07 NRD=0.36 NRS=0.386667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=53630 $Y=940 $D=25
M75 VSS! 1394 E<8> VSS! nfet L=1.2e-07 W=7e-07 AD=1.35333e-13 AS=2.24e-13 PD=1.23667e-06 PS=2.04e-06 NRD=0.27619 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=9.88571e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=53630 $Y=460 $D=25
M76 13 8 1393 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=53630 $Y=3360 $D=25
M77 E<9> 1397 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.26e-13 PD=2.04e-06 PS=1.06e-06 NRD=0.457143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=64070 $Y=2360 $D=25
M78 VSS! 1397 E<9> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=64070 $Y=1880 $D=25
M79 E<9> 1397 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=64070 $Y=1400 $D=25
M80 VSS! 1397 E<9> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=64070 $Y=920 $D=25
M81 E<9> 1397 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=2.24e-13 PD=1.06e-06 PS=2.04e-06 NRD=0.257143 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=64070 $Y=440 $D=25
M82 86 11 1396 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=64430 $Y=3360 $D=25
M83 A<7> 7 46 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=53650 $Y=10920 $D=25
M84 A<9> 10 49 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=64470 $Y=10920 $D=25
M85 VSS! 1398 1400 VSS! nfet L=1.2e-07 W=7.3e-07 AD=2.336e-13 AS=2.336e-13 PD=2.1e-06 PS=2.1e-06 NRD=0.438356 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=65630 $Y=2360 $D=25
M86 VSS! 1400 1399 VSS! nfet L=1.2e-07 W=5e-07 AD=1.6e-13 AS=9e-14 PD=1.64e-06 PS=8.6e-07 NRD=0.64 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=65630 $Y=1420 $D=25
M87 1399 1400 VSS! VSS! nfet L=1.2e-07 W=5e-07 AD=9e-14 AS=9.66667e-14 PD=8.6e-07 PS=8.83333e-07 NRD=0.36 NRS=0.386667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=65630 $Y=940 $D=25
M88 VSS! 1399 E<10> VSS! nfet L=1.2e-07 W=7e-07 AD=1.35333e-13 AS=2.24e-13 PD=1.23667e-06 PS=2.04e-06 NRD=0.27619 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=9.88571e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=65630 $Y=460 $D=25
M89 21 8 1398 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=65630 $Y=3360 $D=25
M90 E<11> 1402 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.26e-13 PD=2.04e-06 PS=1.06e-06 NRD=0.457143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=76070 $Y=2360 $D=25
M91 VSS! 1402 E<11> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=76070 $Y=1880 $D=25
M92 E<11> 1402 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=76070 $Y=1400 $D=25
M93 VSS! 1402 E<11> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=76070 $Y=920 $D=25
M94 E<11> 1402 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=2.24e-13 PD=1.06e-06 PS=2.04e-06 NRD=0.257143 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=76070 $Y=440 $D=25
M95 88 11 1401 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=76430 $Y=3360 $D=25
M96 A<9> 7 52 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=65650 $Y=10920 $D=25
M97 A<11> 10 55 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=76470 $Y=10920 $D=25
M98 VSS! 1403 1405 VSS! nfet L=1.2e-07 W=7.3e-07 AD=2.336e-13 AS=2.336e-13 PD=2.1e-06 PS=2.1e-06 NRD=0.438356 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=77630 $Y=2360 $D=25
M99 VSS! 1405 1404 VSS! nfet L=1.2e-07 W=5e-07 AD=1.6e-13 AS=9e-14 PD=1.64e-06 PS=8.6e-07 NRD=0.64 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=77630 $Y=1420 $D=25
M100 1404 1405 VSS! VSS! nfet L=1.2e-07 W=5e-07 AD=9e-14 AS=9.66667e-14 PD=8.6e-07 PS=8.83333e-07 NRD=0.36 NRS=0.386667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=77630 $Y=940 $D=25
M101 VSS! 1404 E<12> VSS! nfet L=1.2e-07 W=7e-07 AD=1.35333e-13 AS=2.24e-13 PD=1.23667e-06 PS=2.04e-06 NRD=0.27619 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=9.88571e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=77630 $Y=460 $D=25
M102 29 8 1403 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=77630 $Y=3360 $D=25
M103 E<13> 1407 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.26e-13 PD=2.04e-06 PS=1.06e-06 NRD=0.457143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=88070 $Y=2360 $D=25
M104 VSS! 1407 E<13> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=88070 $Y=1880 $D=25
M105 E<13> 1407 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=88070 $Y=1400 $D=25
M106 VSS! 1407 E<13> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=88070 $Y=920 $D=25
M107 E<13> 1407 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=2.24e-13 PD=1.06e-06 PS=2.04e-06 NRD=0.257143 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=88070 $Y=440 $D=25
M108 91 11 1406 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=88430 $Y=3360 $D=25
M109 A<11> 7 57 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=77650 $Y=10920 $D=25
M110 A<13> 10 59 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=88470 $Y=10920 $D=25
M111 VSS! 1408 1410 VSS! nfet L=1.2e-07 W=7.3e-07 AD=2.336e-13 AS=2.336e-13 PD=2.1e-06 PS=2.1e-06 NRD=0.438356 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=89630 $Y=2360 $D=25
M112 VSS! 1410 1409 VSS! nfet L=1.2e-07 W=5e-07 AD=1.6e-13 AS=9e-14 PD=1.64e-06 PS=8.6e-07 NRD=0.64 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=89630 $Y=1420 $D=25
M113 1409 1410 VSS! VSS! nfet L=1.2e-07 W=5e-07 AD=9e-14 AS=9.66667e-14 PD=8.6e-07 PS=8.83333e-07 NRD=0.36 NRS=0.386667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=89630 $Y=940 $D=25
M114 VSS! 1409 E<14> VSS! nfet L=1.2e-07 W=7e-07 AD=1.35333e-13 AS=2.24e-13 PD=1.23667e-06 PS=2.04e-06 NRD=0.27619 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=9.88571e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=89630 $Y=460 $D=25
M115 37 8 1408 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=1.32e-14 $X=89630 $Y=3360 $D=25
M116 E<15> 1412 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=2.24e-13 AS=1.26e-13 PD=2.04e-06 PS=1.06e-06 NRD=0.457143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=100070 $Y=2360 $D=25
M117 VSS! 1412 E<15> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=100070 $Y=1880 $D=25
M118 E<15> 1412 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=100070 $Y=1400 $D=25
M119 VSS! 1412 E<15> VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=1.26e-13 PD=1.06e-06 PS=1.06e-06 NRD=0.257143 NRS=0.257143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=100070 $Y=920 $D=25
M120 E<15> 1412 VSS! VSS! nfet L=1.2e-07 W=7e-07 AD=1.26e-13 AS=2.24e-13 PD=1.06e-06 PS=2.04e-06 NRD=0.257143 NRS=0.457143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=100070 $Y=440 $D=25
M121 96 11 1411 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=1.088e-13 PD=1.32e-06 PS=1.32e-06 NRD=0.941176 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=1.32e-14 panw8=2.4e-14 panw9=3.6e-15 panw10=0 $X=100430 $Y=3360 $D=25
M122 A<13> 7 95 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=89650 $Y=10920 $D=25
M123 A<15> 10 98 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.64e-14 panw10=7.2e-15 $X=100470 $Y=10920 $D=25
M124 VDD! SEL<3> 80 VDD! pfet L=1.2e-07 W=7.6e-07 AD=1.84461e-13 AS=2.432e-13 PD=1.28814e-06 PS=2.16e-06 NRD=0.319358 NRS=0.421053 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=9.48e-14 panw8=2.4e-14 panw9=4.8e-14 panw10=3e-14 $X=530 $Y=720 $D=108
M125 8 80 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 AD=2.88e-13 AS=3.88339e-13 PD=1.96e-06 PS=2.71186e-06 NRD=0.1125 NRS=0.151695 m=1 par=1 nf=1 ngcon=1 psp=0 sa=5.67e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=2.4e-14 panw9=2.832e-13 panw10=1.44e-13 $X=530 $Y=1240 $D=108
M126 VDD! 80 8 VDD! pfet L=1.2e-07 W=1.6e-06 AD=3.56061e-13 AS=2.88e-13 PD=2.22538e-06 PS=1.96e-06 NRD=0.139086 NRS=0.1125 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.047e-06 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=2.4e-14 panw9=9.12e-14 panw10=3.36e-13 $X=530 $Y=1720 $D=108
M127 11 8 VDD! VDD! pfet L=1.2e-07 W=2.34e-06 AD=4.212e-13 AS=5.20739e-13 PD=2.7e-06 PS=3.25462e-06 NRD=0.0769231 NRS=0.0951017 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.17265e-06 sb=1.9241e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.76e-14 panw8=4.8e-14 panw9=9.6e-14 panw10=1.44e-13 $X=530 $Y=2240 $D=108
M128 VDD! 8 11 VDD! pfet L=1.2e-07 W=2.34e-06 AD=4.212e-13 AS=4.212e-13 PD=2.7e-06 PS=2.7e-06 NRD=0.0769231 NRS=0.0769231 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.54872e-06 sb=1.77231e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.76e-14 panw8=4.8e-14 panw9=9.6e-14 panw10=1.44e-13 $X=530 $Y=2720 $D=108
M129 5 9 VDD! VDD! pfet L=1.2e-07 W=2.34e-06 AD=4.212e-13 AS=4.212e-13 PD=2.7e-06 PS=2.7e-06 NRD=0.0769231 NRS=0.0769231 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.77231e-06 sb=1.54872e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.76e-14 panw8=4.8e-14 panw9=9.6e-14 panw10=1.44e-13 $X=530 $Y=3200 $D=108
M130 VDD! 9 5 VDD! pfet L=1.2e-07 W=2.34e-06 AD=5.20739e-13 AS=4.212e-13 PD=3.25462e-06 PS=2.7e-06 NRD=0.0951017 NRS=0.0769231 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.9241e-06 sb=1.17265e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.76e-14 panw8=4.8e-14 panw9=9.6e-14 panw10=1.44e-13 $X=530 $Y=3680 $D=108
M131 9 81 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 AD=2.88e-13 AS=3.56061e-13 PD=1.96e-06 PS=2.22538e-06 NRD=0.1125 NRS=0.139086 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.047e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=2.4e-14 panw9=9.12e-14 panw10=1.44e-13 $X=530 $Y=4200 $D=108
M132 VDD! 81 9 VDD! pfet L=1.2e-07 W=1.6e-06 AD=3.88339e-13 AS=2.88e-13 PD=2.71186e-06 PS=1.96e-06 NRD=0.151695 NRS=0.1125 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=5.67e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=2.4e-14 panw9=9.12e-14 panw10=1.44e-13 $X=530 $Y=4680 $D=108
M133 81 SEL<2> VDD! VDD! pfet L=1.2e-07 W=7.6e-07 AD=2.432e-13 AS=1.84461e-13 PD=2.16e-06 PS=1.28814e-06 NRD=0.421053 NRS=0.319358 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=2.4e-14 panw9=4.8e-14 panw10=3e-14 $X=530 $Y=5200 $D=108
M134 VDD! SEL<1> 82 VDD! pfet L=1.2e-07 W=7.6e-07 AD=1.84461e-13 AS=2.432e-13 PD=1.28814e-06 PS=2.16e-06 NRD=0.319358 NRS=0.421053 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=2.4e-14 panw9=4.8e-14 panw10=3e-14 $X=530 $Y=6280 $D=108
M135 6 82 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 AD=2.88e-13 AS=3.88339e-13 PD=1.96e-06 PS=2.71186e-06 NRD=0.1125 NRS=0.151695 m=1 par=1 nf=1 ngcon=1 psp=0 sa=5.67e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=2.4e-14 panw9=9.12e-14 panw10=1.44e-13 $X=530 $Y=6800 $D=108
M136 VDD! 82 6 VDD! pfet L=1.2e-07 W=1.6e-06 AD=3.56061e-13 AS=2.88e-13 PD=2.22538e-06 PS=1.96e-06 NRD=0.139086 NRS=0.1125 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.047e-06 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=2.4e-14 panw9=9.12e-14 panw10=1.44e-13 $X=530 $Y=7280 $D=108
M137 12 6 VDD! VDD! pfet L=1.2e-07 W=2.34e-06 AD=4.212e-13 AS=5.20739e-13 PD=2.7e-06 PS=3.25462e-06 NRD=0.0769231 NRS=0.0951017 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.17265e-06 sb=1.9241e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.76e-14 panw8=4.8e-14 panw9=9.6e-14 panw10=1.44e-13 $X=530 $Y=7800 $D=108
M138 VDD! 6 12 VDD! pfet L=1.2e-07 W=2.34e-06 AD=4.212e-13 AS=4.212e-13 PD=2.7e-06 PS=2.7e-06 NRD=0.0769231 NRS=0.0769231 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.54872e-06 sb=1.77231e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.76e-14 panw8=4.8e-14 panw9=9.6e-14 panw10=1.44e-13 $X=530 $Y=8280 $D=108
M139 10 7 VDD! VDD! pfet L=1.2e-07 W=2.34e-06 AD=4.212e-13 AS=4.212e-13 PD=2.7e-06 PS=2.7e-06 NRD=0.0769231 NRS=0.0769231 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.77231e-06 sb=1.54872e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.76e-14 panw8=4.8e-14 panw9=9.6e-14 panw10=1.44e-13 $X=530 $Y=8760 $D=108
M140 VDD! 7 10 VDD! pfet L=1.2e-07 W=2.34e-06 AD=5.20739e-13 AS=4.212e-13 PD=3.25462e-06 PS=2.7e-06 NRD=0.0951017 NRS=0.0769231 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.9241e-06 sb=1.17265e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.76e-14 panw8=4.8e-14 panw9=9.6e-14 panw10=1.44e-13 $X=530 $Y=9240 $D=108
M141 7 83 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 AD=2.88e-13 AS=3.56061e-13 PD=1.96e-06 PS=2.22538e-06 NRD=0.1125 NRS=0.139086 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.047e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=2.4e-14 panw9=9.12e-14 panw10=3.36e-13 $X=530 $Y=9760 $D=108
M142 VDD! 83 7 VDD! pfet L=1.2e-07 W=1.6e-06 AD=3.88339e-13 AS=2.88e-13 PD=2.71186e-06 PS=1.96e-06 NRD=0.151695 NRS=0.1125 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=5.67e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=2.4e-14 panw9=1.872e-13 panw10=2.4e-13 $X=530 $Y=10240 $D=108
M143 83 SEL<0> VDD! VDD! pfet L=1.2e-07 W=7.6e-07 AD=2.432e-13 AS=1.84461e-13 PD=2.16e-06 PS=1.28814e-06 NRD=0.421053 NRS=0.319358 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=1.152e-13 panw9=4.8e-14 panw10=3e-14 $X=530 $Y=10760 $D=108
D144 VSS! VDD! diodenx AREA=8.4e-14 perim=1.16e-06 $X=-140 $Y=11700 $D=480
D145 VSS! VDD! diodenwx AREA=3.9556e-11 perim=3.002e-05 t3well=0 $X=-240 $Y=100 $D=474
D146 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=6660 $Y=-200 $D=474
D147 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=12660 $Y=-200 $D=474
D148 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=18660 $Y=-200 $D=474
D149 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=24660 $Y=-200 $D=474
D150 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=30660 $Y=-200 $D=474
D151 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=36660 $Y=-200 $D=474
D152 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=42660 $Y=-200 $D=474
D153 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=48660 $Y=-200 $D=474
D154 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=54660 $Y=-200 $D=474
D155 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=60660 $Y=-200 $D=474
D156 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=66660 $Y=-200 $D=474
D157 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=72660 $Y=-200 $D=474
D158 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=78660 $Y=-200 $D=474
D159 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=84660 $Y=-200 $D=474
D160 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=90660 $Y=-200 $D=474
D161 VSS! VDD! diodenwx AREA=3.49554e-11 perim=3.096e-05 t3well=0 $X=96660 $Y=-200 $D=474
X162 9 5 6 12 8 11 7 10 E<0> E<1> VSS! VSS! VDD! 13 14 VSS! 17 18 VSS! 15
+ VSS! 19 VDD! VDD! VSS! A<0> A<1> 1373 1374 1375 1376 1377
+ ICV_2 $T=5200 3740 1 0 $X=4950 $Y=-200
X163 9 5 6 12 8 11 7 10 E<2> E<3> VSS! VSS! VDD! 21 22 VSS! 25 26 15 23
+ 19 27 VDD! VDD! A<1> A<2> A<3> 1378 1379 1380 1381 1382
+ ICV_2 $T=17200 3740 1 0 $X=16950 $Y=-200
X164 9 5 6 12 8 11 7 10 E<4> E<5> VSS! 14 VDD! 29 30 18 33 34 23 31
+ 27 35 VDD! VDD! A<3> A<4> A<5> 1383 1384 1385 1386 1387
+ ICV_2 $T=29200 3740 1 0 $X=28950 $Y=-200
X165 9 5 6 12 8 11 7 10 E<6> E<7> VSS! 22 VDD! 37 38 26 41 42 31 39
+ 35 43 VDD! VDD! A<5> A<6> A<7> 1388 1389 1390 1391 1392
+ ICV_2 $T=41200 3740 1 0 $X=40950 $Y=-200
X166 9 5 6 12 8 11 7 10 E<8> E<9> VSS! 30 VDD! 85 45 34 86 48 39 46
+ 43 49 13 17 A<7> A<8> A<9> 1393 1394 1395 1396 1397
+ ICV_2 $T=53200 3740 1 0 $X=52950 $Y=-200
X167 9 5 6 12 8 11 7 10 E<10> E<11> VSS! 38 VDD! 87 51 42 88 54 46 52
+ 49 55 21 25 A<9> A<10> A<11> 1398 1399 1400 1401 1402
+ ICV_2 $T=65200 3740 1 0 $X=64950 $Y=-200
X168 9 5 6 12 8 11 7 10 E<12> E<13> VSS! 45 VDD! 89 90 48 91 92 52 57
+ 55 59 29 33 A<11> A<12> A<13> 1403 1404 1405 1406 1407
+ ICV_2 $T=77200 3740 1 0 $X=76950 $Y=-200
X169 9 5 6 12 8 11 7 10 E<14> E<15> VSS! 51 VDD! 93 94 54 96 97 57 95
+ 59 98 37 41 A<13> A<14> A<15> 1408 1409 1410 1411 1412
+ ICV_2 $T=89200 3740 1 0 $X=88950 $Y=-200
.ENDS
***************************************
