* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT REN_Driver_1bit RENB REN IN VSS! VDD!
** N=68 EP=5 IP=0 FDC=18
M0 VSS! 7 6 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=450 $D=97
M1 VSS! 7 6 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=4750 $D=97
M2 RENB 6 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=450 $D=97
M3 7 IN VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=4750 $D=97
M4 VSS! 6 RENB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=450 $D=97
M5 RENB 6 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M6 VSS! IN 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=4750 $D=97
M7 VSS! 6 RENB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M8 REN 8 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=4750 $D=97
M9 VDD! 7 6 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=1490 $D=189
M10 VDD! 7 6 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=3310 $D=189
M11 RENB 6 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=1490 $D=189
M12 7 IN VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3310 $D=189
M13 VDD! 6 RENB VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=1490 $D=189
M14 RENB 6 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1490 $D=189
M15 VDD! IN 8 VDD! pfet L=1.2e-07 W=6.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3310 $D=189
M16 VDD! 6 RENB VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1490 $D=189
M17 REN 8 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7
** N=7 EP=7 IP=10 FDC=36
X0 1 4 3 6 7 REN_Driver_1bit $T=0 0 0 0 $X=0 $Y=-240
X1 2 5 3 6 7 REN_Driver_1bit $T=3200 0 0 0 $X=3200 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11
** N=11 EP=11 IP=14 FDC=72
X0 1 2 5 6 7 10 11 ICV_1 $T=0 0 0 0 $X=0 $Y=-240
X1 3 4 5 8 9 10 11 ICV_1 $T=6400 0 0 0 $X=6400 $Y=-240
.ENDS
***************************************
.SUBCKT REN_Driver RENB<0> RENB<1> RENB<2> RENB<3> RENB<4> RENB<5> RENB<6> RENB<7> RENB<8> RENB<9> RENB<10> RENB<11> RENB<12> RENB<13> RENB<14> RENB<15> REN_SRAM REN<0> REN<1> REN<2>
+ REN<3> REN<4> REN<5> REN<6> REN<7> REN<8> REN<9> REN<10> REN<11> REN<12> REN<13> REN<14> REN<15> VSS! VDD!
** N=36 EP=35 IP=44 FDC=288
X0 RENB<0> RENB<1> RENB<2> RENB<3> REN_SRAM REN<0> REN<1> REN<2> REN<3> VSS! VDD! ICV_2 $T=0 0 0 0 $X=0 $Y=-240
X1 RENB<4> RENB<5> RENB<6> RENB<7> REN_SRAM REN<4> REN<5> REN<6> REN<7> VSS! VDD! ICV_2 $T=12800 0 0 0 $X=12800 $Y=-240
X2 RENB<8> RENB<9> RENB<10> RENB<11> REN_SRAM REN<8> REN<9> REN<10> REN<11> VSS! VDD! ICV_2 $T=25600 0 0 0 $X=25600 $Y=-240
X3 RENB<12> RENB<13> RENB<14> RENB<15> REN_SRAM REN<12> REN<13> REN<14> REN<15> VSS! VDD! ICV_2 $T=38400 0 0 0 $X=38400 $Y=-240
.ENDS
***************************************
