* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT RWL_Driver_1bit RWLN RWLBP RWLP RWLBN RWL_N RWLB_N RWLB_P RWL_P VSS! VDD!
** N=155 EP=10 IP=0 FDC=48
M0 RWLN 5 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 5 RWL_N VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=4730 $D=97
M2 VSS! 5 RWLN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=450 $D=97
M3 VSS! RWL_N 5 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=4730 $D=97
M4 RWLN 5 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=450 $D=97
M5 RWLBN 12 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=4730 $D=97
M6 VSS! 5 RWLN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=450 $D=97
M7 VSS! 12 RWLBN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=4730 $D=97
M8 12 RWLB_N VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=450 $D=97
M9 RWLBN 12 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=4730 $D=97
M10 VSS! RWLB_N 12 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=450 $D=97
M11 VSS! 12 RWLBN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=4730 $D=97
M12 RWLBP 9 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=450 $D=97
M13 9 RWLB_P VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=4730 $D=97
M14 VSS! 9 RWLBP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=450 $D=97
M15 VSS! RWLB_P 9 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=4730 $D=97
M16 RWLBP 9 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=450 $D=97
M17 RWLP 10 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=4730 $D=97
M18 VSS! 9 RWLBP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=450 $D=97
M19 VSS! 10 RWLP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=4730 $D=97
M20 10 RWL_P VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=450 $D=97
M21 RWLP 10 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=4730 $D=97
M22 VSS! RWL_P 10 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=450 $D=97
M23 VSS! 10 RWLP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=4730 $D=97
M24 RWLN 5 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=1490 $D=189
M25 5 RWL_N VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M26 VDD! 5 RWLN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=1490 $D=189
M27 VDD! RWL_N 5 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=3310 $D=189
M28 RWLN 5 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=1490 $D=189
M29 RWLBN 12 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=3310 $D=189
M30 VDD! 5 RWLN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=1490 $D=189
M31 VDD! 12 RWLBN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=3310 $D=189
M32 12 RWLB_N VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=1480 $D=189
M33 RWLBN 12 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=3310 $D=189
M34 VDD! RWLB_N 12 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=1480 $D=189
M35 VDD! 12 RWLBN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=3310 $D=189
M36 RWLBP 9 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=1490 $D=189
M37 9 RWLB_P VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=3310 $D=189
M38 VDD! 9 RWLBP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=1490 $D=189
M39 VDD! RWLB_P 9 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=3310 $D=189
M40 RWLBP 9 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=1490 $D=189
M41 RWLP 10 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=3310 $D=189
M42 VDD! 9 RWLBP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=1490 $D=189
M43 VDD! 10 RWLP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=3310 $D=189
M44 10 RWL_P VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=1480 $D=189
M45 RWLP 10 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=3310 $D=189
M46 VDD! RWL_P 10 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=1480 $D=189
M47 VDD! 10 RWLP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=3310 $D=189
.ENDS
***************************************
