* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT decoder_1st 1 2 3 4 5 6 7
** N=40 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4870 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT decoder_2st 1 2 3 4 5 6 7
** N=37 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4590 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=1730 $D=189
.ENDS
***************************************
.SUBCKT decoder_3rd 1 2 3 4 5 6 7
** N=67 EP=7 IP=0 FDC=8
M0 8 1 4 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=450 $D=97
M1 8 2 5 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=4870 $D=97
M2 3 8 6 6 nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=450 $D=97
M3 3 8 6 6 nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=4550 $D=97
M4 8 1 5 7 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=3310 $D=189
M5 8 2 4 7 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=2010 $D=189
M6 3 8 7 7 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=1690 $D=189
M7 3 8 7 7 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT decoder_4th E SH3_BAR SH<3> IN IN1 VSS! VDD!
** N=109 EP=7 IP=0 FDC=26
M0 10 SH3_BAR IN VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=450 $D=97
M1 10 SH<3> IN1 VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=4810 $D=97
M2 11 10 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=450 $D=97
M3 11 10 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=4780 $D=97
M4 9 11 VSS! VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=450 $D=97
M5 E 9 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=4550 $D=97
M6 VSS! 11 9 VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=450 $D=97
M7 VSS! 9 E VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=4550 $D=97
M8 E 9 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3780 $Y=4550 $D=97
M9 E 9 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3820 $Y=450 $D=97
M10 VSS! 9 E VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4260 $Y=4550 $D=97
M11 VSS! 9 E VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4300 $Y=450 $D=97
M12 E 9 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4740 $Y=4550 $D=97
M13 10 SH3_BAR IN1 VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=3310 $D=189
M14 10 SH<3> IN VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=1950 $D=189
M15 11 10 VDD! VDD! pfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=1920 $D=189
M16 11 10 VDD! VDD! pfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=3310 $D=189
M17 9 11 VDD! VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=1790 $D=189
M18 E 9 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=3310 $D=189
M19 VDD! 11 9 VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=1790 $D=189
M20 VDD! 9 E VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=3310 $D=189
M21 E 9 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3780 $Y=3310 $D=189
M22 E 9 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3820 $Y=1690 $D=189
M23 VDD! 9 E VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4260 $Y=3310 $D=189
M24 VDD! 9 E VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4300 $Y=1690 $D=189
M25 E 9 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4740 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9 10
** N=11 EP=10 IP=15 FDC=34
X0 2 3 4 8 9 10 7 decoder_3rd $T=-4800 0 0 0 $X=-5100 $Y=-240
X1 1 5 6 4 7 10 7 decoder_4th $T=0 0 0 0 $X=-300 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11 12 13 14
** N=15 EP=14 IP=22 FDC=68
X0 1 3 4 5 6 7 9 10 11 12 ICV_1 $T=0 0 0 0 $X=-5100 $Y=-240
X1 2 3 4 8 6 7 9 13 14 12 ICV_1 $T=0 5600 0 0 $X=-5100 $Y=5360
.ENDS
***************************************
.SUBCKT ICV_3 1 2 3 4 5 6 7 8
** N=9 EP=8 IP=16 FDC=52
X0 1 3 4 5 6 7 5 decoder_4th $T=0 0 0 0 $X=-300 $Y=-240
X1 2 3 4 5 8 7 5 decoder_4th $T=0 5600 0 0 $X=-300 $Y=5360
.ENDS
***************************************
.SUBCKT DECODER_WORST SEL<3> SEL<2> SEL<0> SEL<1> EN E<7> E<6> E<5> E<4> E<3> E<2> E<1> E<0> E<15> E<14> E<13> E<12> E<11> E<10> E<9>
+ E<8> VDD! VSS!
** N=304 EP=23 IP=140 FDC=586
M0 2 1 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6730 $Y=34050 $D=97
M1 VSS! 1 2 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=34050 $D=97
M2 VSS! 3 4 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=39650 $D=97
M3 2 1 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=34050 $D=97
M4 4 3 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=39650 $D=97
M5 VSS! 45 1 VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8090 $Y=38320 $D=97
M6 VSS! 46 3 VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8110 $Y=43980 $D=97
M7 VSS! 1 2 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=34050 $D=97
M8 VSS! 3 4 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=39650 $D=97
M9 1 45 VSS! VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8570 $Y=38320 $D=97
M10 3 46 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8590 $Y=43980 $D=97
M11 2 1 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=34050 $D=97
M12 4 3 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=39650 $D=97
M13 VSS! 45 1 VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9050 $Y=38320 $D=97
M14 VSS! 46 3 VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9070 $Y=43980 $D=97
M15 VSS! 1 2 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=34050 $D=97
M16 VSS! 3 4 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=39650 $D=97
M17 1 45 VSS! VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9540 $Y=38320 $D=97
M18 3 46 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9560 $Y=43980 $D=97
M19 2 1 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=34050 $D=97
M20 4 3 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=39650 $D=97
M21 VSS! 45 1 VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10020 $Y=38320 $D=97
M22 VSS! 46 3 VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10040 $Y=43980 $D=97
M23 VSS! 1 2 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=34050 $D=97
M24 VSS! 3 4 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=39650 $D=97
M25 2 1 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=34050 $D=97
M26 45 SEL<3> VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=38330 $D=97
M27 4 3 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=39650 $D=97
M28 46 SEL<2> VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=43990 $D=97
M29 VSS! 1 2 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=34050 $D=97
M30 VSS! SEL<3> 45 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=38330 $D=97
M31 VSS! 3 4 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=39650 $D=97
M32 VSS! SEL<2> 46 VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=43990 $D=97
M33 7 8 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12820 $Y=62050 $D=97
M34 VSS! 8 7 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13300 $Y=62050 $D=97
M35 8 SEL<1> VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13500 $Y=66400 $D=97
M36 7 8 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=62050 $D=97
M37 42 SEL<0> VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=77600 $D=97
M38 VSS! SEL<1> 8 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13980 $Y=66400 $D=97
M39 VSS! 8 7 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=62050 $D=97
M40 VSS! SEL<0> 42 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=77600 $D=97
M41 2 1 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6730 $Y=35110 $D=189
M42 VDD! 1 2 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=35110 $D=189
M43 VDD! 3 4 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=40790 $D=189
M44 2 1 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=35110 $D=189
M45 4 3 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=40790 $D=189
M46 VDD! 45 1 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8090 $Y=36910 $D=189
M47 VDD! 46 3 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8110 $Y=42510 $D=189
M48 VDD! 1 2 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=35110 $D=189
M49 VDD! 3 4 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=40790 $D=189
M50 1 45 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8570 $Y=36910 $D=189
M51 3 46 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8590 $Y=42510 $D=189
M52 2 1 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=35110 $D=189
M53 4 3 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=40790 $D=189
M54 VDD! 45 1 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9050 $Y=36910 $D=189
M55 VDD! 46 3 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9070 $Y=42510 $D=189
M56 VDD! 1 2 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=35110 $D=189
M57 VDD! 3 4 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=40790 $D=189
M58 1 45 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9540 $Y=36910 $D=189
M59 3 46 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9560 $Y=42510 $D=189
M60 2 1 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=35110 $D=189
M61 4 3 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=40790 $D=189
M62 VDD! 45 1 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10020 $Y=36910 $D=189
M63 VDD! 46 3 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10040 $Y=42510 $D=189
M64 VDD! 1 2 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=35110 $D=189
M65 VDD! 3 4 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=40790 $D=189
M66 2 1 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=35110 $D=189
M67 45 SEL<3> VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=36910 $D=189
M68 4 3 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=40790 $D=189
M69 46 SEL<2> VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=42510 $D=189
M70 VDD! 1 2 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=35110 $D=189
M71 VDD! SEL<3> 45 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=36910 $D=189
M72 VDD! 3 4 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=40790 $D=189
M73 VDD! SEL<2> 46 VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=42510 $D=189
M74 7 8 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12820 $Y=63090 $D=189
M75 VDD! 8 7 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13300 $Y=63090 $D=189
M76 8 SEL<1> VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13500 $Y=64910 $D=189
M77 7 8 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=63090 $D=189
M78 42 SEL<0> VDD! VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=76110 $D=189
M79 VDD! SEL<1> 8 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13980 $Y=64910 $D=189
M80 VDD! 8 7 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=63090 $D=189
M81 VDD! SEL<0> 42 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=76110 $D=189
X82 42 SEL<0> 41 VSS! VDD! VSS! EN decoder_1st $T=14800 78400 1 180 $X=12280 $Y=78160
X83 42 SEL<0> 40 VSS! VDD! EN VSS! decoder_1st $T=14800 84000 1 180 $X=12280 $Y=83760
X84 8 7 39 VSS! VDD! VSS! 41 decoder_2st $T=13200 67200 1 180 $X=10680 $Y=66960
X85 8 7 38 VSS! VDD! VSS! 40 decoder_2st $T=13200 72800 1 180 $X=10680 $Y=72560
X86 8 7 37 VSS! VDD! 41 VSS! decoder_2st $T=13200 78400 1 180 $X=10680 $Y=78160
X87 8 7 36 VSS! VDD! 40 VSS! decoder_2st $T=13200 84000 1 180 $X=10680 $Y=83760
X88 E<7> E<6> 4 3 35 2 1 34 VDD! VSS! 39 VSS! VSS! 38 ICV_2 $T=6000 44800 1 180 $X=-300 $Y=44560
X89 E<5> E<4> 4 3 33 2 1 32 VDD! VSS! 37 VSS! VSS! 36 ICV_2 $T=6000 56000 1 180 $X=-300 $Y=55760
X90 E<3> E<2> 4 3 31 2 1 30 VDD! 39 VSS! VSS! 38 VSS! ICV_2 $T=6000 67200 1 180 $X=-300 $Y=66960
X91 E<1> E<0> 4 3 29 2 1 28 VDD! 37 VSS! VSS! 36 VSS! ICV_2 $T=6000 78400 1 180 $X=-300 $Y=78160
X92 E<15> E<14> 2 1 VDD! 35 VSS! 34 ICV_3 $T=6000 0 1 180 $X=-300 $Y=-240
X93 E<13> E<12> 2 1 VDD! 33 VSS! 32 ICV_3 $T=6000 11200 1 180 $X=-300 $Y=10960
X94 E<11> E<10> 2 1 VDD! 31 VSS! 30 ICV_3 $T=6000 22400 1 180 $X=-300 $Y=22160
X95 E<9> E<8> 2 1 VDD! 29 VSS! 28 ICV_3 $T=6000 33600 1 180 $X=-300 $Y=33360
.ENDS
***************************************
