* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT ICV_22
** N=544 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_21
** N=363 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_20
** N=583 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_19
** N=4226 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_18
** N=4223 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_17
** N=4483 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_16
** N=4223 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_15
** N=2364 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT RA1SH16x512MUX_CD_EVEN
** N=85 EP=0 IP=0 FDC=0
*.CALIBRE ISOLATED NETS: VSS VDD BL0_2 DRSA_2 BL0__2 DWSA_2 BL1__2 BL1_2 BL1_1 BL1__1 STUBDW_0 STUBDR_0 BL0__1 BL0_1 BL0_0 STUBDR__0 BL0__0 STUBDW__0 BL1__0 BL1_0
+ BL1 BL1_ DWSA DRSA BL0_ BL0 A0_ A0 YP1_3 YP1_2 YP1_1 YP1_0 YP0_3 YP0_2 YP0_1 YP0_0 GTP STUBDW STUBDR_ STUBDR
+ STUBDW_
.ENDS
***************************************
.SUBCKT RA1SH16x512MUX_CD_ODD
** N=85 EP=0 IP=0 FDC=0
*.CALIBRE ISOLATED NETS: VSS VDD BL0_2 DRSA_2 BL0__2 DWSA_2 BL1__2 BL1_2 BL1_1 BL1__1 STUBDW STUBDR BL0__1 BL0_1 BL0_0 STUBDR_ BL0__0 STUBDW_ BL1__0 BL1_0
+ BL1 BL1_ DWSA DRSA BL0_ BL0 A0_ STUBDW_0 STUBDR__0 STUBDR_0 STUBDW__0 A0 YP1_3 YP1_2 YP1_1 YP1_0 YP0_3 YP0_2 YP0_1 YP0_0
+ GTP
.ENDS
***************************************
.SUBCKT RA1SH16x512HXP38X
** N=77 EP=0 IP=0 FDC=0
*.CALIBRE ISOLATED NETS: VSS A2 VDD A1 A0 XP_7 XP_6 XP_5 XP_4 XP_3 XP_2 XP_1 XP_0 BWEN WEI OEI_ AGTPB AY0 AY0_ YP1_3
+ YP1_2 YP1_1 YP1_0 YP0_3 YP0_2 YP0_1 YP0_0 AGTPT
.ENDS
***************************************
.SUBCKT RA1SH16x512 VDD VSS D<0> Q<0> Q<1> D<1> D<2> Q<2> Q<3> D<3> D<4> Q<4> Q<5> D<5> D<6> Q<6> Q<7> D<7> A<2> A<1>
+ A<0> CEN WEN CLK D<8> Q<8> Q<9> D<9> D<10> Q<10> Q<11> D<11> D<12> Q<12> Q<13> D<13> D<14> Q<14> Q<15> D<15>
+ A<8> A<7> A<6> A<5> A<4> A<3>
** N=1361 EP=46 IP=4477 FDC=0
.ENDS
***************************************
.SUBCKT MUX_D_buf SEL_OUT SEL_IN VDD! VSS!
** N=29 EP=4 IP=0 FDC=6
M0 SEL_OUT 5 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=450 $D=97
M1 VSS! 5 SEL_OUT VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=450 $D=97
M2 5 SEL_IN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1390 $Y=450 $D=97
M3 SEL_OUT 5 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=1650 $D=189
M4 VDD! 5 SEL_OUT VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=1650 $D=189
M5 5 SEL_IN VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1390 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT MUX_D_1bit SEL<1> D ALU Shifter SEL<0> DMEM VSS! VDD!
** N=108 EP=8 IP=0 FDC=22
M0 VSS! SEL<1> 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=450 $D=97
M1 D 14 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=5210 $D=97
M2 12 SEL<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=450 $D=97
M3 VSS! 14 D VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=5210 $D=97
M4 VSS! ALU 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=450 $D=97
M5 14 SEL<1> 10 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=5070 $D=97
M6 15 Shifter VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=450 $D=97
M7 11 9 14 VSS! nfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2780 $Y=5230 $D=97
M8 11 SEL<0> 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=450 $D=97
M9 13 12 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=5270 $D=97
M10 VSS! DMEM 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=450 $D=97
M11 VDD! SEL<1> 9 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=1950 $D=189
M12 D 14 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=3510 $D=189
M13 12 SEL<0> VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=1950 $D=189
M14 VDD! 14 D VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=3510 $D=189
M15 VDD! ALU 10 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=1790 $D=189
M16 14 9 10 VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=3510 $D=189
M17 15 Shifter VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=1790 $D=189
M18 11 SEL<1> 14 VDD! pfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2880 $Y=3510 $D=189
M19 11 12 15 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=2210 $D=189
M20 13 SEL<0> 11 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=3510 $D=189
M21 VDD! DMEM 13 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=1790 $D=189
.ENDS
***************************************
.SUBCKT ICV_13 1 2 3 4 5 6 7 8 9 10 11 12
** N=12 EP=12 IP=16 FDC=44
X0 1 2 3 4 5 6 12 11 MUX_D_1bit $T=0 0 0 0 $X=-430 $Y=-240
X1 1 7 8 9 5 10 12 11 MUX_D_1bit $T=4800 0 0 0 $X=4370 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_14 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
** N=20 EP=20 IP=24 FDC=88
X0 1 2 3 4 18 5 6 7 8 9 19 20 ICV_13 $T=0 0 0 0 $X=-430 $Y=-240
X1 1 10 11 12 18 13 14 15 16 17 19 20 ICV_13 $T=9600 0 0 0 $X=9170 $Y=-240
.ENDS
***************************************
.SUBCKT MUX_D OUT<4> ALU<4> SH<4> DMEM<4> OUT<15> ALU<15> SH<15> DMEM<15> OUT<13> ALU<13> SH<13> DMEM<13> OUT<14> ALU<14> SH<14> DMEM<14> OUT<0> ALU<0> SH<0> DMEM<0>
+ OUT<1> ALU<1> SH<1> DMEM<1> OUT<2> ALU<2> SH<2> DMEM<2> OUT<3> ALU<3> SH<3> DMEM<3> OUT<5> ALU<5> SH<5> DMEM<5> OUT<6> ALU<6> SH<6> DMEM<6>
+ OUT<7> ALU<7> SH<7> DMEM<7> OUT<8> ALU<8> SH<8> DMEM<8> OUT<9> ALU<9> SH<9> DMEM<9> OUT<10> ALU<10> SH<10> DMEM<10> OUT<11> ALU<11> SH<11> DMEM<11>
+ OUT<12> ALU<12> SH<12> DMEM<12> SEL<1> SEL<0> VSS! VDD!
** N=265 EP=68 IP=96 FDC=364
X0 102 SEL<1> VDD! VSS! MUX_D_buf $T=0 26000 0 270 $X=-520 $Y=23870
X1 103 SEL<0> VDD! VSS! MUX_D_buf $T=6000 26000 1 270 $X=2510 $Y=23870
X2 102 OUT<4> ALU<4> SH<4> 103 DMEM<4> VSS! VDD! MUX_D_1bit $T=6000 19200 0 90 $X=-240 $Y=18770
X3 102 OUT<15> ALU<15> SH<15> 103 DMEM<15> VSS! VDD! MUX_D_1bit $T=6000 99600 0 90 $X=-240 $Y=99170
X4 102 OUT<13> ALU<13> SH<13> 103 DMEM<13> OUT<14> ALU<14> SH<14> DMEM<14> VDD! VSS! ICV_13 $T=6000 90000 0 90 $X=-240 $Y=89570
X5 102 OUT<0> ALU<0> SH<0> DMEM<0> OUT<1> ALU<1> SH<1> DMEM<1> OUT<2> ALU<2> SH<2> DMEM<2> OUT<3> ALU<3> SH<3> DMEM<3> 103 VDD! VSS! ICV_14 $T=6000 0 0 90 $X=-240 $Y=-430
X6 102 OUT<5> ALU<5> SH<5> DMEM<5> OUT<6> ALU<6> SH<6> DMEM<6> OUT<7> ALU<7> SH<7> DMEM<7> OUT<8> ALU<8> SH<8> DMEM<8> 103 VDD! VSS! ICV_14 $T=6000 51600 0 90 $X=-240 $Y=51170
X7 102 OUT<9> ALU<9> SH<9> DMEM<9> OUT<10> ALU<10> SH<10> DMEM<10> OUT<11> ALU<11> SH<11> DMEM<11> OUT<12> ALU<12> SH<12> DMEM<12> 103 VDD! VSS! ICV_14 $T=6000 70800 0 90 $X=-240 $Y=70370
.ENDS
***************************************
.SUBCKT MUX_Shifter_SEL_BUF SEL_OUT SEL_IN VDD! VSS!
** N=24 EP=4 IP=0 FDC=4
M0 VSS! 5 SEL_OUT VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=450 $D=97
M1 5 SEL_IN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=450 $D=97
M2 VDD! 5 SEL_OUT VDD! pfet L=1.2e-07 W=7.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=1750 $D=189
M3 5 SEL_IN VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT MUX_Shifter_SEL_1bit SEL<1> ALU_B Imm_8 SEL<0> Rsrc VSS! EIGHT VDD!
** N=104 EP=8 IP=0 FDC=22
M0 VSS! SEL<1> 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=450 $D=97
M1 ALU_B 14 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=5210 $D=97
M2 12 SEL<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=450 $D=97
M3 VSS! 14 ALU_B VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=5210 $D=97
M4 VSS! EIGHT 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=450 $D=97
M5 14 SEL<1> 10 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=5070 $D=97
M6 15 Imm_8 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=450 $D=97
M7 11 9 14 VSS! nfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2780 $Y=5230 $D=97
M8 11 SEL<0> 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=450 $D=97
M9 13 12 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=5270 $D=97
M10 VSS! Rsrc 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=450 $D=97
M11 VDD! SEL<1> 9 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=1950 $D=189
M12 ALU_B 14 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=3510 $D=189
M13 12 SEL<0> VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=1950 $D=189
M14 VDD! 14 ALU_B VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=3510 $D=189
M15 VDD! EIGHT 10 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=1950 $D=189
M16 14 9 10 VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=3510 $D=189
M17 15 Imm_8 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=1950 $D=189
M18 11 SEL<1> 14 VDD! pfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2880 $Y=3510 $D=189
M19 11 12 15 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=2210 $D=189
M20 13 SEL<0> 11 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=3510 $D=189
M21 VDD! Rsrc 13 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=1950 $D=189
.ENDS
***************************************
.SUBCKT Sign_Ext_left Imm_16 Imm_8 3 4
** N=22 EP=4 IP=0 FDC=4
M0 Imm_16 5 3 3 nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=1020 $D=97
M1 3 Imm_8 5 3 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=670 $Y=490 $D=97
M2 Imm_16 5 4 4 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=1020 $D=189
M3 4 Imm_8 5 4 pfet L=1.2e-07 W=4.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2040 $Y=490 $D=189
.ENDS
***************************************
.SUBCKT Sign_Ext_right IMM_16 SEL_BAR SEL 4 5
** N=49 EP=5 IP=0 FDC=10
M0 4 6 7 4 nfet L=1.2e-07 W=3.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=2210 $D=97
M1 IMM_16 7 4 4 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=670 $Y=2730 $D=97
M2 4 7 IMM_16 4 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=670 $Y=3330 $D=97
M3 6 SEL_BAR 4 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=720 $Y=570 $D=97
M4 5 SEL 6 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=720 $Y=1130 $D=97
M5 5 6 7 5 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=2210 $D=189
M6 IMM_16 7 5 5 pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2730 $D=189
M7 5 7 IMM_16 5 pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=3330 $D=189
M8 6 SEL 4 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=570 $D=189
M9 5 SEL_BAR 6 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1130 $D=189
.ENDS
***************************************
.SUBCKT BUFFER_SEL IN VSS! VDD! OUT
** N=85 EP=4 IP=0 FDC=14
M0 VSS! IN 5 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=730 $Y=450 $D=97
M1 6 5 VSS! VSS! nfet L=1.2e-07 W=6.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=450 $D=97
M2 7 6 VSS! VSS! nfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M3 VSS! 6 7 VSS! nfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M4 OUT 7 VSS! VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=450 $D=97
M5 VSS! 7 OUT VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=450 $D=97
M6 OUT 7 VSS! VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4110 $Y=450 $D=97
M7 VDD! IN 5 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=730 $Y=4150 $D=189
M8 6 5 VDD! VDD! pfet L=1.2e-07 W=1.19e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3500 $D=189
M9 7 6 VDD! VDD! pfet L=1.2e-07 W=1.3e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3390 $D=189
M10 VDD! 6 7 VDD! pfet L=1.2e-07 W=1.3e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3390 $D=189
M11 OUT 7 VDD! VDD! pfet L=1.2e-07 W=1.92e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=2770 $D=189
M12 VDD! 7 OUT VDD! pfet L=1.2e-07 W=1.92e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=2770 $D=189
M13 OUT 7 VDD! VDD! pfet L=1.2e-07 W=1.92e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4110 $Y=2770 $D=189
.ENDS
***************************************
.SUBCKT Z_cell4 VSS! P<1> P<3> P<0> P<2> VDD! OUT
** N=50 EP=7 IP=0 FDC=10
M0 9 P<0> VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=440 $D=97
M1 10 P<1> 9 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=920 $D=97
M2 11 P<2> 10 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=1400 $D=97
M3 8 P<3> 11 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=1880 $D=97
M4 VSS! 8 OUT VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5100 $Y=1070 $D=97
M5 8 P<0> VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=440 $D=189
M6 VDD! P<1> 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=920 $D=189
M7 8 P<2> VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1400 $D=189
M8 VDD! P<3> 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1880 $D=189
M9 VDD! 8 OUT VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3560 $Y=1070 $D=189
.ENDS
***************************************
.SUBCKT MUX41 SEL1 SEL0 OUT VSS! IN3 IN2 VDD! IN0 IN1
** N=127 EP=9 IP=0 FDC=28
M0 VSS! SEL1 12 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1090 $D=97
M1 13 SEL0 IN3 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2080 $D=97
M2 IN2 10 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2880 $D=97
M3 10 SEL0 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3880 $D=97
M4 OUT 11 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-1350 $D=97
M5 VSS! 11 OUT VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-870 $D=97
M6 OUT 11 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-390 $D=97
M7 VSS! 11 OUT VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=90 $D=97
M8 VSS! 14 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=-650 $D=97
M9 14 SEL1 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=940 $D=97
M10 15 12 14 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=2090 $D=97
M11 15 10 IN0 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=3340 $D=97
M12 IN1 SEL0 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=4140 $D=97
M13 OUT 11 VDD! VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-1350 $D=189
M14 VDD! 11 OUT VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-870 $D=189
M15 OUT 11 VDD! VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-390 $D=189
M16 VDD! 11 OUT VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=90 $D=189
M17 12 SEL1 VDD! VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=610 $D=189
M18 VDD! SEL1 12 VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1090 $D=189
M19 10 SEL0 VDD! VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=3880 $D=189
M20 VDD! SEL0 10 VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=4360 $D=189
M21 13 10 IN3 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=2080 $D=189
M22 IN2 SEL0 13 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=2880 $D=189
M23 VDD! 14 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=-650 $D=189
M24 14 12 13 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=940 $D=189
M25 15 SEL1 14 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2140 $D=189
M26 15 SEL0 IN0 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3340 $D=189
M27 IN1 10 15 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=4140 $D=189
.ENDS
***************************************
.SUBCKT SETUP_BIT A B XOR AND OR VSS! VDD!
** N=98 EP=7 IP=0 FDC=26
M0 13 A 11 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=5030 $D=97
M1 VSS! A 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=520 $Y=430 $D=97
M2 VSS! B 13 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=5030 $D=97
M3 8 B VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1000 $Y=430 $D=97
M4 AND 11 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=5290 $D=97
M5 14 8 OR VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=430 $D=97
M6 15 A 12 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=5030 $D=97
M7 VSS! 10 14 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=430 $D=97
M8 VSS! 8 15 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=5030 $D=97
M9 16 10 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=430 $D=97
M10 17 12 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=5030 $D=97
M11 9 B 16 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=430 $D=97
M12 XOR 9 17 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=5030 $D=97
M13 11 A VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=3510 $D=189
M14 VDD! A 10 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=520 $Y=1950 $D=189
M15 VDD! B 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=3510 $D=189
M16 8 B VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1000 $Y=1950 $D=189
M17 AND 11 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=3510 $D=189
M18 OR 8 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=1950 $D=189
M19 12 A VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=3510 $D=189
M20 VDD! 10 OR VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=1950 $D=189
M21 VDD! 8 12 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=3510 $D=189
M22 9 10 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=1950 $D=189
M23 XOR 12 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=3510 $D=189
M24 VDD! B 9 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=1950 $D=189
M25 VDD! 9 XOR VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT ICV_12 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
** N=18 EP=18 IP=32 FDC=108
X0 1 2 5 15 10 16 17 9 8 MUX41 $T=0 0 0 0 $X=-240 $Y=-2300
X1 1 2 6 15 14 18 17 13 12 MUX41 $T=6000 0 0 0 $X=5760 $Y=-2300
X2 7 3 8 9 10 15 17 SETUP_BIT $T=0 9600 0 270 $X=-240 $Y=4490
X3 11 4 12 13 14 15 17 SETUP_BIT $T=6000 9600 0 270 $X=5760 $Y=4490
.ENDS
***************************************
.SUBCKT MUX21_BAR SEL VSS! IN<1> VDD! OUT IN<0>
** N=41 EP=6 IP=0 FDC=8
M0 8 SEL IN<1> VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=600 $Y=5270 $D=97
M1 VSS! SEL 7 VSS! nfet L=1.2e-07 W=5.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=640 $Y=450 $D=97
M2 OUT 8 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1120 $Y=450 $D=97
M3 IN<0> 7 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1280 $Y=5270 $D=97
M4 8 7 IN<1> VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=600 $Y=3510 $D=189
M5 VDD! SEL 7 VDD! pfet L=1.2e-07 W=9.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=640 $Y=1580 $D=189
M6 OUT 8 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1120 $Y=1950 $D=189
M7 IN<0> SEL 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1280 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT MUX84 SEL VDD! VSS! S3<1> S<3> S3<0> S2<1> S<2> S2<0> S1<1> S<1> S1<0> S0<1> S<0> S0<0>
** N=15 EP=15 IP=24 FDC=32
X0 SEL VSS! S3<1> VDD! S<3> S3<0> MUX21_BAR $T=6000 2000 1 270 $X=-240 $Y=-200
X1 SEL VSS! S2<1> VDD! S<2> S2<0> MUX21_BAR $T=12000 2000 1 270 $X=5760 $Y=-200
X2 SEL VSS! S1<1> VDD! S<1> S1<0> MUX21_BAR $T=18000 2000 1 270 $X=11760 $Y=-200
X3 SEL VSS! S0<1> VDD! S<0> S0<0> MUX21_BAR $T=24000 2000 1 270 $X=17760 $Y=-200
.ENDS
***************************************
.SUBCKT RIPPLE_BIT_BAR B CIN A VSS! SUM VDD! COUT
** N=115 EP=7 IP=0 FDC=24
M0 10 8 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=450 $D=97
M1 VSS! B 12 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=5070 $D=97
M2 13 A VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1360 $Y=5190 $D=97
M3 9 10 CIN VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1920 $Y=450 $D=97
M4 8 B 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1980 $Y=5270 $D=97
M5 14 8 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2600 $Y=450 $D=97
M6 A 12 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2660 $Y=5270 $D=97
M7 VSS! CIN 14 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3220 $Y=450 $D=97
M8 SUM 9 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3700 $Y=450 $D=97
M9 11 10 B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3740 $Y=5270 $D=97
M10 CIN 8 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4420 $Y=5270 $D=97
M11 COUT 11 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4930 $Y=450 $D=97
M12 10 8 VDD! VDD! pfet L=1.2e-07 W=9.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=1540 $D=189
M13 VDD! B 12 VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=3510 $D=189
M14 13 A VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1360 $Y=3510 $D=189
M15 9 8 CIN VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1920 $Y=2210 $D=189
M16 8 12 13 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1980 $Y=3510 $D=189
M17 14 10 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2600 $Y=2210 $D=189
M18 A B 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2660 $Y=3510 $D=189
M19 VDD! CIN 14 VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3220 $Y=1630 $D=189
M20 SUM 9 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3700 $Y=1630 $D=189
M21 11 8 B VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3740 $Y=3510 $D=189
M22 CIN 10 11 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4420 $Y=3510 $D=189
M23 COUT 11 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4930 $Y=1630 $D=189
.ENDS
***************************************
.SUBCKT RIPPLE_BIT B CIN A SUM VSS! VDD! COUT
** N=124 EP=7 IP=0 FDC=26
M0 VSS! B 13 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=470 $Y=5070 $D=97
M1 10 8 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=450 $D=97
M2 14 A VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=950 $Y=5190 $D=97
M3 8 B 14 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=5270 $D=97
M4 9 10 CIN VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1650 $Y=450 $D=97
M5 A 13 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2250 $Y=5270 $D=97
M6 15 8 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2330 $Y=450 $D=97
M7 VSS! CIN 15 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2950 $Y=450 $D=97
M8 11 10 B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3330 $Y=5270 $D=97
M9 SUM 9 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3430 $Y=450 $D=97
M10 CIN 8 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4010 $Y=5270 $D=97
M11 COUT 12 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4850 $Y=450 $D=97
M12 12 11 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4990 $Y=5150 $D=97
M13 VDD! B 13 VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=470 $Y=3510 $D=189
M14 10 8 VDD! VDD! pfet L=1.2e-07 W=9.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=1540 $D=189
M15 14 A VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=950 $Y=3510 $D=189
M16 8 13 14 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=3510 $D=189
M17 9 8 CIN VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1650 $Y=2210 $D=189
M18 A B 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2250 $Y=3510 $D=189
M19 15 10 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2330 $Y=2210 $D=189
M20 VDD! CIN 15 VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2950 $Y=1630 $D=189
M21 11 8 B VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3330 $Y=3510 $D=189
M22 SUM 9 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3430 $Y=1630 $D=189
M23 CIN 10 11 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4010 $Y=3510 $D=189
M24 COUT 12 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4850 $Y=1630 $D=189
M25 12 11 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4990 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT RIPPLE_4BIT CIN B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! SUM<3> COUT SUM<2> SUM<1> SUM<0>
** N=19 EP=16 IP=28 FDC=102
X0 B<3> 17 A<3> VSS! SUM<3> VDD! COUT RIPPLE_BIT_BAR $T=0 0 0 0 $X=-200 $Y=-240
X1 B<2> 18 A<2> SUM<2> VSS! VDD! 17 RIPPLE_BIT $T=0 6000 0 0 $X=-200 $Y=5760
X2 B<1> 19 A<1> SUM<1> VSS! VDD! 18 RIPPLE_BIT $T=0 12000 0 0 $X=-200 $Y=11760
X3 B<0> CIN A<0> SUM<0> VSS! VDD! 19 RIPPLE_BIT $T=0 18000 0 0 $X=-200 $Y=17760
.ENDS
***************************************
.SUBCKT RIPPLE_4BIT_LAST_4BIT C3 CIN B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! SUM<3> COUT SUM<2> SUM<1> SUM<0>
** N=19 EP=17 IP=28 FDC=102
X0 B<3> C3 A<3> VSS! SUM<3> VDD! COUT RIPPLE_BIT_BAR $T=0 0 0 0 $X=-200 $Y=-240
X1 B<2> 18 A<2> SUM<2> VSS! VDD! C3 RIPPLE_BIT $T=0 6000 0 0 $X=-200 $Y=5760
X2 B<1> 19 A<1> SUM<1> VSS! VDD! 18 RIPPLE_BIT $T=0 12000 0 0 $X=-200 $Y=11760
X3 B<0> CIN A<0> SUM<0> VSS! VDD! 19 RIPPLE_BIT $T=0 18000 0 0 $X=-200 $Y=17760
.ENDS
***************************************
.SUBCKT ALU SEL<1> F N SEL<0> Z CIN B<0> B<1> B<2> B<3> B<4> B<5> B<6> B<7> B<8> B<9> COUT B<10> B<11> B<12>
+ B<13> B<14> B<15> A<0> A<1> A<2> A<3> A<4> A<5> A<6> A<7> A<8> A<9> A<10> A<11> A<12> A<13> A<14> A<15> OUT<0>
+ OUT<1> OUT<2> OUT<3> OUT<4> OUT<5> OUT<6> OUT<7> OUT<8> OUT<9> OUT<10> OUT<11> OUT<12> OUT<13> OUT<14> OUT<15> VDD! VSS!
** N=501 EP=57 IP=401 FDC=1972
M0 195 CIN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2740 $Y=14450 $D=97
M1 74 195 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2740 $Y=15410 $D=97
M2 204 106 197 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40190 $Y=14960 $D=97
M3 VSS! 74 204 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40190 $Y=15440 $D=97
M4 VSS! 197 Z VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40450 $Y=14010 $D=97
M5 VSS! COUT 126 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=64370 $Y=15090 $D=97
M6 205 130 129 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=14070 $D=97
M7 VSS! 127 205 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=14550 $D=97
M8 206 127 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=15030 $D=97
M9 130 126 206 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=15510 $D=97
M10 207 199 N VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=14070 $D=97
M11 VSS! 129 207 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=14550 $D=97
M12 208 126 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=15030 $D=97
M13 199 130 208 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=15510 $D=97
M14 209 146 144 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=14070 $D=97
M15 VSS! 145 209 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=14550 $D=97
M16 210 145 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=15030 $D=97
M17 146 COUT 210 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=15510 $D=97
M18 211 201 F VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=14070 $D=97
M19 VSS! 144 211 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=14550 $D=97
M20 212 COUT VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=15030 $D=97
M21 201 146 212 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=15510 $D=97
M22 VSS! 134 151 VSS! nfet L=1.2e-07 W=5.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=89650 $Y=14040 $D=97
M23 202 150 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=89650 $Y=14520 $D=97
M24 145 202 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=89650 $Y=15460 $D=97
M25 150 134 158 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=94470 $Y=14200 $D=97
M26 154 151 150 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=94470 $Y=14880 $D=97
M27 195 CIN VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1600 $Y=14450 $D=189
M28 74 195 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1600 $Y=15410 $D=189
M29 VDD! 197 Z VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38750 $Y=14010 $D=189
M30 197 106 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38750 $Y=14960 $D=189
M31 VDD! 74 197 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38750 $Y=15440 $D=189
M32 VDD! COUT 126 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=62750 $Y=15090 $D=189
M33 129 130 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=14070 $D=189
M34 VDD! 127 129 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=14550 $D=189
M35 130 127 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=15030 $D=189
M36 VDD! 126 130 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=15510 $D=189
M37 N 199 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=14070 $D=189
M38 VDD! 129 N VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=14550 $D=189
M39 199 126 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=15030 $D=189
M40 VDD! 130 199 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=15510 $D=189
M41 144 146 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=14070 $D=189
M42 VDD! 145 144 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=14550 $D=189
M43 146 145 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=15030 $D=189
M44 VDD! COUT 146 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=15510 $D=189
M45 F 201 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=14070 $D=189
M46 VDD! 144 F VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=14550 $D=189
M47 201 COUT VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=15030 $D=189
M48 VDD! 146 201 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=15510 $D=189
M49 VDD! 134 151 VDD! pfet L=1.2e-07 W=9.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=90780 $Y=14040 $D=189
M50 202 150 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=91150 $Y=14520 $D=189
M51 145 202 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=91150 $Y=15460 $D=189
M52 150 151 158 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=92710 $Y=14200 $D=189
M53 154 134 150 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=92710 $Y=14880 $D=189
X54 SEL<1> VSS! VDD! 75 BUFFER_SEL $T=5200 4800 1 270 $X=-240 $Y=-100
X55 SEL<0> VSS! VDD! 76 BUFFER_SEL $T=5200 9600 1 270 $X=-240 $Y=4700
X56 VSS! 82 83 79 84 VDD! 78 Z_cell4 $T=11200 13600 1 180 $X=4930 $Y=13350
X57 VSS! 100 101 97 102 VDD! 96 Z_cell4 $T=35200 13600 1 180 $X=28930 $Y=13350
X58 VSS! 96 110 78 111 VDD! 106 Z_cell4 $T=47200 13600 1 180 $X=40930 $Y=13350
X59 VSS! 120 121 117 122 VDD! 111 Z_cell4 $T=59200 13600 1 180 $X=52930 $Y=13350
X60 VSS! 141 127 138 142 VDD! 110 Z_cell4 $T=83200 13600 1 180 $X=76930 $Y=13350
X61 75 76 B<0> B<1> OUT<0> OUT<1> A<0> 79 81 77 A<1> 82 87 85 VSS! 159 VDD! 161 ICV_12 $T=5200 2000 0 0 $X=4960 $Y=-300
X62 75 76 B<2> B<3> OUT<2> OUT<3> A<2> 84 90 88 A<3> 83 94 91 VSS! 163 VDD! 165 ICV_12 $T=17200 2000 0 0 $X=16960 $Y=-300
X63 75 76 B<4> B<5> OUT<4> OUT<5> A<4> 97 99 95 A<5> 100 105 103 VSS! 168 VDD! 170 ICV_12 $T=29200 2000 0 0 $X=28960 $Y=-300
X64 75 76 B<6> B<7> OUT<6> OUT<7> A<6> 102 109 107 A<7> 101 115 112 VSS! 172 VDD! 174 ICV_12 $T=41200 2000 0 0 $X=40960 $Y=-300
X65 75 76 B<8> B<9> OUT<8> OUT<9> A<8> 117 119 116 A<9> 120 125 123 VSS! 177 VDD! 179 ICV_12 $T=53200 2000 0 0 $X=52960 $Y=-300
X66 75 76 B<10> B<11> OUT<10> OUT<11> A<10> 122 132 128 A<11> 121 136 133 VSS! 181 VDD! 183 ICV_12 $T=65200 2000 0 0 $X=64960 $Y=-300
X67 75 76 B<12> B<13> OUT<12> OUT<13> A<12> 138 140 137 A<13> 141 148 143 VSS! 186 VDD! 188 ICV_12 $T=77200 2000 0 0 $X=76960 $Y=-300
X68 75 76 B<14> B<15> OUT<14> OUT<15> A<14> 142 153 149 A<15> 127 157 155 VSS! 190 VDD! 192 ICV_12 $T=89200 2000 0 0 $X=88960 $Y=-300
X69 74 VSS! 196 VDD! 92 167 MUX21_BAR $T=23200 16000 0 270 $X=22960 $Y=13800
X70 92 VSS! 198 VDD! 113 176 MUX21_BAR $T=47200 16000 0 270 $X=46960 $Y=13800
X71 113 VSS! 200 VDD! 134 185 MUX21_BAR $T=71200 16000 0 270 $X=70960 $Y=13800
X72 134 VSS! 203 VDD! COUT 194 MUX21_BAR $T=95200 16000 0 270 $X=94960 $Y=13800
X73 74 VDD! VSS! 166 165 93 164 163 89 162 161 86 160 159 80 MUX84 $T=29200 11600 1 180 $X=4960 $Y=11400
X74 92 VDD! VSS! 175 174 114 173 172 108 171 170 104 169 168 98 MUX84 $T=53200 11600 1 180 $X=28960 $Y=11400
X75 113 VDD! VSS! 184 183 135 182 181 131 180 179 124 178 177 118 MUX84 $T=77200 11600 1 180 $X=52960 $Y=11400
X76 134 VDD! VSS! 193 192 156 191 190 152 189 188 147 187 186 139 MUX84 $T=101200 11600 1 180 $X=76960 $Y=11400
X77 VDD! B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! 166 196 164 162 160 RIPPLE_4BIT $T=29200 21600 1 270 $X=4960 $Y=15800
X78 VSS! B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! 93 167 89 86 80 RIPPLE_4BIT $T=29200 27200 1 270 $X=4960 $Y=21400
X79 VDD! B<7> A<7> B<6> A<6> B<5> A<5> B<4> A<4> VDD! VSS! 175 198 173 171 169 RIPPLE_4BIT $T=53200 21600 1 270 $X=28960 $Y=15800
X80 VSS! B<7> A<7> B<6> A<6> B<5> A<5> B<4> A<4> VDD! VSS! 114 176 108 104 98 RIPPLE_4BIT $T=53200 27200 1 270 $X=28960 $Y=21400
X81 VDD! B<11> A<11> B<10> A<10> B<9> A<9> B<8> A<8> VDD! VSS! 184 200 182 180 178 RIPPLE_4BIT $T=77200 21600 1 270 $X=52960 $Y=15800
X82 VSS! B<11> A<11> B<10> A<10> B<9> A<9> B<8> A<8> VDD! VSS! 135 185 131 124 118 RIPPLE_4BIT $T=77200 27200 1 270 $X=52960 $Y=21400
X83 158 VDD! B<15> A<15> B<14> A<14> B<13> A<13> B<12> A<12> VDD! VSS! 193 203 191 189 187 RIPPLE_4BIT_LAST_4BIT $T=101200 21600 1 270 $X=76960 $Y=15800
X84 154 VSS! B<15> A<15> B<14> A<14> B<13> A<13> B<12> A<12> VDD! VSS! 156 194 152 147 139 RIPPLE_4BIT_LAST_4BIT $T=101200 27200 1 270 $X=76960 $Y=21400
.ENDS
***************************************
.SUBCKT 3rd_bit SH<2> SH<2>_BAR D 4 5 6 7
** N=48 EP=7 IP=0 FDC=8
M0 4 8 D 4 nfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=360 $D=97
M1 5 SH<2> 8 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1320 $D=97
M2 4 8 D 4 nfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4920 $Y=360 $D=97
M3 7 SH<2>_BAR 8 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=1320 $D=97
M4 6 8 D 6 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=360 $D=189
M5 5 SH<2>_BAR 8 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1320 $D=189
M6 6 8 D 6 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=360 $D=189
M7 7 SH<2> 8 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1320 $D=189
.ENDS
***************************************
.SUBCKT 1st_bit SH<0> SH<0>_BAR 3 4 5 6 7
** N=30 EP=7 IP=0 FDC=4
M0 5 SH<0> 3 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=520 $D=97
M1 7 SH<0>_BAR 3 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=520 $D=97
M2 5 SH<0>_BAR 3 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=520 $D=189
M3 7 SH<0> 3 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
.ENDS
***************************************
.SUBCKT 2rd_bit SH<1> SH<1>_BAR 3 4 5 6 7
** N=28 EP=7 IP=0 FDC=4
M0 5 SH<1> 3 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=520 $D=97
M1 7 SH<1>_BAR 3 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4990 $Y=520 $D=97
M2 5 SH<1>_BAR 3 6 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1930 $Y=520 $D=189
M3 7 SH<1> 3 6 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
.ENDS
***************************************
.SUBCKT Data_Buffer E VSS! IN VDD!
** N=80 EP=4 IP=0 FDC=18
M0 6 IN VSS! VSS! nfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=1260 $D=97
M1 5 6 VSS! VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=2200 $D=97
M2 VSS! 6 5 VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=2680 $D=97
M3 E 5 VSS! VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=3160 $D=97
M4 VSS! 5 E VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=1260 $D=97
M5 E 5 VSS! VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=1740 $D=97
M6 VSS! 5 E VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=2220 $D=97
M7 E 5 VSS! VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=2700 $D=97
M8 VSS! 5 E VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=3180 $D=97
M9 6 IN VDD! VDD! pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1760 $Y=1260 $D=189
M10 E 5 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1790 $Y=3160 $D=189
M11 5 6 VDD! VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2200 $D=189
M12 VDD! 6 5 VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2680 $D=189
M13 VDD! 5 E VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1260 $D=189
M14 E 5 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1740 $D=189
M15 VDD! 5 E VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2220 $D=189
M16 E 5 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2700 $D=189
M17 VDD! 5 E VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3180 $D=189
.ENDS
***************************************
.SUBCKT 4th_bit SH3 SH3_BAR OUT VSS! IN1 VDD! IN2
** N=28 EP=7 IP=0 FDC=4
M0 OUT SH3 IN1 VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=520 $D=97
M1 OUT SH3_BAR IN2 VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5230 $Y=520 $D=97
M2 OUT SH3_BAR IN1 VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2150 $Y=520 $D=189
M3 OUT SH3 IN2 VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
.ENDS
***************************************
.SUBCKT ICV_10 1 2 3 4 5 6 7
** N=8 EP=7 IP=11 FDC=22
X0 3 4 8 5 Data_Buffer $T=0 0 0 0 $X=-240 $Y=-460
X1 1 2 8 4 6 5 7 4th_bit $T=0 -260 0 0 $X=-240 $Y=-460
.ENDS
***************************************
.SUBCKT ICV_11 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27
** N=27 EP=27 IP=56 FDC=76
X0 1 2 14 11 12 13 15 3rd_bit $T=0 -1460 1 0 $X=-250 $Y=-3860
X1 1 2 17 11 16 13 18 3rd_bit $T=6000 -1460 1 0 $X=5750 $Y=-3860
X2 3 4 20 11 19 13 21 1st_bit $T=0 -6660 1 0 $X=-240 $Y=-8460
X3 3 4 22 11 21 13 23 1st_bit $T=6000 -6660 1 0 $X=5760 $Y=-8460
X4 5 6 15 11 24 13 20 2rd_bit $T=0 -4660 1 0 $X=-240 $Y=-6600
X5 5 6 18 11 25 13 22 2rd_bit $T=6000 -4660 1 0 $X=5760 $Y=-6600
X6 7 8 9 11 13 26 14 ICV_10 $T=0 0 0 0 $X=-240 $Y=-460
X7 7 8 10 11 13 27 17 ICV_10 $T=6000 0 0 0 $X=5760 $Y=-460
.ENDS
***************************************
.SUBCKT SHIFTER A<0> A<1> A<2> A<3> A<4> A<5> A<6> A<7> A<8> A<9> A<10> A<11> A<12> A<13> A<14> SEL<3> SEL<2> SEL<1> SEL<0> A<15>
+ E<0> E<1> E<2> E<3> E<4> E<5> E<6> E<7> E<8> E<9> E<10> E<11> E<12> E<13> E<14> E<15> VDD! VSS!
** N=881 EP=38 IP=216 FDC=648
M0 76 73 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=2240 $D=97
M1 VSS! 73 76 VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=2720 $D=97
M2 70 74 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=3200 $D=97
M3 VSS! 74 70 VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=3680 $D=97
M4 77 71 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=7800 $D=97
M5 VSS! 71 77 VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=8280 $D=97
M6 75 72 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=8760 $D=97
M7 VSS! 72 75 VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=9240 $D=97
M8 73 112 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=1240 $D=97
M9 VSS! 112 73 VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=1720 $D=97
M10 74 113 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=4200 $D=97
M11 VSS! 113 74 VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=4680 $D=97
M12 71 114 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=6800 $D=97
M13 VSS! 114 71 VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=7280 $D=97
M14 72 115 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=9760 $D=97
M15 VSS! 115 72 VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=10240 $D=97
M16 VSS! SEL<3> 112 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4310 $Y=720 $D=97
M17 113 SEL<2> VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4310 $Y=5200 $D=97
M18 VSS! SEL<1> 114 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4310 $Y=6280 $D=97
M19 115 SEL<0> VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4310 $Y=10760 $D=97
M20 VDD! SEL<3> 112 VDD! pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=720 $D=189
M21 73 112 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=1240 $D=189
M22 VDD! 112 73 VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=1720 $D=189
M23 76 73 VDD! VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=2240 $D=189
M24 VDD! 73 76 VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=2720 $D=189
M25 70 74 VDD! VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=3200 $D=189
M26 VDD! 74 70 VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=3680 $D=189
M27 74 113 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=4200 $D=189
M28 VDD! 113 74 VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=4680 $D=189
M29 113 SEL<2> VDD! VDD! pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=5200 $D=189
M30 VDD! SEL<1> 114 VDD! pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=6280 $D=189
M31 71 114 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=6800 $D=189
M32 VDD! 114 71 VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=7280 $D=189
M33 77 71 VDD! VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=7800 $D=189
M34 VDD! 71 77 VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=8280 $D=189
M35 75 72 VDD! VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=8760 $D=189
M36 VDD! 72 75 VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=9240 $D=189
M37 72 115 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=9760 $D=189
M38 VDD! 115 72 VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=10240 $D=189
M39 115 SEL<0> VDD! VDD! pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=10760 $D=189
X40 74 70 72 75 71 77 73 76 E<0> E<1> VSS! VSS! VDD! 78 79 VSS! 81 82 VSS! 80
+ A<0> 83 A<1> VSS! VSS! VDD! VDD!
+ ICV_11 $T=5200 3740 1 0 $X=4950 $Y=-200
X41 74 70 72 75 71 77 73 76 E<2> E<3> VSS! VSS! VDD! 84 85 VSS! 87 88 A<1> 86
+ A<2> 89 A<3> 80 83 VDD! VDD!
+ ICV_11 $T=17200 3740 1 0 $X=16950 $Y=-200
X42 74 70 72 75 71 77 73 76 E<4> E<5> VSS! 79 VDD! 90 91 82 93 94 A<3> 92
+ A<4> 95 A<5> 86 89 VDD! VDD!
+ ICV_11 $T=29200 3740 1 0 $X=28950 $Y=-200
X43 74 70 72 75 71 77 73 76 E<6> E<7> VSS! 85 VDD! 96 97 88 99 100 A<5> 98
+ A<6> 101 A<7> 92 95 VDD! VDD!
+ ICV_11 $T=41200 3740 1 0 $X=40950 $Y=-200
X44 74 70 72 75 71 77 73 76 E<8> E<9> VSS! 91 VDD! 116 102 94 117 104 A<7> 103
+ A<8> 105 A<9> 98 101 78 81
+ ICV_11 $T=53200 3740 1 0 $X=52950 $Y=-200
X45 74 70 72 75 71 77 73 76 E<10> E<11> VSS! 97 VDD! 118 106 100 119 108 A<9> 107
+ A<10> 109 A<11> 103 105 84 87
+ ICV_11 $T=65200 3740 1 0 $X=64950 $Y=-200
X46 74 70 72 75 71 77 73 76 E<12> E<13> VSS! 102 VDD! 120 121 104 122 123 A<11> 110
+ A<12> 111 A<13> 107 109 90 93
+ ICV_11 $T=77200 3740 1 0 $X=76950 $Y=-200
X47 74 70 72 75 71 77 73 76 E<14> E<15> VSS! 106 VDD! 124 125 108 127 128 A<13> 126
+ A<14> 129 A<15> 110 111 96 99
+ ICV_11 $T=89200 3740 1 0 $X=88950 $Y=-200
.ENDS
***************************************
.SUBCKT RF_SCLK CLK VDD! WE<15> WE<14> WE<13> WE<12> WE<11> WE<10> WE<9> WE<8> WE<7> WE<6> WE<5> WE<4> WE<3> WE<2> WE<1> WE<0> WE_CLK_B<15> WE_CLK_B<14>
+ WE_CLK_B<13> WE_CLK_B<12> WE_CLK_B<11> WE_CLK_B<10> WE_CLK_B<9> WE_CLK_B<8> WE_CLK_B<7> WE_CLK_B<6> WE_CLK_B<5> WE_CLK_B<4> WE_CLK_B<3> WE_CLK_B<2> WE_CLK_B<1> WE_CLK_B<0> WE_CLK<15> WE_CLK<14> WE_CLK<13> WE_CLK<12> WE_CLK<11> WE_CLK<10>
+ WE_CLK<9> WE_CLK<8> WE_CLK<7> WE_CLK<6> WE_CLK<5> WE_CLK<4> WE_CLK<3> WE_CLK<2> WE_CLK<1> WE_CLK<0> VSS!
** N=1263 EP=51 IP=0 FDC=224
M0 WE_CLK_B<15> WE_CLK<15> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=470 $D=97
M1 VSS! WE_CLK<15> WE_CLK_B<15> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=950 $D=97
M2 WE_CLK_B<15> WE_CLK<15> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=1430 $D=97
M3 VSS! WE_CLK<15> WE_CLK_B<15> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=1910 $D=97
M4 WE_CLK<15> 52 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=2390 $D=97
M5 WE_CLK_B<14> WE_CLK<14> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=4290 $D=97
M6 VSS! WE_CLK<14> WE_CLK_B<14> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=4770 $D=97
M7 WE_CLK_B<14> WE_CLK<14> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=5250 $D=97
M8 VSS! WE_CLK<14> WE_CLK_B<14> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=5730 $D=97
M9 WE_CLK<14> 53 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=6210 $D=97
M10 WE_CLK_B<13> WE_CLK<13> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=8110 $D=97
M11 VSS! WE_CLK<13> WE_CLK_B<13> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=8590 $D=97
M12 WE_CLK_B<13> WE_CLK<13> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=9070 $D=97
M13 VSS! WE_CLK<13> WE_CLK_B<13> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=9550 $D=97
M14 WE_CLK<13> 54 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=10030 $D=97
M15 WE_CLK_B<12> WE_CLK<12> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=11930 $D=97
M16 VSS! WE_CLK<12> WE_CLK_B<12> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=12410 $D=97
M17 WE_CLK_B<12> WE_CLK<12> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=12890 $D=97
M18 VSS! WE_CLK<12> WE_CLK_B<12> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=13370 $D=97
M19 WE_CLK<12> 55 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=13850 $D=97
M20 WE_CLK_B<11> WE_CLK<11> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=15750 $D=97
M21 VSS! WE_CLK<11> WE_CLK_B<11> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=16230 $D=97
M22 WE_CLK_B<11> WE_CLK<11> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=16710 $D=97
M23 VSS! WE_CLK<11> WE_CLK_B<11> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=17190 $D=97
M24 WE_CLK<11> 56 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=17670 $D=97
M25 WE_CLK_B<10> WE_CLK<10> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=19570 $D=97
M26 VSS! WE_CLK<10> WE_CLK_B<10> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=20050 $D=97
M27 WE_CLK_B<10> WE_CLK<10> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=20530 $D=97
M28 VSS! WE_CLK<10> WE_CLK_B<10> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=21010 $D=97
M29 WE_CLK<10> 57 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=21490 $D=97
M30 WE_CLK_B<9> WE_CLK<9> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=23390 $D=97
M31 VSS! WE_CLK<9> WE_CLK_B<9> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=23870 $D=97
M32 WE_CLK_B<9> WE_CLK<9> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=24350 $D=97
M33 VSS! WE_CLK<9> WE_CLK_B<9> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=24830 $D=97
M34 WE_CLK<9> 58 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=25310 $D=97
M35 WE_CLK_B<8> WE_CLK<8> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=27210 $D=97
M36 VSS! WE_CLK<8> WE_CLK_B<8> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=27690 $D=97
M37 WE_CLK_B<8> WE_CLK<8> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=28170 $D=97
M38 VSS! WE_CLK<8> WE_CLK_B<8> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=28650 $D=97
M39 WE_CLK<8> 59 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=29130 $D=97
M40 WE_CLK_B<7> WE_CLK<7> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=31030 $D=97
M41 VSS! WE_CLK<7> WE_CLK_B<7> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=31510 $D=97
M42 WE_CLK_B<7> WE_CLK<7> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=31990 $D=97
M43 VSS! WE_CLK<7> WE_CLK_B<7> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=32470 $D=97
M44 WE_CLK<7> 60 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=32950 $D=97
M45 WE_CLK_B<6> WE_CLK<6> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=34850 $D=97
M46 VSS! WE_CLK<6> WE_CLK_B<6> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=35330 $D=97
M47 WE_CLK_B<6> WE_CLK<6> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=35810 $D=97
M48 VSS! WE_CLK<6> WE_CLK_B<6> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=36290 $D=97
M49 WE_CLK<6> 61 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=36770 $D=97
M50 WE_CLK_B<5> WE_CLK<5> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=38670 $D=97
M51 VSS! WE_CLK<5> WE_CLK_B<5> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=39150 $D=97
M52 WE_CLK_B<5> WE_CLK<5> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=39630 $D=97
M53 VSS! WE_CLK<5> WE_CLK_B<5> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=40110 $D=97
M54 WE_CLK<5> 62 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=40590 $D=97
M55 WE_CLK_B<4> WE_CLK<4> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=42490 $D=97
M56 VSS! WE_CLK<4> WE_CLK_B<4> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=42970 $D=97
M57 WE_CLK_B<4> WE_CLK<4> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=43450 $D=97
M58 VSS! WE_CLK<4> WE_CLK_B<4> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=43930 $D=97
M59 WE_CLK<4> 63 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=44410 $D=97
M60 WE_CLK_B<3> WE_CLK<3> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=46310 $D=97
M61 VSS! WE_CLK<3> WE_CLK_B<3> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=46790 $D=97
M62 WE_CLK_B<3> WE_CLK<3> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=47270 $D=97
M63 VSS! WE_CLK<3> WE_CLK_B<3> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=47750 $D=97
M64 WE_CLK<3> 64 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=48230 $D=97
M65 WE_CLK_B<2> WE_CLK<2> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=50130 $D=97
M66 VSS! WE_CLK<2> WE_CLK_B<2> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=50610 $D=97
M67 WE_CLK_B<2> WE_CLK<2> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=51090 $D=97
M68 VSS! WE_CLK<2> WE_CLK_B<2> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=51570 $D=97
M69 WE_CLK<2> 65 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=52050 $D=97
M70 WE_CLK_B<1> WE_CLK<1> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=53950 $D=97
M71 VSS! WE_CLK<1> WE_CLK_B<1> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=54430 $D=97
M72 WE_CLK_B<1> WE_CLK<1> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=54910 $D=97
M73 VSS! WE_CLK<1> WE_CLK_B<1> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=55390 $D=97
M74 WE_CLK<1> 66 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=55870 $D=97
M75 WE_CLK_B<0> WE_CLK<0> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=57770 $D=97
M76 VSS! WE_CLK<0> WE_CLK_B<0> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=58250 $D=97
M77 WE_CLK_B<0> WE_CLK<0> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=58730 $D=97
M78 VSS! WE_CLK<0> WE_CLK_B<0> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=59210 $D=97
M79 WE_CLK<0> 67 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=59690 $D=97
M80 68 CLK 52 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=3330 $D=97
M81 VSS! WE<15> 68 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=3810 $D=97
M82 69 CLK 53 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=7150 $D=97
M83 VSS! WE<14> 69 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=7630 $D=97
M84 70 CLK 54 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=10970 $D=97
M85 VSS! WE<13> 70 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=11450 $D=97
M86 71 CLK 55 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=14790 $D=97
M87 VSS! WE<12> 71 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=15270 $D=97
M88 72 CLK 56 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=18610 $D=97
M89 VSS! WE<11> 72 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=19090 $D=97
M90 73 CLK 57 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=22430 $D=97
M91 VSS! WE<10> 73 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=22910 $D=97
M92 74 CLK 58 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=26250 $D=97
M93 VSS! WE<9> 74 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=26730 $D=97
M94 75 CLK 59 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=30070 $D=97
M95 VSS! WE<8> 75 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=30550 $D=97
M96 76 CLK 60 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=33890 $D=97
M97 VSS! WE<7> 76 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=34370 $D=97
M98 77 CLK 61 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=37710 $D=97
M99 VSS! WE<6> 77 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=38190 $D=97
M100 78 CLK 62 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=41530 $D=97
M101 VSS! WE<5> 78 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=42010 $D=97
M102 79 CLK 63 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=45350 $D=97
M103 VSS! WE<4> 79 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=45830 $D=97
M104 80 CLK 64 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=49170 $D=97
M105 VSS! WE<3> 80 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=49650 $D=97
M106 81 CLK 65 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=52990 $D=97
M107 VSS! WE<2> 81 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=53470 $D=97
M108 82 CLK 66 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=56810 $D=97
M109 VSS! WE<1> 82 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=57290 $D=97
M110 83 CLK 67 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=60630 $D=97
M111 VSS! WE<0> 83 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=61110 $D=97
M112 WE_CLK_B<15> WE_CLK<15> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=470 $D=189
M113 VDD! WE_CLK<15> WE_CLK_B<15> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=950 $D=189
M114 WE_CLK_B<15> WE_CLK<15> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=1430 $D=189
M115 VDD! WE_CLK<15> WE_CLK_B<15> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=1910 $D=189
M116 WE_CLK<15> 52 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=2390 $D=189
M117 52 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=3330 $D=189
M118 VDD! WE<15> 52 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=3810 $D=189
M119 WE_CLK_B<14> WE_CLK<14> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=4290 $D=189
M120 VDD! WE_CLK<14> WE_CLK_B<14> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=4770 $D=189
M121 WE_CLK_B<14> WE_CLK<14> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=5250 $D=189
M122 VDD! WE_CLK<14> WE_CLK_B<14> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=5730 $D=189
M123 WE_CLK<14> 53 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=6210 $D=189
M124 53 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=7150 $D=189
M125 VDD! WE<14> 53 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=7630 $D=189
M126 WE_CLK_B<13> WE_CLK<13> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=8110 $D=189
M127 VDD! WE_CLK<13> WE_CLK_B<13> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=8590 $D=189
M128 WE_CLK_B<13> WE_CLK<13> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=9070 $D=189
M129 VDD! WE_CLK<13> WE_CLK_B<13> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=9550 $D=189
M130 WE_CLK<13> 54 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=10030 $D=189
M131 54 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=10970 $D=189
M132 VDD! WE<13> 54 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=11450 $D=189
M133 WE_CLK_B<12> WE_CLK<12> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=11930 $D=189
M134 VDD! WE_CLK<12> WE_CLK_B<12> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=12410 $D=189
M135 WE_CLK_B<12> WE_CLK<12> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=12890 $D=189
M136 VDD! WE_CLK<12> WE_CLK_B<12> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=13370 $D=189
M137 WE_CLK<12> 55 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=13850 $D=189
M138 55 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=14790 $D=189
M139 VDD! WE<12> 55 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=15270 $D=189
M140 WE_CLK_B<11> WE_CLK<11> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=15750 $D=189
M141 VDD! WE_CLK<11> WE_CLK_B<11> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=16230 $D=189
M142 WE_CLK_B<11> WE_CLK<11> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=16710 $D=189
M143 VDD! WE_CLK<11> WE_CLK_B<11> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=17190 $D=189
M144 WE_CLK<11> 56 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=17670 $D=189
M145 56 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=18610 $D=189
M146 VDD! WE<11> 56 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=19090 $D=189
M147 WE_CLK_B<10> WE_CLK<10> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=19570 $D=189
M148 VDD! WE_CLK<10> WE_CLK_B<10> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=20050 $D=189
M149 WE_CLK_B<10> WE_CLK<10> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=20530 $D=189
M150 VDD! WE_CLK<10> WE_CLK_B<10> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=21010 $D=189
M151 WE_CLK<10> 57 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=21490 $D=189
M152 57 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=22430 $D=189
M153 VDD! WE<10> 57 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=22910 $D=189
M154 WE_CLK_B<9> WE_CLK<9> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=23390 $D=189
M155 VDD! WE_CLK<9> WE_CLK_B<9> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=23870 $D=189
M156 WE_CLK_B<9> WE_CLK<9> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=24350 $D=189
M157 VDD! WE_CLK<9> WE_CLK_B<9> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=24830 $D=189
M158 WE_CLK<9> 58 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=25310 $D=189
M159 58 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=26250 $D=189
M160 VDD! WE<9> 58 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=26730 $D=189
M161 WE_CLK_B<8> WE_CLK<8> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=27210 $D=189
M162 VDD! WE_CLK<8> WE_CLK_B<8> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=27690 $D=189
M163 WE_CLK_B<8> WE_CLK<8> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=28170 $D=189
M164 VDD! WE_CLK<8> WE_CLK_B<8> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=28650 $D=189
M165 WE_CLK<8> 59 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=29130 $D=189
M166 59 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=30070 $D=189
M167 VDD! WE<8> 59 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=30550 $D=189
M168 WE_CLK_B<7> WE_CLK<7> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=31030 $D=189
M169 VDD! WE_CLK<7> WE_CLK_B<7> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=31510 $D=189
M170 WE_CLK_B<7> WE_CLK<7> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=31990 $D=189
M171 VDD! WE_CLK<7> WE_CLK_B<7> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=32470 $D=189
M172 WE_CLK<7> 60 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=32950 $D=189
M173 60 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=33890 $D=189
M174 VDD! WE<7> 60 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=34370 $D=189
M175 WE_CLK_B<6> WE_CLK<6> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=34850 $D=189
M176 VDD! WE_CLK<6> WE_CLK_B<6> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=35330 $D=189
M177 WE_CLK_B<6> WE_CLK<6> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=35810 $D=189
M178 VDD! WE_CLK<6> WE_CLK_B<6> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=36290 $D=189
M179 WE_CLK<6> 61 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=36770 $D=189
M180 61 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=37710 $D=189
M181 VDD! WE<6> 61 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=38190 $D=189
M182 WE_CLK_B<5> WE_CLK<5> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=38670 $D=189
M183 VDD! WE_CLK<5> WE_CLK_B<5> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=39150 $D=189
M184 WE_CLK_B<5> WE_CLK<5> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=39630 $D=189
M185 VDD! WE_CLK<5> WE_CLK_B<5> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=40110 $D=189
M186 WE_CLK<5> 62 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=40590 $D=189
M187 62 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=41530 $D=189
M188 VDD! WE<5> 62 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=42010 $D=189
M189 WE_CLK_B<4> WE_CLK<4> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=42490 $D=189
M190 VDD! WE_CLK<4> WE_CLK_B<4> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=42970 $D=189
M191 WE_CLK_B<4> WE_CLK<4> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=43450 $D=189
M192 VDD! WE_CLK<4> WE_CLK_B<4> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=43930 $D=189
M193 WE_CLK<4> 63 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=44410 $D=189
M194 63 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=45350 $D=189
M195 VDD! WE<4> 63 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=45830 $D=189
M196 WE_CLK_B<3> WE_CLK<3> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=46310 $D=189
M197 VDD! WE_CLK<3> WE_CLK_B<3> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=46790 $D=189
M198 WE_CLK_B<3> WE_CLK<3> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=47270 $D=189
M199 VDD! WE_CLK<3> WE_CLK_B<3> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=47750 $D=189
M200 WE_CLK<3> 64 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=48230 $D=189
M201 64 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=49170 $D=189
M202 VDD! WE<3> 64 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=49650 $D=189
M203 WE_CLK_B<2> WE_CLK<2> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=50130 $D=189
M204 VDD! WE_CLK<2> WE_CLK_B<2> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=50610 $D=189
M205 WE_CLK_B<2> WE_CLK<2> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=51090 $D=189
M206 VDD! WE_CLK<2> WE_CLK_B<2> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=51570 $D=189
M207 WE_CLK<2> 65 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=52050 $D=189
M208 65 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=52990 $D=189
M209 VDD! WE<2> 65 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=53470 $D=189
M210 WE_CLK_B<1> WE_CLK<1> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=53950 $D=189
M211 VDD! WE_CLK<1> WE_CLK_B<1> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=54430 $D=189
M212 WE_CLK_B<1> WE_CLK<1> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=54910 $D=189
M213 VDD! WE_CLK<1> WE_CLK_B<1> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=55390 $D=189
M214 WE_CLK<1> 66 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=55870 $D=189
M215 66 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=56810 $D=189
M216 VDD! WE<1> 66 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=57290 $D=189
M217 WE_CLK_B<0> WE_CLK<0> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=57770 $D=189
M218 VDD! WE_CLK<0> WE_CLK_B<0> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=58250 $D=189
M219 WE_CLK_B<0> WE_CLK<0> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=58730 $D=189
M220 VDD! WE_CLK<0> WE_CLK_B<0> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=59210 $D=189
M221 WE_CLK<0> 67 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=59690 $D=189
M222 67 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=60630 $D=189
M223 VDD! WE<0> 67 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=61110 $D=189
.ENDS
***************************************
.SUBCKT RF_slave_1 we_clkb READ_B we_clk READ_A S QA QB VSS! VDD!
** N=95 EP=9 IP=0 FDC=20
M0 16 11 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1440 $D=97
M1 12 we_clkb 16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1890 $D=97
M2 13 we_clk 12 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3000 $D=97
M3 VSS! S 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3480 $D=97
M4 10 12 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3960 $D=97
M5 10 READ_B QB VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=3120 $D=97
M6 QA READ_A 10 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=3600 $D=97
M7 VSS! 12 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=1620 $D=97
M8 14 READ_B VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=2140 $D=97
M9 VSS! READ_A 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=4560 $D=97
M10 17 11 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=1440 $D=189
M11 12 we_clk 17 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=1890 $D=189
M12 13 we_clkb 12 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3000 $D=189
M13 VDD! S 13 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3480 $D=189
M14 10 12 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3960 $D=189
M15 VDD! 12 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1620 $D=189
M16 14 READ_B VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2140 $D=189
M17 10 14 QB VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3120 $D=189
M18 QA 15 10 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3600 $D=189
M19 VDD! READ_A 15 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=4560 $D=189
.ENDS
***************************************
.SUBCKT ICV_5 1 2 3 4 5 6 7 8 9 10 11 12
** N=12 EP=12 IP=18 FDC=40
X0 1 2 3 4 5 6 7 11 12 RF_slave_1 $T=0 0 0 0 $X=-320 $Y=0
X1 1 2 3 4 8 9 10 11 12 RF_slave_1 $T=6000 0 0 0 $X=5680 $Y=0
.ENDS
***************************************
.SUBCKT ICV_6 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
** N=18 EP=18 IP=24 FDC=80
X0 1 2 3 4 5 6 7 8 9 10 17 18 ICV_5 $T=0 0 0 0 $X=-320 $Y=0
X1 1 2 3 4 11 12 13 14 15 16 17 18 ICV_5 $T=12000 0 0 0 $X=11680 $Y=0
.ENDS
***************************************
.SUBCKT RF_WD_1 WE_CLKB READ_B WE_CLK READ_A S<0> QA<0> QB<0> S<1> QA<1> QB<1> S<2> QA<2> QB<2> S<3> QA<3> QB<3> S<4> QA<4> QB<4> S<5>
+ QA<5> QB<5> S<6> QA<6> QB<6> S<7> QA<7> QB<7> S<8> QA<8> QB<8> S<9> QA<9> QB<9> S<10> QA<10> QB<10> S<11> QA<11> QB<11>
+ S<12> QA<12> QB<12> S<13> QA<13> QB<13> S<14> QA<14> QB<14> S<15> QA<15> QB<15> VSS! VDD!
** N=54 EP=54 IP=72 FDC=320
X0 WE_CLKB READ_B WE_CLK READ_A S<0> QA<0> QB<0> S<1> QA<1> QB<1> S<2> QA<2> QB<2> S<3> QA<3> QB<3> VSS! VDD! ICV_6 $T=0 0 0 0 $X=-320 $Y=0
X1 WE_CLKB READ_B WE_CLK READ_A S<4> QA<4> QB<4> S<5> QA<5> QB<5> S<6> QA<6> QB<6> S<7> QA<7> QB<7> VSS! VDD! ICV_6 $T=24000 0 0 0 $X=23680 $Y=0
X2 WE_CLKB READ_B WE_CLK READ_A S<8> QA<8> QB<8> S<9> QA<9> QB<9> S<10> QA<10> QB<10> S<11> QA<11> QB<11> VSS! VDD! ICV_6 $T=48000 0 0 0 $X=47680 $Y=0
X3 WE_CLKB READ_B WE_CLK READ_A S<12> QA<12> QB<12> S<13> QA<13> QB<13> S<14> QA<14> QB<14> S<15> QA<15> QB<15> VSS! VDD! ICV_6 $T=72000 0 0 0 $X=71680 $Y=0
.ENDS
***************************************
.SUBCKT RF_slave READ_B we_clkb we_clk READ_A S QA QB VSS! VDD!
** N=94 EP=9 IP=0 FDC=20
M0 VSS! 11 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=960 $D=97
M1 16 12 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1440 $D=97
M2 11 we_clkb 16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1890 $D=97
M3 13 we_clk 11 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3000 $D=97
M4 VSS! S 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3480 $D=97
M5 10 11 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3960 $D=97
M6 10 READ_B QB VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=2520 $D=97
M7 QA READ_A 10 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=3000 $D=97
M8 14 READ_B VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=1540 $D=97
M9 VSS! READ_A 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=3960 $D=97
M10 VDD! 11 12 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=960 $D=189
M11 17 12 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=1440 $D=189
M12 11 we_clk 17 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=1890 $D=189
M13 13 we_clkb 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3000 $D=189
M14 VDD! S 13 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3480 $D=189
M15 10 11 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3960 $D=189
M16 14 READ_B VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1540 $D=189
M17 10 14 QB VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2520 $D=189
M18 QA 15 10 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3000 $D=189
M19 VDD! READ_A 15 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3960 $D=189
.ENDS
***************************************
.SUBCKT ICV_7 1 2 3 4 5 6 7 8 9 10 11 12
** N=12 EP=12 IP=18 FDC=40
X0 1 2 3 4 5 6 7 11 12 RF_slave $T=0 0 0 0 $X=-320 $Y=0
X1 1 2 3 4 8 9 10 11 12 RF_slave $T=6000 0 0 0 $X=5680 $Y=0
.ENDS
***************************************
.SUBCKT ICV_8 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
** N=18 EP=18 IP=24 FDC=80
X0 1 2 3 4 5 6 7 8 9 10 17 18 ICV_7 $T=0 0 0 0 $X=-320 $Y=0
X1 1 2 3 4 11 12 13 14 15 16 17 18 ICV_7 $T=12000 0 0 0 $X=11680 $Y=0
.ENDS
***************************************
.SUBCKT RF_WD READ_B WE_CLKB WE_CLK READ_A S<0> QA<0> QB<0> S<1> QA<1> QB<1> S<2> QA<2> QB<2> S<3> QA<3> QB<3> S<4> QA<4> QB<4> S<5>
+ QA<5> QB<5> S<6> QA<6> QB<6> S<7> QA<7> QB<7> S<8> QA<8> QB<8> S<9> QA<9> QB<9> S<10> QA<10> QB<10> S<11> QA<11> QB<11>
+ S<12> QA<12> QB<12> S<13> QA<13> QB<13> S<14> QA<14> QB<14> S<15> QA<15> QB<15> VSS! VDD!
** N=54 EP=54 IP=72 FDC=320
X0 READ_B WE_CLKB WE_CLK READ_A S<0> QA<0> QB<0> S<1> QA<1> QB<1> S<2> QA<2> QB<2> S<3> QA<3> QB<3> VSS! VDD! ICV_8 $T=0 0 0 0 $X=-320 $Y=0
X1 READ_B WE_CLKB WE_CLK READ_A S<4> QA<4> QB<4> S<5> QA<5> QB<5> S<6> QA<6> QB<6> S<7> QA<7> QB<7> VSS! VDD! ICV_8 $T=24000 0 0 0 $X=23680 $Y=0
X2 READ_B WE_CLKB WE_CLK READ_A S<8> QA<8> QB<8> S<9> QA<9> QB<9> S<10> QA<10> QB<10> S<11> QA<11> QB<11> VSS! VDD! ICV_8 $T=48000 0 0 0 $X=47680 $Y=0
X3 READ_B WE_CLKB WE_CLK READ_A S<12> QA<12> QB<12> S<13> QA<13> QB<13> S<14> QA<14> QB<14> S<15> QA<15> QB<15> VSS! VDD! ICV_8 $T=72000 0 0 0 $X=71680 $Y=0
.ENDS
***************************************
.SUBCKT ICV_9 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58
** N=58 EP=58 IP=108 FDC=640
X0 1 3 2 4 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
+ 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44
+ 45 46 47 48 49 50 51 52 53 54 55 56 57 58
+ RF_WD_1 $T=0 0 0 0 $X=-320 $Y=0
X1 7 5 6 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
+ 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44
+ 45 46 47 48 49 50 51 52 53 54 55 56 57 58
+ RF_WD $T=0 4000 0 0 $X=-320 $Y=4000
.ENDS
***************************************
.SUBCKT RF_master WE_MASTER_B WE_MASTER D MASTER_DATA VSS! VDD!
** N=92 EP=6 IP=0 FDC=24
M0 VSS! 7 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=5290 $D=97
M1 9 D VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=850 $Y=430 $D=97
M2 10 8 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=5220 $D=97
M3 7 WE_MASTER 9 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1360 $Y=430 $D=97
M4 VSS! 8 10 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=5220 $D=97
M5 MASTER_DATA 10 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1940 $Y=5130 $D=97
M6 12 WE_MASTER_B 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2140 $Y=430 $D=97
M7 VSS! 10 MASTER_DATA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2420 $Y=5130 $D=97
M8 VSS! 11 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2560 $Y=430 $D=97
M9 MASTER_DATA 10 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2900 $Y=5130 $D=97
M10 11 7 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3040 $Y=430 $D=97
M11 VSS! 10 MASTER_DATA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3380 $Y=5130 $D=97
M12 VDD! 7 8 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=3510 $D=189
M13 9 D VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=850 $Y=1950 $D=189
M14 10 8 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=3510 $D=189
M15 VDD! 8 10 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=3510 $D=189
M16 7 WE_MASTER_B 9 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1600 $Y=1950 $D=189
M17 MASTER_DATA 10 VDD! VDD! pfet L=1.2e-07 W=8.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1940 $Y=3510 $D=189
M18 13 WE_MASTER 7 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2140 $Y=1950 $D=189
M19 VDD! 10 MASTER_DATA VDD! pfet L=1.2e-07 W=8.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2420 $Y=3510 $D=189
M20 VDD! 11 13 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2560 $Y=1950 $D=189
M21 MASTER_DATA 10 VDD! VDD! pfet L=1.2e-07 W=8.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2900 $Y=3510 $D=189
M22 11 7 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3040 $Y=1950 $D=189
M23 VDD! 10 MASTER_DATA VDD! pfet L=1.2e-07 W=8.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3380 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT ICV_4 1 2 3 4 5 6 7 8
** N=8 EP=8 IP=12 FDC=48
X0 1 2 3 4 8 7 RF_master $T=0 -6000 0 0 $X=-200 $Y=-6300
X1 1 2 5 6 8 7 RF_master $T=0 0 0 0 $X=-200 $Y=-300
.ENDS
***************************************
.SUBCKT RF_BUFFER BUFF_A QA BUFF_B QB VSS! VDD!
** N=76 EP=6 IP=0 FDC=16
M0 VSS! 8 QA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1010 $D=97
M1 QA 8 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1490 $D=97
M2 VSS! 8 QA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1970 $D=97
M3 VSS! BUFF_A 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2960 $D=97
M4 VSS! 9 QB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=780 $D=97
M5 QB 9 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=1260 $D=97
M6 VSS! 9 QB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=1740 $D=97
M7 VSS! BUFF_B 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=2680 $D=97
M8 VDD! 8 QA VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1490 $Y=1010 $D=189
M9 QA 8 VDD! VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1490 $Y=1490 $D=189
M10 VDD! 8 QA VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1490 $Y=1970 $D=189
M11 VDD! BUFF_A 8 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1850 $Y=2960 $D=189
M12 VDD! 9 QB VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=780 $D=189
M13 QB 9 VDD! VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1260 $D=189
M14 VDD! 9 QB VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1740 $D=189
M15 VDD! BUFF_B 9 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2680 $D=189
.ENDS
***************************************
.SUBCKT ICV_3 1 2 3 4 5 6 7 8 9 10
** N=12 EP=10 IP=14 FDC=32
X0 1 2 3 4 9 10 RF_BUFFER $T=0 0 0 0 $X=-240 $Y=0
X1 5 6 7 8 9 10 RF_BUFFER $T=6000 0 0 0 $X=5760 $Y=0
.ENDS
***************************************
.SUBCKT RF WE_MASTER 2 READ_B<15> READ_A<15> READ_B<14> READ_A<14> READ_B<13> READ_A<13> READ_B<12> READ_A<12> READ_B<11> READ_A<11> READ_B<10> READ_A<10> READ_B<9> READ_A<9> READ_B<8> READ_A<8> READ_B<7> READ_A<7>
+ READ_B<6> READ_A<6> READ_B<5> READ_A<5> READ_B<4> READ_A<4> READ_B<3> READ_A<3> READ_B<2> READ_A<2> READ_B<1> READ_A<1> READ_B<0> READ_A<0> CLK D<1> D<0> D<3> D<2> D<5>
+ D<4> D<7> D<6> D<9> D<8> D<11> D<10> D<13> D<12> D<15> D<14> QA<0> QB<0> QA<1> QB<1> QA<2> QB<2> QA<3> QB<3> QA<4>
+ QB<4> QA<5> QB<5> QA<6> QB<6> QA<7> QB<7> QA<8> QB<8> QA<9> QB<9> QA<10> QB<10> QA<11> QB<11> QA<12> QB<12> QA<13> QB<13> QA<14>
+ QB<14> QA<15> QB<15> VDD! VSS! WE<15> WE<14> WE<13> WE<12> WE<11> WE<10> WE<9> WE<8> WE<7> WE<6> WE<5> WE<4> WE<3> WE<2> WE<1>
+ WE<0>
** N=273 EP=101 IP=675 FDC=5998
M0 134 182 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=63990 $D=97
M1 VSS! 182 134 VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=64470 $D=97
M2 134 182 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=64950 $D=97
M3 VSS! 182 134 VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=65430 $D=97
M4 182 183 VSS! VSS! nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4190 $Y=65970 $D=97
M5 VSS! 183 182 VSS! nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4190 $Y=66450 $D=97
M6 183 WE_MASTER VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4470 $Y=66990 $D=97
M7 134 182 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=63990 $D=189
M8 VDD! 182 134 VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=64470 $D=189
M9 134 182 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=64950 $D=189
M10 VDD! 182 134 VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=65430 $D=189
M11 182 183 VDD! VDD! pfet L=1.2e-07 W=1.08e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=65970 $D=189
M12 VDD! 183 182 VDD! pfet L=1.2e-07 W=1.08e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=66450 $D=189
M13 183 WE_MASTER VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=66990 $D=189
X14 CLK VDD! WE<15> WE<14> WE<13> WE<12> WE<11> WE<10> WE<9> WE<8> WE<7> WE<6> WE<5> WE<4> WE<3> WE<2> WE<1> WE<0> 102 104
+ 106 108 110 112 114 116 118 120 122 124 126 128 129 130 103 105 107 109 111 113
+ 115 117 119 121 123 125 127 131 132 133 VSS!
+ RF_SCLK $T=0 2400 0 0 $X=-240 $Y=2200
X15 102 103 READ_B<15> READ_A<15> 104 105 READ_B<14> READ_A<14> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 2000 0 0 $X=4880 $Y=2000
X16 106 107 READ_B<13> READ_A<13> 108 109 READ_B<12> READ_A<12> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 9600 0 0 $X=4880 $Y=9600
X17 110 111 READ_B<11> READ_A<11> 112 113 READ_B<10> READ_A<10> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 17200 0 0 $X=4880 $Y=17200
X18 114 115 READ_B<9> READ_A<9> 116 117 READ_B<8> READ_A<8> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 24800 0 0 $X=4880 $Y=24800
X19 118 119 READ_B<7> READ_A<7> 120 121 READ_B<6> READ_A<6> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 32400 0 0 $X=4880 $Y=32400
X20 122 123 READ_B<5> READ_A<5> 124 125 READ_B<4> READ_A<4> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 40000 0 0 $X=4880 $Y=40000
X21 126 127 READ_B<3> READ_A<3> 128 131 READ_B<2> READ_A<2> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 47600 0 0 $X=4880 $Y=47600
X22 129 132 READ_B<1> READ_A<1> 130 133 READ_B<0> READ_A<0> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 55200 0 0 $X=4880 $Y=55200
X23 134 WE_MASTER D<1> 152 D<0> 150 VDD! VSS! ICV_4 $T=11200 67600 1 270 $X=4900 $Y=63200
X24 134 WE_MASTER D<3> 156 D<2> 154 VDD! VSS! ICV_4 $T=23200 67600 1 270 $X=16900 $Y=63200
X25 134 WE_MASTER D<5> 160 D<4> 158 VDD! VSS! ICV_4 $T=35200 67600 1 270 $X=28900 $Y=63200
X26 134 WE_MASTER D<7> 164 D<6> 162 VDD! VSS! ICV_4 $T=47200 67600 1 270 $X=40900 $Y=63200
X27 134 WE_MASTER D<9> 168 D<8> 166 VDD! VSS! ICV_4 $T=59200 67600 1 270 $X=52900 $Y=63200
X28 134 WE_MASTER D<11> 172 D<10> 170 VDD! VSS! ICV_4 $T=71200 67600 1 270 $X=64900 $Y=63200
X29 134 WE_MASTER D<13> 176 D<12> 174 VDD! VSS! ICV_4 $T=83200 67600 1 270 $X=76900 $Y=63200
X30 134 WE_MASTER D<15> 180 D<14> 178 VDD! VSS! ICV_4 $T=95200 67600 1 270 $X=88900 $Y=63200
X31 135 QA<0> 151 QB<0> 2 QA<1> 153 QB<1> VSS! VDD! ICV_3 $T=5200 0 0 0 $X=4960 $Y=0
X32 136 QA<2> 155 QB<2> 137 QA<3> 157 QB<3> VSS! VDD! ICV_3 $T=17200 0 0 0 $X=16960 $Y=0
X33 138 QA<4> 159 QB<4> 139 QA<5> 161 QB<5> VSS! VDD! ICV_3 $T=29200 0 0 0 $X=28960 $Y=0
X34 140 QA<6> 163 QB<6> 141 QA<7> 165 QB<7> VSS! VDD! ICV_3 $T=41200 0 0 0 $X=40960 $Y=0
X35 142 QA<8> 167 QB<8> 143 QA<9> 169 QB<9> VSS! VDD! ICV_3 $T=53200 0 0 0 $X=52960 $Y=0
X36 144 QA<10> 171 QB<10> 145 QA<11> 173 QB<11> VSS! VDD! ICV_3 $T=65200 0 0 0 $X=64960 $Y=0
X37 146 QA<12> 175 QB<12> 147 QA<13> 177 QB<13> VSS! VDD! ICV_3 $T=77200 0 0 0 $X=76960 $Y=0
X38 148 QA<14> 179 QB<14> 149 QA<15> 181 QB<15> VSS! VDD! ICV_3 $T=89200 0 0 0 $X=88960 $Y=0
.ENDS
***************************************
.SUBCKT MUX_Rsrc_1bit SEL<1> SEL_BAR<1> SEL_BAR<0> SEL<0> ALU_B Imm_16 Rsrc VSS! VDD!
** N=85 EP=9 IP=0 FDC=18
M0 12 SEL<1> 10 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=540 $D=97
M1 11 SEL_BAR<1> 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1100 $D=97
M2 ALU_B 12 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2240 $D=97
M3 VSS! 12 ALU_B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2800 $D=97
M4 ALU_B 12 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3360 $D=97
M5 VSS! 12 ALU_B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3900 $D=97
M6 VSS! 10 11 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=2340 $D=97
M7 10 SEL<0> Imm_16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5250 $Y=540 $D=97
M8 Rsrc SEL_BAR<0> 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5250 $Y=1100 $D=97
M9 ALU_B 12 VDD! VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=2240 $D=189
M10 VDD! 12 ALU_B VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=2800 $D=189
M11 ALU_B 12 VDD! VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=3360 $D=189
M12 VDD! 12 ALU_B VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=3900 $D=189
M13 12 SEL_BAR<1> 10 VDD! pfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2050 $Y=540 $D=189
M14 11 SEL<1> 12 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1100 $D=189
M15 10 SEL_BAR<0> Imm_16 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=540 $D=189
M16 Rsrc SEL<0> 10 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=1100 $D=189
M17 VDD! 10 11 VDD! pfet L=1.2e-07 W=6.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=2340 $D=189
.ENDS
***************************************
.SUBCKT MUX_Rdest_1bit ALU_A Rdest SEL SEL_BAR VSS! VDD!
** N=54 EP=6 IP=0 FDC=10
M0 7 SEL VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=970 $D=97
M1 Rdest SEL_BAR 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1730 $D=97
M2 VSS! 7 8 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=870 $D=97
M3 ALU_A 8 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1400 $D=97
M4 VSS! 8 ALU_A VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1880 $D=97
M5 7 SEL_BAR VSS! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=970 $D=189
M6 Rdest SEL 7 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1730 $D=189
M7 VDD! 7 8 VDD! pfet L=1.2e-07 W=7.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=870 $D=189
M8 ALU_A 8 VDD! VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1400 $D=189
M9 VDD! 8 ALU_A VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1880 $D=189
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
** N=18 EP=18 IP=30 FDC=56
X0 1 2 3 4 7 8 9 17 18 MUX_Rsrc_1bit $T=0 0 0 0 $X=-240 $Y=-160
X1 1 2 3 4 10 11 12 17 18 MUX_Rsrc_1bit $T=6000 0 0 0 $X=5760 $Y=-160
X2 5 13 14 15 17 18 MUX_Rdest_1bit $T=6000 4400 1 180 $X=-240 $Y=4380
X3 6 16 14 15 17 18 MUX_Rdest_1bit $T=12000 4400 1 180 $X=5760 $Y=4380
.ENDS
***************************************
.SUBCKT MUX_Shifter_1bit Rdest Shifter SEL Imm_16 SEL_BAR VSS! VDD!
** N=50 EP=7 IP=0 FDC=10
M0 8 SEL Imm_16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=570 $D=97
M1 Rdest SEL_BAR 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1330 $D=97
M2 VSS! 8 9 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=470 $D=97
M3 Shifter 9 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1000 $D=97
M4 VSS! 9 Shifter VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1480 $D=97
M5 8 SEL_BAR Imm_16 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=570 $D=189
M6 Rdest SEL 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1330 $D=189
M7 VDD! 8 9 VDD! pfet L=1.2e-07 W=7.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=470 $D=189
M8 Shifter 9 VDD! VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1000 $D=189
M9 VDD! 9 Shifter VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1480 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9 10
** N=10 EP=10 IP=14 FDC=20
X0 3 1 4 5 6 9 10 MUX_Shifter_1bit $T=-6000 0 0 0 $X=-6240 $Y=-300
X1 7 2 4 8 6 9 10 MUX_Shifter_1bit $T=0 0 0 0 $X=-240 $Y=-300
.ENDS
***************************************
.SUBCKT Datapath1 Rsrc<8> Rsrc<7> Rsrc<6> Rsrc<5> READ_B<12> Rsrc<4> READ_A<12> READ_B<2> D<14> LUI_SEL READ_B<4> READ_B<1> READ_A<14> READ_A<13> D<13> READ_B<13> READ_A<7> READ_B<6> READ_A<4> READ_B<5>
+ READ_B<11> READ_B<9> READ_A<8> D<15> READ_A<11> READ_A<10> READ_A<9> READ_B<8> READ_B<7> READ_A<5> READ_B<10> READ_A<6> READ_B<3> READ_A<3> READ_B<14> Rsrc<3> Rsrc<9> READ_B<15> READ_A<15> READ_A<2>
+ READ_B<0> Imm_8<4> D<12> READ_A<1> D<11> D<10> D<9> READ_A<0> Rsrc<15> Imm_8<7> Rsrc<14> Rsrc<13> VDD! VSS! Rsrc<2> Rsrc<1> Imm_8<0> CIN Rsrc<0> CLKB
+ Rsrc<10> Rsrc<11> Rsrc<12> CEN WEN N F OUT<4> OUT<3> Z OUT<2> ALU_SEL<0> OUT<1> WE<12> OUT<8> OUT<5> ALU_SEL<1> OUT<6> WE<5> OUT<11>
+ WE<8> WE<6> OUT<13> OUT<14> OUT<15> OUT<7> WE<13> OUT<0> WE<11> OUT<9> WE<10> OUT<10> WE<9> SEL_1 WE<7> OUT<12> WE<4> WE<2> WE<14> CLK
+ WE<3> D<0> D_SEL<1> D_SEL<0> Fun_I_SEL<1> Fun_I_SEL<0> MOV_SEL Amount<0> Amount<1> WE<15> WE<1> D<8> D<7> WE<0> D<6> D<5> D<4> D<3> WE_MASTER D<2>
+ D<1> Imm_8<2> Imm_8<6> Imm_8<5> Imm_8<3> Imm_8<1> COUT
** N=539 EP=127 IP=727 FDC=9854
M0 VSS! 242 243 VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=34040 $D=97
M1 243 242 VSS! VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=34600 $D=97
M2 VSS! 242 243 VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=35160 $D=97
M3 242 283 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=35720 $D=97
M4 VSS! 283 242 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=36280 $D=97
M5 283 Fun_I_SEL<1> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=36840 $D=97
M6 246 284 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130400 $Y=35720 $D=97
M7 VSS! 284 246 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130400 $Y=36280 $D=97
M8 VSS! 246 244 VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130420 $Y=34040 $D=97
M9 244 246 VSS! VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130420 $Y=34600 $D=97
M10 VSS! 246 244 VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130420 $Y=35160 $D=97
M11 284 Fun_I_SEL<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130470 $Y=36840 $D=97
M12 VSS! 281 280 VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=83450 $D=97
M13 280 281 VSS! VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=84010 $D=97
M14 VSS! 281 280 VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=84570 $D=97
M15 281 285 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=85130 $D=97
M16 VSS! 285 281 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=85690 $D=97
M17 285 SEL_1 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=86250 $D=97
M18 257 255 VSS! VSS! nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138680 $Y=39850 $D=97
M19 VSS! 255 257 VSS! nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138680 $Y=40330 $D=97
M20 258 256 VSS! VSS! nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138680 $Y=54250 $D=97
M21 VSS! 256 258 VSS! nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138680 $Y=54730 $D=97
M22 255 286 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138800 $Y=38850 $D=97
M23 VSS! 286 255 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138800 $Y=39330 $D=97
M24 256 287 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138800 $Y=53250 $D=97
M25 VSS! 287 256 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138800 $Y=53730 $D=97
M26 VSS! MOV_SEL 286 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138870 $Y=38320 $D=97
M27 VSS! LUI_SEL 287 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138870 $Y=52720 $D=97
M28 VDD! 242 243 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127030 $Y=34040 $D=189
M29 243 242 VDD! VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127030 $Y=34600 $D=189
M30 VDD! 242 243 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127030 $Y=35160 $D=189
M31 242 283 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127070 $Y=35720 $D=189
M32 VDD! 283 242 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127070 $Y=36280 $D=189
M33 283 Fun_I_SEL<1> VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127190 $Y=36840 $D=189
M34 VDD! 246 244 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=34040 $D=189
M35 244 246 VDD! VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=34600 $D=189
M36 VDD! 246 244 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=35160 $D=189
M37 246 284 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=35720 $D=189
M38 VDD! 284 246 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=36280 $D=189
M39 284 Fun_I_SEL<0> VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=36840 $D=189
M40 VDD! 281 280 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=83450 $D=189
M41 280 281 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=84010 $D=189
M42 VDD! 281 280 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=84570 $D=189
M43 281 285 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=85130 $D=189
M44 VDD! 285 281 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=85690 $D=189
M45 285 SEL_1 VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133210 $Y=86250 $D=189
M46 VDD! MOV_SEL 286 VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=38320 $D=189
M47 255 286 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=38850 $D=189
M48 VDD! 286 255 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=39330 $D=189
M49 257 255 VDD! VDD! pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=39850 $D=189
M50 VDD! 255 257 VDD! pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=40330 $D=189
M51 VDD! LUI_SEL 287 VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=52720 $D=189
M52 256 287 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=53250 $D=189
M53 VDD! 287 256 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=53730 $D=189
M54 258 256 VDD! VDD! pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=54250 $D=189
M55 VDD! 256 258 VDD! pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=54730 $D=189
X56 538 539 91 92 93 94 95 96 97 98 99 100 101 102 103 1 2 104 Rsrc<2> Rsrc<1>
+ Rsrc<0> CEN WEN CLKB 122 61 62 128 136 63 64 142 152 65 60 158 169 107 108 173
+ Rsrc<8> Rsrc<7> Rsrc<6> Rsrc<5> Rsrc<4> Rsrc<3>
+ RA1SH16x512 $T=200 134100 0 0 $X=198 $Y=134098
X57 OUT<4> 254 129 100 OUT<15> 87 88 108 OUT<13> 83 84 60 OUT<14> 85 86 107 OUT<0> 245 115 92
+ OUT<1> 247 118 93 OUT<2> 249 82 96 OUT<3> 252 125 97 OUT<5> 130 133 101 OUT<6> 134 138 1
+ OUT<7> 139 143 2 OUT<8> 145 148 61 OUT<9> 149 153 62 OUT<10> 154 159 63 OUT<11> 160 164 64
+ OUT<12> 165 170 65 D_SEL<1> D_SEL<0> VSS! VDD!
+ MUX_D $T=125200 7200 0 0 $X=124680 $Y=6770
X58 279 Amount<0> VDD! VSS! MUX_Shifter_SEL_BUF $T=125200 39600 0 270 $X=124950 $Y=37820
X59 278 Amount<1> VDD! VSS! MUX_Shifter_SEL_BUF $T=131200 39600 1 270 $X=127710 $Y=37820
X60 278 251 Imm_8<3> 279 Rsrc<3> VSS! VDD! VDD! MUX_Shifter_SEL_1bit $T=125200 39600 1 90 $X=124960 $Y=39170
X61 278 253 Imm_8<2> 279 Rsrc<2> VSS! VSS! VDD! MUX_Shifter_SEL_1bit $T=125200 44400 1 90 $X=124960 $Y=43970
X62 278 250 Imm_8<1> 279 Rsrc<1> VSS! VSS! VDD! MUX_Shifter_SEL_1bit $T=125200 49200 1 90 $X=124960 $Y=48770
X63 278 248 Imm_8<0> 279 Rsrc<0> VSS! VSS! VDD! MUX_Shifter_SEL_1bit $T=125200 54000 1 90 $X=124960 $Y=53570
X64 66 Imm_8<0> VSS! VDD! Sign_Ext_left $T=131200 38000 0 0 $X=130960 $Y=37820
X65 67 Imm_8<1> VSS! VDD! Sign_Ext_left $T=131200 39600 0 0 $X=130960 $Y=39420
X66 68 Imm_8<2> VSS! VDD! Sign_Ext_left $T=131200 41200 0 0 $X=130960 $Y=41020
X67 69 Imm_8<3> VSS! VDD! Sign_Ext_left $T=131200 42800 0 0 $X=130960 $Y=42620
X68 70 Imm_8<4> VSS! VDD! Sign_Ext_left $T=131200 44400 0 0 $X=130960 $Y=44220
X69 71 Imm_8<5> VSS! VDD! Sign_Ext_left $T=131200 46000 0 0 $X=130960 $Y=45820
X70 72 Imm_8<6> VSS! VDD! Sign_Ext_left $T=131200 47600 0 0 $X=130960 $Y=47420
X71 73 Imm_8<7> VSS! VDD! Sign_Ext_left $T=131200 49200 0 0 $X=130960 $Y=49020
X72 74 280 281 VSS! VDD! Sign_Ext_right $T=131200 50800 0 0 $X=130960 $Y=50500
X73 75 280 281 VSS! VDD! Sign_Ext_right $T=131200 54800 0 0 $X=130960 $Y=54500
X74 76 280 281 VSS! VDD! Sign_Ext_right $T=131200 58800 0 0 $X=130960 $Y=58500
X75 77 280 281 VSS! VDD! Sign_Ext_right $T=131200 62800 0 0 $X=130960 $Y=62500
X76 78 280 281 VSS! VDD! Sign_Ext_right $T=131200 66800 0 0 $X=130960 $Y=66500
X77 79 280 281 VSS! VDD! Sign_Ext_right $T=131200 70800 0 0 $X=130960 $Y=70500
X78 80 280 281 VSS! VDD! Sign_Ext_right $T=131200 74800 0 0 $X=130960 $Y=74500
X79 81 280 281 VSS! VDD! Sign_Ext_right $T=131200 78800 0 0 $X=130960 $Y=78500
X80 ALU_SEL<1> F N ALU_SEL<0> Z CIN 114 117 121 124 127 132 137 141 147 151 COUT 156 162 167
+ 172 175 178 113 116 120 123 126 131 135 140 146 150 155 161 166 171 174 177 245
+ 247 249 252 254 130 134 139 145 149 154 160 165 83 85 87 VDD! VSS!
+ ALU $T=134400 6400 0 0 $X=134000 $Y=6100
X81 275 261 262 263 264 265 266 267 268 269 270 271 272 273 274 251 253 250 248 276
+ 115 118 82 125 129 133 138 143 148 153 159 164 170 84 86 88 VDD! VSS!
+ SHIFTER $T=134400 40800 0 0 $X=134160 $Y=40600
X82 WE_MASTER 282 READ_B<15> READ_A<15> READ_B<14> READ_A<14> READ_B<13> READ_A<13> READ_B<12> READ_A<12> READ_B<11> READ_A<11> READ_B<10> READ_A<10> READ_B<9> READ_A<9> READ_B<8> READ_A<8> READ_B<7> READ_A<7>
+ READ_B<6> READ_A<6> READ_B<5> READ_A<5> READ_B<4> READ_A<4> READ_B<3> READ_A<3> READ_B<2> READ_A<2> READ_B<1> READ_A<1> READ_B<0> READ_A<0> CLK D<1> D<0> D<3> D<2> D<5>
+ D<4> D<7> D<6> D<9> D<8> D<11> D<10> D<13> D<12> D<15> D<14> Rsrc<0> 91 Rsrc<1> 94 Rsrc<2> 95 Rsrc<3> 98 Rsrc<4>
+ 99 Rsrc<5> 102 Rsrc<6> 103 Rsrc<7> 104 Rsrc<8> 122 Rsrc<9> 128 Rsrc<10> 136 Rsrc<11> 142 Rsrc<12> 152 Rsrc<13> 158 Rsrc<14>
+ 169 Rsrc<15> 173 VDD! VSS! WE<15> WE<14> WE<13> WE<12> WE<11> WE<10> WE<9> WE<8> WE<7> WE<6> WE<5> WE<4> WE<3> WE<2> WE<1>
+ WE<0>
+ RF $T=134400 54800 0 0 $X=134160 $Y=54800
X83 242 243 244 246 113 116 114 66 Rsrc<0> 117 67 Rsrc<1> 91 255 257 94 VSS! VDD! ICV_2 $T=139600 33600 0 0 $X=139360 $Y=33440
X84 242 243 244 246 120 123 121 68 Rsrc<2> 124 69 Rsrc<3> 95 255 257 98 VSS! VDD! ICV_2 $T=151600 33600 0 0 $X=151360 $Y=33440
X85 242 243 244 246 126 131 127 70 Rsrc<4> 132 71 Rsrc<5> 99 255 257 102 VSS! VDD! ICV_2 $T=163600 33600 0 0 $X=163360 $Y=33440
X86 242 243 244 246 135 140 137 72 Rsrc<6> 141 73 Rsrc<7> 103 255 257 104 VSS! VDD! ICV_2 $T=175600 33600 0 0 $X=175360 $Y=33440
X87 242 243 244 246 146 150 147 74 Rsrc<8> 151 75 Rsrc<9> 122 255 257 128 VSS! VDD! ICV_2 $T=187600 33600 0 0 $X=187360 $Y=33440
X88 242 243 244 246 155 161 156 76 Rsrc<10> 162 77 Rsrc<11> 136 255 257 142 VSS! VDD! ICV_2 $T=199600 33600 0 0 $X=199360 $Y=33440
X89 242 243 244 246 166 171 167 78 Rsrc<12> 172 79 Rsrc<13> 152 255 257 158 VSS! VDD! ICV_2 $T=211600 33600 0 0 $X=211360 $Y=33440
X90 242 243 244 246 174 177 175 80 Rsrc<14> 178 81 Rsrc<15> 169 255 257 173 VSS! VDD! ICV_2 $T=223600 33600 0 0 $X=223360 $Y=33440
X91 261 275 94 256 67 258 91 66 VSS! VDD! ICV_1 $T=145600 52800 1 180 $X=139360 $Y=52500
X92 263 262 98 256 69 258 95 68 VSS! VDD! ICV_1 $T=157600 52800 1 180 $X=151360 $Y=52500
X93 265 264 102 256 71 258 99 70 VSS! VDD! ICV_1 $T=169600 52800 1 180 $X=163360 $Y=52500
X94 267 266 104 256 73 258 103 72 VSS! VDD! ICV_1 $T=181600 52800 1 180 $X=175360 $Y=52500
X95 269 268 128 256 75 258 122 74 VSS! VDD! ICV_1 $T=193600 52800 1 180 $X=187360 $Y=52500
X96 271 270 142 256 77 258 136 76 VSS! VDD! ICV_1 $T=205600 52800 1 180 $X=199360 $Y=52500
X97 273 272 158 256 79 258 152 78 VSS! VDD! ICV_1 $T=217600 52800 1 180 $X=211360 $Y=52500
X98 276 274 173 256 81 258 169 80 VSS! VDD! ICV_1 $T=229600 52800 1 180 $X=223360 $Y=52500
.ENDS
***************************************
