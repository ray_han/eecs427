* SPICE NETLIST
***************************************

*.CALIBRE ABORT_INFO SUPPLY_ERROR
.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT decoder_1st 1 2 3 4 5 6 7
** N=39 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4870 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT decoder_2st 1 2 3 4 5 6 7
** N=37 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4590 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=1730 $D=189
.ENDS
***************************************
.SUBCKT decoder_3rd 1 2 3 4 5 6 7
** N=67 EP=7 IP=0 FDC=8
M0 8 1 4 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=450 $D=97
M1 8 2 5 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=4870 $D=97
M2 3 8 6 6 nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=450 $D=97
M3 3 8 6 6 nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=4550 $D=97
M4 8 1 5 7 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=3310 $D=189
M5 8 2 4 7 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=2010 $D=189
M6 3 8 7 7 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=1690 $D=189
M7 3 8 7 7 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT DECODER_3 EN SH0_BAR SH<0> SH1_BAR SH<1> SH2_BAR SH<2> D<7> D<6> D<5> D<4> D<3> D<2> D<1> D<0> VSS! VDD!
** N=32 EP=17 IP=98 FDC=88
X0 SH0_BAR SH<0> 4 VSS! VDD! VSS! EN decoder_1st $T=0 0 0 0 $X=-300 $Y=-240
X1 SH0_BAR SH<0> 5 VSS! VDD! EN VSS! decoder_1st $T=0 5600 0 0 $X=-300 $Y=5360
X2 SH1_BAR SH<1> 8 VSS! VDD! VSS! 4 decoder_2st $T=1600 -11200 0 0 $X=1300 $Y=-11440
X3 SH1_BAR SH<1> 9 VSS! VDD! VSS! 5 decoder_2st $T=1600 -5600 0 0 $X=1300 $Y=-5840
X4 SH1_BAR SH<1> 10 VSS! VDD! 4 VSS! decoder_2st $T=1600 0 0 0 $X=1300 $Y=-240
X5 SH1_BAR SH<1> 11 VSS! VDD! 5 VSS! decoder_2st $T=1600 5600 0 0 $X=1300 $Y=5360
X6 SH2_BAR SH<2> D<7> VSS! 8 VSS! VDD! decoder_3rd $T=4000 -33600 0 0 $X=3700 $Y=-33840
X7 SH2_BAR SH<2> D<6> VSS! 9 VSS! VDD! decoder_3rd $T=4000 -28000 0 0 $X=3700 $Y=-28240
X8 SH2_BAR SH<2> D<5> VSS! 10 VSS! VDD! decoder_3rd $T=4000 -22400 0 0 $X=3700 $Y=-22640
X9 SH2_BAR SH<2> D<4> VSS! 11 VSS! VDD! decoder_3rd $T=4000 -16800 0 0 $X=3700 $Y=-17040
X10 SH2_BAR SH<2> D<3> 8 VSS! VSS! VDD! decoder_3rd $T=4000 -11200 0 0 $X=3700 $Y=-11440
X11 SH2_BAR SH<2> D<2> 9 VSS! VSS! VDD! decoder_3rd $T=4000 -5600 0 0 $X=3700 $Y=-5840
X12 SH2_BAR SH<2> D<1> 10 VSS! VSS! VDD! decoder_3rd $T=4000 0 0 0 $X=3700 $Y=-240
X13 SH2_BAR SH<2> D<0> 11 VSS! VSS! VDD! decoder_3rd $T=4000 5600 0 0 $X=3700 $Y=5360
.ENDS
***************************************
