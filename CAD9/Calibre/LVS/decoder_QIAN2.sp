* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT decoder_1st 1 2 3 4 5 6 7
** N=34 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4870 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT decoder_2st 1 2 3 4 5 6 7
** N=32 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4590 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=1730 $D=189
.ENDS
***************************************
.SUBCKT decoder_QIAN2 EN SH0_BAR SH<0> SH1_BAR SH<1> C3 C2 C1 C0 VSS! VDD!
** N=14 EP=11 IP=48 FDC=24
X0 SH0_BAR SH<0> 4 VSS! VDD! VSS! EN decoder_1st $T=2690 6640 0 0 $X=2390 $Y=6400
X1 SH0_BAR SH<0> 5 VSS! VDD! EN VSS! decoder_1st $T=2690 12240 0 0 $X=2390 $Y=12000
X2 SH1_BAR SH<1> C3 VSS! VDD! VSS! 4 decoder_2st $T=4290 -4560 0 0 $X=3990 $Y=-4800
X3 SH1_BAR SH<1> C2 VSS! VDD! VSS! 5 decoder_2st $T=4290 1040 0 0 $X=3990 $Y=800
X4 SH1_BAR SH<1> C1 VSS! VDD! 4 VSS! decoder_2st $T=4290 6640 0 0 $X=3990 $Y=6400
X5 SH1_BAR SH<1> C0 VSS! VDD! 5 VSS! decoder_2st $T=4290 12240 0 0 $X=3990 $Y=12000
.ENDS
***************************************
