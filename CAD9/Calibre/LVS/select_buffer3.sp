* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT select_buffer3 SH<2> SH2 VSS! VDD! SH2_BAR
** N=96 EP=5 IP=0 FDC=32
M0 SH2_BAR SH<2> VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=6050 $D=97
M1 3 SH2 VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=10390 $D=97
M2 VSS! SH<2> SH2_BAR VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=6050 $D=97
M3 VSS! SH2 3 VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=10390 $D=97
M4 SH2_BAR SH<2> VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1380 $Y=6050 $D=97
M5 SH<2> 3 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1440 $Y=10380 $D=97
M6 VSS! SH<2> SH2_BAR VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=6050 $D=97
M7 VSS! 3 SH<2> VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1920 $Y=10380 $D=97
M8 SH2_BAR SH<2> VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2340 $Y=6050 $D=97
M9 SH<2> 3 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=10380 $D=97
M10 VSS! SH<2> SH2_BAR VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=6050 $D=97
M11 VSS! 3 SH<2> VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=10380 $D=97
M12 SH2_BAR SH<2> VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=6050 $D=97
M13 SH<2> 3 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=10380 $D=97
M14 VSS! SH<2> SH2_BAR VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3790 $Y=6050 $D=97
M15 SH2_BAR SH<2> VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4270 $Y=6050 $D=97
M16 SH2_BAR SH<2> VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=7190 $D=189
M17 3 SH2 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=8910 $D=189
M18 VDD! SH<2> SH2_BAR VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=7190 $D=189
M19 VDD! SH2 3 VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=8910 $D=189
M20 SH2_BAR SH<2> VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1380 $Y=7190 $D=189
M21 SH<2> 3 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1440 $Y=8910 $D=189
M22 VDD! SH<2> SH2_BAR VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=7190 $D=189
M23 VDD! 3 SH<2> VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1920 $Y=8910 $D=189
M24 SH2_BAR SH<2> VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2340 $Y=7190 $D=189
M25 SH<2> 3 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=8910 $D=189
M26 VDD! SH<2> SH2_BAR VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=7190 $D=189
M27 VDD! 3 SH<2> VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=8910 $D=189
M28 SH2_BAR SH<2> VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=7190 $D=189
M29 SH<2> 3 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=8910 $D=189
M30 VDD! SH<2> SH2_BAR VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3790 $Y=7190 $D=189
M31 SH2_BAR SH<2> VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4270 $Y=7190 $D=189
.ENDS
***************************************
