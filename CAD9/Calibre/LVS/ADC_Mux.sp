* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT ADC_Mux Vin RBL<0> RBL<1> RBL<2> RBL<3> RBL<4> RBL<5> RBL<6> RBL<7> RBL<8> RBL<9> RBL<10> RBL<11> RBL<12> RBL<13> RBL<14> RBL<15> Col_Sel<0> Col_Sel<1> Col_Sel<2>
+ Col_Sel<3> Col_Sel<4> Col_Sel<5> Col_Sel<6> Col_Sel<7> Col_Sel<8> Col_Sel<9> Col_Sel<10> Col_Sel<11> Col_Sel<12> Col_Sel<13> Col_Sel<14> Col_Sel<15> VSS! VDD!
** N=692 EP=35 IP=0 FDC=128
M0 Vin Col_Sel<0> RBL<0> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=710 $D=97
M1 RBL<0> Col_Sel<0> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1010 $Y=710 $D=97
M2 Vin Col_Sel<0> RBL<0> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=710 $D=97
M3 VSS! Col_Sel<0> 18 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2620 $Y=1430 $D=97
M4 Vin Col_Sel<1> RBL<1> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3650 $Y=710 $D=97
M5 RBL<1> Col_Sel<1> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=710 $D=97
M6 Vin Col_Sel<1> RBL<1> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4770 $Y=710 $D=97
M7 VSS! Col_Sel<1> 20 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5820 $Y=1430 $D=97
M8 Vin Col_Sel<2> RBL<2> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6850 $Y=710 $D=97
M9 RBL<2> Col_Sel<2> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7410 $Y=710 $D=97
M10 Vin Col_Sel<2> RBL<2> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7970 $Y=710 $D=97
M11 VSS! Col_Sel<2> 22 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9020 $Y=1430 $D=97
M12 Vin Col_Sel<3> RBL<3> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10050 $Y=710 $D=97
M13 RBL<3> Col_Sel<3> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10610 $Y=710 $D=97
M14 Vin Col_Sel<3> RBL<3> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11170 $Y=710 $D=97
M15 VSS! Col_Sel<3> 24 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12220 $Y=1430 $D=97
M16 Vin Col_Sel<4> RBL<4> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13250 $Y=710 $D=97
M17 RBL<4> Col_Sel<4> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13810 $Y=710 $D=97
M18 Vin Col_Sel<4> RBL<4> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14370 $Y=710 $D=97
M19 VSS! Col_Sel<4> 26 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=15420 $Y=1430 $D=97
M20 Vin Col_Sel<5> RBL<5> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=16450 $Y=710 $D=97
M21 RBL<5> Col_Sel<5> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17010 $Y=710 $D=97
M22 Vin Col_Sel<5> RBL<5> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17570 $Y=710 $D=97
M23 VSS! Col_Sel<5> 28 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=18620 $Y=1430 $D=97
M24 Vin Col_Sel<6> RBL<6> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=19650 $Y=710 $D=97
M25 RBL<6> Col_Sel<6> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20210 $Y=710 $D=97
M26 Vin Col_Sel<6> RBL<6> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20770 $Y=710 $D=97
M27 VSS! Col_Sel<6> 30 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=21820 $Y=1430 $D=97
M28 Vin Col_Sel<7> RBL<7> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=22850 $Y=710 $D=97
M29 RBL<7> Col_Sel<7> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23410 $Y=710 $D=97
M30 Vin Col_Sel<7> RBL<7> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23970 $Y=710 $D=97
M31 VSS! Col_Sel<7> 32 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=25020 $Y=1430 $D=97
M32 Vin Col_Sel<8> RBL<8> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26050 $Y=710 $D=97
M33 RBL<8> Col_Sel<8> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26610 $Y=710 $D=97
M34 Vin Col_Sel<8> RBL<8> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=27170 $Y=710 $D=97
M35 VSS! Col_Sel<8> 34 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=28220 $Y=1430 $D=97
M36 Vin Col_Sel<9> RBL<9> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29250 $Y=710 $D=97
M37 RBL<9> Col_Sel<9> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29810 $Y=710 $D=97
M38 Vin Col_Sel<9> RBL<9> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=30370 $Y=710 $D=97
M39 VSS! Col_Sel<9> 36 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=31420 $Y=1430 $D=97
M40 Vin Col_Sel<10> RBL<10> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=32450 $Y=710 $D=97
M41 RBL<10> Col_Sel<10> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33010 $Y=710 $D=97
M42 Vin Col_Sel<10> RBL<10> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33570 $Y=710 $D=97
M43 VSS! Col_Sel<10> 38 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=34620 $Y=1430 $D=97
M44 Vin Col_Sel<11> RBL<11> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=35650 $Y=710 $D=97
M45 RBL<11> Col_Sel<11> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36210 $Y=710 $D=97
M46 Vin Col_Sel<11> RBL<11> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36770 $Y=710 $D=97
M47 VSS! Col_Sel<11> 40 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=37820 $Y=1430 $D=97
M48 Vin Col_Sel<12> RBL<12> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38850 $Y=710 $D=97
M49 RBL<12> Col_Sel<12> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39410 $Y=710 $D=97
M50 Vin Col_Sel<12> RBL<12> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39970 $Y=710 $D=97
M51 VSS! Col_Sel<12> 42 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=41020 $Y=1430 $D=97
M52 Vin Col_Sel<13> RBL<13> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42050 $Y=710 $D=97
M53 RBL<13> Col_Sel<13> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42610 $Y=710 $D=97
M54 Vin Col_Sel<13> RBL<13> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=43170 $Y=710 $D=97
M55 VSS! Col_Sel<13> 44 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=44220 $Y=1430 $D=97
M56 Vin Col_Sel<14> RBL<14> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45250 $Y=710 $D=97
M57 RBL<14> Col_Sel<14> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45810 $Y=710 $D=97
M58 Vin Col_Sel<14> RBL<14> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=46370 $Y=710 $D=97
M59 VSS! Col_Sel<14> 46 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=47420 $Y=1430 $D=97
M60 Vin Col_Sel<15> RBL<15> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=48450 $Y=710 $D=97
M61 RBL<15> Col_Sel<15> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49010 $Y=710 $D=97
M62 Vin Col_Sel<15> RBL<15> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49570 $Y=710 $D=97
M63 VSS! Col_Sel<15> 48 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=50620 $Y=1430 $D=97
M64 Vin 18 RBL<0> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2890 $D=189
M65 RBL<0> 18 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1010 $Y=2890 $D=189
M66 Vin 18 RBL<0> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=2890 $D=189
M67 VDD! Col_Sel<0> 18 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2620 $Y=2890 $D=189
M68 Vin 20 RBL<1> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3650 $Y=2890 $D=189
M69 RBL<1> 20 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=2890 $D=189
M70 Vin 20 RBL<1> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4770 $Y=2890 $D=189
M71 VDD! Col_Sel<1> 20 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5820 $Y=2890 $D=189
M72 Vin 22 RBL<2> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6850 $Y=2890 $D=189
M73 RBL<2> 22 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7410 $Y=2890 $D=189
M74 Vin 22 RBL<2> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7970 $Y=2890 $D=189
M75 VDD! Col_Sel<2> 22 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9020 $Y=2890 $D=189
M76 Vin 24 RBL<3> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10050 $Y=2890 $D=189
M77 RBL<3> 24 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10610 $Y=2890 $D=189
M78 Vin 24 RBL<3> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11170 $Y=2890 $D=189
M79 VDD! Col_Sel<3> 24 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12220 $Y=2890 $D=189
M80 Vin 26 RBL<4> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13250 $Y=2890 $D=189
M81 RBL<4> 26 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13810 $Y=2890 $D=189
M82 Vin 26 RBL<4> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14370 $Y=2890 $D=189
M83 VDD! Col_Sel<4> 26 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=15420 $Y=2890 $D=189
M84 Vin 28 RBL<5> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=16450 $Y=2890 $D=189
M85 RBL<5> 28 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17010 $Y=2890 $D=189
M86 Vin 28 RBL<5> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17570 $Y=2890 $D=189
M87 VDD! Col_Sel<5> 28 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=18620 $Y=2890 $D=189
M88 Vin 30 RBL<6> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=19650 $Y=2890 $D=189
M89 RBL<6> 30 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20210 $Y=2890 $D=189
M90 Vin 30 RBL<6> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20770 $Y=2890 $D=189
M91 VDD! Col_Sel<6> 30 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=21820 $Y=2890 $D=189
M92 Vin 32 RBL<7> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=22850 $Y=2890 $D=189
M93 RBL<7> 32 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23410 $Y=2890 $D=189
M94 Vin 32 RBL<7> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23970 $Y=2890 $D=189
M95 VDD! Col_Sel<7> 32 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=25020 $Y=2890 $D=189
M96 Vin 34 RBL<8> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26050 $Y=2890 $D=189
M97 RBL<8> 34 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26610 $Y=2890 $D=189
M98 Vin 34 RBL<8> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=27170 $Y=2890 $D=189
M99 VDD! Col_Sel<8> 34 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=28220 $Y=2890 $D=189
M100 Vin 36 RBL<9> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29250 $Y=2890 $D=189
M101 RBL<9> 36 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29810 $Y=2890 $D=189
M102 Vin 36 RBL<9> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=30370 $Y=2890 $D=189
M103 VDD! Col_Sel<9> 36 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=31420 $Y=2890 $D=189
M104 Vin 38 RBL<10> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=32450 $Y=2890 $D=189
M105 RBL<10> 38 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33010 $Y=2890 $D=189
M106 Vin 38 RBL<10> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33570 $Y=2890 $D=189
M107 VDD! Col_Sel<10> 38 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=34620 $Y=2890 $D=189
M108 Vin 40 RBL<11> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=35650 $Y=2890 $D=189
M109 RBL<11> 40 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36210 $Y=2890 $D=189
M110 Vin 40 RBL<11> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36770 $Y=2890 $D=189
M111 VDD! Col_Sel<11> 40 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=37820 $Y=2890 $D=189
M112 Vin 42 RBL<12> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38850 $Y=2890 $D=189
M113 RBL<12> 42 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39410 $Y=2890 $D=189
M114 Vin 42 RBL<12> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39970 $Y=2890 $D=189
M115 VDD! Col_Sel<12> 42 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=41020 $Y=2890 $D=189
M116 Vin 44 RBL<13> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42050 $Y=2890 $D=189
M117 RBL<13> 44 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42610 $Y=2890 $D=189
M118 Vin 44 RBL<13> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=43170 $Y=2890 $D=189
M119 VDD! Col_Sel<13> 44 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=44220 $Y=2890 $D=189
M120 Vin 46 RBL<14> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45250 $Y=2890 $D=189
M121 RBL<14> 46 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45810 $Y=2890 $D=189
M122 Vin 46 RBL<14> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=46370 $Y=2890 $D=189
M123 VDD! Col_Sel<14> 46 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=47420 $Y=2890 $D=189
M124 Vin 48 RBL<15> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=48450 $Y=2890 $D=189
M125 RBL<15> 48 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49010 $Y=2890 $D=189
M126 Vin 48 RBL<15> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49570 $Y=2890 $D=189
M127 VDD! Col_Sel<15> 48 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=50620 $Y=2890 $D=189
.ENDS
***************************************
