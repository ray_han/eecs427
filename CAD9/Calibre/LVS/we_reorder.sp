* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT CLKAND2X2TR A B VSS VDD Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9
** N=9 EP=9 IP=14 FDC=0
X0 1 2 3 4 5 CLKAND2X2TR $T=0 0 0 0 $X=-340 $Y=-280
X1 6 2 3 4 7 CLKAND2X2TR $T=2000 0 0 0 $X=1660 $Y=-280
.ENDS
***************************************
.SUBCKT CLKINVX2TR A VSS VDD Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI33X1TR B2 B1 VSS B0 A0 A1 A2 VDD Y
** N=12 EP=9 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT INVX2TR A VSS VDD Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND4BX1TR AN D VSS C B Y VDD
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR3X1TR C VDD B A VSS Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT we_reorder VDD VSS inst7_4<0> inst7_4<1> WE<5> inst7_4<3> WE<10> WE<14> WE<4> WE<7> WE<1> WE<12> WE<3> WE_OUT<1> WE_OUT<10> WE_OUT<12> WE<8> WE_OUT<7> WE_OUT<5> WE_OUT<14>
+ WE<2> WE<9> WE<11> WE<0> WE<6> WE<15> WE<13> WE_OUT<3> WE_OUT<9> WE_OUT<6> WE_OUT<13> WE_OUT<11> WE_OUT<0> WE_OUT<4> WE_OUT<15> WE_OUT<2> WE_OUT<8> inst15_12<0> inst15_12<1> inst15_12<3>
+ inst15_12<2> inst7_4<2>
** N=65 EP=42 IP=128 FDC=0
X0 WE<3> 16 VSS VDD WE_OUT<3> CLKAND2X2TR $T=28400 54000 0 0 $X=28060 $Y=53720
X1 WE<8> 16 VSS VDD WE_OUT<8> CLKAND2X2TR $T=28400 68400 1 0 $X=28060 $Y=64400
X2 WE<0> 16 VSS VDD WE_OUT<0> CLKAND2X2TR $T=28800 46800 0 0 $X=28460 $Y=46520
X3 WE<15> 16 VSS VDD WE_OUT<15> CLKAND2X2TR $T=28800 82800 1 0 $X=28460 $Y=78800
X4 WE<1> 16 VSS VDD WE_OUT<1> WE<2> WE_OUT<2> 61 65 ICV_1 $T=26400 54000 1 0 $X=26060 $Y=50000
X5 WE<5> 16 VSS VDD WE_OUT<5> WE<4> WE_OUT<4> 62 65 ICV_1 $T=26400 61200 1 0 $X=26060 $Y=57200
X6 WE<10> 16 VSS VDD WE_OUT<10> WE<9> WE_OUT<9> 56 65 ICV_1 $T=26400 68400 0 0 $X=26060 $Y=68120
X7 WE<12> 16 VSS VDD WE_OUT<12> WE<11> WE_OUT<11> 56 65 ICV_1 $T=26400 75600 1 0 $X=26060 $Y=71600
X8 WE<7> 16 VSS VDD WE_OUT<7> WE<6> WE_OUT<6> 63 65 ICV_1 $T=26800 61200 0 0 $X=26460 $Y=60920
X9 WE<14> 16 VSS VDD WE_OUT<14> WE<13> WE_OUT<13> 64 65 ICV_1 $T=26800 75600 0 0 $X=26460 $Y=75320
X10 inst7_4<0> VSS VDD 48 CLKINVX2TR $T=17600 104400 1 0 $X=17260 $Y=100400
X11 inst7_4<3> VSS VDD 7 CLKINVX2TR $T=24800 104400 1 180 $X=23260 $Y=104120
X12 inst7_4<1> inst7_4<3> VSS inst7_4<0> 7 48 47 VDD 4 OAI33X1TR $T=23600 104400 1 180 $X=19660 $Y=104120
X13 inst7_4<1> VSS VDD 47 INVX2TR $T=18800 104400 0 0 $X=18460 $Y=104120
X14 inst15_12<0> 3 VSS inst7_4<2> 4 16 VDD NAND4BX1TR $T=14000 104400 0 0 $X=13660 $Y=104120
X15 inst15_12<2> VDD inst15_12<3> inst15_12<1> VSS 3 NOR3X1TR $T=14000 104400 1 180 $X=11660 $Y=104120
.ENDS
***************************************
