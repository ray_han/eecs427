* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT decoder_1st 1 2 3 4 5 6 7
** N=39 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4870 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT decoder_2st 1 2 3 4 5 6 7
** N=37 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4590 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=1730 $D=189
.ENDS
***************************************
.SUBCKT decoder_3rd 1 2 3 4 5 6 7
** N=67 EP=7 IP=0 FDC=8
M0 8 1 4 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=450 $D=97
M1 8 2 5 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=4870 $D=97
M2 3 8 6 6 nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=450 $D=97
M3 3 8 6 6 nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=4550 $D=97
M4 8 1 5 7 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=3310 $D=189
M5 8 2 4 7 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=2010 $D=189
M6 3 8 7 7 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=1690 $D=189
M7 3 8 7 7 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT decoder_4th E SH3_BAR SH<3> IN IN1 VSS! VDD!
** N=108 EP=7 IP=0 FDC=26
M0 9 SH3_BAR IN VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=450 $D=97
M1 9 SH<3> IN1 VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=4810 $D=97
M2 10 9 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=450 $D=97
M3 10 9 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=4780 $D=97
M4 8 10 VSS! VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=450 $D=97
M5 E 8 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=4550 $D=97
M6 VSS! 10 8 VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=450 $D=97
M7 VSS! 8 E VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=4550 $D=97
M8 E 8 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3780 $Y=4550 $D=97
M9 E 8 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3820 $Y=450 $D=97
M10 VSS! 8 E VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4260 $Y=4550 $D=97
M11 VSS! 8 E VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4300 $Y=450 $D=97
M12 E 8 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4740 $Y=4550 $D=97
M13 9 SH3_BAR IN1 VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=3310 $D=189
M14 9 SH<3> IN VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=1950 $D=189
M15 10 9 VDD! VDD! pfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=1920 $D=189
M16 10 9 VDD! VDD! pfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=3310 $D=189
M17 8 10 VDD! VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=1790 $D=189
M18 E 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=3310 $D=189
M19 VDD! 10 8 VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=1790 $D=189
M20 VDD! 8 E VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=3310 $D=189
M21 E 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3780 $Y=3310 $D=189
M22 E 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3820 $Y=1690 $D=189
M23 VDD! 8 E VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4260 $Y=3310 $D=189
M24 VDD! 8 E VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4300 $Y=1690 $D=189
M25 E 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4740 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9 10
** N=10 EP=10 IP=14 FDC=34
X0 2 3 4 8 9 10 7 decoder_3rd $T=-4800 0 0 0 $X=-5100 $Y=-240
X1 1 5 6 4 7 10 7 decoder_4th $T=0 0 0 0 $X=-300 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11 12 13 14
** N=14 EP=14 IP=20 FDC=68
X0 1 3 4 5 6 7 9 10 11 12 ICV_1 $T=0 0 0 0 $X=-5100 $Y=-240
X1 2 3 4 8 6 7 9 13 14 12 ICV_1 $T=0 5600 0 0 $X=-5100 $Y=5360
.ENDS
***************************************
.SUBCKT ICV_3 1 2 3 4 5 6 7 8
** N=8 EP=8 IP=14 FDC=52
X0 1 3 4 5 6 7 5 decoder_4th $T=0 0 0 0 $X=-300 $Y=-240
X1 2 3 4 5 8 7 5 decoder_4th $T=0 5600 0 0 $X=-300 $Y=5360
.ENDS
***************************************
.SUBCKT DECODER_WORST SEL<3> SEL<2> SEL<0> SEL<1> EN E<7> E<6> E<5> E<4> E<3> E<2> E<1> E<0> E<15> E<14> E<13> E<12> E<11> E<10> E<9>
+ E<8> VDD! VSS!
** N=303 EP=23 IP=130 FDC=586
M0 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6730 $Y=34050 $D=97
M1 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=34050 $D=97
M2 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=39650 $D=97
M3 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=34050 $D=97
M4 27 26 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=39650 $D=97
M5 VSS! 45 24 VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8090 $Y=38320 $D=97
M6 VSS! 46 26 VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8110 $Y=43980 $D=97
M7 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=34050 $D=97
M8 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=39650 $D=97
M9 24 45 VSS! VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8570 $Y=38320 $D=97
M10 26 46 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8590 $Y=43980 $D=97
M11 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=34050 $D=97
M12 27 26 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=39650 $D=97
M13 VSS! 45 24 VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9050 $Y=38320 $D=97
M14 VSS! 46 26 VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9070 $Y=43980 $D=97
M15 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=34050 $D=97
M16 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=39650 $D=97
M17 24 45 VSS! VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9540 $Y=38320 $D=97
M18 26 46 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9560 $Y=43980 $D=97
M19 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=34050 $D=97
M20 27 26 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=39650 $D=97
M21 VSS! 45 24 VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10020 $Y=38320 $D=97
M22 VSS! 46 26 VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10040 $Y=43980 $D=97
M23 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=34050 $D=97
M24 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=39650 $D=97
M25 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=34050 $D=97
M26 45 SEL<3> VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=38330 $D=97
M27 27 26 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=39650 $D=97
M28 46 SEL<2> VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=43990 $D=97
M29 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=34050 $D=97
M30 VSS! SEL<3> 45 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=38330 $D=97
M31 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=39650 $D=97
M32 VSS! SEL<2> 46 VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=43990 $D=97
M33 28 29 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12820 $Y=62050 $D=97
M34 VSS! 29 28 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13300 $Y=62050 $D=97
M35 29 SEL<1> VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13500 $Y=66400 $D=97
M36 28 29 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=62050 $D=97
M37 44 SEL<0> VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=77600 $D=97
M38 VSS! SEL<1> 29 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13980 $Y=66400 $D=97
M39 VSS! 29 28 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=62050 $D=97
M40 VSS! SEL<0> 44 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=77600 $D=97
M41 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6730 $Y=35110 $D=189
M42 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=35110 $D=189
M43 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=40790 $D=189
M44 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=35110 $D=189
M45 27 26 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=40790 $D=189
M46 VDD! 45 24 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8090 $Y=36910 $D=189
M47 VDD! 46 26 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8110 $Y=42510 $D=189
M48 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=35110 $D=189
M49 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=40790 $D=189
M50 24 45 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8570 $Y=36910 $D=189
M51 26 46 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8590 $Y=42510 $D=189
M52 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=35110 $D=189
M53 27 26 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=40790 $D=189
M54 VDD! 45 24 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9050 $Y=36910 $D=189
M55 VDD! 46 26 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9070 $Y=42510 $D=189
M56 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=35110 $D=189
M57 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=40790 $D=189
M58 24 45 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9540 $Y=36910 $D=189
M59 26 46 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9560 $Y=42510 $D=189
M60 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=35110 $D=189
M61 27 26 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=40790 $D=189
M62 VDD! 45 24 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10020 $Y=36910 $D=189
M63 VDD! 46 26 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10040 $Y=42510 $D=189
M64 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=35110 $D=189
M65 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=40790 $D=189
M66 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=35110 $D=189
M67 45 SEL<3> VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=36910 $D=189
M68 27 26 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=40790 $D=189
M69 46 SEL<2> VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=42510 $D=189
M70 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=35110 $D=189
M71 VDD! SEL<3> 45 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=36910 $D=189
M72 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=40790 $D=189
M73 VDD! SEL<2> 46 VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=42510 $D=189
M74 28 29 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12820 $Y=63090 $D=189
M75 VDD! 29 28 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13300 $Y=63090 $D=189
M76 29 SEL<1> VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13500 $Y=64910 $D=189
M77 28 29 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=63090 $D=189
M78 44 SEL<0> VDD! VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=76110 $D=189
M79 VDD! SEL<1> 29 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13980 $Y=64910 $D=189
M80 VDD! 29 28 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=63090 $D=189
M81 VDD! SEL<0> 44 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=76110 $D=189
X82 44 SEL<0> 43 VSS! VDD! VSS! EN decoder_1st $T=14800 78400 1 180 $X=12280 $Y=78160
X83 44 SEL<0> 42 VSS! VDD! EN VSS! decoder_1st $T=14800 84000 1 180 $X=12280 $Y=83760
X84 29 28 41 VSS! VDD! VSS! 43 decoder_2st $T=13200 67200 1 180 $X=10680 $Y=66960
X85 29 28 40 VSS! VDD! VSS! 42 decoder_2st $T=13200 72800 1 180 $X=10680 $Y=72560
X86 29 28 39 VSS! VDD! 43 VSS! decoder_2st $T=13200 78400 1 180 $X=10680 $Y=78160
X87 29 28 38 VSS! VDD! 42 VSS! decoder_2st $T=13200 84000 1 180 $X=10680 $Y=83760
X88 E<7> E<6> 27 26 37 25 24 36 VDD! VSS! 41 VSS! VSS! 40 ICV_2 $T=6000 44800 1 180 $X=-300 $Y=44560
X89 E<5> E<4> 27 26 35 25 24 34 VDD! VSS! 39 VSS! VSS! 38 ICV_2 $T=6000 56000 1 180 $X=-300 $Y=55760
X90 E<3> E<2> 27 26 33 25 24 32 VDD! 41 VSS! VSS! 40 VSS! ICV_2 $T=6000 67200 1 180 $X=-300 $Y=66960
X91 E<1> E<0> 27 26 31 25 24 30 VDD! 39 VSS! VSS! 38 VSS! ICV_2 $T=6000 78400 1 180 $X=-300 $Y=78160
X92 E<15> E<14> 25 24 VDD! 37 VSS! 36 ICV_3 $T=6000 0 1 180 $X=-300 $Y=-240
X93 E<13> E<12> 25 24 VDD! 35 VSS! 34 ICV_3 $T=6000 11200 1 180 $X=-300 $Y=10960
X94 E<11> E<10> 25 24 VDD! 33 VSS! 32 ICV_3 $T=6000 22400 1 180 $X=-300 $Y=22160
X95 E<9> E<8> 25 24 VDD! 31 VSS! 30 ICV_3 $T=6000 33600 1 180 $X=-300 $Y=33360
.ENDS
***************************************
.SUBCKT WBL_Driver_1bit IN WBLB WBL VSS! VDD!
** N=69 EP=5 IP=0 FDC=18
M0 VSS! 7 6 VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=450 $D=97
M1 VSS! 7 6 VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=4840 $D=97
M2 WBLB 6 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=450 $D=97
M3 7 IN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=4870 $D=97
M4 VSS! 6 WBLB VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=450 $D=97
M5 WBLB 6 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M6 VSS! IN 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=4870 $D=97
M7 VSS! 6 WBLB VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M8 WBL 8 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=4750 $D=97
M9 VDD! 7 6 VDD! pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=1560 $D=189
M10 VDD! 7 6 VDD! pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=3310 $D=189
M11 WBLB 6 VDD! VDD! pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=1500 $D=189
M12 7 IN VDD! VDD! pfet L=1.2e-07 W=6.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3310 $D=189
M13 VDD! 6 WBLB VDD! pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=1500 $D=189
M14 WBLB 6 VDD! VDD! pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1500 $D=189
M15 VDD! IN 8 VDD! pfet L=1.2e-07 W=5.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3310 $D=189
M16 VDD! 6 WBLB VDD! pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1500 $D=189
M17 WBL 8 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT REN_Driver_1bit RENB REN IN VSS! VDD!
** N=68 EP=5 IP=0 FDC=18
M0 VSS! 7 6 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=450 $D=97
M1 VSS! 7 6 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=4750 $D=97
M2 RENB 6 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=450 $D=97
M3 7 IN VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=4750 $D=97
M4 VSS! 6 RENB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=450 $D=97
M5 RENB 6 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M6 VSS! IN 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=4750 $D=97
M7 VSS! 6 RENB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M8 REN 8 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=4750 $D=97
M9 VDD! 7 6 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=1490 $D=189
M10 VDD! 7 6 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=3310 $D=189
M11 RENB 6 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=1490 $D=189
M12 7 IN VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3310 $D=189
M13 VDD! 6 RENB VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=1490 $D=189
M14 RENB 6 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1490 $D=189
M15 VDD! IN 8 VDD! pfet L=1.2e-07 W=6.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3310 $D=189
M16 VDD! 6 RENB VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1490 $D=189
M17 REN 8 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_4 1 2 3 4 5 6 7 8 9 10 11 12 13
** N=13 EP=13 IP=20 FDC=72
X0 3 4 5 13 12 WBL_Driver_1bit $T=0 0 0 0 $X=0 $Y=-240
X1 6 7 8 13 12 WBL_Driver_1bit $T=3200 0 0 0 $X=3200 $Y=-240
X2 1 10 9 13 12 REN_Driver_1bit $T=0 5600 0 0 $X=0 $Y=5360
X3 2 11 9 13 12 REN_Driver_1bit $T=3200 5600 0 0 $X=3200 $Y=5360
.ENDS
***************************************
.SUBCKT ICV_5 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23
** N=23 EP=23 IP=26 FDC=144
X0 1 2 5 7 9 10 11 13 6 8 12 22 23 ICV_4 $T=0 0 0 0 $X=0 $Y=-240
X1 3 4 14 15 17 18 19 21 6 16 20 22 23 ICV_4 $T=6400 0 0 0 $X=6400 $Y=-240
.ENDS
***************************************
.SUBCKT BITCELL RENB RWL_N WWL RWLB_P RWL_P RWLB_N REN RBL WBLB WBL VSS! VDD!
** N=72 EP=12 IP=0 FDC=12
M0 13 REN RBL VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=700 $Y=450 $D=97
M1 RWL_N 15 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=450 $D=97
M2 RWLB_N 16 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=4870 $D=97
M3 16 15 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2120 $Y=4870 $D=97
M4 15 WWL WBLB VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2240 $Y=450 $D=97
M5 WBL WWL 16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2600 $Y=4870 $D=97
M6 VSS! 16 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2720 $Y=450 $D=97
M7 14 RENB RBL VDD! pfet L=1.2e-07 W=8.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=700 $Y=3310 $D=189
M8 RWLB_P 15 14 VDD! pfet L=1.2e-07 W=8.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=1420 $D=189
M9 RWL_P 16 14 VDD! pfet L=1.2e-07 W=8.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=3310 $D=189
M10 16 15 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2120 $Y=3310 $D=189
M11 VDD! 16 15 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1750 $D=189
.ENDS
***************************************
.SUBCKT ICV_6 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
** N=17 EP=17 IP=24 FDC=24
X0 1 3 4 5 6 7 9 8 10 11 17 16 BITCELL $T=0 0 0 0 $X=0 $Y=-240
X1 2 3 4 5 6 7 13 12 14 15 17 16 BITCELL $T=3200 0 0 0 $X=3200 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_7 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27
** N=27 EP=27 IP=34 FDC=48
X0 1 2 5 6 7 8 9 10 11 12 13 14 15 16 17 26 27 ICV_6 $T=0 0 0 0 $X=0 $Y=-240
X1 3 4 5 6 7 8 9 18 19 20 21 22 23 24 25 26 27 ICV_6 $T=6400 0 0 0 $X=6400 $Y=-240
.ENDS
***************************************
.SUBCKT SRAM_1WORD RENB<0> RENB<1> RENB<2> RENB<3> RENB<4> RENB<5> RENB<6> RENB<7> RENB<8> RENB<9> RENB<10> RENB<11> RENB<12> RENB<13> RENB<14> RENB<15> RWL_N WWL RWLB_P RWL_P
+ RWLB_N RBL<0> REN<0> WBLB<0> WBL<0> RBL<1> REN<1> WBLB<1> WBL<1> RBL<2> REN<2> WBLB<2> WBL<2> RBL<3> REN<3> WBLB<3> WBL<3> RBL<4> REN<4> WBLB<4>
+ WBL<4> RBL<5> REN<5> WBLB<5> WBL<5> RBL<6> REN<6> WBLB<6> WBL<6> RBL<7> REN<7> WBLB<7> WBL<7> RBL<8> REN<8> WBLB<8> WBL<8> RBL<9> REN<9> WBLB<9>
+ WBL<9> RBL<10> REN<10> WBLB<10> WBL<10> RBL<11> REN<11> WBLB<11> WBL<11> RBL<12> REN<12> WBLB<12> WBL<12> RBL<13> REN<13> WBLB<13> WBL<13> RBL<14> REN<14> WBLB<14>
+ WBL<14> RBL<15> REN<15> WBLB<15> WBL<15> VDD! VSS!
** N=87 EP=87 IP=108 FDC=192
X0 RENB<0> RENB<1> RENB<2> RENB<3> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<0> REN<0> WBLB<0> WBL<0> RBL<1> REN<1> WBLB<1> WBL<1> RBL<2> REN<2> WBLB<2>
+ WBL<2> RBL<3> REN<3> WBLB<3> WBL<3> VDD! VSS!
+ ICV_7 $T=0 250 0 0 $X=0 $Y=10
X1 RENB<4> RENB<5> RENB<6> RENB<7> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<4> REN<4> WBLB<4> WBL<4> RBL<5> REN<5> WBLB<5> WBL<5> RBL<6> REN<6> WBLB<6>
+ WBL<6> RBL<7> REN<7> WBLB<7> WBL<7> VDD! VSS!
+ ICV_7 $T=12800 250 0 0 $X=12800 $Y=10
X2 RENB<8> RENB<9> RENB<10> RENB<11> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<8> REN<8> WBLB<8> WBL<8> RBL<9> REN<9> WBLB<9> WBL<9> RBL<10> REN<10> WBLB<10>
+ WBL<10> RBL<11> REN<11> WBLB<11> WBL<11> VDD! VSS!
+ ICV_7 $T=25600 250 0 0 $X=25600 $Y=10
X3 RENB<12> RENB<13> RENB<14> RENB<15> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<12> REN<12> WBLB<12> WBL<12> RBL<13> REN<13> WBLB<13> WBL<13> RBL<14> REN<14> WBLB<14>
+ WBL<14> RBL<15> REN<15> WBLB<15> WBL<15> VDD! VSS!
+ ICV_7 $T=38400 250 0 0 $X=38400 $Y=10
.ENDS
***************************************
.SUBCKT RWL_Driver_1bit RWLN RWLBP RWLP RWLBN RWL_N RWLB_N RWLB_P RWL_P VSS! VDD!
** N=154 EP=10 IP=0 FDC=48
M0 RWLN 11 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 11 RWL_N VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=4730 $D=97
M2 VSS! 11 RWLN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=450 $D=97
M3 VSS! RWL_N 11 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=4730 $D=97
M4 RWLN 11 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=450 $D=97
M5 RWLBN 14 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=4730 $D=97
M6 VSS! 11 RWLN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=450 $D=97
M7 VSS! 14 RWLBN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=4730 $D=97
M8 14 RWLB_N VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=450 $D=97
M9 RWLBN 14 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=4730 $D=97
M10 VSS! RWLB_N 14 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=450 $D=97
M11 VSS! 14 RWLBN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=4730 $D=97
M12 RWLBP 12 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=450 $D=97
M13 12 RWLB_P VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=4730 $D=97
M14 VSS! 12 RWLBP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=450 $D=97
M15 VSS! RWLB_P 12 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=4730 $D=97
M16 RWLBP 12 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=450 $D=97
M17 RWLP 13 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=4730 $D=97
M18 VSS! 12 RWLBP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=450 $D=97
M19 VSS! 13 RWLP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=4730 $D=97
M20 13 RWL_P VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=450 $D=97
M21 RWLP 13 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=4730 $D=97
M22 VSS! RWL_P 13 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=450 $D=97
M23 VSS! 13 RWLP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=4730 $D=97
M24 RWLN 11 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=1490 $D=189
M25 11 RWL_N VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M26 VDD! 11 RWLN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=1490 $D=189
M27 VDD! RWL_N 11 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=3310 $D=189
M28 RWLN 11 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=1490 $D=189
M29 RWLBN 14 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=3310 $D=189
M30 VDD! 11 RWLN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=1490 $D=189
M31 VDD! 14 RWLBN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=3310 $D=189
M32 14 RWLB_N VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=1480 $D=189
M33 RWLBN 14 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=3310 $D=189
M34 VDD! RWLB_N 14 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=1480 $D=189
M35 VDD! 14 RWLBN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=3310 $D=189
M36 RWLBP 12 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=1490 $D=189
M37 12 RWLB_P VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=3310 $D=189
M38 VDD! 12 RWLBP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=1490 $D=189
M39 VDD! RWLB_P 12 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=3310 $D=189
M40 RWLBP 12 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=1490 $D=189
M41 RWLP 13 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=3310 $D=189
M42 VDD! 12 RWLBP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=1490 $D=189
M43 VDD! 13 RWLP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=3310 $D=189
M44 13 RWL_P VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=1480 $D=189
M45 RWLP 13 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=3310 $D=189
M46 VDD! RWL_P 13 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=1480 $D=189
M47 VDD! 13 RWLP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_8 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
+ 81 82 83 84 85 86 87
** N=91 EP=87 IP=97 FDC=240
X0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 88 17 89 90
+ 91 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
+ 81 82 83 84 85 86 87
+ SRAM_1WORD $T=0 0 0 0 $X=0 $Y=10
X1 88 89 90 91 21 19 20 18 87 86 RWL_Driver_1bit $T=51200 250 0 0 $X=51140 $Y=10
.ENDS
***************************************
.SUBCKT ICV_9 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
+ 81 82 83 84 85 86 87 88 89 90 91 92
** N=92 EP=92 IP=174 FDC=480
X0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45
+ 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65
+ 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85
+ 86 87 88 89 90 91 92
+ ICV_8 $T=0 0 0 0 $X=0 $Y=10
X1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 22 23 24 25
+ 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45
+ 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65
+ 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85
+ 86 87 88 89 90 91 92
+ ICV_8 $T=0 5600 0 0 $X=0 $Y=5610
.ENDS
***************************************
.SUBCKT ICV_10 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
+ 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100
+ 101 102
** N=102 EP=102 IP=184 FDC=960
X0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 19 20 21
+ 22 18 23 24 25 26 37 38 39 40 41 42 43 44 45 46 47 48 49 50
+ 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70
+ 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90
+ 91 92 93 94 95 96 97 98 99 100 102 101
+ ICV_9 $T=0 0 0 0 $X=0 $Y=10
X1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 27 29 30 31
+ 32 28 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50
+ 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70
+ 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90
+ 91 92 93 94 95 96 97 98 99 100 102 101
+ ICV_9 $T=0 11200 0 0 $X=0 $Y=11210
.ENDS
***************************************
.SUBCKT XNOR_SRAM_16X16_backup RBL<0> REN_SRAM RWL_P<15> RWLB_N<15> RWLB_P<15> RWL_N<15> RWL_P<14> RWLB_N<14> RWLB_P<14> RWL_N<14> RWL_P<13> RWLB_N<13> RWLB_P<13> RWL_N<13> RWL_P<12> RWLB_N<12> RWLB_P<12> RWL_N<12> RWL_P<11> RWLB_N<11>
+ RWLB_P<11> RWL_N<11> RWL_P<10> RWLB_N<10> RWLB_P<10> RWL_N<10> RWL_P<9> RWLB_N<9> RWLB_P<9> RWL_N<9> RWL_P<8> RWLB_N<8> RWLB_P<8> RWL_N<8> RWL_P<7> RWLB_N<7> RWLB_P<7> RWL_N<7> RWL_P<6> RWLB_N<6>
+ RWLB_P<6> RWL_N<6> RWL_P<5> RWLB_N<5> RWLB_P<5> RWL_N<5> RWL_P<4> RWLB_N<4> RWLB_P<4> RWL_N<4> RWL_P<3> RWLB_N<3> RWLB_P<3> RWL_N<3> RWL_P<2> RWLB_N<2> RWLB_P<2> RWL_N<2> RWL_P<1> RWLB_N<1>
+ RWLB_P<1> RWL_N<1> RWL_P<0> RWLB_N<0> RWLB_P<0> RWL_N<0> WR_Addr<3> WR_Addr<2> WR_Addr<0> WR_EN WR_Addr<1> WR_Data<0> WR_Data<1> WR_Data<2> WR_Data<3> WR_Data<4> WR_Data<5> WR_Data<6> WR_Data<7> WR_Data<8>
+ WR_Data<9> WR_Data<10> WR_Data<11> WR_Data<12> WR_Data<13> WR_Data<14> WR_Data<15> RBL<1> RBL<2> RBL<3> RBL<4> RBL<5> RBL<6> RBL<7> RBL<8> RBL<9> RBL<10> RBL<11> RBL<12> RBL<13>
+ RBL<14> RBL<15> VSS! VDD!
** N=185 EP=104 IP=523 FDC=5002
X0 WR_Addr<3> WR_Addr<2> WR_Addr<0> WR_Addr<1> WR_EN 41 42 43 44 35 45 46 47 48 49 50 36 37 38 39
+ 40 VDD! VSS!
+ DECODER_WORST $T=57600 0 0 0 $X=57300 $Y=-240
X1 2 3 4 5 WR_Data<0> REN_SRAM 121 18 122 WR_Data<1> 124 20 125 WR_Data<2> 127 21 128 WR_Data<3> 130 22
+ 131 VDD! VSS!
+ ICV_5 $T=0 89600 0 0 $X=0 $Y=89360
X2 6 7 8 9 WR_Data<4> REN_SRAM 133 23 134 WR_Data<5> 136 24 137 WR_Data<6> 139 25 140 WR_Data<7> 142 26
+ 143 VDD! VSS!
+ ICV_5 $T=12800 89600 0 0 $X=12800 $Y=89360
X3 10 11 12 13 WR_Data<8> REN_SRAM 145 27 146 WR_Data<9> 148 28 149 WR_Data<10> 151 29 152 WR_Data<11> 154 30
+ 155 VDD! VSS!
+ ICV_5 $T=25600 89600 0 0 $X=25600 $Y=89360
X4 14 15 16 17 WR_Data<12> REN_SRAM 157 31 158 WR_Data<13> 160 32 161 WR_Data<14> 163 33 164 WR_Data<15> 166 34
+ 167 VDD! VSS!
+ ICV_5 $T=38400 89600 0 0 $X=38400 $Y=89360
X5 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 48 49 RWL_P<15> RWLB_N<15>
+ RWLB_P<15> RWL_N<15> RWL_P<14> RWLB_N<14> RWLB_P<14> RWL_N<14> 50 36 RWL_P<13> RWLB_N<13> RWLB_P<13> RWL_N<13> RWL_P<12> RWLB_N<12> RWLB_P<12> RWL_N<12> RBL<0> 18 121 122
+ RBL<1> 20 124 125 RBL<2> 21 127 128 RBL<3> 22 130 131 RBL<4> 23 133 134 RBL<5> 24 136 137
+ RBL<6> 25 139 140 RBL<7> 26 142 143 RBL<8> 27 145 146 RBL<9> 28 148 149 RBL<10> 29 151 152
+ RBL<11> 30 154 155 RBL<12> 31 157 158 RBL<13> 32 160 161 RBL<14> 33 163 164 RBL<15> 34 166 167
+ VSS! VDD!
+ ICV_10 $T=0 -250 0 0 $X=0 $Y=-240
X6 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 37 38 RWL_P<11> RWLB_N<11>
+ RWLB_P<11> RWL_N<11> RWL_P<10> RWLB_N<10> RWLB_P<10> RWL_N<10> 39 40 RWL_P<9> RWLB_N<9> RWLB_P<9> RWL_N<9> RWL_P<8> RWLB_N<8> RWLB_P<8> RWL_N<8> RBL<0> 18 121 122
+ RBL<1> 20 124 125 RBL<2> 21 127 128 RBL<3> 22 130 131 RBL<4> 23 133 134 RBL<5> 24 136 137
+ RBL<6> 25 139 140 RBL<7> 26 142 143 RBL<8> 27 145 146 RBL<9> 28 148 149 RBL<10> 29 151 152
+ RBL<11> 30 154 155 RBL<12> 31 157 158 RBL<13> 32 160 161 RBL<14> 33 163 164 RBL<15> 34 166 167
+ VSS! VDD!
+ ICV_10 $T=0 22150 0 0 $X=0 $Y=22160
X7 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 41 42 RWL_P<7> RWLB_N<7>
+ RWLB_P<7> RWL_N<7> RWL_P<6> RWLB_N<6> RWLB_P<6> RWL_N<6> 43 44 RWL_P<5> RWLB_N<5> RWLB_P<5> RWL_N<5> RWL_P<4> RWLB_N<4> RWLB_P<4> RWL_N<4> RBL<0> 18 121 122
+ RBL<1> 20 124 125 RBL<2> 21 127 128 RBL<3> 22 130 131 RBL<4> 23 133 134 RBL<5> 24 136 137
+ RBL<6> 25 139 140 RBL<7> 26 142 143 RBL<8> 27 145 146 RBL<9> 28 148 149 RBL<10> 29 151 152
+ RBL<11> 30 154 155 RBL<12> 31 157 158 RBL<13> 32 160 161 RBL<14> 33 163 164 RBL<15> 34 166 167
+ VSS! VDD!
+ ICV_10 $T=0 44550 0 0 $X=0 $Y=44560
X8 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 35 45 RWL_P<3> RWLB_N<3>
+ RWLB_P<3> RWL_N<3> RWL_P<2> RWLB_N<2> RWLB_P<2> RWL_N<2> 46 47 RWL_P<1> RWLB_N<1> RWLB_P<1> RWL_N<1> RWL_P<0> RWLB_N<0> RWLB_P<0> RWL_N<0> RBL<0> 18 121 122
+ RBL<1> 20 124 125 RBL<2> 21 127 128 RBL<3> 22 130 131 RBL<4> 23 133 134 RBL<5> 24 136 137
+ RBL<6> 25 139 140 RBL<7> 26 142 143 RBL<8> 27 145 146 RBL<9> 28 148 149 RBL<10> 29 151 152
+ RBL<11> 30 154 155 RBL<12> 31 157 158 RBL<13> 32 160 161 RBL<14> 33 163 164 RBL<15> 34 166 167
+ VSS! VDD!
+ ICV_10 $T=0 66950 0 0 $X=0 $Y=66960
.ENDS
***************************************
