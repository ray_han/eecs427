* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT ADC3
** N=40 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT decoder_1st
** N=26 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT decoder_2st
** N=28 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT decoder_3rd
** N=46 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT decoder_4th
** N=73 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_1
** N=66 EP=0 IP=73 FDC=0
.ENDS
***************************************
.SUBCKT ICV_2
** N=98 EP=0 IP=132 FDC=0
.ENDS
***************************************
.SUBCKT ICV_3
** N=58 EP=0 IP=78 FDC=0
.ENDS
***************************************
.SUBCKT DECODER_WORST
** N=513 EP=0 IP=584 FDC=0
.ENDS
***************************************
.SUBCKT WBL_Driver_1bit
** N=51 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT REN_Driver_1bit
** N=64 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_4
** N=52 EP=0 IP=84 FDC=0
.ENDS
***************************************
.SUBCKT ICV_5
** N=60 EP=0 IP=72 FDC=0
.ENDS
***************************************
.SUBCKT BITCELL
** N=79 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_6
** N=48 EP=0 IP=56 FDC=0
.ENDS
***************************************
.SUBCKT ICV_7
** N=88 EP=0 IP=96 FDC=0
.ENDS
***************************************
.SUBCKT SRAM_1WORD
** N=328 EP=0 IP=352 FDC=0
.ENDS
***************************************
.SUBCKT RWL_Driver_1bit
** N=137 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_8
** N=360 EP=0 IP=368 FDC=0
.ENDS
***************************************
.SUBCKT ICV_9
** N=508 EP=0 IP=720 FDC=0
.ENDS
***************************************
.SUBCKT ICV_10
** N=516 EP=0 IP=728 FDC=0
.ENDS
***************************************
.SUBCKT ADC_Mux
** N=346 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ADC3_tp2
** N=44 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ADC
** N=53 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT XNOR_SRAM_16X16_ADC_MUXWBuffer
** N=5004 EP=0 IP=2040 FDC=0
.ENDS
***************************************
