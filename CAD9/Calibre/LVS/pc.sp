* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT NAND2X1TR B VSS Y A VDD
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT DFFTRX1TR D RN CK Q VSS VDD QN
** N=17 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI211X1TR A1 A0 VSS B0 C0 VDD Y
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT DFFQX1TR D CK VSS VDD Q
** N=13 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OR2X2TR A B VSS VDD Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT INVX2TR A VSS VDD Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR2BX1TR AN B VDD Y VSS
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI22X1TR B1 VDD B0 A0 A1 VSS Y
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKINVX2TR A VSS VDD Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI21X1TR A1 A0 VDD B0 VSS Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT XOR2X2TR B VSS A VDD Y
** N=10 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT XNOR2X2TR B A VSS VDD Y
** N=10 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR2X1TR B VDD A Y VSS
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI21X1TR A1 VSS A0 B0 VDD Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKXOR2X2TR A B VSS VDD Y
** N=11 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKINVX1TR A VSS VDD Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI21X2TR A0 A1 VSS B0 Y VDD
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AND2X2TR A B VDD VSS Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND2BX1TR AN B VSS Y VDD
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR3X1TR C VDD B A VSS Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT INVX16TR A Y VSS VDD
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKBUFX20TR A VSS Y VDD
** N=7 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR3BX2TR Y B VSS C AN VDD
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT XNOR2X1TR B A Y VDD VSS
** N=10 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT XOR2X1TR A B Y VDD VSS
** N=10 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AND2X1TR A B VSS VDD Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OR2X1TR VSS A B VDD Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI21X2TR A1 VDD A0 B0 VSS Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT pc RF_in<9> RF_in<8> RF_in<5> RF_in<6> VDD VSS addr<0> addr<1> addr<2> addr<3> addr<5> addr<6> addr<8> addr<9> clk Rlink<0> Rlink<1> addr<4> Rlink<2> Rlink<3>
+ addr<7> scan_en Rlink<5> Rlink<8> Rlink<6> resetn stall jmp disp<2> disp<3> disp<5> Rlink<7> Rlink<9> RF_in<3> RF_in<4> RF_in<0> RF_in<2> RF_in<1> RF_in<7> Rlink<4>
+ disp<0> br disp<1> disp<6> disp<4> disp<7> scan_in disp<8> disp<9> scan_out
** N=186 EP=50 IP=1014 FDC=0
X0 addr<2> VSS 18 25 VDD NAND2X1TR $T=14000 32400 0 180 $X=12060 $Y=28400
X1 addr<1> VSS 131 28 VDD NAND2X1TR $T=15200 39600 1 180 $X=13260 $Y=39320
X2 18 VSS 29 17 VDD NAND2X1TR $T=14000 25200 0 0 $X=13660 $Y=24920
X3 31 VSS 32 164 VDD NAND2X1TR $T=16400 18000 0 180 $X=14460 $Y=14000
X4 131 VSS 133 26 VDD NAND2X1TR $T=15600 39600 0 0 $X=15260 $Y=39320
X5 addr<3> VSS 31 37 VDD NAND2X1TR $T=16800 18000 0 0 $X=16460 $Y=17720
X6 addr<0> VSS 36 53 VDD NAND2X1TR $T=24400 39600 0 0 $X=24060 $Y=39320
X7 addr<5> VSS 66 64 VDD NAND2X1TR $T=38800 25200 0 0 $X=38460 $Y=24920
X8 141 VSS 60 63 VDD NAND2X1TR $T=40800 18000 1 0 $X=40460 $Y=14000
X9 addr<4> VSS 141 73 VDD NAND2X1TR $T=43200 25200 1 0 $X=42860 $Y=21200
X10 66 VSS 58 166 VDD NAND2X1TR $T=45200 25200 1 0 $X=44860 $Y=21200
X11 br VSS 84 80 VDD NAND2X1TR $T=50400 39600 0 180 $X=48460 $Y=35600
X12 87 VSS 76 85 VDD NAND2X1TR $T=53600 10800 0 180 $X=51660 $Y=6800
X13 151 VSS 149 82 VDD NAND2X1TR $T=58400 18000 0 180 $X=56460 $Y=14000
X14 addr<6> VSS 87 98 VDD NAND2X1TR $T=63600 18000 0 0 $X=63260 $Y=17720
X15 102 VSS 100 167 VDD NAND2X1TR $T=66000 18000 1 0 $X=65660 $Y=14000
X16 addr<7> VSS 102 105 VDD NAND2X1TR $T=66800 18000 0 0 $X=66460 $Y=17720
X17 157 VSS 110 112 VDD NAND2X1TR $T=75200 25200 0 180 $X=73260 $Y=21200
X18 addr<8> VSS 157 113 VDD NAND2X1TR $T=74400 32400 1 0 $X=74060 $Y=28400
X19 160 VSS 111 114 VDD NAND2X1TR $T=76400 39600 0 180 $X=74460 $Y=35600
X20 addr<9> VSS 160 119 VDD NAND2X1TR $T=76400 39600 1 0 $X=76060 $Y=35600
X21 RF_in<3> VSS 139 89 VDD NAND2X1TR $T=96400 18000 0 180 $X=94460 $Y=14000
X22 RF_in<4> VSS 56 89 VDD NAND2X1TR $T=98000 25200 1 180 $X=96060 $Y=24920
X23 RF_in<2> VSS 49 89 VDD NAND2X1TR $T=98400 32400 0 180 $X=96460 $Y=28400
X24 RF_in<0> VSS 103 89 VDD NAND2X1TR $T=99200 32400 1 180 $X=97260 $Y=32120
X25 RF_in<8> VSS 123 89 VDD NAND2X1TR $T=99600 25200 1 180 $X=97660 $Y=24920
X26 RF_in<5> VSS 67 89 VDD NAND2X1TR $T=100000 32400 0 180 $X=98060 $Y=28400
X27 RF_in<1> VSS 44 89 VDD NAND2X1TR $T=100000 39600 0 180 $X=98060 $Y=35600
X28 RF_in<9> VSS 124 89 VDD NAND2X1TR $T=100800 32400 1 180 $X=98860 $Y=32120
X29 RF_in<7> VSS 115 89 VDD NAND2X1TR $T=101200 25200 1 180 $X=99260 $Y=24920
X30 RF_in<6> VSS 83 89 VDD NAND2X1TR $T=101600 32400 0 180 $X=99660 $Y=28400
X31 addr<9> scan_en 39 scan_out VSS VDD 186 DFFTRX1TR $T=82000 39600 0 0 $X=81660 $Y=39320
X32 43 134 VSS 137 44 VDD 135 OAI211X1TR $T=21600 32400 0 0 $X=21260 $Y=32120
X33 43 40 VSS 138 49 VDD 42 OAI211X1TR $T=23200 32400 1 0 $X=22860 $Y=28400
X34 43 45 VSS 47 139 VDD 46 OAI211X1TR $T=24400 18000 1 0 $X=24060 $Y=14000
X35 43 55 VSS 54 56 VDD 50 OAI211X1TR $T=28800 18000 0 0 $X=28460 $Y=17720
X36 43 65 VSS 69 67 VDD 142 OAI211X1TR $T=40400 32400 0 0 $X=40060 $Y=32120
X37 43 78 VSS 81 83 VDD 144 OAI211X1TR $T=46400 32400 1 0 $X=46060 $Y=28400
X38 43 70 VSS 104 103 VDD 168 OAI211X1TR $T=64800 39600 1 0 $X=64460 $Y=35600
X39 43 109 VSS 158 115 VDD 116 OAI211X1TR $T=77600 18000 1 180 $X=74860 $Y=17720
X40 43 120 VSS 117 123 VDD 162 OAI211X1TR $T=78800 25200 1 0 $X=78460 $Y=21200
X41 43 122 VSS 118 124 VDD 121 OAI211X1TR $T=80400 32400 0 0 $X=80060 $Y=32120
X42 42 39 VSS VDD addr<2> DFFQX1TR $T=22400 32400 0 180 $X=15260 $Y=28400
X43 46 39 VSS VDD addr<3> DFFQX1TR $T=23600 18000 0 180 $X=16460 $Y=14000
X44 135 39 VSS VDD addr<1> DFFQX1TR $T=22800 39600 1 0 $X=22460 $Y=35600
X45 50 39 VSS VDD addr<4> DFFQX1TR $T=26800 18000 1 0 $X=26460 $Y=14000
X46 142 39 VSS VDD addr<5> DFFQX1TR $T=40800 25200 0 0 $X=40460 $Y=24920
X47 144 39 VSS VDD addr<6> DFFQX1TR $T=48400 25200 0 0 $X=48060 $Y=24920
X48 168 39 VSS VDD addr<0> DFFQX1TR $T=67200 39600 1 0 $X=66860 $Y=35600
X49 116 39 VSS VDD addr<7> DFFQX1TR $T=82000 18000 0 180 $X=74860 $Y=14000
X50 121 39 VSS VDD addr<9> DFFQX1TR $T=78800 32400 1 0 $X=78460 $Y=28400
X51 162 39 VSS VDD addr<8> DFFQX1TR $T=79200 18000 0 0 $X=78860 $Y=17720
X52 84 disp<0> VSS VDD 53 OR2X2TR $T=49600 39600 1 180 $X=47260 $Y=39320
X53 113 addr<8> VSS VDD 112 OR2X2TR $T=77600 25200 1 180 $X=75260 $Y=24920
X54 119 addr<9> VSS VDD 114 OR2X2TR $T=80000 39600 0 180 $X=77660 $Y=35600
X55 132 VSS VDD 19 INVX2TR $T=15600 25200 0 0 $X=15260 $Y=24920
X56 Rlink<3> VSS VDD 45 INVX2TR $T=22800 18000 0 0 $X=22460 $Y=17720
X57 Rlink<5> VSS VDD 65 INVX2TR $T=39200 32400 0 0 $X=38860 $Y=32120
X58 71 VSS VDD 63 INVX2TR $T=43600 18000 0 180 $X=42060 $Y=14000
X59 Rlink<6> VSS VDD 78 INVX2TR $T=44800 32400 1 0 $X=44460 $Y=28400
X60 33 VSS VDD 61 INVX2TR $T=46000 18000 0 0 $X=45660 $Y=17720
X61 77 VSS VDD 150 INVX2TR $T=59200 10800 0 180 $X=57660 $Y=6800
X62 Rlink<7> VSS VDD 109 INVX2TR $T=71600 18000 0 0 $X=71260 $Y=17720
X63 Rlink<9> VSS VDD 122 INVX2TR $T=76400 32400 0 0 $X=76060 $Y=32120
X64 Rlink<8> VSS VDD 120 INVX2TR $T=77600 25200 1 0 $X=77260 $Y=21200
X65 disp<2> 84 VDD 25 VSS NOR2BX1TR $T=52000 32400 1 180 $X=49660 $Y=32120
X66 disp<1> 84 VDD 28 VSS NOR2BX1TR $T=53200 39600 1 180 $X=50860 $Y=39320
X67 disp<3> 84 VDD 37 VSS NOR2BX1TR $T=56000 39600 0 180 $X=53660 $Y=35600
X68 disp<5> 84 VDD 64 VSS NOR2BX1TR $T=60000 39600 0 180 $X=57660 $Y=35600
X69 disp<6> 84 VDD 98 VSS NOR2BX1TR $T=64000 39600 0 180 $X=61660 $Y=35600
X70 disp<7> 84 VDD 105 VSS NOR2BX1TR $T=71200 39600 1 180 $X=68860 $Y=39320
X71 disp<8> 84 VDD 113 VSS NOR2BX1TR $T=76400 39600 1 180 $X=74060 $Y=39320
X72 disp<9> 84 VDD 119 VSS NOR2BX1TR $T=78400 39600 1 180 $X=76060 $Y=39320
X73 addr<2> VDD scan_en 48 addr<3> VSS 47 AOI22X1TR $T=26400 18000 1 180 $X=23660 $Y=17720
X74 addr<0> VDD scan_en 48 addr<1> VSS 137 AOI22X1TR $T=26800 32400 1 180 $X=24060 $Y=32120
X75 addr<1> VDD scan_en 48 addr<2> VSS 138 AOI22X1TR $T=28000 32400 0 180 $X=25260 $Y=28400
X76 addr<3> VDD scan_en 48 addr<4> VSS 54 AOI22X1TR $T=26400 18000 0 0 $X=26060 $Y=17720
X77 addr<4> VDD scan_en 48 addr<5> VSS 69 AOI22X1TR $T=42400 32400 1 0 $X=42060 $Y=28400
X78 addr<5> VDD scan_en 48 addr<6> VSS 81 AOI22X1TR $T=51200 25200 1 0 $X=50860 $Y=21200
X79 scan_in VDD scan_en 48 addr<0> VSS 104 AOI22X1TR $T=74400 39600 1 180 $X=71660 $Y=39320
X80 addr<6> VDD scan_en 48 addr<7> VSS 158 AOI22X1TR $T=72800 18000 0 0 $X=72460 $Y=17720
X81 addr<7> VDD scan_en 48 addr<8> VSS 117 AOI22X1TR $T=75200 25200 1 0 $X=74860 $Y=21200
X82 addr<8> VDD scan_en 48 addr<9> VSS 118 AOI22X1TR $T=78800 32400 0 180 $X=76060 $Y=28400
X83 15 VSS VDD 17 CLKINVX2TR $T=8000 25200 1 0 $X=7660 $Y=21200
X84 21 VSS VDD 164 CLKINVX2TR $T=13600 18000 1 0 $X=13260 $Y=14000
X85 Rlink<2> VSS VDD 40 CLKINVX2TR $T=20400 32400 0 0 $X=20060 $Y=32120
X86 Rlink<1> VSS VDD 134 CLKINVX2TR $T=20800 39600 0 0 $X=20460 $Y=39320
X87 Rlink<4> VSS VDD 55 CLKINVX2TR $T=32400 18000 1 180 $X=30860 $Y=17720
X88 Rlink<0> VSS VDD 70 CLKINVX2TR $T=36800 39600 0 0 $X=36460 $Y=39320
X89 141 VSS VDD 59 CLKINVX2TR $T=41200 18000 1 180 $X=39660 $Y=17720
X90 74 VSS VDD 166 CLKINVX2TR $T=48000 25200 0 180 $X=46460 $Y=21200
X91 90 VSS VDD 85 CLKINVX2TR $T=54800 10800 0 180 $X=53260 $Y=6800
X92 82 VSS VDD 92 CLKINVX2TR $T=58000 10800 0 180 $X=56460 $Y=6800
X93 99 VSS VDD 167 CLKINVX2TR $T=68800 18000 0 180 $X=67260 $Y=14000
X94 157 VSS VDD 107 CLKINVX2TR $T=75600 25200 1 180 $X=74060 $Y=24920
X95 63 61 VDD 59 VSS 140 AOI21X1TR $T=39200 18000 1 180 $X=36860 $Y=17720
X96 82 61 VDD 77 VSS 143 AOI21X1TR $T=48400 10800 1 180 $X=46060 $Y=10520
X97 86 61 VDD 91 VSS 93 AOI21X1TR $T=53200 10800 0 0 $X=52860 $Y=10520
X98 151 77 VDD 95 VSS 152 AOI21X1TR $T=58800 18000 1 0 $X=58460 $Y=14000
X99 112 96 VDD 107 VSS 106 AOI21X1TR $T=71600 25200 1 180 $X=69260 $Y=24920
X100 111 VSS 106 VDD Rlink<9> XOR2X2TR $T=73600 32400 1 180 $X=68060 $Y=32120
X101 110 96 VSS VDD Rlink<8> XNOR2X2TR $T=73600 25200 0 180 $X=68060 $Y=21200
X102 addr<2> VDD 25 15 VSS NOR2X1TR $T=8800 25200 0 0 $X=8460 $Y=24920
X103 21 VDD 15 20 VSS NOR2X1TR $T=11600 18000 1 180 $X=9660 $Y=17720
X104 addr<1> VDD 28 23 VSS NOR2X1TR $T=15200 32400 1 180 $X=13260 $Y=32120
X105 addr<3> VDD 37 21 VSS NOR2X1TR $T=20400 18000 1 180 $X=18460 $Y=17720
X106 addr<4> VDD 73 71 VSS NOR2X1TR $T=43600 18000 1 0 $X=43260 $Y=14000
X107 74 VDD 71 82 VSS NOR2X1TR $T=45600 18000 1 0 $X=45260 $Y=14000
X108 addr<5> VDD 64 74 VSS NOR2X1TR $T=51200 25200 0 180 $X=49260 $Y=21200
X109 84 VDD 147 73 VSS NOR2X1TR $T=55600 32400 1 180 $X=53660 $Y=32120
X110 90 VDD 92 86 VSS NOR2X1TR $T=56400 10800 0 180 $X=54460 $Y=6800
X111 99 VDD 90 151 VSS NOR2X1TR $T=62000 10800 1 180 $X=60060 $Y=10520
X112 addr<6> VDD 98 90 VSS NOR2X1TR $T=63200 18000 1 180 $X=61260 $Y=17720
X113 addr<7> VDD 105 99 VSS NOR2X1TR $T=70000 18000 1 180 $X=68060 $Y=17720
X114 15 VSS 19 18 VDD 27 OAI21X1TR $T=9200 25200 1 0 $X=8860 $Y=21200
X115 18 VSS 21 31 VDD 24 OAI21X1TR $T=14000 25200 1 0 $X=13660 $Y=21200
X116 141 VSS 74 66 VDD 77 OAI21X1TR $T=44000 18000 0 0 $X=43660 $Y=17720
X117 90 VSS 150 87 VDD 91 OAI21X1TR $T=56800 10800 0 0 $X=56460 $Y=10520
X118 87 VSS 99 102 VDD 95 OAI21X1TR $T=63600 18000 1 0 $X=63260 $Y=14000
X119 143 76 VSS VDD Rlink<6> CLKXOR2X2TR $T=46800 10800 0 180 $X=42860 $Y=6800
X120 93 100 VSS VDD Rlink<7> CLKXOR2X2TR $T=63600 10800 0 0 $X=63260 $Y=10520
X121 23 VSS VDD 26 CLKINVX1TR $T=11200 32400 0 0 $X=10860 $Y=32120
X122 disp<4> VSS VDD 147 CLKINVX1TR $T=61200 39600 1 180 $X=59660 $Y=39320
X123 23 36 VSS 131 132 VDD OAI21X2TR $T=18800 32400 1 180 $X=14860 $Y=32120
X124 33 149 VSS 152 96 VDD OAI21X2TR $T=56800 18000 0 0 $X=56460 $Y=17720
X125 79 jmp VDD VSS 89 AND2X2TR $T=52000 39600 1 0 $X=51660 $Y=35600
X126 jmp 79 VSS 43 VDD NAND2BX1TR $T=46800 39600 1 0 $X=46460 $Y=35600
X127 scan_en VDD stall resetn VSS 79 NOR3X1TR $T=44000 39600 1 0 $X=43660 $Y=35600
X128 resetn 80 VSS VDD INVX16TR $T=47600 39600 1 180 $X=42460 $Y=39320
X129 clk VSS 39 VDD CLKBUFX20TR $T=42800 32400 0 0 $X=42460 $Y=32120
X130 48 resetn VSS scan_en stall VDD NOR3BX2TR $T=40800 39600 1 0 $X=40460 $Y=35600
X131 32 27 Rlink<3> VDD VSS XNOR2X1TR $T=17600 25200 1 0 $X=17260 $Y=21200
X132 60 61 Rlink<4> VDD VSS XNOR2X1TR $T=38800 18000 0 180 $X=35260 $Y=14000
X133 19 29 Rlink<2> VDD VSS XOR2X1TR $T=17200 25200 0 0 $X=16860 $Y=24920
X134 133 36 Rlink<1> VDD VSS XOR2X1TR $T=17600 39600 1 0 $X=17260 $Y=35600
X135 140 58 Rlink<5> VDD VSS XOR2X1TR $T=38400 25200 0 180 $X=34860 $Y=21200
X136 165 36 VSS VDD Rlink<0> AND2X1TR $T=29600 39600 0 0 $X=29260 $Y=39320
X137 VSS 53 addr<0> VDD 165 OR2X1TR $T=27600 39600 0 0 $X=27260 $Y=39320
X138 20 VDD 132 24 VSS 33 AOI21X2TR $T=15600 18000 1 180 $X=11660 $Y=17720
.ENDS
***************************************
