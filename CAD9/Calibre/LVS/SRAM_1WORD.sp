* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT BITCELL RENB RWL_N WWL RWLB_P RWL_P RWLB_N REN RBL WBLB WBL VSS! VDD!
** N=72 EP=12 IP=0 FDC=12
M0 13 REN RBL VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=700 $Y=450 $D=97
M1 RWL_N 15 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=450 $D=97
M2 RWLB_N 16 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=4870 $D=97
M3 16 15 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2120 $Y=4870 $D=97
M4 15 WWL WBLB VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2240 $Y=450 $D=97
M5 WBL WWL 16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2600 $Y=4870 $D=97
M6 VSS! 16 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2720 $Y=450 $D=97
M7 14 RENB RBL VDD! pfet L=1.2e-07 W=8.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=700 $Y=3310 $D=189
M8 RWLB_P 15 14 VDD! pfet L=1.2e-07 W=8.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=1420 $D=189
M9 RWL_P 16 14 VDD! pfet L=1.2e-07 W=8.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=3310 $D=189
M10 16 15 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2120 $Y=3310 $D=189
M11 VDD! 16 15 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1750 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
** N=17 EP=17 IP=24 FDC=24
X0 1 3 4 5 6 7 9 8 10 11 16 17 BITCELL $T=0 0 0 0 $X=0 $Y=-240
X1 2 3 4 5 6 7 13 12 14 15 16 17 BITCELL $T=3200 0 0 0 $X=3200 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27
** N=27 EP=27 IP=34 FDC=48
X0 1 2 5 6 7 8 9 10 11 12 13 14 15 16 17 26 27 ICV_1 $T=0 0 0 0 $X=0 $Y=-240
X1 3 4 5 6 7 8 9 18 19 20 21 22 23 24 25 26 27 ICV_1 $T=6400 0 0 0 $X=6400 $Y=-240
.ENDS
***************************************
.SUBCKT SRAM_1WORD RENB<0> RENB<1> RENB<2> RENB<3> RENB<4> RENB<5> RENB<6> RENB<7> RENB<8> RENB<9> RENB<10> RENB<11> RENB<12> RENB<13> RENB<14> RENB<15> RWL_N WWL RWLB_P RWL_P
+ RWLB_N RBL<0> REN<0> WBLB<0> WBL<0> RBL<1> REN<1> WBLB<1> WBL<1> RBL<2> REN<2> WBLB<2> WBL<2> RBL<3> REN<3> WBLB<3> WBL<3> RBL<4> REN<4> WBLB<4>
+ WBL<4> RBL<5> REN<5> WBLB<5> WBL<5> RBL<6> REN<6> WBLB<6> WBL<6> RBL<7> REN<7> WBLB<7> WBL<7> RBL<8> REN<8> WBLB<8> WBL<8> RBL<9> REN<9> WBLB<9>
+ WBL<9> RBL<10> REN<10> WBLB<10> WBL<10> RBL<11> REN<11> WBLB<11> WBL<11> RBL<12> REN<12> WBLB<12> WBL<12> RBL<13> REN<13> WBLB<13> WBL<13> RBL<14> REN<14> WBLB<14>
+ WBL<14> RBL<15> REN<15> WBLB<15> WBL<15> VSS! VDD!
** N=88 EP=87 IP=108 FDC=192
X0 RENB<0> RENB<1> RENB<2> RENB<3> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<0> REN<0> WBLB<0> WBL<0> RBL<1> REN<1> WBLB<1> WBL<1> RBL<2> REN<2> WBLB<2>
+ WBL<2> RBL<3> REN<3> WBLB<3> WBL<3> VSS! VDD!
+ ICV_2 $T=0 0 0 0 $X=0 $Y=-240
X1 RENB<4> RENB<5> RENB<6> RENB<7> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<4> REN<4> WBLB<4> WBL<4> RBL<5> REN<5> WBLB<5> WBL<5> RBL<6> REN<6> WBLB<6>
+ WBL<6> RBL<7> REN<7> WBLB<7> WBL<7> VSS! VDD!
+ ICV_2 $T=12800 0 0 0 $X=12800 $Y=-240
X2 RENB<8> RENB<9> RENB<10> RENB<11> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<8> REN<8> WBLB<8> WBL<8> RBL<9> REN<9> WBLB<9> WBL<9> RBL<10> REN<10> WBLB<10>
+ WBL<10> RBL<11> REN<11> WBLB<11> WBL<11> VSS! VDD!
+ ICV_2 $T=25600 0 0 0 $X=25600 $Y=-240
X3 RENB<12> RENB<13> RENB<14> RENB<15> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<12> REN<12> WBLB<12> WBL<12> RBL<13> REN<13> WBLB<13> WBL<13> RBL<14> REN<14> WBLB<14>
+ WBL<14> RBL<15> REN<15> WBLB<15> WBL<15> VSS! VDD!
+ ICV_2 $T=38400 0 0 0 $X=38400 $Y=-240
.ENDS
***************************************
