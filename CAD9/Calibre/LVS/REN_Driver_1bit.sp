* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT REN_Driver_1bit RENB REN IN VSS! VDD!
** N=69 EP=5 IP=0 FDC=18
M0 VSS! 4 2 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=450 $D=97
M1 VSS! 4 2 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=4750 $D=97
M2 RENB 2 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=450 $D=97
M3 4 IN VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=4750 $D=97
M4 VSS! 2 RENB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=450 $D=97
M5 RENB 2 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M6 VSS! IN 6 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=4750 $D=97
M7 VSS! 2 RENB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M8 REN 6 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=4750 $D=97
M9 VDD! 4 2 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=1490 $D=189
M10 VDD! 4 2 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=3310 $D=189
M11 RENB 2 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=1490 $D=189
M12 4 IN VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3310 $D=189
M13 VDD! 2 RENB VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=1490 $D=189
M14 RENB 2 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1490 $D=189
M15 VDD! IN 6 VDD! pfet L=1.2e-07 W=6.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3310 $D=189
M16 VDD! 2 RENB VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1490 $D=189
M17 REN 6 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3310 $D=189
.ENDS
***************************************
