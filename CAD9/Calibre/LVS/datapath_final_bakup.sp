* SPICE NETLIST
***************************************

*.CALIBRE ABORT_INFO SUPPLY_ERROR
.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT ICV_94
** N=14 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_93
** N=8 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_92
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_91
** N=1607 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_27
** N=22 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_26
** N=22 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_25
** N=4 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_24
** N=1754 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT nct
** N=8 EP=0 IP=0 FDC=0
*.CALIBRE ISOLATED NETS: DVSS pad ntr0m2 ntr1m2 sdhigh sdlow ng1 ng0
.ENDS
***************************************
.SUBCKT ICV_23
** N=2449 EP=0 IP=48 FDC=0
.ENDS
***************************************
.SUBCKT pct
** N=7 EP=0 IP=0 FDC=0
*.CALIBRE ISOLATED NETS: pad DVDD ptr0m2 ptr1m2 poff pg0 pg1
.ENDS
***************************************
.SUBCKT ICV_22
** N=2970 EP=0 IP=21 FDC=0
.ENDS
***************************************
.SUBCKT ICV_21
** N=2216 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_20
** N=2548 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT PICS VSS DVDD P DVSS VDD IE Y 8 9
** N=49 EP=9 IP=131 FDC=0
*.CALIBRE WARNING OPEN Open circuit(s) detected by extraction in this cell. See extraction report for details.
.ENDS
***************************************
.SUBCKT ICV_28 1
** N=10 EP=1 IP=10 FDC=0
X0 2 3 4 5 6 7 8 9 10 PICS $T=0 0 0 0 $X=-2 $Y=0
.ENDS
***************************************
.SUBCKT ICV_89
** N=5 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_88
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_87
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_90
** N=10299 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ADC CLK Vin Vref OUT VSS! VDD!
** N=86 EP=6 IP=0 FDC=15
M0 VSS! 8 OUT VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=490 $D=97
M1 9 8 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1660 $Y=490 $D=97
M2 10 8 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=490 $D=97
M3 10 CLK VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3260 $Y=770 $D=97
M4 11 7 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4460 $Y=490 $D=97
M5 8 7 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5020 $Y=490 $D=97
M6 12 7 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6140 $Y=490 $D=97
M7 VDD! 8 OUT VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=3040 $D=189
M8 7 Vin 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1660 $Y=3040 $D=189
M9 VDD! 8 7 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=3040 $D=189
M10 7 CLK VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2780 $Y=3040 $D=189
M11 VDD! CLK 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3900 $Y=3040 $D=189
M12 8 7 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4460 $Y=3040 $D=189
M13 11 Vref 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5020 $Y=3040 $D=189
M14 12 7 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6140 $Y=3040 $D=189
.ENDS
***************************************
.SUBCKT ADC3_tp2 CLK Vin Vref OUT VSS! VDD!
** N=89 EP=6 IP=0 FDC=14
M0 VSS! 9 OUT VSS! nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=380 $Y=1330 $D=97
M1 10 8 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=1610 $D=97
M2 9 Vin 10 VSS! nfet L=5e-07 W=1.2e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=690 $D=97
M3 7 CLK VSS! VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=2660 $D=97
M4 VSS! CLK 7 VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=3260 $D=97
M5 11 Vref 8 VSS! nfet L=5e-07 W=1.2e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4280 $Y=680 $D=97
M6 7 9 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5220 $Y=1610 $D=97
M7 12 8 VSS! VSS! nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6300 $Y=1330 $D=97
M8 VDD! 9 OUT VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=380 $Y=4010 $D=189
M9 VDD! 8 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=4010 $D=189
M10 9 CLK VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=4010 $D=189
M11 VDD! CLK 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4660 $Y=4010 $D=189
M12 8 9 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5220 $Y=4010 $D=189
M13 12 8 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6300 $Y=4010 $D=189
.ENDS
***************************************
.SUBCKT ADC3 CLK Vin Vref OUT VSS! VDD!
** N=81 EP=6 IP=0 FDC=13
M0 VSS! 8 OUT VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=580 $Y=490 $D=97
M1 11 Vin 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1660 $Y=490 $D=97
M2 7 9 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=490 $D=97
M3 7 CLK VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3260 $Y=830 $D=97
M4 12 8 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4460 $Y=490 $D=97
M5 9 Vref 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5020 $Y=490 $D=97
M6 10 9 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6100 $Y=490 $D=97
M7 VDD! 8 OUT VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=580 $Y=2240 $D=189
M8 8 CLK VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1660 $Y=2240 $D=189
M9 VDD! 9 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=2240 $D=189
M10 9 8 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4460 $Y=2240 $D=189
M11 VDD! CLK 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5020 $Y=2240 $D=189
M12 10 9 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6100 $Y=2240 $D=189
.ENDS
***************************************
.SUBCKT 32ADC CLK Vin VSS! Vref<0> Vref<1> Vref<2> Vref<3> Vref<4> Vref<5> Vref<6> Vref<7> Vref<8> Vref<9> Vref<10> Vref<11> Vref<12> Vref<13> Vref<14> Vref<15> Vref<16>
+ Vref<17> Vref<18> Vref<19> Vref<20> Vref<21> Vref<22> Vref<23> Vref<24> Vref<25> Vref<26> Vref<27> Vref<28> Vref<29> Vref<30> Vref<31> VDD! OUT<0> OUT<1> OUT<2> OUT<3>
+ OUT<4> OUT<5> OUT<6> OUT<7> OUT<8> OUT<9> OUT<10> OUT<11> OUT<12> OUT<13> OUT<14> OUT<15> OUT<16> OUT<17> OUT<18> OUT<19> OUT<20> OUT<21> OUT<22> OUT<23>
+ OUT<24> OUT<25> OUT<26> OUT<27> OUT<28> OUT<29> OUT<30> OUT<31>
** N=3080 EP=68 IP=192 FDC=439
X0 CLK Vin Vref<0> OUT<0> VSS! VDD! ADC $T=0 6800 0 270 $X=-400 $Y=-770
X1 CLK Vin Vref<1> OUT<1> VSS! VDD! ADC $T=11200 6800 1 270 $X=6710 $Y=-770
X2 CLK Vin Vref<2> OUT<2> VSS! VDD! ADC $T=14400 6800 0 270 $X=14000 $Y=-770
X3 CLK Vin Vref<3> OUT<3> VSS! VDD! ADC $T=25600 6800 1 270 $X=21110 $Y=-770
X4 CLK Vin Vref<4> OUT<4> VSS! VDD! ADC $T=28800 6800 0 270 $X=28400 $Y=-770
X5 CLK Vin Vref<5> OUT<5> VSS! VDD! ADC $T=40000 6800 1 270 $X=35510 $Y=-770
X6 CLK Vin Vref<6> OUT<6> VSS! VDD! ADC $T=43200 6800 0 270 $X=42800 $Y=-770
X7 CLK Vin Vref<7> OUT<7> VSS! VDD! ADC $T=54400 6800 1 270 $X=49910 $Y=-770
X8 CLK Vin Vref<8> OUT<8> VSS! VDD! ADC $T=57600 6800 0 270 $X=57200 $Y=-770
X9 CLK Vin Vref<9> OUT<9> VSS! VDD! ADC $T=68800 6800 1 270 $X=64310 $Y=-770
X10 CLK Vin Vref<10> OUT<10> VSS! VDD! ADC3_tp2 $T=72000 6800 0 270 $X=71540 $Y=-500
X11 CLK Vin Vref<11> OUT<11> VSS! VDD! ADC3_tp2 $T=84800 6800 1 270 $X=79710 $Y=-500
X12 CLK Vin Vref<12> OUT<12> VSS! VDD! ADC3_tp2 $T=88000 6800 0 270 $X=87540 $Y=-500
X13 CLK Vin Vref<13> OUT<13> VSS! VDD! ADC3 $T=99200 6800 1 270 $X=95720 $Y=-310
X14 CLK Vin Vref<14> OUT<14> VSS! VDD! ADC3 $T=102400 6800 0 270 $X=102140 $Y=-310
X15 CLK Vin Vref<15> OUT<15> VSS! VDD! ADC3 $T=112000 6800 1 270 $X=108520 $Y=-310
X16 CLK Vin Vref<16> OUT<16> VSS! VDD! ADC3 $T=115200 6800 0 270 $X=114940 $Y=-310
X17 CLK Vin Vref<17> OUT<17> VSS! VDD! ADC3 $T=124800 6800 1 270 $X=121320 $Y=-310
X18 CLK Vin Vref<18> OUT<18> VSS! VDD! ADC3 $T=128000 6800 0 270 $X=127740 $Y=-310
X19 CLK Vin Vref<19> OUT<19> VSS! VDD! ADC3 $T=137600 6800 1 270 $X=134120 $Y=-310
X20 CLK Vin Vref<20> OUT<20> VSS! VDD! ADC3 $T=140800 6800 0 270 $X=140540 $Y=-310
X21 CLK Vin Vref<21> OUT<21> VSS! VDD! ADC3 $T=150400 6800 1 270 $X=146920 $Y=-310
X22 CLK Vin Vref<22> OUT<22> VSS! VDD! ADC3 $T=153600 6800 0 270 $X=153340 $Y=-310
X23 CLK Vin Vref<23> OUT<23> VSS! VDD! ADC3 $T=163200 6800 1 270 $X=159720 $Y=-310
X24 CLK Vin Vref<24> OUT<24> VSS! VDD! ADC3 $T=166400 6800 0 270 $X=166140 $Y=-310
X25 CLK Vin Vref<25> OUT<25> VSS! VDD! ADC3 $T=176000 6800 1 270 $X=172520 $Y=-310
X26 CLK Vin Vref<26> OUT<26> VSS! VDD! ADC3 $T=179200 6800 0 270 $X=178940 $Y=-310
X27 CLK Vin Vref<27> OUT<27> VSS! VDD! ADC3 $T=188800 6800 1 270 $X=185320 $Y=-310
X28 CLK Vin Vref<28> OUT<28> VSS! VDD! ADC3 $T=192000 6800 0 270 $X=191740 $Y=-310
X29 CLK Vin Vref<29> OUT<29> VSS! VDD! ADC3 $T=201600 6800 1 270 $X=198120 $Y=-310
X30 CLK Vin Vref<30> OUT<30> VSS! VDD! ADC3 $T=204800 6800 0 270 $X=204540 $Y=-310
X31 CLK Vin Vref<31> OUT<31> VSS! VDD! ADC3 $T=214400 6800 1 270 $X=210920 $Y=-310
.ENDS
***************************************
.SUBCKT ADC_Mux RBL<0> Vin RBL<1> RBL<2> RBL<3> RBL<4> RBL<5> RBL<6> RBL<7> RBL<8> RBL<9> RBL<10> RBL<11> RBL<12> RBL<13> RBL<14> RBL<15> Col_Sel<0> Col_Sel<1> Col_Sel<2>
+ Col_Sel<3> Col_Sel<4> Col_Sel<5> Col_Sel<6> Col_Sel<7> Col_Sel<8> Col_Sel<9> Col_Sel<10> Col_Sel<11> Col_Sel<12> Col_Sel<13> Col_Sel<14> Col_Sel<15> VSS! VDD!
** N=691 EP=35 IP=0 FDC=128
M0 Vin Col_Sel<0> RBL<0> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=710 $D=97
M1 RBL<0> Col_Sel<0> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1010 $Y=710 $D=97
M2 Vin Col_Sel<0> RBL<0> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=710 $D=97
M3 VSS! Col_Sel<0> 36 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2620 $Y=1430 $D=97
M4 Vin Col_Sel<1> RBL<1> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3650 $Y=710 $D=97
M5 RBL<1> Col_Sel<1> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=710 $D=97
M6 Vin Col_Sel<1> RBL<1> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4770 $Y=710 $D=97
M7 VSS! Col_Sel<1> 37 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5820 $Y=1430 $D=97
M8 Vin Col_Sel<2> RBL<2> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6850 $Y=710 $D=97
M9 RBL<2> Col_Sel<2> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7410 $Y=710 $D=97
M10 Vin Col_Sel<2> RBL<2> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7970 $Y=710 $D=97
M11 VSS! Col_Sel<2> 38 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9020 $Y=1430 $D=97
M12 Vin Col_Sel<3> RBL<3> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10050 $Y=710 $D=97
M13 RBL<3> Col_Sel<3> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10610 $Y=710 $D=97
M14 Vin Col_Sel<3> RBL<3> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11170 $Y=710 $D=97
M15 VSS! Col_Sel<3> 39 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12220 $Y=1430 $D=97
M16 Vin Col_Sel<4> RBL<4> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13250 $Y=710 $D=97
M17 RBL<4> Col_Sel<4> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13810 $Y=710 $D=97
M18 Vin Col_Sel<4> RBL<4> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14370 $Y=710 $D=97
M19 VSS! Col_Sel<4> 40 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=15420 $Y=1430 $D=97
M20 Vin Col_Sel<5> RBL<5> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=16450 $Y=710 $D=97
M21 RBL<5> Col_Sel<5> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17010 $Y=710 $D=97
M22 Vin Col_Sel<5> RBL<5> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17570 $Y=710 $D=97
M23 VSS! Col_Sel<5> 41 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=18620 $Y=1430 $D=97
M24 Vin Col_Sel<6> RBL<6> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=19650 $Y=710 $D=97
M25 RBL<6> Col_Sel<6> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20210 $Y=710 $D=97
M26 Vin Col_Sel<6> RBL<6> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20770 $Y=710 $D=97
M27 VSS! Col_Sel<6> 42 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=21820 $Y=1430 $D=97
M28 Vin Col_Sel<7> RBL<7> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=22850 $Y=710 $D=97
M29 RBL<7> Col_Sel<7> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23410 $Y=710 $D=97
M30 Vin Col_Sel<7> RBL<7> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23970 $Y=710 $D=97
M31 VSS! Col_Sel<7> 43 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=25020 $Y=1430 $D=97
M32 Vin Col_Sel<8> RBL<8> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26050 $Y=710 $D=97
M33 RBL<8> Col_Sel<8> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26610 $Y=710 $D=97
M34 Vin Col_Sel<8> RBL<8> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=27170 $Y=710 $D=97
M35 VSS! Col_Sel<8> 44 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=28220 $Y=1430 $D=97
M36 Vin Col_Sel<9> RBL<9> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29250 $Y=710 $D=97
M37 RBL<9> Col_Sel<9> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29810 $Y=710 $D=97
M38 Vin Col_Sel<9> RBL<9> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=30370 $Y=710 $D=97
M39 VSS! Col_Sel<9> 45 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=31420 $Y=1430 $D=97
M40 Vin Col_Sel<10> RBL<10> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=32450 $Y=710 $D=97
M41 RBL<10> Col_Sel<10> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33010 $Y=710 $D=97
M42 Vin Col_Sel<10> RBL<10> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33570 $Y=710 $D=97
M43 VSS! Col_Sel<10> 46 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=34620 $Y=1430 $D=97
M44 Vin Col_Sel<11> RBL<11> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=35650 $Y=710 $D=97
M45 RBL<11> Col_Sel<11> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36210 $Y=710 $D=97
M46 Vin Col_Sel<11> RBL<11> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36770 $Y=710 $D=97
M47 VSS! Col_Sel<11> 47 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=37820 $Y=1430 $D=97
M48 Vin Col_Sel<12> RBL<12> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38850 $Y=710 $D=97
M49 RBL<12> Col_Sel<12> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39410 $Y=710 $D=97
M50 Vin Col_Sel<12> RBL<12> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39970 $Y=710 $D=97
M51 VSS! Col_Sel<12> 48 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=41020 $Y=1430 $D=97
M52 Vin Col_Sel<13> RBL<13> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42050 $Y=710 $D=97
M53 RBL<13> Col_Sel<13> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42610 $Y=710 $D=97
M54 Vin Col_Sel<13> RBL<13> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=43170 $Y=710 $D=97
M55 VSS! Col_Sel<13> 49 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=44220 $Y=1430 $D=97
M56 Vin Col_Sel<14> RBL<14> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45250 $Y=710 $D=97
M57 RBL<14> Col_Sel<14> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45810 $Y=710 $D=97
M58 Vin Col_Sel<14> RBL<14> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=46370 $Y=710 $D=97
M59 VSS! Col_Sel<14> 50 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=47420 $Y=1430 $D=97
M60 Vin Col_Sel<15> RBL<15> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=48450 $Y=710 $D=97
M61 RBL<15> Col_Sel<15> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49010 $Y=710 $D=97
M62 Vin Col_Sel<15> RBL<15> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49570 $Y=710 $D=97
M63 VSS! Col_Sel<15> 51 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=50620 $Y=1430 $D=97
M64 Vin 36 RBL<0> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2890 $D=189
M65 RBL<0> 36 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1010 $Y=2890 $D=189
M66 Vin 36 RBL<0> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=2890 $D=189
M67 VDD! Col_Sel<0> 36 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2620 $Y=2890 $D=189
M68 Vin 37 RBL<1> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3650 $Y=2890 $D=189
M69 RBL<1> 37 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=2890 $D=189
M70 Vin 37 RBL<1> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4770 $Y=2890 $D=189
M71 VDD! Col_Sel<1> 37 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5820 $Y=2890 $D=189
M72 Vin 38 RBL<2> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6850 $Y=2890 $D=189
M73 RBL<2> 38 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7410 $Y=2890 $D=189
M74 Vin 38 RBL<2> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7970 $Y=2890 $D=189
M75 VDD! Col_Sel<2> 38 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9020 $Y=2890 $D=189
M76 Vin 39 RBL<3> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10050 $Y=2890 $D=189
M77 RBL<3> 39 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10610 $Y=2890 $D=189
M78 Vin 39 RBL<3> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11170 $Y=2890 $D=189
M79 VDD! Col_Sel<3> 39 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12220 $Y=2890 $D=189
M80 Vin 40 RBL<4> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13250 $Y=2890 $D=189
M81 RBL<4> 40 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13810 $Y=2890 $D=189
M82 Vin 40 RBL<4> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14370 $Y=2890 $D=189
M83 VDD! Col_Sel<4> 40 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=15420 $Y=2890 $D=189
M84 Vin 41 RBL<5> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=16450 $Y=2890 $D=189
M85 RBL<5> 41 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17010 $Y=2890 $D=189
M86 Vin 41 RBL<5> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17570 $Y=2890 $D=189
M87 VDD! Col_Sel<5> 41 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=18620 $Y=2890 $D=189
M88 Vin 42 RBL<6> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=19650 $Y=2890 $D=189
M89 RBL<6> 42 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20210 $Y=2890 $D=189
M90 Vin 42 RBL<6> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20770 $Y=2890 $D=189
M91 VDD! Col_Sel<6> 42 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=21820 $Y=2890 $D=189
M92 Vin 43 RBL<7> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=22850 $Y=2890 $D=189
M93 RBL<7> 43 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23410 $Y=2890 $D=189
M94 Vin 43 RBL<7> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23970 $Y=2890 $D=189
M95 VDD! Col_Sel<7> 43 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=25020 $Y=2890 $D=189
M96 Vin 44 RBL<8> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26050 $Y=2890 $D=189
M97 RBL<8> 44 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26610 $Y=2890 $D=189
M98 Vin 44 RBL<8> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=27170 $Y=2890 $D=189
M99 VDD! Col_Sel<8> 44 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=28220 $Y=2890 $D=189
M100 Vin 45 RBL<9> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29250 $Y=2890 $D=189
M101 RBL<9> 45 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29810 $Y=2890 $D=189
M102 Vin 45 RBL<9> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=30370 $Y=2890 $D=189
M103 VDD! Col_Sel<9> 45 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=31420 $Y=2890 $D=189
M104 Vin 46 RBL<10> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=32450 $Y=2890 $D=189
M105 RBL<10> 46 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33010 $Y=2890 $D=189
M106 Vin 46 RBL<10> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33570 $Y=2890 $D=189
M107 VDD! Col_Sel<10> 46 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=34620 $Y=2890 $D=189
M108 Vin 47 RBL<11> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=35650 $Y=2890 $D=189
M109 RBL<11> 47 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36210 $Y=2890 $D=189
M110 Vin 47 RBL<11> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36770 $Y=2890 $D=189
M111 VDD! Col_Sel<11> 47 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=37820 $Y=2890 $D=189
M112 Vin 48 RBL<12> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38850 $Y=2890 $D=189
M113 RBL<12> 48 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39410 $Y=2890 $D=189
M114 Vin 48 RBL<12> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39970 $Y=2890 $D=189
M115 VDD! Col_Sel<12> 48 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=41020 $Y=2890 $D=189
M116 Vin 49 RBL<13> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42050 $Y=2890 $D=189
M117 RBL<13> 49 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42610 $Y=2890 $D=189
M118 Vin 49 RBL<13> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=43170 $Y=2890 $D=189
M119 VDD! Col_Sel<13> 49 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=44220 $Y=2890 $D=189
M120 Vin 50 RBL<14> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45250 $Y=2890 $D=189
M121 RBL<14> 50 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45810 $Y=2890 $D=189
M122 Vin 50 RBL<14> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=46370 $Y=2890 $D=189
M123 VDD! Col_Sel<14> 50 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=47420 $Y=2890 $D=189
M124 Vin 51 RBL<15> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=48450 $Y=2890 $D=189
M125 RBL<15> 51 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49010 $Y=2890 $D=189
M126 Vin 51 RBL<15> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49570 $Y=2890 $D=189
M127 VDD! Col_Sel<15> 51 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=50620 $Y=2890 $D=189
.ENDS
***************************************
.SUBCKT special_inverter IN<15> OUT<15> IN<14> OUT<14> IN<13> OUT<13> IN<12> OUT<12> IN<11> OUT<11> IN<10> OUT<10> IN<9> OUT<9> IN<8> OUT<8> IN<7> OUT<7> IN<6> OUT<6>
+ IN<5> OUT<5> IN<4> OUT<4> IN<3> OUT<3> IN<2> OUT<2> IN<1> OUT<1> IN<0> VDD! VSS! OUT<0>
** N=610 EP=34 IP=0 FDC=96
M0 35 IN<15> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=440 $D=97
M1 OUT<15> 35 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2270 $Y=440 $D=97
M2 36 IN<14> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4380 $Y=440 $D=97
M3 OUT<14> 36 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5470 $Y=440 $D=97
M4 37 IN<13> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7580 $Y=440 $D=97
M5 OUT<13> 37 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8670 $Y=440 $D=97
M6 38 IN<12> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10780 $Y=440 $D=97
M7 OUT<12> 38 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11870 $Y=440 $D=97
M8 39 IN<11> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13980 $Y=440 $D=97
M9 OUT<11> 39 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=15070 $Y=440 $D=97
M10 40 IN<10> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17180 $Y=440 $D=97
M11 OUT<10> 40 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=18270 $Y=440 $D=97
M12 41 IN<9> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20380 $Y=440 $D=97
M13 OUT<9> 41 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=21470 $Y=440 $D=97
M14 42 IN<8> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23580 $Y=440 $D=97
M15 OUT<8> 42 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=24670 $Y=440 $D=97
M16 43 IN<7> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26780 $Y=440 $D=97
M17 OUT<7> 43 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=27870 $Y=440 $D=97
M18 44 IN<6> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29980 $Y=440 $D=97
M19 OUT<6> 44 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=31070 $Y=440 $D=97
M20 45 IN<5> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33180 $Y=440 $D=97
M21 OUT<5> 45 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=34270 $Y=440 $D=97
M22 46 IN<4> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36380 $Y=440 $D=97
M23 OUT<4> 46 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=37470 $Y=440 $D=97
M24 47 IN<3> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39580 $Y=440 $D=97
M25 OUT<3> 47 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40670 $Y=440 $D=97
M26 48 IN<2> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42780 $Y=440 $D=97
M27 OUT<2> 48 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=43870 $Y=440 $D=97
M28 49 IN<1> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45980 $Y=440 $D=97
M29 OUT<1> 49 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=47070 $Y=440 $D=97
M30 50 IN<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49180 $Y=440 $D=97
M31 OUT<0> 50 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=50270 $Y=440 $D=97
M32 VDD! IN<15> 35 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=810 $Y=2690 $D=189
M33 35 IN<15> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1290 $Y=2690 $D=189
M34 VDD! IN<15> 35 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1770 $Y=2690 $D=189
M35 OUT<15> 35 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2270 $Y=3210 $D=189
M36 VDD! IN<14> 36 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4010 $Y=2690 $D=189
M37 36 IN<14> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4490 $Y=2690 $D=189
M38 VDD! IN<14> 36 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4970 $Y=2690 $D=189
M39 OUT<14> 36 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5470 $Y=3210 $D=189
M40 VDD! IN<13> 37 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=2690 $D=189
M41 37 IN<13> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=2690 $D=189
M42 VDD! IN<13> 37 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8170 $Y=2690 $D=189
M43 OUT<13> 37 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8670 $Y=3210 $D=189
M44 VDD! IN<12> 38 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10410 $Y=2690 $D=189
M45 38 IN<12> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10890 $Y=2690 $D=189
M46 VDD! IN<12> 38 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11370 $Y=2690 $D=189
M47 OUT<12> 38 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11870 $Y=3210 $D=189
M48 VDD! IN<11> 39 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13610 $Y=2690 $D=189
M49 39 IN<11> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14090 $Y=2690 $D=189
M50 VDD! IN<11> 39 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14570 $Y=2690 $D=189
M51 OUT<11> 39 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=15070 $Y=3210 $D=189
M52 VDD! IN<10> 40 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=16810 $Y=2690 $D=189
M53 40 IN<10> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17290 $Y=2690 $D=189
M54 VDD! IN<10> 40 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17770 $Y=2690 $D=189
M55 OUT<10> 40 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=18270 $Y=3210 $D=189
M56 VDD! IN<9> 41 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20010 $Y=2690 $D=189
M57 41 IN<9> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20490 $Y=2690 $D=189
M58 VDD! IN<9> 41 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20970 $Y=2690 $D=189
M59 OUT<9> 41 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=21470 $Y=3210 $D=189
M60 VDD! IN<8> 42 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23210 $Y=2690 $D=189
M61 42 IN<8> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23690 $Y=2690 $D=189
M62 VDD! IN<8> 42 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=24170 $Y=2690 $D=189
M63 OUT<8> 42 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=24670 $Y=3210 $D=189
M64 VDD! IN<7> 43 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26410 $Y=2690 $D=189
M65 43 IN<7> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26890 $Y=2690 $D=189
M66 VDD! IN<7> 43 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=27370 $Y=2690 $D=189
M67 OUT<7> 43 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=27870 $Y=3210 $D=189
M68 VDD! IN<6> 44 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29610 $Y=2690 $D=189
M69 44 IN<6> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=30090 $Y=2690 $D=189
M70 VDD! IN<6> 44 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=30570 $Y=2690 $D=189
M71 OUT<6> 44 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=31070 $Y=3210 $D=189
M72 VDD! IN<5> 45 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=32810 $Y=2690 $D=189
M73 45 IN<5> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33290 $Y=2690 $D=189
M74 VDD! IN<5> 45 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33770 $Y=2690 $D=189
M75 OUT<5> 45 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=34270 $Y=3210 $D=189
M76 VDD! IN<4> 46 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36010 $Y=2690 $D=189
M77 46 IN<4> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36490 $Y=2690 $D=189
M78 VDD! IN<4> 46 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36970 $Y=2690 $D=189
M79 OUT<4> 46 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=37470 $Y=3210 $D=189
M80 VDD! IN<3> 47 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39210 $Y=2690 $D=189
M81 47 IN<3> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39690 $Y=2690 $D=189
M82 VDD! IN<3> 47 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40170 $Y=2690 $D=189
M83 OUT<3> 47 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40670 $Y=3210 $D=189
M84 VDD! IN<2> 48 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42410 $Y=2690 $D=189
M85 48 IN<2> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42890 $Y=2690 $D=189
M86 VDD! IN<2> 48 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=43370 $Y=2690 $D=189
M87 OUT<2> 48 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=43870 $Y=3210 $D=189
M88 VDD! IN<1> 49 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45610 $Y=2690 $D=189
M89 49 IN<1> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=46090 $Y=2690 $D=189
M90 VDD! IN<1> 49 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=46570 $Y=2690 $D=189
M91 OUT<1> 49 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=47070 $Y=3210 $D=189
M92 VDD! IN<0> 50 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=48810 $Y=2690 $D=189
M93 50 IN<0> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49290 $Y=2690 $D=189
M94 VDD! IN<0> 50 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49770 $Y=2690 $D=189
M95 OUT<0> 50 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=50270 $Y=3210 $D=189
.ENDS
***************************************
.SUBCKT BITCELL RENB RWL_N WWL RWLB_P RWL_P RWLB_N REN RBL WBLB WBL VSS! VDD!
** N=72 EP=12 IP=0 FDC=12
M0 13 REN RBL VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=700 $Y=450 $D=97
M1 RWL_N 15 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=450 $D=97
M2 RWLB_N 16 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=4870 $D=97
M3 16 15 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2120 $Y=4870 $D=97
M4 15 WWL WBLB VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2240 $Y=450 $D=97
M5 WBL WWL 16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2600 $Y=4870 $D=97
M6 VSS! 16 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2720 $Y=450 $D=97
M7 14 RENB RBL VDD! pfet L=1.2e-07 W=8.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=700 $Y=3310 $D=189
M8 RWLB_P 15 14 VDD! pfet L=1.2e-07 W=8.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=1420 $D=189
M9 RWL_P 16 14 VDD! pfet L=1.2e-07 W=8.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=3310 $D=189
M10 16 15 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2120 $Y=3310 $D=189
M11 VDD! 16 15 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1750 $D=189
.ENDS
***************************************
.SUBCKT ICV_100 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
** N=17 EP=17 IP=24 FDC=24
X0 1 3 4 5 6 7 9 8 10 11 17 16 BITCELL $T=0 0 0 0 $X=0 $Y=-240
X1 2 3 4 5 6 7 13 12 14 15 17 16 BITCELL $T=3200 0 0 0 $X=3200 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_101 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27
** N=27 EP=27 IP=34 FDC=48
X0 1 2 5 6 7 8 9 10 11 12 13 14 15 16 17 26 27 ICV_100 $T=0 0 0 0 $X=0 $Y=-240
X1 3 4 5 6 7 8 9 18 19 20 21 22 23 24 25 26 27 ICV_100 $T=6400 0 0 0 $X=6400 $Y=-240
.ENDS
***************************************
.SUBCKT SRAM_1WORD RENB<0> RENB<1> RENB<2> RENB<3> RENB<4> RENB<5> RENB<6> RENB<7> RENB<8> RENB<9> RENB<10> RENB<11> RENB<12> RENB<13> RENB<14> RENB<15> RWL_N WWL RWLB_P RWL_P
+ RWLB_N RBL<0> REN<0> WBLB<0> WBL<0> RBL<1> REN<1> WBLB<1> WBL<1> RBL<2> REN<2> WBLB<2> WBL<2> RBL<3> REN<3> WBLB<3> WBL<3> RBL<4> REN<4> WBLB<4>
+ WBL<4> RBL<5> REN<5> WBLB<5> WBL<5> RBL<6> REN<6> WBLB<6> WBL<6> RBL<7> REN<7> WBLB<7> WBL<7> RBL<8> REN<8> WBLB<8> WBL<8> RBL<9> REN<9> WBLB<9>
+ WBL<9> RBL<10> REN<10> WBLB<10> WBL<10> RBL<11> REN<11> WBLB<11> WBL<11> RBL<12> REN<12> WBLB<12> WBL<12> RBL<13> REN<13> WBLB<13> WBL<13> RBL<14> REN<14> WBLB<14>
+ WBL<14> RBL<15> REN<15> WBLB<15> WBL<15> VDD! VSS!
** N=87 EP=87 IP=108 FDC=192
X0 RENB<0> RENB<1> RENB<2> RENB<3> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<0> REN<0> WBLB<0> WBL<0> RBL<1> REN<1> WBLB<1> WBL<1> RBL<2> REN<2> WBLB<2>
+ WBL<2> RBL<3> REN<3> WBLB<3> WBL<3> VDD! VSS!
+ ICV_101 $T=0 0 0 0 $X=0 $Y=-240
X1 RENB<4> RENB<5> RENB<6> RENB<7> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<4> REN<4> WBLB<4> WBL<4> RBL<5> REN<5> WBLB<5> WBL<5> RBL<6> REN<6> WBLB<6>
+ WBL<6> RBL<7> REN<7> WBLB<7> WBL<7> VDD! VSS!
+ ICV_101 $T=12800 0 0 0 $X=12800 $Y=-240
X2 RENB<8> RENB<9> RENB<10> RENB<11> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<8> REN<8> WBLB<8> WBL<8> RBL<9> REN<9> WBLB<9> WBL<9> RBL<10> REN<10> WBLB<10>
+ WBL<10> RBL<11> REN<11> WBLB<11> WBL<11> VDD! VSS!
+ ICV_101 $T=25600 0 0 0 $X=25600 $Y=-240
X3 RENB<12> RENB<13> RENB<14> RENB<15> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<12> REN<12> WBLB<12> WBL<12> RBL<13> REN<13> WBLB<13> WBL<13> RBL<14> REN<14> WBLB<14>
+ WBL<14> RBL<15> REN<15> WBLB<15> WBL<15> VDD! VSS!
+ ICV_101 $T=38400 0 0 0 $X=38400 $Y=-240
.ENDS
***************************************
.SUBCKT RWL_Driver_1bit RWLN RWLBP RWLP RWLBN RWL_N RWLB_N RWLB_P RWL_P VSS! VDD!
** N=154 EP=10 IP=0 FDC=48
M0 RWLN 11 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 11 RWL_N VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=4730 $D=97
M2 VSS! 11 RWLN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=450 $D=97
M3 VSS! RWL_N 11 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=4730 $D=97
M4 RWLN 11 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=450 $D=97
M5 RWLBN 14 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=4730 $D=97
M6 VSS! 11 RWLN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=450 $D=97
M7 VSS! 14 RWLBN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=4730 $D=97
M8 14 RWLB_N VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=450 $D=97
M9 RWLBN 14 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=4730 $D=97
M10 VSS! RWLB_N 14 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=450 $D=97
M11 VSS! 14 RWLBN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=4730 $D=97
M12 RWLBP 12 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=450 $D=97
M13 12 RWLB_P VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=4730 $D=97
M14 VSS! 12 RWLBP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=450 $D=97
M15 VSS! RWLB_P 12 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=4730 $D=97
M16 RWLBP 12 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=450 $D=97
M17 RWLP 13 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=4730 $D=97
M18 VSS! 12 RWLBP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=450 $D=97
M19 VSS! 13 RWLP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=4730 $D=97
M20 13 RWL_P VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=450 $D=97
M21 RWLP 13 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=4730 $D=97
M22 VSS! RWL_P 13 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=450 $D=97
M23 VSS! 13 RWLP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=4730 $D=97
M24 RWLN 11 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=1490 $D=189
M25 11 RWL_N VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M26 VDD! 11 RWLN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=1490 $D=189
M27 VDD! RWL_N 11 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=3310 $D=189
M28 RWLN 11 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=1490 $D=189
M29 RWLBN 14 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=3310 $D=189
M30 VDD! 11 RWLN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=1490 $D=189
M31 VDD! 14 RWLBN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=3310 $D=189
M32 14 RWLB_N VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=1480 $D=189
M33 RWLBN 14 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=3310 $D=189
M34 VDD! RWLB_N 14 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=1480 $D=189
M35 VDD! 14 RWLBN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=3310 $D=189
M36 RWLBP 12 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=1490 $D=189
M37 12 RWLB_P VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=3310 $D=189
M38 VDD! 12 RWLBP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=1490 $D=189
M39 VDD! RWLB_P 12 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=3310 $D=189
M40 RWLBP 12 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=1490 $D=189
M41 RWLP 13 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=3310 $D=189
M42 VDD! 12 RWLBP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=1490 $D=189
M43 VDD! 13 RWLP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=3310 $D=189
M44 13 RWL_P VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=1480 $D=189
M45 RWLP 13 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=3310 $D=189
M46 VDD! RWL_P 13 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=1480 $D=189
M47 VDD! 13 RWLP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_102 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
+ 81 82 83 84 85 86 87
** N=91 EP=87 IP=97 FDC=240
X0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 88 17 89 90
+ 91 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
+ 81 82 83 84 85 86 87
+ SRAM_1WORD $T=0 0 0 0 $X=0 $Y=-240
X1 88 89 90 91 21 19 20 18 87 86 RWL_Driver_1bit $T=51200 0 0 0 $X=51140 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_103 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
+ 81 82 83 84 85 86 87 88 89 90 91 92
** N=92 EP=92 IP=174 FDC=480
X0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45
+ 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65
+ 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85
+ 86 87 88 89 90 91 92
+ ICV_102 $T=0 0 0 0 $X=0 $Y=-240
X1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 22 23 24 25
+ 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45
+ 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65
+ 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85
+ 86 87 88 89 90 91 92
+ ICV_102 $T=0 5600 0 0 $X=0 $Y=5360
.ENDS
***************************************
.SUBCKT ICV_104 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
+ 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100
+ 101 102
** N=102 EP=102 IP=184 FDC=960
X0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 19 20 21
+ 22 18 23 24 25 26 37 38 39 40 41 42 43 44 45 46 47 48 49 50
+ 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70
+ 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90
+ 91 92 93 94 95 96 97 98 99 100 101 102
+ ICV_103 $T=0 0 0 0 $X=0 $Y=-240
X1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 27 29 30 31
+ 32 28 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50
+ 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70
+ 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90
+ 91 92 93 94 95 96 97 98 99 100 101 102
+ ICV_103 $T=0 11200 0 0 $X=0 $Y=10960
.ENDS
***************************************
.SUBCKT WBL_Driver_1bit IN WBLB WBL VSS! VDD! 6
** N=70 EP=6 IP=0 FDC=18
M0 VSS! 8 7 6 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=450 $D=97
M1 VSS! 8 7 6 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=4840 $D=97
M2 WBLB 7 VSS! 6 nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=450 $D=97
M3 8 IN VSS! 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=4870 $D=97
M4 VSS! 7 WBLB 6 nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=450 $D=97
M5 WBLB 7 VSS! 6 nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M6 VSS! IN 9 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=4870 $D=97
M7 VSS! 7 WBLB 6 nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M8 WBL 9 VSS! 6 nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=4750 $D=97
M9 VDD! 8 7 6 pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=1560 $D=189
M10 VDD! 8 7 6 pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=3310 $D=189
M11 WBLB 7 VDD! 6 pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=1500 $D=189
M12 8 IN VDD! 6 pfet L=1.2e-07 W=6.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3310 $D=189
M13 VDD! 7 WBLB 6 pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=1500 $D=189
M14 WBLB 7 VDD! 6 pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1500 $D=189
M15 VDD! IN 9 6 pfet L=1.2e-07 W=5.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3310 $D=189
M16 VDD! 7 WBLB 6 pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1500 $D=189
M17 WBL 9 VDD! 6 pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT REN_Driver_1bit RENB REN IN VSS! VDD!
** N=68 EP=5 IP=0 FDC=18
M0 VSS! 7 6 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=450 $D=97
M1 VSS! 7 6 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=4750 $D=97
M2 RENB 6 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=450 $D=97
M3 7 IN VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=4750 $D=97
M4 VSS! 6 RENB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=450 $D=97
M5 RENB 6 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M6 VSS! IN 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=4750 $D=97
M7 VSS! 6 RENB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M8 REN 8 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=4750 $D=97
M9 VDD! 7 6 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=1490 $D=189
M10 VDD! 7 6 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=3310 $D=189
M11 RENB 6 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=1490 $D=189
M12 7 IN VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3310 $D=189
M13 VDD! 6 RENB VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=1490 $D=189
M14 RENB 6 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1490 $D=189
M15 VDD! IN 8 VDD! pfet L=1.2e-07 W=6.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3310 $D=189
M16 VDD! 6 RENB VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1490 $D=189
M17 REN 8 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_98 1 2 3 4 5 6 7 8 9 10 11 12 13
** N=13 EP=13 IP=22 FDC=72
X0 3 4 5 12 12 12 WBL_Driver_1bit $T=0 0 0 0 $X=0 $Y=-240
X1 6 7 8 12 12 12 WBL_Driver_1bit $T=3200 0 0 0 $X=3200 $Y=-240
X2 1 10 9 12 13 REN_Driver_1bit $T=0 5600 0 0 $X=0 $Y=5360
X3 2 11 9 12 13 REN_Driver_1bit $T=3200 5600 0 0 $X=3200 $Y=5360
.ENDS
***************************************
.SUBCKT ICV_99 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23
** N=23 EP=23 IP=26 FDC=144
X0 1 2 5 7 9 10 11 13 6 8 12 22 23 ICV_98 $T=0 0 0 0 $X=0 $Y=-240
X1 3 4 14 15 17 18 19 21 6 16 20 22 23 ICV_98 $T=6400 0 0 0 $X=6400 $Y=-240
.ENDS
***************************************
.SUBCKT decoder_4th E SH3_BAR SH<3> IN IN1 VSS! VDD!
** N=108 EP=7 IP=0 FDC=26
M0 9 SH3_BAR IN VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=450 $D=97
M1 9 SH<3> IN1 VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=4810 $D=97
M2 10 9 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=450 $D=97
M3 10 9 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=4780 $D=97
M4 8 10 VSS! VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=450 $D=97
M5 E 8 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=4550 $D=97
M6 VSS! 10 8 VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=450 $D=97
M7 VSS! 8 E VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=4550 $D=97
M8 E 8 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3780 $Y=4550 $D=97
M9 E 8 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3820 $Y=450 $D=97
M10 VSS! 8 E VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4260 $Y=4550 $D=97
M11 VSS! 8 E VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4300 $Y=450 $D=97
M12 E 8 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4740 $Y=4550 $D=97
M13 9 SH3_BAR IN1 VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=3310 $D=189
M14 9 SH<3> IN VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=1950 $D=189
M15 10 9 VDD! VDD! pfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=1920 $D=189
M16 10 9 VDD! VDD! pfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=3310 $D=189
M17 8 10 VDD! VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=1790 $D=189
M18 E 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=3310 $D=189
M19 VDD! 10 8 VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=1790 $D=189
M20 VDD! 8 E VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=3310 $D=189
M21 E 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3780 $Y=3310 $D=189
M22 E 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3820 $Y=1690 $D=189
M23 VDD! 8 E VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4260 $Y=3310 $D=189
M24 VDD! 8 E VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4300 $Y=1690 $D=189
M25 E 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4740 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_97 1 2 3 4 5 6 7 8
** N=8 EP=8 IP=14 FDC=52
X0 1 3 4 5 6 7 5 decoder_4th $T=0 0 0 0 $X=-300 $Y=-240
X1 2 3 4 5 8 7 5 decoder_4th $T=0 5600 0 0 $X=-300 $Y=5360
.ENDS
***************************************
.SUBCKT decoder_3rd 1 2 3 4 5 6 7
** N=67 EP=7 IP=0 FDC=8
M0 8 1 4 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=450 $D=97
M1 8 2 5 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=4870 $D=97
M2 3 8 6 6 nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=450 $D=97
M3 3 8 6 6 nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=4550 $D=97
M4 8 1 5 7 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=3310 $D=189
M5 8 2 4 7 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=2010 $D=189
M6 3 8 7 7 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=1690 $D=189
M7 3 8 7 7 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_95 1 2 3 4 5 6 7 8 9 10
** N=10 EP=10 IP=14 FDC=34
X0 1 2 3 6 7 8 7 decoder_4th $T=0 0 0 0 $X=-300 $Y=-240
X1 4 5 6 9 10 8 7 decoder_3rd $T=-4800 0 0 0 $X=-5100 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_96 1 2 3 4 5 6 7 8 9 10 11 12 13 14
** N=14 EP=14 IP=20 FDC=68
X0 1 6 7 3 4 5 11 12 9 10 ICV_95 $T=0 0 0 0 $X=-5100 $Y=-240
X1 2 6 7 3 4 8 11 12 13 14 ICV_95 $T=0 5600 0 0 $X=-5100 $Y=5360
.ENDS
***************************************
.SUBCKT decoder_2st 1 2 3 4 5 6 7
** N=37 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4590 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=1730 $D=189
.ENDS
***************************************
.SUBCKT decoder_1st 1 2 3 4 5 6 7
** N=39 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4870 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT DECODER_WORST SEL<3> SEL<2> SEL<0> SEL<1> EN E<15> E<14> E<13> E<12> E<11> E<10> E<9> E<8> E<7> E<6> E<5> E<4> E<3> E<2> E<1>
+ E<0> VDD! VSS!
** N=303 EP=23 IP=130 FDC=586
M0 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6730 $Y=34050 $D=97
M1 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=34050 $D=97
M2 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=39650 $D=97
M3 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=34050 $D=97
M4 27 26 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=39650 $D=97
M5 VSS! 45 24 VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8090 $Y=38320 $D=97
M6 VSS! 46 26 VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8110 $Y=43980 $D=97
M7 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=34050 $D=97
M8 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=39650 $D=97
M9 24 45 VSS! VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8570 $Y=38320 $D=97
M10 26 46 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8590 $Y=43980 $D=97
M11 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=34050 $D=97
M12 27 26 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=39650 $D=97
M13 VSS! 45 24 VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9050 $Y=38320 $D=97
M14 VSS! 46 26 VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9070 $Y=43980 $D=97
M15 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=34050 $D=97
M16 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=39650 $D=97
M17 24 45 VSS! VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9540 $Y=38320 $D=97
M18 26 46 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9560 $Y=43980 $D=97
M19 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=34050 $D=97
M20 27 26 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=39650 $D=97
M21 VSS! 45 24 VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10020 $Y=38320 $D=97
M22 VSS! 46 26 VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10040 $Y=43980 $D=97
M23 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=34050 $D=97
M24 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=39650 $D=97
M25 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=34050 $D=97
M26 45 SEL<3> VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=38330 $D=97
M27 27 26 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=39650 $D=97
M28 46 SEL<2> VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=43990 $D=97
M29 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=34050 $D=97
M30 VSS! SEL<3> 45 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=38330 $D=97
M31 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=39650 $D=97
M32 VSS! SEL<2> 46 VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=43990 $D=97
M33 28 29 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12820 $Y=62050 $D=97
M34 VSS! 29 28 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13300 $Y=62050 $D=97
M35 29 SEL<1> VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13500 $Y=66400 $D=97
M36 28 29 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=62050 $D=97
M37 44 SEL<0> VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=77600 $D=97
M38 VSS! SEL<1> 29 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13980 $Y=66400 $D=97
M39 VSS! 29 28 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=62050 $D=97
M40 VSS! SEL<0> 44 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=77600 $D=97
M41 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6730 $Y=35110 $D=189
M42 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=35110 $D=189
M43 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=40790 $D=189
M44 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=35110 $D=189
M45 27 26 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=40790 $D=189
M46 VDD! 45 24 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8090 $Y=36910 $D=189
M47 VDD! 46 26 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8110 $Y=42510 $D=189
M48 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=35110 $D=189
M49 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=40790 $D=189
M50 24 45 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8570 $Y=36910 $D=189
M51 26 46 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8590 $Y=42510 $D=189
M52 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=35110 $D=189
M53 27 26 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=40790 $D=189
M54 VDD! 45 24 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9050 $Y=36910 $D=189
M55 VDD! 46 26 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9070 $Y=42510 $D=189
M56 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=35110 $D=189
M57 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=40790 $D=189
M58 24 45 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9540 $Y=36910 $D=189
M59 26 46 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9560 $Y=42510 $D=189
M60 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=35110 $D=189
M61 27 26 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=40790 $D=189
M62 VDD! 45 24 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10020 $Y=36910 $D=189
M63 VDD! 46 26 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10040 $Y=42510 $D=189
M64 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=35110 $D=189
M65 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=40790 $D=189
M66 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=35110 $D=189
M67 45 SEL<3> VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=36910 $D=189
M68 27 26 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=40790 $D=189
M69 46 SEL<2> VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=42510 $D=189
M70 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=35110 $D=189
M71 VDD! SEL<3> 45 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=36910 $D=189
M72 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=40790 $D=189
M73 VDD! SEL<2> 46 VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=42510 $D=189
M74 28 29 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12820 $Y=63090 $D=189
M75 VDD! 29 28 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13300 $Y=63090 $D=189
M76 29 SEL<1> VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13500 $Y=64910 $D=189
M77 28 29 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=63090 $D=189
M78 44 SEL<0> VDD! VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=76110 $D=189
M79 VDD! SEL<1> 29 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13980 $Y=64910 $D=189
M80 VDD! 29 28 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=63090 $D=189
M81 VDD! SEL<0> 44 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=76110 $D=189
X82 E<15> E<14> 25 24 VDD! 37 VSS! 36 ICV_97 $T=6000 0 1 180 $X=-300 $Y=-240
X83 E<13> E<12> 25 24 VDD! 35 VSS! 34 ICV_97 $T=6000 11200 1 180 $X=-300 $Y=10960
X84 E<11> E<10> 25 24 VDD! 33 VSS! 32 ICV_97 $T=6000 22400 1 180 $X=-300 $Y=22160
X85 E<9> E<8> 25 24 VDD! 31 VSS! 30 ICV_97 $T=6000 33600 1 180 $X=-300 $Y=33360
X86 E<7> E<6> 27 26 37 25 24 36 VSS! 41 VDD! VSS! VSS! 40 ICV_96 $T=6000 44800 1 180 $X=-300 $Y=44560
X87 E<5> E<4> 27 26 35 25 24 34 VSS! 39 VDD! VSS! VSS! 38 ICV_96 $T=6000 56000 1 180 $X=-300 $Y=55760
X88 E<3> E<2> 27 26 33 25 24 32 41 VSS! VDD! VSS! 40 VSS! ICV_96 $T=6000 67200 1 180 $X=-300 $Y=66960
X89 E<1> E<0> 27 26 31 25 24 30 39 VSS! VDD! VSS! 38 VSS! ICV_96 $T=6000 78400 1 180 $X=-300 $Y=78160
X90 29 28 41 VSS! VDD! VSS! 43 decoder_2st $T=13200 67200 1 180 $X=10680 $Y=66960
X91 29 28 40 VSS! VDD! VSS! 42 decoder_2st $T=13200 72800 1 180 $X=10680 $Y=72560
X92 29 28 39 VSS! VDD! 43 VSS! decoder_2st $T=13200 78400 1 180 $X=10680 $Y=78160
X93 29 28 38 VSS! VDD! 42 VSS! decoder_2st $T=13200 84000 1 180 $X=10680 $Y=83760
X94 44 SEL<0> 43 VSS! VDD! VSS! EN decoder_1st $T=14800 78400 1 180 $X=12280 $Y=78160
X95 44 SEL<0> 42 VSS! VDD! EN VSS! decoder_1st $T=14800 84000 1 180 $X=12280 $Y=83760
.ENDS
***************************************
.SUBCKT ICV_1 OUT<15> OUT<19> OUT<18> OUT<17> OUT<16> OUT<13> OUT<9> OUT<10> OUT<11> OUT<14> OUT<12> OUT<8> OUT<6> OUT<7> OUT<0> OUT<1> OUT<2> OUT<3> OUT<4> OUT<5>
+ WR_Data<0> OUT<20> Col_Sel<0> WR_Data<1> Col_Sel<1> Dsram<1> WR_Data<2> Col_Sel<2> Dsram<2> OUT<21> WR_Data<3> Col_Sel<3> Dsram<3> WR_Data<4> Col_Sel<4> Dsram<4> OUT<22> WR_Data<5> Col_Sel<5> Dsram<5>
+ WR_Data<6> Col_Sel<6> Dsram<6> OUT<23> WR_Data<7> Col_Sel<7> Dsram<7> WR_Data<8> Col_Sel<8> Dsram<8> OUT<24> WR_Data<9> Col_Sel<9> Dsram<9> WR_Data<10> Col_Sel<10> Dsram<10> OUT<25> WR_Data<11> Col_Sel<11>
+ Dsram<11> OUT<26> WR_Data<12> Col_Sel<12> Dsram<12> WR_Data<13> Col_Sel<13> Dsram<13> WR_Data<14> Col_Sel<14> Dsram<14> OUT<27> WR_Data<15> Col_Sel<15> Dsram<15> OUT<28> OUT<29> RWL_P<9> RWLB_P<9> RWLB_N<9>
+ RWLB_P<8> RWLB_P<7> RWL_P<6> RWL_N<3> RWL_N<9> RWL_N<8> RWL_N<7> RWLB_N<6> RWL_N<5> RWLB_P<3> RWLB_N<15> RWLB_N<14> RWLB_N<13> RWLB_N<12> OUT<30> RWLB_P<13> RWL_P<3> RWL_N<2> RWLB_N<2> RWLB_N<3>
+ RWLB_P<2> RWL_P<2> WR_Addr<3> RWLB_P<11> RWL_P<5> RWL_N<4> RWLB_N<4> RWL_N<6> RWLB_P<5> RWLB_N<5> RWLB_P<4> RWL_P<4> RWL_N<15> RWLB_N<7> RWLB_P<6> RWL_P<1> RWL_N<13> RWL_P<7> RWL_N<12> RWLB_N<0>
+ RWL_P<15> RWLB_N<8> RWL_P<14> RWL_P<8> WR_Addr<2> WR_Addr<0> Dsram<0> VSS! VDD! RWLB_N<11> RWLB_N<10> RWLB_P<15> RWLB_P<12> RWLB_P<14> RWL_N<1> RWLB_P<10> RWLB_N<1> RWLB_P<0> RWLB_P<1> RWL_P<0>
+ RWL_N<0> REN_SRAM RWL_N<14> RWL_P<13> RWL_P<12> RWL_N<11> RWL_P<11> RWL_N<10> RWL_P<10> WR_Addr<1> CLK WR_EN OUT<31> Vref<0> Vref<1> Vref<2> Vref<3> Vref<4> Vref<5> Vref<6>
+ Vref<7> Vref<8> Vref<9> Vref<10> Vref<11> Vref<12> Vref<13> Vref<14> Vref<15> Vref<16> Vref<17> Vref<18> Vref<19> Vref<20> Vref<21> Vref<22> Vref<23> Vref<24> Vref<25> Vref<26>
+ Vref<27> Vref<28> Vref<29> Vref<30> Vref<31>
** N=313 EP=185 IP=660 FDC=5665
X0 CLK 266 VSS! Vref<0> Vref<1> Vref<2> Vref<3> Vref<4> Vref<5> Vref<6> Vref<7> Vref<8> Vref<9> Vref<10> Vref<11> Vref<12> Vref<13> Vref<14> Vref<15> Vref<16>
+ Vref<17> Vref<18> Vref<19> Vref<20> Vref<21> Vref<22> Vref<23> Vref<24> Vref<25> Vref<26> Vref<27> Vref<28> Vref<29> Vref<30> Vref<31> VDD! OUT<0> OUT<1> OUT<2> OUT<3>
+ OUT<4> OUT<5> OUT<6> OUT<7> OUT<8> OUT<9> OUT<10> OUT<11> OUT<12> OUT<13> OUT<14> OUT<15> OUT<16> OUT<17> OUT<18> OUT<19> OUT<20> OUT<21> OUT<22> OUT<23>
+ OUT<24> OUT<25> OUT<26> OUT<27> OUT<28> OUT<29> OUT<30> OUT<31>
+ 32ADC $T=-139200 -26400 0 0 $X=-142980 $Y=-29870
X1 217 266 267 268 269 270 271 272 273 274 275 276 277 278 279 280 281 Col_Sel<0> Col_Sel<1> Col_Sel<2>
+ Col_Sel<3> Col_Sel<4> Col_Sel<5> Col_Sel<6> Col_Sel<7> Col_Sel<8> Col_Sel<9> Col_Sel<10> Col_Sel<11> Col_Sel<12> Col_Sel<13> Col_Sel<14> Col_Sel<15> VSS! VDD!
+ ADC_Mux $T=0 0 1 0 $X=-340 $Y=-4660
X2 217 Dsram<0> 267 Dsram<1> 268 Dsram<2> 269 Dsram<3> 270 Dsram<4> 271 Dsram<5> 272 Dsram<6> 273 Dsram<7> 274 Dsram<8> 275 Dsram<9>
+ 276 Dsram<10> 277 Dsram<11> 278 Dsram<12> 279 Dsram<13> 280 Dsram<14> 281 VDD! VSS! Dsram<15>
+ special_inverter $T=0 -8400 0 0 $X=0 $Y=-8640
X3 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 263 264 RWLB_P<15> RWL_N<15>
+ RWL_P<15> RWLB_N<15> RWLB_P<14> RWL_N<14> RWL_P<14> RWLB_N<14> 265 251 RWLB_P<13> RWL_N<13> RWL_P<13> RWLB_N<13> RWLB_P<12> RWL_N<12> RWL_P<12> RWLB_N<12> 217 234 282 283
+ 267 235 284 285 268 236 286 287 269 237 288 289 270 238 290 291 271 239 292 293
+ 272 240 294 295 273 241 296 297 274 242 298 299 275 243 300 301 276 244 302 303
+ 277 245 304 305 278 246 306 307 279 247 308 309 280 248 310 311 281 249 312 313
+ VDD! VSS!
+ ICV_104 $T=0 0 0 0 $X=0 $Y=-240
X4 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 252 253 RWLB_P<11> RWL_N<11>
+ RWL_P<11> RWLB_N<11> RWLB_P<10> RWL_N<10> RWL_P<10> RWLB_N<10> 254 255 RWLB_P<9> RWL_N<9> RWL_P<9> RWLB_N<9> RWLB_P<8> RWL_N<8> RWL_P<8> RWLB_N<8> 217 234 282 283
+ 267 235 284 285 268 236 286 287 269 237 288 289 270 238 290 291 271 239 292 293
+ 272 240 294 295 273 241 296 297 274 242 298 299 275 243 300 301 276 244 302 303
+ 277 245 304 305 278 246 306 307 279 247 308 309 280 248 310 311 281 249 312 313
+ VDD! VSS!
+ ICV_104 $T=0 22400 0 0 $X=0 $Y=22160
X5 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 256 257 RWLB_P<7> RWL_N<7>
+ RWL_P<7> RWLB_N<7> RWLB_P<6> RWL_N<6> RWL_P<6> RWLB_N<6> 258 259 RWLB_P<5> RWL_N<5> RWL_P<5> RWLB_N<5> RWLB_P<4> RWL_N<4> RWL_P<4> RWLB_N<4> 217 234 282 283
+ 267 235 284 285 268 236 286 287 269 237 288 289 270 238 290 291 271 239 292 293
+ 272 240 294 295 273 241 296 297 274 242 298 299 275 243 300 301 276 244 302 303
+ 277 245 304 305 278 246 306 307 279 247 308 309 280 248 310 311 281 249 312 313
+ VDD! VSS!
+ ICV_104 $T=0 44800 0 0 $X=0 $Y=44560
X6 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 250 260 RWLB_P<3> RWL_N<3>
+ RWL_P<3> RWLB_N<3> RWLB_P<2> RWL_N<2> RWL_P<2> RWLB_N<2> 261 262 RWLB_P<1> RWL_N<1> RWL_P<1> RWLB_N<1> RWLB_P<0> RWL_N<0> RWL_P<0> RWLB_N<0> 217 234 282 283
+ 267 235 284 285 268 236 286 287 269 237 288 289 270 238 290 291 271 239 292 293
+ 272 240 294 295 273 241 296 297 274 242 298 299 275 243 300 301 276 244 302 303
+ 277 245 304 305 278 246 306 307 279 247 308 309 280 248 310 311 281 249 312 313
+ VDD! VSS!
+ ICV_104 $T=0 67200 0 0 $X=0 $Y=66960
X7 218 219 220 221 WR_Data<0> REN_SRAM 282 234 283 WR_Data<1> 284 235 285 WR_Data<2> 286 236 287 WR_Data<3> 288 237
+ 289 VSS! VDD!
+ ICV_99 $T=0 89600 0 0 $X=0 $Y=89360
X8 222 223 224 225 WR_Data<4> REN_SRAM 290 238 291 WR_Data<5> 292 239 293 WR_Data<6> 294 240 295 WR_Data<7> 296 241
+ 297 VSS! VDD!
+ ICV_99 $T=12800 89600 0 0 $X=12800 $Y=89360
X9 226 227 228 229 WR_Data<8> REN_SRAM 298 242 299 WR_Data<9> 300 243 301 WR_Data<10> 302 244 303 WR_Data<11> 304 245
+ 305 VSS! VDD!
+ ICV_99 $T=25600 89600 0 0 $X=25600 $Y=89360
X10 230 231 232 233 WR_Data<12> REN_SRAM 306 246 307 WR_Data<13> 308 247 309 WR_Data<14> 310 248 311 WR_Data<15> 312 249
+ 313 VSS! VDD!
+ ICV_99 $T=38400 89600 0 0 $X=38400 $Y=89360
X11 WR_Addr<3> WR_Addr<2> WR_Addr<0> WR_Addr<1> WR_EN 263 264 265 251 252 253 254 255 256 257 258 259 250 260 261
+ 262 VDD! VSS!
+ DECODER_WORST $T=57600 0 0 0 $X=57300 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_37
** N=190 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_36
** N=335 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_35
** N=466 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_34
** N=294 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_33
** N=318 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_32
** N=319 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_31
** N=322 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_30
** N=228 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT Inst_rom_g1 VDD VSS A<9> A<8> A<7> A<6> A<5> A<4> A<3> A<2> A<1> A<0> CLK CEN Q<0> Q<1> Q<2> Q<3> Q<4> Q<5>
+ Q<6> Q<7> Q<8> Q<9> Q<10> Q<11> Q<12> Q<13> Q<14> Q<15>
** N=455 EP=30 IP=1566 FDC=0
.ENDS
***************************************
.SUBCKT ICV_38 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29
** N=29 EP=29 IP=31 FDC=0
X0 1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
+ 20 21 22 23 24 25 26 27 28 29
+ Inst_rom_g1 $T=0 0 0 0 $X=-2 $Y=-2
.ENDS
***************************************
.SUBCKT NOR3X1TR C VDD B A VSS Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND4BX1TR AN D VSS C B Y VDD
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKINVX2TR A VSS VDD Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT INVX2TR A VSS VDD Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI33X1TR B2 B1 VSS B0 A0 A1 A2 VDD Y
** N=12 EP=9 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKAND2X2TR A B VSS VDD Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_85 1 2 3 4 5 6 7 8 9
** N=9 EP=9 IP=14 FDC=0
X0 1 2 3 4 5 CLKAND2X2TR $T=0 0 0 0 $X=-340 $Y=-280
X1 6 2 3 4 7 CLKAND2X2TR $T=2000 0 0 0 $X=1660 $Y=-280
.ENDS
***************************************
.SUBCKT we_reorder inst7_4<0> inst7_4<1> WE<5> inst7_4<3> WE<10> WE<14> WE<4> WE<7> WE<1> WE<12> WE<3> WE_OUT<1> WE_OUT<10> WE_OUT<12> WE<8> WE_OUT<7> WE_OUT<5> WE_OUT<14> WE<2> WE<9>
+ WE<11> WE<0> WE<6> WE<15> WE<13> WE_OUT<3> WE_OUT<9> WE_OUT<6> WE_OUT<13> WE_OUT<11> WE_OUT<0> WE_OUT<4> WE_OUT<15> WE_OUT<2> WE_OUT<8> inst15_12<0> inst15_12<1> inst15_12<3> inst15_12<2> inst7_4<2>
+ 41
** N=64 EP=41 IP=128 FDC=0
X0 inst15_12<2> VDD inst15_12<3> inst15_12<1> VSS 44 NOR3X1TR $T=14000 104400 1 180 $X=11660 $Y=104120
X1 inst15_12<0> 44 VSS inst7_4<2> 45 47 VDD NAND4BX1TR $T=14000 104400 0 0 $X=13660 $Y=104120
X2 inst7_4<0> VSS VDD 49 CLKINVX2TR $T=17600 104400 1 0 $X=17260 $Y=100400
X3 inst7_4<3> VSS VDD 46 CLKINVX2TR $T=24800 104400 1 180 $X=23260 $Y=104120
X4 inst7_4<1> VSS VDD 48 INVX2TR $T=18800 104400 0 0 $X=18460 $Y=104120
X5 inst7_4<1> inst7_4<3> VSS inst7_4<0> 46 49 48 VDD 45 OAI33X1TR $T=23600 104400 1 180 $X=19660 $Y=104120
X6 WE<3> 47 VSS VDD WE_OUT<3> CLKAND2X2TR $T=28400 54000 0 0 $X=28060 $Y=53720
X7 WE<8> 47 VSS VDD WE_OUT<8> CLKAND2X2TR $T=28400 68400 1 0 $X=28060 $Y=64400
X8 WE<0> 47 VSS VDD WE_OUT<0> CLKAND2X2TR $T=28800 46800 0 0 $X=28460 $Y=46520
X9 WE<15> 47 VSS VDD WE_OUT<15> CLKAND2X2TR $T=28800 82800 1 0 $X=28460 $Y=78800
X10 WE<1> 47 VSS VDD WE_OUT<1> WE<2> WE_OUT<2> 61 41 ICV_85 $T=26400 54000 1 0 $X=26060 $Y=50000
X11 WE<5> 47 VSS VDD WE_OUT<5> WE<4> WE_OUT<4> 62 41 ICV_85 $T=26400 61200 1 0 $X=26060 $Y=57200
X12 WE<10> 47 VSS VDD WE_OUT<10> WE<9> WE_OUT<9> 56 41 ICV_85 $T=26400 68400 0 0 $X=26060 $Y=68120
X13 WE<12> 47 VSS VDD WE_OUT<12> WE<11> WE_OUT<11> 56 41 ICV_85 $T=26400 75600 1 0 $X=26060 $Y=71600
X14 WE<7> 47 VSS VDD WE_OUT<7> WE<6> WE_OUT<6> 63 41 ICV_85 $T=26800 61200 0 0 $X=26460 $Y=60920
X15 WE<14> 47 VSS VDD WE_OUT<14> WE<13> WE_OUT<13> 64 41 ICV_85 $T=26800 75600 0 0 $X=26460 $Y=75320
.ENDS
***************************************
.SUBCKT ICV_86 13 15 16 18 20 21 22 23 25 28 29 31 32 33 34 35 36 37 39 40
+ 57 58 91 95 104 128 156 157 158 159 160 161 175 195 196 197 198 199 200 201
+ 202
** N=206 EP=41 IP=41 FDC=0
X0 199 200 91 202 160 13 157 18 57 158 156 37 159 31 15 23 32 28 58 22
+ 25 16 36 104 21 95 39 128 34 40 35 33 175 29 20 195 196 198 197 201
+ 161
+ we_reorder $T=-142200 -46500 0 0 $X=-142200 $Y=-46500
.ENDS
***************************************
.SUBCKT ICV_84
** N=36 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_83
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_82
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_81
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_80
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_79
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_78
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_9
** N=3 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_8
** N=4 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_7
** N=4 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_6
** N=1748 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT nc_grounds
** N=3 EP=0 IP=0 FDC=0
*.CALIBRE ISOLATED NETS: vss_dvss_l1 pad vss_dvss_r1
.ENDS
***************************************
.SUBCKT ICV_5
** N=2527 EP=0 IP=15 FDC=0
.ENDS
***************************************
.SUBCKT ICV_4
** N=2875 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_3
** N=2179 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_2
** N=2737 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ipplscp_dvss_sub
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT PDVSS 1 2 DVSS VDD DVDD VSS
** N=8 EP=6 IP=37 FDC=0
*.CALIBRE WARNING OPEN Open circuit(s) detected by extraction in this cell. See extraction report for details.
.ENDS
***************************************
.SUBCKT ICV_10 1
** N=7 EP=1 IP=7 FDC=0
X0 2 3 4 5 6 7 PDVSS $T=0 0 0 0 $X=-2 $Y=0
.ENDS
***************************************
.SUBCKT XNOR2X1TR B A Y VDD VSS
** N=10 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKINVX1TR A VSS VDD Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI21X1TR A1 VSS A0 B0 VDD Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND2X1TR B VSS Y A VDD
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR2X1TR B VDD A Y VSS
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT XOR2X1TR A B Y VDD VSS
** N=10 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT DFFQX1TR D CK VSS VDD Q
** N=13 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI21X4TR A0 A1 VDD B0 VSS Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI21X2TR A0 A1 VSS B0 Y VDD
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT INVX1TR VSS VDD A Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI211X1TR A1 A0 VSS B0 C0 VDD Y
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI22XLTR B1 VDD B0 A0 A1 Y VSS
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI22X1TR B1 VDD B0 A0 A1 VSS Y
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OR2X1TR VSS A B VDD Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AND2X1TR A B VSS VDD Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR3BX2TR Y B VSS C AN VDD
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI21X1TR A1 A0 VDD B0 VSS Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKBUFX20TR A VSS Y VDD
** N=7 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND2BX1TR AN B VSS Y VDD
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AND2X2TR A B VDD VSS Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR2X2TR A B Y VSS VDD
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND2XLTR B VSS A Y VDD
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKINVX6TR A VSS VDD Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT XOR2X2TR B VSS A VDD Y
** N=10 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT XNOR2X2TR B A VSS VDD Y
** N=10 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OR2X2TR A B VSS VDD Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT DFFTRX2TR D RN CK Q VSS VDD QN
** N=17 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT TIEHITR VSS VDD Y
** N=6 EP=3 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT DFFTRX1TR D RN CK Q VSS VDD QN
** N=17 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT pc RF_in<5> RF_in<9> RF_in<8> RF_in<6> VDD VSS addr<0> addr<1> addr<2> addr<4> addr<8> addr<9> Rlink<3> clk addr<6> addr<7> addr<5> Rlink<2> addr<3> Rlink<1>
+ Rlink<5> scan_en Rlink<7> Rlink<8> Rlink<4> resetn jmp stall br disp<3> scan_in disp<7> Rlink<9> scan_out RF_in<3> RF_in<1> RF_in<7> RF_in<0> RF_in<2> RF_in<4>
+ Rlink<0> Rlink<6> disp<2> disp<0> disp<1> disp<4> disp<5> disp<6> disp<8> disp<9> 51
** N=192 EP=51 IP=1010 FDC=0
X0 resetn VDD stall scan_en VSS 92 NOR3X1TR $T=49600 39600 0 0 $X=49260 $Y=39320
X1 61 VSS VDD 58 CLKINVX2TR $T=18000 25200 0 180 $X=16460 $Y=21200
X2 65 VSS VDD 163 CLKINVX2TR $T=19600 39600 0 0 $X=19260 $Y=39320
X3 Rlink<3> VSS VDD 135 CLKINVX2TR $T=26400 18000 0 0 $X=26060 $Y=17720
X4 96 VSS VDD 145 CLKINVX2TR $T=60400 10800 1 0 $X=60060 $Y=6800
X5 103 VSS VDD 104 CLKINVX2TR $T=61200 10800 0 0 $X=60860 $Y=10520
X6 105 VSS VDD 111 CLKINVX2TR $T=70000 10800 0 180 $X=68460 $Y=6800
X7 118 VSS VDD 113 CLKINVX2TR $T=75200 32400 0 180 $X=73660 $Y=28400
X8 Rlink<9> VSS VDD 127 CLKINVX2TR $T=86400 39600 1 0 $X=86060 $Y=35600
X9 134 VSS VDD 54 INVX2TR $T=20000 32400 0 180 $X=18460 $Y=28400
X10 Rlink<6> VSS VDD 116 INVX2TR $T=70400 25200 1 0 $X=70060 $Y=21200
X11 132 52 Rlink<3> VDD VSS XNOR2X1TR $T=10800 25200 0 180 $X=7260 $Y=21200
X12 140 85 Rlink<4> VDD VSS XNOR2X1TR $T=40400 18000 1 180 $X=36860 $Y=17720
X13 53 VSS VDD 133 CLKINVX1TR $T=12800 25200 0 0 $X=12460 $Y=24920
X14 88 VSS VDD 90 CLKINVX1TR $T=50000 18000 1 180 $X=48460 $Y=17720
X15 114 VSS VDD 153 CLKINVX1TR $T=70000 10800 1 0 $X=69660 $Y=6800
X16 53 VSS 54 56 VDD 52 OAI21X1TR $T=14000 25200 0 0 $X=13660 $Y=24920
X17 56 VSS 61 60 VDD 59 OAI21X1TR $T=18000 25200 1 180 $X=15660 $Y=24920
X18 105 VSS 104 106 VDD 149 OAI21X1TR $T=62400 10800 0 0 $X=62060 $Y=10520
X19 106 VSS 114 155 VDD 108 OAI21X1TR $T=70800 18000 1 0 $X=70460 $Y=14000
X20 56 VSS 55 133 VDD NAND2X1TR $T=15600 32400 0 180 $X=13660 $Y=28400
X21 60 VSS 132 58 VDD NAND2X1TR $T=16800 25200 0 180 $X=14860 $Y=21200
X22 66 VSS 64 163 VDD NAND2X1TR $T=22400 39600 1 180 $X=20460 $Y=39320
X23 72 VSS 66 addr<1> VDD NAND2X1TR $T=24000 39600 1 180 $X=22060 $Y=39320
X24 70 VSS 60 addr<3> VDD NAND2X1TR $T=24400 25200 0 180 $X=22460 $Y=21200
X25 68 VSS 56 addr<2> VDD NAND2X1TR $T=22800 25200 0 0 $X=22460 $Y=24920
X26 79 VSS 62 addr<0> VDD NAND2X1TR $T=29200 39600 1 180 $X=27260 $Y=39320
X27 88 VSS 140 87 VDD NAND2X1TR $T=44000 18000 1 180 $X=42060 $Y=17720
X28 94 VSS 88 addr<4> VDD NAND2X1TR $T=48400 25200 0 180 $X=46460 $Y=21200
X29 143 VSS 100 addr<5> VDD NAND2X1TR $T=56000 25200 1 0 $X=55660 $Y=21200
X30 107 VSS 146 96 VDD NAND2X1TR $T=63600 18000 0 180 $X=61660 $Y=14000
X31 106 VSS 102 111 VDD NAND2X1TR $T=67200 18000 1 0 $X=66860 $Y=14000
X32 151 VSS 106 addr<6> VDD NAND2X1TR $T=69600 18000 0 0 $X=69260 $Y=17720
X33 155 VSS 152 153 VDD NAND2X1TR $T=72800 10800 1 180 $X=70860 $Y=10520
X34 154 VSS 155 addr<7> VDD NAND2X1TR $T=72400 25200 1 0 $X=72060 $Y=21200
X35 118 VSS 115 117 VDD NAND2X1TR $T=75200 32400 1 0 $X=74860 $Y=28400
X36 119 VSS 118 addr<8> VDD NAND2X1TR $T=76400 32400 0 0 $X=76060 $Y=32120
X37 122 VSS 156 addr<9> VDD NAND2X1TR $T=80000 39600 0 0 $X=79660 $Y=39320
X38 156 VSS 120 166 VDD NAND2X1TR $T=80400 39600 1 0 $X=80060 $Y=35600
X39 RF_in<3> VSS 76 97 VDD NAND2X1TR $T=97600 18000 0 180 $X=95660 $Y=14000
X40 RF_in<6> VSS 129 97 VDD NAND2X1TR $T=99200 25200 1 180 $X=97260 $Y=24920
X41 RF_in<2> VSS 78 97 VDD NAND2X1TR $T=99600 32400 0 180 $X=97660 $Y=28400
X42 RF_in<1> VSS 81 97 VDD NAND2X1TR $T=100400 32400 1 180 $X=98460 $Y=32120
X43 RF_in<7> VSS 125 97 VDD NAND2X1TR $T=100800 25200 1 180 $X=98860 $Y=24920
X44 RF_in<0> VSS 126 97 VDD NAND2X1TR $T=100800 39600 0 180 $X=98860 $Y=35600
X45 RF_in<5> VSS 93 97 VDD NAND2X1TR $T=101200 32400 0 180 $X=99260 $Y=28400
X46 RF_in<9> VSS 131 97 VDD NAND2X1TR $T=102000 32400 1 180 $X=100060 $Y=32120
X47 RF_in<4> VSS 84 97 VDD NAND2X1TR $T=102400 25200 1 180 $X=100460 $Y=24920
X48 RF_in<8> VSS 124 97 VDD NAND2X1TR $T=102800 32400 0 180 $X=100860 $Y=28400
X49 61 VDD 53 57 VSS NOR2X1TR $T=17200 18000 1 180 $X=15260 $Y=17720
X50 68 VDD addr<2> 53 VSS NOR2X1TR $T=21200 25200 1 180 $X=19260 $Y=24920
X51 72 VDD addr<1> 65 VSS NOR2X1TR $T=26000 39600 1 180 $X=24060 $Y=39320
X52 70 VDD addr<3> 61 VSS NOR2X1TR $T=26400 25200 0 180 $X=24460 $Y=21200
X53 94 VDD addr<4> 95 VSS NOR2X1TR $T=52000 25200 0 180 $X=50060 $Y=21200
X54 143 VDD addr<5> 99 VSS NOR2X1TR $T=56400 25200 1 180 $X=54460 $Y=24920
X55 105 VDD 145 148 VSS NOR2X1TR $T=63200 10800 0 180 $X=61260 $Y=6800
X56 151 VDD addr<6> 105 VSS NOR2X1TR $T=68000 18000 1 180 $X=66060 $Y=17720
X57 114 VDD 105 107 VSS NOR2X1TR $T=70400 18000 0 180 $X=68460 $Y=14000
X58 154 VDD addr<7> 114 VSS NOR2X1TR $T=73200 18000 1 180 $X=71260 $Y=17720
X59 54 55 Rlink<2> VDD VSS XOR2X1TR $T=15600 32400 1 0 $X=15260 $Y=28400
X60 64 62 Rlink<1> VDD VSS XOR2X1TR $T=19600 39600 1 180 $X=16060 $Y=39320
X61 141 89 Rlink<5> VDD VSS XOR2X1TR $T=45200 25200 0 180 $X=41660 $Y=21200
X62 165 102 Rlink<6> VDD VSS XOR2X1TR $T=58400 18000 1 0 $X=58060 $Y=14000
X63 69 67 VSS VDD addr<3> DFFQX1TR $T=24400 18000 1 180 $X=17260 $Y=17720
X64 136 67 VSS VDD addr<2> DFFQX1TR $T=30000 18000 0 180 $X=22860 $Y=14000
X65 138 67 VSS VDD addr<1> DFFQX1TR $T=31600 39600 0 180 $X=24460 $Y=35600
X66 82 67 VSS VDD addr<4> DFFQX1TR $T=34000 18000 1 0 $X=33660 $Y=14000
X67 142 67 VSS VDD addr<5> DFFQX1TR $T=49600 25200 1 180 $X=42460 $Y=24920
X68 144 67 VSS VDD addr<0> DFFQX1TR $T=60800 32400 1 180 $X=53660 $Y=32120
X69 157 67 VSS VDD addr<6> DFFQX1TR $T=82400 18000 1 180 $X=75260 $Y=17720
X70 167 67 VSS VDD addr<8> DFFQX1TR $T=86400 25200 0 0 $X=86060 $Y=24920
X71 128 67 VSS VDD addr<9> DFFQX1TR $T=88000 39600 1 0 $X=87660 $Y=35600
X72 134 57 VDD 59 VSS 63 AOI21X4TR $T=22800 25200 0 180 $X=17660 $Y=21200
X73 65 62 VSS 66 134 VDD OAI21X2TR $T=18800 39600 1 0 $X=18460 $Y=35600
X74 99 88 VSS 100 103 VDD OAI21X2TR $T=54400 18000 0 0 $X=54060 $Y=17720
X75 63 146 VSS 150 109 VDD OAI21X2TR $T=61600 18000 0 0 $X=61260 $Y=17720
X76 VSS VDD Rlink<2> 73 INVX1TR $T=22000 32400 1 0 $X=21660 $Y=28400
X77 VSS VDD Rlink<1> 71 INVX1TR $T=23600 39600 1 0 $X=23260 $Y=35600
X78 VSS VDD Rlink<0> 101 INVX1TR $T=34800 39600 0 0 $X=34460 $Y=39320
X79 VSS VDD Rlink<4> 139 INVX1TR $T=40400 25200 0 180 $X=38860 $Y=21200
X80 VSS VDD Rlink<5> 91 INVX1TR $T=44000 32400 0 0 $X=43660 $Y=32120
X81 VSS VDD 95 87 INVX1TR $T=51200 18000 1 180 $X=49660 $Y=17720
X82 VSS VDD 99 98 INVX1TR $T=56000 25200 0 180 $X=54460 $Y=21200
X83 VSS VDD Rlink<8> 158 INVX1TR $T=82000 32400 1 0 $X=81660 $Y=28400
X84 VSS VDD Rlink<7> 160 INVX1TR $T=84400 18000 0 0 $X=84060 $Y=17720
X85 74 135 VSS 77 76 VDD 69 OAI211X1TR $T=27600 18000 0 0 $X=27260 $Y=17720
X86 74 73 VSS 137 78 VDD 136 OAI211X1TR $T=28000 25200 1 0 $X=27660 $Y=21200
X87 74 71 VSS 75 81 VDD 138 OAI211X1TR $T=30400 32400 0 0 $X=30060 $Y=32120
X88 74 139 VSS 83 84 VDD 82 OAI211X1TR $T=34800 18000 0 0 $X=34460 $Y=17720
X89 74 91 VSS 86 93 VDD 142 OAI211X1TR $T=48000 32400 1 0 $X=47660 $Y=28400
X90 74 101 VSS 147 126 VDD 144 OAI211X1TR $T=59600 39600 1 0 $X=59260 $Y=35600
X91 74 116 VSS 121 129 VDD 157 OAI211X1TR $T=79600 25200 1 0 $X=79260 $Y=21200
X92 74 158 VSS 159 124 VDD 167 OAI211X1TR $T=84000 25200 0 0 $X=83660 $Y=24920
X93 74 160 VSS 123 125 VDD 162 OAI211X1TR $T=85200 25200 1 0 $X=84860 $Y=21200
X94 74 127 VSS 130 131 VDD 128 OAI211X1TR $T=89600 32400 0 0 $X=89260 $Y=32120
X95 addr<1> VDD scan_en 80 addr<2> 137 VSS AOI22XLTR $T=28000 25200 0 0 $X=27660 $Y=24920
X96 addr<2> VDD scan_en 80 addr<3> 77 VSS AOI22XLTR $T=30000 18000 0 0 $X=29660 $Y=17720
X97 addr<6> VDD scan_en 80 addr<7> 123 VSS AOI22XLTR $T=82000 25200 1 0 $X=81660 $Y=21200
X98 addr<8> VDD scan_en 80 addr<9> 130 VSS AOI22XLTR $T=86800 32400 0 0 $X=86460 $Y=32120
X99 addr<7> VDD scan_en 80 addr<8> 159 VSS AOI22XLTR $T=90400 32400 0 180 $X=87660 $Y=28400
X100 addr<0> VDD scan_en 80 addr<1> VSS 75 AOI22X1TR $T=30400 32400 1 180 $X=27660 $Y=32120
X101 addr<3> VDD scan_en 80 addr<4> VSS 83 AOI22X1TR $T=32400 18000 0 0 $X=32060 $Y=17720
X102 addr<4> VDD scan_en 80 addr<5> VSS 86 AOI22X1TR $T=40400 25200 0 0 $X=40060 $Y=24920
X103 scan_in VDD scan_en 80 addr<0> VSS 147 AOI22X1TR $T=64400 39600 0 180 $X=61660 $Y=35600
X104 addr<5> VDD scan_en 80 addr<6> VSS 121 AOI22X1TR $T=74400 25200 1 0 $X=74060 $Y=21200
X105 VSS 79 addr<0> VDD 164 OR2X1TR $T=30800 39600 0 0 $X=30460 $Y=39320
X106 VSS addr<9> 122 VDD 166 OR2X1TR $T=84000 39600 0 180 $X=81660 $Y=35600
X107 164 62 VSS VDD Rlink<0> AND2X1TR $T=32800 39600 0 0 $X=32460 $Y=39320
X108 br disp<8> VSS VDD 119 AND2X1TR $T=74800 39600 0 0 $X=74460 $Y=39320
X109 br disp<9> VSS VDD 122 AND2X1TR $T=78000 39600 0 0 $X=77660 $Y=39320
X110 80 scan_en VSS resetn stall VDD NOR3BX2TR $T=43600 39600 0 0 $X=43260 $Y=39320
X111 87 85 VDD 90 VSS 141 AOI21X1TR $T=46000 18000 1 180 $X=43660 $Y=17720
X112 85 96 VDD 103 VSS 165 AOI21X1TR $T=53600 18000 1 0 $X=53260 $Y=14000
X113 148 85 VDD 149 VSS 110 AOI21X1TR $T=63600 10800 1 0 $X=63260 $Y=6800
X114 107 103 VDD 108 VSS 150 AOI21X1TR $T=63600 18000 1 0 $X=63260 $Y=14000
X115 117 109 VDD 113 VSS 112 AOI21X1TR $T=71600 32400 1 180 $X=69260 $Y=32120
X116 clk VSS 67 VDD CLKBUFX20TR $T=45200 32400 0 0 $X=44860 $Y=32120
X117 jmp 92 VSS 74 VDD NAND2BX1TR $T=47200 39600 1 0 $X=46860 $Y=35600
X118 disp<0> br VSS 79 VDD NAND2BX1TR $T=54400 39600 1 180 $X=52060 $Y=39320
X119 92 jmp VDD VSS 97 AND2X2TR $T=49200 39600 1 0 $X=48860 $Y=35600
X120 br disp<2> VDD VSS 68 AND2X2TR $T=53600 39600 0 180 $X=51260 $Y=35600
X121 br disp<3> VDD VSS 70 AND2X2TR $T=55600 39600 0 180 $X=53260 $Y=35600
X122 br disp<1> VDD VSS 72 AND2X2TR $T=56400 39600 1 180 $X=54060 $Y=39320
X123 br disp<4> VDD VSS 94 AND2X2TR $T=57600 39600 0 180 $X=55260 $Y=35600
X124 br disp<5> VDD VSS 143 AND2X2TR $T=59600 39600 0 180 $X=57260 $Y=35600
X125 br disp<6> VDD VSS 151 AND2X2TR $T=70400 39600 0 180 $X=68060 $Y=35600
X126 br disp<7> VDD VSS 154 AND2X2TR $T=70400 39600 1 0 $X=70060 $Y=35600
X127 95 99 96 VSS VDD NOR2X2TR $T=53200 18000 1 180 $X=50860 $Y=17720
X128 100 VSS 98 89 VDD NAND2XLTR $T=54400 25200 0 180 $X=52460 $Y=21200
X129 63 VSS VDD 85 CLKINVX6TR $T=59200 18000 0 0 $X=58860 $Y=17720
X130 152 VSS 110 VDD Rlink<7> XOR2X2TR $T=71200 10800 1 180 $X=65660 $Y=10520
X131 120 VSS 112 VDD Rlink<9> XOR2X2TR $T=77600 39600 0 180 $X=72060 $Y=35600
X132 115 109 VSS VDD Rlink<8> XNOR2X2TR $T=71600 32400 0 180 $X=66060 $Y=28400
X133 addr<8> 119 VSS VDD 117 OR2X2TR $T=74400 32400 0 0 $X=74060 $Y=32120
X134 161 162 67 addr<7> VSS VDD 191 DFFTRX2TR $T=85600 18000 0 0 $X=85260 $Y=17720
X135 VSS VDD 161 TIEHITR $T=87600 18000 0 180 $X=86060 $Y=14000
X136 addr<9> scan_en 67 scan_out VSS VDD 192 DFFTRX1TR $T=86800 39600 0 0 $X=86460 $Y=39320
.ENDS
***************************************
.SUBCKT DFFXLTR D CK Q VSS VDD QN
** N=14 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI21XLTR A1 VSS A0 B0 VDD Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AO22X1TR B1 B0 A0 A1 VDD VSS Y
** N=11 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI22X1TR B1 VSS B0 A0 A1 VDD Y
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_76 1 2 3 4 5 6 7 8 9 10
** N=10 EP=10 IP=14 FDC=0
X0 1 2 3 4 5 NOR2X1TR $T=0 0 0 0 $X=-340 $Y=-280
X1 6 2 7 8 5 NOR2X1TR $T=1600 0 0 0 $X=1260 $Y=-280
.ENDS
***************************************
.SUBCKT NAND3X1TR C VSS B VDD A Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OR2XLTR A B VDD VSS Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI31X1TR VSS A2 A1 A0 B0 VDD Y
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI33X1TR B2 B1 B0 VDD A0 A1 VSS Y A2
** N=12 EP=9 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI2BB1X1TR A1N A0N B0 VSS Y VDD
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND2X2TR VSS A B Y VDD
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI33XLTR B2 B1 B0 VSS A0 A1 A2 VDD Y
** N=11 EP=9 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR4BBX1TR AN D C Y VSS BN VDD
** N=11 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI31X1TR A2 A1 A0 VDD B0 VSS Y
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI32X1TR A2 A1 A0 VDD B0 Y B1 VSS
** N=11 EP=8 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI211X1TR A1 A0 VDD C0 B0 VSS Y
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND3BX1TR AN C VSS B VDD Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR2BX1TR AN B VDD Y VSS
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI32X1TR VSS A2 A1 A0 B0 Y VDD B1
** N=11 EP=8 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR4XLTR VSS D VDD C B Y A
** N=9 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKINVX4TR A Y VSS VDD
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR3BX1TR AN C VDD B VSS Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT DFFX1TR D CK Q VDD VSS QN
** N=14 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AND3X2TR A B C VSS VDD Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND3XLTR C VSS B A VDD Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKBUFX8TR A VSS Y VDD
** N=7 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND4X1TR D VSS C B Y A VDD
** N=9 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKBUFX2TR A VSS VDD Y
** N=7 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OR3X2TR A B C VDD VSS Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI211X4TR A1 A0 C0 B0 Y VSS VDD
** N=11 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OR3X1TR A B C VDD VSS Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR2XLTR B VDD A Y VSS
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR2BXLTR AN B VDD Y VSS
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OA21XLTR A1 A0 B0 VSS VDD Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AND2XLTR A B VSS VDD Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND4XLTR D VSS C B Y A VDD
** N=9 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AO21XLTR A1 A0 B0 VSS VDD Y
** N=10 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT controllerLING VDD VSS RWL_P<10> Dsram<10> Dsram<13> RWL_P<11> RWL_P<12> RWL_P<13> RWL_P<14> RWL_N<10> RWL_N<11> RWL_N<13> RWLB_P<11> RWLB_P<15> RWLB_N<10> RENB_SRAM RWLB_N<12> RWLB_N<15> Dxac<1> Dxac<4>
+ REN_SRAM Col_Sel<0> Col_Sel<11> Col_Sel<14> Col_Sel<10> RWLB_P<12> RWLB_P<10> RWLB_N<0> Col_Sel<4> RWLB_N<1> Col_Sel<5> Col_Sel<1> RWLB_P<14> Dsram<14> Dsram<15> Col_Sel<15> Dxac<5> Dxac<2> RWLB_P<13> Col_Sel<12>
+ Col_Sel<13> Dsram<12> RWLB_N<13> RWLB_P<0> RWL_P<15> RWLB_N<11> rSrcIn<14> RWL_P<1> RWL_N<15> rSrcIn<11> RWLB_N<14> RWL_N<12> RWL_N<14> Dxac<0> Col_Sel<3> RWL_P<2> RWL_N<0> rSrcIn<13> Col_Sel<2> Dsram<11>
+ rSrcIn<2> RWLB_P<1> Dxac<3> rSrcIn<0> rDestIn<12> rSrcIn<7> RWL_N<1> rSrcIn<1> RWL_P<0> rDestIn<15> RWLB_P<7> Dxac<6> rSrcIn<9> Dxac<11> RWL_N<9> Dxac<15> Dxac<8> Dxac<7> Dxac<12> rSrcIn<8>
+ rDestIn<10> Dxac<14> RWLB_N<9> Col_Sel<7> WR_EN_SRAM WR_Data<10> RWLB_P<9> Dxac<10> WEN Dxac<9> WR_Data<0> Dxac<21> Dxac<17> rDestIn<9> WR_Data<2> RWL_P<4> Dxac<16> WR_Data<9> rSrcIn<12> Dxac<26>
+ Dxac<18> Dxac<20> RWLB_N<4> WR_Data<12> Dxac<23> instruIn<6> WR_Data<4> rDestIn<4> Dxac<25> Dxac<29> WR_Data<6> Dxac<24> Dxac<28> instruIn<7> imm8_disp<6> Dxac<30> clk Dxac<31> instruIn<11> imm8_disp<5>
+ imm8_disp<4> Br Dsram<0> FunISEL<0> DOut<0> imm8_disp<7> DOut<2> FunISEL<1> instruIn<10> pc<0> DIn<1> instruIn<3> instruIn<1> DOut<1> AluSEL<1> instruIn<2> DIn<2> Dsram<3> pc<4> instruIn<0>
+ Amount<1> N imm8_disp<1> DSEL<0> WE<5> LuiSEL ReadB<15> ReadA<1> DOut<4> imm8_disp<0> DSEL<1> pc<6> ReadB<10> C Z F ReadB<6> ReadB<5> ReadB<13> pc<7>
+ imm8_disp<3> ReadA<15> ReadB<8> ReadB<2> WE<15> pc<8> ReadB<11> DIn<3> imm8_disp<2> ReadB<7> ReadB<3> AluSEL<0> WEMaster ReadB<0> ReadB<4> ReadB<9> ReadB<12> DIn<4> WE<10> WE<6>
+ WE<1> WE<2> DOut<5> WE<14> DIn<9> DOut<6> MovSEL DOut<9> WE<8> DOut<7> WE<7> WE<9> DIn<11> WE<3> DIn<10> DOut<8> DOut<3> DIn<12> DIn<6> DIn<0>
+ DIn<15> WE<11> DIn<14> WE<4> DOut<10> DOut<14> DIn<5> DOut<11> DOut<13> DIn<8> DIn<13> DOut<15> Dsram<5> Dsram<6> ReadB<14> DIn<7> WE<13> Amount<0> Dsram<9> WE<12>
+ DOut<12> WE<0> Dsram<7> Dsram<8> SEL1 rSrcIn<10> rSrcIn<15> RWLB_P<2> RWLB_N<2> RWL_N<2> rSrcIn<3> RWLB_P<3> Col_Sel<9> RWL_N<3> RWLB_N<3> RWL_P<3> rDestIn<13> rDestIn<14> RWL_N<7> RWLB_N<6>
+ Col_Sel<8> RWLB_P<6> RWLB_N<7> rSrcIn<6> CEN RWL_N<6> RWL_N<8> RWL_P<6> Col_Sel<6> RWLB_N<5> RWL_P<7> rDestIn<11> RWLB_P<5> WR_Data<11> RWLB_P<8> RWL_P<9> rSrcIn<5> WR_Data<13> RWLB_N<8> RWL_N<5>
+ Dxac<13> RWL_P<8> Dxac<27> RWLB_P<4> rDestIn<0> RWL_P<5> rDestIn<1> rDestIn<2> WR_Data<1> Dxac<19> rDestIn<5> rSrcIn<4> Dxac<22> RWL_N<4> WR_Data<5> WR_Data<3> rDestIn<3> instruIn<5> rDestIn<6> WR_Data<8>
+ instruIn<4> WR_Data<7> rDestIn<8> rDestIn<7> Dsram<4> WR_Data<14> Dsram<1> Dsram<2> instruIn<13> instruIn<12> Stall instruIn<14> instruIn<15> Jmp WR_Data<15> pc<1> instruIn<9> pc<2> ReadA<10> pc<3>
+ ReadA<6> ReadA<8> ReadA<14> ReadA<4> instruIn<8> ReadA<12> ReadA<2> pc<5> ReadA<0> ReadA<5> ReadA<13> ReadA<9> ReadA<11> ReadB<1> ReadA<7> pc<9> ReadA<3> 318
** N=706 EP=318 IP=3582 FDC=0
X0 464 VDD imm8_disp<6> 456 VSS 592 NOR3X1TR $T=61200 25200 0 180 $X=58860 $Y=21200
X1 461 VDD FunISEL<0> 464 VSS 450 NOR3X1TR $T=63200 18000 1 180 $X=60860 $Y=17720
X2 494 VDD 496 486 VSS 468 NOR3X1TR $T=71600 32400 0 180 $X=69260 $Y=28400
X3 493 VDD 494 487 VSS LuiSEL NOR3X1TR $T=74800 25200 0 0 $X=74460 $Y=24920
X4 Dxac<1> 360 VSS 383 Dxac<0> 374 VDD NAND4BX1TR $T=24400 46800 0 0 $X=24060 $Y=46520
X5 rSrcIn<14> VSS VDD 556 CLKINVX2TR $T=10800 25200 0 0 $X=10460 $Y=24920
X6 rSrcIn<10> VSS VDD 332 CLKINVX2TR $T=12800 10800 0 180 $X=11260 $Y=6800
X7 rSrcIn<11> VSS VDD 334 CLKINVX2TR $T=13600 10800 1 180 $X=12060 $Y=10520
X8 rSrcIn<15> VSS VDD 557 CLKINVX2TR $T=14400 18000 0 180 $X=12860 $Y=14000
X9 rSrcIn<13> VSS VDD 333 CLKINVX2TR $T=15600 18000 1 180 $X=14060 $Y=17720
X10 rSrcIn<12> VSS VDD 336 CLKINVX2TR $T=16400 18000 0 180 $X=14860 $Y=14000
X11 rSrcIn<2> VSS VDD 349 CLKINVX2TR $T=17600 18000 1 0 $X=17260 $Y=14000
X12 Dxac<2> VSS VDD 360 CLKINVX2TR $T=19600 61200 1 0 $X=19260 $Y=57200
X13 rSrcIn<3> VSS VDD 357 CLKINVX2TR $T=24000 18000 1 0 $X=23660 $Y=14000
X14 Dxac<3> VSS VDD 571 CLKINVX2TR $T=27200 46800 0 0 $X=26860 $Y=46520
X15 rSrcIn<1> VSS VDD 381 CLKINVX2TR $T=30400 25200 0 0 $X=30060 $Y=24920
X16 rSrcIn<0> VSS VDD 379 CLKINVX2TR $T=30800 32400 1 0 $X=30460 $Y=28400
X17 Dxac<6> VSS VDD 390 CLKINVX2TR $T=32000 54000 1 0 $X=31660 $Y=50000
X18 rSrcIn<6> VSS VDD 395 CLKINVX2TR $T=33200 10800 1 0 $X=32860 $Y=6800
X19 rSrcIn<7> VSS VDD 573 CLKINVX2TR $T=35200 18000 1 0 $X=34860 $Y=14000
X20 391 VSS VDD 392 CLKINVX2TR $T=35200 39600 1 0 $X=34860 $Y=35600
X21 Dxac<10> VSS VDD 404 CLKINVX2TR $T=38000 46800 0 0 $X=37660 $Y=46520
X22 389 VSS VDD 574 CLKINVX2TR $T=38400 54000 1 0 $X=38060 $Y=50000
X23 Dxac<8> VSS VDD 397 CLKINVX2TR $T=40400 32400 0 0 $X=40060 $Y=32120
X24 rSrcIn<9> VSS VDD 575 CLKINVX2TR $T=41200 25200 0 0 $X=40860 $Y=24920
X25 Dxac<17> VSS VDD 414 CLKINVX2TR $T=41200 32400 1 0 $X=40860 $Y=28400
X26 rSrcIn<5> VSS VDD 577 CLKINVX2TR $T=41600 10800 1 0 $X=41260 $Y=6800
X27 Dxac<21> VSS VDD 620 CLKINVX2TR $T=41600 32400 0 0 $X=41260 $Y=32120
X28 Dxac<13> VSS VDD 581 CLKINVX2TR $T=42000 61200 1 0 $X=41660 $Y=57200
X29 rSrcIn<8> VSS VDD 579 CLKINVX2TR $T=42800 25200 1 0 $X=42460 $Y=21200
X30 Dxac<9> VSS VDD 578 CLKINVX2TR $T=44800 54000 0 180 $X=43260 $Y=50000
X31 Dxac<20> VSS VDD 424 CLKINVX2TR $T=45600 25200 0 0 $X=45260 $Y=24920
X32 437 VSS VDD 580 CLKINVX2TR $T=47200 39600 1 180 $X=45660 $Y=39320
X33 407 VSS VDD 402 CLKINVX2TR $T=47600 46800 0 180 $X=46060 $Y=42800
X34 Dxac<27> VSS VDD 421 CLKINVX2TR $T=46400 54000 1 0 $X=46060 $Y=50000
X35 Dxac<26> VSS VDD 426 CLKINVX2TR $T=48400 39600 0 180 $X=46860 $Y=35600
X36 Dxac<22> VSS VDD 427 CLKINVX2TR $T=48400 32400 1 0 $X=48060 $Y=28400
X37 Dxac<25> VSS VDD 583 CLKINVX2TR $T=48800 61200 1 0 $X=48460 $Y=57200
X38 Dxac<19> VSS VDD 586 CLKINVX2TR $T=49200 25200 0 0 $X=48860 $Y=24920
X39 Dxac<23> VSS VDD 584 CLKINVX2TR $T=49200 32400 0 0 $X=48860 $Y=32120
X40 435 VSS VDD 585 CLKINVX2TR $T=51600 39600 0 180 $X=50060 $Y=35600
X41 429 VSS VDD 588 CLKINVX2TR $T=51600 39600 1 0 $X=51260 $Y=35600
X42 Dxac<29> VSS VDD 587 CLKINVX2TR $T=51600 61200 1 0 $X=51260 $Y=57200
X43 rSrcIn<4> VSS VDD 589 CLKINVX2TR $T=52000 10800 0 0 $X=51660 $Y=10520
X44 326 VSS VDD 335 CLKINVX2TR $T=52000 18000 1 0 $X=51660 $Y=14000
X45 Dxac<16> VSS VDD 434 CLKINVX2TR $T=55600 61200 1 0 $X=55260 $Y=57200
X46 430 VSS VDD 547 CLKINVX2TR $T=57200 32400 1 0 $X=56860 $Y=28400
X47 450 VSS VDD 475 CLKINVX2TR $T=62800 25200 1 0 $X=62460 $Y=21200
X48 474 VSS VDD WR_EN_SRAM CLKINVX2TR $T=65200 39600 1 180 $X=63660 $Y=39320
X49 477 VSS VDD 483 CLKINVX2TR $T=68000 10800 1 0 $X=67660 $Y=6800
X50 599 VSS VDD 496 CLKINVX2TR $T=71600 32400 0 0 $X=71260 $Y=32120
X51 495 VSS VDD 600 CLKINVX2TR $T=71600 61200 1 0 $X=71260 $Y=57200
X52 491 VSS VDD 498 CLKINVX2TR $T=72400 10800 0 0 $X=72060 $Y=10520
X53 480 VSS VDD 494 CLKINVX2TR $T=74000 32400 1 0 $X=73660 $Y=28400
X54 603 VSS VDD 601 CLKINVX2TR $T=75600 18000 1 180 $X=74060 $Y=17720
X55 473 VSS VDD 505 CLKINVX2TR $T=74400 54000 1 0 $X=74060 $Y=50000
X56 Dsram<3> VSS VDD 605 CLKINVX2TR $T=75200 32400 1 0 $X=74860 $Y=28400
X57 514 VSS VDD 520 CLKINVX2TR $T=80400 54000 1 0 $X=80060 $Y=50000
X58 524 VSS VDD 606 CLKINVX2TR $T=81600 61200 0 180 $X=80060 $Y=57200
X59 525 VSS VDD 511 CLKINVX2TR $T=83600 61200 1 0 $X=83260 $Y=57200
X60 imm8_disp<1> VSS VDD 610 CLKINVX2TR $T=85200 18000 0 0 $X=84860 $Y=17720
X61 imm8_disp<0> VSS VDD 609 CLKINVX2TR $T=86400 25200 0 180 $X=84860 $Y=21200
X62 503 VSS VDD 501 CLKINVX2TR $T=92000 39600 1 180 $X=90460 $Y=39320
X63 imm8_disp<2> VSS VDD 540 CLKINVX2TR $T=94000 18000 1 0 $X=93660 $Y=14000
X64 469 VSS VDD WEMaster CLKINVX2TR $T=94400 39600 1 0 $X=94060 $Y=35600
X65 imm8_disp<3> VSS VDD 543 CLKINVX2TR $T=100400 10800 1 180 $X=98860 $Y=10520
X66 Dsram<5> VSS VDD 615 CLKINVX2TR $T=100800 32400 1 180 $X=99260 $Y=32120
X67 Dsram<6> VSS VDD 616 CLKINVX2TR $T=105200 39600 1 180 $X=103660 $Y=39320
X68 Dsram<9> VSS VDD 627 CLKINVX2TR $T=107600 32400 0 180 $X=106060 $Y=28400
X69 Dsram<8> VSS VDD 618 CLKINVX2TR $T=108000 25200 1 180 $X=106460 $Y=24920
X70 Dsram<7> VSS VDD 628 CLKINVX2TR $T=108000 39600 0 180 $X=106460 $Y=35600
X71 REN_SRAM VSS VDD RENB_SRAM INVX2TR $T=8400 39600 1 180 $X=6860 $Y=39320
X72 619 VSS VDD CEN INVX2TR $T=36000 61200 0 180 $X=34460 $Y=57200
X73 492 VSS VDD 486 INVX2TR $T=71600 32400 1 180 $X=70060 $Y=32120
X74 481 VSS VDD 487 INVX2TR $T=75200 25200 1 0 $X=74860 $Y=21200
X75 494 481 VSS 493 475 imm8_disp<5> 471 VDD MovSEL OAI33X1TR $T=73200 25200 0 180 $X=69260 $Y=21200
X76 rDestIn<10> WR_EN_SRAM VSS VDD WR_Data<10> CLKAND2X2TR $T=36000 54000 0 0 $X=35660 $Y=53720
X77 rDestIn<11> WR_EN_SRAM VSS VDD WR_Data<11> CLKAND2X2TR $T=38000 54000 0 0 $X=37660 $Y=53720
X78 rDestIn<13> WR_EN_SRAM VSS VDD WR_Data<13> CLKAND2X2TR $T=40000 54000 0 0 $X=39660 $Y=53720
X79 rDestIn<9> WR_EN_SRAM VSS VDD WR_Data<9> CLKAND2X2TR $T=42000 54000 0 0 $X=41660 $Y=53720
X80 rDestIn<0> WR_EN_SRAM VSS VDD WR_Data<0> CLKAND2X2TR $T=44000 54000 0 0 $X=43660 $Y=53720
X81 rDestIn<12> WR_EN_SRAM VSS VDD WR_Data<12> CLKAND2X2TR $T=44400 46800 0 0 $X=44060 $Y=46520
X82 rDestIn<1> WR_EN_SRAM VSS VDD WR_Data<1> CLKAND2X2TR $T=46000 54000 0 0 $X=45660 $Y=53720
X83 rDestIn<2> WR_EN_SRAM VSS VDD WR_Data<2> CLKAND2X2TR $T=46400 46800 0 0 $X=46060 $Y=46520
X84 rDestIn<5> WR_EN_SRAM VSS VDD WR_Data<5> CLKAND2X2TR $T=48000 54000 0 0 $X=47660 $Y=53720
X85 rDestIn<4> WR_EN_SRAM VSS VDD WR_Data<4> CLKAND2X2TR $T=50400 46800 1 180 $X=48060 $Y=46520
X86 rDestIn<3> WR_EN_SRAM VSS VDD WR_Data<3> CLKAND2X2TR $T=52000 54000 1 180 $X=49660 $Y=53720
X87 rDestIn<6> WR_EN_SRAM VSS VDD WR_Data<6> CLKAND2X2TR $T=54000 54000 1 180 $X=51660 $Y=53720
X88 rDestIn<8> WR_EN_SRAM VSS VDD WR_Data<8> CLKAND2X2TR $T=55600 61200 0 180 $X=53260 $Y=57200
X89 rDestIn<7> WR_EN_SRAM VSS VDD WR_Data<7> CLKAND2X2TR $T=56000 54000 1 180 $X=53660 $Y=53720
X90 423 335 VSS VDD 430 CLKAND2X2TR $T=55200 32400 0 0 $X=54860 $Y=32120
X91 rDestIn<14> WR_EN_SRAM VSS VDD WR_Data<14> CLKAND2X2TR $T=56800 61200 1 0 $X=56460 $Y=57200
X92 rDestIn<15> WR_EN_SRAM VSS VDD WR_Data<15> CLKAND2X2TR $T=62400 61200 1 0 $X=62060 $Y=57200
X93 499 478 VSS VDD 479 CLKAND2X2TR $T=71200 25200 1 180 $X=68860 $Y=24920
X94 DIn<4> 488 VSS VDD 517 CLKAND2X2TR $T=98000 18000 0 180 $X=95660 $Y=14000
X95 607 511 510 VDD VSS XNOR2X1TR $T=78400 46800 0 180 $X=74860 $Y=42800
X96 522 525 523 VDD VSS XNOR2X1TR $T=85600 46800 1 0 $X=85260 $Y=42800
X97 320 VSS VDD RWLB_N<10> CLKINVX1TR $T=8400 39600 0 180 $X=6860 $Y=35600
X98 324 VSS VDD RWLB_N<15> CLKINVX1TR $T=8400 46800 0 180 $X=6860 $Y=42800
X99 554 VSS VDD RWLB_N<12> CLKINVX1TR $T=9600 39600 1 180 $X=8060 $Y=39320
X100 555 VSS VDD RWLB_N<13> CLKINVX1TR $T=10800 39600 1 180 $X=9260 $Y=39320
X101 319 VSS VDD RWLB_N<11> CLKINVX1TR $T=12000 39600 1 180 $X=10460 $Y=39320
X102 338 VSS VDD RWLB_N<14> CLKINVX1TR $T=13200 39600 1 180 $X=11660 $Y=39320
X103 356 VSS VDD RWLB_N<0> CLKINVX1TR $T=18800 32400 0 180 $X=17260 $Y=28400
X104 345 VSS VDD RWLB_N<2> CLKINVX1TR $T=19200 10800 0 180 $X=17660 $Y=6800
X105 361 VSS VDD RWLB_N<3> CLKINVX1TR $T=25200 10800 0 180 $X=23660 $Y=6800
X106 568 VSS VDD RWLB_N<1> CLKINVX1TR $T=25200 32400 1 180 $X=23660 $Y=32120
X107 572 VSS VDD RWLB_N<6> CLKINVX1TR $T=28400 10800 1 0 $X=28060 $Y=6800
X108 393 VSS VDD RWLB_N<7> CLKINVX1TR $T=34400 18000 0 180 $X=32860 $Y=14000
X109 405 VSS VDD RWLB_N<5> CLKINVX1TR $T=39600 10800 0 180 $X=38060 $Y=6800
X110 406 VSS VDD RWLB_N<9> CLKINVX1TR $T=40000 25200 1 180 $X=38460 $Y=24920
X111 411 VSS VDD RWLB_N<8> CLKINVX1TR $T=41200 18000 1 0 $X=40860 $Y=14000
X112 428 VSS VDD RWLB_N<4> CLKINVX1TR $T=55200 10800 0 180 $X=53660 $Y=6800
X113 332 VSS 326 323 VDD RWL_P<10> OAI21X1TR $T=9600 10800 0 180 $X=7260 $Y=6800
X114 323 VSS 327 319 VDD RWLB_P<11> OAI21X1TR $T=7600 32400 1 0 $X=7260 $Y=28400
X115 323 VSS 328 554 VDD RWLB_P<12> OAI21X1TR $T=7600 32400 0 0 $X=7260 $Y=32120
X116 334 VSS 326 323 VDD RWL_P<11> OAI21X1TR $T=10000 10800 1 180 $X=7660 $Y=10520
X117 333 VSS 326 323 VDD RWL_P<13> OAI21X1TR $T=10000 18000 1 180 $X=7660 $Y=17720
X118 323 VSS 330 320 VDD RWLB_P<10> OAI21X1TR $T=8000 25200 0 0 $X=7660 $Y=24920
X119 323 VSS 331 555 VDD RWLB_P<13> OAI21X1TR $T=8400 39600 1 0 $X=8060 $Y=35600
X120 557 VSS 326 323 VDD RWL_P<15> OAI21X1TR $T=13200 18000 0 180 $X=10860 $Y=14000
X121 323 VSS 339 324 VDD RWLB_P<15> OAI21X1TR $T=13600 39600 0 180 $X=11260 $Y=35600
X122 349 VSS 326 323 VDD RWL_P<2> OAI21X1TR $T=14800 10800 0 180 $X=12460 $Y=6800
X123 323 VSS 346 338 VDD RWLB_P<14> OAI21X1TR $T=16000 32400 1 180 $X=13660 $Y=32120
X124 323 VSS 560 345 VDD RWLB_P<2> OAI21X1TR $T=16000 10800 1 0 $X=15660 $Y=6800
X125 323 VSS 358 361 VDD RWLB_P<3> OAI21X1TR $T=20400 10800 1 0 $X=20060 $Y=6800
X126 323 VSS 369 356 VDD RWLB_P<0> OAI21X1TR $T=24800 32400 0 180 $X=22460 $Y=28400
X127 323 VSS 567 568 VDD RWLB_P<1> OAI21X1TR $T=24800 32400 1 0 $X=24460 $Y=28400
X128 357 VSS 326 323 VDD RWL_P<3> OAI21X1TR $T=25200 10800 1 0 $X=24860 $Y=6800
X129 323 VSS 378 572 VDD RWLB_P<6> OAI21X1TR $T=28000 10800 0 0 $X=27660 $Y=10520
X130 381 VSS 326 323 VDD RWL_P<1> OAI21X1TR $T=29600 32400 0 0 $X=29260 $Y=32120
X131 323 VSS 388 393 VDD RWLB_P<7> OAI21X1TR $T=31200 18000 0 0 $X=30860 $Y=17720
X132 379 VSS 326 323 VDD RWL_P<0> OAI21X1TR $T=31600 25200 0 0 $X=31260 $Y=24920
X133 395 VSS 326 323 VDD RWL_P<6> OAI21X1TR $T=35200 10800 1 0 $X=34860 $Y=6800
X134 323 VSS 403 405 VDD RWLB_P<5> OAI21X1TR $T=36800 10800 0 0 $X=36460 $Y=10520
X135 323 VSS 400 406 VDD RWLB_P<9> OAI21X1TR $T=36800 25200 0 0 $X=36460 $Y=24920
X136 573 VSS 326 323 VDD RWL_P<7> OAI21X1TR $T=37200 18000 1 0 $X=36860 $Y=14000
X137 323 VSS 398 411 VDD RWLB_P<8> OAI21X1TR $T=39200 18000 0 0 $X=38860 $Y=17720
X138 575 VSS 326 323 VDD RWL_P<9> OAI21X1TR $T=42400 25200 0 180 $X=40060 $Y=21200
X139 579 VSS 326 323 VDD RWL_P<8> OAI21X1TR $T=43600 18000 0 0 $X=43260 $Y=17720
X140 577 VSS 326 323 VDD RWL_P<5> OAI21X1TR $T=45200 10800 1 0 $X=44860 $Y=6800
X141 323 VSS 399 428 VDD RWLB_P<4> OAI21X1TR $T=47200 10800 0 0 $X=46860 $Y=10520
X142 409 VSS Dxac<16> 585 VDD 433 OAI21X1TR $T=48400 39600 1 0 $X=48060 $Y=35600
X143 589 VSS 326 323 VDD RWL_P<4> OAI21X1TR $T=51600 10800 0 180 $X=49260 $Y=6800
X144 587 VSS 442 440 VDD 448 OAI21X1TR $T=52800 54000 1 0 $X=52460 $Y=50000
X145 442 VSS Dxac<29> 431 VDD 591 OAI21X1TR $T=56400 39600 1 180 $X=54060 $Y=39320
X146 596 VSS 461 486 VDD 490 OAI21X1TR $T=67200 18000 0 0 $X=66860 $Y=17720
X147 332 VSS 320 335 VDD NAND2X1TR $T=9600 10800 1 0 $X=9260 $Y=6800
X148 336 VSS 554 335 VDD NAND2X1TR $T=9600 32400 1 0 $X=9260 $Y=28400
X149 333 VSS 555 335 VDD NAND2X1TR $T=9600 32400 0 0 $X=9260 $Y=32120
X150 334 VSS 319 335 VDD NAND2X1TR $T=10000 10800 0 0 $X=9660 $Y=10520
X151 557 VSS 324 335 VDD NAND2X1TR $T=12800 32400 0 180 $X=10860 $Y=28400
X152 556 VSS 338 335 VDD NAND2X1TR $T=11600 32400 0 0 $X=11260 $Y=32120
X153 349 VSS 345 335 VDD NAND2X1TR $T=15200 10800 1 180 $X=13260 $Y=10520
X154 357 VSS 354 349 VDD NAND2X1TR $T=19200 18000 1 180 $X=17260 $Y=17720
X155 357 VSS 361 335 VDD NAND2X1TR $T=21600 10800 1 180 $X=19660 $Y=10520
X156 349 VSS 359 rSrcIn<3> VDD NAND2X1TR $T=20400 18000 1 0 $X=20060 $Y=14000
X157 rSrcIn<3> VSS 562 rSrcIn<2> VDD NAND2X1TR $T=20800 18000 0 0 $X=20460 $Y=17720
X158 353 VSS 348 335 VDD NAND2X1TR $T=22800 39600 0 180 $X=20860 $Y=35600
X159 357 VSS 372 rSrcIn<2> VDD NAND2X1TR $T=26000 18000 1 180 $X=24060 $Y=17720
X160 379 VSS 356 335 VDD NAND2X1TR $T=28400 32400 1 180 $X=26460 $Y=32120
X161 381 VSS 568 335 VDD NAND2X1TR $T=28800 32400 0 180 $X=26860 $Y=28400
X162 rSrcIn<0> VSS 362 rSrcIn<1> VDD NAND2X1TR $T=28800 25200 0 0 $X=28460 $Y=24920
X163 381 VSS 373 rSrcIn<0> VDD NAND2X1TR $T=29200 32400 1 0 $X=28860 $Y=28400
X164 379 VSS 368 rSrcIn<1> VDD NAND2X1TR $T=30000 25200 1 0 $X=29660 $Y=21200
X165 395 VSS 572 335 VDD NAND2X1TR $T=32400 10800 0 180 $X=30460 $Y=6800
X166 367 VSS 366 326 VDD NAND2X1TR $T=31600 32400 0 0 $X=31260 $Y=32120
X167 381 VSS 365 379 VDD NAND2X1TR $T=32000 32400 1 0 $X=31660 $Y=28400
X168 397 VSS 391 Dxac<4> VDD NAND2X1TR $T=34800 39600 1 180 $X=32860 $Y=39320
X169 573 VSS 393 335 VDD NAND2X1TR $T=37200 18000 0 0 $X=36860 $Y=17720
X170 577 VSS 405 335 VDD NAND2X1TR $T=42800 10800 0 0 $X=42460 $Y=10520
X171 575 VSS 406 335 VDD NAND2X1TR $T=42800 25200 0 0 $X=42460 $Y=24920
X172 579 VSS 411 335 VDD NAND2X1TR $T=44800 25200 1 0 $X=44460 $Y=21200
X173 427 VSS 416 Dxac<20> VDD NAND2X1TR $T=48400 32400 0 180 $X=46460 $Y=28400
X174 589 VSS 428 335 VDD NAND2X1TR $T=54400 10800 0 0 $X=54060 $Y=10520
X175 335 VSS 455 423 VDD NAND2X1TR $T=56800 39600 1 0 $X=56460 $Y=35600
X176 340 VSS 594 Dsram<4> VDD NAND2X1TR $T=60000 32400 1 180 $X=58060 $Y=32120
X177 340 VSS 457 Dsram<1> VDD NAND2X1TR $T=59200 61200 1 0 $X=58860 $Y=57200
X178 464 VSS WEN 459 VDD NAND2X1TR $T=61600 32400 0 180 $X=59660 $Y=28400
X179 340 VSS 460 Dsram<0> VDD NAND2X1TR $T=60000 39600 0 0 $X=59660 $Y=39320
X180 340 VSS 595 Dsram<2> VDD NAND2X1TR $T=60000 54000 1 0 $X=59660 $Y=50000
X181 471 VSS 465 450 VDD NAND2X1TR $T=62400 25200 1 180 $X=60460 $Y=24920
X182 464 VSS 596 471 VDD NAND2X1TR $T=66800 18000 1 180 $X=64860 $Y=17720
X183 494 VSS FunISEL<0> 502 VDD NAND2X1TR $T=76400 25200 1 0 $X=76060 $Y=21200
X184 500 VSS 509 507 VDD NAND2X1TR $T=79200 32400 0 0 $X=78860 $Y=32120
X185 511 VSS 531 514 VDD NAND2X1TR $T=83200 54000 0 180 $X=81260 $Y=50000
X186 609 VSS 506 imm8_disp<1> VDD NAND2X1TR $T=84400 18000 0 180 $X=82460 $Y=14000
X187 518 VSS DOut<4> 462 VDD NAND2X1TR $T=83200 32400 1 0 $X=82860 $Y=28400
X188 520 VSS 528 511 VDD NAND2X1TR $T=85600 54000 0 180 $X=83660 $Y=50000
X189 473 VSS 529 503 VDD NAND2X1TR $T=86400 61200 0 180 $X=84460 $Y=57200
X190 610 VSS 521 609 VDD NAND2X1TR $T=86800 18000 0 180 $X=84860 $Y=14000
X191 505 VSS 530 503 VDD NAND2X1TR $T=87200 54000 0 180 $X=85260 $Y=50000
X192 610 VSS 527 imm8_disp<0> VDD NAND2X1TR $T=88000 18000 1 0 $X=87660 $Y=14000
X193 505 VSS 536 501 VDD NAND2X1TR $T=88800 46800 1 0 $X=88460 $Y=42800
X194 525 VSS 533 514 VDD NAND2X1TR $T=90400 54000 1 180 $X=88460 $Y=53720
X195 501 VSS 539 473 VDD NAND2X1TR $T=90400 46800 1 0 $X=90060 $Y=42800
X196 540 VSS 508 imm8_disp<3> VDD NAND2X1TR $T=92400 10800 1 180 $X=90460 $Y=10520
X197 imm8_disp<3> VSS 519 imm8_disp<2> VDD NAND2X1TR $T=90800 18000 1 0 $X=90460 $Y=14000
X198 520 VSS 535 525 VDD NAND2X1TR $T=92000 39600 0 0 $X=91660 $Y=39320
X199 543 VSS 512 imm8_disp<2> VDD NAND2X1TR $T=95200 10800 1 180 $X=93260 $Y=10520
X200 543 VSS 526 540 VDD NAND2X1TR $T=97200 10800 1 180 $X=95260 $Y=10520
X201 imm8_disp<0> VSS 538 imm8_disp<1> VDD NAND2X1TR $T=98800 18000 1 0 $X=98460 $Y=14000
X202 354 VDD 368 560 VSS NOR2X1TR $T=19200 25200 0 180 $X=17260 $Y=21200
X203 562 VDD 368 346 VSS NOR2X1TR $T=20800 25200 0 180 $X=18860 $Y=21200
X204 562 VDD 373 331 VSS NOR2X1TR $T=21200 32400 0 180 $X=19260 $Y=28400
X205 368 VDD 359 330 VSS NOR2X1TR $T=22400 25200 0 180 $X=20460 $Y=21200
X206 562 VDD 362 339 VSS NOR2X1TR $T=20800 25200 0 0 $X=20460 $Y=24920
X207 562 VDD 365 328 VSS NOR2X1TR $T=21200 32400 1 0 $X=20860 $Y=28400
X208 362 VDD 359 327 VSS NOR2X1TR $T=24000 25200 0 180 $X=22060 $Y=21200
X209 354 VDD 362 358 VSS NOR2X1TR $T=24000 25200 1 0 $X=23660 $Y=21200
X210 368 VDD 372 378 VSS NOR2X1TR $T=27200 18000 0 0 $X=26860 $Y=17720
X211 Dxac<3> VDD 360 386 VSS NOR2X1TR $T=28400 46800 0 0 $X=28060 $Y=46520
X212 362 VDD 372 388 VSS NOR2X1TR $T=29600 18000 0 0 $X=29260 $Y=17720
X213 365 VDD 359 398 VSS NOR2X1TR $T=33200 25200 1 0 $X=32860 $Y=21200
X214 Dxac<8> VDD Dxac<4> 383 VSS NOR2X1TR $T=35200 54000 0 180 $X=33260 $Y=50000
X215 372 VDD 365 399 VSS NOR2X1TR $T=34000 18000 0 0 $X=33660 $Y=17720
X216 372 VDD 373 403 VSS NOR2X1TR $T=34800 25200 0 0 $X=34460 $Y=24920
X217 359 VDD 373 400 VSS NOR2X1TR $T=34800 32400 1 0 $X=34460 $Y=28400
X218 Dxac<12> VDD 397 407 VSS NOR2X1TR $T=40800 39600 0 0 $X=40460 $Y=39320
X219 Dxac<24> VDD 434 429 VSS NOR2X1TR $T=51600 54000 0 180 $X=49660 $Y=50000
X220 480 VDD 496 491 VSS NOR2X1TR $T=73600 25200 1 180 $X=71660 $Y=24920
X221 598 VDD 602 502 VSS NOR2X1TR $T=72800 32400 0 0 $X=72460 $Y=32120
X222 506 VDD 508 ReadA<10> VSS NOR2X1TR $T=75600 10800 1 0 $X=75260 $Y=6800
X223 506 VDD 512 ReadA<6> VSS NOR2X1TR $T=77600 10800 1 0 $X=77260 $Y=6800
X224 500 VDD 473 607 VSS NOR2X1TR $T=80000 46800 0 180 $X=78060 $Y=42800
X225 imm8_disp<4> VDD imm8_disp<5> 478 VSS NOR2X1TR $T=78800 25200 1 0 $X=78460 $Y=21200
X226 464 VDD 513 SEL1 VSS NOR2X1TR $T=78800 32400 1 0 $X=78460 $Y=28400
X227 519 VDD 506 ReadA<14> VSS NOR2X1TR $T=81200 10800 0 180 $X=79260 $Y=6800
X228 521 VDD 508 ReadA<8> VSS NOR2X1TR $T=82000 10800 1 180 $X=80060 $Y=10520
X229 512 VDD 521 ReadA<4> VSS NOR2X1TR $T=81600 10800 1 0 $X=81260 $Y=6800
X230 526 VDD 506 ReadA<2> VSS NOR2X1TR $T=84800 10800 0 180 $X=82860 $Y=6800
X231 519 VDD 527 ReadA<13> VSS NOR2X1TR $T=84000 10800 0 0 $X=83660 $Y=10520
X232 514 VDD 511 504 VSS NOR2X1TR $T=85600 46800 0 180 $X=83660 $Y=42800
X233 526 VDD 521 ReadA<0> VSS NOR2X1TR $T=86400 10800 0 180 $X=84460 $Y=6800
X234 499 VDD DSEL<0> DSEL<1> VSS NOR2X1TR $T=85600 32400 1 0 $X=85260 $Y=28400
X235 512 VDD 527 ReadA<5> VSS NOR2X1TR $T=87600 10800 1 180 $X=85660 $Y=10520
X236 526 VDD 527 ReadA<1> VSS NOR2X1TR $T=88800 10800 0 180 $X=86860 $Y=6800
X237 508 VDD 527 ReadA<9> VSS NOR2X1TR $T=90000 10800 1 180 $X=88060 $Y=10520
X238 538 VDD 508 ReadA<11> VSS NOR2X1TR $T=90800 10800 0 180 $X=88860 $Y=6800
X239 528 VDD 539 ReadB<8> VSS NOR2X1TR $T=90800 54000 1 0 $X=90460 $Y=50000
X240 519 VDD 538 ReadA<15> VSS NOR2X1TR $T=91600 10800 1 0 $X=91260 $Y=6800
X241 533 VDD 539 ReadB<11> VSS NOR2X1TR $T=92400 46800 0 0 $X=92060 $Y=46520
X242 536 VDD 533 ReadB<3> VSS NOR2X1TR $T=92800 61200 1 0 $X=92460 $Y=57200
X243 529 VDD 531 ReadB<14> VSS NOR2X1TR $T=93600 39600 0 0 $X=93260 $Y=39320
X244 538 VDD 512 ReadA<7> VSS NOR2X1TR $T=95600 10800 0 180 $X=93660 $Y=6800
X245 533 VDD 530 ReadB<7> VSS NOR2X1TR $T=94400 25200 1 0 $X=94060 $Y=21200
X246 526 VDD 538 ReadA<3> VSS NOR2X1TR $T=97200 10800 1 0 $X=96860 $Y=6800
X247 536 VDD 531 ReadB<2> VSS NOR2X1TR $T=103200 54000 1 180 $X=101260 $Y=53720
X248 530 VDD 528 ReadB<4> VSS NOR2X1TR $T=103200 54000 0 0 $X=102860 $Y=53720
X249 536 VDD 528 ReadB<0> VSS NOR2X1TR $T=106400 54000 1 180 $X=104460 $Y=53720
X250 instruIn<11> 337 VSS VDD 473 DFFQX1TR $T=58000 46800 0 0 $X=57660 $Y=46520
X251 instruIn<13> 337 VSS VDD 481 DFFQX1TR $T=61600 46800 1 0 $X=61260 $Y=42800
X252 instruIn<12> 337 VSS VDD 492 DFFQX1TR $T=64800 46800 0 0 $X=64460 $Y=46520
X253 instruIn<10> 337 VSS VDD 503 DFFQX1TR $T=68400 46800 1 0 $X=68060 $Y=42800
X254 instruIn<9> 485 VSS VDD 514 DFFQX1TR $T=73600 46800 0 0 $X=73260 $Y=46520
X255 N 485 VSS VDD 500 DFFQX1TR $T=80800 39600 0 180 $X=73660 $Y=35600
X256 instruIn<15> 485 VSS VDD 480 DFFQX1TR $T=75200 54000 0 0 $X=74860 $Y=53720
X257 instruIn<1> 485 VSS VDD imm8_disp<1> DFFQX1TR $T=77600 18000 0 0 $X=77260 $Y=17720
X258 C 485 VSS VDD 522 DFFQX1TR $T=87600 39600 0 180 $X=80460 $Y=35600
X259 Z 485 VSS VDD 507 DFFQX1TR $T=88000 32400 1 180 $X=80860 $Y=32120
X260 instruIn<0> 485 VSS VDD imm8_disp<0> DFFQX1TR $T=81600 25200 0 0 $X=81260 $Y=24920
X261 instruIn<8> 485 VSS VDD 525 DFFQX1TR $T=82000 54000 0 0 $X=81660 $Y=53720
X262 537 485 VSS VDD WE<5> DFFQX1TR $T=90000 46800 1 180 $X=82860 $Y=46520
X263 instruIn<3> 485 VSS VDD imm8_disp<3> DFFQX1TR $T=86400 18000 0 0 $X=86060 $Y=17720
X264 instruIn<2> 485 VSS VDD imm8_disp<2> DFFQX1TR $T=86800 25200 1 0 $X=86460 $Y=21200
X265 532 485 VSS VDD WE<10> DFFQX1TR $T=87200 32400 1 0 $X=86860 $Y=28400
X266 611 485 VSS VDD WE<15> DFFQX1TR $T=88000 32400 0 0 $X=87660 $Y=32120
X267 534 485 VSS VDD WE<13> DFFQX1TR $T=88400 25200 0 0 $X=88060 $Y=24920
X268 612 485 VSS VDD WE<6> DFFQX1TR $T=92000 46800 1 0 $X=91660 $Y=42800
X269 541 485 VSS VDD WE<14> DFFQX1TR $T=93600 18000 0 0 $X=93260 $Y=17720
X270 542 485 VSS VDD WE<9> DFFQX1TR $T=94000 32400 1 0 $X=93660 $Y=28400
X271 613 485 VSS VDD WE<2> DFFQX1TR $T=94400 61200 1 0 $X=94060 $Y=57200
X272 624 485 VSS VDD WE<1> DFFQX1TR $T=94800 54000 0 0 $X=94460 $Y=53720
X273 544 485 VSS VDD WE<11> DFFQX1TR $T=95600 25200 0 0 $X=95260 $Y=24920
X274 625 485 VSS VDD WE<8> DFFQX1TR $T=97200 39600 0 0 $X=96860 $Y=39320
X275 626 485 VSS VDD WE<4> DFFQX1TR $T=98000 46800 0 0 $X=97660 $Y=46520
X276 545 485 VSS VDD WE<3> DFFQX1TR $T=98400 54000 1 0 $X=98060 $Y=50000
X277 546 485 VSS VDD WE<7> DFFQX1TR $T=98800 46800 1 0 $X=98460 $Y=42800
X278 614 485 VSS VDD WE<12> DFFQX1TR $T=99200 25200 1 0 $X=98860 $Y=21200
X279 548 485 VSS VDD WE<0> DFFQX1TR $T=101200 61200 1 0 $X=100860 $Y=57200
X280 417 Dxac<13> VSS 413 576 VDD 415 OAI211X1TR $T=44000 46800 1 180 $X=41260 $Y=46520
X281 581 417 VSS 413 412 VDD 446 OAI211X1TR $T=46400 46800 0 180 $X=43660 $Y=42800
X282 424 Dxac<23> VSS 582 416 VDD 425 OAI211X1TR $T=48800 32400 1 180 $X=46060 $Y=32120
X283 418 Dxac<16> VSS 444 442 VDD 435 OAI211X1TR $T=52000 39600 0 0 $X=51660 $Y=39320
X284 588 443 VSS 432 444 VDD 445 OAI211X1TR $T=53200 46800 1 0 $X=52860 $Y=42800
X285 326 449 VSS 460 467 VDD DOut<0> OAI211X1TR $T=61200 39600 1 0 $X=60860 $Y=35600
X286 326 439 VSS 595 466 VDD DOut<2> OAI211X1TR $T=61600 39600 0 0 $X=61260 $Y=39320
X287 326 463 VSS 457 489 VDD DOut<1> OAI211X1TR $T=72400 39600 0 0 $X=72060 $Y=39320
X288 605 323 VSS 451 608 VDD DOut<3> OAI211X1TR $T=76800 32400 0 0 $X=76460 $Y=32120
X289 615 323 VSS 455 549 VDD DOut<5> OAI211X1TR $T=99600 39600 1 0 $X=99260 $Y=35600
X290 616 323 VSS 455 550 VDD DOut<6> OAI211X1TR $T=104400 39600 0 180 $X=101660 $Y=35600
X291 627 323 VSS 547 551 VDD DOut<9> OAI211X1TR $T=106400 32400 0 180 $X=103660 $Y=28400
X292 618 323 VSS 547 617 VDD DOut<8> OAI211X1TR $T=106800 32400 1 180 $X=104060 $Y=32120
X293 628 323 VSS 455 552 VDD DOut<7> OAI211X1TR $T=106800 39600 0 180 $X=104060 $Y=35600
X294 483 VDD pc<0> DIn<0> 488 VSS 467 AOI22X1TR $T=69600 25200 0 180 $X=66860 $Y=21200
X295 483 VDD pc<1> DIn<1> 488 VSS 489 AOI22X1TR $T=72400 10800 0 180 $X=69660 $Y=6800
X296 483 VDD pc<2> DIn<2> 488 VSS 466 AOI22X1TR $T=76000 10800 1 180 $X=73260 $Y=10520
X297 511 VDD 509 604 504 VSS 497 AOI22X1TR $T=77200 39600 1 180 $X=74460 $Y=39320
X298 494 VDD 496 480 493 VSS 513 AOI22X1TR $T=76400 32400 1 0 $X=76060 $Y=28400
X299 483 VDD pc<3> DIn<3> 488 VSS 608 AOI22X1TR $T=76800 10800 0 0 $X=76460 $Y=10520
X300 504 VDD 507 514 523 VSS 516 AOI22X1TR $T=80400 46800 0 0 $X=80060 $Y=46520
X301 483 VDD pc<9> DIn<9> 488 VSS 551 AOI22X1TR $T=96000 25200 1 0 $X=95660 $Y=21200
X302 483 VDD pc<5> DIn<5> 488 VSS 549 AOI22X1TR $T=102400 10800 1 0 $X=102060 $Y=6800
X303 483 VDD pc<8> DIn<8> 488 VSS 617 AOI22X1TR $T=102400 18000 0 0 $X=102060 $Y=17720
X304 483 VDD pc<7> DIn<7> 488 VSS 552 AOI22X1TR $T=102800 18000 1 0 $X=102460 $Y=14000
X305 483 VDD pc<6> DIn<6> 488 VSS 550 AOI22X1TR $T=103200 10800 0 0 $X=102860 $Y=10520
X306 340 Dsram<13> VDD 430 VSS 436 AOI21X1TR $T=47600 25200 1 0 $X=47260 $Y=21200
X307 340 Dsram<12> VDD 430 VSS 438 AOI21X1TR $T=48000 18000 0 0 $X=47660 $Y=17720
X308 340 Dsram<11> VDD 430 VSS 441 AOI21X1TR $T=51600 25200 1 0 $X=51260 $Y=21200
X309 340 Dsram<10> VDD 430 VSS 447 AOI21X1TR $T=52400 25200 0 0 $X=52060 $Y=24920
X310 340 Dsram<15> VDD 430 VSS 452 AOI21X1TR $T=54000 32400 1 0 $X=53660 $Y=28400
X311 340 Dsram<14> VDD 430 VSS 454 AOI21X1TR $T=55600 25200 0 0 $X=55260 $Y=24920
X312 468 481 VDD 459 VSS 470 AOI21X1TR $T=68000 32400 0 180 $X=65660 $Y=28400
X313 clk VSS 485 VDD CLKBUFX20TR $T=64000 54000 1 0 $X=63660 $Y=50000
X314 clk VSS 337 VDD CLKBUFX20TR $T=64000 54000 0 0 $X=63660 $Y=53720
X315 Dxac<30> 453 VSS 442 VDD NAND2BX1TR $T=59960 54000 0 180 $X=57620 $Y=50000
X316 468 465 VSS FunISEL<1> VDD NAND2BX1TR $T=62800 32400 1 0 $X=62460 $Y=28400
X317 FunISEL<0> 478 VSS 456 VDD NAND2BX1TR $T=67200 25200 0 180 $X=64860 $Y=21200
X318 483 REN_SRAM 488 VSS VDD NOR2X2TR $T=66400 25200 0 0 $X=66060 $Y=24920
X319 482 VSS 477 Jmp VDD NAND2XLTR $T=67200 10800 0 180 $X=65260 $Y=6800
X320 597 VSS 480 472 VDD NAND2XLTR $T=67600 39600 0 180 $X=65660 $Y=35600
X321 496 VSS 492 493 VDD NAND2XLTR $T=72400 32400 1 0 $X=72060 $Y=28400
X322 366 350 VSS VDD 355 OR2X2TR $T=16800 39600 0 0 $X=16460 $Y=39320
X323 481 492 VSS VDD 602 OR2X2TR $T=71600 46800 0 0 $X=71260 $Y=46520
X324 507 500 VSS VDD 604 OR2X2TR $T=76800 32400 1 180 $X=74460 $Y=32120
X325 VSS VDD 619 TIEHITR $T=41600 61200 0 180 $X=40060 $Y=57200
X326 558 337 Col_Sel<13> VSS VDD 321 DFFXLTR $T=14400 54000 0 180 $X=6860 $Y=50000
X327 342 337 Col_Sel<12> VSS VDD 322 DFFXLTR $T=14400 54000 1 180 $X=6860 $Y=53720
X328 343 337 Col_Sel<14> VSS VDD 325 DFFXLTR $T=14800 46800 1 180 $X=7260 $Y=46520
X329 559 337 Col_Sel<1> VSS VDD 329 DFFXLTR $T=15600 46800 0 180 $X=8060 $Y=42800
X330 561 337 Col_Sel<3> VSS VDD 341 DFFXLTR $T=20800 39600 0 180 $X=13260 $Y=35600
X331 563 337 Col_Sel<11> VSS VDD 344 DFFXLTR $T=21600 54000 1 180 $X=14060 $Y=53720
X332 352 337 Col_Sel<2> VSS VDD 347 DFFXLTR $T=22000 46800 1 180 $X=14460 $Y=46520
X333 564 337 Col_Sel<4> VSS VDD 351 DFFXLTR $T=24000 32400 1 180 $X=16460 $Y=32120
X334 370 337 Col_Sel<15> VSS VDD 353 DFFXLTR $T=24800 54000 0 180 $X=17260 $Y=50000
X335 566 337 Col_Sel<0> VSS VDD 350 DFFXLTR $T=25200 46800 0 180 $X=17660 $Y=42800
X336 375 337 Col_Sel<9> VSS VDD 364 DFFXLTR $T=28000 61200 0 180 $X=20460 $Y=57200
X337 569 337 Col_Sel<10> VSS VDD 363 DFFXLTR $T=28800 54000 1 180 $X=21260 $Y=53720
X338 376 337 Col_Sel<5> VSS VDD 371 DFFXLTR $T=32000 39600 0 180 $X=24460 $Y=35600
X339 380 337 Col_Sel<8> VSS VDD 570 DFFXLTR $T=36000 54000 1 180 $X=28460 $Y=53720
X340 377 337 Col_Sel<6> VSS VDD 387 DFFXLTR $T=30800 46800 0 0 $X=30460 $Y=46520
X341 394 337 Col_Sel<7> VSS VDD 385 DFFXLTR $T=32000 46800 1 0 $X=31660 $Y=42800
X342 instruIn<14> 485 598 VSS VDD 599 DFFXLTR $T=65200 39600 0 0 $X=64860 $Y=39320
X343 F 485 698 VSS VDD 623 DFFXLTR $T=88400 39600 1 180 $X=80860 $Y=39320
X344 336 VSS 326 323 VDD RWL_P<12> OAI21XLTR $T=10000 18000 0 180 $X=7660 $Y=14000
X345 556 VSS 326 323 VDD RWL_P<14> OAI21XLTR $T=10000 25200 0 180 $X=7660 $Y=21200
X346 330 340 335 rSrcIn<10> VDD VSS RWL_N<10> AO22X1TR $T=14400 18000 1 180 $X=11260 $Y=17720
X347 327 340 335 rSrcIn<11> VDD VSS RWL_N<11> AO22X1TR $T=14400 25200 0 180 $X=11260 $Y=21200
X348 339 340 335 rSrcIn<15> VDD VSS RWL_N<15> AO22X1TR $T=15600 25200 1 180 $X=12460 $Y=24920
X349 331 340 335 rSrcIn<13> VDD VSS RWL_N<13> AO22X1TR $T=17200 32400 0 180 $X=14060 $Y=28400
X350 346 340 335 rSrcIn<14> VDD VSS RWL_N<14> AO22X1TR $T=17600 25200 0 180 $X=14460 $Y=21200
X351 328 340 335 rSrcIn<12> VDD VSS RWL_N<12> AO22X1TR $T=18400 25200 1 180 $X=15260 $Y=24920
X352 560 340 335 rSrcIn<2> VDD VSS RWL_N<2> AO22X1TR $T=16400 10800 0 0 $X=16060 $Y=10520
X353 358 340 335 rSrcIn<3> VDD VSS RWL_N<3> AO22X1TR $T=26000 10800 1 180 $X=22860 $Y=10520
X354 567 340 335 rSrcIn<1> VDD VSS RWL_N<1> AO22X1TR $T=26000 25200 1 0 $X=25660 $Y=21200
X355 369 340 335 rSrcIn<0> VDD VSS RWL_N<0> AO22X1TR $T=26000 25200 0 0 $X=25660 $Y=24920
X356 388 340 335 rSrcIn<7> VDD VSS RWL_N<7> AO22X1TR $T=30000 18000 0 180 $X=26860 $Y=14000
X357 378 340 335 rSrcIn<6> VDD VSS RWL_N<6> AO22X1TR $T=31600 10800 0 0 $X=31260 $Y=10520
X358 398 340 335 rSrcIn<8> VDD VSS RWL_N<8> AO22X1TR $T=38400 25200 0 180 $X=35260 $Y=21200
X359 400 340 335 rSrcIn<9> VDD VSS RWL_N<9> AO22X1TR $T=36800 32400 1 0 $X=36460 $Y=28400
X360 403 340 335 rSrcIn<5> VDD VSS RWL_N<5> AO22X1TR $T=40000 10800 0 0 $X=39660 $Y=10520
X361 399 340 335 rSrcIn<4> VDD VSS RWL_N<4> AO22X1TR $T=46800 18000 1 0 $X=46460 $Y=14000
X362 350 VSS 348 366 329 VDD 559 OAI22X1TR $T=16800 39600 1 180 $X=14060 $Y=39320
X363 322 VSS 348 366 321 VDD 558 OAI22X1TR $T=16800 54000 0 180 $X=14060 $Y=50000
X364 321 VSS 348 325 366 VDD 343 OAI22X1TR $T=14800 61200 1 0 $X=14460 $Y=57200
X365 329 VSS 348 366 347 VDD 352 OAI22X1TR $T=15600 46800 1 0 $X=15260 $Y=42800
X366 344 VSS 348 366 322 VDD 342 OAI22X1TR $T=19600 61200 0 180 $X=16860 $Y=57200
X367 347 VSS 348 366 341 VDD 561 OAI22X1TR $T=18800 39600 0 0 $X=18460 $Y=39320
X368 341 VSS 348 366 351 VDD 564 OAI22X1TR $T=21200 39600 0 0 $X=20860 $Y=39320
X369 363 VSS 348 366 344 VDD 563 OAI22X1TR $T=24400 46800 1 180 $X=21660 $Y=46520
X370 325 VSS 348 366 353 VDD 370 OAI22X1TR $T=24800 54000 1 0 $X=24460 $Y=50000
X371 351 VSS 348 366 371 VDD 376 OAI22X1TR $T=25600 39600 0 0 $X=25260 $Y=39320
X372 371 VSS 348 366 387 VDD 377 OAI22X1TR $T=26000 46800 1 0 $X=25660 $Y=42800
X373 364 VSS 348 366 363 VDD 569 OAI22X1TR $T=29600 54000 0 180 $X=26860 $Y=50000
X374 385 VSS 348 366 570 VDD 380 OAI22X1TR $T=30400 61200 0 180 $X=27660 $Y=57200
X375 570 VSS 348 366 364 VDD 375 OAI22X1TR $T=29600 54000 1 0 $X=29260 $Y=50000
X376 387 VSS 348 366 385 VDD 394 OAI22X1TR $T=30400 61200 1 0 $X=30060 $Y=57200
X377 354 VDD 365 369 VSS 354 373 567 705 318 ICV_76 $T=22800 25200 0 0 $X=22460 $Y=24920
X378 622 VDD 517 518 VSS 519 521 ReadA<12> 700 318 ICV_76 $T=79600 18000 1 0 $X=79260 $Y=14000
X379 531 VDD 530 ReadB<6> VSS 530 535 ReadB<5> 704 318 ICV_76 $T=86400 61200 1 0 $X=86060 $Y=57200
X380 529 VDD 533 ReadB<15> VSS 529 535 ReadB<13> 703 318 ICV_76 $T=87200 54000 1 0 $X=86860 $Y=50000
X381 531 VDD 539 ReadB<10> VSS 536 535 ReadB<1> 704 318 ICV_76 $T=89600 61200 1 0 $X=89260 $Y=57200
X382 539 VDD 535 ReadB<9> VSS 529 528 ReadB<12> 705 318 ICV_76 $T=100800 32400 1 0 $X=100460 $Y=28400
X383 367 VSS 355 VDD 565 566 NAND3X1TR $T=22800 39600 1 0 $X=22460 $Y=35600
X384 Dxac<19> VSS 424 VDD Dxac<18> 582 NAND3X1TR $T=48800 25200 1 180 $X=46460 $Y=24920
X385 Dxac<26> VSS 421 VDD 437 432 NAND3X1TR $T=47600 39600 0 0 $X=47260 $Y=39320
X386 479 VSS 471 VDD imm8_disp<7> 477 NAND3X1TR $T=69600 10800 1 180 $X=67260 $Y=10520
X387 326 353 VDD VSS 565 OR2XLTR $T=25600 39600 1 180 $X=23260 $Y=39320
X388 VSS 360 Dxac<4> 571 382 VDD 384 OAI31X1TR $T=28000 39600 0 0 $X=27660 $Y=39320
X389 VSS Dxac<5> Dxac<6> 391 374 VDD 396 OAI31X1TR $T=30400 39600 0 0 $X=30060 $Y=39320
X390 VSS 590 437 433 335 VDD 451 OAI31X1TR $T=52800 39600 1 0 $X=52460 $Y=35600
X391 VSS 501 505 497 600 VDD 476 OAI31X1TR $T=74400 54000 0 180 $X=71660 $Y=50000
X392 VSS 621 492 498 603 VDD AluSEL<0> OAI31X1TR $T=74000 18000 1 0 $X=73660 $Y=14000
X393 VSS 525 507 514 505 VDD 524 OAI31X1TR $T=90800 39600 1 180 $X=88060 $Y=39320
X394 383 360 Dxac<1> VDD Dxac<5> 390 VSS 389 392 AOI33X1TR $T=28400 46800 1 0 $X=28060 $Y=42800
X395 Dxac<22> 584 Dxac<20> VDD 586 424 VSS 443 Dxac<18> AOI33X1TR $T=49600 32400 1 0 $X=49260 $Y=28400
X396 Dxac<7> Dxac<6> Dxac<4> VSS 382 VDD OAI2BB1X1TR $T=35200 39600 0 180 $X=32460 $Y=35600
X397 488 DIn<11> 441 VSS DOut<11> VDD OAI2BB1X1TR $T=100800 32400 0 0 $X=100460 $Y=32120
X398 488 DIn<10> 447 VSS DOut<10> VDD OAI2BB1X1TR $T=104400 25200 0 0 $X=104060 $Y=24920
X399 488 DIn<12> 438 VSS DOut<12> VDD OAI2BB1X1TR $T=105200 39600 0 0 $X=104860 $Y=39320
X400 488 DIn<15> 452 VSS DOut<15> VDD OAI2BB1X1TR $T=105200 46800 0 0 $X=104860 $Y=46520
X401 488 DIn<13> 436 VSS DOut<13> VDD OAI2BB1X1TR $T=105600 46800 1 0 $X=105260 $Y=42800
X402 488 DIn<14> 454 VSS DOut<14> VDD OAI2BB1X1TR $T=105600 54000 1 0 $X=105260 $Y=50000
X403 VSS 323 326 REN_SRAM VDD NAND2X2TR $T=33600 32400 0 0 $X=33260 $Y=32120
X404 404 Dxac<11> 402 VSS 391 Dxac<7> 390 VDD 401 OAI33XLTR $T=38000 39600 1 180 $X=34460 $Y=39320
X405 Dxac<14> 397 Dxac<15> 408 VSS Dxac<12> VDD NOR4BBX1TR $T=36800 32400 0 0 $X=36460 $Y=32120
X406 Dxac<7> 392 Dxac<6> VDD 407 VSS 409 AOI31X1TR $T=36800 39600 1 0 $X=36460 $Y=35600
X407 Dxac<9> 404 407 VDD 574 VSS 412 AOI31X1TR $T=41600 46800 1 180 $X=38860 $Y=46520
X408 407 578 404 VDD 396 VSS 576 AOI31X1TR $T=44000 46800 0 180 $X=41260 $Y=42800
X409 418 402 Dxac<8> VDD Dxac<16> VSS 423 AOI31X1TR $T=46400 39600 0 180 $X=43660 $Y=35600
X410 491 487 490 VDD 601 VSS AluSEL<1> AOI31X1TR $T=71200 18000 0 0 $X=70860 $Y=17720
X411 Dxac<11> 407 Dxac<10> VDD 384 410 397 VSS AOI32X1TR $T=38000 39600 0 0 $X=37660 $Y=39320
X412 437 426 583 VDD 429 431 420 VSS AOI32X1TR $T=50400 46800 0 180 $X=47260 $Y=42800
X413 Dxac<25> 426 437 VDD 429 440 422 VSS AOI32X1TR $T=50400 46800 1 0 $X=50060 $Y=42800
X414 623 520 473 VDD 516 515 606 VSS AOI32X1TR $T=81200 39600 1 180 $X=78060 $Y=39320
X415 386 383 VDD 401 408 VSS 413 AOI211X1TR $T=39200 46800 1 0 $X=38860 $Y=42800
X416 425 429 VDD 435 419 VSS 439 AOI211X1TR $T=49600 39600 0 0 $X=49260 $Y=39320
X417 434 415 VDD 445 591 VSS 449 AOI211X1TR $T=57600 54000 0 180 $X=54860 $Y=50000
X418 434 446 VDD 445 448 VSS 463 AOI211X1TR $T=59200 46800 1 0 $X=58860 $Y=42800
X419 Dxac<14> Dxac<12> VSS Dxac<8> VDD 417 NAND3BX1TR $T=39600 39600 1 0 $X=39260 $Y=35600
X420 Dxac<31> Dxac<30> VSS 453 VDD 444 NAND3BX1TR $T=58800 46800 0 180 $X=56060 $Y=42800
X421 417 408 VDD 418 VSS NOR2BX1TR $T=42000 39600 1 0 $X=41660 $Y=35600
X422 479 471 VDD 459 VSS NOR2BX1TR $T=65600 25200 1 180 $X=63260 $Y=24920
X423 476 472 VDD Br VSS NOR2BX1TR $T=65600 39600 0 180 $X=63260 $Y=35600
X424 ReadB<15> 484 VDD 611 VSS NOR2BX1TR $T=89600 39600 0 180 $X=87260 $Y=35600
X425 ReadB<10> 484 VDD 532 VSS NOR2BX1TR $T=91600 39600 0 180 $X=89260 $Y=35600
X426 ReadB<6> 484 VDD 612 VSS NOR2BX1TR $T=90400 46800 0 0 $X=90060 $Y=46520
X427 ReadB<5> 484 VDD 537 VSS NOR2BX1TR $T=90800 54000 0 0 $X=90460 $Y=53720
X428 ReadB<13> 484 VDD 534 VSS NOR2BX1TR $T=93600 39600 0 180 $X=91260 $Y=35600
X429 ReadB<2> 484 VDD 613 VSS NOR2BX1TR $T=92400 54000 1 0 $X=92060 $Y=50000
X430 ReadB<1> 484 VDD 624 VSS NOR2BX1TR $T=92800 54000 0 0 $X=92460 $Y=53720
X431 ReadB<3> 484 VDD 545 VSS NOR2BX1TR $T=94000 46800 0 0 $X=93660 $Y=46520
X432 ReadB<7> 484 VDD 546 VSS NOR2BX1TR $T=94400 54000 1 0 $X=94060 $Y=50000
X433 ReadB<14> 484 VDD 541 VSS NOR2BX1TR $T=96800 32400 1 180 $X=94460 $Y=32120
X434 ReadB<8> 484 VDD 625 VSS NOR2BX1TR $T=95200 39600 0 0 $X=94860 $Y=39320
X435 ReadB<9> 484 VDD 542 VSS NOR2BX1TR $T=97600 39600 0 180 $X=95260 $Y=35600
X436 ReadB<4> 484 VDD 626 VSS NOR2BX1TR $T=96000 46800 0 0 $X=95660 $Y=46520
X437 ReadB<0> 484 VDD 548 VSS NOR2BX1TR $T=96400 54000 1 0 $X=96060 $Y=50000
X438 ReadB<11> 484 VDD 544 VSS NOR2BX1TR $T=99600 32400 1 180 $X=97260 $Y=32120
X439 ReadB<12> 484 VDD 614 VSS NOR2BX1TR $T=97600 39600 1 0 $X=97260 $Y=35600
X440 VSS Dxac<18> Dxac<20> 414 416 422 VDD 620 OAI32X1TR $T=45600 32400 1 180 $X=42460 $Y=32120
X441 VSS Dxac<18> Dxac<20> Dxac<17> Dxac<21> 420 VDD 416 OAI32X1TR $T=46000 32400 0 180 $X=42860 $Y=28400
X442 VSS 426 580 421 Dxac<16> 419 VDD 410 OAI32X1TR $T=46000 39600 1 180 $X=42860 $Y=39320
X443 VSS 510 520 501 503 495 VDD 515 OAI32X1TR $T=81200 46800 1 0 $X=80860 $Y=42800
X444 VSS 584 VDD 427 424 590 588 NOR4XLTR $T=51200 32400 0 0 $X=50860 $Y=32120
X445 340 323 VSS VDD CLKINVX4TR $T=51600 18000 0 0 $X=51260 $Y=17720
X446 Dxac<24> 434 VDD Dxac<28> VSS 437 NOR3BX1TR $T=54000 46800 1 180 $X=51260 $Y=46520
X447 instruIn<5> 337 imm8_disp<5> VDD VSS 458 DFFX1TR $T=53200 18000 1 0 $X=52860 $Y=14000
X448 instruIn<4> 337 imm8_disp<4> VDD VSS 461 DFFX1TR $T=53600 18000 0 0 $X=53260 $Y=17720
X449 instruIn<6> 337 imm8_disp<6> VDD VSS 471 DFFX1TR $T=56800 10800 0 0 $X=56460 $Y=10520
X450 instruIn<7> 337 imm8_disp<7> VDD VSS 464 DFFX1TR $T=57200 10800 1 0 $X=56860 $Y=6800
X451 Dxac<24> Dxac<16> Dxac<28> VSS VDD 453 AND3X2TR $T=54800 46800 0 0 $X=54460 $Y=46520
X452 455 593 594 VSS VDD 462 AND3X2TR $T=58800 39600 1 0 $X=58460 $Y=35600
X453 598 486 487 VSS VDD 597 AND3X2TR $T=70000 39600 0 180 $X=67260 $Y=35600
X454 imm8_disp<5> VSS imm8_disp<6> 450 VDD 367 NAND3XLTR $T=58000 25200 0 180 $X=55660 $Y=21200
X455 476 VSS imm8_disp<7> 479 VDD 482 NAND3XLTR $T=72400 10800 1 180 $X=70060 $Y=10520
X456 491 VSS 492 481 VDD 603 NAND3XLTR $T=73200 25200 1 0 $X=72860 $Y=21200
X457 clk VSS 469 VDD CLKBUFX8TR $T=57200 54000 0 0 $X=56860 $Y=53720
X458 335 VSS 453 Dxac<30> 593 Dxac<31> VDD NAND4X1TR $T=57600 39600 0 0 $X=57260 $Y=39320
X459 592 VSS VDD 340 CLKBUFX2TR $T=60000 25200 1 180 $X=58060 $Y=24920
X460 471 456 464 VDD VSS 326 OR3X2TR $T=63600 18000 0 180 $X=60460 $Y=14000
X461 465 458 470 472 484 VSS VDD OAI211X4TR $T=62400 32400 0 0 $X=62060 $Y=32120
X462 456 imm8_disp<7> 471 VDD VSS 474 OR3X1TR $T=66800 10800 1 180 $X=64060 $Y=10520
X463 596 VDD 456 Stall VSS NOR2XLTR $T=66400 18000 0 180 $X=64460 $Y=14000
X464 Amount<1> VDD imm8_disp<6> Amount<0> VSS NOR2XLTR $T=104800 18000 0 0 $X=104460 $Y=17720
X465 597 480 VDD 499 VSS NOR2BXLTR $T=68000 32400 0 0 $X=67660 $Y=32120
X466 458 596 487 VSS VDD 621 OA21XLTR $T=69600 18000 1 0 $X=69260 $Y=14000
X467 pc<4> 483 VSS VDD 622 AND2XLTR $T=77600 18000 1 0 $X=77260 $Y=14000
X468 464 VSS 502 478 Amount<1> 480 VDD NAND4XLTR $T=78800 25200 0 0 $X=78460 $Y=24920
X469 480 502 LuiSEL VSS VDD DSEL<0> AO21XLTR $T=80400 32400 1 0 $X=80060 $Y=28400
.ENDS
***************************************
.SUBCKT ICV_77 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 52 53 54 55 56 57 58 59 60 61
+ 62 63 64 65 68 69 70 73 74 75 76 77 78 80 81 82 83 85 86 87
+ 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104 105 106 107
+ 108 109 110 111 112 113 114 115 116 117 118 119 120 122 124 125 126 127 128 129
+ 130 134 135 136 137 138 139 140 141 142 143 145 146 147 148 149 150 151 152 153
+ 154 155 157 158 159 160 161 162 163 164 165 166 167 168 170 171 172 173 174 175
+ 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 195
+ 196 197 198 199 200 201 202 203 204 205 206 207 208 209 210 211 212 213 214 215
+ 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 236
+ 237 238 239 240 241 242 243 244 245 246 247 248 249 250 251 252 253 255 256 257
+ 258 259 260 261 262 263 264 265 266 267 268 269 270 271 272 273 274 275 276 277
+ 278 284 285 286 287 288 289 290 291 292 293 294 295 296 297 298 299 300 301 302
+ 303 304 305 306 307 308 309 310 311 312 313 314 315 316 317 318 319 320 321 322
+ 323 324 325 326 327 328 329 330 331 332 333 334 335 336 337 338 339
** N=404 EP=317 IP=369 FDC=0
X0 175 172 173 171 234 234 284 285 286 288 292 237 343 158 290 291 289 342 287 341
+ 345 338 347 348 344 336 350 351 352 183 337 186 349 339 6 9 165 8 7 177
+ 340 346 182 179 180 184 181 185 186 186 234
+ pc $T=-8600 -65500 0 0 $X=-8600 $Y=-65500
X1 234 234 307 32 35 293 294 295 47 296 297 57 77 239 305 238 103 106 108 111
+ 240 116 126 129 125 304 299 69 73 333 88 115 303 36 37 130 112 109 81 127
+ 128 34 104 302 48 306 178 76 58 160 105 56 298 107 118 124 300 162 117 33
+ 7 301 110 8 166 165 241 9 244 170 54 97 172 85 44 16 96 98 95 173
+ 163 94 45 154 334 149 38 62 247 61 137 15 20 243 139 99 41 140 176 39
+ 18 19 100 150 13 249 142 261 40 11 135 12 10 250 185 2 158 1 255 181
+ 184 352 3 219 217 186 224 317 260 340 323 262 264 223 310 266 322 24 344 267
+ 326 313 180 277 21 230 196 198 221 179 278 346 201 233 324 325 202 205 225 347
+ 183 206 227 212 78 348 228 309 182 210 208 311 189 209 213 226 231 320 65 22
+ 91 335 218 25 321 215 214 327 27 329 89 155 220 83 316 328 236 315 276 319
+ 272 26 273 59 318 222 275 331 216 274 271 232 29 90 229 312 42 314 31 157
+ 330 195 28 30 332 159 161 119 122 120 6 102 151 101 114 113 164 168 55 75
+ 152 68 64 171 245 70 49 74 153 87 63 174 80 136 50 43 175 138 53 82
+ 60 52 46 92 167 86 246 248 141 17 265 177 14 93 143 145 252 251 269 134
+ 253 146 270 242 23 147 4 5 256 257 351 258 259 350 148 341 263 342 187 343
+ 188 190 191 192 268 193 194 345 197 199 200 203 204 308 207 349 211 234
+ controllerLING $T=-8600 -14100 0 0 $X=-8600 $Y=-14100
.ENDS
***************************************
.SUBCKT ICV_75
** N=51 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_74
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_73
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_72
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_71
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_70
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_69
** N=1 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_46
** N=544 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_45
** N=363 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_44
** N=583 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_43
** N=4226 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_42
** N=4223 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_41
** N=4483 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_40
** N=4223 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_39
** N=2364 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT RA1SH16x512MUX_CD_EVEN
** N=85 EP=0 IP=0 FDC=0
*.CALIBRE ISOLATED NETS: VSS VDD BL0_2 DRSA_2 BL0__2 DWSA_2 BL1__2 BL1_2 BL1_1 BL1__1 STUBDW_0 STUBDR_0 BL0__1 BL0_1 BL0_0 STUBDR__0 BL0__0 STUBDW__0 BL1__0 BL1_0
+ BL1 BL1_ DWSA DRSA BL0_ BL0 A0_ A0 YP1_3 YP1_2 YP1_1 YP1_0 YP0_3 YP0_2 YP0_1 YP0_0 GTP STUBDW STUBDR_ STUBDR
+ STUBDW_
.ENDS
***************************************
.SUBCKT RA1SH16x512MUX_CD_ODD
** N=85 EP=0 IP=0 FDC=0
*.CALIBRE ISOLATED NETS: VSS VDD BL0_2 DRSA_2 BL0__2 DWSA_2 BL1__2 BL1_2 BL1_1 BL1__1 STUBDW STUBDR BL0__1 BL0_1 BL0_0 STUBDR_ BL0__0 STUBDW_ BL1__0 BL1_0
+ BL1 BL1_ DWSA DRSA BL0_ BL0 A0_ STUBDW_0 STUBDR__0 STUBDR_0 STUBDW__0 A0 YP1_3 YP1_2 YP1_1 YP1_0 YP0_3 YP0_2 YP0_1 YP0_0
+ GTP
.ENDS
***************************************
.SUBCKT RA1SH16x512HXP38X
** N=77 EP=0 IP=0 FDC=0
*.CALIBRE ISOLATED NETS: VSS A2 VDD A1 A0 XP_7 XP_6 XP_5 XP_4 XP_3 XP_2 XP_1 XP_0 BWEN WEI OEI_ AGTPB AY0 AY0_ YP1_3
+ YP1_2 YP1_1 YP1_0 YP0_3 YP0_2 YP0_1 YP0_0 AGTPT
.ENDS
***************************************
.SUBCKT RA1SH16x512 VDD VSS D<0> Q<0> Q<1> D<1> D<2> Q<2> Q<3> D<3> D<4> Q<4> Q<5> D<5> D<6> Q<6> Q<7> D<7> A<2> A<1>
+ A<0> CEN WEN CLK D<8> Q<8> Q<9> D<9> D<10> Q<10> Q<11> D<11> D<12> Q<12> Q<13> D<13> D<14> Q<14> Q<15> D<15>
+ A<8> A<7> A<6> A<5> A<4> A<3>
** N=1361 EP=46 IP=4477 FDC=0
.ENDS
***************************************
.SUBCKT ICV_47 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45
** N=46 EP=45 IP=47 FDC=0
X0 3 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 26 27
+ 28 29 30 31 1 32 33 2 34 35 36 37 38 39 40 41 42 43 44 45
+ 20 21 22 23 24 25
+ RA1SH16x512 $T=0 0 0 0 $X=-2 $Y=-2
.ENDS
***************************************
.SUBCKT ICV_29 1
** N=10 EP=1 IP=10 FDC=0
X0 2 3 4 5 6 7 8 9 10 PICS $T=0 0 0 0 $X=-2 $Y=0
.ENDS
***************************************
.SUBCKT MUX_D_buf SEL_OUT SEL_IN VDD! VSS!
** N=29 EP=4 IP=0 FDC=6
M0 SEL_OUT 5 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=450 $D=97
M1 VSS! 5 SEL_OUT VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=450 $D=97
M2 5 SEL_IN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1390 $Y=450 $D=97
M3 SEL_OUT 5 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=1650 $D=189
M4 VDD! 5 SEL_OUT VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=1650 $D=189
M5 5 SEL_IN VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1390 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT MUX_D_1bit SEL<1> D ALU Shifter SEL<0> DMEM VSS! VDD!
** N=111 EP=8 IP=0 FDC=22
M0 VSS! SEL<1> 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=450 $D=97
M1 D 17 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=5210 $D=97
M2 15 SEL<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=450 $D=97
M3 VSS! 17 D VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=5210 $D=97
M4 VSS! ALU 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=450 $D=97
M5 17 SEL<1> 13 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=5070 $D=97
M6 18 Shifter VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=450 $D=97
M7 14 12 17 VSS! nfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2780 $Y=5230 $D=97
M8 14 SEL<0> 18 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=450 $D=97
M9 16 15 14 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=5270 $D=97
M10 VSS! DMEM 16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=450 $D=97
M11 VDD! SEL<1> 12 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=1950 $D=189
M12 D 17 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=3510 $D=189
M13 15 SEL<0> VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=1950 $D=189
M14 VDD! 17 D VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=3510 $D=189
M15 VDD! ALU 13 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=1790 $D=189
M16 17 12 13 VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=3510 $D=189
M17 18 Shifter VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=1790 $D=189
M18 14 SEL<1> 17 VDD! pfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2880 $Y=3510 $D=189
M19 14 15 18 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=2210 $D=189
M20 16 SEL<0> 14 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=3510 $D=189
M21 VDD! DMEM 16 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=1790 $D=189
.ENDS
***************************************
.SUBCKT ICV_66 1 2 3 4 5 6 7 8 9 10 11 12
** N=15 EP=12 IP=22 FDC=44
X0 1 2 3 4 5 6 12 11 MUX_D_1bit $T=0 0 0 0 $X=-430 $Y=-240
X1 1 7 8 9 5 10 12 11 MUX_D_1bit $T=4800 0 0 0 $X=4370 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_67 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
** N=23 EP=20 IP=30 FDC=88
X0 1 2 3 4 18 5 6 7 8 9 19 20 ICV_66 $T=0 0 0 0 $X=-430 $Y=-240
X1 1 10 11 12 18 13 14 15 16 17 19 20 ICV_66 $T=9600 0 0 0 $X=9170 $Y=-240
.ENDS
***************************************
.SUBCKT MUX_D OUT<4> ALU<4> SH<4> DMEM<4> OUT<15> ALU<15> SH<15> DMEM<15> OUT<13> ALU<13> SH<13> DMEM<13> OUT<14> ALU<14> SH<14> DMEM<14> OUT<0> ALU<0> SH<0> DMEM<0>
+ OUT<1> ALU<1> SH<1> DMEM<1> OUT<2> ALU<2> SH<2> DMEM<2> OUT<3> ALU<3> SH<3> DMEM<3> OUT<5> ALU<5> SH<5> DMEM<5> OUT<6> ALU<6> SH<6> DMEM<6>
+ OUT<7> ALU<7> SH<7> DMEM<7> OUT<8> ALU<8> SH<8> DMEM<8> OUT<9> ALU<9> SH<9> DMEM<9> OUT<10> ALU<10> SH<10> DMEM<10> OUT<11> ALU<11> SH<11> DMEM<11>
+ OUT<12> ALU<12> SH<12> DMEM<12> SEL<1> SEL<0> VSS! VDD!
** N=266 EP=68 IP=114 FDC=364
X0 103 SEL<1> VDD! VSS! MUX_D_buf $T=0 26000 0 270 $X=-520 $Y=23870
X1 104 SEL<0> VDD! VSS! MUX_D_buf $T=6000 26000 1 270 $X=2510 $Y=23870
X2 103 OUT<4> ALU<4> SH<4> 104 DMEM<4> VSS! VDD! MUX_D_1bit $T=6000 19200 0 90 $X=-240 $Y=18770
X3 103 OUT<15> ALU<15> SH<15> 104 DMEM<15> VSS! VDD! MUX_D_1bit $T=6000 99600 0 90 $X=-240 $Y=99170
X4 103 OUT<13> ALU<13> SH<13> 104 DMEM<13> OUT<14> ALU<14> SH<14> DMEM<14> VDD! VSS! ICV_66 $T=6000 90000 0 90 $X=-240 $Y=89570
X5 103 OUT<0> ALU<0> SH<0> DMEM<0> OUT<1> ALU<1> SH<1> DMEM<1> OUT<2> ALU<2> SH<2> DMEM<2> OUT<3> ALU<3> SH<3> DMEM<3> 104 VDD! VSS! ICV_67 $T=6000 0 0 90 $X=-240 $Y=-430
X6 103 OUT<5> ALU<5> SH<5> DMEM<5> OUT<6> ALU<6> SH<6> DMEM<6> OUT<7> ALU<7> SH<7> DMEM<7> OUT<8> ALU<8> SH<8> DMEM<8> 104 VDD! VSS! ICV_67 $T=6000 51600 0 90 $X=-240 $Y=51170
X7 103 OUT<9> ALU<9> SH<9> DMEM<9> OUT<10> ALU<10> SH<10> DMEM<10> OUT<11> ALU<11> SH<11> DMEM<11> OUT<12> ALU<12> SH<12> DMEM<12> 104 VDD! VSS! ICV_67 $T=6000 70800 0 90 $X=-240 $Y=70370
.ENDS
***************************************
.SUBCKT MUX_Shifter_SEL_BUF SEL_OUT SEL_IN VDD! VSS! 5
** N=25 EP=5 IP=0 FDC=4
M0 VSS! 6 SEL_OUT 5 nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=450 $D=97
M1 6 SEL_IN VSS! 5 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=450 $D=97
M2 VDD! 6 SEL_OUT 5 pfet L=1.2e-07 W=7.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=1750 $D=189
M3 6 SEL_IN VDD! 5 pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT MUX_Shifter_SEL_1bit SEL<1> ALU_B Imm_8 SEL<0> Rsrc VSS! EIGHT VDD! 9
** N=105 EP=9 IP=0 FDC=22
M0 VSS! SEL<1> 10 9 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=450 $D=97
M1 ALU_B 15 VSS! 9 nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=5210 $D=97
M2 13 SEL<0> VSS! 9 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=450 $D=97
M3 VSS! 15 ALU_B 9 nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=5210 $D=97
M4 9 EIGHT 11 9 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=450 $D=97
M5 15 SEL<1> 11 9 nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=5070 $D=97
M6 16 Imm_8 9 9 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=450 $D=97
M7 12 10 15 9 nfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2780 $Y=5230 $D=97
M8 12 SEL<0> 16 9 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=450 $D=97
M9 14 13 12 9 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=5270 $D=97
M10 VSS! Rsrc 14 9 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=450 $D=97
M11 VDD! SEL<1> 10 9 pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=1950 $D=189
M12 ALU_B 15 VDD! 9 pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=3510 $D=189
M13 13 SEL<0> VDD! 9 pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=1950 $D=189
M14 VDD! 15 ALU_B 9 pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=3510 $D=189
M15 9 EIGHT 11 9 pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=1950 $D=189
M16 15 10 11 9 pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=3510 $D=189
M17 16 Imm_8 9 9 pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=1950 $D=189
M18 12 SEL<1> 15 9 pfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2880 $Y=3510 $D=189
M19 12 13 16 9 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=2210 $D=189
M20 14 SEL<0> 12 9 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=3510 $D=189
M21 VDD! Rsrc 14 9 pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=1950 $D=189
.ENDS
***************************************
.SUBCKT Sign_Ext_left Imm_16 Imm_8 3
** N=21 EP=3 IP=0 FDC=4
M0 Imm_16 4 3 3 nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=1020 $D=97
M1 3 Imm_8 4 3 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=670 $Y=490 $D=97
M2 Imm_16 4 3 3 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=1020 $D=189
M3 3 Imm_8 4 3 pfet L=1.2e-07 W=4.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2040 $Y=490 $D=189
.ENDS
***************************************
.SUBCKT Sign_Ext_right IMM_16 SEL_BAR SEL 4
** N=48 EP=4 IP=0 FDC=10
M0 4 5 6 4 nfet L=1.2e-07 W=3.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=2210 $D=97
M1 IMM_16 6 4 4 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=670 $Y=2730 $D=97
M2 4 6 IMM_16 4 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=670 $Y=3330 $D=97
M3 5 SEL_BAR 4 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=720 $Y=570 $D=97
M4 4 SEL 5 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=720 $Y=1130 $D=97
M5 4 5 6 4 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=2210 $D=189
M6 IMM_16 6 4 4 pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2730 $D=189
M7 4 6 IMM_16 4 pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=3330 $D=189
M8 5 SEL 4 4 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=570 $D=189
M9 4 SEL_BAR 5 4 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1130 $D=189
.ENDS
***************************************
.SUBCKT BUFFER_SEL IN VSS! VDD! OUT
** N=85 EP=4 IP=0 FDC=14
M0 VSS! IN 5 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=730 $Y=450 $D=97
M1 6 5 VSS! VSS! nfet L=1.2e-07 W=6.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=450 $D=97
M2 7 6 VSS! VSS! nfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M3 VSS! 6 7 VSS! nfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M4 OUT 7 VSS! VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=450 $D=97
M5 VSS! 7 OUT VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=450 $D=97
M6 OUT 7 VSS! VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4110 $Y=450 $D=97
M7 VDD! IN 5 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=730 $Y=4150 $D=189
M8 6 5 VDD! VDD! pfet L=1.2e-07 W=1.19e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3500 $D=189
M9 7 6 VDD! VDD! pfet L=1.2e-07 W=1.3e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3390 $D=189
M10 VDD! 6 7 VDD! pfet L=1.2e-07 W=1.3e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3390 $D=189
M11 OUT 7 VDD! VDD! pfet L=1.2e-07 W=1.92e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=2770 $D=189
M12 VDD! 7 OUT VDD! pfet L=1.2e-07 W=1.92e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=2770 $D=189
M13 OUT 7 VDD! VDD! pfet L=1.2e-07 W=1.92e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4110 $Y=2770 $D=189
.ENDS
***************************************
.SUBCKT Z_cell4 VSS! P<1> P<3> P<0> P<2> VDD! OUT
** N=50 EP=7 IP=0 FDC=10
M0 9 P<0> VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=440 $D=97
M1 10 P<1> 9 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=920 $D=97
M2 11 P<2> 10 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=1400 $D=97
M3 8 P<3> 11 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=1880 $D=97
M4 VSS! 8 OUT VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5100 $Y=1070 $D=97
M5 8 P<0> VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=440 $D=189
M6 VDD! P<1> 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=920 $D=189
M7 8 P<2> VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1400 $D=189
M8 VDD! P<3> 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1880 $D=189
M9 VDD! 8 OUT VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3560 $Y=1070 $D=189
.ENDS
***************************************
.SUBCKT MUX41 SEL1 SEL0 OUT VSS! IN3 IN2 VDD! IN0 IN1
** N=127 EP=9 IP=0 FDC=28
M0 VSS! SEL1 12 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1090 $D=97
M1 13 SEL0 IN3 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2080 $D=97
M2 IN2 10 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2880 $D=97
M3 10 SEL0 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3880 $D=97
M4 OUT 11 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-1350 $D=97
M5 VSS! 11 OUT VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-870 $D=97
M6 OUT 11 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-390 $D=97
M7 VSS! 11 OUT VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=90 $D=97
M8 VSS! 14 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=-650 $D=97
M9 14 SEL1 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=940 $D=97
M10 15 12 14 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=2090 $D=97
M11 15 10 IN0 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=3340 $D=97
M12 IN1 SEL0 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=4140 $D=97
M13 OUT 11 VDD! VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-1350 $D=189
M14 VDD! 11 OUT VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-870 $D=189
M15 OUT 11 VDD! VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-390 $D=189
M16 VDD! 11 OUT VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=90 $D=189
M17 12 SEL1 VDD! VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=610 $D=189
M18 VDD! SEL1 12 VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1090 $D=189
M19 10 SEL0 VDD! VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=3880 $D=189
M20 VDD! SEL0 10 VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=4360 $D=189
M21 13 10 IN3 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=2080 $D=189
M22 IN2 SEL0 13 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=2880 $D=189
M23 VDD! 14 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=-650 $D=189
M24 14 12 13 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=940 $D=189
M25 15 SEL1 14 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2140 $D=189
M26 15 SEL0 IN0 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3340 $D=189
M27 IN1 10 15 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=4140 $D=189
.ENDS
***************************************
.SUBCKT SETUP_BIT A B XOR AND OR VSS! VDD!
** N=98 EP=7 IP=0 FDC=26
M0 13 A 11 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=5030 $D=97
M1 VSS! A 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=520 $Y=430 $D=97
M2 VSS! B 13 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=5030 $D=97
M3 8 B VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1000 $Y=430 $D=97
M4 AND 11 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=5290 $D=97
M5 14 8 OR VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=430 $D=97
M6 15 A 12 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=5030 $D=97
M7 VSS! 10 14 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=430 $D=97
M8 VSS! 8 15 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=5030 $D=97
M9 16 10 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=430 $D=97
M10 17 12 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=5030 $D=97
M11 9 B 16 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=430 $D=97
M12 XOR 9 17 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=5030 $D=97
M13 11 A VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=3510 $D=189
M14 VDD! A 10 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=520 $Y=1950 $D=189
M15 VDD! B 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=3510 $D=189
M16 8 B VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1000 $Y=1950 $D=189
M17 AND 11 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=3510 $D=189
M18 OR 8 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=1950 $D=189
M19 12 A VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=3510 $D=189
M20 VDD! 10 OR VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=1950 $D=189
M21 VDD! 8 12 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=3510 $D=189
M22 9 10 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=1950 $D=189
M23 XOR 12 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=3510 $D=189
M24 VDD! B 9 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=1950 $D=189
M25 VDD! 9 XOR VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT ICV_65 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
** N=18 EP=18 IP=32 FDC=108
X0 1 2 5 15 10 16 17 9 8 MUX41 $T=0 0 0 0 $X=-240 $Y=-2300
X1 1 2 6 15 14 18 17 13 12 MUX41 $T=6000 0 0 0 $X=5760 $Y=-2300
X2 7 3 8 9 10 15 17 SETUP_BIT $T=0 9600 0 270 $X=-240 $Y=4490
X3 11 4 12 13 14 15 17 SETUP_BIT $T=6000 9600 0 270 $X=5760 $Y=4490
.ENDS
***************************************
.SUBCKT MUX21_BAR SEL VSS! IN<1> VDD! OUT IN<0>
** N=41 EP=6 IP=0 FDC=8
M0 8 SEL IN<1> VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=600 $Y=5270 $D=97
M1 VSS! SEL 7 VSS! nfet L=1.2e-07 W=5.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=640 $Y=450 $D=97
M2 OUT 8 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1120 $Y=450 $D=97
M3 IN<0> 7 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1280 $Y=5270 $D=97
M4 8 7 IN<1> VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=600 $Y=3510 $D=189
M5 VDD! SEL 7 VDD! pfet L=1.2e-07 W=9.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=640 $Y=1580 $D=189
M6 OUT 8 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1120 $Y=1950 $D=189
M7 IN<0> SEL 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1280 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT MUX84 SEL VDD! VSS! S3<1> S<3> S3<0> S2<1> S<2> S2<0> S1<1> S<1> S1<0> S0<1> S<0> S0<0>
** N=15 EP=15 IP=24 FDC=32
X0 SEL VSS! S3<1> VDD! S<3> S3<0> MUX21_BAR $T=6000 2000 1 270 $X=-240 $Y=-200
X1 SEL VSS! S2<1> VDD! S<2> S2<0> MUX21_BAR $T=12000 2000 1 270 $X=5760 $Y=-200
X2 SEL VSS! S1<1> VDD! S<1> S1<0> MUX21_BAR $T=18000 2000 1 270 $X=11760 $Y=-200
X3 SEL VSS! S0<1> VDD! S<0> S0<0> MUX21_BAR $T=24000 2000 1 270 $X=17760 $Y=-200
.ENDS
***************************************
.SUBCKT RIPPLE_BIT_BAR B CIN A VSS! SUM VDD! COUT
** N=116 EP=7 IP=0 FDC=24
M0 11 9 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=450 $D=97
M1 VSS! B 13 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=5070 $D=97
M2 14 A VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1360 $Y=5190 $D=97
M3 10 11 CIN VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1920 $Y=450 $D=97
M4 9 B 14 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1980 $Y=5270 $D=97
M5 15 9 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2600 $Y=450 $D=97
M6 A 13 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2660 $Y=5270 $D=97
M7 VSS! CIN 15 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3220 $Y=450 $D=97
M8 SUM 10 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3700 $Y=450 $D=97
M9 12 11 B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3740 $Y=5270 $D=97
M10 CIN 9 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4420 $Y=5270 $D=97
M11 COUT 12 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4930 $Y=450 $D=97
M12 11 9 VDD! VDD! pfet L=1.2e-07 W=9.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=1540 $D=189
M13 VDD! B 13 VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=3510 $D=189
M14 14 A VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1360 $Y=3510 $D=189
M15 10 9 CIN VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1920 $Y=2210 $D=189
M16 9 13 14 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1980 $Y=3510 $D=189
M17 15 11 10 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2600 $Y=2210 $D=189
M18 A B 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2660 $Y=3510 $D=189
M19 VDD! CIN 15 VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3220 $Y=1630 $D=189
M20 SUM 10 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3700 $Y=1630 $D=189
M21 12 9 B VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3740 $Y=3510 $D=189
M22 CIN 11 12 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4420 $Y=3510 $D=189
M23 COUT 12 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4930 $Y=1630 $D=189
.ENDS
***************************************
.SUBCKT RIPPLE_BIT B CIN A SUM VSS! VDD! COUT
** N=125 EP=7 IP=0 FDC=26
M0 VSS! B 14 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=470 $Y=5070 $D=97
M1 11 9 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=450 $D=97
M2 15 A VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=950 $Y=5190 $D=97
M3 9 B 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=5270 $D=97
M4 10 11 CIN VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1650 $Y=450 $D=97
M5 A 14 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2250 $Y=5270 $D=97
M6 16 9 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2330 $Y=450 $D=97
M7 VSS! CIN 16 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2950 $Y=450 $D=97
M8 12 11 B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3330 $Y=5270 $D=97
M9 SUM 10 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3430 $Y=450 $D=97
M10 CIN 9 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4010 $Y=5270 $D=97
M11 COUT 13 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4850 $Y=450 $D=97
M12 13 12 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4990 $Y=5150 $D=97
M13 VDD! B 14 VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=470 $Y=3510 $D=189
M14 11 9 VDD! VDD! pfet L=1.2e-07 W=9.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=1540 $D=189
M15 15 A VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=950 $Y=3510 $D=189
M16 9 14 15 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=3510 $D=189
M17 10 9 CIN VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1650 $Y=2210 $D=189
M18 A B 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2250 $Y=3510 $D=189
M19 16 11 10 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2330 $Y=2210 $D=189
M20 VDD! CIN 16 VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2950 $Y=1630 $D=189
M21 12 9 B VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3330 $Y=3510 $D=189
M22 SUM 10 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3430 $Y=1630 $D=189
M23 CIN 11 12 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4010 $Y=3510 $D=189
M24 COUT 13 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4850 $Y=1630 $D=189
M25 13 12 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4990 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT RIPPLE_4BIT CIN B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! SUM<3> COUT SUM<2> SUM<1> SUM<0>
** N=21 EP=16 IP=32 FDC=102
X0 B<3> 19 A<3> VSS! SUM<3> VDD! COUT RIPPLE_BIT_BAR $T=0 0 0 0 $X=-200 $Y=-240
X1 B<2> 20 A<2> SUM<2> VSS! VDD! 19 RIPPLE_BIT $T=0 6000 0 0 $X=-200 $Y=5760
X2 B<1> 21 A<1> SUM<1> VSS! VDD! 20 RIPPLE_BIT $T=0 12000 0 0 $X=-200 $Y=11760
X3 B<0> CIN A<0> SUM<0> VSS! VDD! 21 RIPPLE_BIT $T=0 18000 0 0 $X=-200 $Y=17760
.ENDS
***************************************
.SUBCKT RIPPLE_4BIT_LAST_4BIT C3 CIN B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! SUM<3> COUT SUM<2> SUM<1> SUM<0>
** N=20 EP=17 IP=32 FDC=102
X0 B<3> C3 A<3> VSS! SUM<3> VDD! COUT RIPPLE_BIT_BAR $T=0 0 0 0 $X=-200 $Y=-240
X1 B<2> 19 A<2> SUM<2> VSS! VDD! C3 RIPPLE_BIT $T=0 6000 0 0 $X=-200 $Y=5760
X2 B<1> 20 A<1> SUM<1> VSS! VDD! 19 RIPPLE_BIT $T=0 12000 0 0 $X=-200 $Y=11760
X3 B<0> CIN A<0> SUM<0> VSS! VDD! 20 RIPPLE_BIT $T=0 18000 0 0 $X=-200 $Y=17760
.ENDS
***************************************
.SUBCKT ALU SEL<1> F N SEL<0> Z CIN B<0> B<1> B<2> B<3> B<4> B<5> B<6> B<7> B<8> B<9> COUT B<10> B<11> B<12>
+ B<13> B<14> B<15> A<0> A<1> A<2> A<3> A<4> A<5> A<6> A<7> A<8> A<9> A<10> A<11> A<12> A<13> A<14> A<15> OUT<0>
+ OUT<1> OUT<2> OUT<3> OUT<4> OUT<5> OUT<6> OUT<7> OUT<8> OUT<9> OUT<10> OUT<11> OUT<12> OUT<13> OUT<14> OUT<15> VDD! VSS!
** N=502 EP=57 IP=415 FDC=1972
M0 196 CIN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2740 $Y=14450 $D=97
M1 75 196 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2740 $Y=15410 $D=97
M2 205 107 198 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40190 $Y=14960 $D=97
M3 VSS! 75 205 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40190 $Y=15440 $D=97
M4 VSS! 198 Z VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40450 $Y=14010 $D=97
M5 VSS! COUT 127 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=64370 $Y=15090 $D=97
M6 206 131 130 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=14070 $D=97
M7 VSS! 128 206 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=14550 $D=97
M8 207 128 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=15030 $D=97
M9 131 127 207 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=15510 $D=97
M10 208 200 N VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=14070 $D=97
M11 VSS! 130 208 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=14550 $D=97
M12 209 127 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=15030 $D=97
M13 200 131 209 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=15510 $D=97
M14 210 147 145 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=14070 $D=97
M15 VSS! 146 210 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=14550 $D=97
M16 211 146 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=15030 $D=97
M17 147 COUT 211 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=15510 $D=97
M18 212 202 F VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=14070 $D=97
M19 VSS! 145 212 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=14550 $D=97
M20 213 COUT VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=15030 $D=97
M21 202 147 213 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=15510 $D=97
M22 VSS! 135 152 VSS! nfet L=1.2e-07 W=5.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=89650 $Y=14040 $D=97
M23 203 151 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=89650 $Y=14520 $D=97
M24 146 203 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=89650 $Y=15460 $D=97
M25 151 135 159 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=94470 $Y=14200 $D=97
M26 155 152 151 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=94470 $Y=14880 $D=97
M27 196 CIN VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1600 $Y=14450 $D=189
M28 75 196 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1600 $Y=15410 $D=189
M29 VDD! 198 Z VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38750 $Y=14010 $D=189
M30 198 107 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38750 $Y=14960 $D=189
M31 VDD! 75 198 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38750 $Y=15440 $D=189
M32 VDD! COUT 127 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=62750 $Y=15090 $D=189
M33 130 131 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=14070 $D=189
M34 VDD! 128 130 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=14550 $D=189
M35 131 128 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=15030 $D=189
M36 VDD! 127 131 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=15510 $D=189
M37 N 200 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=14070 $D=189
M38 VDD! 130 N VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=14550 $D=189
M39 200 127 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=15030 $D=189
M40 VDD! 131 200 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=15510 $D=189
M41 145 147 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=14070 $D=189
M42 VDD! 146 145 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=14550 $D=189
M43 147 146 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=15030 $D=189
M44 VDD! COUT 147 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=15510 $D=189
M45 F 202 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=14070 $D=189
M46 VDD! 145 F VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=14550 $D=189
M47 202 COUT VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=15030 $D=189
M48 VDD! 147 202 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=15510 $D=189
M49 VDD! 135 152 VDD! pfet L=1.2e-07 W=9.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=90780 $Y=14040 $D=189
M50 203 151 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=91150 $Y=14520 $D=189
M51 146 203 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=91150 $Y=15460 $D=189
M52 151 152 159 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=92710 $Y=14200 $D=189
M53 155 135 151 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=92710 $Y=14880 $D=189
X54 SEL<1> VSS! VDD! 76 BUFFER_SEL $T=5200 4800 1 270 $X=-240 $Y=-100
X55 SEL<0> VSS! VDD! 77 BUFFER_SEL $T=5200 9600 1 270 $X=-240 $Y=4700
X56 VSS! 83 84 80 85 VDD! 79 Z_cell4 $T=11200 13600 1 180 $X=4930 $Y=13350
X57 VSS! 101 102 98 103 VDD! 97 Z_cell4 $T=35200 13600 1 180 $X=28930 $Y=13350
X58 VSS! 97 111 79 112 VDD! 107 Z_cell4 $T=47200 13600 1 180 $X=40930 $Y=13350
X59 VSS! 121 122 118 123 VDD! 112 Z_cell4 $T=59200 13600 1 180 $X=52930 $Y=13350
X60 VSS! 142 128 139 143 VDD! 111 Z_cell4 $T=83200 13600 1 180 $X=76930 $Y=13350
X61 76 77 B<0> B<1> OUT<0> OUT<1> A<0> 80 82 78 A<1> 83 88 86 VSS! 160 VDD! 162 ICV_65 $T=5200 2000 0 0 $X=4960 $Y=-300
X62 76 77 B<2> B<3> OUT<2> OUT<3> A<2> 85 91 89 A<3> 84 95 92 VSS! 164 VDD! 166 ICV_65 $T=17200 2000 0 0 $X=16960 $Y=-300
X63 76 77 B<4> B<5> OUT<4> OUT<5> A<4> 98 100 96 A<5> 101 106 104 VSS! 169 VDD! 171 ICV_65 $T=29200 2000 0 0 $X=28960 $Y=-300
X64 76 77 B<6> B<7> OUT<6> OUT<7> A<6> 103 110 108 A<7> 102 116 113 VSS! 173 VDD! 175 ICV_65 $T=41200 2000 0 0 $X=40960 $Y=-300
X65 76 77 B<8> B<9> OUT<8> OUT<9> A<8> 118 120 117 A<9> 121 126 124 VSS! 178 VDD! 180 ICV_65 $T=53200 2000 0 0 $X=52960 $Y=-300
X66 76 77 B<10> B<11> OUT<10> OUT<11> A<10> 123 133 129 A<11> 122 137 134 VSS! 182 VDD! 184 ICV_65 $T=65200 2000 0 0 $X=64960 $Y=-300
X67 76 77 B<12> B<13> OUT<12> OUT<13> A<12> 139 141 138 A<13> 142 149 144 VSS! 187 VDD! 189 ICV_65 $T=77200 2000 0 0 $X=76960 $Y=-300
X68 76 77 B<14> B<15> OUT<14> OUT<15> A<14> 143 154 150 A<15> 128 158 156 VSS! 191 VDD! 193 ICV_65 $T=89200 2000 0 0 $X=88960 $Y=-300
X69 75 VSS! 197 VDD! 93 168 MUX21_BAR $T=23200 16000 0 270 $X=22960 $Y=13800
X70 93 VSS! 199 VDD! 114 177 MUX21_BAR $T=47200 16000 0 270 $X=46960 $Y=13800
X71 114 VSS! 201 VDD! 135 186 MUX21_BAR $T=71200 16000 0 270 $X=70960 $Y=13800
X72 135 VSS! 204 VDD! COUT 195 MUX21_BAR $T=95200 16000 0 270 $X=94960 $Y=13800
X73 75 VDD! VSS! 167 166 94 165 164 90 163 162 87 161 160 81 MUX84 $T=29200 11600 1 180 $X=4960 $Y=11400
X74 93 VDD! VSS! 176 175 115 174 173 109 172 171 105 170 169 99 MUX84 $T=53200 11600 1 180 $X=28960 $Y=11400
X75 114 VDD! VSS! 185 184 136 183 182 132 181 180 125 179 178 119 MUX84 $T=77200 11600 1 180 $X=52960 $Y=11400
X76 135 VDD! VSS! 194 193 157 192 191 153 190 189 148 188 187 140 MUX84 $T=101200 11600 1 180 $X=76960 $Y=11400
X77 VDD! B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! 167 197 165 163 161 RIPPLE_4BIT $T=29200 21600 1 270 $X=4960 $Y=15800
X78 VSS! B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! 94 168 90 87 81 RIPPLE_4BIT $T=29200 27200 1 270 $X=4960 $Y=21400
X79 VDD! B<7> A<7> B<6> A<6> B<5> A<5> B<4> A<4> VDD! VSS! 176 199 174 172 170 RIPPLE_4BIT $T=53200 21600 1 270 $X=28960 $Y=15800
X80 VSS! B<7> A<7> B<6> A<6> B<5> A<5> B<4> A<4> VDD! VSS! 115 177 109 105 99 RIPPLE_4BIT $T=53200 27200 1 270 $X=28960 $Y=21400
X81 VDD! B<11> A<11> B<10> A<10> B<9> A<9> B<8> A<8> VDD! VSS! 185 201 183 181 179 RIPPLE_4BIT $T=77200 21600 1 270 $X=52960 $Y=15800
X82 VSS! B<11> A<11> B<10> A<10> B<9> A<9> B<8> A<8> VDD! VSS! 136 186 132 125 119 RIPPLE_4BIT $T=77200 27200 1 270 $X=52960 $Y=21400
X83 159 VDD! B<15> A<15> B<14> A<14> B<13> A<13> B<12> A<12> VDD! VSS! 194 204 192 190 188 RIPPLE_4BIT_LAST_4BIT $T=101200 21600 1 270 $X=76960 $Y=15800
X84 155 VSS! B<15> A<15> B<14> A<14> B<13> A<13> B<12> A<12> VDD! VSS! 157 195 153 148 140 RIPPLE_4BIT_LAST_4BIT $T=101200 27200 1 270 $X=76960 $Y=21400
.ENDS
***************************************
.SUBCKT 3rd_bit SH<2> SH<2>_BAR D 4 5 6 7
** N=48 EP=7 IP=0 FDC=8
M0 4 8 D 4 nfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=360 $D=97
M1 5 SH<2> 8 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1320 $D=97
M2 4 8 D 4 nfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4920 $Y=360 $D=97
M3 7 SH<2>_BAR 8 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=1320 $D=97
M4 6 8 D 6 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=360 $D=189
M5 5 SH<2>_BAR 8 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1320 $D=189
M6 6 8 D 6 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=360 $D=189
M7 7 SH<2> 8 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1320 $D=189
.ENDS
***************************************
.SUBCKT 1st_bit SH<0> SH<0>_BAR 3 4 5 6 7
** N=30 EP=7 IP=0 FDC=4
M0 5 SH<0> 3 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=520 $D=97
M1 7 SH<0>_BAR 3 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=520 $D=97
M2 5 SH<0>_BAR 3 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=520 $D=189
M3 7 SH<0> 3 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
.ENDS
***************************************
.SUBCKT 2rd_bit SH<1> SH<1>_BAR 3 4 5 6 7
** N=28 EP=7 IP=0 FDC=4
M0 5 SH<1> 3 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=520 $D=97
M1 7 SH<1>_BAR 3 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4990 $Y=520 $D=97
M2 5 SH<1>_BAR 3 6 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1930 $Y=520 $D=189
M3 7 SH<1> 3 6 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
.ENDS
***************************************
.SUBCKT Data_Buffer E VSS! IN VDD!
** N=80 EP=4 IP=0 FDC=18
M0 6 IN VSS! VSS! nfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=1260 $D=97
M1 5 6 VSS! VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=2200 $D=97
M2 VSS! 6 5 VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=2680 $D=97
M3 E 5 VSS! VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=3160 $D=97
M4 VSS! 5 E VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=1260 $D=97
M5 E 5 VSS! VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=1740 $D=97
M6 VSS! 5 E VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=2220 $D=97
M7 E 5 VSS! VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=2700 $D=97
M8 VSS! 5 E VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=3180 $D=97
M9 6 IN VDD! VDD! pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1760 $Y=1260 $D=189
M10 E 5 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1790 $Y=3160 $D=189
M11 5 6 VDD! VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2200 $D=189
M12 VDD! 6 5 VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2680 $D=189
M13 VDD! 5 E VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1260 $D=189
M14 E 5 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1740 $D=189
M15 VDD! 5 E VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2220 $D=189
M16 E 5 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2700 $D=189
M17 VDD! 5 E VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3180 $D=189
.ENDS
***************************************
.SUBCKT 4th_bit SH3 SH3_BAR OUT VSS! IN1 VDD! IN2
** N=28 EP=7 IP=0 FDC=4
M0 OUT SH3 IN1 VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=520 $D=97
M1 OUT SH3_BAR IN2 VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5230 $Y=520 $D=97
M2 OUT SH3_BAR IN1 VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2150 $Y=520 $D=189
M3 OUT SH3 IN2 VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
.ENDS
***************************************
.SUBCKT ICV_63 1 2 3 4 5 6 7
** N=8 EP=7 IP=11 FDC=22
X0 3 4 8 5 Data_Buffer $T=0 0 0 0 $X=-240 $Y=-460
X1 1 2 8 4 6 5 7 4th_bit $T=0 -260 0 0 $X=-240 $Y=-460
.ENDS
***************************************
.SUBCKT ICV_64 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27
** N=27 EP=27 IP=56 FDC=76
X0 1 2 14 11 12 13 15 3rd_bit $T=0 -1460 1 0 $X=-250 $Y=-3860
X1 1 2 17 11 16 13 18 3rd_bit $T=6000 -1460 1 0 $X=5750 $Y=-3860
X2 3 4 20 11 19 13 21 1st_bit $T=0 -6660 1 0 $X=-240 $Y=-8460
X3 3 4 22 11 21 13 23 1st_bit $T=6000 -6660 1 0 $X=5760 $Y=-8460
X4 5 6 15 11 24 13 20 2rd_bit $T=0 -4660 1 0 $X=-240 $Y=-6600
X5 5 6 18 11 25 13 22 2rd_bit $T=6000 -4660 1 0 $X=5760 $Y=-6600
X6 7 8 9 11 13 26 14 ICV_63 $T=0 0 0 0 $X=-240 $Y=-460
X7 7 8 10 11 13 27 17 ICV_63 $T=6000 0 0 0 $X=5760 $Y=-460
.ENDS
***************************************
.SUBCKT SHIFTER A<0> A<1> A<2> A<3> A<4> A<5> A<6> A<7> A<8> A<9> A<10> A<11> A<12> A<13> A<14> SEL<3> SEL<2> SEL<1> SEL<0> A<15>
+ E<0> E<1> E<2> E<3> E<4> E<5> E<6> E<7> E<8> E<9> E<10> E<11> E<12> E<13> E<14> E<15> VDD! VSS! 39
** N=882 EP=39 IP=216 FDC=648
M0 77 74 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=2240 $D=97
M1 VSS! 74 77 VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=2720 $D=97
M2 71 75 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=3200 $D=97
M3 VSS! 75 71 VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=3680 $D=97
M4 78 72 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=7800 $D=97
M5 VSS! 72 78 VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=8280 $D=97
M6 76 73 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=8760 $D=97
M7 VSS! 73 76 VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=9240 $D=97
M8 74 113 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=1240 $D=97
M9 VSS! 113 74 VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=1720 $D=97
M10 75 114 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=4200 $D=97
M11 VSS! 114 75 VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=4680 $D=97
M12 72 115 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=6800 $D=97
M13 VSS! 115 72 VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=7280 $D=97
M14 73 116 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=9760 $D=97
M15 VSS! 116 73 VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=10240 $D=97
M16 VSS! SEL<3> 113 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4310 $Y=720 $D=97
M17 114 SEL<2> VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4310 $Y=5200 $D=97
M18 VSS! SEL<1> 115 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4310 $Y=6280 $D=97
M19 116 SEL<0> VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4310 $Y=10760 $D=97
M20 VDD! SEL<3> 113 39 pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=720 $D=189
M21 74 113 VDD! 39 pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=1240 $D=189
M22 VDD! 113 74 39 pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=1720 $D=189
M23 77 74 VDD! 39 pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=2240 $D=189
M24 VDD! 74 77 39 pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=2720 $D=189
M25 71 75 VDD! 39 pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=3200 $D=189
M26 VDD! 75 71 39 pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=3680 $D=189
M27 75 114 VDD! 39 pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=4200 $D=189
M28 VDD! 114 75 39 pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=4680 $D=189
M29 114 SEL<2> VDD! 39 pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=5200 $D=189
M30 VDD! SEL<1> 115 39 pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=6280 $D=189
M31 72 115 VDD! 39 pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=6800 $D=189
M32 VDD! 115 72 39 pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=7280 $D=189
M33 78 72 VDD! 39 pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=7800 $D=189
M34 VDD! 72 78 39 pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=8280 $D=189
M35 76 73 VDD! 39 pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=8760 $D=189
M36 VDD! 73 76 39 pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=9240 $D=189
M37 73 116 VDD! 39 pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=9760 $D=189
M38 VDD! 116 73 39 pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=10240 $D=189
M39 116 SEL<0> VDD! 39 pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=10760 $D=189
X40 75 71 73 76 72 78 74 77 E<0> E<1> VSS! VSS! VDD! 79 80 VSS! 82 83 VSS! 81
+ A<0> 84 A<1> VSS! VSS! VDD! VDD!
+ ICV_64 $T=5200 3740 1 0 $X=4950 $Y=-200
X41 75 71 73 76 72 78 74 77 E<2> E<3> VSS! VSS! VDD! 85 86 VSS! 88 89 A<1> 87
+ A<2> 90 A<3> 81 84 VDD! VDD!
+ ICV_64 $T=17200 3740 1 0 $X=16950 $Y=-200
X42 75 71 73 76 72 78 74 77 E<4> E<5> VSS! 80 VDD! 91 92 83 94 95 A<3> 93
+ A<4> 96 A<5> 87 90 VDD! VDD!
+ ICV_64 $T=29200 3740 1 0 $X=28950 $Y=-200
X43 75 71 73 76 72 78 74 77 E<6> E<7> VSS! 86 VDD! 97 98 89 100 101 A<5> 99
+ A<6> 102 A<7> 93 96 VDD! VDD!
+ ICV_64 $T=41200 3740 1 0 $X=40950 $Y=-200
X44 75 71 73 76 72 78 74 77 E<8> E<9> VSS! 92 VDD! 117 103 95 118 105 A<7> 104
+ A<8> 106 A<9> 99 102 79 82
+ ICV_64 $T=53200 3740 1 0 $X=52950 $Y=-200
X45 75 71 73 76 72 78 74 77 E<10> E<11> VSS! 98 VDD! 119 107 101 120 109 A<9> 108
+ A<10> 110 A<11> 104 106 85 88
+ ICV_64 $T=65200 3740 1 0 $X=64950 $Y=-200
X46 75 71 73 76 72 78 74 77 E<12> E<13> VSS! 103 VDD! 121 122 105 123 124 A<11> 111
+ A<12> 112 A<13> 108 110 91 94
+ ICV_64 $T=77200 3740 1 0 $X=76950 $Y=-200
X47 75 71 73 76 72 78 74 77 E<14> E<15> VSS! 107 VDD! 125 126 109 128 129 A<13> 127
+ A<14> 130 A<15> 111 112 97 100
+ ICV_64 $T=89200 3740 1 0 $X=88950 $Y=-200
.ENDS
***************************************
.SUBCKT RF_SCLK CLK VDD! WE<15> WE<14> WE<13> WE<12> WE<11> WE<10> WE<9> WE<8> WE<7> WE<6> WE<5> WE<4> WE<3> WE<2> WE<1> WE<0> WE_CLK_B<15> WE_CLK_B<14>
+ WE_CLK_B<13> WE_CLK_B<12> WE_CLK_B<11> WE_CLK_B<10> WE_CLK_B<9> WE_CLK_B<8> WE_CLK_B<7> WE_CLK_B<6> WE_CLK_B<5> WE_CLK_B<4> WE_CLK_B<3> WE_CLK_B<2> WE_CLK_B<1> WE_CLK_B<0> WE_CLK<15> WE_CLK<14> WE_CLK<13> WE_CLK<12> WE_CLK<11> WE_CLK<10>
+ WE_CLK<9> WE_CLK<8> WE_CLK<7> WE_CLK<6> WE_CLK<5> WE_CLK<4> WE_CLK<3> WE_CLK<2> WE_CLK<1> WE_CLK<0> VSS! 52
** N=1264 EP=52 IP=0 FDC=224
M0 WE_CLK_B<15> WE_CLK<15> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=470 $D=97
M1 VSS! WE_CLK<15> WE_CLK_B<15> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=950 $D=97
M2 WE_CLK_B<15> WE_CLK<15> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=1430 $D=97
M3 VSS! WE_CLK<15> WE_CLK_B<15> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=1910 $D=97
M4 WE_CLK<15> 53 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=2390 $D=97
M5 WE_CLK_B<14> WE_CLK<14> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=4290 $D=97
M6 VSS! WE_CLK<14> WE_CLK_B<14> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=4770 $D=97
M7 WE_CLK_B<14> WE_CLK<14> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=5250 $D=97
M8 VSS! WE_CLK<14> WE_CLK_B<14> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=5730 $D=97
M9 WE_CLK<14> 54 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=6210 $D=97
M10 WE_CLK_B<13> WE_CLK<13> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=8110 $D=97
M11 VSS! WE_CLK<13> WE_CLK_B<13> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=8590 $D=97
M12 WE_CLK_B<13> WE_CLK<13> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=9070 $D=97
M13 VSS! WE_CLK<13> WE_CLK_B<13> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=9550 $D=97
M14 WE_CLK<13> 55 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=10030 $D=97
M15 WE_CLK_B<12> WE_CLK<12> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=11930 $D=97
M16 VSS! WE_CLK<12> WE_CLK_B<12> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=12410 $D=97
M17 WE_CLK_B<12> WE_CLK<12> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=12890 $D=97
M18 VSS! WE_CLK<12> WE_CLK_B<12> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=13370 $D=97
M19 WE_CLK<12> 56 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=13850 $D=97
M20 WE_CLK_B<11> WE_CLK<11> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=15750 $D=97
M21 VSS! WE_CLK<11> WE_CLK_B<11> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=16230 $D=97
M22 WE_CLK_B<11> WE_CLK<11> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=16710 $D=97
M23 VSS! WE_CLK<11> WE_CLK_B<11> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=17190 $D=97
M24 WE_CLK<11> 57 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=17670 $D=97
M25 WE_CLK_B<10> WE_CLK<10> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=19570 $D=97
M26 VSS! WE_CLK<10> WE_CLK_B<10> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=20050 $D=97
M27 WE_CLK_B<10> WE_CLK<10> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=20530 $D=97
M28 VSS! WE_CLK<10> WE_CLK_B<10> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=21010 $D=97
M29 WE_CLK<10> 58 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=21490 $D=97
M30 WE_CLK_B<9> WE_CLK<9> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=23390 $D=97
M31 VSS! WE_CLK<9> WE_CLK_B<9> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=23870 $D=97
M32 WE_CLK_B<9> WE_CLK<9> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=24350 $D=97
M33 VSS! WE_CLK<9> WE_CLK_B<9> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=24830 $D=97
M34 WE_CLK<9> 59 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=25310 $D=97
M35 WE_CLK_B<8> WE_CLK<8> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=27210 $D=97
M36 VSS! WE_CLK<8> WE_CLK_B<8> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=27690 $D=97
M37 WE_CLK_B<8> WE_CLK<8> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=28170 $D=97
M38 VSS! WE_CLK<8> WE_CLK_B<8> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=28650 $D=97
M39 WE_CLK<8> 60 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=29130 $D=97
M40 WE_CLK_B<7> WE_CLK<7> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=31030 $D=97
M41 VSS! WE_CLK<7> WE_CLK_B<7> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=31510 $D=97
M42 WE_CLK_B<7> WE_CLK<7> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=31990 $D=97
M43 VSS! WE_CLK<7> WE_CLK_B<7> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=32470 $D=97
M44 WE_CLK<7> 61 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=32950 $D=97
M45 WE_CLK_B<6> WE_CLK<6> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=34850 $D=97
M46 VSS! WE_CLK<6> WE_CLK_B<6> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=35330 $D=97
M47 WE_CLK_B<6> WE_CLK<6> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=35810 $D=97
M48 VSS! WE_CLK<6> WE_CLK_B<6> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=36290 $D=97
M49 WE_CLK<6> 62 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=36770 $D=97
M50 WE_CLK_B<5> WE_CLK<5> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=38670 $D=97
M51 VSS! WE_CLK<5> WE_CLK_B<5> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=39150 $D=97
M52 WE_CLK_B<5> WE_CLK<5> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=39630 $D=97
M53 VSS! WE_CLK<5> WE_CLK_B<5> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=40110 $D=97
M54 WE_CLK<5> 63 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=40590 $D=97
M55 WE_CLK_B<4> WE_CLK<4> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=42490 $D=97
M56 VSS! WE_CLK<4> WE_CLK_B<4> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=42970 $D=97
M57 WE_CLK_B<4> WE_CLK<4> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=43450 $D=97
M58 VSS! WE_CLK<4> WE_CLK_B<4> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=43930 $D=97
M59 WE_CLK<4> 64 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=44410 $D=97
M60 WE_CLK_B<3> WE_CLK<3> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=46310 $D=97
M61 VSS! WE_CLK<3> WE_CLK_B<3> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=46790 $D=97
M62 WE_CLK_B<3> WE_CLK<3> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=47270 $D=97
M63 VSS! WE_CLK<3> WE_CLK_B<3> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=47750 $D=97
M64 WE_CLK<3> 65 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=48230 $D=97
M65 WE_CLK_B<2> WE_CLK<2> 52 VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=50130 $D=97
M66 VSS! WE_CLK<2> WE_CLK_B<2> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=50610 $D=97
M67 WE_CLK_B<2> WE_CLK<2> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=51090 $D=97
M68 VSS! WE_CLK<2> WE_CLK_B<2> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=51570 $D=97
M69 WE_CLK<2> 66 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=52050 $D=97
M70 WE_CLK_B<1> WE_CLK<1> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=53950 $D=97
M71 VSS! WE_CLK<1> WE_CLK_B<1> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=54430 $D=97
M72 WE_CLK_B<1> WE_CLK<1> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=54910 $D=97
M73 VSS! WE_CLK<1> WE_CLK_B<1> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=55390 $D=97
M74 WE_CLK<1> 67 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=55870 $D=97
M75 WE_CLK_B<0> WE_CLK<0> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=57770 $D=97
M76 VSS! WE_CLK<0> WE_CLK_B<0> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=58250 $D=97
M77 WE_CLK_B<0> WE_CLK<0> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=58730 $D=97
M78 VSS! WE_CLK<0> WE_CLK_B<0> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=59210 $D=97
M79 WE_CLK<0> 68 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=59690 $D=97
M80 69 CLK 53 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=3330 $D=97
M81 VSS! WE<15> 69 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=3810 $D=97
M82 70 CLK 54 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=7150 $D=97
M83 VSS! WE<14> 70 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=7630 $D=97
M84 71 CLK 55 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=10970 $D=97
M85 VSS! WE<13> 71 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=11450 $D=97
M86 72 CLK 56 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=14790 $D=97
M87 VSS! WE<12> 72 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=15270 $D=97
M88 73 CLK 57 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=18610 $D=97
M89 VSS! WE<11> 73 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=19090 $D=97
M90 74 CLK 58 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=22430 $D=97
M91 VSS! WE<10> 74 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=22910 $D=97
M92 75 CLK 59 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=26250 $D=97
M93 VSS! WE<9> 75 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=26730 $D=97
M94 76 CLK 60 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=30070 $D=97
M95 VSS! WE<8> 76 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=30550 $D=97
M96 77 CLK 61 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=33890 $D=97
M97 VSS! WE<7> 77 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=34370 $D=97
M98 78 CLK 62 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=37710 $D=97
M99 VSS! WE<6> 78 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=38190 $D=97
M100 79 CLK 63 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=41530 $D=97
M101 VSS! WE<5> 79 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=42010 $D=97
M102 80 CLK 64 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=45350 $D=97
M103 VSS! WE<4> 80 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=45830 $D=97
M104 81 CLK 65 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=49170 $D=97
M105 52 WE<3> 81 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=49650 $D=97
M106 82 CLK 66 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=52990 $D=97
M107 VSS! WE<2> 82 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=53470 $D=97
M108 83 CLK 67 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=56810 $D=97
M109 VSS! WE<1> 83 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=57290 $D=97
M110 84 CLK 68 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=60630 $D=97
M111 VSS! WE<0> 84 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=61110 $D=97
M112 WE_CLK_B<15> WE_CLK<15> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=470 $D=189
M113 VDD! WE_CLK<15> WE_CLK_B<15> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=950 $D=189
M114 WE_CLK_B<15> WE_CLK<15> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=1430 $D=189
M115 VDD! WE_CLK<15> WE_CLK_B<15> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=1910 $D=189
M116 WE_CLK<15> 53 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=2390 $D=189
M117 53 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=3330 $D=189
M118 VDD! WE<15> 53 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=3810 $D=189
M119 WE_CLK_B<14> WE_CLK<14> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=4290 $D=189
M120 VDD! WE_CLK<14> WE_CLK_B<14> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=4770 $D=189
M121 WE_CLK_B<14> WE_CLK<14> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=5250 $D=189
M122 VDD! WE_CLK<14> WE_CLK_B<14> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=5730 $D=189
M123 WE_CLK<14> 54 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=6210 $D=189
M124 54 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=7150 $D=189
M125 VDD! WE<14> 54 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=7630 $D=189
M126 WE_CLK_B<13> WE_CLK<13> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=8110 $D=189
M127 VDD! WE_CLK<13> WE_CLK_B<13> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=8590 $D=189
M128 WE_CLK_B<13> WE_CLK<13> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=9070 $D=189
M129 VDD! WE_CLK<13> WE_CLK_B<13> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=9550 $D=189
M130 WE_CLK<13> 55 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=10030 $D=189
M131 55 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=10970 $D=189
M132 VDD! WE<13> 55 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=11450 $D=189
M133 WE_CLK_B<12> WE_CLK<12> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=11930 $D=189
M134 VDD! WE_CLK<12> WE_CLK_B<12> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=12410 $D=189
M135 WE_CLK_B<12> WE_CLK<12> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=12890 $D=189
M136 VDD! WE_CLK<12> WE_CLK_B<12> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=13370 $D=189
M137 WE_CLK<12> 56 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=13850 $D=189
M138 56 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=14790 $D=189
M139 VDD! WE<12> 56 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=15270 $D=189
M140 WE_CLK_B<11> WE_CLK<11> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=15750 $D=189
M141 VDD! WE_CLK<11> WE_CLK_B<11> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=16230 $D=189
M142 WE_CLK_B<11> WE_CLK<11> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=16710 $D=189
M143 VDD! WE_CLK<11> WE_CLK_B<11> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=17190 $D=189
M144 WE_CLK<11> 57 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=17670 $D=189
M145 57 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=18610 $D=189
M146 VDD! WE<11> 57 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=19090 $D=189
M147 WE_CLK_B<10> WE_CLK<10> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=19570 $D=189
M148 VDD! WE_CLK<10> WE_CLK_B<10> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=20050 $D=189
M149 WE_CLK_B<10> WE_CLK<10> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=20530 $D=189
M150 VDD! WE_CLK<10> WE_CLK_B<10> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=21010 $D=189
M151 WE_CLK<10> 58 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=21490 $D=189
M152 58 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=22430 $D=189
M153 VDD! WE<10> 58 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=22910 $D=189
M154 WE_CLK_B<9> WE_CLK<9> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=23390 $D=189
M155 VDD! WE_CLK<9> WE_CLK_B<9> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=23870 $D=189
M156 WE_CLK_B<9> WE_CLK<9> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=24350 $D=189
M157 VDD! WE_CLK<9> WE_CLK_B<9> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=24830 $D=189
M158 WE_CLK<9> 59 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=25310 $D=189
M159 59 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=26250 $D=189
M160 VDD! WE<9> 59 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=26730 $D=189
M161 WE_CLK_B<8> WE_CLK<8> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=27210 $D=189
M162 VDD! WE_CLK<8> WE_CLK_B<8> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=27690 $D=189
M163 WE_CLK_B<8> WE_CLK<8> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=28170 $D=189
M164 VDD! WE_CLK<8> WE_CLK_B<8> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=28650 $D=189
M165 WE_CLK<8> 60 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=29130 $D=189
M166 60 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=30070 $D=189
M167 VDD! WE<8> 60 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=30550 $D=189
M168 WE_CLK_B<7> WE_CLK<7> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=31030 $D=189
M169 VDD! WE_CLK<7> WE_CLK_B<7> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=31510 $D=189
M170 WE_CLK_B<7> WE_CLK<7> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=31990 $D=189
M171 VDD! WE_CLK<7> WE_CLK_B<7> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=32470 $D=189
M172 WE_CLK<7> 61 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=32950 $D=189
M173 61 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=33890 $D=189
M174 VDD! WE<7> 61 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=34370 $D=189
M175 WE_CLK_B<6> WE_CLK<6> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=34850 $D=189
M176 VDD! WE_CLK<6> WE_CLK_B<6> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=35330 $D=189
M177 WE_CLK_B<6> WE_CLK<6> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=35810 $D=189
M178 VDD! WE_CLK<6> WE_CLK_B<6> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=36290 $D=189
M179 WE_CLK<6> 62 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=36770 $D=189
M180 62 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=37710 $D=189
M181 VDD! WE<6> 62 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=38190 $D=189
M182 WE_CLK_B<5> WE_CLK<5> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=38670 $D=189
M183 VDD! WE_CLK<5> WE_CLK_B<5> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=39150 $D=189
M184 WE_CLK_B<5> WE_CLK<5> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=39630 $D=189
M185 VDD! WE_CLK<5> WE_CLK_B<5> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=40110 $D=189
M186 WE_CLK<5> 63 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=40590 $D=189
M187 63 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=41530 $D=189
M188 VDD! WE<5> 63 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=42010 $D=189
M189 WE_CLK_B<4> WE_CLK<4> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=42490 $D=189
M190 VDD! WE_CLK<4> WE_CLK_B<4> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=42970 $D=189
M191 WE_CLK_B<4> WE_CLK<4> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=43450 $D=189
M192 VDD! WE_CLK<4> WE_CLK_B<4> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=43930 $D=189
M193 WE_CLK<4> 64 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=44410 $D=189
M194 64 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=45350 $D=189
M195 VDD! WE<4> 64 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=45830 $D=189
M196 WE_CLK_B<3> WE_CLK<3> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=46310 $D=189
M197 VDD! WE_CLK<3> WE_CLK_B<3> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=46790 $D=189
M198 WE_CLK_B<3> WE_CLK<3> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=47270 $D=189
M199 VDD! WE_CLK<3> WE_CLK_B<3> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=47750 $D=189
M200 WE_CLK<3> 65 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=48230 $D=189
M201 65 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=49170 $D=189
M202 VDD! WE<3> 65 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=49650 $D=189
M203 WE_CLK_B<2> WE_CLK<2> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=50130 $D=189
M204 VDD! WE_CLK<2> WE_CLK_B<2> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=50610 $D=189
M205 WE_CLK_B<2> WE_CLK<2> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=51090 $D=189
M206 VDD! WE_CLK<2> WE_CLK_B<2> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=51570 $D=189
M207 WE_CLK<2> 66 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=52050 $D=189
M208 66 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=52990 $D=189
M209 VDD! WE<2> 66 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=53470 $D=189
M210 WE_CLK_B<1> WE_CLK<1> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=53950 $D=189
M211 VDD! WE_CLK<1> WE_CLK_B<1> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=54430 $D=189
M212 WE_CLK_B<1> WE_CLK<1> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=54910 $D=189
M213 VDD! WE_CLK<1> WE_CLK_B<1> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=55390 $D=189
M214 WE_CLK<1> 67 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=55870 $D=189
M215 67 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=56810 $D=189
M216 VDD! WE<1> 67 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=57290 $D=189
M217 WE_CLK_B<0> WE_CLK<0> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=57770 $D=189
M218 VDD! WE_CLK<0> WE_CLK_B<0> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=58250 $D=189
M219 WE_CLK_B<0> WE_CLK<0> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=58730 $D=189
M220 VDD! WE_CLK<0> WE_CLK_B<0> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=59210 $D=189
M221 WE_CLK<0> 68 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=59690 $D=189
M222 68 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=60630 $D=189
M223 VDD! WE<0> 68 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=61110 $D=189
.ENDS
***************************************
.SUBCKT RF_slave_1 we_clkb READ_B we_clk READ_A S QA QB VSS! VDD! 10 11
** N=99 EP=11 IP=0 FDC=20
M0 20 15 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1440 $D=97
M1 16 we_clkb 20 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1890 $D=97
M2 17 we_clk 16 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3000 $D=97
M3 VSS! S 17 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3480 $D=97
M4 14 16 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3960 $D=97
M5 14 READ_B QB VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=3120 $D=97
M6 QA READ_A 14 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=3600 $D=97
M7 10 16 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=1620 $D=97
M8 18 READ_B 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=2140 $D=97
M9 11 READ_A 19 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=4560 $D=97
M10 21 15 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=1440 $D=189
M11 16 we_clk 21 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=1890 $D=189
M12 17 we_clkb 16 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3000 $D=189
M13 VDD! S 17 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3480 $D=189
M14 14 16 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3960 $D=189
M15 VDD! 16 15 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1620 $D=189
M16 18 READ_B VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2140 $D=189
M17 14 18 QB VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3120 $D=189
M18 QA 19 14 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3600 $D=189
M19 VDD! READ_A 19 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=4560 $D=189
.ENDS
***************************************
.SUBCKT ICV_58 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
** N=17 EP=15 IP=26 FDC=40
X0 1 2 3 4 5 6 7 11 12 13 14 RF_slave_1 $T=0 0 0 0 $X=-320 $Y=0
X1 1 2 3 4 8 9 10 11 12 15 14 RF_slave_1 $T=6000 0 0 0 $X=5680 $Y=0
.ENDS
***************************************
.SUBCKT ICV_59 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21
** N=23 EP=21 IP=34 FDC=80
X0 1 2 3 4 5 6 7 8 9 10 17 18 19 20 19 ICV_58 $T=0 0 0 0 $X=-320 $Y=0
X1 1 2 3 4 11 12 13 14 15 16 17 18 19 20 21 ICV_58 $T=12000 0 0 0 $X=11680 $Y=0
.ENDS
***************************************
.SUBCKT RF_WD_1 WE_CLKB READ_B WE_CLK READ_A S<0> QA<0> QB<0> S<1> QA<1> QB<1> S<2> QA<2> QB<2> S<3> QA<3> QB<3> S<4> QA<4> QB<4> S<5>
+ QA<5> QB<5> S<6> QA<6> QB<6> S<7> QA<7> QB<7> S<8> QA<8> QB<8> S<9> QA<9> QB<9> S<10> QA<10> QB<10> S<11> QA<11> QB<11>
+ S<12> QA<12> QB<12> S<13> QA<13> QB<13> S<14> QA<14> QB<14> S<15> QA<15> QB<15> VSS! VDD! 55 56 57
** N=57 EP=57 IP=92 FDC=320
X0 WE_CLKB READ_B WE_CLK READ_A S<0> QA<0> QB<0> S<1> QA<1> QB<1> S<2> QA<2> QB<2> S<3> QA<3> QB<3> VSS! VDD! 55 56
+ 55
+ ICV_59 $T=0 0 0 0 $X=-320 $Y=0
X1 WE_CLKB READ_B WE_CLK READ_A S<4> QA<4> QB<4> S<5> QA<5> QB<5> S<6> QA<6> QB<6> S<7> QA<7> QB<7> VSS! VDD! 55 56
+ 55
+ ICV_59 $T=24000 0 0 0 $X=23680 $Y=0
X2 WE_CLKB READ_B WE_CLK READ_A S<8> QA<8> QB<8> S<9> QA<9> QB<9> S<10> QA<10> QB<10> S<11> QA<11> QB<11> VSS! VDD! 55 56
+ 55
+ ICV_59 $T=48000 0 0 0 $X=47680 $Y=0
X3 WE_CLKB READ_B WE_CLK READ_A S<12> QA<12> QB<12> S<13> QA<13> QB<13> S<14> QA<14> QB<14> S<15> QA<15> QB<15> VSS! VDD! 55 56
+ 57
+ ICV_59 $T=72000 0 0 0 $X=71680 $Y=0
.ENDS
***************************************
.SUBCKT RF_slave READ_B we_clkb we_clk READ_A S QA QB VSS! VDD!
** N=95 EP=9 IP=0 FDC=20
M0 VSS! 12 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=960 $D=97
M1 17 13 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1440 $D=97
M2 12 we_clkb 17 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1890 $D=97
M3 14 we_clk 12 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3000 $D=97
M4 VSS! S 14 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3480 $D=97
M5 11 12 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3960 $D=97
M6 11 READ_B QB VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=2520 $D=97
M7 QA READ_A 11 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=3000 $D=97
M8 15 READ_B VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=1540 $D=97
M9 VSS! READ_A 16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=3960 $D=97
M10 VDD! 12 13 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=960 $D=189
M11 18 13 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=1440 $D=189
M12 12 we_clk 18 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=1890 $D=189
M13 14 we_clkb 12 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3000 $D=189
M14 VDD! S 14 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3480 $D=189
M15 11 12 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3960 $D=189
M16 15 READ_B VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1540 $D=189
M17 11 15 QB VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2520 $D=189
M18 QA 16 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3000 $D=189
M19 VDD! READ_A 16 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3960 $D=189
.ENDS
***************************************
.SUBCKT ICV_60 1 2 3 4 5 6 7 8 9 10 11 12
** N=13 EP=12 IP=20 FDC=40
X0 1 2 3 4 5 6 7 11 12 RF_slave $T=0 0 0 0 $X=-320 $Y=0
X1 1 2 3 4 8 9 10 11 12 RF_slave $T=6000 0 0 0 $X=5680 $Y=0
.ENDS
***************************************
.SUBCKT ICV_61 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
** N=19 EP=18 IP=26 FDC=80
X0 1 2 3 4 5 6 7 8 9 10 17 18 ICV_60 $T=0 0 0 0 $X=-320 $Y=0
X1 1 2 3 4 11 12 13 14 15 16 17 18 ICV_60 $T=12000 0 0 0 $X=11680 $Y=0
.ENDS
***************************************
.SUBCKT RF_WD READ_B WE_CLKB WE_CLK READ_A S<0> QA<0> QB<0> S<1> QA<1> QB<1> S<2> QA<2> QB<2> S<3> QA<3> QB<3> S<4> QA<4> QB<4> S<5>
+ QA<5> QB<5> S<6> QA<6> QB<6> S<7> QA<7> QB<7> S<8> QA<8> QB<8> S<9> QA<9> QB<9> S<10> QA<10> QB<10> S<11> QA<11> QB<11>
+ S<12> QA<12> QB<12> S<13> QA<13> QB<13> S<14> QA<14> QB<14> S<15> QA<15> QB<15> VSS! VDD!
** N=55 EP=54 IP=76 FDC=320
X0 READ_B WE_CLKB WE_CLK READ_A S<0> QA<0> QB<0> S<1> QA<1> QB<1> S<2> QA<2> QB<2> S<3> QA<3> QB<3> VSS! VDD! ICV_61 $T=0 0 0 0 $X=-320 $Y=0
X1 READ_B WE_CLKB WE_CLK READ_A S<4> QA<4> QB<4> S<5> QA<5> QB<5> S<6> QA<6> QB<6> S<7> QA<7> QB<7> VSS! VDD! ICV_61 $T=24000 0 0 0 $X=23680 $Y=0
X2 READ_B WE_CLKB WE_CLK READ_A S<8> QA<8> QB<8> S<9> QA<9> QB<9> S<10> QA<10> QB<10> S<11> QA<11> QB<11> VSS! VDD! ICV_61 $T=48000 0 0 0 $X=47680 $Y=0
X3 READ_B WE_CLKB WE_CLK READ_A S<12> QA<12> QB<12> S<13> QA<13> QB<13> S<14> QA<14> QB<14> S<15> QA<15> QB<15> VSS! VDD! ICV_61 $T=72000 0 0 0 $X=71680 $Y=0
.ENDS
***************************************
.SUBCKT ICV_62 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61
** N=61 EP=61 IP=112 FDC=640
X0 1 2 3 4 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
+ 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44
+ 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61
+ RF_WD_1 $T=0 0 0 0 $X=-320 $Y=0
X1 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
+ 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44
+ 45 46 47 48 49 50 51 52 53 54 55 56 57 58
+ RF_WD $T=0 4000 0 0 $X=-320 $Y=4000
.ENDS
***************************************
.SUBCKT RF_master WE_MASTER_B WE_MASTER D MASTER_DATA VSS! VDD!
** N=92 EP=6 IP=0 FDC=24
M0 VSS! 7 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=5290 $D=97
M1 9 D VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=850 $Y=430 $D=97
M2 10 8 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=5220 $D=97
M3 7 WE_MASTER 9 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1360 $Y=430 $D=97
M4 VSS! 8 10 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=5220 $D=97
M5 MASTER_DATA 10 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1940 $Y=5130 $D=97
M6 12 WE_MASTER_B 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2140 $Y=430 $D=97
M7 VSS! 10 MASTER_DATA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2420 $Y=5130 $D=97
M8 VSS! 11 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2560 $Y=430 $D=97
M9 MASTER_DATA 10 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2900 $Y=5130 $D=97
M10 11 7 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3040 $Y=430 $D=97
M11 VSS! 10 MASTER_DATA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3380 $Y=5130 $D=97
M12 VDD! 7 8 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=3510 $D=189
M13 9 D VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=850 $Y=1950 $D=189
M14 10 8 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=3510 $D=189
M15 VDD! 8 10 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=3510 $D=189
M16 7 WE_MASTER_B 9 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1600 $Y=1950 $D=189
M17 MASTER_DATA 10 VDD! VDD! pfet L=1.2e-07 W=8.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1940 $Y=3510 $D=189
M18 13 WE_MASTER 7 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2140 $Y=1950 $D=189
M19 VDD! 10 MASTER_DATA VDD! pfet L=1.2e-07 W=8.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2420 $Y=3510 $D=189
M20 VDD! 11 13 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2560 $Y=1950 $D=189
M21 MASTER_DATA 10 VDD! VDD! pfet L=1.2e-07 W=8.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2900 $Y=3510 $D=189
M22 11 7 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3040 $Y=1950 $D=189
M23 VDD! 10 MASTER_DATA VDD! pfet L=1.2e-07 W=8.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3380 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT ICV_57 1 2 3 4 5 6 7 8
** N=8 EP=8 IP=12 FDC=48
X0 1 2 3 4 8 7 RF_master $T=0 -6000 0 0 $X=-200 $Y=-6300
X1 1 2 5 6 8 7 RF_master $T=0 0 0 0 $X=-200 $Y=-300
.ENDS
***************************************
.SUBCKT RF_BUFFER BUFF_A QA BUFF_B QB VSS! VDD!
** N=78 EP=6 IP=0 FDC=16
M0 VSS! 10 QA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1010 $D=97
M1 QA 10 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1490 $D=97
M2 VSS! 10 QA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1970 $D=97
M3 VSS! BUFF_A 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2960 $D=97
M4 VSS! 11 QB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=780 $D=97
M5 QB 11 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=1260 $D=97
M6 VSS! 11 QB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=1740 $D=97
M7 VSS! BUFF_B 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=2680 $D=97
M8 VDD! 10 QA VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1490 $Y=1010 $D=189
M9 QA 10 VDD! VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1490 $Y=1490 $D=189
M10 VDD! 10 QA VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1490 $Y=1970 $D=189
M11 VDD! BUFF_A 10 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1850 $Y=2960 $D=189
M12 VDD! 11 QB VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=780 $D=189
M13 QB 11 VDD! VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1260 $D=189
M14 VDD! 11 QB VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1740 $D=189
M15 VDD! BUFF_B 11 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2680 $D=189
.ENDS
***************************************
.SUBCKT ICV_56 1 2 3 4 5 6 7 8 9 10
** N=14 EP=10 IP=18 FDC=32
X0 1 2 3 4 9 10 RF_BUFFER $T=0 0 0 0 $X=-240 $Y=0
X1 5 6 7 8 9 10 RF_BUFFER $T=6000 0 0 0 $X=5760 $Y=0
.ENDS
***************************************
.SUBCKT RF WE_MASTER 2 READ_B<15> READ_A<15> READ_B<14> READ_A<14> READ_B<13> READ_A<13> READ_B<12> READ_A<12> READ_B<11> READ_A<11> READ_B<10> READ_A<10> READ_B<9> READ_A<9> READ_B<8> READ_A<8> READ_B<7> READ_A<7>
+ READ_B<6> READ_A<6> READ_B<5> READ_A<5> READ_B<4> READ_A<4> READ_B<3> READ_A<3> READ_B<2> READ_A<2> READ_B<1> READ_A<1> READ_B<0> READ_A<0> CLK D<1> D<0> D<3> D<2> D<5>
+ D<4> D<7> D<6> D<9> D<8> D<11> D<10> D<13> D<12> D<15> D<14> QA<0> QB<0> QA<1> QB<1> QA<2> QB<2> QA<3> QB<3> QA<4>
+ QB<4> QA<5> QB<5> QA<6> QB<6> QA<7> QB<7> QA<8> QB<8> QA<9> QB<9> QA<10> QB<10> QA<11> QB<11> QA<12> QB<12> QA<13> QB<13> QA<14>
+ QB<14> QA<15> QB<15> VDD! VSS! WE<15> WE<14> WE<13> WE<12> WE<11> WE<10> WE<9> WE<8> WE<7> WE<6> WE<5> WE<4> WE<3> WE<2> WE<1>
+ WE<0> 102
** N=274 EP=102 IP=716 FDC=5998
M0 135 183 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=63990 $D=97
M1 VSS! 183 135 VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=64470 $D=97
M2 135 183 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=64950 $D=97
M3 VSS! 183 135 VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=65430 $D=97
M4 183 184 VSS! VSS! nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4190 $Y=65970 $D=97
M5 VSS! 184 183 VSS! nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4190 $Y=66450 $D=97
M6 184 WE_MASTER VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4470 $Y=66990 $D=97
M7 135 183 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=63990 $D=189
M8 VDD! 183 135 VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=64470 $D=189
M9 135 183 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=64950 $D=189
M10 VDD! 183 135 VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=65430 $D=189
M11 183 184 VDD! VDD! pfet L=1.2e-07 W=1.08e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=65970 $D=189
M12 VDD! 184 183 VDD! pfet L=1.2e-07 W=1.08e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=66450 $D=189
M13 184 WE_MASTER VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=66990 $D=189
X14 CLK VDD! WE<15> WE<14> WE<13> WE<12> WE<11> WE<10> WE<9> WE<8> WE<7> WE<6> WE<5> WE<4> WE<3> WE<2> WE<1> WE<0> 103 105
+ 107 109 111 113 115 117 119 121 123 125 127 129 130 131 104 106 108 110 112 114
+ 116 118 120 122 124 126 128 132 133 134 VSS! 102
+ RF_SCLK $T=0 2400 0 0 $X=-240 $Y=2200
X15 103 READ_B<15> 104 READ_A<15> READ_B<14> 105 106 READ_A<14> 151 136 152 153 2 154 155 137 156 157 138 158
+ 159 139 160 161 140 162 163 141 164 165 142 166 167 143 168 169 144 170 171 145
+ 172 173 146 174 175 147 176 177 148 178 179 149 180 181 150 182 VSS! VDD! VSS! VSS!
+ VSS!
+ ICV_62 $T=5200 2000 0 0 $X=4880 $Y=2000
X16 107 READ_B<13> 108 READ_A<13> READ_B<12> 109 110 READ_A<12> 151 136 152 153 2 154 155 137 156 157 138 158
+ 159 139 160 161 140 162 163 141 164 165 142 166 167 143 168 169 144 170 171 145
+ 172 173 146 174 175 147 176 177 148 178 179 149 180 181 150 182 VSS! VDD! VSS! VSS!
+ VSS!
+ ICV_62 $T=5200 9600 0 0 $X=4880 $Y=9600
X17 111 READ_B<11> 112 READ_A<11> READ_B<10> 113 114 READ_A<10> 151 136 152 153 2 154 155 137 156 157 138 158
+ 159 139 160 161 140 162 163 141 164 165 142 166 167 143 168 169 144 170 171 145
+ 172 173 146 174 175 147 176 177 148 178 179 149 180 181 150 182 VSS! VDD! VSS! VSS!
+ VSS!
+ ICV_62 $T=5200 17200 0 0 $X=4880 $Y=17200
X18 115 READ_B<9> 116 READ_A<9> READ_B<8> 117 118 READ_A<8> 151 136 152 153 2 154 155 137 156 157 138 158
+ 159 139 160 161 140 162 163 141 164 165 142 166 167 143 168 169 144 170 171 145
+ 172 173 146 174 175 147 176 177 148 178 179 149 180 181 150 182 VSS! VDD! VSS! VSS!
+ VSS!
+ ICV_62 $T=5200 24800 0 0 $X=4880 $Y=24800
X19 119 READ_B<7> 120 READ_A<7> READ_B<6> 121 122 READ_A<6> 151 136 152 153 2 154 155 137 156 157 138 158
+ 159 139 160 161 140 162 163 141 164 165 142 166 167 143 168 169 144 170 171 145
+ 172 173 146 174 175 147 176 177 148 178 179 149 180 181 150 182 VSS! VDD! VSS! VSS!
+ VSS!
+ ICV_62 $T=5200 32400 0 0 $X=4880 $Y=32400
X20 123 READ_B<5> 124 READ_A<5> READ_B<4> 125 126 READ_A<4> 151 136 152 153 2 154 155 137 156 157 138 158
+ 159 139 160 161 140 162 163 141 164 165 142 166 167 143 168 169 144 170 171 145
+ 172 173 146 174 175 147 176 177 148 178 179 149 180 181 150 182 VSS! VDD! 102 VSS!
+ 102
+ ICV_62 $T=5200 40000 0 0 $X=4880 $Y=40000
X21 127 READ_B<3> 128 READ_A<3> READ_B<2> 129 132 READ_A<2> 151 136 152 153 2 154 155 137 156 157 138 158
+ 159 139 160 161 140 162 163 141 164 165 142 166 167 143 168 169 144 170 171 145
+ 172 173 146 174 175 147 176 177 148 178 179 149 180 181 150 182 VSS! VDD! 102 102
+ VSS!
+ ICV_62 $T=5200 47600 0 0 $X=4880 $Y=47600
X22 130 READ_B<1> 133 READ_A<1> READ_B<0> 131 134 READ_A<0> 151 136 152 153 2 154 155 137 156 157 138 158
+ 159 139 160 161 140 162 163 141 164 165 142 166 167 143 168 169 144 170 171 145
+ 172 173 146 174 175 147 176 177 148 178 179 149 180 181 150 182 VSS! VDD! VSS! VSS!
+ VSS!
+ ICV_62 $T=5200 55200 0 0 $X=4880 $Y=55200
X23 135 WE_MASTER D<1> 153 D<0> 151 VDD! VSS! ICV_57 $T=11200 67600 1 270 $X=4900 $Y=63200
X24 135 WE_MASTER D<3> 157 D<2> 155 VDD! VSS! ICV_57 $T=23200 67600 1 270 $X=16900 $Y=63200
X25 135 WE_MASTER D<5> 161 D<4> 159 VDD! VSS! ICV_57 $T=35200 67600 1 270 $X=28900 $Y=63200
X26 135 WE_MASTER D<7> 165 D<6> 163 VDD! VSS! ICV_57 $T=47200 67600 1 270 $X=40900 $Y=63200
X27 135 WE_MASTER D<9> 169 D<8> 167 VDD! VSS! ICV_57 $T=59200 67600 1 270 $X=52900 $Y=63200
X28 135 WE_MASTER D<11> 173 D<10> 171 VDD! VSS! ICV_57 $T=71200 67600 1 270 $X=64900 $Y=63200
X29 135 WE_MASTER D<13> 177 D<12> 175 VDD! VSS! ICV_57 $T=83200 67600 1 270 $X=76900 $Y=63200
X30 135 WE_MASTER D<15> 181 D<14> 179 VDD! VSS! ICV_57 $T=95200 67600 1 270 $X=88900 $Y=63200
X31 136 QA<0> 152 QB<0> 2 QA<1> 154 QB<1> VSS! VDD! ICV_56 $T=5200 0 0 0 $X=4960 $Y=0
X32 137 QA<2> 156 QB<2> 138 QA<3> 158 QB<3> VSS! VDD! ICV_56 $T=17200 0 0 0 $X=16960 $Y=0
X33 139 QA<4> 160 QB<4> 140 QA<5> 162 QB<5> VSS! VDD! ICV_56 $T=29200 0 0 0 $X=28960 $Y=0
X34 141 QA<6> 164 QB<6> 142 QA<7> 166 QB<7> VSS! VDD! ICV_56 $T=41200 0 0 0 $X=40960 $Y=0
X35 143 QA<8> 168 QB<8> 144 QA<9> 170 QB<9> VSS! VDD! ICV_56 $T=53200 0 0 0 $X=52960 $Y=0
X36 145 QA<10> 172 QB<10> 146 QA<11> 174 QB<11> VSS! VDD! ICV_56 $T=65200 0 0 0 $X=64960 $Y=0
X37 147 QA<12> 176 QB<12> 148 QA<13> 178 QB<13> VSS! VDD! ICV_56 $T=77200 0 0 0 $X=76960 $Y=0
X38 149 QA<14> 180 QB<14> 150 QA<15> 182 QB<15> VSS! VDD! ICV_56 $T=89200 0 0 0 $X=88960 $Y=0
.ENDS
***************************************
.SUBCKT MUX_Rsrc_1bit SEL<1> SEL_BAR<1> SEL_BAR<0> SEL<0> ALU_B Imm_16 Rsrc VSS! VDD!
** N=85 EP=9 IP=0 FDC=18
M0 12 SEL<1> 10 VDD! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=540 $D=97
M1 11 SEL_BAR<1> 12 VDD! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1100 $D=97
M2 ALU_B 12 VSS! VDD! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2240 $D=97
M3 VSS! 12 ALU_B VDD! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2800 $D=97
M4 ALU_B 12 VSS! VDD! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3360 $D=97
M5 VSS! 12 ALU_B VDD! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3900 $D=97
M6 VSS! 10 11 VDD! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=2340 $D=97
M7 10 SEL<0> Imm_16 VDD! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5250 $Y=540 $D=97
M8 Rsrc SEL_BAR<0> 10 VDD! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5250 $Y=1100 $D=97
M9 ALU_B 12 VDD! VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=2240 $D=189
M10 VDD! 12 ALU_B VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=2800 $D=189
M11 ALU_B 12 VDD! VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=3360 $D=189
M12 VDD! 12 ALU_B VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=3900 $D=189
M13 12 SEL_BAR<1> 10 VDD! pfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2050 $Y=540 $D=189
M14 11 SEL<1> 12 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1100 $D=189
M15 10 SEL_BAR<0> Imm_16 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=540 $D=189
M16 Rsrc SEL<0> 10 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=1100 $D=189
M17 VDD! 10 11 VDD! pfet L=1.2e-07 W=6.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=2340 $D=189
.ENDS
***************************************
.SUBCKT MUX_Rdest_1bit ALU_A Rdest SEL SEL_BAR VSS! VDD!
** N=54 EP=6 IP=0 FDC=10
M0 7 SEL VSS! VDD! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=970 $D=97
M1 Rdest SEL_BAR 7 VDD! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1730 $D=97
M2 VSS! 7 8 VDD! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=870 $D=97
M3 ALU_A 8 VSS! VDD! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1400 $D=97
M4 VSS! 8 ALU_A VDD! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1880 $D=97
M5 7 SEL_BAR VSS! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=970 $D=189
M6 Rdest SEL 7 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1730 $D=189
M7 VDD! 7 8 VDD! pfet L=1.2e-07 W=7.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=870 $D=189
M8 ALU_A 8 VDD! VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1400 $D=189
M9 VDD! 8 ALU_A VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1880 $D=189
.ENDS
***************************************
.SUBCKT ICV_55 1 2 3 4 5 6 7 8 9 10 11 12
** N=12 EP=12 IP=15 FDC=28
X0 1 2 3 4 6 7 8 12 12 MUX_Rsrc_1bit $T=0 0 0 0 $X=-240 $Y=-160
X1 5 9 10 11 12 12 MUX_Rdest_1bit $T=6000 4400 1 180 $X=-240 $Y=4380
.ENDS
***************************************
.SUBCKT MUX_Shifter_1bit Rdest Shifter SEL Imm_16 SEL_BAR VSS! VDD!
** N=50 EP=7 IP=0 FDC=10
M0 8 SEL Imm_16 VDD! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=570 $D=97
M1 Rdest SEL_BAR 8 VDD! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1330 $D=97
M2 VSS! 8 9 VDD! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=470 $D=97
M3 Shifter 9 VSS! VDD! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1000 $D=97
M4 VSS! 9 Shifter VDD! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1480 $D=97
M5 8 SEL_BAR Imm_16 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=570 $D=189
M6 Rdest SEL 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1330 $D=189
M7 VDD! 8 9 VDD! pfet L=1.2e-07 W=7.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=470 $D=189
M8 Shifter 9 VDD! VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1000 $D=189
M9 VDD! 9 Shifter VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1480 $D=189
.ENDS
***************************************
.SUBCKT ICV_68 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41
+ 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61
+ 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
+ 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101
+ 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121
+ 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 141
+ 142 143 144 145 146 147 148 149 150 151 152 153 154 155
** N=530 EP=154 IP=754 FDC=9854
M0 64 238 239 64 nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=-30260 $D=97
M1 239 238 64 64 nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=-29700 $D=97
M2 64 238 239 64 nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=-29140 $D=97
M3 238 277 64 64 nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=-28580 $D=97
M4 64 277 238 64 nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=-28020 $D=97
M5 277 103 64 64 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=-27460 $D=97
M6 242 278 64 64 nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130400 $Y=-28580 $D=97
M7 64 278 242 64 nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130400 $Y=-28020 $D=97
M8 64 242 240 64 nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130420 $Y=-30260 $D=97
M9 240 242 64 64 nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130420 $Y=-29700 $D=97
M10 64 242 240 64 nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130420 $Y=-29140 $D=97
M11 278 111 64 64 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130470 $Y=-27460 $D=97
M12 64 275 274 64 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=19150 $D=97
M13 274 275 64 64 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=19710 $D=97
M14 64 275 274 64 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=20270 $D=97
M15 275 279 64 64 nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=20830 $D=97
M16 64 279 275 64 nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=21390 $D=97
M17 279 134 64 64 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=21950 $D=97
M18 253 251 64 64 nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138680 $Y=-24450 $D=97
M19 64 251 253 64 nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138680 $Y=-23970 $D=97
M20 254 252 64 64 nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138680 $Y=-10050 $D=97
M21 64 252 254 64 nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138680 $Y=-9570 $D=97
M22 251 280 64 64 nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138800 $Y=-25450 $D=97
M23 64 280 251 64 nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138800 $Y=-24970 $D=97
M24 252 281 64 64 nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138800 $Y=-11050 $D=97
M25 64 281 252 64 nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138800 $Y=-10570 $D=97
M26 64 112 280 64 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138870 $Y=-25980 $D=97
M27 64 13 281 64 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138870 $Y=-11580 $D=97
M28 64 238 239 64 pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127030 $Y=-30260 $D=189
M29 239 238 64 64 pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127030 $Y=-29700 $D=189
M30 64 238 239 64 pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127030 $Y=-29140 $D=189
M31 238 277 64 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127070 $Y=-28580 $D=189
M32 64 277 238 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127070 $Y=-28020 $D=189
M33 277 103 64 64 pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127190 $Y=-27460 $D=189
M34 64 242 240 64 pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=-30260 $D=189
M35 240 242 64 64 pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=-29700 $D=189
M36 64 242 240 64 pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=-29140 $D=189
M37 242 278 64 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=-28580 $D=189
M38 64 278 242 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=-28020 $D=189
M39 278 111 64 64 pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=-27460 $D=189
M40 64 275 274 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=19150 $D=189
M41 274 275 64 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=19710 $D=189
M42 64 275 274 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=20270 $D=189
M43 275 279 64 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=20830 $D=189
M44 64 279 275 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=21390 $D=189
M45 279 134 64 64 pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133210 $Y=21950 $D=189
M46 64 112 280 64 pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=-25980 $D=189
M47 251 280 64 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=-25450 $D=189
M48 64 280 251 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=-24970 $D=189
M49 253 251 64 64 pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=-24450 $D=189
M50 64 251 253 64 pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=-23970 $D=189
M51 64 13 281 64 pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=-11580 $D=189
M52 252 281 64 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=-11050 $D=189
M53 64 281 252 64 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=-10570 $D=189
M54 254 252 64 64 pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=-10050 $D=189
M55 64 252 254 64 pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=-9570 $D=189
X56 107 250 197 108 145 183 178 67 140 179 180 65 142 181 182 66 93 241 186 95
+ 96 243 189 100 101 245 177 104 105 248 194 106 120 198 201 68 123 202 205 69
+ 125 206 209 70 128 211 214 71 130 215 218 72 132 219 222 73 135 223 226 74
+ 138 227 230 75 109 110 64 64
+ MUX_D $T=125200 -57100 0 0 $X=124680 $Y=-57530
X57 273 113 64 64 64 MUX_Shifter_SEL_BUF $T=125200 -24700 0 270 $X=124950 $Y=-26480
X58 272 114 64 64 64 MUX_Shifter_SEL_BUF $T=131200 -24700 1 270 $X=127710 $Y=-26480
X59 272 247 115 273 85 64 64 64 64 MUX_Shifter_SEL_1bit $T=125200 -24700 1 90 $X=124960 $Y=-25130
X60 272 249 117 273 83 64 64 64 64 MUX_Shifter_SEL_1bit $T=125200 -19900 1 90 $X=124960 $Y=-20330
X61 272 246 119 273 81 64 64 64 64 MUX_Shifter_SEL_1bit $T=125200 -15100 1 90 $X=124960 $Y=-15530
X62 272 244 76 273 80 64 64 64 64 MUX_Shifter_SEL_1bit $T=125200 -10300 1 90 $X=124960 $Y=-10730
X63 161 76 64 Sign_Ext_left $T=131200 -26300 0 0 $X=130960 $Y=-26480
X64 162 119 64 Sign_Ext_left $T=131200 -24700 0 0 $X=130960 $Y=-24880
X65 163 117 64 Sign_Ext_left $T=131200 -23100 0 0 $X=130960 $Y=-23280
X66 164 115 64 Sign_Ext_left $T=131200 -21500 0 0 $X=130960 $Y=-21680
X67 165 9 64 Sign_Ext_left $T=131200 -19900 0 0 $X=130960 $Y=-20080
X68 166 116 64 Sign_Ext_left $T=131200 -18300 0 0 $X=130960 $Y=-18480
X69 167 118 64 Sign_Ext_left $T=131200 -16700 0 0 $X=130960 $Y=-16880
X70 168 10 64 Sign_Ext_left $T=131200 -15100 0 0 $X=130960 $Y=-15280
X71 169 274 275 64 Sign_Ext_right $T=131200 -13500 0 0 $X=130960 $Y=-13800
X72 170 274 275 64 Sign_Ext_right $T=131200 -9500 0 0 $X=130960 $Y=-9800
X73 171 274 275 64 Sign_Ext_right $T=131200 -5500 0 0 $X=130960 $Y=-5800
X74 172 274 275 64 Sign_Ext_right $T=131200 -1500 0 0 $X=130960 $Y=-1800
X75 173 274 275 64 Sign_Ext_right $T=131200 2500 0 0 $X=130960 $Y=2200
X76 174 274 275 64 Sign_Ext_right $T=131200 6500 0 0 $X=130960 $Y=6200
X77 175 274 275 64 Sign_Ext_right $T=131200 10500 0 0 $X=130960 $Y=10200
X78 176 274 275 64 Sign_Ext_right $T=131200 14500 0 0 $X=130960 $Y=14200
X79 94 98 97 99 102 103 185 188 191 193 196 200 204 208 213 217 1 221 225 229
+ 232 234 237 184 187 190 192 195 199 203 207 212 216 220 224 228 231 233 236 241
+ 243 245 248 250 198 202 206 211 215 219 223 227 179 181 183 64 64
+ ALU $T=134400 -57900 0 0 $X=134000 $Y=-58200
X80 270 256 257 258 259 260 261 262 263 264 265 266 267 268 269 247 249 246 244 271
+ 186 189 177 194 197 201 205 209 214 218 222 226 230 180 182 178 64 64 64
+ SHIFTER $T=134400 -23500 0 0 $X=134160 $Y=-23700
X81 57 276 56 11 12 32 14 33 15 34 16 35 36 37 17 38 18 39 40 41
+ 42 43 44 45 19 46 47 48 49 77 155 79 147 78 122 153 23 26 152 151
+ 148 63 150 58 62 61 59 22 60 20 21 80 55 81 82 83 84 85 86 5
+ 87 4 88 7 89 6 90 3 91 8 92 31 54 30 53 29 52 28 51 27
+ 50 2 24 64 64 121 124 126 127 129 131 133 136 137 139 141 143 144 154 146
+ 149 64
+ RF $T=134400 -9500 0 0 $X=134160 $Y=-9500
X82 238 239 240 242 184 185 161 80 55 251 253 64 ICV_55 $T=139600 -30700 0 0 $X=139360 $Y=-30860
X83 238 239 240 242 187 188 162 81 82 251 253 64 ICV_55 $T=145600 -30700 0 0 $X=145360 $Y=-30860
X84 238 239 240 242 190 191 163 83 84 251 253 64 ICV_55 $T=151600 -30700 0 0 $X=151360 $Y=-30860
X85 238 239 240 242 192 193 164 85 86 251 253 64 ICV_55 $T=157600 -30700 0 0 $X=157360 $Y=-30860
X86 238 239 240 242 195 196 165 5 87 251 253 64 ICV_55 $T=163600 -30700 0 0 $X=163360 $Y=-30860
X87 238 239 240 242 199 200 166 4 88 251 253 64 ICV_55 $T=169600 -30700 0 0 $X=169360 $Y=-30860
X88 238 239 240 242 203 204 167 7 89 251 253 64 ICV_55 $T=175600 -30700 0 0 $X=175360 $Y=-30860
X89 238 239 240 242 207 208 168 6 90 251 253 64 ICV_55 $T=181600 -30700 0 0 $X=181360 $Y=-30860
X90 238 239 240 242 212 213 169 3 91 251 253 64 ICV_55 $T=187600 -30700 0 0 $X=187360 $Y=-30860
X91 238 239 240 242 216 217 170 8 92 251 253 64 ICV_55 $T=193600 -30700 0 0 $X=193360 $Y=-30860
X92 238 239 240 242 220 221 171 31 54 251 253 64 ICV_55 $T=199600 -30700 0 0 $X=199360 $Y=-30860
X93 238 239 240 242 224 225 172 30 53 251 253 64 ICV_55 $T=205600 -30700 0 0 $X=205360 $Y=-30860
X94 238 239 240 242 228 229 173 29 52 251 253 64 ICV_55 $T=211600 -30700 0 0 $X=211360 $Y=-30860
X95 238 239 240 242 231 232 174 28 51 251 253 64 ICV_55 $T=217600 -30700 0 0 $X=217360 $Y=-30860
X96 238 239 240 242 233 234 175 27 50 251 253 64 ICV_55 $T=223600 -30700 0 0 $X=223360 $Y=-30860
X97 238 239 240 242 236 237 176 2 24 251 253 64 ICV_55 $T=229600 -30700 0 0 $X=229360 $Y=-30860
X98 55 270 252 161 254 64 64 MUX_Shifter_1bit $T=145600 -11500 1 180 $X=139360 $Y=-11800
X99 82 256 252 162 254 64 64 MUX_Shifter_1bit $T=151600 -11500 1 180 $X=145360 $Y=-11800
X100 84 257 252 163 254 64 64 MUX_Shifter_1bit $T=157600 -11500 1 180 $X=151360 $Y=-11800
X101 86 258 252 164 254 64 64 MUX_Shifter_1bit $T=163600 -11500 1 180 $X=157360 $Y=-11800
X102 87 259 252 165 254 64 64 MUX_Shifter_1bit $T=169600 -11500 1 180 $X=163360 $Y=-11800
X103 88 260 252 166 254 64 64 MUX_Shifter_1bit $T=175600 -11500 1 180 $X=169360 $Y=-11800
X104 89 261 252 167 254 64 64 MUX_Shifter_1bit $T=181600 -11500 1 180 $X=175360 $Y=-11800
X105 90 262 252 168 254 64 64 MUX_Shifter_1bit $T=187600 -11500 1 180 $X=181360 $Y=-11800
X106 91 263 252 169 254 64 64 MUX_Shifter_1bit $T=193600 -11500 1 180 $X=187360 $Y=-11800
X107 92 264 252 170 254 64 64 MUX_Shifter_1bit $T=199600 -11500 1 180 $X=193360 $Y=-11800
X108 54 265 252 171 254 64 64 MUX_Shifter_1bit $T=205600 -11500 1 180 $X=199360 $Y=-11800
X109 53 266 252 172 254 64 64 MUX_Shifter_1bit $T=211600 -11500 1 180 $X=205360 $Y=-11800
X110 52 267 252 173 254 64 64 MUX_Shifter_1bit $T=217600 -11500 1 180 $X=211360 $Y=-11800
X111 51 268 252 174 254 64 64 MUX_Shifter_1bit $T=223600 -11500 1 180 $X=217360 $Y=-11800
X112 50 269 252 175 254 64 64 MUX_Shifter_1bit $T=229600 -11500 1 180 $X=223360 $Y=-11800
X113 24 271 252 176 254 64 64 MUX_Shifter_1bit $T=235600 -11500 1 180 $X=229360 $Y=-11800
.ENDS
***************************************
.SUBCKT ICV_54
** N=40 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_53
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_52
** N=1607 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_51
** N=10299 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_50
** N=5 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_49
** N=4 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_48
** N=11891 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_18
** N=2 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_17
** N=8 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_16
** N=24 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_15
** N=1836 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_14
** N=2442 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_13
** N=2877 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_12
** N=2181 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_11
** N=2722 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT PDVDD DVDD VDD 3 DVSS VSS
** N=11 EP=5 IP=43 FDC=0
*.CALIBRE WARNING OPEN Open circuit(s) detected by extraction in this cell. See extraction report for details.
.ENDS
***************************************
.SUBCKT ICV_19 1
** N=6 EP=1 IP=6 FDC=0
X0 2 3 4 5 6 PDVDD $T=0 0 0 0 $X=-2 $Y=0
.ENDS
***************************************
.SUBCKT datapath_final_bakup VDD! clk CEN_1 RENB_SRAM Vref<0> Vref<1> Vref<2> Vref<3> Vref<4> Vref<5> Vref<6> Vref<7> Vref<8> Vref<9> Vref<10> Vref<11> Vref<12> Vref<13> Vref<14> Vref<15>
+ Vref<16> Vref<17> Vref<18> Vref<19> Vref<20> Vref<21> Vref<22> Vref<23> Vref<24> Vref<25> Vref<26> Vref<27> Vref<28> Vref<29> Vref<30> Vref<31> resetn scan_in scan_en scan_out
** N=399 EP=40 IP=1203 FDC=15519
X4 396 ICV_28 $T=-362670 539430 0 0 $X=-362672 $Y=539430
X9 13 14 15 16 19 24 1 2 3 34 35 4 5 6 36 37 38 39 40 41
+ 147 64 130 151 129 53 149 131 54 63 154 132 68 152 104 67 62 153 115 70
+ 145 50 116 61 144 51 69 155 49 71 60 150 160 72 158 138 73 81 146 139
+ 74 80 159 140 75 148 141 76 156 142 77 85 157 143 78 58 59 82 79 84
+ 89 92 105 121 83 88 93 106 111 122 126 125 124 123 52 110 127 134 135 128
+ 133 137 55 108 113 118 120 102 109 114 117 119 96 98 100 107 95 97 94 101
+ 87 91 86 90 56 57 163 VDD! VDD! 294 293 292 291 290 295 285 289 288 287 172
+ 286 251 284 281 280 283 279 282 278 161 250 296 162 Vref<0> Vref<1> Vref<2> Vref<3> Vref<4> Vref<5> Vref<6>
+ Vref<7> Vref<8> Vref<9> Vref<10> Vref<11> Vref<12> Vref<13> Vref<14> Vref<15> Vref<16> Vref<17> Vref<18> Vref<19> Vref<20> Vref<21> Vref<22> Vref<23> Vref<24> Vref<25> Vref<26>
+ Vref<27> Vref<28> Vref<29> Vref<30> Vref<31>
+ ICV_1 $T=-92000 -34700 0 0 $X=-234980 $Y=-64570
X10 VDD! 12 11 10 9 8 7 233 234 235 236 clk CEN_1 166 239 240 241 168 169 170
+ 171 242 243 244 167 245 246 247 248
+ ICV_38 $T=-187200 90200 0 0 $X=-187202 $Y=90198
X11 32 29 18 33 30 27 17 46 20 42 22 45 26 31 44 21 23 25 28 43
+ 65 66 99 103 112 136 164 165 249 47 48 VDD! 277 245 246 247 248 168 169 170
+ 171
+ ICV_86 $T=0 0 0 0 $X=-142200 $Y=-78820
X19 396 ICV_10 $T=-105180 989650 0 0 $X=-105182 $Y=989650
X20 162 52 163 53 54 55 56 57 161 58 59 60 61 62 63 13 14 15 64 16
+ 65 66 67 68 17 165 18 69 70 71 72 73 74 75 76 77 78 79 80 81
+ 19 164 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 23 24 1
+ 2 97 98 99 100 101 102 104 105 106 107 108 29 109 110 111 112 3 113 114
+ 115 32 116 33 117 118 34 35 4 5 6 119 120 121 122 123 124 125 126 36
+ 37 38 39 40 41 127 128 129 130 131 132 133 134 135 137 138 139 140 141 142
+ 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160 49 50
+ 51 27 20 clk 204 203 175 201 227 224 179 225 228 223 197 180 181 176 226 177
+ 202 178 200 320 319 316 317 315 182 318 183 210 216 250 212 205 219 207 332 48
+ 321 341 335 218 206 209 215 211 208 217 184 214 220 339 213 221 222 192 312 345
+ 195 196 346 311 329 342 194 348 347 187 190 191 189 185 186 188 193 173 VDD! 199
+ 12 RENB_SRAM 292 251 295 252 253 172 229 254 230 255 170 171 169 256 168 167 246 245
+ 247 248 244 257 241 243 239 258 240 166 242 259 260 331 334 333 325 322 323 310
+ 309 236 235 234 233 7 8 9 10 11 279 280 281 282 283 284 285 286 287 288
+ 290 291 293 294 278 349 307 299 303 324 301 313 330 327 306 338 298 308 326 304
+ 300 305 302 314 340 343 344 336 337 328 289 296 249 resetn scan_in scan_en scan_out
+ ICV_77 $T=0 0 0 0 $X=-8600 $Y=-78820
X28 252 253 VDD! 228 261 262 254 255 263 264 256 257 265 266 258 259 231 232 260 176
+ 179 180 177 178 55 56 161 57 229 230 250 267 268 227 269 270 226 225 271 272
+ 224 223 273 274 197
+ ICV_47 $T=200 69800 0 0 $X=198 $Y=69798
X29 396 ICV_29 $T=17840 576500 1 0 $X=17838 $Y=329498
X30 173 175 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193
+ 194 195 196 197 199 200 201 202 203 204 205 206 207 208 209 210 211 212 213 214
+ 215 216 217 218 219 220 221 222 223 224 225 226 227 228 321 250 340 338 336 337
+ 343 344 VDD! 272 273 274 266 231 232 267 268 269 270 271 320 332 341 335 57 161
+ 254 56 255 55 256 257 258 259 260 252 253 298 299 261 300 301 302 303 262 304
+ 305 306 263 307 264 308 265 309 310 311 312 313 314 315 316 317 318 319 322 30
+ clk 323 28 324 103 43 325 31 326 26 327 44 328 329 21 42 330 22 331 25
+ 333 136 277 334 46 339 342 47 345 346 347 348 45 349
+ ICV_68 $T=0 0 0 0 $X=124680 $Y=-78820
X38 396 ICV_19 $T=438080 592720 0 0 $X=438078 $Y=592720
*.CALIBRE WARNING SHORT Short circuit(s) detected by extraction in this cell. See extraction report for details.
.ENDS
***************************************
