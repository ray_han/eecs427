* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT WBL_Driver_1bit IN WBLB WBL VSS! VDD!
** N=69 EP=5 IP=0 FDC=18
M0 VSS! 7 6 VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=450 $D=97
M1 VSS! 7 6 VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=4840 $D=97
M2 WBLB 6 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=450 $D=97
M3 7 IN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=4870 $D=97
M4 VSS! 6 WBLB VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=450 $D=97
M5 WBLB 6 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M6 VSS! IN 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=4870 $D=97
M7 VSS! 6 WBLB VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M8 WBL 8 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=4750 $D=97
M9 VDD! 7 6 VDD! pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=1560 $D=189
M10 VDD! 7 6 VDD! pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=3310 $D=189
M11 WBLB 6 VDD! VDD! pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=1500 $D=189
M12 7 IN VDD! VDD! pfet L=1.2e-07 W=6.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3310 $D=189
M13 VDD! 6 WBLB VDD! pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=1500 $D=189
M14 WBLB 6 VDD! VDD! pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1500 $D=189
M15 VDD! IN 8 VDD! pfet L=1.2e-07 W=5.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3310 $D=189
M16 VDD! 6 WBLB VDD! pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1500 $D=189
M17 WBL 8 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8
** N=8 EP=8 IP=10 FDC=36
X0 1 2 3 7 8 WBL_Driver_1bit $T=0 0 0 0 $X=0 $Y=-240
X1 4 5 6 7 8 WBL_Driver_1bit $T=3200 0 0 0 $X=3200 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11 12 13 14
** N=14 EP=14 IP=16 FDC=72
X0 1 2 3 4 5 6 13 14 ICV_1 $T=0 0 0 0 $X=0 $Y=-240
X1 7 8 9 10 11 12 13 14 ICV_1 $T=6400 0 0 0 $X=6400 $Y=-240
.ENDS
***************************************
.SUBCKT WBL_Driver Write_BL<0> WBLB<0> WBL<0> Write_BL<1> WBLB<1> WBL<1> Write_BL<2> WBLB<2> WBL<2> Write_BL<3> WBLB<3> WBL<3> Write_BL<4> WBLB<4> WBL<4> Write_BL<5> WBLB<5> WBL<5> Write_BL<6> WBLB<6>
+ WBL<6> Write_BL<7> WBLB<7> WBL<7> Write_BL<8> WBLB<8> WBL<8> Write_BL<9> WBLB<9> WBL<9> Write_BL<10> WBLB<10> WBL<10> Write_BL<11> WBLB<11> WBL<11> Write_BL<12> WBLB<12> WBL<12> Write_BL<13>
+ WBLB<13> WBL<13> Write_BL<14> WBLB<14> WBL<14> Write_BL<15> WBLB<15> WBL<15> VSS! VDD!
** N=51 EP=50 IP=56 FDC=288
X0 Write_BL<0> WBLB<0> WBL<0> Write_BL<1> WBLB<1> WBL<1> Write_BL<2> WBLB<2> WBL<2> Write_BL<3> WBLB<3> WBL<3> VSS! VDD! ICV_2 $T=0 0 0 0 $X=0 $Y=-240
X1 Write_BL<4> WBLB<4> WBL<4> Write_BL<5> WBLB<5> WBL<5> Write_BL<6> WBLB<6> WBL<6> Write_BL<7> WBLB<7> WBL<7> VSS! VDD! ICV_2 $T=12800 0 0 0 $X=12800 $Y=-240
X2 Write_BL<8> WBLB<8> WBL<8> Write_BL<9> WBLB<9> WBL<9> Write_BL<10> WBLB<10> WBL<10> Write_BL<11> WBLB<11> WBL<11> VSS! VDD! ICV_2 $T=25600 0 0 0 $X=25600 $Y=-240
X3 Write_BL<12> WBLB<12> WBL<12> Write_BL<13> WBLB<13> WBL<13> Write_BL<14> WBLB<14> WBL<14> Write_BL<15> WBLB<15> WBL<15> VSS! VDD! ICV_2 $T=38400 0 0 0 $X=38400 $Y=-240
.ENDS
***************************************
