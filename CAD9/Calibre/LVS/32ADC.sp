* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT ADC3 CLK Vin Vref OUT VSS! VDD!
** N=81 EP=6 IP=0 FDC=13
M0 VSS! 8 OUT VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=580 $Y=490 $D=97
M1 11 Vin 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1660 $Y=490 $D=97
M2 7 9 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=490 $D=97
M3 7 CLK VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3260 $Y=830 $D=97
M4 12 8 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4460 $Y=490 $D=97
M5 9 Vref 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5020 $Y=490 $D=97
M6 10 9 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6100 $Y=490 $D=97
M7 VDD! 8 OUT VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=580 $Y=2240 $D=189
M8 8 CLK VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1660 $Y=2240 $D=189
M9 VDD! 9 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=2240 $D=189
M10 9 8 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4460 $Y=2240 $D=189
M11 VDD! CLK 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5020 $Y=2240 $D=189
M12 10 9 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6100 $Y=2240 $D=189
.ENDS
***************************************
.SUBCKT ADC3_tp2 CLK Vin Vref OUT VSS! VDD!
** N=89 EP=6 IP=0 FDC=14
M0 VSS! 9 OUT VSS! nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=380 $Y=1330 $D=97
M1 10 8 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=1610 $D=97
M2 9 Vin 10 VSS! nfet L=5e-07 W=1.2e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=690 $D=97
M3 7 CLK VSS! VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=2660 $D=97
M4 VSS! CLK 7 VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=3260 $D=97
M5 11 Vref 8 VSS! nfet L=5e-07 W=1.2e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4280 $Y=680 $D=97
M6 7 9 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5220 $Y=1610 $D=97
M7 12 8 VSS! VSS! nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6300 $Y=1330 $D=97
M8 VDD! 9 OUT VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=380 $Y=4010 $D=189
M9 VDD! 8 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=4010 $D=189
M10 9 CLK VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=4010 $D=189
M11 VDD! CLK 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4660 $Y=4010 $D=189
M12 8 9 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5220 $Y=4010 $D=189
M13 12 8 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6300 $Y=4010 $D=189
.ENDS
***************************************
.SUBCKT ADC CLK Vin Vref OUT VSS! VDD!
** N=86 EP=6 IP=0 FDC=15
M0 VSS! 8 OUT VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=490 $D=97
M1 9 8 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1660 $Y=490 $D=97
M2 10 8 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=490 $D=97
M3 10 CLK VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3260 $Y=770 $D=97
M4 11 7 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4460 $Y=490 $D=97
M5 8 7 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5020 $Y=490 $D=97
M6 12 7 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6140 $Y=490 $D=97
M7 VDD! 8 OUT VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=3040 $D=189
M8 7 Vin 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1660 $Y=3040 $D=189
M9 VDD! 8 7 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=3040 $D=189
M10 7 CLK VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2780 $Y=3040 $D=189
M11 VDD! CLK 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3900 $Y=3040 $D=189
M12 8 7 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4460 $Y=3040 $D=189
M13 11 Vref 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5020 $Y=3040 $D=189
M14 12 7 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6140 $Y=3040 $D=189
.ENDS
***************************************
.SUBCKT 32ADC CLK Vin VSS! Vref<13> Vref<14> Vref<15> Vref<16> Vref<17> Vref<18> Vref<19> Vref<20> Vref<21> Vref<22> Vref<23> Vref<24> Vref<25> Vref<26> Vref<27> Vref<28> Vref<29>
+ Vref<30> Vref<31> Vref<10> Vref<11> Vref<12> Vref<0> Vref<1> Vref<2> Vref<3> Vref<4> Vref<5> Vref<6> Vref<7> Vref<8> Vref<9> VDD! OUT<13> OUT<14> OUT<15> OUT<16>
+ OUT<17> OUT<18> OUT<19> OUT<20> OUT<21> OUT<22> OUT<23> OUT<24> OUT<25> OUT<26> OUT<27> OUT<28> OUT<29> OUT<30> OUT<31> OUT<10> OUT<11> OUT<12> OUT<0> OUT<1>
+ OUT<2> OUT<3> OUT<4> OUT<5> OUT<6> OUT<7> OUT<8> OUT<9>
** N=3081 EP=68 IP=192 FDC=439
X0 CLK Vin Vref<13> OUT<13> VSS! VDD! ADC3 $T=99200 6800 1 270 $X=95720 $Y=-310
X1 CLK Vin Vref<14> OUT<14> VSS! VDD! ADC3 $T=102400 6800 0 270 $X=102140 $Y=-310
X2 CLK Vin Vref<15> OUT<15> VSS! VDD! ADC3 $T=112000 6800 1 270 $X=108520 $Y=-310
X3 CLK Vin Vref<16> OUT<16> VSS! VDD! ADC3 $T=115200 6800 0 270 $X=114940 $Y=-310
X4 CLK Vin Vref<17> OUT<17> VSS! VDD! ADC3 $T=124800 6800 1 270 $X=121320 $Y=-310
X5 CLK Vin Vref<18> OUT<18> VSS! VDD! ADC3 $T=128000 6800 0 270 $X=127740 $Y=-310
X6 CLK Vin Vref<19> OUT<19> VSS! VDD! ADC3 $T=137600 6800 1 270 $X=134120 $Y=-310
X7 CLK Vin Vref<20> OUT<20> VSS! VDD! ADC3 $T=140800 6800 0 270 $X=140540 $Y=-310
X8 CLK Vin Vref<21> OUT<21> VSS! VDD! ADC3 $T=150400 6800 1 270 $X=146920 $Y=-310
X9 CLK Vin Vref<22> OUT<22> VSS! VDD! ADC3 $T=153600 6800 0 270 $X=153340 $Y=-310
X10 CLK Vin Vref<23> OUT<23> VSS! VDD! ADC3 $T=163200 6800 1 270 $X=159720 $Y=-310
X11 CLK Vin Vref<24> OUT<24> VSS! VDD! ADC3 $T=166400 6800 0 270 $X=166140 $Y=-310
X12 CLK Vin Vref<25> OUT<25> VSS! VDD! ADC3 $T=176000 6800 1 270 $X=172520 $Y=-310
X13 CLK Vin Vref<26> OUT<26> VSS! VDD! ADC3 $T=179200 6800 0 270 $X=178940 $Y=-310
X14 CLK Vin Vref<27> OUT<27> VSS! VDD! ADC3 $T=188800 6800 1 270 $X=185320 $Y=-310
X15 CLK Vin Vref<28> OUT<28> VSS! VDD! ADC3 $T=192000 6800 0 270 $X=191740 $Y=-310
X16 CLK Vin Vref<29> OUT<29> VSS! VDD! ADC3 $T=201600 6800 1 270 $X=198120 $Y=-310
X17 CLK Vin Vref<30> OUT<30> VSS! VDD! ADC3 $T=204800 6800 0 270 $X=204540 $Y=-310
X18 CLK Vin Vref<31> OUT<31> VSS! VDD! ADC3 $T=214400 6800 1 270 $X=210920 $Y=-310
X19 CLK Vin Vref<10> OUT<10> VSS! VDD! ADC3_tp2 $T=72000 6800 0 270 $X=71540 $Y=-500
X20 CLK Vin Vref<11> OUT<11> VSS! VDD! ADC3_tp2 $T=84800 6800 1 270 $X=79710 $Y=-500
X21 CLK Vin Vref<12> OUT<12> VSS! VDD! ADC3_tp2 $T=88000 6800 0 270 $X=87540 $Y=-500
X22 CLK Vin Vref<0> OUT<0> VSS! VDD! ADC $T=0 6800 0 270 $X=-400 $Y=-770
X23 CLK Vin Vref<1> OUT<1> VSS! VDD! ADC $T=11200 6800 1 270 $X=6710 $Y=-770
X24 CLK Vin Vref<2> OUT<2> VSS! VDD! ADC $T=14400 6800 0 270 $X=14000 $Y=-770
X25 CLK Vin Vref<3> OUT<3> VSS! VDD! ADC $T=25600 6800 1 270 $X=21110 $Y=-770
X26 CLK Vin Vref<4> OUT<4> VSS! VDD! ADC $T=28800 6800 0 270 $X=28400 $Y=-770
X27 CLK Vin Vref<5> OUT<5> VSS! VDD! ADC $T=40000 6800 1 270 $X=35510 $Y=-770
X28 CLK Vin Vref<6> OUT<6> VSS! VDD! ADC $T=43200 6800 0 270 $X=42800 $Y=-770
X29 CLK Vin Vref<7> OUT<7> VSS! VDD! ADC $T=54400 6800 1 270 $X=49910 $Y=-770
X30 CLK Vin Vref<8> OUT<8> VSS! VDD! ADC $T=57600 6800 0 270 $X=57200 $Y=-770
X31 CLK Vin Vref<9> OUT<9> VSS! VDD! ADC $T=68800 6800 1 270 $X=64310 $Y=-770
.ENDS
***************************************
