* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT select_buffer2 SH1_BAR SH1 VSS! SH<1> VDD!
** N=53 EP=5 IP=0 FDC=12
M0 SH<1> SH1_BAR VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=450 $D=97
M1 SH1_BAR SH1 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=700 $Y=4800 $D=97
M2 VSS! SH1_BAR SH<1> VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=450 $D=97
M3 VSS! SH1 SH1_BAR VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=4800 $D=97
M4 SH<1> SH1_BAR VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1380 $Y=450 $D=97
M5 VSS! SH1_BAR SH<1> VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=450 $D=97
M6 SH<1> SH1_BAR VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=1490 $D=189
M7 SH1_BAR SH1 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=700 $Y=3310 $D=189
M8 VDD! SH1_BAR SH<1> VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=1490 $D=189
M9 VDD! SH1 SH1_BAR VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=3310 $D=189
M10 SH<1> SH1_BAR VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1380 $Y=1490 $D=189
M11 VDD! SH1_BAR SH<1> VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=1490 $D=189
.ENDS
***************************************
