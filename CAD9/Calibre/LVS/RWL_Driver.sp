* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT RWL_Driver_1bit RWLN RWLBP RWLP RWLBN RWL_N RWLB_N RWLB_P RWL_P VSS! VDD!
** N=155 EP=10 IP=0 FDC=48
M0 RWLN 12 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 12 RWL_N VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=4730 $D=97
M2 VSS! 12 RWLN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=450 $D=97
M3 VSS! RWL_N 12 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=4730 $D=97
M4 RWLN 12 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=450 $D=97
M5 RWLBN 15 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=4730 $D=97
M6 VSS! 12 RWLN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=450 $D=97
M7 VSS! 15 RWLBN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=4730 $D=97
M8 15 RWLB_N VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=450 $D=97
M9 RWLBN 15 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=4730 $D=97
M10 VSS! RWLB_N 15 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=450 $D=97
M11 VSS! 15 RWLBN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=4730 $D=97
M12 RWLBP 13 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=450 $D=97
M13 13 RWLB_P VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=4730 $D=97
M14 VSS! 13 RWLBP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=450 $D=97
M15 VSS! RWLB_P 13 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=4730 $D=97
M16 RWLBP 13 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=450 $D=97
M17 RWLP 14 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=4730 $D=97
M18 VSS! 13 RWLBP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=450 $D=97
M19 VSS! 14 RWLP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=4730 $D=97
M20 14 RWL_P VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=450 $D=97
M21 RWLP 14 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=4730 $D=97
M22 VSS! RWL_P 14 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=450 $D=97
M23 VSS! 14 RWLP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=4730 $D=97
M24 RWLN 12 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=1490 $D=189
M25 12 RWL_N VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M26 VDD! 12 RWLN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=1490 $D=189
M27 VDD! RWL_N 12 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=3310 $D=189
M28 RWLN 12 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=1490 $D=189
M29 RWLBN 15 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=3310 $D=189
M30 VDD! 12 RWLN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=1490 $D=189
M31 VDD! 15 RWLBN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=3310 $D=189
M32 15 RWLB_N VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=1480 $D=189
M33 RWLBN 15 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=3310 $D=189
M34 VDD! RWLB_N 15 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=1480 $D=189
M35 VDD! 15 RWLBN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=3310 $D=189
M36 RWLBP 13 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=1490 $D=189
M37 13 RWLB_P VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=3310 $D=189
M38 VDD! 13 RWLBP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=1490 $D=189
M39 VDD! RWLB_P 13 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=3310 $D=189
M40 RWLBP 13 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=1490 $D=189
M41 RWLP 14 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=3310 $D=189
M42 VDD! 13 RWLBP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=1490 $D=189
M43 VDD! 14 RWLP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=3310 $D=189
M44 14 RWL_P VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=1480 $D=189
M45 RWLP 14 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=3310 $D=189
M46 VDD! RWL_P 14 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=1480 $D=189
M47 VDD! 14 RWLP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
** N=19 EP=18 IP=22 FDC=96
X0 1 2 3 4 5 6 7 8 18 17 RWL_Driver_1bit $T=0 0 0 0 $X=-60 $Y=-240
X1 9 10 11 12 13 14 15 16 18 17 RWL_Driver_1bit $T=0 5600 0 0 $X=-60 $Y=5360
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34
** N=35 EP=34 IP=38 FDC=192
X0 1 2 3 4 9 11 12 15 5 6 7 8 10 13 14 16 34 33 ICV_1 $T=0 0 0 0 $X=-60 $Y=-240
X1 17 18 19 20 25 27 28 31 21 22 23 24 26 29 30 32 34 33 ICV_1 $T=0 11200 0 0 $X=-60 $Y=10960
.ENDS
***************************************
.SUBCKT RWL_Driver RWLN<15> RWLBP<15> RWLP<15> RWLBN<15> RWLN<14> RWLBP<14> RWLP<14> RWLBN<14> RWLN<13> RWLBP<13> RWLP<13> RWLBN<13> RWLN<12> RWLBP<12> RWLP<12> RWLBN<12> RWL_N<15> RWL_N<14> RWL_N<13> RWL_N<12>
+ RWLB_N<15> RWLB_P<15> RWLB_N<14> RWLB_P<14> RWLB_N<13> RWLB_P<13> RWLB_N<12> RWLB_P<12> RWL_P<15> RWL_P<14> RWL_P<13> RWL_P<12> RWLN<11> RWLBP<11> RWLP<11> RWLBN<11> RWLN<10> RWLBP<10> RWLP<10> RWLBN<10>
+ RWLN<9> RWLBP<9> RWLP<9> RWLBN<9> RWLN<8> RWLBP<8> RWLP<8> RWLBN<8> RWL_N<11> RWL_N<10> RWL_N<9> RWL_N<8> RWLB_N<11> RWLB_P<11> RWLB_N<10> RWLB_P<10> RWLB_N<9> RWLB_P<9> RWLB_N<8> RWLB_P<8>
+ RWL_P<11> RWL_P<10> RWL_P<9> RWL_P<8> RWLN<7> RWLBP<7> RWLP<7> RWLBN<7> RWLN<6> RWLBP<6> RWLP<6> RWLBN<6> RWLN<5> RWLBP<5> RWLP<5> RWLBN<5> RWLN<4> RWLBP<4> RWLP<4> RWLBN<4>
+ RWL_N<7> RWL_N<6> RWL_N<5> RWL_N<4> RWLB_N<7> RWLB_P<7> RWLB_N<6> RWLB_P<6> RWLB_N<5> RWLB_P<5> RWLB_N<4> RWLB_P<4> RWL_P<7> RWL_P<6> RWL_P<5> RWL_P<4> RWLN<3> RWLBP<3> RWLP<3> RWLBN<3>
+ RWLN<2> RWLBP<2> RWLP<2> RWLBN<2> RWLN<1> RWLBP<1> RWLP<1> RWLBN<1> RWLN<0> RWLBP<0> RWLP<0> RWLBN<0> RWL_N<3> RWL_N<2> RWL_N<1> RWL_N<0> RWLB_N<3> RWLB_P<3> RWLB_N<2> RWLB_P<2>
+ RWLB_N<1> RWLB_P<1> RWLB_N<0> RWLB_P<0> RWL_P<3> RWL_P<2> RWL_P<1> RWL_P<0> VSS! VDD!
** N=131 EP=130 IP=140 FDC=768
X0 RWLN<15> RWLBP<15> RWLP<15> RWLBN<15> RWLN<14> RWLBP<14> RWLP<14> RWLBN<14> RWL_N<15> RWL_N<14> RWLB_N<15> RWLB_P<15> RWLB_N<14> RWLB_P<14> RWL_P<15> RWL_P<14> RWLN<13> RWLBP<13> RWLP<13> RWLBN<13>
+ RWLN<12> RWLBP<12> RWLP<12> RWLBN<12> RWL_N<13> RWL_N<12> RWLB_N<13> RWLB_P<13> RWLB_N<12> RWLB_P<12> RWL_P<13> RWL_P<12> VSS! VDD!
+ ICV_2 $T=0 0 0 0 $X=-60 $Y=-240
X1 RWLN<11> RWLBP<11> RWLP<11> RWLBN<11> RWLN<10> RWLBP<10> RWLP<10> RWLBN<10> RWL_N<11> RWL_N<10> RWLB_N<11> RWLB_P<11> RWLB_N<10> RWLB_P<10> RWL_P<11> RWL_P<10> RWLN<9> RWLBP<9> RWLP<9> RWLBN<9>
+ RWLN<8> RWLBP<8> RWLP<8> RWLBN<8> RWL_N<9> RWL_N<8> RWLB_N<9> RWLB_P<9> RWLB_N<8> RWLB_P<8> RWL_P<9> RWL_P<8> VSS! VDD!
+ ICV_2 $T=0 22400 0 0 $X=-60 $Y=22160
X2 RWLN<7> RWLBP<7> RWLP<7> RWLBN<7> RWLN<6> RWLBP<6> RWLP<6> RWLBN<6> RWL_N<7> RWL_N<6> RWLB_N<7> RWLB_P<7> RWLB_N<6> RWLB_P<6> RWL_P<7> RWL_P<6> RWLN<5> RWLBP<5> RWLP<5> RWLBN<5>
+ RWLN<4> RWLBP<4> RWLP<4> RWLBN<4> RWL_N<5> RWL_N<4> RWLB_N<5> RWLB_P<5> RWLB_N<4> RWLB_P<4> RWL_P<5> RWL_P<4> VSS! VDD!
+ ICV_2 $T=0 44800 0 0 $X=-60 $Y=44560
X3 RWLN<3> RWLBP<3> RWLP<3> RWLBN<3> RWLN<2> RWLBP<2> RWLP<2> RWLBN<2> RWL_N<3> RWL_N<2> RWLB_N<3> RWLB_P<3> RWLB_N<2> RWLB_P<2> RWL_P<3> RWL_P<2> RWLN<1> RWLBP<1> RWLP<1> RWLBN<1>
+ RWLN<0> RWLBP<0> RWLP<0> RWLBN<0> RWL_N<1> RWL_N<0> RWLB_N<1> RWLB_P<1> RWLB_N<0> RWLB_P<0> RWL_P<1> RWL_P<0> VSS! VDD!
+ ICV_2 $T=0 67200 0 0 $X=-60 $Y=66960
.ENDS
***************************************
