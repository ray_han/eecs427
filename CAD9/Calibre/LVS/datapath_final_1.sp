* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT ADC CLK Vin Vref OUT VSS! VDD!
** N=86 EP=6 IP=0 FDC=15
M0 VSS! 8 OUT VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=490 $D=97
M1 9 8 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1660 $Y=490 $D=97
M2 10 8 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=490 $D=97
M3 10 CLK VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3260 $Y=770 $D=97
M4 11 7 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4460 $Y=490 $D=97
M5 8 7 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5020 $Y=490 $D=97
M6 12 7 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6140 $Y=490 $D=97
M7 VDD! 8 OUT VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=3040 $D=189
M8 7 Vin 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1660 $Y=3040 $D=189
M9 VDD! 8 7 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=3040 $D=189
M10 7 CLK VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2780 $Y=3040 $D=189
M11 VDD! CLK 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3900 $Y=3040 $D=189
M12 8 7 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4460 $Y=3040 $D=189
M13 11 Vref 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5020 $Y=3040 $D=189
M14 12 7 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6140 $Y=3040 $D=189
.ENDS
***************************************
.SUBCKT ADC3_tp2 CLK Vin Vref OUT VSS! VDD!
** N=89 EP=6 IP=0 FDC=14
M0 VSS! 9 OUT VSS! nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=380 $Y=1330 $D=97
M1 10 8 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=1610 $D=97
M2 9 Vin 10 VSS! nfet L=5e-07 W=1.2e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=690 $D=97
M3 7 CLK VSS! VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=2660 $D=97
M4 VSS! CLK 7 VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=3260 $D=97
M5 11 Vref 8 VSS! nfet L=5e-07 W=1.2e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4280 $Y=680 $D=97
M6 7 9 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5220 $Y=1610 $D=97
M7 12 8 VSS! VSS! nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6300 $Y=1330 $D=97
M8 VDD! 9 OUT VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=380 $Y=4010 $D=189
M9 VDD! 8 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=4010 $D=189
M10 9 CLK VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=4010 $D=189
M11 VDD! CLK 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4660 $Y=4010 $D=189
M12 8 9 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5220 $Y=4010 $D=189
M13 12 8 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6300 $Y=4010 $D=189
.ENDS
***************************************
.SUBCKT ADC3 CLK Vin Vref OUT VSS! VDD!
** N=81 EP=6 IP=0 FDC=13
M0 VSS! 8 OUT VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=580 $Y=490 $D=97
M1 11 Vin 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1660 $Y=490 $D=97
M2 7 9 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=490 $D=97
M3 7 CLK VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3260 $Y=830 $D=97
M4 12 8 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4460 $Y=490 $D=97
M5 9 Vref 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5020 $Y=490 $D=97
M6 10 9 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6100 $Y=490 $D=97
M7 VDD! 8 OUT VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=580 $Y=2240 $D=189
M8 8 CLK VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1660 $Y=2240 $D=189
M9 VDD! 9 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=2240 $D=189
M10 9 8 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4460 $Y=2240 $D=189
M11 VDD! CLK 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5020 $Y=2240 $D=189
M12 10 9 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6100 $Y=2240 $D=189
.ENDS
***************************************
.SUBCKT 32ADC CLK Vin VSS! Vref<0> Vref<1> Vref<2> Vref<3> Vref<4> Vref<5> Vref<6> Vref<7> Vref<8> Vref<9> Vref<10> Vref<11> Vref<12> Vref<13> Vref<14> Vref<15> Vref<16>
+ Vref<17> Vref<18> Vref<19> Vref<20> Vref<21> Vref<22> Vref<23> Vref<24> Vref<25> Vref<26> Vref<27> Vref<28> Vref<29> Vref<30> Vref<31> VDD! OUT<0> OUT<1> OUT<2> OUT<3>
+ OUT<4> OUT<5> OUT<6> OUT<7> OUT<8> OUT<9> OUT<10> OUT<11> OUT<12> OUT<13> OUT<14> OUT<15> OUT<16> OUT<17> OUT<18> OUT<19> OUT<20> OUT<21> OUT<22> OUT<23>
+ OUT<24> OUT<25> OUT<26> OUT<27> OUT<28> OUT<29> OUT<30> OUT<31>
** N=3080 EP=68 IP=192 FDC=439
X0 CLK Vin Vref<0> OUT<0> VSS! VDD! ADC $T=0 6800 0 270 $X=-400 $Y=-770
X1 CLK Vin Vref<1> OUT<1> VSS! VDD! ADC $T=11200 6800 1 270 $X=6710 $Y=-770
X2 CLK Vin Vref<2> OUT<2> VSS! VDD! ADC $T=14400 6800 0 270 $X=14000 $Y=-770
X3 CLK Vin Vref<3> OUT<3> VSS! VDD! ADC $T=25600 6800 1 270 $X=21110 $Y=-770
X4 CLK Vin Vref<4> OUT<4> VSS! VDD! ADC $T=28800 6800 0 270 $X=28400 $Y=-770
X5 CLK Vin Vref<5> OUT<5> VSS! VDD! ADC $T=40000 6800 1 270 $X=35510 $Y=-770
X6 CLK Vin Vref<6> OUT<6> VSS! VDD! ADC $T=43200 6800 0 270 $X=42800 $Y=-770
X7 CLK Vin Vref<7> OUT<7> VSS! VDD! ADC $T=54400 6800 1 270 $X=49910 $Y=-770
X8 CLK Vin Vref<8> OUT<8> VSS! VDD! ADC $T=57600 6800 0 270 $X=57200 $Y=-770
X9 CLK Vin Vref<9> OUT<9> VSS! VDD! ADC $T=68800 6800 1 270 $X=64310 $Y=-770
X10 CLK Vin Vref<10> OUT<10> VSS! VDD! ADC3_tp2 $T=72000 6800 0 270 $X=71540 $Y=-500
X11 CLK Vin Vref<11> OUT<11> VSS! VDD! ADC3_tp2 $T=84800 6800 1 270 $X=79710 $Y=-500
X12 CLK Vin Vref<12> OUT<12> VSS! VDD! ADC3_tp2 $T=88000 6800 0 270 $X=87540 $Y=-500
X13 CLK Vin Vref<13> OUT<13> VSS! VDD! ADC3 $T=99200 6800 1 270 $X=95720 $Y=-310
X14 CLK Vin Vref<14> OUT<14> VSS! VDD! ADC3 $T=102400 6800 0 270 $X=102140 $Y=-310
X15 CLK Vin Vref<15> OUT<15> VSS! VDD! ADC3 $T=112000 6800 1 270 $X=108520 $Y=-310
X16 CLK Vin Vref<16> OUT<16> VSS! VDD! ADC3 $T=115200 6800 0 270 $X=114940 $Y=-310
X17 CLK Vin Vref<17> OUT<17> VSS! VDD! ADC3 $T=124800 6800 1 270 $X=121320 $Y=-310
X18 CLK Vin Vref<18> OUT<18> VSS! VDD! ADC3 $T=128000 6800 0 270 $X=127740 $Y=-310
X19 CLK Vin Vref<19> OUT<19> VSS! VDD! ADC3 $T=137600 6800 1 270 $X=134120 $Y=-310
X20 CLK Vin Vref<20> OUT<20> VSS! VDD! ADC3 $T=140800 6800 0 270 $X=140540 $Y=-310
X21 CLK Vin Vref<21> OUT<21> VSS! VDD! ADC3 $T=150400 6800 1 270 $X=146920 $Y=-310
X22 CLK Vin Vref<22> OUT<22> VSS! VDD! ADC3 $T=153600 6800 0 270 $X=153340 $Y=-310
X23 CLK Vin Vref<23> OUT<23> VSS! VDD! ADC3 $T=163200 6800 1 270 $X=159720 $Y=-310
X24 CLK Vin Vref<24> OUT<24> VSS! VDD! ADC3 $T=166400 6800 0 270 $X=166140 $Y=-310
X25 CLK Vin Vref<25> OUT<25> VSS! VDD! ADC3 $T=176000 6800 1 270 $X=172520 $Y=-310
X26 CLK Vin Vref<26> OUT<26> VSS! VDD! ADC3 $T=179200 6800 0 270 $X=178940 $Y=-310
X27 CLK Vin Vref<27> OUT<27> VSS! VDD! ADC3 $T=188800 6800 1 270 $X=185320 $Y=-310
X28 CLK Vin Vref<28> OUT<28> VSS! VDD! ADC3 $T=192000 6800 0 270 $X=191740 $Y=-310
X29 CLK Vin Vref<29> OUT<29> VSS! VDD! ADC3 $T=201600 6800 1 270 $X=198120 $Y=-310
X30 CLK Vin Vref<30> OUT<30> VSS! VDD! ADC3 $T=204800 6800 0 270 $X=204540 $Y=-310
X31 CLK Vin Vref<31> OUT<31> VSS! VDD! ADC3 $T=214400 6800 1 270 $X=210920 $Y=-310
.ENDS
***************************************
.SUBCKT ADC_Mux RBL<0> Vin RBL<1> RBL<2> RBL<3> RBL<4> RBL<5> RBL<6> RBL<7> RBL<8> RBL<9> RBL<10> RBL<11> RBL<12> RBL<13> RBL<14> RBL<15> Col_Sel<0> Col_Sel<1> Col_Sel<2>
+ Col_Sel<3> Col_Sel<4> Col_Sel<5> Col_Sel<6> Col_Sel<7> Col_Sel<8> Col_Sel<9> Col_Sel<10> Col_Sel<11> Col_Sel<12> Col_Sel<13> Col_Sel<14> Col_Sel<15> VSS! VDD!
** N=691 EP=35 IP=0 FDC=128
M0 Vin Col_Sel<0> RBL<0> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=710 $D=97
M1 RBL<0> Col_Sel<0> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1010 $Y=710 $D=97
M2 Vin Col_Sel<0> RBL<0> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=710 $D=97
M3 VSS! Col_Sel<0> 36 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2620 $Y=1430 $D=97
M4 Vin Col_Sel<1> RBL<1> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3650 $Y=710 $D=97
M5 RBL<1> Col_Sel<1> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=710 $D=97
M6 Vin Col_Sel<1> RBL<1> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4770 $Y=710 $D=97
M7 VSS! Col_Sel<1> 37 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5820 $Y=1430 $D=97
M8 Vin Col_Sel<2> RBL<2> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6850 $Y=710 $D=97
M9 RBL<2> Col_Sel<2> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7410 $Y=710 $D=97
M10 Vin Col_Sel<2> RBL<2> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7970 $Y=710 $D=97
M11 VSS! Col_Sel<2> 38 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9020 $Y=1430 $D=97
M12 Vin Col_Sel<3> RBL<3> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10050 $Y=710 $D=97
M13 RBL<3> Col_Sel<3> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10610 $Y=710 $D=97
M14 Vin Col_Sel<3> RBL<3> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11170 $Y=710 $D=97
M15 VSS! Col_Sel<3> 39 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12220 $Y=1430 $D=97
M16 Vin Col_Sel<4> RBL<4> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13250 $Y=710 $D=97
M17 RBL<4> Col_Sel<4> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13810 $Y=710 $D=97
M18 Vin Col_Sel<4> RBL<4> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14370 $Y=710 $D=97
M19 VSS! Col_Sel<4> 40 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=15420 $Y=1430 $D=97
M20 Vin Col_Sel<5> RBL<5> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=16450 $Y=710 $D=97
M21 RBL<5> Col_Sel<5> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17010 $Y=710 $D=97
M22 Vin Col_Sel<5> RBL<5> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17570 $Y=710 $D=97
M23 VSS! Col_Sel<5> 41 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=18620 $Y=1430 $D=97
M24 Vin Col_Sel<6> RBL<6> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=19650 $Y=710 $D=97
M25 RBL<6> Col_Sel<6> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20210 $Y=710 $D=97
M26 Vin Col_Sel<6> RBL<6> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20770 $Y=710 $D=97
M27 VSS! Col_Sel<6> 42 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=21820 $Y=1430 $D=97
M28 Vin Col_Sel<7> RBL<7> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=22850 $Y=710 $D=97
M29 RBL<7> Col_Sel<7> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23410 $Y=710 $D=97
M30 Vin Col_Sel<7> RBL<7> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23970 $Y=710 $D=97
M31 VSS! Col_Sel<7> 43 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=25020 $Y=1430 $D=97
M32 Vin Col_Sel<8> RBL<8> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26050 $Y=710 $D=97
M33 RBL<8> Col_Sel<8> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26610 $Y=710 $D=97
M34 Vin Col_Sel<8> RBL<8> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=27170 $Y=710 $D=97
M35 VSS! Col_Sel<8> 44 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=28220 $Y=1430 $D=97
M36 Vin Col_Sel<9> RBL<9> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29250 $Y=710 $D=97
M37 RBL<9> Col_Sel<9> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29810 $Y=710 $D=97
M38 Vin Col_Sel<9> RBL<9> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=30370 $Y=710 $D=97
M39 VSS! Col_Sel<9> 45 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=31420 $Y=1430 $D=97
M40 Vin Col_Sel<10> RBL<10> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=32450 $Y=710 $D=97
M41 RBL<10> Col_Sel<10> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33010 $Y=710 $D=97
M42 Vin Col_Sel<10> RBL<10> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33570 $Y=710 $D=97
M43 VSS! Col_Sel<10> 46 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=34620 $Y=1430 $D=97
M44 Vin Col_Sel<11> RBL<11> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=35650 $Y=710 $D=97
M45 RBL<11> Col_Sel<11> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36210 $Y=710 $D=97
M46 Vin Col_Sel<11> RBL<11> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36770 $Y=710 $D=97
M47 VSS! Col_Sel<11> 47 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=37820 $Y=1430 $D=97
M48 Vin Col_Sel<12> RBL<12> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38850 $Y=710 $D=97
M49 RBL<12> Col_Sel<12> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39410 $Y=710 $D=97
M50 Vin Col_Sel<12> RBL<12> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39970 $Y=710 $D=97
M51 VSS! Col_Sel<12> 48 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=41020 $Y=1430 $D=97
M52 Vin Col_Sel<13> RBL<13> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42050 $Y=710 $D=97
M53 RBL<13> Col_Sel<13> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42610 $Y=710 $D=97
M54 Vin Col_Sel<13> RBL<13> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=43170 $Y=710 $D=97
M55 VSS! Col_Sel<13> 49 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=44220 $Y=1430 $D=97
M56 Vin Col_Sel<14> RBL<14> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45250 $Y=710 $D=97
M57 RBL<14> Col_Sel<14> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45810 $Y=710 $D=97
M58 Vin Col_Sel<14> RBL<14> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=46370 $Y=710 $D=97
M59 VSS! Col_Sel<14> 50 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=47420 $Y=1430 $D=97
M60 Vin Col_Sel<15> RBL<15> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=48450 $Y=710 $D=97
M61 RBL<15> Col_Sel<15> Vin VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49010 $Y=710 $D=97
M62 Vin Col_Sel<15> RBL<15> VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49570 $Y=710 $D=97
M63 VSS! Col_Sel<15> 51 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=50620 $Y=1430 $D=97
M64 Vin 36 RBL<0> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2890 $D=189
M65 RBL<0> 36 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1010 $Y=2890 $D=189
M66 Vin 36 RBL<0> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=2890 $D=189
M67 VDD! Col_Sel<0> 36 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2620 $Y=2890 $D=189
M68 Vin 37 RBL<1> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3650 $Y=2890 $D=189
M69 RBL<1> 37 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=2890 $D=189
M70 Vin 37 RBL<1> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4770 $Y=2890 $D=189
M71 VDD! Col_Sel<1> 37 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5820 $Y=2890 $D=189
M72 Vin 38 RBL<2> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6850 $Y=2890 $D=189
M73 RBL<2> 38 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7410 $Y=2890 $D=189
M74 Vin 38 RBL<2> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7970 $Y=2890 $D=189
M75 VDD! Col_Sel<2> 38 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9020 $Y=2890 $D=189
M76 Vin 39 RBL<3> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10050 $Y=2890 $D=189
M77 RBL<3> 39 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10610 $Y=2890 $D=189
M78 Vin 39 RBL<3> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11170 $Y=2890 $D=189
M79 VDD! Col_Sel<3> 39 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12220 $Y=2890 $D=189
M80 Vin 40 RBL<4> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13250 $Y=2890 $D=189
M81 RBL<4> 40 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13810 $Y=2890 $D=189
M82 Vin 40 RBL<4> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14370 $Y=2890 $D=189
M83 VDD! Col_Sel<4> 40 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=15420 $Y=2890 $D=189
M84 Vin 41 RBL<5> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=16450 $Y=2890 $D=189
M85 RBL<5> 41 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17010 $Y=2890 $D=189
M86 Vin 41 RBL<5> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17570 $Y=2890 $D=189
M87 VDD! Col_Sel<5> 41 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=18620 $Y=2890 $D=189
M88 Vin 42 RBL<6> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=19650 $Y=2890 $D=189
M89 RBL<6> 42 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20210 $Y=2890 $D=189
M90 Vin 42 RBL<6> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20770 $Y=2890 $D=189
M91 VDD! Col_Sel<6> 42 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=21820 $Y=2890 $D=189
M92 Vin 43 RBL<7> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=22850 $Y=2890 $D=189
M93 RBL<7> 43 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23410 $Y=2890 $D=189
M94 Vin 43 RBL<7> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23970 $Y=2890 $D=189
M95 VDD! Col_Sel<7> 43 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=25020 $Y=2890 $D=189
M96 Vin 44 RBL<8> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26050 $Y=2890 $D=189
M97 RBL<8> 44 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26610 $Y=2890 $D=189
M98 Vin 44 RBL<8> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=27170 $Y=2890 $D=189
M99 VDD! Col_Sel<8> 44 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=28220 $Y=2890 $D=189
M100 Vin 45 RBL<9> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29250 $Y=2890 $D=189
M101 RBL<9> 45 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29810 $Y=2890 $D=189
M102 Vin 45 RBL<9> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=30370 $Y=2890 $D=189
M103 VDD! Col_Sel<9> 45 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=31420 $Y=2890 $D=189
M104 Vin 46 RBL<10> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=32450 $Y=2890 $D=189
M105 RBL<10> 46 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33010 $Y=2890 $D=189
M106 Vin 46 RBL<10> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33570 $Y=2890 $D=189
M107 VDD! Col_Sel<10> 46 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=34620 $Y=2890 $D=189
M108 Vin 47 RBL<11> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=35650 $Y=2890 $D=189
M109 RBL<11> 47 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36210 $Y=2890 $D=189
M110 Vin 47 RBL<11> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36770 $Y=2890 $D=189
M111 VDD! Col_Sel<11> 47 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=37820 $Y=2890 $D=189
M112 Vin 48 RBL<12> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38850 $Y=2890 $D=189
M113 RBL<12> 48 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39410 $Y=2890 $D=189
M114 Vin 48 RBL<12> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39970 $Y=2890 $D=189
M115 VDD! Col_Sel<12> 48 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=41020 $Y=2890 $D=189
M116 Vin 49 RBL<13> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42050 $Y=2890 $D=189
M117 RBL<13> 49 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42610 $Y=2890 $D=189
M118 Vin 49 RBL<13> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=43170 $Y=2890 $D=189
M119 VDD! Col_Sel<13> 49 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=44220 $Y=2890 $D=189
M120 Vin 50 RBL<14> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45250 $Y=2890 $D=189
M121 RBL<14> 50 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45810 $Y=2890 $D=189
M122 Vin 50 RBL<14> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=46370 $Y=2890 $D=189
M123 VDD! Col_Sel<14> 50 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=47420 $Y=2890 $D=189
M124 Vin 51 RBL<15> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=48450 $Y=2890 $D=189
M125 RBL<15> 51 Vin VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49010 $Y=2890 $D=189
M126 Vin 51 RBL<15> VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49570 $Y=2890 $D=189
M127 VDD! Col_Sel<15> 51 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=50620 $Y=2890 $D=189
.ENDS
***************************************
.SUBCKT special_inverter IN<15> OUT<15> IN<14> OUT<14> IN<13> OUT<13> IN<12> OUT<12> IN<11> OUT<11> IN<10> OUT<10> IN<9> OUT<9> IN<8> OUT<8> IN<7> OUT<7> IN<6> OUT<6>
+ IN<5> OUT<5> IN<4> OUT<4> IN<3> OUT<3> IN<2> OUT<2> IN<1> OUT<1> IN<0> VDD! VSS! OUT<0>
** N=610 EP=34 IP=0 FDC=96
M0 35 IN<15> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=440 $D=97
M1 OUT<15> 35 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2270 $Y=440 $D=97
M2 36 IN<14> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4380 $Y=440 $D=97
M3 OUT<14> 36 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5470 $Y=440 $D=97
M4 37 IN<13> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7580 $Y=440 $D=97
M5 OUT<13> 37 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8670 $Y=440 $D=97
M6 38 IN<12> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10780 $Y=440 $D=97
M7 OUT<12> 38 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11870 $Y=440 $D=97
M8 39 IN<11> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13980 $Y=440 $D=97
M9 OUT<11> 39 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=15070 $Y=440 $D=97
M10 40 IN<10> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17180 $Y=440 $D=97
M11 OUT<10> 40 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=18270 $Y=440 $D=97
M12 41 IN<9> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20380 $Y=440 $D=97
M13 OUT<9> 41 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=21470 $Y=440 $D=97
M14 42 IN<8> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23580 $Y=440 $D=97
M15 OUT<8> 42 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=24670 $Y=440 $D=97
M16 43 IN<7> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26780 $Y=440 $D=97
M17 OUT<7> 43 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=27870 $Y=440 $D=97
M18 44 IN<6> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29980 $Y=440 $D=97
M19 OUT<6> 44 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=31070 $Y=440 $D=97
M20 45 IN<5> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33180 $Y=440 $D=97
M21 OUT<5> 45 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=34270 $Y=440 $D=97
M22 46 IN<4> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36380 $Y=440 $D=97
M23 OUT<4> 46 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=37470 $Y=440 $D=97
M24 47 IN<3> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39580 $Y=440 $D=97
M25 OUT<3> 47 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40670 $Y=440 $D=97
M26 48 IN<2> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42780 $Y=440 $D=97
M27 OUT<2> 48 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=43870 $Y=440 $D=97
M28 49 IN<1> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45980 $Y=440 $D=97
M29 OUT<1> 49 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=47070 $Y=440 $D=97
M30 50 IN<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49180 $Y=440 $D=97
M31 OUT<0> 50 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=50270 $Y=440 $D=97
M32 VDD! IN<15> 35 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=810 $Y=2690 $D=189
M33 35 IN<15> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1290 $Y=2690 $D=189
M34 VDD! IN<15> 35 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1770 $Y=2690 $D=189
M35 OUT<15> 35 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2270 $Y=3210 $D=189
M36 VDD! IN<14> 36 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4010 $Y=2690 $D=189
M37 36 IN<14> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4490 $Y=2690 $D=189
M38 VDD! IN<14> 36 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4970 $Y=2690 $D=189
M39 OUT<14> 36 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5470 $Y=3210 $D=189
M40 VDD! IN<13> 37 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=2690 $D=189
M41 37 IN<13> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=2690 $D=189
M42 VDD! IN<13> 37 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8170 $Y=2690 $D=189
M43 OUT<13> 37 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8670 $Y=3210 $D=189
M44 VDD! IN<12> 38 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10410 $Y=2690 $D=189
M45 38 IN<12> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10890 $Y=2690 $D=189
M46 VDD! IN<12> 38 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11370 $Y=2690 $D=189
M47 OUT<12> 38 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11870 $Y=3210 $D=189
M48 VDD! IN<11> 39 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13610 $Y=2690 $D=189
M49 39 IN<11> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14090 $Y=2690 $D=189
M50 VDD! IN<11> 39 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14570 $Y=2690 $D=189
M51 OUT<11> 39 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=15070 $Y=3210 $D=189
M52 VDD! IN<10> 40 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=16810 $Y=2690 $D=189
M53 40 IN<10> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17290 $Y=2690 $D=189
M54 VDD! IN<10> 40 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=17770 $Y=2690 $D=189
M55 OUT<10> 40 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=18270 $Y=3210 $D=189
M56 VDD! IN<9> 41 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20010 $Y=2690 $D=189
M57 41 IN<9> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20490 $Y=2690 $D=189
M58 VDD! IN<9> 41 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=20970 $Y=2690 $D=189
M59 OUT<9> 41 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=21470 $Y=3210 $D=189
M60 VDD! IN<8> 42 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23210 $Y=2690 $D=189
M61 42 IN<8> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=23690 $Y=2690 $D=189
M62 VDD! IN<8> 42 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=24170 $Y=2690 $D=189
M63 OUT<8> 42 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=24670 $Y=3210 $D=189
M64 VDD! IN<7> 43 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26410 $Y=2690 $D=189
M65 43 IN<7> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=26890 $Y=2690 $D=189
M66 VDD! IN<7> 43 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=27370 $Y=2690 $D=189
M67 OUT<7> 43 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=27870 $Y=3210 $D=189
M68 VDD! IN<6> 44 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=29610 $Y=2690 $D=189
M69 44 IN<6> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=30090 $Y=2690 $D=189
M70 VDD! IN<6> 44 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=30570 $Y=2690 $D=189
M71 OUT<6> 44 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=31070 $Y=3210 $D=189
M72 VDD! IN<5> 45 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=32810 $Y=2690 $D=189
M73 45 IN<5> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33290 $Y=2690 $D=189
M74 VDD! IN<5> 45 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=33770 $Y=2690 $D=189
M75 OUT<5> 45 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=34270 $Y=3210 $D=189
M76 VDD! IN<4> 46 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36010 $Y=2690 $D=189
M77 46 IN<4> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36490 $Y=2690 $D=189
M78 VDD! IN<4> 46 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=36970 $Y=2690 $D=189
M79 OUT<4> 46 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=37470 $Y=3210 $D=189
M80 VDD! IN<3> 47 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39210 $Y=2690 $D=189
M81 47 IN<3> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=39690 $Y=2690 $D=189
M82 VDD! IN<3> 47 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40170 $Y=2690 $D=189
M83 OUT<3> 47 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40670 $Y=3210 $D=189
M84 VDD! IN<2> 48 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42410 $Y=2690 $D=189
M85 48 IN<2> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=42890 $Y=2690 $D=189
M86 VDD! IN<2> 48 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=43370 $Y=2690 $D=189
M87 OUT<2> 48 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=43870 $Y=3210 $D=189
M88 VDD! IN<1> 49 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=45610 $Y=2690 $D=189
M89 49 IN<1> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=46090 $Y=2690 $D=189
M90 VDD! IN<1> 49 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=46570 $Y=2690 $D=189
M91 OUT<1> 49 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=47070 $Y=3210 $D=189
M92 VDD! IN<0> 50 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=48810 $Y=2690 $D=189
M93 50 IN<0> VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49290 $Y=2690 $D=189
M94 VDD! IN<0> 50 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=49770 $Y=2690 $D=189
M95 OUT<0> 50 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=50270 $Y=3210 $D=189
.ENDS
***************************************
.SUBCKT BITCELL RENB RWL_N WWL RWLB_P RWL_P RWLB_N REN RBL WBLB WBL VSS! VDD!
** N=72 EP=12 IP=0 FDC=12
M0 13 REN RBL VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=700 $Y=450 $D=97
M1 RWL_N 15 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=450 $D=97
M2 RWLB_N 16 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=4870 $D=97
M3 16 15 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2120 $Y=4870 $D=97
M4 15 WWL WBLB VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2240 $Y=450 $D=97
M5 WBL WWL 16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2600 $Y=4870 $D=97
M6 VSS! 16 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2720 $Y=450 $D=97
M7 14 RENB RBL VDD! pfet L=1.2e-07 W=8.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=700 $Y=3310 $D=189
M8 RWLB_P 15 14 VDD! pfet L=1.2e-07 W=8.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=1420 $D=189
M9 RWL_P 16 14 VDD! pfet L=1.2e-07 W=8.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1180 $Y=3310 $D=189
M10 16 15 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2120 $Y=3310 $D=189
M11 VDD! 16 15 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1750 $D=189
.ENDS
***************************************
.SUBCKT ICV_38 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
** N=17 EP=17 IP=24 FDC=24
X0 1 3 4 5 6 7 9 8 10 11 17 16 BITCELL $T=0 0 0 0 $X=0 $Y=-240
X1 2 3 4 5 6 7 13 12 14 15 17 16 BITCELL $T=3200 0 0 0 $X=3200 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_39 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27
** N=27 EP=27 IP=34 FDC=48
X0 1 2 5 6 7 8 9 10 11 12 13 14 15 16 17 26 27 ICV_38 $T=0 0 0 0 $X=0 $Y=-240
X1 3 4 5 6 7 8 9 18 19 20 21 22 23 24 25 26 27 ICV_38 $T=6400 0 0 0 $X=6400 $Y=-240
.ENDS
***************************************
.SUBCKT SRAM_1WORD RENB<0> RENB<1> RENB<2> RENB<3> RENB<4> RENB<5> RENB<6> RENB<7> RENB<8> RENB<9> RENB<10> RENB<11> RENB<12> RENB<13> RENB<14> RENB<15> RWL_N WWL RWLB_P RWL_P
+ RWLB_N RBL<0> REN<0> WBLB<0> WBL<0> RBL<1> REN<1> WBLB<1> WBL<1> RBL<2> REN<2> WBLB<2> WBL<2> RBL<3> REN<3> WBLB<3> WBL<3> RBL<4> REN<4> WBLB<4>
+ WBL<4> RBL<5> REN<5> WBLB<5> WBL<5> RBL<6> REN<6> WBLB<6> WBL<6> RBL<7> REN<7> WBLB<7> WBL<7> RBL<8> REN<8> WBLB<8> WBL<8> RBL<9> REN<9> WBLB<9>
+ WBL<9> RBL<10> REN<10> WBLB<10> WBL<10> RBL<11> REN<11> WBLB<11> WBL<11> RBL<12> REN<12> WBLB<12> WBL<12> RBL<13> REN<13> WBLB<13> WBL<13> RBL<14> REN<14> WBLB<14>
+ WBL<14> RBL<15> REN<15> WBLB<15> WBL<15> VDD! VSS!
** N=87 EP=87 IP=108 FDC=192
X0 RENB<0> RENB<1> RENB<2> RENB<3> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<0> REN<0> WBLB<0> WBL<0> RBL<1> REN<1> WBLB<1> WBL<1> RBL<2> REN<2> WBLB<2>
+ WBL<2> RBL<3> REN<3> WBLB<3> WBL<3> VDD! VSS!
+ ICV_39 $T=0 0 0 0 $X=0 $Y=-240
X1 RENB<4> RENB<5> RENB<6> RENB<7> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<4> REN<4> WBLB<4> WBL<4> RBL<5> REN<5> WBLB<5> WBL<5> RBL<6> REN<6> WBLB<6>
+ WBL<6> RBL<7> REN<7> WBLB<7> WBL<7> VDD! VSS!
+ ICV_39 $T=12800 0 0 0 $X=12800 $Y=-240
X2 RENB<8> RENB<9> RENB<10> RENB<11> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<8> REN<8> WBLB<8> WBL<8> RBL<9> REN<9> WBLB<9> WBL<9> RBL<10> REN<10> WBLB<10>
+ WBL<10> RBL<11> REN<11> WBLB<11> WBL<11> VDD! VSS!
+ ICV_39 $T=25600 0 0 0 $X=25600 $Y=-240
X3 RENB<12> RENB<13> RENB<14> RENB<15> RWL_N WWL RWLB_P RWL_P RWLB_N RBL<12> REN<12> WBLB<12> WBL<12> RBL<13> REN<13> WBLB<13> WBL<13> RBL<14> REN<14> WBLB<14>
+ WBL<14> RBL<15> REN<15> WBLB<15> WBL<15> VDD! VSS!
+ ICV_39 $T=38400 0 0 0 $X=38400 $Y=-240
.ENDS
***************************************
.SUBCKT RWL_Driver_1bit RWLN RWLBP RWLP RWLBN RWL_N RWLB_N RWLB_P RWL_P VSS! VDD!
** N=154 EP=10 IP=0 FDC=48
M0 RWLN 11 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 11 RWL_N VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=4730 $D=97
M2 VSS! 11 RWLN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=450 $D=97
M3 VSS! RWL_N 11 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=4730 $D=97
M4 RWLN 11 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=450 $D=97
M5 RWLBN 14 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=4730 $D=97
M6 VSS! 11 RWLN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=450 $D=97
M7 VSS! 14 RWLBN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=4730 $D=97
M8 14 RWLB_N VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=450 $D=97
M9 RWLBN 14 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=4730 $D=97
M10 VSS! RWLB_N 14 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=450 $D=97
M11 VSS! 14 RWLBN VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=4730 $D=97
M12 RWLBP 12 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=450 $D=97
M13 12 RWLB_P VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=4730 $D=97
M14 VSS! 12 RWLBP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=450 $D=97
M15 VSS! RWLB_P 12 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=4730 $D=97
M16 RWLBP 12 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=450 $D=97
M17 RWLP 13 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=4730 $D=97
M18 VSS! 12 RWLBP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=450 $D=97
M19 VSS! 13 RWLP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=4730 $D=97
M20 13 RWL_P VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=450 $D=97
M21 RWLP 13 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=4730 $D=97
M22 VSS! RWL_P 13 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=450 $D=97
M23 VSS! 13 RWLP VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=4730 $D=97
M24 RWLN 11 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=1490 $D=189
M25 11 RWL_N VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M26 VDD! 11 RWLN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=1490 $D=189
M27 VDD! RWL_N 11 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=3310 $D=189
M28 RWLN 11 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=1490 $D=189
M29 RWLBN 14 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1520 $Y=3310 $D=189
M30 VDD! 11 RWLN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=1490 $D=189
M31 VDD! 14 RWLBN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2000 $Y=3310 $D=189
M32 14 RWLB_N VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=1480 $D=189
M33 RWLBN 14 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2480 $Y=3310 $D=189
M34 VDD! RWLB_N 14 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=1480 $D=189
M35 VDD! 14 RWLBN VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2960 $Y=3310 $D=189
M36 RWLBP 12 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=1490 $D=189
M37 12 RWLB_P VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3440 $Y=3310 $D=189
M38 VDD! 12 RWLBP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=1490 $D=189
M39 VDD! RWLB_P 12 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3920 $Y=3310 $D=189
M40 RWLBP 12 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=1490 $D=189
M41 RWLP 13 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4400 $Y=3310 $D=189
M42 VDD! 12 RWLBP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=1490 $D=189
M43 VDD! 13 RWLP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4880 $Y=3310 $D=189
M44 13 RWL_P VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=1480 $D=189
M45 RWLP 13 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5360 $Y=3310 $D=189
M46 VDD! RWL_P 13 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=1480 $D=189
M47 VDD! 13 RWLP VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5840 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_40 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
+ 81 82 83 84 85 86 87
** N=91 EP=87 IP=97 FDC=240
X0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 88 17 89 90
+ 91 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
+ 81 82 83 84 85 86 87
+ SRAM_1WORD $T=0 0 0 0 $X=0 $Y=-240
X1 88 89 90 91 21 19 20 18 87 86 RWL_Driver_1bit $T=51200 0 0 0 $X=51140 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_41 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
+ 81 82 83 84 85 86 87 88 89 90 91 92
** N=92 EP=92 IP=174 FDC=480
X0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45
+ 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65
+ 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85
+ 86 87 88 89 90 91 92
+ ICV_40 $T=0 0 0 0 $X=0 $Y=-240
X1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 22 23 24 25
+ 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45
+ 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65
+ 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85
+ 86 87 88 89 90 91 92
+ ICV_40 $T=0 5600 0 0 $X=0 $Y=5360
.ENDS
***************************************
.SUBCKT ICV_42 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
+ 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
+ 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100
+ 101 102
** N=102 EP=102 IP=184 FDC=960
X0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 19 20 21
+ 22 18 23 24 25 26 37 38 39 40 41 42 43 44 45 46 47 48 49 50
+ 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70
+ 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90
+ 91 92 93 94 95 96 97 98 99 100 101 102
+ ICV_41 $T=0 0 0 0 $X=0 $Y=-240
X1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 27 29 30 31
+ 32 28 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50
+ 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70
+ 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90
+ 91 92 93 94 95 96 97 98 99 100 101 102
+ ICV_41 $T=0 11200 0 0 $X=0 $Y=10960
.ENDS
***************************************
.SUBCKT WBL_Driver_1bit IN WBLB WBL VSS! VDD!
** N=69 EP=5 IP=0 FDC=18
M0 VSS! 7 6 VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=450 $D=97
M1 VSS! 7 6 VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=4840 $D=97
M2 WBLB 6 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=450 $D=97
M3 7 IN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=4870 $D=97
M4 VSS! 6 WBLB VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=450 $D=97
M5 WBLB 6 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M6 VSS! IN 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=4870 $D=97
M7 VSS! 6 WBLB VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M8 WBL 8 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=4750 $D=97
M9 VDD! 7 6 VDD! pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=1560 $D=189
M10 VDD! 7 6 VDD! pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=3310 $D=189
M11 WBLB 6 VDD! VDD! pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=1500 $D=189
M12 7 IN VDD! VDD! pfet L=1.2e-07 W=6.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3310 $D=189
M13 VDD! 6 WBLB VDD! pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=1500 $D=189
M14 WBLB 6 VDD! VDD! pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1500 $D=189
M15 VDD! IN 8 VDD! pfet L=1.2e-07 W=5.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3310 $D=189
M16 VDD! 6 WBLB VDD! pfet L=1.2e-07 W=7.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1500 $D=189
M17 WBL 8 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT REN_Driver_1bit RENB REN IN VSS! VDD!
** N=68 EP=5 IP=0 FDC=18
M0 VSS! 7 6 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=450 $D=97
M1 VSS! 7 6 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=4750 $D=97
M2 RENB 6 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=450 $D=97
M3 7 IN VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=4750 $D=97
M4 VSS! 6 RENB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=450 $D=97
M5 RENB 6 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M6 VSS! IN 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=4750 $D=97
M7 VSS! 6 RENB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M8 REN 8 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=4750 $D=97
M9 VDD! 7 6 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=750 $Y=1490 $D=189
M10 VDD! 7 6 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=770 $Y=3310 $D=189
M11 RENB 6 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1230 $Y=1490 $D=189
M12 7 IN VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3310 $D=189
M13 VDD! 6 RENB VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1710 $Y=1490 $D=189
M14 RENB 6 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1490 $D=189
M15 VDD! IN 8 VDD! pfet L=1.2e-07 W=6.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3310 $D=189
M16 VDD! 6 RENB VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=1490 $D=189
M17 REN 8 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_36 1 2 3 4 5 6 7 8 9 10 11 12 13
** N=13 EP=13 IP=20 FDC=72
X0 3 4 5 12 13 WBL_Driver_1bit $T=0 0 0 0 $X=0 $Y=-240
X1 6 7 8 12 13 WBL_Driver_1bit $T=3200 0 0 0 $X=3200 $Y=-240
X2 1 10 9 12 13 REN_Driver_1bit $T=0 5600 0 0 $X=0 $Y=5360
X3 2 11 9 12 13 REN_Driver_1bit $T=3200 5600 0 0 $X=3200 $Y=5360
.ENDS
***************************************
.SUBCKT ICV_37 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23
** N=23 EP=23 IP=26 FDC=144
X0 1 2 5 7 9 10 11 13 6 8 12 22 23 ICV_36 $T=0 0 0 0 $X=0 $Y=-240
X1 3 4 14 15 17 18 19 21 6 16 20 22 23 ICV_36 $T=6400 0 0 0 $X=6400 $Y=-240
.ENDS
***************************************
.SUBCKT decoder_4th E SH3_BAR SH<3> IN IN1 VSS! VDD!
** N=108 EP=7 IP=0 FDC=26
M0 9 SH3_BAR IN VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=450 $D=97
M1 9 SH<3> IN1 VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=4810 $D=97
M2 10 9 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=450 $D=97
M3 10 9 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=4780 $D=97
M4 8 10 VSS! VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=450 $D=97
M5 E 8 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=4550 $D=97
M6 VSS! 10 8 VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=450 $D=97
M7 VSS! 8 E VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=4550 $D=97
M8 E 8 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3780 $Y=4550 $D=97
M9 E 8 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3820 $Y=450 $D=97
M10 VSS! 8 E VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4260 $Y=4550 $D=97
M11 VSS! 8 E VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4300 $Y=450 $D=97
M12 E 8 VSS! VSS! nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4740 $Y=4550 $D=97
M13 9 SH3_BAR IN1 VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=3310 $D=189
M14 9 SH<3> IN VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=1950 $D=189
M15 10 9 VDD! VDD! pfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=1920 $D=189
M16 10 9 VDD! VDD! pfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=3310 $D=189
M17 8 10 VDD! VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=1790 $D=189
M18 E 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=3310 $D=189
M19 VDD! 10 8 VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=1790 $D=189
M20 VDD! 8 E VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=3310 $D=189
M21 E 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3780 $Y=3310 $D=189
M22 E 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3820 $Y=1690 $D=189
M23 VDD! 8 E VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4260 $Y=3310 $D=189
M24 VDD! 8 E VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4300 $Y=1690 $D=189
M25 E 8 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4740 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_35 1 2 3 4 5 6 7 8
** N=8 EP=8 IP=14 FDC=52
X0 1 3 4 5 6 7 5 decoder_4th $T=0 0 0 0 $X=-300 $Y=-240
X1 2 3 4 5 8 7 5 decoder_4th $T=0 5600 0 0 $X=-300 $Y=5360
.ENDS
***************************************
.SUBCKT decoder_3rd 1 2 3 4 5 6 7
** N=67 EP=7 IP=0 FDC=8
M0 8 1 4 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=450 $D=97
M1 8 2 5 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=4870 $D=97
M2 3 8 6 6 nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=450 $D=97
M3 3 8 6 6 nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=4550 $D=97
M4 8 1 5 7 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=3310 $D=189
M5 8 2 4 7 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=2010 $D=189
M6 3 8 7 7 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=1690 $D=189
M7 3 8 7 7 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_33 1 2 3 4 5 6 7 8 9 10
** N=10 EP=10 IP=14 FDC=34
X0 1 2 3 6 7 8 7 decoder_4th $T=0 0 0 0 $X=-300 $Y=-240
X1 4 5 6 9 10 8 7 decoder_3rd $T=-4800 0 0 0 $X=-5100 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_34 1 2 3 4 5 6 7 8 9 10 11 12 13 14
** N=14 EP=14 IP=20 FDC=68
X0 1 6 7 3 4 5 11 12 9 10 ICV_33 $T=0 0 0 0 $X=-5100 $Y=-240
X1 2 6 7 3 4 8 11 12 13 14 ICV_33 $T=0 5600 0 0 $X=-5100 $Y=5360
.ENDS
***************************************
.SUBCKT decoder_2st 1 2 3 4 5 6 7
** N=37 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4590 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=1730 $D=189
.ENDS
***************************************
.SUBCKT decoder_1st 1 2 3 4 5 6 7
** N=39 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4870 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT DECODER_WORST SEL<3> SEL<2> SEL<0> SEL<1> EN E<15> E<14> E<13> E<12> E<11> E<10> E<9> E<8> E<7> E<6> E<5> E<4> E<3> E<2> E<1>
+ E<0> VDD! VSS!
** N=303 EP=23 IP=130 FDC=586
M0 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6730 $Y=34050 $D=97
M1 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=34050 $D=97
M2 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=39650 $D=97
M3 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=34050 $D=97
M4 27 26 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=39650 $D=97
M5 VSS! 45 24 VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8090 $Y=38320 $D=97
M6 VSS! 46 26 VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8110 $Y=43980 $D=97
M7 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=34050 $D=97
M8 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=39650 $D=97
M9 24 45 VSS! VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8570 $Y=38320 $D=97
M10 26 46 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8590 $Y=43980 $D=97
M11 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=34050 $D=97
M12 27 26 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=39650 $D=97
M13 VSS! 45 24 VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9050 $Y=38320 $D=97
M14 VSS! 46 26 VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9070 $Y=43980 $D=97
M15 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=34050 $D=97
M16 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=39650 $D=97
M17 24 45 VSS! VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9540 $Y=38320 $D=97
M18 26 46 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9560 $Y=43980 $D=97
M19 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=34050 $D=97
M20 27 26 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=39650 $D=97
M21 VSS! 45 24 VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10020 $Y=38320 $D=97
M22 VSS! 46 26 VSS! nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10040 $Y=43980 $D=97
M23 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=34050 $D=97
M24 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=39650 $D=97
M25 25 24 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=34050 $D=97
M26 45 SEL<3> VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=38330 $D=97
M27 27 26 VSS! VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=39650 $D=97
M28 46 SEL<2> VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=43990 $D=97
M29 VSS! 24 25 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=34050 $D=97
M30 VSS! SEL<3> 45 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=38330 $D=97
M31 VSS! 26 27 VSS! nfet L=1.2e-07 W=4.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=39650 $D=97
M32 VSS! SEL<2> 46 VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=43990 $D=97
M33 28 29 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12820 $Y=62050 $D=97
M34 VSS! 29 28 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13300 $Y=62050 $D=97
M35 29 SEL<1> VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13500 $Y=66400 $D=97
M36 28 29 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=62050 $D=97
M37 44 SEL<0> VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=77600 $D=97
M38 VSS! SEL<1> 29 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13980 $Y=66400 $D=97
M39 VSS! 29 28 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=62050 $D=97
M40 VSS! SEL<0> 44 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=77600 $D=97
M41 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6730 $Y=35110 $D=189
M42 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=35110 $D=189
M43 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7210 $Y=40790 $D=189
M44 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=35110 $D=189
M45 27 26 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7690 $Y=40790 $D=189
M46 VDD! 45 24 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8090 $Y=36910 $D=189
M47 VDD! 46 26 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8110 $Y=42510 $D=189
M48 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=35110 $D=189
M49 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8180 $Y=40790 $D=189
M50 24 45 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8570 $Y=36910 $D=189
M51 26 46 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8590 $Y=42510 $D=189
M52 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=35110 $D=189
M53 27 26 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=8660 $Y=40790 $D=189
M54 VDD! 45 24 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9050 $Y=36910 $D=189
M55 VDD! 46 26 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9070 $Y=42510 $D=189
M56 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=35110 $D=189
M57 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9140 $Y=40790 $D=189
M58 24 45 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9540 $Y=36910 $D=189
M59 26 46 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9560 $Y=42510 $D=189
M60 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=35110 $D=189
M61 27 26 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=9620 $Y=40790 $D=189
M62 VDD! 45 24 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10020 $Y=36910 $D=189
M63 VDD! 46 26 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10040 $Y=42510 $D=189
M64 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=35110 $D=189
M65 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10100 $Y=40790 $D=189
M66 25 24 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=35110 $D=189
M67 45 SEL<3> VDD! VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=36910 $D=189
M68 27 26 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=40790 $D=189
M69 46 SEL<2> VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=10590 $Y=42510 $D=189
M70 VDD! 24 25 VDD! pfet L=1.2e-07 W=7.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=35110 $D=189
M71 VDD! SEL<3> 45 VDD! pfet L=1.2e-07 W=8.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=36910 $D=189
M72 VDD! 26 27 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=40790 $D=189
M73 VDD! SEL<2> 46 VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=11070 $Y=42510 $D=189
M74 28 29 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=12820 $Y=63090 $D=189
M75 VDD! 29 28 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13300 $Y=63090 $D=189
M76 29 SEL<1> VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13500 $Y=64910 $D=189
M77 28 29 VDD! VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=63090 $D=189
M78 44 SEL<0> VDD! VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13790 $Y=76110 $D=189
M79 VDD! SEL<1> 29 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=13980 $Y=64910 $D=189
M80 VDD! 29 28 VDD! pfet L=1.2e-07 W=8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=63090 $D=189
M81 VDD! SEL<0> 44 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=14270 $Y=76110 $D=189
X82 E<15> E<14> 25 24 VDD! 37 VSS! 36 ICV_35 $T=6000 0 1 180 $X=-300 $Y=-240
X83 E<13> E<12> 25 24 VDD! 35 VSS! 34 ICV_35 $T=6000 11200 1 180 $X=-300 $Y=10960
X84 E<11> E<10> 25 24 VDD! 33 VSS! 32 ICV_35 $T=6000 22400 1 180 $X=-300 $Y=22160
X85 E<9> E<8> 25 24 VDD! 31 VSS! 30 ICV_35 $T=6000 33600 1 180 $X=-300 $Y=33360
X86 E<7> E<6> 27 26 37 25 24 36 VSS! 41 VDD! VSS! VSS! 40 ICV_34 $T=6000 44800 1 180 $X=-300 $Y=44560
X87 E<5> E<4> 27 26 35 25 24 34 VSS! 39 VDD! VSS! VSS! 38 ICV_34 $T=6000 56000 1 180 $X=-300 $Y=55760
X88 E<3> E<2> 27 26 33 25 24 32 41 VSS! VDD! VSS! 40 VSS! ICV_34 $T=6000 67200 1 180 $X=-300 $Y=66960
X89 E<1> E<0> 27 26 31 25 24 30 39 VSS! VDD! VSS! 38 VSS! ICV_34 $T=6000 78400 1 180 $X=-300 $Y=78160
X90 29 28 41 VSS! VDD! VSS! 43 decoder_2st $T=13200 67200 1 180 $X=10680 $Y=66960
X91 29 28 40 VSS! VDD! VSS! 42 decoder_2st $T=13200 72800 1 180 $X=10680 $Y=72560
X92 29 28 39 VSS! VDD! 43 VSS! decoder_2st $T=13200 78400 1 180 $X=10680 $Y=78160
X93 29 28 38 VSS! VDD! 42 VSS! decoder_2st $T=13200 84000 1 180 $X=10680 $Y=83760
X94 44 SEL<0> 43 VSS! VDD! VSS! EN decoder_1st $T=14800 78400 1 180 $X=12280 $Y=78160
X95 44 SEL<0> 42 VSS! VDD! EN VSS! decoder_1st $T=14800 84000 1 180 $X=12280 $Y=83760
.ENDS
***************************************
.SUBCKT XNOR_SRAM_16X16_ADC_MUXWBuffer VSS! VDD! REN_SRAM RWLB_P<15> RWL_N<15> RWL_P<15> RWLB_N<15> RWLB_P<14> RWL_N<14> RWL_P<14> RWLB_N<14> RWLB_P<13> RWL_N<13> RWL_P<13> RWLB_N<13> RWLB_P<12> RWL_N<12> RWL_P<12> RWLB_N<12> RWLB_P<11>
+ RWL_N<11> RWL_P<11> RWLB_N<11> RWLB_P<10> RWL_N<10> RWL_P<10> RWLB_N<10> RWLB_P<9> RWL_N<9> RWL_P<9> RWLB_N<9> RWLB_P<8> RWL_N<8> RWL_P<8> RWLB_N<8> RWLB_P<7> RWL_N<7> RWL_P<7> RWLB_N<7> RWLB_P<6>
+ RWL_N<6> RWL_P<6> RWLB_N<6> RWLB_P<5> RWL_N<5> RWL_P<5> RWLB_N<5> RWLB_P<4> RWL_N<4> RWL_P<4> RWLB_N<4> RWLB_P<3> RWL_N<3> RWL_P<3> RWLB_N<3> RWLB_P<2> RWL_N<2> RWL_P<2> RWLB_N<2> RWLB_P<1>
+ RWL_N<1> RWL_P<1> RWLB_N<1> RWLB_P<0> RWL_N<0> RWL_P<0> RWLB_N<0> CLK WR_Addr<3> WR_Addr<2> WR_Addr<0> WR_EN WR_Addr<1> Vref<0> Vref<1> Vref<2> Vref<3> Vref<4> Vref<5> Vref<6>
+ Vref<7> Vref<8> Vref<9> Vref<10> Vref<11> Vref<12> Vref<13> Vref<14> Vref<15> Vref<16> Vref<17> Vref<18> Vref<19> Vref<20> Vref<21> Vref<22> Vref<23> Vref<24> Vref<25> Vref<26>
+ Vref<27> Vref<28> Vref<29> Vref<30> Vref<31> WR_Data<0> WR_Data<1> WR_Data<2> WR_Data<3> WR_Data<4> WR_Data<5> WR_Data<6> WR_Data<7> WR_Data<8> WR_Data<9> WR_Data<10> WR_Data<11> WR_Data<12> WR_Data<13> WR_Data<14>
+ WR_Data<15> OUT<0> OUT<1> OUT<2> OUT<3> OUT<4> OUT<5> OUT<6> OUT<7> OUT<8> OUT<9> OUT<10> OUT<11> OUT<12> OUT<13> OUT<14> OUT<15> OUT<16> OUT<17> OUT<18>
+ OUT<19> OUT<20> OUT<21> OUT<22> OUT<23> OUT<24> OUT<25> OUT<26> OUT<27> OUT<28> OUT<29> OUT<30> OUT<31> Col_Sel<0> Col_Sel<1> Col_Sel<2> Col_Sel<3> Col_Sel<4> Col_Sel<5> Col_Sel<6>
+ Col_Sel<7> Col_Sel<8> Col_Sel<9> Col_Sel<10> Col_Sel<11> Col_Sel<12> Col_Sel<13> Col_Sel<14> Col_Sel<15> Dsram<0> Dsram<1> Dsram<2> Dsram<3> Dsram<4> Dsram<5> Dsram<6> Dsram<7> Dsram<8> Dsram<9> Dsram<10>
+ Dsram<11> Dsram<12> Dsram<13> Dsram<14> Dsram<15>
** N=282 EP=185 IP=660 FDC=5665
X0 CLK 235 VSS! Vref<0> Vref<1> Vref<2> Vref<3> Vref<4> Vref<5> Vref<6> Vref<7> Vref<8> Vref<9> Vref<10> Vref<11> Vref<12> Vref<13> Vref<14> Vref<15> Vref<16>
+ Vref<17> Vref<18> Vref<19> Vref<20> Vref<21> Vref<22> Vref<23> Vref<24> Vref<25> Vref<26> Vref<27> Vref<28> Vref<29> Vref<30> Vref<31> VDD! OUT<0> OUT<1> OUT<2> OUT<3>
+ OUT<4> OUT<5> OUT<6> OUT<7> OUT<8> OUT<9> OUT<10> OUT<11> OUT<12> OUT<13> OUT<14> OUT<15> OUT<16> OUT<17> OUT<18> OUT<19> OUT<20> OUT<21> OUT<22> OUT<23>
+ OUT<24> OUT<25> OUT<26> OUT<27> OUT<28> OUT<29> OUT<30> OUT<31>
+ 32ADC $T=-139200 -26400 0 0 $X=-142980 $Y=-29870
X1 186 235 236 237 238 239 240 241 242 243 244 245 246 247 248 249 250 Col_Sel<0> Col_Sel<1> Col_Sel<2>
+ Col_Sel<3> Col_Sel<4> Col_Sel<5> Col_Sel<6> Col_Sel<7> Col_Sel<8> Col_Sel<9> Col_Sel<10> Col_Sel<11> Col_Sel<12> Col_Sel<13> Col_Sel<14> Col_Sel<15> VSS! VDD!
+ ADC_Mux $T=0 0 1 0 $X=-340 $Y=-4660
X2 186 Dsram<0> 236 Dsram<1> 237 Dsram<2> 238 Dsram<3> 239 Dsram<4> 240 Dsram<5> 241 Dsram<6> 242 Dsram<7> 243 Dsram<8> 244 Dsram<9>
+ 245 Dsram<10> 246 Dsram<11> 247 Dsram<12> 248 Dsram<13> 249 Dsram<14> 250 VDD! VSS! Dsram<15>
+ special_inverter $T=0 -8400 0 0 $X=0 $Y=-8640
X3 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 232 233 RWLB_P<15> RWL_N<15>
+ RWL_P<15> RWLB_N<15> RWLB_P<14> RWL_N<14> RWL_P<14> RWLB_N<14> 234 220 RWLB_P<13> RWL_N<13> RWL_P<13> RWLB_N<13> RWLB_P<12> RWL_N<12> RWL_P<12> RWLB_N<12> 186 203 251 252
+ 236 204 253 254 237 205 255 256 238 206 257 258 239 207 259 260 240 208 261 262
+ 241 209 263 264 242 210 265 266 243 211 267 268 244 212 269 270 245 213 271 272
+ 246 214 273 274 247 215 275 276 248 216 277 278 249 217 279 280 250 218 281 282
+ VDD! VSS!
+ ICV_42 $T=0 0 0 0 $X=0 $Y=-240
X4 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 221 222 RWLB_P<11> RWL_N<11>
+ RWL_P<11> RWLB_N<11> RWLB_P<10> RWL_N<10> RWL_P<10> RWLB_N<10> 223 224 RWLB_P<9> RWL_N<9> RWL_P<9> RWLB_N<9> RWLB_P<8> RWL_N<8> RWL_P<8> RWLB_N<8> 186 203 251 252
+ 236 204 253 254 237 205 255 256 238 206 257 258 239 207 259 260 240 208 261 262
+ 241 209 263 264 242 210 265 266 243 211 267 268 244 212 269 270 245 213 271 272
+ 246 214 273 274 247 215 275 276 248 216 277 278 249 217 279 280 250 218 281 282
+ VDD! VSS!
+ ICV_42 $T=0 22400 0 0 $X=0 $Y=22160
X5 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 225 226 RWLB_P<7> RWL_N<7>
+ RWL_P<7> RWLB_N<7> RWLB_P<6> RWL_N<6> RWL_P<6> RWLB_N<6> 227 228 RWLB_P<5> RWL_N<5> RWL_P<5> RWLB_N<5> RWLB_P<4> RWL_N<4> RWL_P<4> RWLB_N<4> 186 203 251 252
+ 236 204 253 254 237 205 255 256 238 206 257 258 239 207 259 260 240 208 261 262
+ 241 209 263 264 242 210 265 266 243 211 267 268 244 212 269 270 245 213 271 272
+ 246 214 273 274 247 215 275 276 248 216 277 278 249 217 279 280 250 218 281 282
+ VDD! VSS!
+ ICV_42 $T=0 44800 0 0 $X=0 $Y=44560
X6 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 219 229 RWLB_P<3> RWL_N<3>
+ RWL_P<3> RWLB_N<3> RWLB_P<2> RWL_N<2> RWL_P<2> RWLB_N<2> 230 231 RWLB_P<1> RWL_N<1> RWL_P<1> RWLB_N<1> RWLB_P<0> RWL_N<0> RWL_P<0> RWLB_N<0> 186 203 251 252
+ 236 204 253 254 237 205 255 256 238 206 257 258 239 207 259 260 240 208 261 262
+ 241 209 263 264 242 210 265 266 243 211 267 268 244 212 269 270 245 213 271 272
+ 246 214 273 274 247 215 275 276 248 216 277 278 249 217 279 280 250 218 281 282
+ VDD! VSS!
+ ICV_42 $T=0 67200 0 0 $X=0 $Y=66960
X7 187 188 189 190 WR_Data<0> REN_SRAM 251 203 252 WR_Data<1> 253 204 254 WR_Data<2> 255 205 256 WR_Data<3> 257 206
+ 258 VSS! VDD!
+ ICV_37 $T=0 89600 0 0 $X=0 $Y=89360
X8 191 192 193 194 WR_Data<4> REN_SRAM 259 207 260 WR_Data<5> 261 208 262 WR_Data<6> 263 209 264 WR_Data<7> 265 210
+ 266 VSS! VDD!
+ ICV_37 $T=12800 89600 0 0 $X=12800 $Y=89360
X9 195 196 197 198 WR_Data<8> REN_SRAM 267 211 268 WR_Data<9> 269 212 270 WR_Data<10> 271 213 272 WR_Data<11> 273 214
+ 274 VSS! VDD!
+ ICV_37 $T=25600 89600 0 0 $X=25600 $Y=89360
X10 199 200 201 202 WR_Data<12> REN_SRAM 275 215 276 WR_Data<13> 277 216 278 WR_Data<14> 279 217 280 WR_Data<15> 281 218
+ 282 VSS! VDD!
+ ICV_37 $T=38400 89600 0 0 $X=38400 $Y=89360
X11 WR_Addr<3> WR_Addr<2> WR_Addr<0> WR_Addr<1> WR_EN 232 233 234 220 221 222 223 224 225 226 227 228 219 229 230
+ 231 VDD! VSS!
+ DECODER_WORST $T=57600 0 0 0 $X=57300 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_32
** N=189 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_31
** N=334 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_30
** N=465 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_29
** N=293 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_28
** N=317 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_27
** N=318 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_26
** N=321 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_25
** N=227 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT Inst_rom_g1 VDD VSS A<9> A<8> A<7> A<6> A<5> A<4> A<3> A<2> A<1> A<0> CLK CEN Q<0> Q<1> Q<2> Q<3> Q<4> Q<5>
+ Q<6> Q<7> Q<8> Q<9> Q<10> Q<11> Q<12> Q<13> Q<14> Q<15>
** N=454 EP=30 IP=1558 FDC=0
.ENDS
***************************************
.SUBCKT NOR3X1TR C VDD B A VSS Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND4BX1TR AN D VSS C B Y VDD
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKINVX2TR A VSS VDD Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT INVX2TR A VSS VDD Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI33X1TR B2 B1 VSS B0 A0 A1 A2 VDD Y
** N=12 EP=9 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKAND2X2TR A B VSS VDD Y
** N=8 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_24 1 2 3 4 5 6 7 8 9
** N=9 EP=9 IP=14 FDC=0
X0 1 2 3 4 5 CLKAND2X2TR $T=0 0 0 0 $X=-340 $Y=-280
X1 6 2 3 4 7 CLKAND2X2TR $T=2000 0 0 0 $X=1660 $Y=-280
.ENDS
***************************************
.SUBCKT we_reorder inst7_4<0> inst7_4<1> WE<5> inst7_4<3> WE<10> WE<14> WE<4> WE<7> WE<1> WE<12> WE<3> WE_OUT<1> WE_OUT<10> WE_OUT<12> WE<8> WE_OUT<7> WE_OUT<5> WE_OUT<14> WE<2> WE<9>
+ WE<11> WE<0> WE<6> WE<15> WE<13> WE_OUT<3> WE_OUT<9> WE_OUT<6> WE_OUT<13> WE_OUT<11> WE_OUT<0> WE_OUT<4> WE_OUT<15> WE_OUT<2> WE_OUT<8> inst15_12<0> inst15_12<1> inst15_12<3> inst15_12<2> inst7_4<2>
+ 41
** N=64 EP=41 IP=128 FDC=0
X0 inst15_12<2> VDD inst15_12<3> inst15_12<1> VSS 44 NOR3X1TR $T=14000 104400 1 180 $X=11660 $Y=104120
X1 inst15_12<0> 44 VSS inst7_4<2> 45 47 VDD NAND4BX1TR $T=14000 104400 0 0 $X=13660 $Y=104120
X2 inst7_4<0> VSS VDD 49 CLKINVX2TR $T=17600 104400 1 0 $X=17260 $Y=100400
X3 inst7_4<3> VSS VDD 46 CLKINVX2TR $T=24800 104400 1 180 $X=23260 $Y=104120
X4 inst7_4<1> VSS VDD 48 INVX2TR $T=18800 104400 0 0 $X=18460 $Y=104120
X5 inst7_4<1> inst7_4<3> VSS inst7_4<0> 46 49 48 VDD 45 OAI33X1TR $T=23600 104400 1 180 $X=19660 $Y=104120
X6 WE<3> 47 VSS VDD WE_OUT<3> CLKAND2X2TR $T=28400 54000 0 0 $X=28060 $Y=53720
X7 WE<8> 47 VSS VDD WE_OUT<8> CLKAND2X2TR $T=28400 68400 1 0 $X=28060 $Y=64400
X8 WE<0> 47 VSS VDD WE_OUT<0> CLKAND2X2TR $T=28800 46800 0 0 $X=28460 $Y=46520
X9 WE<15> 47 VSS VDD WE_OUT<15> CLKAND2X2TR $T=28800 82800 1 0 $X=28460 $Y=78800
X10 WE<1> 47 VSS VDD WE_OUT<1> WE<2> WE_OUT<2> 61 41 ICV_24 $T=26400 54000 1 0 $X=26060 $Y=50000
X11 WE<5> 47 VSS VDD WE_OUT<5> WE<4> WE_OUT<4> 62 41 ICV_24 $T=26400 61200 1 0 $X=26060 $Y=57200
X12 WE<10> 47 VSS VDD WE_OUT<10> WE<9> WE_OUT<9> 56 41 ICV_24 $T=26400 68400 0 0 $X=26060 $Y=68120
X13 WE<12> 47 VSS VDD WE_OUT<12> WE<11> WE_OUT<11> 56 41 ICV_24 $T=26400 75600 1 0 $X=26060 $Y=71600
X14 WE<7> 47 VSS VDD WE_OUT<7> WE<6> WE_OUT<6> 63 41 ICV_24 $T=26800 61200 0 0 $X=26460 $Y=60920
X15 WE<14> 47 VSS VDD WE_OUT<14> WE<13> WE_OUT<13> 64 41 ICV_24 $T=26800 75600 0 0 $X=26460 $Y=75320
.ENDS
***************************************
.SUBCKT XNOR2X1TR B A Y VDD VSS
** N=9 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKINVX1TR A VSS VDD Y
** N=5 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI21X1TR A1 VSS A0 B0 VDD Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND2X1TR B VSS Y A VDD
** N=6 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR2X1TR B VDD A Y VSS
** N=6 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT XOR2X1TR A B Y VDD VSS
** N=9 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT DFFQX1TR D CK VSS VDD Q
** N=12 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI21X4TR A0 A1 VDD B0 VSS Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI21X2TR A0 A1 VSS B0 Y VDD
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT INVX1TR VSS VDD A Y
** N=5 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI211X1TR A1 A0 VSS B0 C0 VDD Y
** N=9 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI22XLTR B1 VDD B0 A0 A1 Y VSS
** N=9 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI22X1TR B1 VDD B0 A0 A1 VSS Y
** N=9 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OR2X1TR VSS A B VDD Y
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AND2X1TR A B VSS VDD Y
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR3BX2TR Y B VSS C AN VDD
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI21X1TR A1 A0 VDD B0 VSS Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKBUFX20TR A VSS Y VDD
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND2BX1TR AN B VSS Y VDD
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AND2X2TR A B VDD VSS Y
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR2X2TR A B Y VSS VDD
** N=6 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND2XLTR B VSS A Y VDD
** N=6 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKINVX6TR A VSS VDD Y
** N=5 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT XOR2X2TR B VSS A VDD Y
** N=9 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT XNOR2X2TR B A VSS VDD Y
** N=9 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OR2X2TR A B VSS VDD Y
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT DFFTRX2TR D RN CK Q VSS VDD QN
** N=16 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT TIEHITR VSS VDD Y
** N=5 EP=3 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT DFFTRX1TR D RN CK Q VSS VDD QN
** N=16 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT pc RF_in<5> RF_in<9> RF_in<8> RF_in<6> VDD VSS addr<0> addr<1> addr<2> addr<4> addr<8> addr<9> Rlink<3> clk addr<6> addr<7> addr<5> Rlink<2> addr<3> Rlink<1>
+ Rlink<5> scan_en Rlink<7> Rlink<8> Rlink<4> resetn jmp stall br disp<3> scan_in disp<7> Rlink<9> scan_out RF_in<3> RF_in<1> RF_in<7> RF_in<0> RF_in<2> RF_in<4>
+ Rlink<0> Rlink<6> disp<2> disp<0> disp<1> disp<4> disp<5> disp<6> disp<8> disp<9>
** N=191 EP=50 IP=884 FDC=0
X0 resetn VDD stall scan_en VSS 91 NOR3X1TR $T=49600 39600 0 0 $X=49260 $Y=39320
X1 60 VSS VDD 57 CLKINVX2TR $T=18000 25200 0 180 $X=16460 $Y=21200
X2 64 VSS VDD 162 CLKINVX2TR $T=19600 39600 0 0 $X=19260 $Y=39320
X3 Rlink<3> VSS VDD 134 CLKINVX2TR $T=26400 18000 0 0 $X=26060 $Y=17720
X4 95 VSS VDD 144 CLKINVX2TR $T=60400 10800 1 0 $X=60060 $Y=6800
X5 102 VSS VDD 103 CLKINVX2TR $T=61200 10800 0 0 $X=60860 $Y=10520
X6 104 VSS VDD 110 CLKINVX2TR $T=70000 10800 0 180 $X=68460 $Y=6800
X7 117 VSS VDD 112 CLKINVX2TR $T=75200 32400 0 180 $X=73660 $Y=28400
X8 Rlink<9> VSS VDD 126 CLKINVX2TR $T=86400 39600 1 0 $X=86060 $Y=35600
X9 133 VSS VDD 53 INVX2TR $T=20000 32400 0 180 $X=18460 $Y=28400
X10 Rlink<6> VSS VDD 115 INVX2TR $T=70400 25200 1 0 $X=70060 $Y=21200
X11 131 51 Rlink<3> VDD VSS XNOR2X1TR $T=10800 25200 0 180 $X=7260 $Y=21200
X12 139 84 Rlink<4> VDD VSS XNOR2X1TR $T=40400 18000 1 180 $X=36860 $Y=17720
X13 52 VSS VDD 132 CLKINVX1TR $T=12800 25200 0 0 $X=12460 $Y=24920
X14 87 VSS VDD 89 CLKINVX1TR $T=50000 18000 1 180 $X=48460 $Y=17720
X15 113 VSS VDD 152 CLKINVX1TR $T=70000 10800 1 0 $X=69660 $Y=6800
X16 52 VSS 53 55 VDD 51 OAI21X1TR $T=14000 25200 0 0 $X=13660 $Y=24920
X17 55 VSS 60 59 VDD 58 OAI21X1TR $T=18000 25200 1 180 $X=15660 $Y=24920
X18 104 VSS 103 105 VDD 148 OAI21X1TR $T=62400 10800 0 0 $X=62060 $Y=10520
X19 105 VSS 113 154 VDD 107 OAI21X1TR $T=70800 18000 1 0 $X=70460 $Y=14000
X20 55 VSS 54 132 VDD NAND2X1TR $T=15600 32400 0 180 $X=13660 $Y=28400
X21 59 VSS 131 57 VDD NAND2X1TR $T=16800 25200 0 180 $X=14860 $Y=21200
X22 65 VSS 63 162 VDD NAND2X1TR $T=22400 39600 1 180 $X=20460 $Y=39320
X23 71 VSS 65 addr<1> VDD NAND2X1TR $T=24000 39600 1 180 $X=22060 $Y=39320
X24 69 VSS 59 addr<3> VDD NAND2X1TR $T=24400 25200 0 180 $X=22460 $Y=21200
X25 67 VSS 55 addr<2> VDD NAND2X1TR $T=22800 25200 0 0 $X=22460 $Y=24920
X26 78 VSS 61 addr<0> VDD NAND2X1TR $T=29200 39600 1 180 $X=27260 $Y=39320
X27 87 VSS 139 86 VDD NAND2X1TR $T=44000 18000 1 180 $X=42060 $Y=17720
X28 93 VSS 87 addr<4> VDD NAND2X1TR $T=48400 25200 0 180 $X=46460 $Y=21200
X29 142 VSS 99 addr<5> VDD NAND2X1TR $T=56000 25200 1 0 $X=55660 $Y=21200
X30 106 VSS 145 95 VDD NAND2X1TR $T=63600 18000 0 180 $X=61660 $Y=14000
X31 105 VSS 101 110 VDD NAND2X1TR $T=67200 18000 1 0 $X=66860 $Y=14000
X32 150 VSS 105 addr<6> VDD NAND2X1TR $T=69600 18000 0 0 $X=69260 $Y=17720
X33 154 VSS 151 152 VDD NAND2X1TR $T=72800 10800 1 180 $X=70860 $Y=10520
X34 153 VSS 154 addr<7> VDD NAND2X1TR $T=72400 25200 1 0 $X=72060 $Y=21200
X35 117 VSS 114 116 VDD NAND2X1TR $T=75200 32400 1 0 $X=74860 $Y=28400
X36 118 VSS 117 addr<8> VDD NAND2X1TR $T=76400 32400 0 0 $X=76060 $Y=32120
X37 121 VSS 155 addr<9> VDD NAND2X1TR $T=80000 39600 0 0 $X=79660 $Y=39320
X38 155 VSS 119 165 VDD NAND2X1TR $T=80400 39600 1 0 $X=80060 $Y=35600
X39 RF_in<3> VSS 75 96 VDD NAND2X1TR $T=97600 18000 0 180 $X=95660 $Y=14000
X40 RF_in<6> VSS 128 96 VDD NAND2X1TR $T=99200 25200 1 180 $X=97260 $Y=24920
X41 RF_in<2> VSS 77 96 VDD NAND2X1TR $T=99600 32400 0 180 $X=97660 $Y=28400
X42 RF_in<1> VSS 80 96 VDD NAND2X1TR $T=100400 32400 1 180 $X=98460 $Y=32120
X43 RF_in<7> VSS 124 96 VDD NAND2X1TR $T=100800 25200 1 180 $X=98860 $Y=24920
X44 RF_in<0> VSS 125 96 VDD NAND2X1TR $T=100800 39600 0 180 $X=98860 $Y=35600
X45 RF_in<5> VSS 92 96 VDD NAND2X1TR $T=101200 32400 0 180 $X=99260 $Y=28400
X46 RF_in<9> VSS 130 96 VDD NAND2X1TR $T=102000 32400 1 180 $X=100060 $Y=32120
X47 RF_in<4> VSS 83 96 VDD NAND2X1TR $T=102400 25200 1 180 $X=100460 $Y=24920
X48 RF_in<8> VSS 123 96 VDD NAND2X1TR $T=102800 32400 0 180 $X=100860 $Y=28400
X49 60 VDD 52 56 VSS NOR2X1TR $T=17200 18000 1 180 $X=15260 $Y=17720
X50 67 VDD addr<2> 52 VSS NOR2X1TR $T=21200 25200 1 180 $X=19260 $Y=24920
X51 71 VDD addr<1> 64 VSS NOR2X1TR $T=26000 39600 1 180 $X=24060 $Y=39320
X52 69 VDD addr<3> 60 VSS NOR2X1TR $T=26400 25200 0 180 $X=24460 $Y=21200
X53 93 VDD addr<4> 94 VSS NOR2X1TR $T=52000 25200 0 180 $X=50060 $Y=21200
X54 142 VDD addr<5> 98 VSS NOR2X1TR $T=56400 25200 1 180 $X=54460 $Y=24920
X55 104 VDD 144 147 VSS NOR2X1TR $T=63200 10800 0 180 $X=61260 $Y=6800
X56 150 VDD addr<6> 104 VSS NOR2X1TR $T=68000 18000 1 180 $X=66060 $Y=17720
X57 113 VDD 104 106 VSS NOR2X1TR $T=70400 18000 0 180 $X=68460 $Y=14000
X58 153 VDD addr<7> 113 VSS NOR2X1TR $T=73200 18000 1 180 $X=71260 $Y=17720
X59 53 54 Rlink<2> VDD VSS XOR2X1TR $T=15600 32400 1 0 $X=15260 $Y=28400
X60 63 61 Rlink<1> VDD VSS XOR2X1TR $T=19600 39600 1 180 $X=16060 $Y=39320
X61 140 88 Rlink<5> VDD VSS XOR2X1TR $T=45200 25200 0 180 $X=41660 $Y=21200
X62 164 101 Rlink<6> VDD VSS XOR2X1TR $T=58400 18000 1 0 $X=58060 $Y=14000
X63 68 66 VSS VDD addr<3> DFFQX1TR $T=24400 18000 1 180 $X=17260 $Y=17720
X64 135 66 VSS VDD addr<2> DFFQX1TR $T=30000 18000 0 180 $X=22860 $Y=14000
X65 137 66 VSS VDD addr<1> DFFQX1TR $T=31600 39600 0 180 $X=24460 $Y=35600
X66 81 66 VSS VDD addr<4> DFFQX1TR $T=34000 18000 1 0 $X=33660 $Y=14000
X67 141 66 VSS VDD addr<5> DFFQX1TR $T=49600 25200 1 180 $X=42460 $Y=24920
X68 143 66 VSS VDD addr<0> DFFQX1TR $T=60800 32400 1 180 $X=53660 $Y=32120
X69 156 66 VSS VDD addr<6> DFFQX1TR $T=82400 18000 1 180 $X=75260 $Y=17720
X70 166 66 VSS VDD addr<8> DFFQX1TR $T=86400 25200 0 0 $X=86060 $Y=24920
X71 127 66 VSS VDD addr<9> DFFQX1TR $T=88000 39600 1 0 $X=87660 $Y=35600
X72 133 56 VDD 58 VSS 62 AOI21X4TR $T=22800 25200 0 180 $X=17660 $Y=21200
X73 64 61 VSS 65 133 VDD OAI21X2TR $T=18800 39600 1 0 $X=18460 $Y=35600
X74 98 87 VSS 99 102 VDD OAI21X2TR $T=54400 18000 0 0 $X=54060 $Y=17720
X75 62 145 VSS 149 108 VDD OAI21X2TR $T=61600 18000 0 0 $X=61260 $Y=17720
X76 VSS VDD Rlink<2> 72 INVX1TR $T=22000 32400 1 0 $X=21660 $Y=28400
X77 VSS VDD Rlink<1> 70 INVX1TR $T=23600 39600 1 0 $X=23260 $Y=35600
X78 VSS VDD Rlink<0> 100 INVX1TR $T=34800 39600 0 0 $X=34460 $Y=39320
X79 VSS VDD Rlink<4> 138 INVX1TR $T=40400 25200 0 180 $X=38860 $Y=21200
X80 VSS VDD Rlink<5> 90 INVX1TR $T=44000 32400 0 0 $X=43660 $Y=32120
X81 VSS VDD 94 86 INVX1TR $T=51200 18000 1 180 $X=49660 $Y=17720
X82 VSS VDD 98 97 INVX1TR $T=56000 25200 0 180 $X=54460 $Y=21200
X83 VSS VDD Rlink<8> 157 INVX1TR $T=82000 32400 1 0 $X=81660 $Y=28400
X84 VSS VDD Rlink<7> 159 INVX1TR $T=84400 18000 0 0 $X=84060 $Y=17720
X85 73 134 VSS 76 75 VDD 68 OAI211X1TR $T=27600 18000 0 0 $X=27260 $Y=17720
X86 73 72 VSS 136 77 VDD 135 OAI211X1TR $T=28000 25200 1 0 $X=27660 $Y=21200
X87 73 70 VSS 74 80 VDD 137 OAI211X1TR $T=30400 32400 0 0 $X=30060 $Y=32120
X88 73 138 VSS 82 83 VDD 81 OAI211X1TR $T=34800 18000 0 0 $X=34460 $Y=17720
X89 73 90 VSS 85 92 VDD 141 OAI211X1TR $T=48000 32400 1 0 $X=47660 $Y=28400
X90 73 100 VSS 146 125 VDD 143 OAI211X1TR $T=59600 39600 1 0 $X=59260 $Y=35600
X91 73 115 VSS 120 128 VDD 156 OAI211X1TR $T=79600 25200 1 0 $X=79260 $Y=21200
X92 73 157 VSS 158 123 VDD 166 OAI211X1TR $T=84000 25200 0 0 $X=83660 $Y=24920
X93 73 159 VSS 122 124 VDD 161 OAI211X1TR $T=85200 25200 1 0 $X=84860 $Y=21200
X94 73 126 VSS 129 130 VDD 127 OAI211X1TR $T=89600 32400 0 0 $X=89260 $Y=32120
X95 addr<1> VDD scan_en 79 addr<2> 136 VSS AOI22XLTR $T=28000 25200 0 0 $X=27660 $Y=24920
X96 addr<2> VDD scan_en 79 addr<3> 76 VSS AOI22XLTR $T=30000 18000 0 0 $X=29660 $Y=17720
X97 addr<6> VDD scan_en 79 addr<7> 122 VSS AOI22XLTR $T=82000 25200 1 0 $X=81660 $Y=21200
X98 addr<8> VDD scan_en 79 addr<9> 129 VSS AOI22XLTR $T=86800 32400 0 0 $X=86460 $Y=32120
X99 addr<7> VDD scan_en 79 addr<8> 158 VSS AOI22XLTR $T=90400 32400 0 180 $X=87660 $Y=28400
X100 addr<0> VDD scan_en 79 addr<1> VSS 74 AOI22X1TR $T=30400 32400 1 180 $X=27660 $Y=32120
X101 addr<3> VDD scan_en 79 addr<4> VSS 82 AOI22X1TR $T=32400 18000 0 0 $X=32060 $Y=17720
X102 addr<4> VDD scan_en 79 addr<5> VSS 85 AOI22X1TR $T=40400 25200 0 0 $X=40060 $Y=24920
X103 scan_in VDD scan_en 79 addr<0> VSS 146 AOI22X1TR $T=64400 39600 0 180 $X=61660 $Y=35600
X104 addr<5> VDD scan_en 79 addr<6> VSS 120 AOI22X1TR $T=74400 25200 1 0 $X=74060 $Y=21200
X105 VSS 78 addr<0> VDD 163 OR2X1TR $T=30800 39600 0 0 $X=30460 $Y=39320
X106 VSS addr<9> 121 VDD 165 OR2X1TR $T=84000 39600 0 180 $X=81660 $Y=35600
X107 163 61 VSS VDD Rlink<0> AND2X1TR $T=32800 39600 0 0 $X=32460 $Y=39320
X108 br disp<8> VSS VDD 118 AND2X1TR $T=74800 39600 0 0 $X=74460 $Y=39320
X109 br disp<9> VSS VDD 121 AND2X1TR $T=78000 39600 0 0 $X=77660 $Y=39320
X110 79 scan_en VSS resetn stall VDD NOR3BX2TR $T=43600 39600 0 0 $X=43260 $Y=39320
X111 86 84 VDD 89 VSS 140 AOI21X1TR $T=46000 18000 1 180 $X=43660 $Y=17720
X112 84 95 VDD 102 VSS 164 AOI21X1TR $T=53600 18000 1 0 $X=53260 $Y=14000
X113 147 84 VDD 148 VSS 109 AOI21X1TR $T=63600 10800 1 0 $X=63260 $Y=6800
X114 106 102 VDD 107 VSS 149 AOI21X1TR $T=63600 18000 1 0 $X=63260 $Y=14000
X115 116 108 VDD 112 VSS 111 AOI21X1TR $T=71600 32400 1 180 $X=69260 $Y=32120
X116 clk VSS 66 VDD CLKBUFX20TR $T=45200 32400 0 0 $X=44860 $Y=32120
X117 jmp 91 VSS 73 VDD NAND2BX1TR $T=47200 39600 1 0 $X=46860 $Y=35600
X118 disp<0> br VSS 78 VDD NAND2BX1TR $T=54400 39600 1 180 $X=52060 $Y=39320
X119 91 jmp VDD VSS 96 AND2X2TR $T=49200 39600 1 0 $X=48860 $Y=35600
X120 br disp<2> VDD VSS 67 AND2X2TR $T=53600 39600 0 180 $X=51260 $Y=35600
X121 br disp<3> VDD VSS 69 AND2X2TR $T=55600 39600 0 180 $X=53260 $Y=35600
X122 br disp<1> VDD VSS 71 AND2X2TR $T=56400 39600 1 180 $X=54060 $Y=39320
X123 br disp<4> VDD VSS 93 AND2X2TR $T=57600 39600 0 180 $X=55260 $Y=35600
X124 br disp<5> VDD VSS 142 AND2X2TR $T=59600 39600 0 180 $X=57260 $Y=35600
X125 br disp<6> VDD VSS 150 AND2X2TR $T=70400 39600 0 180 $X=68060 $Y=35600
X126 br disp<7> VDD VSS 153 AND2X2TR $T=70400 39600 1 0 $X=70060 $Y=35600
X127 94 98 95 VSS VDD NOR2X2TR $T=53200 18000 1 180 $X=50860 $Y=17720
X128 99 VSS 97 88 VDD NAND2XLTR $T=54400 25200 0 180 $X=52460 $Y=21200
X129 62 VSS VDD 84 CLKINVX6TR $T=59200 18000 0 0 $X=58860 $Y=17720
X130 151 VSS 109 VDD Rlink<7> XOR2X2TR $T=71200 10800 1 180 $X=65660 $Y=10520
X131 119 VSS 111 VDD Rlink<9> XOR2X2TR $T=77600 39600 0 180 $X=72060 $Y=35600
X132 114 108 VSS VDD Rlink<8> XNOR2X2TR $T=71600 32400 0 180 $X=66060 $Y=28400
X133 addr<8> 118 VSS VDD 116 OR2X2TR $T=74400 32400 0 0 $X=74060 $Y=32120
X134 160 161 66 addr<7> VSS VDD 190 DFFTRX2TR $T=85600 18000 0 0 $X=85260 $Y=17720
X135 VSS VDD 160 TIEHITR $T=87600 18000 0 180 $X=86060 $Y=14000
X136 addr<9> scan_en 66 scan_out VSS VDD 191 DFFTRX1TR $T=86800 39600 0 0 $X=86460 $Y=39320
.ENDS
***************************************
.SUBCKT DFFXLTR D CK Q VSS VDD QN
** N=13 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI21XLTR A1 VSS A0 B0 VDD Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AO22X1TR B1 B0 A0 A1 VDD VSS Y
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI22X1TR B1 VSS B0 A0 A1 VDD Y
** N=9 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_23 1 2 3 4 5 6 7 8 9
** N=9 EP=9 IP=12 FDC=0
X0 1 2 3 4 5 NOR2X1TR $T=0 0 0 0 $X=-340 $Y=-280
X1 6 2 7 8 5 NOR2X1TR $T=1600 0 0 0 $X=1260 $Y=-280
.ENDS
***************************************
.SUBCKT NAND3X1TR C VSS B VDD A Y
** N=7 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OR2XLTR A B VDD VSS Y
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI31X1TR VSS A2 A1 A0 B0 VDD Y
** N=9 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI33X1TR B2 B1 B0 VDD A0 A1 VSS Y A2
** N=11 EP=9 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI2BB1X1TR A1N A0N B0 VSS Y VDD
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND2X2TR VSS A B Y VDD
** N=6 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI33XLTR B2 B1 B0 VSS A0 A1 A2 VDD Y
** N=10 EP=9 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR4BBX1TR AN D C Y VSS BN VDD
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI31X1TR A2 A1 A0 VDD B0 VSS Y
** N=9 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI32X1TR A2 A1 A0 VDD B0 Y B1 VSS
** N=10 EP=8 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AOI211X1TR A1 A0 VDD C0 B0 VSS Y
** N=9 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND3BX1TR AN C VSS B VDD Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR2BX1TR AN B VDD Y VSS
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI32X1TR VSS A2 A1 A0 B0 Y VDD B1
** N=10 EP=8 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR4XLTR VSS D VDD C B Y A
** N=8 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKINVX4TR A Y VSS VDD
** N=5 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR3BX1TR AN C VDD B VSS Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT DFFX1TR D CK Q VDD VSS QN
** N=13 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AND3X2TR A B C VSS VDD Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND3XLTR C VSS B A VDD Y
** N=7 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKBUFX8TR A VSS Y VDD
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND4X1TR D VSS C B Y A VDD
** N=8 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT CLKBUFX2TR A VSS VDD Y
** N=6 EP=4 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OR3X2TR A B C VDD VSS Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OAI211X4TR A1 A0 C0 B0 Y VSS VDD
** N=10 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OR3X1TR A B C VDD VSS Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR2XLTR B VDD A Y VSS
** N=6 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NOR2BXLTR AN B VDD Y VSS
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT OA21XLTR A1 A0 B0 VSS VDD Y
** N=8 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AND2XLTR A B VSS VDD Y
** N=7 EP=5 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT NAND4XLTR D VSS C B Y A VDD
** N=8 EP=7 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT AO21XLTR A1 A0 B0 VSS VDD Y
** N=9 EP=6 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT controllerLING VDD VSS RWL_P<10> Dsram<10> Dsram<13> RWL_P<11> RWL_P<12> RWL_P<13> RWL_P<14> RWL_N<10> RWL_N<11> RWL_N<13> RWLB_P<11> RWLB_P<15> RWLB_N<10> RENB_SRAM RWLB_N<12> RWLB_N<15> Dxac<1> Dxac<4>
+ REN_SRAM Col_Sel<0> Col_Sel<11> Col_Sel<14> Col_Sel<10> RWLB_P<12> RWLB_P<10> RWLB_N<0> Col_Sel<4> RWLB_N<1> Col_Sel<5> Col_Sel<1> RWLB_P<14> Dsram<14> Dsram<15> Col_Sel<15> Dxac<5> Dxac<2> RWLB_P<13> Col_Sel<12>
+ Col_Sel<13> Dsram<12> RWLB_N<13> RWLB_P<0> RWL_P<15> RWLB_N<11> rSrcIn<14> RWL_P<1> RWL_N<15> rSrcIn<11> RWLB_N<14> RWL_N<12> RWL_N<14> Dxac<0> Col_Sel<3> RWL_P<2> RWL_N<0> rSrcIn<13> Col_Sel<2> Dsram<11>
+ rSrcIn<2> RWLB_P<1> Dxac<3> rSrcIn<0> rDestIn<12> rSrcIn<7> RWL_N<1> rSrcIn<1> RWL_P<0> rDestIn<15> RWLB_P<7> Dxac<6> rSrcIn<9> Dxac<11> RWL_N<9> Dxac<15> Dxac<8> Dxac<7> Dxac<12> rSrcIn<8>
+ rDestIn<10> Dxac<14> RWLB_N<9> Col_Sel<7> WR_EN_SRAM WR_Data<10> RWLB_P<9> Dxac<10> WEN Dxac<9> WR_Data<0> Dxac<21> Dxac<17> rDestIn<9> WR_Data<2> RWL_P<4> Dxac<16> WR_Data<9> rSrcIn<12> Dxac<26>
+ Dxac<18> Dxac<20> RWLB_N<4> WR_Data<12> Dxac<23> instruIn<6> WR_Data<4> rDestIn<4> Dxac<25> Dxac<29> WR_Data<6> Dxac<24> Dxac<28> instruIn<7> imm8_disp<6> Dxac<30> clk Dxac<31> instruIn<11> imm8_disp<5>
+ imm8_disp<4> Br Dsram<0> FunISEL<0> DOut<0> imm8_disp<7> DOut<2> FunISEL<1> instruIn<10> pc<0> DIn<1> instruIn<3> instruIn<1> DOut<1> AluSEL<1> instruIn<2> DIn<2> Dsram<3> pc<4> instruIn<0>
+ Amount<1> N imm8_disp<1> DSEL<0> WE<5> LuiSEL ReadB<15> ReadA<1> DOut<4> imm8_disp<0> DSEL<1> pc<6> ReadB<10> C Z F ReadB<6> ReadB<5> ReadB<13> pc<7>
+ imm8_disp<3> ReadA<15> ReadB<8> ReadB<2> WE<15> pc<8> ReadB<11> DIn<3> imm8_disp<2> ReadB<7> ReadB<3> AluSEL<0> WEMaster ReadB<0> ReadB<4> ReadB<9> ReadB<12> DIn<4> WE<10> WE<6>
+ WE<1> WE<2> DOut<5> WE<14> DIn<9> DOut<6> MovSEL DOut<9> WE<8> DOut<7> WE<7> WE<9> DIn<11> WE<3> DIn<10> DOut<8> DOut<3> DIn<12> DIn<6> DIn<0>
+ DIn<15> WE<11> DIn<14> WE<4> DOut<10> DOut<14> DIn<5> DOut<11> DOut<13> DIn<8> DIn<13> DOut<15> Dsram<5> Dsram<6> ReadB<14> DIn<7> WE<13> Amount<0> Dsram<9> WE<12>
+ DOut<12> WE<0> Dsram<7> Dsram<8> SEL1 rSrcIn<10> rSrcIn<15> RWLB_P<2> RWLB_N<2> RWL_N<2> rSrcIn<3> RWLB_P<3> Col_Sel<9> RWL_N<3> RWLB_N<3> RWL_P<3> rDestIn<13> rDestIn<14> RWL_N<7> RWLB_N<6>
+ Col_Sel<8> RWLB_P<6> RWLB_N<7> rSrcIn<6> CEN RWL_N<6> RWL_N<8> RWL_P<6> Col_Sel<6> RWLB_N<5> RWL_P<7> rDestIn<11> RWLB_P<5> WR_Data<11> RWLB_P<8> RWL_P<9> rSrcIn<5> WR_Data<13> RWLB_N<8> RWL_N<5>
+ Dxac<13> RWL_P<8> Dxac<27> RWLB_P<4> rDestIn<0> RWL_P<5> rDestIn<1> rDestIn<2> WR_Data<1> Dxac<19> rDestIn<5> rSrcIn<4> Dxac<22> RWL_N<4> WR_Data<5> WR_Data<3> rDestIn<3> instruIn<5> rDestIn<6> WR_Data<8>
+ instruIn<4> WR_Data<7> rDestIn<8> rDestIn<7> Dsram<4> WR_Data<14> Dsram<1> Dsram<2> instruIn<13> instruIn<12> Stall instruIn<14> instruIn<15> Jmp WR_Data<15> pc<1> instruIn<9> pc<2> ReadA<10> pc<3>
+ ReadA<6> ReadA<8> ReadA<14> ReadA<4> instruIn<8> ReadA<12> ReadA<2> pc<5> ReadA<0> ReadA<5> ReadA<13> ReadA<9> ReadA<11> ReadB<1> ReadA<7> pc<9> ReadA<3>
** N=705 EP=317 IP=3207 FDC=0
X0 463 VDD imm8_disp<6> 455 VSS 591 NOR3X1TR $T=61200 25200 0 180 $X=58860 $Y=21200
X1 460 VDD FunISEL<0> 463 VSS 449 NOR3X1TR $T=63200 18000 1 180 $X=60860 $Y=17720
X2 493 VDD 495 485 VSS 467 NOR3X1TR $T=71600 32400 0 180 $X=69260 $Y=28400
X3 492 VDD 493 486 VSS LuiSEL NOR3X1TR $T=74800 25200 0 0 $X=74460 $Y=24920
X4 Dxac<1> 359 VSS 382 Dxac<0> 373 VDD NAND4BX1TR $T=24400 46800 0 0 $X=24060 $Y=46520
X5 rSrcIn<14> VSS VDD 555 CLKINVX2TR $T=10800 25200 0 0 $X=10460 $Y=24920
X6 rSrcIn<10> VSS VDD 331 CLKINVX2TR $T=12800 10800 0 180 $X=11260 $Y=6800
X7 rSrcIn<11> VSS VDD 333 CLKINVX2TR $T=13600 10800 1 180 $X=12060 $Y=10520
X8 rSrcIn<15> VSS VDD 556 CLKINVX2TR $T=14400 18000 0 180 $X=12860 $Y=14000
X9 rSrcIn<13> VSS VDD 332 CLKINVX2TR $T=15600 18000 1 180 $X=14060 $Y=17720
X10 rSrcIn<12> VSS VDD 335 CLKINVX2TR $T=16400 18000 0 180 $X=14860 $Y=14000
X11 rSrcIn<2> VSS VDD 348 CLKINVX2TR $T=17600 18000 1 0 $X=17260 $Y=14000
X12 Dxac<2> VSS VDD 359 CLKINVX2TR $T=19600 61200 1 0 $X=19260 $Y=57200
X13 rSrcIn<3> VSS VDD 356 CLKINVX2TR $T=24000 18000 1 0 $X=23660 $Y=14000
X14 Dxac<3> VSS VDD 570 CLKINVX2TR $T=27200 46800 0 0 $X=26860 $Y=46520
X15 rSrcIn<1> VSS VDD 380 CLKINVX2TR $T=30400 25200 0 0 $X=30060 $Y=24920
X16 rSrcIn<0> VSS VDD 378 CLKINVX2TR $T=30800 32400 1 0 $X=30460 $Y=28400
X17 Dxac<6> VSS VDD 389 CLKINVX2TR $T=32000 54000 1 0 $X=31660 $Y=50000
X18 rSrcIn<6> VSS VDD 394 CLKINVX2TR $T=33200 10800 1 0 $X=32860 $Y=6800
X19 rSrcIn<7> VSS VDD 572 CLKINVX2TR $T=35200 18000 1 0 $X=34860 $Y=14000
X20 390 VSS VDD 391 CLKINVX2TR $T=35200 39600 1 0 $X=34860 $Y=35600
X21 Dxac<10> VSS VDD 403 CLKINVX2TR $T=38000 46800 0 0 $X=37660 $Y=46520
X22 388 VSS VDD 573 CLKINVX2TR $T=38400 54000 1 0 $X=38060 $Y=50000
X23 Dxac<8> VSS VDD 396 CLKINVX2TR $T=40400 32400 0 0 $X=40060 $Y=32120
X24 rSrcIn<9> VSS VDD 574 CLKINVX2TR $T=41200 25200 0 0 $X=40860 $Y=24920
X25 Dxac<17> VSS VDD 413 CLKINVX2TR $T=41200 32400 1 0 $X=40860 $Y=28400
X26 rSrcIn<5> VSS VDD 576 CLKINVX2TR $T=41600 10800 1 0 $X=41260 $Y=6800
X27 Dxac<21> VSS VDD 619 CLKINVX2TR $T=41600 32400 0 0 $X=41260 $Y=32120
X28 Dxac<13> VSS VDD 580 CLKINVX2TR $T=42000 61200 1 0 $X=41660 $Y=57200
X29 rSrcIn<8> VSS VDD 578 CLKINVX2TR $T=42800 25200 1 0 $X=42460 $Y=21200
X30 Dxac<9> VSS VDD 577 CLKINVX2TR $T=44800 54000 0 180 $X=43260 $Y=50000
X31 Dxac<20> VSS VDD 423 CLKINVX2TR $T=45600 25200 0 0 $X=45260 $Y=24920
X32 436 VSS VDD 579 CLKINVX2TR $T=47200 39600 1 180 $X=45660 $Y=39320
X33 406 VSS VDD 401 CLKINVX2TR $T=47600 46800 0 180 $X=46060 $Y=42800
X34 Dxac<27> VSS VDD 420 CLKINVX2TR $T=46400 54000 1 0 $X=46060 $Y=50000
X35 Dxac<26> VSS VDD 425 CLKINVX2TR $T=48400 39600 0 180 $X=46860 $Y=35600
X36 Dxac<22> VSS VDD 426 CLKINVX2TR $T=48400 32400 1 0 $X=48060 $Y=28400
X37 Dxac<25> VSS VDD 582 CLKINVX2TR $T=48800 61200 1 0 $X=48460 $Y=57200
X38 Dxac<19> VSS VDD 585 CLKINVX2TR $T=49200 25200 0 0 $X=48860 $Y=24920
X39 Dxac<23> VSS VDD 583 CLKINVX2TR $T=49200 32400 0 0 $X=48860 $Y=32120
X40 434 VSS VDD 584 CLKINVX2TR $T=51600 39600 0 180 $X=50060 $Y=35600
X41 428 VSS VDD 587 CLKINVX2TR $T=51600 39600 1 0 $X=51260 $Y=35600
X42 Dxac<29> VSS VDD 586 CLKINVX2TR $T=51600 61200 1 0 $X=51260 $Y=57200
X43 rSrcIn<4> VSS VDD 588 CLKINVX2TR $T=52000 10800 0 0 $X=51660 $Y=10520
X44 325 VSS VDD 334 CLKINVX2TR $T=52000 18000 1 0 $X=51660 $Y=14000
X45 Dxac<16> VSS VDD 433 CLKINVX2TR $T=55600 61200 1 0 $X=55260 $Y=57200
X46 429 VSS VDD 546 CLKINVX2TR $T=57200 32400 1 0 $X=56860 $Y=28400
X47 449 VSS VDD 474 CLKINVX2TR $T=62800 25200 1 0 $X=62460 $Y=21200
X48 473 VSS VDD WR_EN_SRAM CLKINVX2TR $T=65200 39600 1 180 $X=63660 $Y=39320
X49 476 VSS VDD 482 CLKINVX2TR $T=68000 10800 1 0 $X=67660 $Y=6800
X50 598 VSS VDD 495 CLKINVX2TR $T=71600 32400 0 0 $X=71260 $Y=32120
X51 494 VSS VDD 599 CLKINVX2TR $T=71600 61200 1 0 $X=71260 $Y=57200
X52 490 VSS VDD 497 CLKINVX2TR $T=72400 10800 0 0 $X=72060 $Y=10520
X53 479 VSS VDD 493 CLKINVX2TR $T=74000 32400 1 0 $X=73660 $Y=28400
X54 602 VSS VDD 600 CLKINVX2TR $T=75600 18000 1 180 $X=74060 $Y=17720
X55 472 VSS VDD 504 CLKINVX2TR $T=74400 54000 1 0 $X=74060 $Y=50000
X56 Dsram<3> VSS VDD 604 CLKINVX2TR $T=75200 32400 1 0 $X=74860 $Y=28400
X57 513 VSS VDD 519 CLKINVX2TR $T=80400 54000 1 0 $X=80060 $Y=50000
X58 523 VSS VDD 605 CLKINVX2TR $T=81600 61200 0 180 $X=80060 $Y=57200
X59 524 VSS VDD 510 CLKINVX2TR $T=83600 61200 1 0 $X=83260 $Y=57200
X60 imm8_disp<1> VSS VDD 609 CLKINVX2TR $T=85200 18000 0 0 $X=84860 $Y=17720
X61 imm8_disp<0> VSS VDD 608 CLKINVX2TR $T=86400 25200 0 180 $X=84860 $Y=21200
X62 502 VSS VDD 500 CLKINVX2TR $T=92000 39600 1 180 $X=90460 $Y=39320
X63 imm8_disp<2> VSS VDD 539 CLKINVX2TR $T=94000 18000 1 0 $X=93660 $Y=14000
X64 468 VSS VDD WEMaster CLKINVX2TR $T=94400 39600 1 0 $X=94060 $Y=35600
X65 imm8_disp<3> VSS VDD 542 CLKINVX2TR $T=100400 10800 1 180 $X=98860 $Y=10520
X66 Dsram<5> VSS VDD 614 CLKINVX2TR $T=100800 32400 1 180 $X=99260 $Y=32120
X67 Dsram<6> VSS VDD 615 CLKINVX2TR $T=105200 39600 1 180 $X=103660 $Y=39320
X68 Dsram<9> VSS VDD 626 CLKINVX2TR $T=107600 32400 0 180 $X=106060 $Y=28400
X69 Dsram<8> VSS VDD 617 CLKINVX2TR $T=108000 25200 1 180 $X=106460 $Y=24920
X70 Dsram<7> VSS VDD 627 CLKINVX2TR $T=108000 39600 0 180 $X=106460 $Y=35600
X71 REN_SRAM VSS VDD RENB_SRAM INVX2TR $T=8400 39600 1 180 $X=6860 $Y=39320
X72 618 VSS VDD CEN INVX2TR $T=36000 61200 0 180 $X=34460 $Y=57200
X73 491 VSS VDD 485 INVX2TR $T=71600 32400 1 180 $X=70060 $Y=32120
X74 480 VSS VDD 486 INVX2TR $T=75200 25200 1 0 $X=74860 $Y=21200
X75 493 480 VSS 492 474 imm8_disp<5> 470 VDD MovSEL OAI33X1TR $T=73200 25200 0 180 $X=69260 $Y=21200
X76 rDestIn<10> WR_EN_SRAM VSS VDD WR_Data<10> CLKAND2X2TR $T=36000 54000 0 0 $X=35660 $Y=53720
X77 rDestIn<11> WR_EN_SRAM VSS VDD WR_Data<11> CLKAND2X2TR $T=38000 54000 0 0 $X=37660 $Y=53720
X78 rDestIn<13> WR_EN_SRAM VSS VDD WR_Data<13> CLKAND2X2TR $T=40000 54000 0 0 $X=39660 $Y=53720
X79 rDestIn<9> WR_EN_SRAM VSS VDD WR_Data<9> CLKAND2X2TR $T=42000 54000 0 0 $X=41660 $Y=53720
X80 rDestIn<0> WR_EN_SRAM VSS VDD WR_Data<0> CLKAND2X2TR $T=44000 54000 0 0 $X=43660 $Y=53720
X81 rDestIn<12> WR_EN_SRAM VSS VDD WR_Data<12> CLKAND2X2TR $T=44400 46800 0 0 $X=44060 $Y=46520
X82 rDestIn<1> WR_EN_SRAM VSS VDD WR_Data<1> CLKAND2X2TR $T=46000 54000 0 0 $X=45660 $Y=53720
X83 rDestIn<2> WR_EN_SRAM VSS VDD WR_Data<2> CLKAND2X2TR $T=46400 46800 0 0 $X=46060 $Y=46520
X84 rDestIn<5> WR_EN_SRAM VSS VDD WR_Data<5> CLKAND2X2TR $T=48000 54000 0 0 $X=47660 $Y=53720
X85 rDestIn<4> WR_EN_SRAM VSS VDD WR_Data<4> CLKAND2X2TR $T=50400 46800 1 180 $X=48060 $Y=46520
X86 rDestIn<3> WR_EN_SRAM VSS VDD WR_Data<3> CLKAND2X2TR $T=52000 54000 1 180 $X=49660 $Y=53720
X87 rDestIn<6> WR_EN_SRAM VSS VDD WR_Data<6> CLKAND2X2TR $T=54000 54000 1 180 $X=51660 $Y=53720
X88 rDestIn<8> WR_EN_SRAM VSS VDD WR_Data<8> CLKAND2X2TR $T=55600 61200 0 180 $X=53260 $Y=57200
X89 rDestIn<7> WR_EN_SRAM VSS VDD WR_Data<7> CLKAND2X2TR $T=56000 54000 1 180 $X=53660 $Y=53720
X90 422 334 VSS VDD 429 CLKAND2X2TR $T=55200 32400 0 0 $X=54860 $Y=32120
X91 rDestIn<14> WR_EN_SRAM VSS VDD WR_Data<14> CLKAND2X2TR $T=56800 61200 1 0 $X=56460 $Y=57200
X92 rDestIn<15> WR_EN_SRAM VSS VDD WR_Data<15> CLKAND2X2TR $T=62400 61200 1 0 $X=62060 $Y=57200
X93 498 477 VSS VDD 478 CLKAND2X2TR $T=71200 25200 1 180 $X=68860 $Y=24920
X94 DIn<4> 487 VSS VDD 516 CLKAND2X2TR $T=98000 18000 0 180 $X=95660 $Y=14000
X95 606 510 509 VDD VSS XNOR2X1TR $T=78400 46800 0 180 $X=74860 $Y=42800
X96 521 524 522 VDD VSS XNOR2X1TR $T=85600 46800 1 0 $X=85260 $Y=42800
X97 319 VSS VDD RWLB_N<10> CLKINVX1TR $T=8400 39600 0 180 $X=6860 $Y=35600
X98 323 VSS VDD RWLB_N<15> CLKINVX1TR $T=8400 46800 0 180 $X=6860 $Y=42800
X99 553 VSS VDD RWLB_N<12> CLKINVX1TR $T=9600 39600 1 180 $X=8060 $Y=39320
X100 554 VSS VDD RWLB_N<13> CLKINVX1TR $T=10800 39600 1 180 $X=9260 $Y=39320
X101 318 VSS VDD RWLB_N<11> CLKINVX1TR $T=12000 39600 1 180 $X=10460 $Y=39320
X102 337 VSS VDD RWLB_N<14> CLKINVX1TR $T=13200 39600 1 180 $X=11660 $Y=39320
X103 355 VSS VDD RWLB_N<0> CLKINVX1TR $T=18800 32400 0 180 $X=17260 $Y=28400
X104 344 VSS VDD RWLB_N<2> CLKINVX1TR $T=19200 10800 0 180 $X=17660 $Y=6800
X105 360 VSS VDD RWLB_N<3> CLKINVX1TR $T=25200 10800 0 180 $X=23660 $Y=6800
X106 567 VSS VDD RWLB_N<1> CLKINVX1TR $T=25200 32400 1 180 $X=23660 $Y=32120
X107 571 VSS VDD RWLB_N<6> CLKINVX1TR $T=28400 10800 1 0 $X=28060 $Y=6800
X108 392 VSS VDD RWLB_N<7> CLKINVX1TR $T=34400 18000 0 180 $X=32860 $Y=14000
X109 404 VSS VDD RWLB_N<5> CLKINVX1TR $T=39600 10800 0 180 $X=38060 $Y=6800
X110 405 VSS VDD RWLB_N<9> CLKINVX1TR $T=40000 25200 1 180 $X=38460 $Y=24920
X111 410 VSS VDD RWLB_N<8> CLKINVX1TR $T=41200 18000 1 0 $X=40860 $Y=14000
X112 427 VSS VDD RWLB_N<4> CLKINVX1TR $T=55200 10800 0 180 $X=53660 $Y=6800
X113 331 VSS 325 322 VDD RWL_P<10> OAI21X1TR $T=9600 10800 0 180 $X=7260 $Y=6800
X114 322 VSS 326 318 VDD RWLB_P<11> OAI21X1TR $T=7600 32400 1 0 $X=7260 $Y=28400
X115 322 VSS 327 553 VDD RWLB_P<12> OAI21X1TR $T=7600 32400 0 0 $X=7260 $Y=32120
X116 333 VSS 325 322 VDD RWL_P<11> OAI21X1TR $T=10000 10800 1 180 $X=7660 $Y=10520
X117 332 VSS 325 322 VDD RWL_P<13> OAI21X1TR $T=10000 18000 1 180 $X=7660 $Y=17720
X118 322 VSS 329 319 VDD RWLB_P<10> OAI21X1TR $T=8000 25200 0 0 $X=7660 $Y=24920
X119 322 VSS 330 554 VDD RWLB_P<13> OAI21X1TR $T=8400 39600 1 0 $X=8060 $Y=35600
X120 556 VSS 325 322 VDD RWL_P<15> OAI21X1TR $T=13200 18000 0 180 $X=10860 $Y=14000
X121 322 VSS 338 323 VDD RWLB_P<15> OAI21X1TR $T=13600 39600 0 180 $X=11260 $Y=35600
X122 348 VSS 325 322 VDD RWL_P<2> OAI21X1TR $T=14800 10800 0 180 $X=12460 $Y=6800
X123 322 VSS 345 337 VDD RWLB_P<14> OAI21X1TR $T=16000 32400 1 180 $X=13660 $Y=32120
X124 322 VSS 559 344 VDD RWLB_P<2> OAI21X1TR $T=16000 10800 1 0 $X=15660 $Y=6800
X125 322 VSS 357 360 VDD RWLB_P<3> OAI21X1TR $T=20400 10800 1 0 $X=20060 $Y=6800
X126 322 VSS 368 355 VDD RWLB_P<0> OAI21X1TR $T=24800 32400 0 180 $X=22460 $Y=28400
X127 322 VSS 566 567 VDD RWLB_P<1> OAI21X1TR $T=24800 32400 1 0 $X=24460 $Y=28400
X128 356 VSS 325 322 VDD RWL_P<3> OAI21X1TR $T=25200 10800 1 0 $X=24860 $Y=6800
X129 322 VSS 377 571 VDD RWLB_P<6> OAI21X1TR $T=28000 10800 0 0 $X=27660 $Y=10520
X130 380 VSS 325 322 VDD RWL_P<1> OAI21X1TR $T=29600 32400 0 0 $X=29260 $Y=32120
X131 322 VSS 387 392 VDD RWLB_P<7> OAI21X1TR $T=31200 18000 0 0 $X=30860 $Y=17720
X132 378 VSS 325 322 VDD RWL_P<0> OAI21X1TR $T=31600 25200 0 0 $X=31260 $Y=24920
X133 394 VSS 325 322 VDD RWL_P<6> OAI21X1TR $T=35200 10800 1 0 $X=34860 $Y=6800
X134 322 VSS 402 404 VDD RWLB_P<5> OAI21X1TR $T=36800 10800 0 0 $X=36460 $Y=10520
X135 322 VSS 399 405 VDD RWLB_P<9> OAI21X1TR $T=36800 25200 0 0 $X=36460 $Y=24920
X136 572 VSS 325 322 VDD RWL_P<7> OAI21X1TR $T=37200 18000 1 0 $X=36860 $Y=14000
X137 322 VSS 397 410 VDD RWLB_P<8> OAI21X1TR $T=39200 18000 0 0 $X=38860 $Y=17720
X138 574 VSS 325 322 VDD RWL_P<9> OAI21X1TR $T=42400 25200 0 180 $X=40060 $Y=21200
X139 578 VSS 325 322 VDD RWL_P<8> OAI21X1TR $T=43600 18000 0 0 $X=43260 $Y=17720
X140 576 VSS 325 322 VDD RWL_P<5> OAI21X1TR $T=45200 10800 1 0 $X=44860 $Y=6800
X141 322 VSS 398 427 VDD RWLB_P<4> OAI21X1TR $T=47200 10800 0 0 $X=46860 $Y=10520
X142 408 VSS Dxac<16> 584 VDD 432 OAI21X1TR $T=48400 39600 1 0 $X=48060 $Y=35600
X143 588 VSS 325 322 VDD RWL_P<4> OAI21X1TR $T=51600 10800 0 180 $X=49260 $Y=6800
X144 586 VSS 441 439 VDD 447 OAI21X1TR $T=52800 54000 1 0 $X=52460 $Y=50000
X145 441 VSS Dxac<29> 430 VDD 590 OAI21X1TR $T=56400 39600 1 180 $X=54060 $Y=39320
X146 595 VSS 460 485 VDD 489 OAI21X1TR $T=67200 18000 0 0 $X=66860 $Y=17720
X147 331 VSS 319 334 VDD NAND2X1TR $T=9600 10800 1 0 $X=9260 $Y=6800
X148 335 VSS 553 334 VDD NAND2X1TR $T=9600 32400 1 0 $X=9260 $Y=28400
X149 332 VSS 554 334 VDD NAND2X1TR $T=9600 32400 0 0 $X=9260 $Y=32120
X150 333 VSS 318 334 VDD NAND2X1TR $T=10000 10800 0 0 $X=9660 $Y=10520
X151 556 VSS 323 334 VDD NAND2X1TR $T=12800 32400 0 180 $X=10860 $Y=28400
X152 555 VSS 337 334 VDD NAND2X1TR $T=11600 32400 0 0 $X=11260 $Y=32120
X153 348 VSS 344 334 VDD NAND2X1TR $T=15200 10800 1 180 $X=13260 $Y=10520
X154 356 VSS 353 348 VDD NAND2X1TR $T=19200 18000 1 180 $X=17260 $Y=17720
X155 356 VSS 360 334 VDD NAND2X1TR $T=21600 10800 1 180 $X=19660 $Y=10520
X156 348 VSS 358 rSrcIn<3> VDD NAND2X1TR $T=20400 18000 1 0 $X=20060 $Y=14000
X157 rSrcIn<3> VSS 561 rSrcIn<2> VDD NAND2X1TR $T=20800 18000 0 0 $X=20460 $Y=17720
X158 352 VSS 347 334 VDD NAND2X1TR $T=22800 39600 0 180 $X=20860 $Y=35600
X159 356 VSS 371 rSrcIn<2> VDD NAND2X1TR $T=26000 18000 1 180 $X=24060 $Y=17720
X160 378 VSS 355 334 VDD NAND2X1TR $T=28400 32400 1 180 $X=26460 $Y=32120
X161 380 VSS 567 334 VDD NAND2X1TR $T=28800 32400 0 180 $X=26860 $Y=28400
X162 rSrcIn<0> VSS 361 rSrcIn<1> VDD NAND2X1TR $T=28800 25200 0 0 $X=28460 $Y=24920
X163 380 VSS 372 rSrcIn<0> VDD NAND2X1TR $T=29200 32400 1 0 $X=28860 $Y=28400
X164 378 VSS 367 rSrcIn<1> VDD NAND2X1TR $T=30000 25200 1 0 $X=29660 $Y=21200
X165 394 VSS 571 334 VDD NAND2X1TR $T=32400 10800 0 180 $X=30460 $Y=6800
X166 366 VSS 365 325 VDD NAND2X1TR $T=31600 32400 0 0 $X=31260 $Y=32120
X167 380 VSS 364 378 VDD NAND2X1TR $T=32000 32400 1 0 $X=31660 $Y=28400
X168 396 VSS 390 Dxac<4> VDD NAND2X1TR $T=34800 39600 1 180 $X=32860 $Y=39320
X169 572 VSS 392 334 VDD NAND2X1TR $T=37200 18000 0 0 $X=36860 $Y=17720
X170 576 VSS 404 334 VDD NAND2X1TR $T=42800 10800 0 0 $X=42460 $Y=10520
X171 574 VSS 405 334 VDD NAND2X1TR $T=42800 25200 0 0 $X=42460 $Y=24920
X172 578 VSS 410 334 VDD NAND2X1TR $T=44800 25200 1 0 $X=44460 $Y=21200
X173 426 VSS 415 Dxac<20> VDD NAND2X1TR $T=48400 32400 0 180 $X=46460 $Y=28400
X174 588 VSS 427 334 VDD NAND2X1TR $T=54400 10800 0 0 $X=54060 $Y=10520
X175 334 VSS 454 422 VDD NAND2X1TR $T=56800 39600 1 0 $X=56460 $Y=35600
X176 339 VSS 593 Dsram<4> VDD NAND2X1TR $T=60000 32400 1 180 $X=58060 $Y=32120
X177 339 VSS 456 Dsram<1> VDD NAND2X1TR $T=59200 61200 1 0 $X=58860 $Y=57200
X178 463 VSS WEN 458 VDD NAND2X1TR $T=61600 32400 0 180 $X=59660 $Y=28400
X179 339 VSS 459 Dsram<0> VDD NAND2X1TR $T=60000 39600 0 0 $X=59660 $Y=39320
X180 339 VSS 594 Dsram<2> VDD NAND2X1TR $T=60000 54000 1 0 $X=59660 $Y=50000
X181 470 VSS 464 449 VDD NAND2X1TR $T=62400 25200 1 180 $X=60460 $Y=24920
X182 463 VSS 595 470 VDD NAND2X1TR $T=66800 18000 1 180 $X=64860 $Y=17720
X183 493 VSS FunISEL<0> 501 VDD NAND2X1TR $T=76400 25200 1 0 $X=76060 $Y=21200
X184 499 VSS 508 506 VDD NAND2X1TR $T=79200 32400 0 0 $X=78860 $Y=32120
X185 510 VSS 530 513 VDD NAND2X1TR $T=83200 54000 0 180 $X=81260 $Y=50000
X186 608 VSS 505 imm8_disp<1> VDD NAND2X1TR $T=84400 18000 0 180 $X=82460 $Y=14000
X187 517 VSS DOut<4> 461 VDD NAND2X1TR $T=83200 32400 1 0 $X=82860 $Y=28400
X188 519 VSS 527 510 VDD NAND2X1TR $T=85600 54000 0 180 $X=83660 $Y=50000
X189 472 VSS 528 502 VDD NAND2X1TR $T=86400 61200 0 180 $X=84460 $Y=57200
X190 609 VSS 520 608 VDD NAND2X1TR $T=86800 18000 0 180 $X=84860 $Y=14000
X191 504 VSS 529 502 VDD NAND2X1TR $T=87200 54000 0 180 $X=85260 $Y=50000
X192 609 VSS 526 imm8_disp<0> VDD NAND2X1TR $T=88000 18000 1 0 $X=87660 $Y=14000
X193 504 VSS 535 500 VDD NAND2X1TR $T=88800 46800 1 0 $X=88460 $Y=42800
X194 524 VSS 532 513 VDD NAND2X1TR $T=90400 54000 1 180 $X=88460 $Y=53720
X195 500 VSS 538 472 VDD NAND2X1TR $T=90400 46800 1 0 $X=90060 $Y=42800
X196 539 VSS 507 imm8_disp<3> VDD NAND2X1TR $T=92400 10800 1 180 $X=90460 $Y=10520
X197 imm8_disp<3> VSS 518 imm8_disp<2> VDD NAND2X1TR $T=90800 18000 1 0 $X=90460 $Y=14000
X198 519 VSS 534 524 VDD NAND2X1TR $T=92000 39600 0 0 $X=91660 $Y=39320
X199 542 VSS 511 imm8_disp<2> VDD NAND2X1TR $T=95200 10800 1 180 $X=93260 $Y=10520
X200 542 VSS 525 539 VDD NAND2X1TR $T=97200 10800 1 180 $X=95260 $Y=10520
X201 imm8_disp<0> VSS 537 imm8_disp<1> VDD NAND2X1TR $T=98800 18000 1 0 $X=98460 $Y=14000
X202 353 VDD 367 559 VSS NOR2X1TR $T=19200 25200 0 180 $X=17260 $Y=21200
X203 561 VDD 367 345 VSS NOR2X1TR $T=20800 25200 0 180 $X=18860 $Y=21200
X204 561 VDD 372 330 VSS NOR2X1TR $T=21200 32400 0 180 $X=19260 $Y=28400
X205 367 VDD 358 329 VSS NOR2X1TR $T=22400 25200 0 180 $X=20460 $Y=21200
X206 561 VDD 361 338 VSS NOR2X1TR $T=20800 25200 0 0 $X=20460 $Y=24920
X207 561 VDD 364 327 VSS NOR2X1TR $T=21200 32400 1 0 $X=20860 $Y=28400
X208 361 VDD 358 326 VSS NOR2X1TR $T=24000 25200 0 180 $X=22060 $Y=21200
X209 353 VDD 361 357 VSS NOR2X1TR $T=24000 25200 1 0 $X=23660 $Y=21200
X210 367 VDD 371 377 VSS NOR2X1TR $T=27200 18000 0 0 $X=26860 $Y=17720
X211 Dxac<3> VDD 359 385 VSS NOR2X1TR $T=28400 46800 0 0 $X=28060 $Y=46520
X212 361 VDD 371 387 VSS NOR2X1TR $T=29600 18000 0 0 $X=29260 $Y=17720
X213 364 VDD 358 397 VSS NOR2X1TR $T=33200 25200 1 0 $X=32860 $Y=21200
X214 Dxac<8> VDD Dxac<4> 382 VSS NOR2X1TR $T=35200 54000 0 180 $X=33260 $Y=50000
X215 371 VDD 364 398 VSS NOR2X1TR $T=34000 18000 0 0 $X=33660 $Y=17720
X216 371 VDD 372 402 VSS NOR2X1TR $T=34800 25200 0 0 $X=34460 $Y=24920
X217 358 VDD 372 399 VSS NOR2X1TR $T=34800 32400 1 0 $X=34460 $Y=28400
X218 Dxac<12> VDD 396 406 VSS NOR2X1TR $T=40800 39600 0 0 $X=40460 $Y=39320
X219 Dxac<24> VDD 433 428 VSS NOR2X1TR $T=51600 54000 0 180 $X=49660 $Y=50000
X220 479 VDD 495 490 VSS NOR2X1TR $T=73600 25200 1 180 $X=71660 $Y=24920
X221 597 VDD 601 501 VSS NOR2X1TR $T=72800 32400 0 0 $X=72460 $Y=32120
X222 505 VDD 507 ReadA<10> VSS NOR2X1TR $T=75600 10800 1 0 $X=75260 $Y=6800
X223 505 VDD 511 ReadA<6> VSS NOR2X1TR $T=77600 10800 1 0 $X=77260 $Y=6800
X224 499 VDD 472 606 VSS NOR2X1TR $T=80000 46800 0 180 $X=78060 $Y=42800
X225 imm8_disp<4> VDD imm8_disp<5> 477 VSS NOR2X1TR $T=78800 25200 1 0 $X=78460 $Y=21200
X226 463 VDD 512 SEL1 VSS NOR2X1TR $T=78800 32400 1 0 $X=78460 $Y=28400
X227 518 VDD 505 ReadA<14> VSS NOR2X1TR $T=81200 10800 0 180 $X=79260 $Y=6800
X228 520 VDD 507 ReadA<8> VSS NOR2X1TR $T=82000 10800 1 180 $X=80060 $Y=10520
X229 511 VDD 520 ReadA<4> VSS NOR2X1TR $T=81600 10800 1 0 $X=81260 $Y=6800
X230 525 VDD 505 ReadA<2> VSS NOR2X1TR $T=84800 10800 0 180 $X=82860 $Y=6800
X231 518 VDD 526 ReadA<13> VSS NOR2X1TR $T=84000 10800 0 0 $X=83660 $Y=10520
X232 513 VDD 510 503 VSS NOR2X1TR $T=85600 46800 0 180 $X=83660 $Y=42800
X233 525 VDD 520 ReadA<0> VSS NOR2X1TR $T=86400 10800 0 180 $X=84460 $Y=6800
X234 498 VDD DSEL<0> DSEL<1> VSS NOR2X1TR $T=85600 32400 1 0 $X=85260 $Y=28400
X235 511 VDD 526 ReadA<5> VSS NOR2X1TR $T=87600 10800 1 180 $X=85660 $Y=10520
X236 525 VDD 526 ReadA<1> VSS NOR2X1TR $T=88800 10800 0 180 $X=86860 $Y=6800
X237 507 VDD 526 ReadA<9> VSS NOR2X1TR $T=90000 10800 1 180 $X=88060 $Y=10520
X238 537 VDD 507 ReadA<11> VSS NOR2X1TR $T=90800 10800 0 180 $X=88860 $Y=6800
X239 527 VDD 538 ReadB<8> VSS NOR2X1TR $T=90800 54000 1 0 $X=90460 $Y=50000
X240 518 VDD 537 ReadA<15> VSS NOR2X1TR $T=91600 10800 1 0 $X=91260 $Y=6800
X241 532 VDD 538 ReadB<11> VSS NOR2X1TR $T=92400 46800 0 0 $X=92060 $Y=46520
X242 535 VDD 532 ReadB<3> VSS NOR2X1TR $T=92800 61200 1 0 $X=92460 $Y=57200
X243 528 VDD 530 ReadB<14> VSS NOR2X1TR $T=93600 39600 0 0 $X=93260 $Y=39320
X244 537 VDD 511 ReadA<7> VSS NOR2X1TR $T=95600 10800 0 180 $X=93660 $Y=6800
X245 532 VDD 529 ReadB<7> VSS NOR2X1TR $T=94400 25200 1 0 $X=94060 $Y=21200
X246 525 VDD 537 ReadA<3> VSS NOR2X1TR $T=97200 10800 1 0 $X=96860 $Y=6800
X247 535 VDD 530 ReadB<2> VSS NOR2X1TR $T=103200 54000 1 180 $X=101260 $Y=53720
X248 529 VDD 527 ReadB<4> VSS NOR2X1TR $T=103200 54000 0 0 $X=102860 $Y=53720
X249 535 VDD 527 ReadB<0> VSS NOR2X1TR $T=106400 54000 1 180 $X=104460 $Y=53720
X250 instruIn<11> 336 VSS VDD 472 DFFQX1TR $T=58000 46800 0 0 $X=57660 $Y=46520
X251 instruIn<13> 336 VSS VDD 480 DFFQX1TR $T=61600 46800 1 0 $X=61260 $Y=42800
X252 instruIn<12> 336 VSS VDD 491 DFFQX1TR $T=64800 46800 0 0 $X=64460 $Y=46520
X253 instruIn<10> 336 VSS VDD 502 DFFQX1TR $T=68400 46800 1 0 $X=68060 $Y=42800
X254 instruIn<9> 484 VSS VDD 513 DFFQX1TR $T=73600 46800 0 0 $X=73260 $Y=46520
X255 N 484 VSS VDD 499 DFFQX1TR $T=80800 39600 0 180 $X=73660 $Y=35600
X256 instruIn<15> 484 VSS VDD 479 DFFQX1TR $T=75200 54000 0 0 $X=74860 $Y=53720
X257 instruIn<1> 484 VSS VDD imm8_disp<1> DFFQX1TR $T=77600 18000 0 0 $X=77260 $Y=17720
X258 C 484 VSS VDD 521 DFFQX1TR $T=87600 39600 0 180 $X=80460 $Y=35600
X259 Z 484 VSS VDD 506 DFFQX1TR $T=88000 32400 1 180 $X=80860 $Y=32120
X260 instruIn<0> 484 VSS VDD imm8_disp<0> DFFQX1TR $T=81600 25200 0 0 $X=81260 $Y=24920
X261 instruIn<8> 484 VSS VDD 524 DFFQX1TR $T=82000 54000 0 0 $X=81660 $Y=53720
X262 536 484 VSS VDD WE<5> DFFQX1TR $T=90000 46800 1 180 $X=82860 $Y=46520
X263 instruIn<3> 484 VSS VDD imm8_disp<3> DFFQX1TR $T=86400 18000 0 0 $X=86060 $Y=17720
X264 instruIn<2> 484 VSS VDD imm8_disp<2> DFFQX1TR $T=86800 25200 1 0 $X=86460 $Y=21200
X265 531 484 VSS VDD WE<10> DFFQX1TR $T=87200 32400 1 0 $X=86860 $Y=28400
X266 610 484 VSS VDD WE<15> DFFQX1TR $T=88000 32400 0 0 $X=87660 $Y=32120
X267 533 484 VSS VDD WE<13> DFFQX1TR $T=88400 25200 0 0 $X=88060 $Y=24920
X268 611 484 VSS VDD WE<6> DFFQX1TR $T=92000 46800 1 0 $X=91660 $Y=42800
X269 540 484 VSS VDD WE<14> DFFQX1TR $T=93600 18000 0 0 $X=93260 $Y=17720
X270 541 484 VSS VDD WE<9> DFFQX1TR $T=94000 32400 1 0 $X=93660 $Y=28400
X271 612 484 VSS VDD WE<2> DFFQX1TR $T=94400 61200 1 0 $X=94060 $Y=57200
X272 623 484 VSS VDD WE<1> DFFQX1TR $T=94800 54000 0 0 $X=94460 $Y=53720
X273 543 484 VSS VDD WE<11> DFFQX1TR $T=95600 25200 0 0 $X=95260 $Y=24920
X274 624 484 VSS VDD WE<8> DFFQX1TR $T=97200 39600 0 0 $X=96860 $Y=39320
X275 625 484 VSS VDD WE<4> DFFQX1TR $T=98000 46800 0 0 $X=97660 $Y=46520
X276 544 484 VSS VDD WE<3> DFFQX1TR $T=98400 54000 1 0 $X=98060 $Y=50000
X277 545 484 VSS VDD WE<7> DFFQX1TR $T=98800 46800 1 0 $X=98460 $Y=42800
X278 613 484 VSS VDD WE<12> DFFQX1TR $T=99200 25200 1 0 $X=98860 $Y=21200
X279 547 484 VSS VDD WE<0> DFFQX1TR $T=101200 61200 1 0 $X=100860 $Y=57200
X280 416 Dxac<13> VSS 412 575 VDD 414 OAI211X1TR $T=44000 46800 1 180 $X=41260 $Y=46520
X281 580 416 VSS 412 411 VDD 445 OAI211X1TR $T=46400 46800 0 180 $X=43660 $Y=42800
X282 423 Dxac<23> VSS 581 415 VDD 424 OAI211X1TR $T=48800 32400 1 180 $X=46060 $Y=32120
X283 417 Dxac<16> VSS 443 441 VDD 434 OAI211X1TR $T=52000 39600 0 0 $X=51660 $Y=39320
X284 587 442 VSS 431 443 VDD 444 OAI211X1TR $T=53200 46800 1 0 $X=52860 $Y=42800
X285 325 448 VSS 459 466 VDD DOut<0> OAI211X1TR $T=61200 39600 1 0 $X=60860 $Y=35600
X286 325 438 VSS 594 465 VDD DOut<2> OAI211X1TR $T=61600 39600 0 0 $X=61260 $Y=39320
X287 325 462 VSS 456 488 VDD DOut<1> OAI211X1TR $T=72400 39600 0 0 $X=72060 $Y=39320
X288 604 322 VSS 450 607 VDD DOut<3> OAI211X1TR $T=76800 32400 0 0 $X=76460 $Y=32120
X289 614 322 VSS 454 548 VDD DOut<5> OAI211X1TR $T=99600 39600 1 0 $X=99260 $Y=35600
X290 615 322 VSS 454 549 VDD DOut<6> OAI211X1TR $T=104400 39600 0 180 $X=101660 $Y=35600
X291 626 322 VSS 546 550 VDD DOut<9> OAI211X1TR $T=106400 32400 0 180 $X=103660 $Y=28400
X292 617 322 VSS 546 616 VDD DOut<8> OAI211X1TR $T=106800 32400 1 180 $X=104060 $Y=32120
X293 627 322 VSS 454 551 VDD DOut<7> OAI211X1TR $T=106800 39600 0 180 $X=104060 $Y=35600
X294 482 VDD pc<0> DIn<0> 487 VSS 466 AOI22X1TR $T=69600 25200 0 180 $X=66860 $Y=21200
X295 482 VDD pc<1> DIn<1> 487 VSS 488 AOI22X1TR $T=72400 10800 0 180 $X=69660 $Y=6800
X296 482 VDD pc<2> DIn<2> 487 VSS 465 AOI22X1TR $T=76000 10800 1 180 $X=73260 $Y=10520
X297 510 VDD 508 603 503 VSS 496 AOI22X1TR $T=77200 39600 1 180 $X=74460 $Y=39320
X298 493 VDD 495 479 492 VSS 512 AOI22X1TR $T=76400 32400 1 0 $X=76060 $Y=28400
X299 482 VDD pc<3> DIn<3> 487 VSS 607 AOI22X1TR $T=76800 10800 0 0 $X=76460 $Y=10520
X300 503 VDD 506 513 522 VSS 515 AOI22X1TR $T=80400 46800 0 0 $X=80060 $Y=46520
X301 482 VDD pc<9> DIn<9> 487 VSS 550 AOI22X1TR $T=96000 25200 1 0 $X=95660 $Y=21200
X302 482 VDD pc<5> DIn<5> 487 VSS 548 AOI22X1TR $T=102400 10800 1 0 $X=102060 $Y=6800
X303 482 VDD pc<8> DIn<8> 487 VSS 616 AOI22X1TR $T=102400 18000 0 0 $X=102060 $Y=17720
X304 482 VDD pc<7> DIn<7> 487 VSS 551 AOI22X1TR $T=102800 18000 1 0 $X=102460 $Y=14000
X305 482 VDD pc<6> DIn<6> 487 VSS 549 AOI22X1TR $T=103200 10800 0 0 $X=102860 $Y=10520
X306 339 Dsram<13> VDD 429 VSS 435 AOI21X1TR $T=47600 25200 1 0 $X=47260 $Y=21200
X307 339 Dsram<12> VDD 429 VSS 437 AOI21X1TR $T=48000 18000 0 0 $X=47660 $Y=17720
X308 339 Dsram<11> VDD 429 VSS 440 AOI21X1TR $T=51600 25200 1 0 $X=51260 $Y=21200
X309 339 Dsram<10> VDD 429 VSS 446 AOI21X1TR $T=52400 25200 0 0 $X=52060 $Y=24920
X310 339 Dsram<15> VDD 429 VSS 451 AOI21X1TR $T=54000 32400 1 0 $X=53660 $Y=28400
X311 339 Dsram<14> VDD 429 VSS 453 AOI21X1TR $T=55600 25200 0 0 $X=55260 $Y=24920
X312 467 480 VDD 458 VSS 469 AOI21X1TR $T=68000 32400 0 180 $X=65660 $Y=28400
X313 clk VSS 484 VDD CLKBUFX20TR $T=64000 54000 1 0 $X=63660 $Y=50000
X314 clk VSS 336 VDD CLKBUFX20TR $T=64000 54000 0 0 $X=63660 $Y=53720
X315 Dxac<30> 452 VSS 441 VDD NAND2BX1TR $T=59960 54000 0 180 $X=57620 $Y=50000
X316 467 464 VSS FunISEL<1> VDD NAND2BX1TR $T=62800 32400 1 0 $X=62460 $Y=28400
X317 FunISEL<0> 477 VSS 455 VDD NAND2BX1TR $T=67200 25200 0 180 $X=64860 $Y=21200
X318 482 REN_SRAM 487 VSS VDD NOR2X2TR $T=66400 25200 0 0 $X=66060 $Y=24920
X319 481 VSS 476 Jmp VDD NAND2XLTR $T=67200 10800 0 180 $X=65260 $Y=6800
X320 596 VSS 479 471 VDD NAND2XLTR $T=67600 39600 0 180 $X=65660 $Y=35600
X321 495 VSS 491 492 VDD NAND2XLTR $T=72400 32400 1 0 $X=72060 $Y=28400
X322 365 349 VSS VDD 354 OR2X2TR $T=16800 39600 0 0 $X=16460 $Y=39320
X323 480 491 VSS VDD 601 OR2X2TR $T=71600 46800 0 0 $X=71260 $Y=46520
X324 506 499 VSS VDD 603 OR2X2TR $T=76800 32400 1 180 $X=74460 $Y=32120
X325 VSS VDD 618 TIEHITR $T=41600 61200 0 180 $X=40060 $Y=57200
X326 557 336 Col_Sel<13> VSS VDD 320 DFFXLTR $T=14400 54000 0 180 $X=6860 $Y=50000
X327 341 336 Col_Sel<12> VSS VDD 321 DFFXLTR $T=14400 54000 1 180 $X=6860 $Y=53720
X328 342 336 Col_Sel<14> VSS VDD 324 DFFXLTR $T=14800 46800 1 180 $X=7260 $Y=46520
X329 558 336 Col_Sel<1> VSS VDD 328 DFFXLTR $T=15600 46800 0 180 $X=8060 $Y=42800
X330 560 336 Col_Sel<3> VSS VDD 340 DFFXLTR $T=20800 39600 0 180 $X=13260 $Y=35600
X331 562 336 Col_Sel<11> VSS VDD 343 DFFXLTR $T=21600 54000 1 180 $X=14060 $Y=53720
X332 351 336 Col_Sel<2> VSS VDD 346 DFFXLTR $T=22000 46800 1 180 $X=14460 $Y=46520
X333 563 336 Col_Sel<4> VSS VDD 350 DFFXLTR $T=24000 32400 1 180 $X=16460 $Y=32120
X334 369 336 Col_Sel<15> VSS VDD 352 DFFXLTR $T=24800 54000 0 180 $X=17260 $Y=50000
X335 565 336 Col_Sel<0> VSS VDD 349 DFFXLTR $T=25200 46800 0 180 $X=17660 $Y=42800
X336 374 336 Col_Sel<9> VSS VDD 363 DFFXLTR $T=28000 61200 0 180 $X=20460 $Y=57200
X337 568 336 Col_Sel<10> VSS VDD 362 DFFXLTR $T=28800 54000 1 180 $X=21260 $Y=53720
X338 375 336 Col_Sel<5> VSS VDD 370 DFFXLTR $T=32000 39600 0 180 $X=24460 $Y=35600
X339 379 336 Col_Sel<8> VSS VDD 569 DFFXLTR $T=36000 54000 1 180 $X=28460 $Y=53720
X340 376 336 Col_Sel<6> VSS VDD 386 DFFXLTR $T=30800 46800 0 0 $X=30460 $Y=46520
X341 393 336 Col_Sel<7> VSS VDD 384 DFFXLTR $T=32000 46800 1 0 $X=31660 $Y=42800
X342 instruIn<14> 484 597 VSS VDD 598 DFFXLTR $T=65200 39600 0 0 $X=64860 $Y=39320
X343 F 484 697 VSS VDD 622 DFFXLTR $T=88400 39600 1 180 $X=80860 $Y=39320
X344 335 VSS 325 322 VDD RWL_P<12> OAI21XLTR $T=10000 18000 0 180 $X=7660 $Y=14000
X345 555 VSS 325 322 VDD RWL_P<14> OAI21XLTR $T=10000 25200 0 180 $X=7660 $Y=21200
X346 329 339 334 rSrcIn<10> VDD VSS RWL_N<10> AO22X1TR $T=14400 18000 1 180 $X=11260 $Y=17720
X347 326 339 334 rSrcIn<11> VDD VSS RWL_N<11> AO22X1TR $T=14400 25200 0 180 $X=11260 $Y=21200
X348 338 339 334 rSrcIn<15> VDD VSS RWL_N<15> AO22X1TR $T=15600 25200 1 180 $X=12460 $Y=24920
X349 330 339 334 rSrcIn<13> VDD VSS RWL_N<13> AO22X1TR $T=17200 32400 0 180 $X=14060 $Y=28400
X350 345 339 334 rSrcIn<14> VDD VSS RWL_N<14> AO22X1TR $T=17600 25200 0 180 $X=14460 $Y=21200
X351 327 339 334 rSrcIn<12> VDD VSS RWL_N<12> AO22X1TR $T=18400 25200 1 180 $X=15260 $Y=24920
X352 559 339 334 rSrcIn<2> VDD VSS RWL_N<2> AO22X1TR $T=16400 10800 0 0 $X=16060 $Y=10520
X353 357 339 334 rSrcIn<3> VDD VSS RWL_N<3> AO22X1TR $T=26000 10800 1 180 $X=22860 $Y=10520
X354 566 339 334 rSrcIn<1> VDD VSS RWL_N<1> AO22X1TR $T=26000 25200 1 0 $X=25660 $Y=21200
X355 368 339 334 rSrcIn<0> VDD VSS RWL_N<0> AO22X1TR $T=26000 25200 0 0 $X=25660 $Y=24920
X356 387 339 334 rSrcIn<7> VDD VSS RWL_N<7> AO22X1TR $T=30000 18000 0 180 $X=26860 $Y=14000
X357 377 339 334 rSrcIn<6> VDD VSS RWL_N<6> AO22X1TR $T=31600 10800 0 0 $X=31260 $Y=10520
X358 397 339 334 rSrcIn<8> VDD VSS RWL_N<8> AO22X1TR $T=38400 25200 0 180 $X=35260 $Y=21200
X359 399 339 334 rSrcIn<9> VDD VSS RWL_N<9> AO22X1TR $T=36800 32400 1 0 $X=36460 $Y=28400
X360 402 339 334 rSrcIn<5> VDD VSS RWL_N<5> AO22X1TR $T=40000 10800 0 0 $X=39660 $Y=10520
X361 398 339 334 rSrcIn<4> VDD VSS RWL_N<4> AO22X1TR $T=46800 18000 1 0 $X=46460 $Y=14000
X362 349 VSS 347 365 328 VDD 558 OAI22X1TR $T=16800 39600 1 180 $X=14060 $Y=39320
X363 321 VSS 347 365 320 VDD 557 OAI22X1TR $T=16800 54000 0 180 $X=14060 $Y=50000
X364 320 VSS 347 324 365 VDD 342 OAI22X1TR $T=14800 61200 1 0 $X=14460 $Y=57200
X365 328 VSS 347 365 346 VDD 351 OAI22X1TR $T=15600 46800 1 0 $X=15260 $Y=42800
X366 343 VSS 347 365 321 VDD 341 OAI22X1TR $T=19600 61200 0 180 $X=16860 $Y=57200
X367 346 VSS 347 365 340 VDD 560 OAI22X1TR $T=18800 39600 0 0 $X=18460 $Y=39320
X368 340 VSS 347 365 350 VDD 563 OAI22X1TR $T=21200 39600 0 0 $X=20860 $Y=39320
X369 362 VSS 347 365 343 VDD 562 OAI22X1TR $T=24400 46800 1 180 $X=21660 $Y=46520
X370 324 VSS 347 365 352 VDD 369 OAI22X1TR $T=24800 54000 1 0 $X=24460 $Y=50000
X371 350 VSS 347 365 370 VDD 375 OAI22X1TR $T=25600 39600 0 0 $X=25260 $Y=39320
X372 370 VSS 347 365 386 VDD 376 OAI22X1TR $T=26000 46800 1 0 $X=25660 $Y=42800
X373 363 VSS 347 365 362 VDD 568 OAI22X1TR $T=29600 54000 0 180 $X=26860 $Y=50000
X374 384 VSS 347 365 569 VDD 379 OAI22X1TR $T=30400 61200 0 180 $X=27660 $Y=57200
X375 569 VSS 347 365 363 VDD 374 OAI22X1TR $T=29600 54000 1 0 $X=29260 $Y=50000
X376 386 VSS 347 365 384 VDD 393 OAI22X1TR $T=30400 61200 1 0 $X=30060 $Y=57200
X377 353 VDD 364 368 VSS 353 372 566 704 ICV_23 $T=22800 25200 0 0 $X=22460 $Y=24920
X378 621 VDD 516 517 VSS 518 520 ReadA<12> 699 ICV_23 $T=79600 18000 1 0 $X=79260 $Y=14000
X379 530 VDD 529 ReadB<6> VSS 529 534 ReadB<5> 703 ICV_23 $T=86400 61200 1 0 $X=86060 $Y=57200
X380 528 VDD 532 ReadB<15> VSS 528 534 ReadB<13> 702 ICV_23 $T=87200 54000 1 0 $X=86860 $Y=50000
X381 530 VDD 538 ReadB<10> VSS 535 534 ReadB<1> 703 ICV_23 $T=89600 61200 1 0 $X=89260 $Y=57200
X382 538 VDD 534 ReadB<9> VSS 528 527 ReadB<12> 704 ICV_23 $T=100800 32400 1 0 $X=100460 $Y=28400
X383 366 VSS 354 VDD 564 565 NAND3X1TR $T=22800 39600 1 0 $X=22460 $Y=35600
X384 Dxac<19> VSS 423 VDD Dxac<18> 581 NAND3X1TR $T=48800 25200 1 180 $X=46460 $Y=24920
X385 Dxac<26> VSS 420 VDD 436 431 NAND3X1TR $T=47600 39600 0 0 $X=47260 $Y=39320
X386 478 VSS 470 VDD imm8_disp<7> 476 NAND3X1TR $T=69600 10800 1 180 $X=67260 $Y=10520
X387 325 352 VDD VSS 564 OR2XLTR $T=25600 39600 1 180 $X=23260 $Y=39320
X388 VSS 359 Dxac<4> 570 381 VDD 383 OAI31X1TR $T=28000 39600 0 0 $X=27660 $Y=39320
X389 VSS Dxac<5> Dxac<6> 390 373 VDD 395 OAI31X1TR $T=30400 39600 0 0 $X=30060 $Y=39320
X390 VSS 589 436 432 334 VDD 450 OAI31X1TR $T=52800 39600 1 0 $X=52460 $Y=35600
X391 VSS 500 504 496 599 VDD 475 OAI31X1TR $T=74400 54000 0 180 $X=71660 $Y=50000
X392 VSS 620 491 497 602 VDD AluSEL<0> OAI31X1TR $T=74000 18000 1 0 $X=73660 $Y=14000
X393 VSS 524 506 513 504 VDD 523 OAI31X1TR $T=90800 39600 1 180 $X=88060 $Y=39320
X394 382 359 Dxac<1> VDD Dxac<5> 389 VSS 388 391 AOI33X1TR $T=28400 46800 1 0 $X=28060 $Y=42800
X395 Dxac<22> 583 Dxac<20> VDD 585 423 VSS 442 Dxac<18> AOI33X1TR $T=49600 32400 1 0 $X=49260 $Y=28400
X396 Dxac<7> Dxac<6> Dxac<4> VSS 381 VDD OAI2BB1X1TR $T=35200 39600 0 180 $X=32460 $Y=35600
X397 487 DIn<11> 440 VSS DOut<11> VDD OAI2BB1X1TR $T=100800 32400 0 0 $X=100460 $Y=32120
X398 487 DIn<10> 446 VSS DOut<10> VDD OAI2BB1X1TR $T=104400 25200 0 0 $X=104060 $Y=24920
X399 487 DIn<12> 437 VSS DOut<12> VDD OAI2BB1X1TR $T=105200 39600 0 0 $X=104860 $Y=39320
X400 487 DIn<15> 451 VSS DOut<15> VDD OAI2BB1X1TR $T=105200 46800 0 0 $X=104860 $Y=46520
X401 487 DIn<13> 435 VSS DOut<13> VDD OAI2BB1X1TR $T=105600 46800 1 0 $X=105260 $Y=42800
X402 487 DIn<14> 453 VSS DOut<14> VDD OAI2BB1X1TR $T=105600 54000 1 0 $X=105260 $Y=50000
X403 VSS 322 325 REN_SRAM VDD NAND2X2TR $T=33600 32400 0 0 $X=33260 $Y=32120
X404 403 Dxac<11> 401 VSS 390 Dxac<7> 389 VDD 400 OAI33XLTR $T=38000 39600 1 180 $X=34460 $Y=39320
X405 Dxac<14> 396 Dxac<15> 407 VSS Dxac<12> VDD NOR4BBX1TR $T=36800 32400 0 0 $X=36460 $Y=32120
X406 Dxac<7> 391 Dxac<6> VDD 406 VSS 408 AOI31X1TR $T=36800 39600 1 0 $X=36460 $Y=35600
X407 Dxac<9> 403 406 VDD 573 VSS 411 AOI31X1TR $T=41600 46800 1 180 $X=38860 $Y=46520
X408 406 577 403 VDD 395 VSS 575 AOI31X1TR $T=44000 46800 0 180 $X=41260 $Y=42800
X409 417 401 Dxac<8> VDD Dxac<16> VSS 422 AOI31X1TR $T=46400 39600 0 180 $X=43660 $Y=35600
X410 490 486 489 VDD 600 VSS AluSEL<1> AOI31X1TR $T=71200 18000 0 0 $X=70860 $Y=17720
X411 Dxac<11> 406 Dxac<10> VDD 383 409 396 VSS AOI32X1TR $T=38000 39600 0 0 $X=37660 $Y=39320
X412 436 425 582 VDD 428 430 419 VSS AOI32X1TR $T=50400 46800 0 180 $X=47260 $Y=42800
X413 Dxac<25> 425 436 VDD 428 439 421 VSS AOI32X1TR $T=50400 46800 1 0 $X=50060 $Y=42800
X414 622 519 472 VDD 515 514 605 VSS AOI32X1TR $T=81200 39600 1 180 $X=78060 $Y=39320
X415 385 382 VDD 400 407 VSS 412 AOI211X1TR $T=39200 46800 1 0 $X=38860 $Y=42800
X416 424 428 VDD 434 418 VSS 438 AOI211X1TR $T=49600 39600 0 0 $X=49260 $Y=39320
X417 433 414 VDD 444 590 VSS 448 AOI211X1TR $T=57600 54000 0 180 $X=54860 $Y=50000
X418 433 445 VDD 444 447 VSS 462 AOI211X1TR $T=59200 46800 1 0 $X=58860 $Y=42800
X419 Dxac<14> Dxac<12> VSS Dxac<8> VDD 416 NAND3BX1TR $T=39600 39600 1 0 $X=39260 $Y=35600
X420 Dxac<31> Dxac<30> VSS 452 VDD 443 NAND3BX1TR $T=58800 46800 0 180 $X=56060 $Y=42800
X421 416 407 VDD 417 VSS NOR2BX1TR $T=42000 39600 1 0 $X=41660 $Y=35600
X422 478 470 VDD 458 VSS NOR2BX1TR $T=65600 25200 1 180 $X=63260 $Y=24920
X423 475 471 VDD Br VSS NOR2BX1TR $T=65600 39600 0 180 $X=63260 $Y=35600
X424 ReadB<15> 483 VDD 610 VSS NOR2BX1TR $T=89600 39600 0 180 $X=87260 $Y=35600
X425 ReadB<10> 483 VDD 531 VSS NOR2BX1TR $T=91600 39600 0 180 $X=89260 $Y=35600
X426 ReadB<6> 483 VDD 611 VSS NOR2BX1TR $T=90400 46800 0 0 $X=90060 $Y=46520
X427 ReadB<5> 483 VDD 536 VSS NOR2BX1TR $T=90800 54000 0 0 $X=90460 $Y=53720
X428 ReadB<13> 483 VDD 533 VSS NOR2BX1TR $T=93600 39600 0 180 $X=91260 $Y=35600
X429 ReadB<2> 483 VDD 612 VSS NOR2BX1TR $T=92400 54000 1 0 $X=92060 $Y=50000
X430 ReadB<1> 483 VDD 623 VSS NOR2BX1TR $T=92800 54000 0 0 $X=92460 $Y=53720
X431 ReadB<3> 483 VDD 544 VSS NOR2BX1TR $T=94000 46800 0 0 $X=93660 $Y=46520
X432 ReadB<7> 483 VDD 545 VSS NOR2BX1TR $T=94400 54000 1 0 $X=94060 $Y=50000
X433 ReadB<14> 483 VDD 540 VSS NOR2BX1TR $T=96800 32400 1 180 $X=94460 $Y=32120
X434 ReadB<8> 483 VDD 624 VSS NOR2BX1TR $T=95200 39600 0 0 $X=94860 $Y=39320
X435 ReadB<9> 483 VDD 541 VSS NOR2BX1TR $T=97600 39600 0 180 $X=95260 $Y=35600
X436 ReadB<4> 483 VDD 625 VSS NOR2BX1TR $T=96000 46800 0 0 $X=95660 $Y=46520
X437 ReadB<0> 483 VDD 547 VSS NOR2BX1TR $T=96400 54000 1 0 $X=96060 $Y=50000
X438 ReadB<11> 483 VDD 543 VSS NOR2BX1TR $T=99600 32400 1 180 $X=97260 $Y=32120
X439 ReadB<12> 483 VDD 613 VSS NOR2BX1TR $T=97600 39600 1 0 $X=97260 $Y=35600
X440 VSS Dxac<18> Dxac<20> 413 415 421 VDD 619 OAI32X1TR $T=45600 32400 1 180 $X=42460 $Y=32120
X441 VSS Dxac<18> Dxac<20> Dxac<17> Dxac<21> 419 VDD 415 OAI32X1TR $T=46000 32400 0 180 $X=42860 $Y=28400
X442 VSS 425 579 420 Dxac<16> 418 VDD 409 OAI32X1TR $T=46000 39600 1 180 $X=42860 $Y=39320
X443 VSS 509 519 500 502 494 VDD 514 OAI32X1TR $T=81200 46800 1 0 $X=80860 $Y=42800
X444 VSS 583 VDD 426 423 589 587 NOR4XLTR $T=51200 32400 0 0 $X=50860 $Y=32120
X445 339 322 VSS VDD CLKINVX4TR $T=51600 18000 0 0 $X=51260 $Y=17720
X446 Dxac<24> 433 VDD Dxac<28> VSS 436 NOR3BX1TR $T=54000 46800 1 180 $X=51260 $Y=46520
X447 instruIn<5> 336 imm8_disp<5> VDD VSS 457 DFFX1TR $T=53200 18000 1 0 $X=52860 $Y=14000
X448 instruIn<4> 336 imm8_disp<4> VDD VSS 460 DFFX1TR $T=53600 18000 0 0 $X=53260 $Y=17720
X449 instruIn<6> 336 imm8_disp<6> VDD VSS 470 DFFX1TR $T=56800 10800 0 0 $X=56460 $Y=10520
X450 instruIn<7> 336 imm8_disp<7> VDD VSS 463 DFFX1TR $T=57200 10800 1 0 $X=56860 $Y=6800
X451 Dxac<24> Dxac<16> Dxac<28> VSS VDD 452 AND3X2TR $T=54800 46800 0 0 $X=54460 $Y=46520
X452 454 592 593 VSS VDD 461 AND3X2TR $T=58800 39600 1 0 $X=58460 $Y=35600
X453 597 485 486 VSS VDD 596 AND3X2TR $T=70000 39600 0 180 $X=67260 $Y=35600
X454 imm8_disp<5> VSS imm8_disp<6> 449 VDD 366 NAND3XLTR $T=58000 25200 0 180 $X=55660 $Y=21200
X455 475 VSS imm8_disp<7> 478 VDD 481 NAND3XLTR $T=72400 10800 1 180 $X=70060 $Y=10520
X456 490 VSS 491 480 VDD 602 NAND3XLTR $T=73200 25200 1 0 $X=72860 $Y=21200
X457 clk VSS 468 VDD CLKBUFX8TR $T=57200 54000 0 0 $X=56860 $Y=53720
X458 334 VSS 452 Dxac<30> 592 Dxac<31> VDD NAND4X1TR $T=57600 39600 0 0 $X=57260 $Y=39320
X459 591 VSS VDD 339 CLKBUFX2TR $T=60000 25200 1 180 $X=58060 $Y=24920
X460 470 455 463 VDD VSS 325 OR3X2TR $T=63600 18000 0 180 $X=60460 $Y=14000
X461 464 457 469 471 483 VSS VDD OAI211X4TR $T=62400 32400 0 0 $X=62060 $Y=32120
X462 455 imm8_disp<7> 470 VDD VSS 473 OR3X1TR $T=66800 10800 1 180 $X=64060 $Y=10520
X463 595 VDD 455 Stall VSS NOR2XLTR $T=66400 18000 0 180 $X=64460 $Y=14000
X464 Amount<1> VDD imm8_disp<6> Amount<0> VSS NOR2XLTR $T=104800 18000 0 0 $X=104460 $Y=17720
X465 596 479 VDD 498 VSS NOR2BXLTR $T=68000 32400 0 0 $X=67660 $Y=32120
X466 457 595 486 VSS VDD 620 OA21XLTR $T=69600 18000 1 0 $X=69260 $Y=14000
X467 pc<4> 482 VSS VDD 621 AND2XLTR $T=77600 18000 1 0 $X=77260 $Y=14000
X468 463 VSS 501 477 Amount<1> 479 VDD NAND4XLTR $T=78800 25200 0 0 $X=78460 $Y=24920
X469 479 501 LuiSEL VSS VDD DSEL<0> AO21XLTR $T=80400 32400 1 0 $X=80060 $Y=28400
.ENDS
***************************************
.SUBCKT ICV_22
** N=543 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_21
** N=362 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_20
** N=582 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_19
** N=4225 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_18
** N=4222 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_17
** N=4482 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_16
** N=4222 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT ICV_15
** N=2363 EP=0 IP=0 FDC=0
.ENDS
***************************************
.SUBCKT RA1SH16x512MUX_CD_EVEN
** N=85 EP=0 IP=0 FDC=0
*.CALIBRE ISOLATED NETS: VSS VDD BL0_2 DRSA_2 BL0__2 DWSA_2 BL1__2 BL1_2 BL1_1 BL1__1 STUBDW_0 STUBDR_0 BL0__1 BL0_1 BL0_0 STUBDR__0 BL0__0 STUBDW__0 BL1__0 BL1_0
+ BL1 BL1_ DWSA DRSA BL0_ BL0 A0_ A0 YP1_3 YP1_2 YP1_1 YP1_0 YP0_3 YP0_2 YP0_1 YP0_0 GTP STUBDW STUBDR_ STUBDR
+ STUBDW_
.ENDS
***************************************
.SUBCKT RA1SH16x512MUX_CD_ODD
** N=85 EP=0 IP=0 FDC=0
*.CALIBRE ISOLATED NETS: VSS VDD BL0_2 DRSA_2 BL0__2 DWSA_2 BL1__2 BL1_2 BL1_1 BL1__1 STUBDW STUBDR BL0__1 BL0_1 BL0_0 STUBDR_ BL0__0 STUBDW_ BL1__0 BL1_0
+ BL1 BL1_ DWSA DRSA BL0_ BL0 A0_ STUBDW_0 STUBDR__0 STUBDR_0 STUBDW__0 A0 YP1_3 YP1_2 YP1_1 YP1_0 YP0_3 YP0_2 YP0_1 YP0_0
+ GTP
.ENDS
***************************************
.SUBCKT RA1SH16x512HXP38X
** N=77 EP=0 IP=0 FDC=0
*.CALIBRE ISOLATED NETS: VSS A2 VDD A1 A0 XP_7 XP_6 XP_5 XP_4 XP_3 XP_2 XP_1 XP_0 BWEN WEI OEI_ AGTPB AY0 AY0_ YP1_3
+ YP1_2 YP1_1 YP1_0 YP0_3 YP0_2 YP0_1 YP0_0 AGTPT
.ENDS
***************************************
.SUBCKT RA1SH16x512 VDD VSS D<0> Q<0> Q<1> D<1> D<2> Q<2> Q<3> D<3> D<4> Q<4> Q<5> D<5> D<6> Q<6> Q<7> D<7> A<2> A<1>
+ A<0> CEN WEN CLK D<8> Q<8> Q<9> D<9> D<10> Q<10> Q<11> D<11> D<12> Q<12> Q<13> D<13> D<14> Q<14> Q<15> D<15>
+ A<8> A<7> A<6> A<5> A<4> A<3>
** N=1360 EP=46 IP=4469 FDC=0
.ENDS
***************************************
.SUBCKT MUX_D_buf SEL_OUT SEL_IN VDD! VSS!
** N=29 EP=4 IP=0 FDC=6
M0 SEL_OUT 5 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=450 $D=97
M1 VSS! 5 SEL_OUT VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=450 $D=97
M2 5 SEL_IN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1390 $Y=450 $D=97
M3 SEL_OUT 5 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=1650 $D=189
M4 VDD! 5 SEL_OUT VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=1650 $D=189
M5 5 SEL_IN VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1390 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT MUX_D_1bit SEL<1> D ALU Shifter SEL<0> DMEM VSS! VDD!
** N=108 EP=8 IP=0 FDC=22
M0 VSS! SEL<1> 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=450 $D=97
M1 D 14 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=5210 $D=97
M2 12 SEL<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=450 $D=97
M3 VSS! 14 D VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=5210 $D=97
M4 VSS! ALU 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=450 $D=97
M5 14 SEL<1> 10 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=5070 $D=97
M6 15 Shifter VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=450 $D=97
M7 11 9 14 VSS! nfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2780 $Y=5230 $D=97
M8 11 SEL<0> 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=450 $D=97
M9 13 12 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=5270 $D=97
M10 VSS! DMEM 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=450 $D=97
M11 VDD! SEL<1> 9 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=1950 $D=189
M12 D 14 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=3510 $D=189
M13 12 SEL<0> VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=1950 $D=189
M14 VDD! 14 D VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=3510 $D=189
M15 VDD! ALU 10 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=1790 $D=189
M16 14 9 10 VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=3510 $D=189
M17 15 Shifter VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=1790 $D=189
M18 11 SEL<1> 14 VDD! pfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2880 $Y=3510 $D=189
M19 11 12 15 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=2210 $D=189
M20 13 SEL<0> 11 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=3510 $D=189
M21 VDD! DMEM 13 VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=1790 $D=189
.ENDS
***************************************
.SUBCKT ICV_13 1 2 3 4 5 6 7 8 9 10 11 12
** N=12 EP=12 IP=16 FDC=44
X0 1 2 3 4 5 6 12 11 MUX_D_1bit $T=0 0 0 0 $X=-430 $Y=-240
X1 1 7 8 9 5 10 12 11 MUX_D_1bit $T=4800 0 0 0 $X=4370 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_14 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
** N=20 EP=20 IP=24 FDC=88
X0 1 2 3 4 18 5 6 7 8 9 19 20 ICV_13 $T=0 0 0 0 $X=-430 $Y=-240
X1 1 10 11 12 18 13 14 15 16 17 19 20 ICV_13 $T=9600 0 0 0 $X=9170 $Y=-240
.ENDS
***************************************
.SUBCKT MUX_D OUT<4> ALU<4> SH<4> DMEM<4> OUT<15> ALU<15> SH<15> DMEM<15> OUT<13> ALU<13> SH<13> DMEM<13> OUT<14> ALU<14> SH<14> DMEM<14> OUT<0> ALU<0> SH<0> DMEM<0>
+ OUT<1> ALU<1> SH<1> DMEM<1> OUT<2> ALU<2> SH<2> DMEM<2> OUT<3> ALU<3> SH<3> DMEM<3> OUT<5> ALU<5> SH<5> DMEM<5> OUT<6> ALU<6> SH<6> DMEM<6>
+ OUT<7> ALU<7> SH<7> DMEM<7> OUT<8> ALU<8> SH<8> DMEM<8> OUT<9> ALU<9> SH<9> DMEM<9> OUT<10> ALU<10> SH<10> DMEM<10> OUT<11> ALU<11> SH<11> DMEM<11>
+ OUT<12> ALU<12> SH<12> DMEM<12> SEL<1> SEL<0> VSS! VDD!
** N=265 EP=68 IP=96 FDC=364
X0 102 SEL<1> VDD! VSS! MUX_D_buf $T=0 26000 0 270 $X=-520 $Y=23870
X1 103 SEL<0> VDD! VSS! MUX_D_buf $T=6000 26000 1 270 $X=2510 $Y=23870
X2 102 OUT<4> ALU<4> SH<4> 103 DMEM<4> VSS! VDD! MUX_D_1bit $T=6000 19200 0 90 $X=-240 $Y=18770
X3 102 OUT<15> ALU<15> SH<15> 103 DMEM<15> VSS! VDD! MUX_D_1bit $T=6000 99600 0 90 $X=-240 $Y=99170
X4 102 OUT<13> ALU<13> SH<13> 103 DMEM<13> OUT<14> ALU<14> SH<14> DMEM<14> VDD! VSS! ICV_13 $T=6000 90000 0 90 $X=-240 $Y=89570
X5 102 OUT<0> ALU<0> SH<0> DMEM<0> OUT<1> ALU<1> SH<1> DMEM<1> OUT<2> ALU<2> SH<2> DMEM<2> OUT<3> ALU<3> SH<3> DMEM<3> 103 VDD! VSS! ICV_14 $T=6000 0 0 90 $X=-240 $Y=-430
X6 102 OUT<5> ALU<5> SH<5> DMEM<5> OUT<6> ALU<6> SH<6> DMEM<6> OUT<7> ALU<7> SH<7> DMEM<7> OUT<8> ALU<8> SH<8> DMEM<8> 103 VDD! VSS! ICV_14 $T=6000 51600 0 90 $X=-240 $Y=51170
X7 102 OUT<9> ALU<9> SH<9> DMEM<9> OUT<10> ALU<10> SH<10> DMEM<10> OUT<11> ALU<11> SH<11> DMEM<11> OUT<12> ALU<12> SH<12> DMEM<12> 103 VDD! VSS! ICV_14 $T=6000 70800 0 90 $X=-240 $Y=70370
.ENDS
***************************************
.SUBCKT MUX_Shifter_SEL_BUF SEL_OUT SEL_IN VDD! VSS!
** N=24 EP=4 IP=0 FDC=4
M0 VSS! 5 SEL_OUT VSS! nfet L=1.2e-07 W=4.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=450 $D=97
M1 5 SEL_IN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=450 $D=97
M2 VDD! 5 SEL_OUT VDD! pfet L=1.2e-07 W=7.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=540 $Y=1750 $D=189
M3 5 SEL_IN VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1040 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT MUX_Shifter_SEL_1bit SEL<1> ALU_B Imm_8 SEL<0> Rsrc VSS! EIGHT VDD!
** N=104 EP=8 IP=0 FDC=22
M0 VSS! SEL<1> 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=450 $D=97
M1 ALU_B 14 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=5210 $D=97
M2 12 SEL<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=450 $D=97
M3 VSS! 14 ALU_B VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=5210 $D=97
M4 VSS! EIGHT 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=450 $D=97
M5 14 SEL<1> 10 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=5070 $D=97
M6 15 Imm_8 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=450 $D=97
M7 11 9 14 VSS! nfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2780 $Y=5230 $D=97
M8 11 SEL<0> 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=450 $D=97
M9 13 12 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=5270 $D=97
M10 VSS! Rsrc 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=450 $D=97
M11 VDD! SEL<1> 9 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=1950 $D=189
M12 ALU_B 14 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=410 $Y=3510 $D=189
M13 12 SEL<0> VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=1950 $D=189
M14 VDD! 14 ALU_B VDD! pfet L=1.2e-07 W=8.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=890 $Y=3510 $D=189
M15 VDD! EIGHT 10 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=1950 $D=189
M16 14 9 10 VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2220 $Y=3510 $D=189
M17 15 Imm_8 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2470 $Y=1950 $D=189
M18 11 SEL<1> 14 VDD! pfet L=1.2e-07 W=3.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2880 $Y=3510 $D=189
M19 11 12 15 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3070 $Y=2210 $D=189
M20 13 SEL<0> 11 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3540 $Y=3510 $D=189
M21 VDD! Rsrc 13 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4090 $Y=1950 $D=189
.ENDS
***************************************
.SUBCKT Sign_Ext_left Imm_16 Imm_8 3 4
** N=22 EP=4 IP=0 FDC=4
M0 Imm_16 5 3 3 nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=1020 $D=97
M1 3 Imm_8 5 3 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=670 $Y=490 $D=97
M2 Imm_16 5 4 4 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=1020 $D=189
M3 4 Imm_8 5 4 pfet L=1.2e-07 W=4.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2040 $Y=490 $D=189
.ENDS
***************************************
.SUBCKT Sign_Ext_right IMM_16 SEL_BAR SEL 4 5
** N=49 EP=5 IP=0 FDC=10
M0 4 6 7 4 nfet L=1.2e-07 W=3.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=2210 $D=97
M1 IMM_16 7 4 4 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=670 $Y=2730 $D=97
M2 4 7 IMM_16 4 nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=670 $Y=3330 $D=97
M3 6 SEL_BAR 4 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=720 $Y=570 $D=97
M4 5 SEL 6 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=720 $Y=1130 $D=97
M5 5 6 7 5 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=2210 $D=189
M6 IMM_16 7 5 5 pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2730 $D=189
M7 5 7 IMM_16 5 pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=3330 $D=189
M8 6 SEL 4 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=570 $D=189
M9 5 SEL_BAR 6 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1130 $D=189
.ENDS
***************************************
.SUBCKT BUFFER_SEL IN VSS! VDD! OUT
** N=85 EP=4 IP=0 FDC=14
M0 VSS! IN 5 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=730 $Y=450 $D=97
M1 6 5 VSS! VSS! nfet L=1.2e-07 W=6.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=450 $D=97
M2 7 6 VSS! VSS! nfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M3 VSS! 6 7 VSS! nfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M4 OUT 7 VSS! VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=450 $D=97
M5 VSS! 7 OUT VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=450 $D=97
M6 OUT 7 VSS! VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4110 $Y=450 $D=97
M7 VDD! IN 5 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=730 $Y=4150 $D=189
M8 6 5 VDD! VDD! pfet L=1.2e-07 W=1.19e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3500 $D=189
M9 7 6 VDD! VDD! pfet L=1.2e-07 W=1.3e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3390 $D=189
M10 VDD! 6 7 VDD! pfet L=1.2e-07 W=1.3e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3390 $D=189
M11 OUT 7 VDD! VDD! pfet L=1.2e-07 W=1.92e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=2770 $D=189
M12 VDD! 7 OUT VDD! pfet L=1.2e-07 W=1.92e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=2770 $D=189
M13 OUT 7 VDD! VDD! pfet L=1.2e-07 W=1.92e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4110 $Y=2770 $D=189
.ENDS
***************************************
.SUBCKT Z_cell4 VSS! P<1> P<3> P<0> P<2> VDD! OUT
** N=50 EP=7 IP=0 FDC=10
M0 9 P<0> VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=440 $D=97
M1 10 P<1> 9 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=920 $D=97
M2 11 P<2> 10 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=1400 $D=97
M3 8 P<3> 11 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=1880 $D=97
M4 VSS! 8 OUT VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5100 $Y=1070 $D=97
M5 8 P<0> VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=440 $D=189
M6 VDD! P<1> 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=920 $D=189
M7 8 P<2> VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1400 $D=189
M8 VDD! P<3> 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1880 $D=189
M9 VDD! 8 OUT VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3560 $Y=1070 $D=189
.ENDS
***************************************
.SUBCKT MUX41 SEL1 SEL0 OUT VSS! IN3 IN2 VDD! IN0 IN1
** N=127 EP=9 IP=0 FDC=28
M0 VSS! SEL1 12 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1090 $D=97
M1 13 SEL0 IN3 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2080 $D=97
M2 IN2 10 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2880 $D=97
M3 10 SEL0 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3880 $D=97
M4 OUT 11 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-1350 $D=97
M5 VSS! 11 OUT VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-870 $D=97
M6 OUT 11 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-390 $D=97
M7 VSS! 11 OUT VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=90 $D=97
M8 VSS! 14 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=-650 $D=97
M9 14 SEL1 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=940 $D=97
M10 15 12 14 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=2090 $D=97
M11 15 10 IN0 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=3340 $D=97
M12 IN1 SEL0 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=4140 $D=97
M13 OUT 11 VDD! VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-1350 $D=189
M14 VDD! 11 OUT VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-870 $D=189
M15 OUT 11 VDD! VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-390 $D=189
M16 VDD! 11 OUT VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=90 $D=189
M17 12 SEL1 VDD! VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=610 $D=189
M18 VDD! SEL1 12 VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1090 $D=189
M19 10 SEL0 VDD! VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=3880 $D=189
M20 VDD! SEL0 10 VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=4360 $D=189
M21 13 10 IN3 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=2080 $D=189
M22 IN2 SEL0 13 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=2880 $D=189
M23 VDD! 14 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=-650 $D=189
M24 14 12 13 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=940 $D=189
M25 15 SEL1 14 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2140 $D=189
M26 15 SEL0 IN0 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3340 $D=189
M27 IN1 10 15 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=4140 $D=189
.ENDS
***************************************
.SUBCKT SETUP_BIT A B XOR AND OR VSS! VDD!
** N=98 EP=7 IP=0 FDC=26
M0 13 A 11 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=5030 $D=97
M1 VSS! A 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=520 $Y=430 $D=97
M2 VSS! B 13 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=5030 $D=97
M3 8 B VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1000 $Y=430 $D=97
M4 AND 11 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=5290 $D=97
M5 14 8 OR VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=430 $D=97
M6 15 A 12 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=5030 $D=97
M7 VSS! 10 14 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=430 $D=97
M8 VSS! 8 15 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=5030 $D=97
M9 16 10 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=430 $D=97
M10 17 12 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=5030 $D=97
M11 9 B 16 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=430 $D=97
M12 XOR 9 17 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=5030 $D=97
M13 11 A VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=3510 $D=189
M14 VDD! A 10 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=520 $Y=1950 $D=189
M15 VDD! B 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=3510 $D=189
M16 8 B VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1000 $Y=1950 $D=189
M17 AND 11 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=3510 $D=189
M18 OR 8 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=1950 $D=189
M19 12 A VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=3510 $D=189
M20 VDD! 10 OR VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=1950 $D=189
M21 VDD! 8 12 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=3510 $D=189
M22 9 10 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=1950 $D=189
M23 XOR 12 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=3510 $D=189
M24 VDD! B 9 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=1950 $D=189
M25 VDD! 9 XOR VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT ICV_12 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
** N=18 EP=18 IP=32 FDC=108
X0 1 2 5 15 10 16 17 9 8 MUX41 $T=0 0 0 0 $X=-240 $Y=-2300
X1 1 2 6 15 14 18 17 13 12 MUX41 $T=6000 0 0 0 $X=5760 $Y=-2300
X2 7 3 8 9 10 15 17 SETUP_BIT $T=0 9600 0 270 $X=-240 $Y=4490
X3 11 4 12 13 14 15 17 SETUP_BIT $T=6000 9600 0 270 $X=5760 $Y=4490
.ENDS
***************************************
.SUBCKT MUX21_BAR SEL VSS! IN<1> VDD! OUT IN<0>
** N=41 EP=6 IP=0 FDC=8
M0 8 SEL IN<1> VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=600 $Y=5270 $D=97
M1 VSS! SEL 7 VSS! nfet L=1.2e-07 W=5.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=640 $Y=450 $D=97
M2 OUT 8 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1120 $Y=450 $D=97
M3 IN<0> 7 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1280 $Y=5270 $D=97
M4 8 7 IN<1> VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=600 $Y=3510 $D=189
M5 VDD! SEL 7 VDD! pfet L=1.2e-07 W=9.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=640 $Y=1580 $D=189
M6 OUT 8 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1120 $Y=1950 $D=189
M7 IN<0> SEL 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1280 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT MUX84 SEL VDD! VSS! S3<1> S<3> S3<0> S2<1> S<2> S2<0> S1<1> S<1> S1<0> S0<1> S<0> S0<0>
** N=15 EP=15 IP=24 FDC=32
X0 SEL VSS! S3<1> VDD! S<3> S3<0> MUX21_BAR $T=6000 2000 1 270 $X=-240 $Y=-200
X1 SEL VSS! S2<1> VDD! S<2> S2<0> MUX21_BAR $T=12000 2000 1 270 $X=5760 $Y=-200
X2 SEL VSS! S1<1> VDD! S<1> S1<0> MUX21_BAR $T=18000 2000 1 270 $X=11760 $Y=-200
X3 SEL VSS! S0<1> VDD! S<0> S0<0> MUX21_BAR $T=24000 2000 1 270 $X=17760 $Y=-200
.ENDS
***************************************
.SUBCKT RIPPLE_BIT_BAR B CIN A VSS! SUM VDD! COUT
** N=115 EP=7 IP=0 FDC=24
M0 10 8 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=450 $D=97
M1 VSS! B 12 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=5070 $D=97
M2 13 A VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1360 $Y=5190 $D=97
M3 9 10 CIN VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1920 $Y=450 $D=97
M4 8 B 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1980 $Y=5270 $D=97
M5 14 8 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2600 $Y=450 $D=97
M6 A 12 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2660 $Y=5270 $D=97
M7 VSS! CIN 14 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3220 $Y=450 $D=97
M8 SUM 9 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3700 $Y=450 $D=97
M9 11 10 B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3740 $Y=5270 $D=97
M10 CIN 8 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4420 $Y=5270 $D=97
M11 COUT 11 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4930 $Y=450 $D=97
M12 10 8 VDD! VDD! pfet L=1.2e-07 W=9.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=1540 $D=189
M13 VDD! B 12 VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=3510 $D=189
M14 13 A VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1360 $Y=3510 $D=189
M15 9 8 CIN VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1920 $Y=2210 $D=189
M16 8 12 13 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1980 $Y=3510 $D=189
M17 14 10 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2600 $Y=2210 $D=189
M18 A B 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2660 $Y=3510 $D=189
M19 VDD! CIN 14 VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3220 $Y=1630 $D=189
M20 SUM 9 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3700 $Y=1630 $D=189
M21 11 8 B VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3740 $Y=3510 $D=189
M22 CIN 10 11 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4420 $Y=3510 $D=189
M23 COUT 11 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4930 $Y=1630 $D=189
.ENDS
***************************************
.SUBCKT RIPPLE_BIT B CIN A SUM VSS! VDD! COUT
** N=124 EP=7 IP=0 FDC=26
M0 VSS! B 13 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=470 $Y=5070 $D=97
M1 10 8 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=450 $D=97
M2 14 A VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=950 $Y=5190 $D=97
M3 8 B 14 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=5270 $D=97
M4 9 10 CIN VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1650 $Y=450 $D=97
M5 A 13 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2250 $Y=5270 $D=97
M6 15 8 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2330 $Y=450 $D=97
M7 VSS! CIN 15 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2950 $Y=450 $D=97
M8 11 10 B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3330 $Y=5270 $D=97
M9 SUM 9 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3430 $Y=450 $D=97
M10 CIN 8 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4010 $Y=5270 $D=97
M11 COUT 12 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4850 $Y=450 $D=97
M12 12 11 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4990 $Y=5150 $D=97
M13 VDD! B 13 VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=470 $Y=3510 $D=189
M14 10 8 VDD! VDD! pfet L=1.2e-07 W=9.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=1540 $D=189
M15 14 A VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=950 $Y=3510 $D=189
M16 8 13 14 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=3510 $D=189
M17 9 8 CIN VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1650 $Y=2210 $D=189
M18 A B 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2250 $Y=3510 $D=189
M19 15 10 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2330 $Y=2210 $D=189
M20 VDD! CIN 15 VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2950 $Y=1630 $D=189
M21 11 8 B VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3330 $Y=3510 $D=189
M22 SUM 9 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3430 $Y=1630 $D=189
M23 CIN 10 11 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4010 $Y=3510 $D=189
M24 COUT 12 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4850 $Y=1630 $D=189
M25 12 11 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4990 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT RIPPLE_4BIT CIN B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! SUM<3> COUT SUM<2> SUM<1> SUM<0>
** N=19 EP=16 IP=28 FDC=102
X0 B<3> 17 A<3> VSS! SUM<3> VDD! COUT RIPPLE_BIT_BAR $T=0 0 0 0 $X=-200 $Y=-240
X1 B<2> 18 A<2> SUM<2> VSS! VDD! 17 RIPPLE_BIT $T=0 6000 0 0 $X=-200 $Y=5760
X2 B<1> 19 A<1> SUM<1> VSS! VDD! 18 RIPPLE_BIT $T=0 12000 0 0 $X=-200 $Y=11760
X3 B<0> CIN A<0> SUM<0> VSS! VDD! 19 RIPPLE_BIT $T=0 18000 0 0 $X=-200 $Y=17760
.ENDS
***************************************
.SUBCKT RIPPLE_4BIT_LAST_4BIT C3 CIN B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! SUM<3> COUT SUM<2> SUM<1> SUM<0>
** N=19 EP=17 IP=28 FDC=102
X0 B<3> C3 A<3> VSS! SUM<3> VDD! COUT RIPPLE_BIT_BAR $T=0 0 0 0 $X=-200 $Y=-240
X1 B<2> 18 A<2> SUM<2> VSS! VDD! C3 RIPPLE_BIT $T=0 6000 0 0 $X=-200 $Y=5760
X2 B<1> 19 A<1> SUM<1> VSS! VDD! 18 RIPPLE_BIT $T=0 12000 0 0 $X=-200 $Y=11760
X3 B<0> CIN A<0> SUM<0> VSS! VDD! 19 RIPPLE_BIT $T=0 18000 0 0 $X=-200 $Y=17760
.ENDS
***************************************
.SUBCKT ALU SEL<1> F N SEL<0> Z CIN B<0> B<1> B<2> B<3> B<4> B<5> B<6> B<7> B<8> B<9> COUT B<10> B<11> B<12>
+ B<13> B<14> B<15> A<0> A<1> A<2> A<3> A<4> A<5> A<6> A<7> A<8> A<9> A<10> A<11> A<12> A<13> A<14> A<15> OUT<0>
+ OUT<1> OUT<2> OUT<3> OUT<4> OUT<5> OUT<6> OUT<7> OUT<8> OUT<9> OUT<10> OUT<11> OUT<12> OUT<13> OUT<14> OUT<15> VDD! VSS!
** N=501 EP=57 IP=401 FDC=1972
M0 195 CIN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2740 $Y=14450 $D=97
M1 74 195 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2740 $Y=15410 $D=97
M2 204 106 197 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40190 $Y=14960 $D=97
M3 VSS! 74 204 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40190 $Y=15440 $D=97
M4 VSS! 197 Z VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40450 $Y=14010 $D=97
M5 VSS! COUT 126 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=64370 $Y=15090 $D=97
M6 205 130 129 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=14070 $D=97
M7 VSS! 127 205 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=14550 $D=97
M8 206 127 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=15030 $D=97
M9 130 126 206 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=15510 $D=97
M10 207 199 N VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=14070 $D=97
M11 VSS! 129 207 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=14550 $D=97
M12 208 126 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=15030 $D=97
M13 199 130 208 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=15510 $D=97
M14 209 146 144 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=14070 $D=97
M15 VSS! 145 209 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=14550 $D=97
M16 210 145 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=15030 $D=97
M17 146 COUT 210 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=15510 $D=97
M18 211 201 F VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=14070 $D=97
M19 VSS! 144 211 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=14550 $D=97
M20 212 COUT VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=15030 $D=97
M21 201 146 212 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=15510 $D=97
M22 VSS! 134 151 VSS! nfet L=1.2e-07 W=5.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=89650 $Y=14040 $D=97
M23 202 150 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=89650 $Y=14520 $D=97
M24 145 202 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=89650 $Y=15460 $D=97
M25 150 134 158 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=94470 $Y=14200 $D=97
M26 154 151 150 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=94470 $Y=14880 $D=97
M27 195 CIN VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1600 $Y=14450 $D=189
M28 74 195 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1600 $Y=15410 $D=189
M29 VDD! 197 Z VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38750 $Y=14010 $D=189
M30 197 106 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38750 $Y=14960 $D=189
M31 VDD! 74 197 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38750 $Y=15440 $D=189
M32 VDD! COUT 126 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=62750 $Y=15090 $D=189
M33 129 130 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=14070 $D=189
M34 VDD! 127 129 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=14550 $D=189
M35 130 127 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=15030 $D=189
M36 VDD! 126 130 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=15510 $D=189
M37 N 199 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=14070 $D=189
M38 VDD! 129 N VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=14550 $D=189
M39 199 126 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=15030 $D=189
M40 VDD! 130 199 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=15510 $D=189
M41 144 146 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=14070 $D=189
M42 VDD! 145 144 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=14550 $D=189
M43 146 145 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=15030 $D=189
M44 VDD! COUT 146 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=15510 $D=189
M45 F 201 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=14070 $D=189
M46 VDD! 144 F VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=14550 $D=189
M47 201 COUT VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=15030 $D=189
M48 VDD! 146 201 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=15510 $D=189
M49 VDD! 134 151 VDD! pfet L=1.2e-07 W=9.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=90780 $Y=14040 $D=189
M50 202 150 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=91150 $Y=14520 $D=189
M51 145 202 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=91150 $Y=15460 $D=189
M52 150 151 158 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=92710 $Y=14200 $D=189
M53 154 134 150 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=92710 $Y=14880 $D=189
X54 SEL<1> VSS! VDD! 75 BUFFER_SEL $T=5200 4800 1 270 $X=-240 $Y=-100
X55 SEL<0> VSS! VDD! 76 BUFFER_SEL $T=5200 9600 1 270 $X=-240 $Y=4700
X56 VSS! 82 83 79 84 VDD! 78 Z_cell4 $T=11200 13600 1 180 $X=4930 $Y=13350
X57 VSS! 100 101 97 102 VDD! 96 Z_cell4 $T=35200 13600 1 180 $X=28930 $Y=13350
X58 VSS! 96 110 78 111 VDD! 106 Z_cell4 $T=47200 13600 1 180 $X=40930 $Y=13350
X59 VSS! 120 121 117 122 VDD! 111 Z_cell4 $T=59200 13600 1 180 $X=52930 $Y=13350
X60 VSS! 141 127 138 142 VDD! 110 Z_cell4 $T=83200 13600 1 180 $X=76930 $Y=13350
X61 75 76 B<0> B<1> OUT<0> OUT<1> A<0> 79 81 77 A<1> 82 87 85 VSS! 159 VDD! 161 ICV_12 $T=5200 2000 0 0 $X=4960 $Y=-300
X62 75 76 B<2> B<3> OUT<2> OUT<3> A<2> 84 90 88 A<3> 83 94 91 VSS! 163 VDD! 165 ICV_12 $T=17200 2000 0 0 $X=16960 $Y=-300
X63 75 76 B<4> B<5> OUT<4> OUT<5> A<4> 97 99 95 A<5> 100 105 103 VSS! 168 VDD! 170 ICV_12 $T=29200 2000 0 0 $X=28960 $Y=-300
X64 75 76 B<6> B<7> OUT<6> OUT<7> A<6> 102 109 107 A<7> 101 115 112 VSS! 172 VDD! 174 ICV_12 $T=41200 2000 0 0 $X=40960 $Y=-300
X65 75 76 B<8> B<9> OUT<8> OUT<9> A<8> 117 119 116 A<9> 120 125 123 VSS! 177 VDD! 179 ICV_12 $T=53200 2000 0 0 $X=52960 $Y=-300
X66 75 76 B<10> B<11> OUT<10> OUT<11> A<10> 122 132 128 A<11> 121 136 133 VSS! 181 VDD! 183 ICV_12 $T=65200 2000 0 0 $X=64960 $Y=-300
X67 75 76 B<12> B<13> OUT<12> OUT<13> A<12> 138 140 137 A<13> 141 148 143 VSS! 186 VDD! 188 ICV_12 $T=77200 2000 0 0 $X=76960 $Y=-300
X68 75 76 B<14> B<15> OUT<14> OUT<15> A<14> 142 153 149 A<15> 127 157 155 VSS! 190 VDD! 192 ICV_12 $T=89200 2000 0 0 $X=88960 $Y=-300
X69 74 VSS! 196 VDD! 92 167 MUX21_BAR $T=23200 16000 0 270 $X=22960 $Y=13800
X70 92 VSS! 198 VDD! 113 176 MUX21_BAR $T=47200 16000 0 270 $X=46960 $Y=13800
X71 113 VSS! 200 VDD! 134 185 MUX21_BAR $T=71200 16000 0 270 $X=70960 $Y=13800
X72 134 VSS! 203 VDD! COUT 194 MUX21_BAR $T=95200 16000 0 270 $X=94960 $Y=13800
X73 74 VDD! VSS! 166 165 93 164 163 89 162 161 86 160 159 80 MUX84 $T=29200 11600 1 180 $X=4960 $Y=11400
X74 92 VDD! VSS! 175 174 114 173 172 108 171 170 104 169 168 98 MUX84 $T=53200 11600 1 180 $X=28960 $Y=11400
X75 113 VDD! VSS! 184 183 135 182 181 131 180 179 124 178 177 118 MUX84 $T=77200 11600 1 180 $X=52960 $Y=11400
X76 134 VDD! VSS! 193 192 156 191 190 152 189 188 147 187 186 139 MUX84 $T=101200 11600 1 180 $X=76960 $Y=11400
X77 VDD! B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! 166 196 164 162 160 RIPPLE_4BIT $T=29200 21600 1 270 $X=4960 $Y=15800
X78 VSS! B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! 93 167 89 86 80 RIPPLE_4BIT $T=29200 27200 1 270 $X=4960 $Y=21400
X79 VDD! B<7> A<7> B<6> A<6> B<5> A<5> B<4> A<4> VDD! VSS! 175 198 173 171 169 RIPPLE_4BIT $T=53200 21600 1 270 $X=28960 $Y=15800
X80 VSS! B<7> A<7> B<6> A<6> B<5> A<5> B<4> A<4> VDD! VSS! 114 176 108 104 98 RIPPLE_4BIT $T=53200 27200 1 270 $X=28960 $Y=21400
X81 VDD! B<11> A<11> B<10> A<10> B<9> A<9> B<8> A<8> VDD! VSS! 184 200 182 180 178 RIPPLE_4BIT $T=77200 21600 1 270 $X=52960 $Y=15800
X82 VSS! B<11> A<11> B<10> A<10> B<9> A<9> B<8> A<8> VDD! VSS! 135 185 131 124 118 RIPPLE_4BIT $T=77200 27200 1 270 $X=52960 $Y=21400
X83 158 VDD! B<15> A<15> B<14> A<14> B<13> A<13> B<12> A<12> VDD! VSS! 193 203 191 189 187 RIPPLE_4BIT_LAST_4BIT $T=101200 21600 1 270 $X=76960 $Y=15800
X84 154 VSS! B<15> A<15> B<14> A<14> B<13> A<13> B<12> A<12> VDD! VSS! 156 194 152 147 139 RIPPLE_4BIT_LAST_4BIT $T=101200 27200 1 270 $X=76960 $Y=21400
.ENDS
***************************************
.SUBCKT 3rd_bit SH<2> SH<2>_BAR D 4 5 6 7
** N=48 EP=7 IP=0 FDC=8
M0 4 8 D 4 nfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=360 $D=97
M1 5 SH<2> 8 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1320 $D=97
M2 4 8 D 4 nfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4920 $Y=360 $D=97
M3 7 SH<2>_BAR 8 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=1320 $D=97
M4 6 8 D 6 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1860 $Y=360 $D=189
M5 5 SH<2>_BAR 8 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1320 $D=189
M6 6 8 D 6 pfet L=1.2e-07 W=6.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=360 $D=189
M7 7 SH<2> 8 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1320 $D=189
.ENDS
***************************************
.SUBCKT 1st_bit SH<0> SH<0>_BAR 3 4 5 6 7
** N=30 EP=7 IP=0 FDC=4
M0 5 SH<0> 3 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=520 $D=97
M1 7 SH<0>_BAR 3 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=520 $D=97
M2 5 SH<0>_BAR 3 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=520 $D=189
M3 7 SH<0> 3 6 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
.ENDS
***************************************
.SUBCKT 2rd_bit SH<1> SH<1>_BAR 3 4 5 6 7
** N=28 EP=7 IP=0 FDC=4
M0 5 SH<1> 3 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=520 $D=97
M1 7 SH<1>_BAR 3 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4990 $Y=520 $D=97
M2 5 SH<1>_BAR 3 6 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1930 $Y=520 $D=189
M3 7 SH<1> 3 6 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
.ENDS
***************************************
.SUBCKT Data_Buffer E VSS! IN VDD!
** N=80 EP=4 IP=0 FDC=18
M0 6 IN VSS! VSS! nfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=1260 $D=97
M1 5 6 VSS! VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=2200 $D=97
M2 VSS! 6 5 VSS! nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=2680 $D=97
M3 E 5 VSS! VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=3160 $D=97
M4 VSS! 5 E VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=1260 $D=97
M5 E 5 VSS! VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=1740 $D=97
M6 VSS! 5 E VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=2220 $D=97
M7 E 5 VSS! VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=2700 $D=97
M8 VSS! 5 E VSS! nfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4870 $Y=3180 $D=97
M9 6 IN VDD! VDD! pfet L=1.2e-07 W=7.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1760 $Y=1260 $D=189
M10 E 5 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1790 $Y=3160 $D=189
M11 5 6 VDD! VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2200 $D=189
M12 VDD! 6 5 VDD! pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1990 $Y=2680 $D=189
M13 VDD! 5 E VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1260 $D=189
M14 E 5 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1740 $D=189
M15 VDD! 5 E VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2220 $D=189
M16 E 5 VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2700 $D=189
M17 VDD! 5 E VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3180 $D=189
.ENDS
***************************************
.SUBCKT 4th_bit SH3 SH3_BAR OUT VSS! IN1 VDD! IN2
** N=28 EP=7 IP=0 FDC=4
M0 OUT SH3 IN1 VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=430 $Y=520 $D=97
M1 OUT SH3_BAR IN2 VSS! nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5230 $Y=520 $D=97
M2 OUT SH3_BAR IN1 VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2150 $Y=520 $D=189
M3 OUT SH3 IN2 VDD! pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=520 $D=189
.ENDS
***************************************
.SUBCKT ICV_10 1 2 3 4 5 6 7
** N=8 EP=7 IP=11 FDC=22
X0 3 4 8 5 Data_Buffer $T=0 0 0 0 $X=-240 $Y=-460
X1 1 2 8 4 6 5 7 4th_bit $T=0 -260 0 0 $X=-240 $Y=-460
.ENDS
***************************************
.SUBCKT ICV_11 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27
** N=27 EP=27 IP=56 FDC=76
X0 1 2 14 11 12 13 15 3rd_bit $T=0 -1460 1 0 $X=-250 $Y=-3860
X1 1 2 17 11 16 13 18 3rd_bit $T=6000 -1460 1 0 $X=5750 $Y=-3860
X2 3 4 20 11 19 13 21 1st_bit $T=0 -6660 1 0 $X=-240 $Y=-8460
X3 3 4 22 11 21 13 23 1st_bit $T=6000 -6660 1 0 $X=5760 $Y=-8460
X4 5 6 15 11 24 13 20 2rd_bit $T=0 -4660 1 0 $X=-240 $Y=-6600
X5 5 6 18 11 25 13 22 2rd_bit $T=6000 -4660 1 0 $X=5760 $Y=-6600
X6 7 8 9 11 13 26 14 ICV_10 $T=0 0 0 0 $X=-240 $Y=-460
X7 7 8 10 11 13 27 17 ICV_10 $T=6000 0 0 0 $X=5760 $Y=-460
.ENDS
***************************************
.SUBCKT SHIFTER A<0> A<1> A<2> A<3> A<4> A<5> A<6> A<7> A<8> A<9> A<10> A<11> A<12> A<13> A<14> SEL<3> SEL<2> SEL<1> SEL<0> A<15>
+ E<0> E<1> E<2> E<3> E<4> E<5> E<6> E<7> E<8> E<9> E<10> E<11> E<12> E<13> E<14> E<15> VDD! VSS!
** N=881 EP=38 IP=216 FDC=648
M0 76 73 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=2240 $D=97
M1 VSS! 73 76 VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=2720 $D=97
M2 70 74 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=3200 $D=97
M3 VSS! 74 70 VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=3680 $D=97
M4 77 71 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=7800 $D=97
M5 VSS! 71 77 VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=8280 $D=97
M6 75 72 VSS! VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=8760 $D=97
M7 VSS! 72 75 VSS! nfet L=1.2e-07 W=1.21e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3500 $Y=9240 $D=97
M8 73 112 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=1240 $D=97
M9 VSS! 112 73 VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=1720 $D=97
M10 74 113 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=4200 $D=97
M11 VSS! 113 74 VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=4680 $D=97
M12 71 114 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=6800 $D=97
M13 VSS! 114 71 VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=7280 $D=97
M14 72 115 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=9760 $D=97
M15 VSS! 115 72 VSS! nfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3950 $Y=10240 $D=97
M16 VSS! SEL<3> 112 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4310 $Y=720 $D=97
M17 113 SEL<2> VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4310 $Y=5200 $D=97
M18 VSS! SEL<1> 114 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4310 $Y=6280 $D=97
M19 115 SEL<0> VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4310 $Y=10760 $D=97
M20 VDD! SEL<3> 112 VDD! pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=720 $D=189
M21 73 112 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=1240 $D=189
M22 VDD! 112 73 VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=1720 $D=189
M23 76 73 VDD! VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=2240 $D=189
M24 VDD! 73 76 VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=2720 $D=189
M25 70 74 VDD! VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=3200 $D=189
M26 VDD! 74 70 VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=3680 $D=189
M27 74 113 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=4200 $D=189
M28 VDD! 113 74 VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=4680 $D=189
M29 113 SEL<2> VDD! VDD! pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=5200 $D=189
M30 VDD! SEL<1> 114 VDD! pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=6280 $D=189
M31 71 114 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=6800 $D=189
M32 VDD! 114 71 VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=7280 $D=189
M33 77 71 VDD! VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=7800 $D=189
M34 VDD! 71 77 VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=8280 $D=189
M35 75 72 VDD! VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=8760 $D=189
M36 VDD! 72 75 VDD! pfet L=1.2e-07 W=2.34e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=9240 $D=189
M37 72 115 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=9760 $D=189
M38 VDD! 115 72 VDD! pfet L=1.2e-07 W=1.6e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=10240 $D=189
M39 115 SEL<0> VDD! VDD! pfet L=1.2e-07 W=7.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=10760 $D=189
X40 74 70 72 75 71 77 73 76 E<0> E<1> VSS! VSS! VDD! 78 79 VSS! 81 82 VSS! 80
+ A<0> 83 A<1> VSS! VSS! VDD! VDD!
+ ICV_11 $T=5200 3740 1 0 $X=4950 $Y=-200
X41 74 70 72 75 71 77 73 76 E<2> E<3> VSS! VSS! VDD! 84 85 VSS! 87 88 A<1> 86
+ A<2> 89 A<3> 80 83 VDD! VDD!
+ ICV_11 $T=17200 3740 1 0 $X=16950 $Y=-200
X42 74 70 72 75 71 77 73 76 E<4> E<5> VSS! 79 VDD! 90 91 82 93 94 A<3> 92
+ A<4> 95 A<5> 86 89 VDD! VDD!
+ ICV_11 $T=29200 3740 1 0 $X=28950 $Y=-200
X43 74 70 72 75 71 77 73 76 E<6> E<7> VSS! 85 VDD! 96 97 88 99 100 A<5> 98
+ A<6> 101 A<7> 92 95 VDD! VDD!
+ ICV_11 $T=41200 3740 1 0 $X=40950 $Y=-200
X44 74 70 72 75 71 77 73 76 E<8> E<9> VSS! 91 VDD! 116 102 94 117 104 A<7> 103
+ A<8> 105 A<9> 98 101 78 81
+ ICV_11 $T=53200 3740 1 0 $X=52950 $Y=-200
X45 74 70 72 75 71 77 73 76 E<10> E<11> VSS! 97 VDD! 118 106 100 119 108 A<9> 107
+ A<10> 109 A<11> 103 105 84 87
+ ICV_11 $T=65200 3740 1 0 $X=64950 $Y=-200
X46 74 70 72 75 71 77 73 76 E<12> E<13> VSS! 102 VDD! 120 121 104 122 123 A<11> 110
+ A<12> 111 A<13> 107 109 90 93
+ ICV_11 $T=77200 3740 1 0 $X=76950 $Y=-200
X47 74 70 72 75 71 77 73 76 E<14> E<15> VSS! 106 VDD! 124 125 108 127 128 A<13> 126
+ A<14> 129 A<15> 110 111 96 99
+ ICV_11 $T=89200 3740 1 0 $X=88950 $Y=-200
.ENDS
***************************************
.SUBCKT RF_SCLK CLK VDD! WE<15> WE<14> WE<13> WE<12> WE<11> WE<10> WE<9> WE<8> WE<7> WE<6> WE<5> WE<4> WE<3> WE<2> WE<1> WE<0> WE_CLK_B<15> WE_CLK_B<14>
+ WE_CLK_B<13> WE_CLK_B<12> WE_CLK_B<11> WE_CLK_B<10> WE_CLK_B<9> WE_CLK_B<8> WE_CLK_B<7> WE_CLK_B<6> WE_CLK_B<5> WE_CLK_B<4> WE_CLK_B<3> WE_CLK_B<2> WE_CLK_B<1> WE_CLK_B<0> WE_CLK<15> WE_CLK<14> WE_CLK<13> WE_CLK<12> WE_CLK<11> WE_CLK<10>
+ WE_CLK<9> WE_CLK<8> WE_CLK<7> WE_CLK<6> WE_CLK<5> WE_CLK<4> WE_CLK<3> WE_CLK<2> WE_CLK<1> WE_CLK<0> VSS!
** N=1263 EP=51 IP=0 FDC=224
M0 WE_CLK_B<15> WE_CLK<15> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=470 $D=97
M1 VSS! WE_CLK<15> WE_CLK_B<15> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=950 $D=97
M2 WE_CLK_B<15> WE_CLK<15> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=1430 $D=97
M3 VSS! WE_CLK<15> WE_CLK_B<15> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=1910 $D=97
M4 WE_CLK<15> 52 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=2390 $D=97
M5 WE_CLK_B<14> WE_CLK<14> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=4290 $D=97
M6 VSS! WE_CLK<14> WE_CLK_B<14> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=4770 $D=97
M7 WE_CLK_B<14> WE_CLK<14> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=5250 $D=97
M8 VSS! WE_CLK<14> WE_CLK_B<14> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=5730 $D=97
M9 WE_CLK<14> 53 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=6210 $D=97
M10 WE_CLK_B<13> WE_CLK<13> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=8110 $D=97
M11 VSS! WE_CLK<13> WE_CLK_B<13> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=8590 $D=97
M12 WE_CLK_B<13> WE_CLK<13> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=9070 $D=97
M13 VSS! WE_CLK<13> WE_CLK_B<13> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=9550 $D=97
M14 WE_CLK<13> 54 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=10030 $D=97
M15 WE_CLK_B<12> WE_CLK<12> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=11930 $D=97
M16 VSS! WE_CLK<12> WE_CLK_B<12> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=12410 $D=97
M17 WE_CLK_B<12> WE_CLK<12> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=12890 $D=97
M18 VSS! WE_CLK<12> WE_CLK_B<12> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=13370 $D=97
M19 WE_CLK<12> 55 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=13850 $D=97
M20 WE_CLK_B<11> WE_CLK<11> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=15750 $D=97
M21 VSS! WE_CLK<11> WE_CLK_B<11> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=16230 $D=97
M22 WE_CLK_B<11> WE_CLK<11> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=16710 $D=97
M23 VSS! WE_CLK<11> WE_CLK_B<11> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=17190 $D=97
M24 WE_CLK<11> 56 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=17670 $D=97
M25 WE_CLK_B<10> WE_CLK<10> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=19570 $D=97
M26 VSS! WE_CLK<10> WE_CLK_B<10> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=20050 $D=97
M27 WE_CLK_B<10> WE_CLK<10> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=20530 $D=97
M28 VSS! WE_CLK<10> WE_CLK_B<10> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=21010 $D=97
M29 WE_CLK<10> 57 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=21490 $D=97
M30 WE_CLK_B<9> WE_CLK<9> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=23390 $D=97
M31 VSS! WE_CLK<9> WE_CLK_B<9> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=23870 $D=97
M32 WE_CLK_B<9> WE_CLK<9> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=24350 $D=97
M33 VSS! WE_CLK<9> WE_CLK_B<9> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=24830 $D=97
M34 WE_CLK<9> 58 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=25310 $D=97
M35 WE_CLK_B<8> WE_CLK<8> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=27210 $D=97
M36 VSS! WE_CLK<8> WE_CLK_B<8> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=27690 $D=97
M37 WE_CLK_B<8> WE_CLK<8> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=28170 $D=97
M38 VSS! WE_CLK<8> WE_CLK_B<8> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=28650 $D=97
M39 WE_CLK<8> 59 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=29130 $D=97
M40 WE_CLK_B<7> WE_CLK<7> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=31030 $D=97
M41 VSS! WE_CLK<7> WE_CLK_B<7> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=31510 $D=97
M42 WE_CLK_B<7> WE_CLK<7> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=31990 $D=97
M43 VSS! WE_CLK<7> WE_CLK_B<7> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=32470 $D=97
M44 WE_CLK<7> 60 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=32950 $D=97
M45 WE_CLK_B<6> WE_CLK<6> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=34850 $D=97
M46 VSS! WE_CLK<6> WE_CLK_B<6> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=35330 $D=97
M47 WE_CLK_B<6> WE_CLK<6> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=35810 $D=97
M48 VSS! WE_CLK<6> WE_CLK_B<6> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=36290 $D=97
M49 WE_CLK<6> 61 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=36770 $D=97
M50 WE_CLK_B<5> WE_CLK<5> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=38670 $D=97
M51 VSS! WE_CLK<5> WE_CLK_B<5> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=39150 $D=97
M52 WE_CLK_B<5> WE_CLK<5> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=39630 $D=97
M53 VSS! WE_CLK<5> WE_CLK_B<5> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=40110 $D=97
M54 WE_CLK<5> 62 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=40590 $D=97
M55 WE_CLK_B<4> WE_CLK<4> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=42490 $D=97
M56 VSS! WE_CLK<4> WE_CLK_B<4> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=42970 $D=97
M57 WE_CLK_B<4> WE_CLK<4> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=43450 $D=97
M58 VSS! WE_CLK<4> WE_CLK_B<4> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=43930 $D=97
M59 WE_CLK<4> 63 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=44410 $D=97
M60 WE_CLK_B<3> WE_CLK<3> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=46310 $D=97
M61 VSS! WE_CLK<3> WE_CLK_B<3> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=46790 $D=97
M62 WE_CLK_B<3> WE_CLK<3> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=47270 $D=97
M63 VSS! WE_CLK<3> WE_CLK_B<3> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=47750 $D=97
M64 WE_CLK<3> 64 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=48230 $D=97
M65 WE_CLK_B<2> WE_CLK<2> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=50130 $D=97
M66 VSS! WE_CLK<2> WE_CLK_B<2> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=50610 $D=97
M67 WE_CLK_B<2> WE_CLK<2> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=51090 $D=97
M68 VSS! WE_CLK<2> WE_CLK_B<2> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=51570 $D=97
M69 WE_CLK<2> 65 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=52050 $D=97
M70 WE_CLK_B<1> WE_CLK<1> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=53950 $D=97
M71 VSS! WE_CLK<1> WE_CLK_B<1> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=54430 $D=97
M72 WE_CLK_B<1> WE_CLK<1> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=54910 $D=97
M73 VSS! WE_CLK<1> WE_CLK_B<1> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=55390 $D=97
M74 WE_CLK<1> 66 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=55870 $D=97
M75 WE_CLK_B<0> WE_CLK<0> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=57770 $D=97
M76 VSS! WE_CLK<0> WE_CLK_B<0> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=58250 $D=97
M77 WE_CLK_B<0> WE_CLK<0> VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=58730 $D=97
M78 VSS! WE_CLK<0> WE_CLK_B<0> VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=59210 $D=97
M79 WE_CLK<0> 67 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=59690 $D=97
M80 68 CLK 52 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=3330 $D=97
M81 VSS! WE<15> 68 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=3810 $D=97
M82 69 CLK 53 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=7150 $D=97
M83 VSS! WE<14> 69 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=7630 $D=97
M84 70 CLK 54 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=10970 $D=97
M85 VSS! WE<13> 70 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=11450 $D=97
M86 71 CLK 55 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=14790 $D=97
M87 VSS! WE<12> 71 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=15270 $D=97
M88 72 CLK 56 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=18610 $D=97
M89 VSS! WE<11> 72 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=19090 $D=97
M90 73 CLK 57 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=22430 $D=97
M91 VSS! WE<10> 73 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=22910 $D=97
M92 74 CLK 58 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=26250 $D=97
M93 VSS! WE<9> 74 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=26730 $D=97
M94 75 CLK 59 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=30070 $D=97
M95 VSS! WE<8> 75 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=30550 $D=97
M96 76 CLK 60 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=33890 $D=97
M97 VSS! WE<7> 76 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=34370 $D=97
M98 77 CLK 61 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=37710 $D=97
M99 VSS! WE<6> 77 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=38190 $D=97
M100 78 CLK 62 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=41530 $D=97
M101 VSS! WE<5> 78 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=42010 $D=97
M102 79 CLK 63 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=45350 $D=97
M103 VSS! WE<4> 79 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=45830 $D=97
M104 80 CLK 64 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=49170 $D=97
M105 VSS! WE<3> 80 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=49650 $D=97
M106 81 CLK 65 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=52990 $D=97
M107 VSS! WE<2> 81 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=53470 $D=97
M108 82 CLK 66 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=56810 $D=97
M109 VSS! WE<1> 82 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=57290 $D=97
M110 83 CLK 67 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=60630 $D=97
M111 VSS! WE<0> 83 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4210 $Y=61110 $D=97
M112 WE_CLK_B<15> WE_CLK<15> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=470 $D=189
M113 VDD! WE_CLK<15> WE_CLK_B<15> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=950 $D=189
M114 WE_CLK_B<15> WE_CLK<15> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=1430 $D=189
M115 VDD! WE_CLK<15> WE_CLK_B<15> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=1910 $D=189
M116 WE_CLK<15> 52 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=2390 $D=189
M117 52 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=3330 $D=189
M118 VDD! WE<15> 52 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=3810 $D=189
M119 WE_CLK_B<14> WE_CLK<14> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=4290 $D=189
M120 VDD! WE_CLK<14> WE_CLK_B<14> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=4770 $D=189
M121 WE_CLK_B<14> WE_CLK<14> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=5250 $D=189
M122 VDD! WE_CLK<14> WE_CLK_B<14> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=5730 $D=189
M123 WE_CLK<14> 53 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=6210 $D=189
M124 53 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=7150 $D=189
M125 VDD! WE<14> 53 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=7630 $D=189
M126 WE_CLK_B<13> WE_CLK<13> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=8110 $D=189
M127 VDD! WE_CLK<13> WE_CLK_B<13> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=8590 $D=189
M128 WE_CLK_B<13> WE_CLK<13> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=9070 $D=189
M129 VDD! WE_CLK<13> WE_CLK_B<13> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=9550 $D=189
M130 WE_CLK<13> 54 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=10030 $D=189
M131 54 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=10970 $D=189
M132 VDD! WE<13> 54 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=11450 $D=189
M133 WE_CLK_B<12> WE_CLK<12> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=11930 $D=189
M134 VDD! WE_CLK<12> WE_CLK_B<12> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=12410 $D=189
M135 WE_CLK_B<12> WE_CLK<12> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=12890 $D=189
M136 VDD! WE_CLK<12> WE_CLK_B<12> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=13370 $D=189
M137 WE_CLK<12> 55 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=13850 $D=189
M138 55 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=14790 $D=189
M139 VDD! WE<12> 55 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=15270 $D=189
M140 WE_CLK_B<11> WE_CLK<11> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=15750 $D=189
M141 VDD! WE_CLK<11> WE_CLK_B<11> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=16230 $D=189
M142 WE_CLK_B<11> WE_CLK<11> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=16710 $D=189
M143 VDD! WE_CLK<11> WE_CLK_B<11> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=17190 $D=189
M144 WE_CLK<11> 56 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=17670 $D=189
M145 56 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=18610 $D=189
M146 VDD! WE<11> 56 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=19090 $D=189
M147 WE_CLK_B<10> WE_CLK<10> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=19570 $D=189
M148 VDD! WE_CLK<10> WE_CLK_B<10> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=20050 $D=189
M149 WE_CLK_B<10> WE_CLK<10> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=20530 $D=189
M150 VDD! WE_CLK<10> WE_CLK_B<10> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=21010 $D=189
M151 WE_CLK<10> 57 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=21490 $D=189
M152 57 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=22430 $D=189
M153 VDD! WE<10> 57 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=22910 $D=189
M154 WE_CLK_B<9> WE_CLK<9> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=23390 $D=189
M155 VDD! WE_CLK<9> WE_CLK_B<9> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=23870 $D=189
M156 WE_CLK_B<9> WE_CLK<9> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=24350 $D=189
M157 VDD! WE_CLK<9> WE_CLK_B<9> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=24830 $D=189
M158 WE_CLK<9> 58 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=25310 $D=189
M159 58 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=26250 $D=189
M160 VDD! WE<9> 58 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=26730 $D=189
M161 WE_CLK_B<8> WE_CLK<8> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=27210 $D=189
M162 VDD! WE_CLK<8> WE_CLK_B<8> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=27690 $D=189
M163 WE_CLK_B<8> WE_CLK<8> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=28170 $D=189
M164 VDD! WE_CLK<8> WE_CLK_B<8> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=28650 $D=189
M165 WE_CLK<8> 59 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=29130 $D=189
M166 59 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=30070 $D=189
M167 VDD! WE<8> 59 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=30550 $D=189
M168 WE_CLK_B<7> WE_CLK<7> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=31030 $D=189
M169 VDD! WE_CLK<7> WE_CLK_B<7> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=31510 $D=189
M170 WE_CLK_B<7> WE_CLK<7> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=31990 $D=189
M171 VDD! WE_CLK<7> WE_CLK_B<7> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=32470 $D=189
M172 WE_CLK<7> 60 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=32950 $D=189
M173 60 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=33890 $D=189
M174 VDD! WE<7> 60 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=34370 $D=189
M175 WE_CLK_B<6> WE_CLK<6> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=34850 $D=189
M176 VDD! WE_CLK<6> WE_CLK_B<6> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=35330 $D=189
M177 WE_CLK_B<6> WE_CLK<6> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=35810 $D=189
M178 VDD! WE_CLK<6> WE_CLK_B<6> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=36290 $D=189
M179 WE_CLK<6> 61 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=36770 $D=189
M180 61 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=37710 $D=189
M181 VDD! WE<6> 61 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=38190 $D=189
M182 WE_CLK_B<5> WE_CLK<5> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=38670 $D=189
M183 VDD! WE_CLK<5> WE_CLK_B<5> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=39150 $D=189
M184 WE_CLK_B<5> WE_CLK<5> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=39630 $D=189
M185 VDD! WE_CLK<5> WE_CLK_B<5> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=40110 $D=189
M186 WE_CLK<5> 62 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=40590 $D=189
M187 62 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=41530 $D=189
M188 VDD! WE<5> 62 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=42010 $D=189
M189 WE_CLK_B<4> WE_CLK<4> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=42490 $D=189
M190 VDD! WE_CLK<4> WE_CLK_B<4> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=42970 $D=189
M191 WE_CLK_B<4> WE_CLK<4> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=43450 $D=189
M192 VDD! WE_CLK<4> WE_CLK_B<4> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=43930 $D=189
M193 WE_CLK<4> 63 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=44410 $D=189
M194 63 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=45350 $D=189
M195 VDD! WE<4> 63 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=45830 $D=189
M196 WE_CLK_B<3> WE_CLK<3> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=46310 $D=189
M197 VDD! WE_CLK<3> WE_CLK_B<3> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=46790 $D=189
M198 WE_CLK_B<3> WE_CLK<3> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=47270 $D=189
M199 VDD! WE_CLK<3> WE_CLK_B<3> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=47750 $D=189
M200 WE_CLK<3> 64 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=48230 $D=189
M201 64 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=49170 $D=189
M202 VDD! WE<3> 64 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=49650 $D=189
M203 WE_CLK_B<2> WE_CLK<2> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=50130 $D=189
M204 VDD! WE_CLK<2> WE_CLK_B<2> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=50610 $D=189
M205 WE_CLK_B<2> WE_CLK<2> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=51090 $D=189
M206 VDD! WE_CLK<2> WE_CLK_B<2> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=51570 $D=189
M207 WE_CLK<2> 65 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=52050 $D=189
M208 65 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=52990 $D=189
M209 VDD! WE<2> 65 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=53470 $D=189
M210 WE_CLK_B<1> WE_CLK<1> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=53950 $D=189
M211 VDD! WE_CLK<1> WE_CLK_B<1> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=54430 $D=189
M212 WE_CLK_B<1> WE_CLK<1> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=54910 $D=189
M213 VDD! WE_CLK<1> WE_CLK_B<1> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=55390 $D=189
M214 WE_CLK<1> 66 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=55870 $D=189
M215 66 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=56810 $D=189
M216 VDD! WE<1> 66 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=57290 $D=189
M217 WE_CLK_B<0> WE_CLK<0> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=57770 $D=189
M218 VDD! WE_CLK<0> WE_CLK_B<0> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=58250 $D=189
M219 WE_CLK_B<0> WE_CLK<0> VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=58730 $D=189
M220 VDD! WE_CLK<0> WE_CLK_B<0> VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=59210 $D=189
M221 WE_CLK<0> 67 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=59690 $D=189
M222 67 CLK VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=60630 $D=189
M223 VDD! WE<0> 67 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=61110 $D=189
.ENDS
***************************************
.SUBCKT RF_slave_1 we_clkb READ_B we_clk READ_A S QA QB VSS! VDD!
** N=95 EP=9 IP=0 FDC=20
M0 16 11 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1440 $D=97
M1 12 we_clkb 16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1890 $D=97
M2 13 we_clk 12 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3000 $D=97
M3 VSS! S 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3480 $D=97
M4 10 12 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3960 $D=97
M5 10 READ_B QB VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=3120 $D=97
M6 QA READ_A 10 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=3600 $D=97
M7 VSS! 12 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=1620 $D=97
M8 14 READ_B VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=2140 $D=97
M9 VSS! READ_A 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=4560 $D=97
M10 17 11 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=1440 $D=189
M11 12 we_clk 17 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=1890 $D=189
M12 13 we_clkb 12 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3000 $D=189
M13 VDD! S 13 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3480 $D=189
M14 10 12 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3960 $D=189
M15 VDD! 12 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1620 $D=189
M16 14 READ_B VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2140 $D=189
M17 10 14 QB VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3120 $D=189
M18 QA 15 10 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3600 $D=189
M19 VDD! READ_A 15 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=4560 $D=189
.ENDS
***************************************
.SUBCKT ICV_5 1 2 3 4 5 6 7 8 9 10 11 12
** N=12 EP=12 IP=18 FDC=40
X0 1 2 3 4 5 6 7 11 12 RF_slave_1 $T=0 0 0 0 $X=-320 $Y=0
X1 1 2 3 4 8 9 10 11 12 RF_slave_1 $T=6000 0 0 0 $X=5680 $Y=0
.ENDS
***************************************
.SUBCKT ICV_6 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
** N=18 EP=18 IP=24 FDC=80
X0 1 2 3 4 5 6 7 8 9 10 17 18 ICV_5 $T=0 0 0 0 $X=-320 $Y=0
X1 1 2 3 4 11 12 13 14 15 16 17 18 ICV_5 $T=12000 0 0 0 $X=11680 $Y=0
.ENDS
***************************************
.SUBCKT RF_WD_1 WE_CLKB READ_B WE_CLK READ_A S<0> QA<0> QB<0> S<1> QA<1> QB<1> S<2> QA<2> QB<2> S<3> QA<3> QB<3> S<4> QA<4> QB<4> S<5>
+ QA<5> QB<5> S<6> QA<6> QB<6> S<7> QA<7> QB<7> S<8> QA<8> QB<8> S<9> QA<9> QB<9> S<10> QA<10> QB<10> S<11> QA<11> QB<11>
+ S<12> QA<12> QB<12> S<13> QA<13> QB<13> S<14> QA<14> QB<14> S<15> QA<15> QB<15> VSS! VDD!
** N=54 EP=54 IP=72 FDC=320
X0 WE_CLKB READ_B WE_CLK READ_A S<0> QA<0> QB<0> S<1> QA<1> QB<1> S<2> QA<2> QB<2> S<3> QA<3> QB<3> VSS! VDD! ICV_6 $T=0 0 0 0 $X=-320 $Y=0
X1 WE_CLKB READ_B WE_CLK READ_A S<4> QA<4> QB<4> S<5> QA<5> QB<5> S<6> QA<6> QB<6> S<7> QA<7> QB<7> VSS! VDD! ICV_6 $T=24000 0 0 0 $X=23680 $Y=0
X2 WE_CLKB READ_B WE_CLK READ_A S<8> QA<8> QB<8> S<9> QA<9> QB<9> S<10> QA<10> QB<10> S<11> QA<11> QB<11> VSS! VDD! ICV_6 $T=48000 0 0 0 $X=47680 $Y=0
X3 WE_CLKB READ_B WE_CLK READ_A S<12> QA<12> QB<12> S<13> QA<13> QB<13> S<14> QA<14> QB<14> S<15> QA<15> QB<15> VSS! VDD! ICV_6 $T=72000 0 0 0 $X=71680 $Y=0
.ENDS
***************************************
.SUBCKT RF_slave READ_B we_clkb we_clk READ_A S QA QB VSS! VDD!
** N=94 EP=9 IP=0 FDC=20
M0 VSS! 11 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=960 $D=97
M1 16 12 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1440 $D=97
M2 11 we_clkb 16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1890 $D=97
M3 13 we_clk 11 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3000 $D=97
M4 VSS! S 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3480 $D=97
M5 10 11 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3960 $D=97
M6 10 READ_B QB VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=2520 $D=97
M7 QA READ_A 10 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=3000 $D=97
M8 14 READ_B VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=1540 $D=97
M9 VSS! READ_A 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=3960 $D=97
M10 VDD! 11 12 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=960 $D=189
M11 17 12 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=1440 $D=189
M12 11 we_clk 17 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=1890 $D=189
M13 13 we_clkb 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3000 $D=189
M14 VDD! S 13 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3480 $D=189
M15 10 11 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1950 $Y=3960 $D=189
M16 14 READ_B VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1540 $D=189
M17 10 14 QB VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2520 $D=189
M18 QA 15 10 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3000 $D=189
M19 VDD! READ_A 15 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3960 $D=189
.ENDS
***************************************
.SUBCKT ICV_7 1 2 3 4 5 6 7 8 9 10 11 12
** N=12 EP=12 IP=18 FDC=40
X0 1 2 3 4 5 6 7 11 12 RF_slave $T=0 0 0 0 $X=-320 $Y=0
X1 1 2 3 4 8 9 10 11 12 RF_slave $T=6000 0 0 0 $X=5680 $Y=0
.ENDS
***************************************
.SUBCKT ICV_8 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
** N=18 EP=18 IP=24 FDC=80
X0 1 2 3 4 5 6 7 8 9 10 17 18 ICV_7 $T=0 0 0 0 $X=-320 $Y=0
X1 1 2 3 4 11 12 13 14 15 16 17 18 ICV_7 $T=12000 0 0 0 $X=11680 $Y=0
.ENDS
***************************************
.SUBCKT RF_WD READ_B WE_CLKB WE_CLK READ_A S<0> QA<0> QB<0> S<1> QA<1> QB<1> S<2> QA<2> QB<2> S<3> QA<3> QB<3> S<4> QA<4> QB<4> S<5>
+ QA<5> QB<5> S<6> QA<6> QB<6> S<7> QA<7> QB<7> S<8> QA<8> QB<8> S<9> QA<9> QB<9> S<10> QA<10> QB<10> S<11> QA<11> QB<11>
+ S<12> QA<12> QB<12> S<13> QA<13> QB<13> S<14> QA<14> QB<14> S<15> QA<15> QB<15> VSS! VDD!
** N=54 EP=54 IP=72 FDC=320
X0 READ_B WE_CLKB WE_CLK READ_A S<0> QA<0> QB<0> S<1> QA<1> QB<1> S<2> QA<2> QB<2> S<3> QA<3> QB<3> VSS! VDD! ICV_8 $T=0 0 0 0 $X=-320 $Y=0
X1 READ_B WE_CLKB WE_CLK READ_A S<4> QA<4> QB<4> S<5> QA<5> QB<5> S<6> QA<6> QB<6> S<7> QA<7> QB<7> VSS! VDD! ICV_8 $T=24000 0 0 0 $X=23680 $Y=0
X2 READ_B WE_CLKB WE_CLK READ_A S<8> QA<8> QB<8> S<9> QA<9> QB<9> S<10> QA<10> QB<10> S<11> QA<11> QB<11> VSS! VDD! ICV_8 $T=48000 0 0 0 $X=47680 $Y=0
X3 READ_B WE_CLKB WE_CLK READ_A S<12> QA<12> QB<12> S<13> QA<13> QB<13> S<14> QA<14> QB<14> S<15> QA<15> QB<15> VSS! VDD! ICV_8 $T=72000 0 0 0 $X=71680 $Y=0
.ENDS
***************************************
.SUBCKT ICV_9 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
+ 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
+ 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58
** N=58 EP=58 IP=108 FDC=640
X0 1 2 3 4 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
+ 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44
+ 45 46 47 48 49 50 51 52 53 54 55 56 57 58
+ RF_WD_1 $T=0 0 0 0 $X=-320 $Y=0
X1 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
+ 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44
+ 45 46 47 48 49 50 51 52 53 54 55 56 57 58
+ RF_WD $T=0 4000 0 0 $X=-320 $Y=4000
.ENDS
***************************************
.SUBCKT RF_master WE_MASTER_B WE_MASTER D MASTER_DATA VSS! VDD!
** N=92 EP=6 IP=0 FDC=24
M0 VSS! 7 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=5290 $D=97
M1 9 D VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=850 $Y=430 $D=97
M2 10 8 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=5220 $D=97
M3 7 WE_MASTER 9 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1360 $Y=430 $D=97
M4 VSS! 8 10 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=5220 $D=97
M5 MASTER_DATA 10 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1940 $Y=5130 $D=97
M6 12 WE_MASTER_B 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2140 $Y=430 $D=97
M7 VSS! 10 MASTER_DATA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2420 $Y=5130 $D=97
M8 VSS! 11 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2560 $Y=430 $D=97
M9 MASTER_DATA 10 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2900 $Y=5130 $D=97
M10 11 7 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3040 $Y=430 $D=97
M11 VSS! 10 MASTER_DATA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3380 $Y=5130 $D=97
M12 VDD! 7 8 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=3510 $D=189
M13 9 D VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=850 $Y=1950 $D=189
M14 10 8 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=3510 $D=189
M15 VDD! 8 10 VDD! pfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=3510 $D=189
M16 7 WE_MASTER_B 9 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1600 $Y=1950 $D=189
M17 MASTER_DATA 10 VDD! VDD! pfet L=1.2e-07 W=8.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1940 $Y=3510 $D=189
M18 13 WE_MASTER 7 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2140 $Y=1950 $D=189
M19 VDD! 10 MASTER_DATA VDD! pfet L=1.2e-07 W=8.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2420 $Y=3510 $D=189
M20 VDD! 11 13 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2560 $Y=1950 $D=189
M21 MASTER_DATA 10 VDD! VDD! pfet L=1.2e-07 W=8.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2900 $Y=3510 $D=189
M22 11 7 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3040 $Y=1950 $D=189
M23 VDD! 10 MASTER_DATA VDD! pfet L=1.2e-07 W=8.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3380 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT ICV_4 1 2 3 4 5 6 7 8
** N=8 EP=8 IP=12 FDC=48
X0 1 2 3 4 8 7 RF_master $T=0 -6000 0 0 $X=-200 $Y=-6300
X1 1 2 5 6 8 7 RF_master $T=0 0 0 0 $X=-200 $Y=-300
.ENDS
***************************************
.SUBCKT RF_BUFFER BUFF_A QA BUFF_B QB VSS! VDD!
** N=76 EP=6 IP=0 FDC=16
M0 VSS! 8 QA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1010 $D=97
M1 QA 8 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1490 $D=97
M2 VSS! 8 QA VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1970 $D=97
M3 VSS! BUFF_A 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2960 $D=97
M4 VSS! 9 QB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=780 $D=97
M5 QB 9 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=1260 $D=97
M6 VSS! 9 QB VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=1740 $D=97
M7 VSS! BUFF_B 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=2680 $D=97
M8 VDD! 8 QA VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1490 $Y=1010 $D=189
M9 QA 8 VDD! VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1490 $Y=1490 $D=189
M10 VDD! 8 QA VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1490 $Y=1970 $D=189
M11 VDD! BUFF_A 8 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1850 $Y=2960 $D=189
M12 VDD! 9 QB VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=780 $D=189
M13 QB 9 VDD! VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1260 $D=189
M14 VDD! 9 QB VDD! pfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1740 $D=189
M15 VDD! BUFF_B 9 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2680 $D=189
.ENDS
***************************************
.SUBCKT ICV_3 1 2 3 4 5 6 7 8 9 10
** N=12 EP=10 IP=14 FDC=32
X0 1 2 3 4 9 10 RF_BUFFER $T=0 0 0 0 $X=-240 $Y=0
X1 5 6 7 8 9 10 RF_BUFFER $T=6000 0 0 0 $X=5760 $Y=0
.ENDS
***************************************
.SUBCKT RF WE_MASTER 2 READ_B<15> READ_A<15> READ_B<14> READ_A<14> READ_B<13> READ_A<13> READ_B<12> READ_A<12> READ_B<11> READ_A<11> READ_B<10> READ_A<10> READ_B<9> READ_A<9> READ_B<8> READ_A<8> READ_B<7> READ_A<7>
+ READ_B<6> READ_A<6> READ_B<5> READ_A<5> READ_B<4> READ_A<4> READ_B<3> READ_A<3> READ_B<2> READ_A<2> READ_B<1> READ_A<1> READ_B<0> READ_A<0> CLK D<1> D<0> D<3> D<2> D<5>
+ D<4> D<7> D<6> D<9> D<8> D<11> D<10> D<13> D<12> D<15> D<14> QA<0> QB<0> QA<1> QB<1> QA<2> QB<2> QA<3> QB<3> QA<4>
+ QB<4> QA<5> QB<5> QA<6> QB<6> QA<7> QB<7> QA<8> QB<8> QA<9> QB<9> QA<10> QB<10> QA<11> QB<11> QA<12> QB<12> QA<13> QB<13> QA<14>
+ QB<14> QA<15> QB<15> VDD! VSS! WE<15> WE<14> WE<13> WE<12> WE<11> WE<10> WE<9> WE<8> WE<7> WE<6> WE<5> WE<4> WE<3> WE<2> WE<1>
+ WE<0>
** N=273 EP=101 IP=675 FDC=5998
M0 134 182 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=63990 $D=97
M1 VSS! 182 134 VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=64470 $D=97
M2 134 182 VSS! VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=64950 $D=97
M3 VSS! 182 134 VSS! nfet L=1.2e-07 W=1.12e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=65430 $D=97
M4 182 183 VSS! VSS! nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4190 $Y=65970 $D=97
M5 VSS! 183 182 VSS! nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4190 $Y=66450 $D=97
M6 183 WE_MASTER VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4470 $Y=66990 $D=97
M7 134 182 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=63990 $D=189
M8 VDD! 182 134 VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=64470 $D=189
M9 134 182 VDD! VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=64950 $D=189
M10 VDD! 182 134 VDD! pfet L=1.2e-07 W=2.16e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=65430 $D=189
M11 182 183 VDD! VDD! pfet L=1.2e-07 W=1.08e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=65970 $D=189
M12 VDD! 183 182 VDD! pfet L=1.2e-07 W=1.08e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=66450 $D=189
M13 183 WE_MASTER VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=510 $Y=66990 $D=189
X14 CLK VDD! WE<15> WE<14> WE<13> WE<12> WE<11> WE<10> WE<9> WE<8> WE<7> WE<6> WE<5> WE<4> WE<3> WE<2> WE<1> WE<0> 102 104
+ 106 108 110 112 114 116 118 120 122 124 126 128 129 130 103 105 107 109 111 113
+ 115 117 119 121 123 125 127 131 132 133 VSS!
+ RF_SCLK $T=0 2400 0 0 $X=-240 $Y=2200
X15 102 READ_B<15> 103 READ_A<15> READ_B<14> 104 105 READ_A<14> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 2000 0 0 $X=4880 $Y=2000
X16 106 READ_B<13> 107 READ_A<13> READ_B<12> 108 109 READ_A<12> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 9600 0 0 $X=4880 $Y=9600
X17 110 READ_B<11> 111 READ_A<11> READ_B<10> 112 113 READ_A<10> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 17200 0 0 $X=4880 $Y=17200
X18 114 READ_B<9> 115 READ_A<9> READ_B<8> 116 117 READ_A<8> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 24800 0 0 $X=4880 $Y=24800
X19 118 READ_B<7> 119 READ_A<7> READ_B<6> 120 121 READ_A<6> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 32400 0 0 $X=4880 $Y=32400
X20 122 READ_B<5> 123 READ_A<5> READ_B<4> 124 125 READ_A<4> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 40000 0 0 $X=4880 $Y=40000
X21 126 READ_B<3> 127 READ_A<3> READ_B<2> 128 131 READ_A<2> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 47600 0 0 $X=4880 $Y=47600
X22 129 READ_B<1> 132 READ_A<1> READ_B<0> 130 133 READ_A<0> 150 135 151 152 2 153 154 136 155 156 137 157
+ 158 138 159 160 139 161 162 140 163 164 141 165 166 142 167 168 143 169 170 144
+ 171 172 145 173 174 146 175 176 147 177 178 148 179 180 149 181 VSS! VDD!
+ ICV_9 $T=5200 55200 0 0 $X=4880 $Y=55200
X23 134 WE_MASTER D<1> 152 D<0> 150 VDD! VSS! ICV_4 $T=11200 67600 1 270 $X=4900 $Y=63200
X24 134 WE_MASTER D<3> 156 D<2> 154 VDD! VSS! ICV_4 $T=23200 67600 1 270 $X=16900 $Y=63200
X25 134 WE_MASTER D<5> 160 D<4> 158 VDD! VSS! ICV_4 $T=35200 67600 1 270 $X=28900 $Y=63200
X26 134 WE_MASTER D<7> 164 D<6> 162 VDD! VSS! ICV_4 $T=47200 67600 1 270 $X=40900 $Y=63200
X27 134 WE_MASTER D<9> 168 D<8> 166 VDD! VSS! ICV_4 $T=59200 67600 1 270 $X=52900 $Y=63200
X28 134 WE_MASTER D<11> 172 D<10> 170 VDD! VSS! ICV_4 $T=71200 67600 1 270 $X=64900 $Y=63200
X29 134 WE_MASTER D<13> 176 D<12> 174 VDD! VSS! ICV_4 $T=83200 67600 1 270 $X=76900 $Y=63200
X30 134 WE_MASTER D<15> 180 D<14> 178 VDD! VSS! ICV_4 $T=95200 67600 1 270 $X=88900 $Y=63200
X31 135 QA<0> 151 QB<0> 2 QA<1> 153 QB<1> VSS! VDD! ICV_3 $T=5200 0 0 0 $X=4960 $Y=0
X32 136 QA<2> 155 QB<2> 137 QA<3> 157 QB<3> VSS! VDD! ICV_3 $T=17200 0 0 0 $X=16960 $Y=0
X33 138 QA<4> 159 QB<4> 139 QA<5> 161 QB<5> VSS! VDD! ICV_3 $T=29200 0 0 0 $X=28960 $Y=0
X34 140 QA<6> 163 QB<6> 141 QA<7> 165 QB<7> VSS! VDD! ICV_3 $T=41200 0 0 0 $X=40960 $Y=0
X35 142 QA<8> 167 QB<8> 143 QA<9> 169 QB<9> VSS! VDD! ICV_3 $T=53200 0 0 0 $X=52960 $Y=0
X36 144 QA<10> 171 QB<10> 145 QA<11> 173 QB<11> VSS! VDD! ICV_3 $T=65200 0 0 0 $X=64960 $Y=0
X37 146 QA<12> 175 QB<12> 147 QA<13> 177 QB<13> VSS! VDD! ICV_3 $T=77200 0 0 0 $X=76960 $Y=0
X38 148 QA<14> 179 QB<14> 149 QA<15> 181 QB<15> VSS! VDD! ICV_3 $T=89200 0 0 0 $X=88960 $Y=0
.ENDS
***************************************
.SUBCKT MUX_Rsrc_1bit SEL<1> SEL_BAR<1> SEL_BAR<0> SEL<0> ALU_B Imm_16 Rsrc VSS! VDD!
** N=85 EP=9 IP=0 FDC=18
M0 12 SEL<1> 10 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=540 $D=97
M1 11 SEL_BAR<1> 12 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1100 $D=97
M2 ALU_B 12 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2240 $D=97
M3 VSS! 12 ALU_B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2800 $D=97
M4 ALU_B 12 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3360 $D=97
M5 VSS! 12 ALU_B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3900 $D=97
M6 VSS! 10 11 VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=2340 $D=97
M7 10 SEL<0> Imm_16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5250 $Y=540 $D=97
M8 Rsrc SEL_BAR<0> 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5250 $Y=1100 $D=97
M9 ALU_B 12 VDD! VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=2240 $D=189
M10 VDD! 12 ALU_B VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=2800 $D=189
M11 ALU_B 12 VDD! VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=3360 $D=189
M12 VDD! 12 ALU_B VDD! pfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1960 $Y=3900 $D=189
M13 12 SEL_BAR<1> 10 VDD! pfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2050 $Y=540 $D=189
M14 11 SEL<1> 12 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=1100 $D=189
M15 10 SEL_BAR<0> Imm_16 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=540 $D=189
M16 Rsrc SEL<0> 10 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=1100 $D=189
M17 VDD! 10 11 VDD! pfet L=1.2e-07 W=6.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3530 $Y=2340 $D=189
.ENDS
***************************************
.SUBCKT MUX_Rdest_1bit ALU_A Rdest SEL SEL_BAR VSS! VDD!
** N=54 EP=6 IP=0 FDC=10
M0 7 SEL VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=970 $D=97
M1 Rdest SEL_BAR 7 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1730 $D=97
M2 VSS! 7 8 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=870 $D=97
M3 ALU_A 8 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1400 $D=97
M4 VSS! 8 ALU_A VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1880 $D=97
M5 7 SEL_BAR VSS! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=970 $D=189
M6 Rdest SEL 7 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1730 $D=189
M7 VDD! 7 8 VDD! pfet L=1.2e-07 W=7.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=870 $D=189
M8 ALU_A 8 VDD! VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1400 $D=189
M9 VDD! 8 ALU_A VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1880 $D=189
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
** N=18 EP=18 IP=30 FDC=56
X0 1 2 3 4 7 8 9 17 18 MUX_Rsrc_1bit $T=0 0 0 0 $X=-240 $Y=-160
X1 1 2 3 4 10 11 12 17 18 MUX_Rsrc_1bit $T=6000 0 0 0 $X=5760 $Y=-160
X2 5 13 14 15 17 18 MUX_Rdest_1bit $T=6000 4400 1 180 $X=-240 $Y=4380
X3 6 16 14 15 17 18 MUX_Rdest_1bit $T=12000 4400 1 180 $X=5760 $Y=4380
.ENDS
***************************************
.SUBCKT MUX_Shifter_1bit Rdest Shifter SEL Imm_16 SEL_BAR VSS! VDD!
** N=50 EP=7 IP=0 FDC=10
M0 8 SEL Imm_16 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=570 $D=97
M1 Rdest SEL_BAR 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1330 $D=97
M2 VSS! 8 9 VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=470 $D=97
M3 Shifter 9 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1000 $D=97
M4 VSS! 9 Shifter VSS! nfet L=1.2e-07 W=4.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5130 $Y=1480 $D=97
M5 8 SEL_BAR Imm_16 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=570 $D=189
M6 Rdest SEL 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=1330 $D=189
M7 VDD! 8 9 VDD! pfet L=1.2e-07 W=7.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=470 $D=189
M8 Shifter 9 VDD! VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1000 $D=189
M9 VDD! 9 Shifter VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=1480 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9 10
** N=10 EP=10 IP=14 FDC=20
X0 3 1 4 5 6 9 10 MUX_Shifter_1bit $T=-6000 0 0 0 $X=-6240 $Y=-300
X1 7 2 4 8 6 9 10 MUX_Shifter_1bit $T=0 0 0 0 $X=-240 $Y=-300
.ENDS
***************************************
.SUBCKT datapath_final_1 clk VDD! CEN_1 VSS! Vref<0> Vref<1> Vref<2> Vref<3> Vref<4> Vref<5> Vref<6> Vref<7> Vref<8> Vref<9> Vref<10> Vref<11> Vref<12> Vref<13> Vref<14> Vref<15>
+ Vref<16> Vref<17> Vref<18> Vref<19> Vref<20> Vref<21> Vref<22> Vref<23> Vref<24> Vref<25> Vref<26> Vref<27> Vref<28> Vref<29> Vref<30> Vref<31> resetn scan_in scan_en scan_out
** N=828 EP=40 IP=1349 FDC=15519
M0 VSS! 443 444 VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=34040 $D=97
M1 444 443 VSS! VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=34600 $D=97
M2 VSS! 443 444 VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=35160 $D=97
M3 443 518 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=35720 $D=97
M4 VSS! 518 443 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=36280 $D=97
M5 518 433 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=125650 $Y=36840 $D=97
M6 447 519 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130400 $Y=35720 $D=97
M7 VSS! 519 447 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130400 $Y=36280 $D=97
M8 VSS! 447 445 VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130420 $Y=34040 $D=97
M9 445 447 VSS! VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130420 $Y=34600 $D=97
M10 VSS! 447 445 VSS! nfet L=1.2e-07 W=3.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130420 $Y=35160 $D=97
M11 519 264 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=130470 $Y=36840 $D=97
M12 VSS! 480 479 VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=83450 $D=97
M13 479 480 VSS! VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=84010 $D=97
M14 VSS! 480 479 VSS! nfet L=1.2e-07 W=3.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=84570 $D=97
M15 480 520 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=85130 $D=97
M16 VSS! 520 480 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=85690 $D=97
M17 520 442 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=131650 $Y=86250 $D=97
M18 458 456 VSS! VSS! nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138680 $Y=39850 $D=97
M19 VSS! 456 458 VSS! nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138680 $Y=40330 $D=97
M20 459 457 VSS! VSS! nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138680 $Y=54250 $D=97
M21 VSS! 457 459 VSS! nfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138680 $Y=54730 $D=97
M22 456 521 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138800 $Y=38850 $D=97
M23 VSS! 521 456 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138800 $Y=39330 $D=97
M24 457 522 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138800 $Y=53250 $D=97
M25 VSS! 522 457 VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138800 $Y=53730 $D=97
M26 VSS! 259 521 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138870 $Y=38320 $D=97
M27 VSS! 275 522 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=138870 $Y=52720 $D=97
M28 VDD! 443 444 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127030 $Y=34040 $D=189
M29 444 443 VDD! VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127030 $Y=34600 $D=189
M30 VDD! 443 444 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127030 $Y=35160 $D=189
M31 443 518 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127070 $Y=35720 $D=189
M32 VDD! 518 443 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127070 $Y=36280 $D=189
M33 518 433 VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=127190 $Y=36840 $D=189
M34 VDD! 447 445 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=34040 $D=189
M35 445 447 VDD! VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=34600 $D=189
M36 VDD! 447 445 VDD! pfet L=1.2e-07 W=6.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=35160 $D=189
M37 447 519 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=35720 $D=189
M38 VDD! 519 447 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=36280 $D=189
M39 519 264 VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=128730 $Y=36840 $D=189
M40 VDD! 480 479 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=83450 $D=189
M41 479 480 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=84010 $D=189
M42 VDD! 480 479 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=84570 $D=189
M43 480 520 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=85130 $D=189
M44 VDD! 520 480 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133090 $Y=85690 $D=189
M45 520 442 VDD! VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=133210 $Y=86250 $D=189
M46 VDD! 259 521 VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=38320 $D=189
M47 456 521 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=38850 $D=189
M48 VDD! 521 456 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=39330 $D=189
M49 458 456 VDD! VDD! pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=39850 $D=189
M50 VDD! 456 458 VDD! pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=40330 $D=189
M51 VDD! 275 522 VDD! pfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=52720 $D=189
M52 457 522 VDD! VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=53250 $D=189
M53 VDD! 522 457 VDD! pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=53730 $D=189
M54 459 457 VDD! VDD! pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=54250 $D=189
M55 VDD! 457 459 VDD! pfet L=1.2e-07 W=9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=134910 $Y=54730 $D=189
X56 VSS! VDD! 335 325 158 166 136 327 413 168 137 141 162 416 138 326 164 417 139 149
+ 414 418 323 329 415 420 324 123 129 122 124 125 130 169 167 126 131 163 159 160
+ 153 127 132 154 133 150 155 156 151 157 152 134 128 142 145 146 143 147 144 332
+ 328 161 330 331 333 170 165 233 148 171 172 419 173 Vref<0> Vref<1> Vref<2> Vref<3> Vref<4> Vref<5> Vref<6>
+ Vref<7> Vref<8> Vref<9> Vref<10> Vref<11> Vref<12> Vref<13> Vref<14> Vref<15> Vref<16> Vref<17> Vref<18> Vref<19> Vref<20> Vref<21> Vref<22> Vref<23> Vref<24> Vref<25> Vref<26>
+ Vref<27> Vref<28> Vref<29> Vref<30> Vref<31> 54 57 60 65 68 72 75 80 84 89 92 97 102 105 108
+ 112 37 38 41 26 17 18 1 2 3 6 9 10 13 15 16 19 22 29 35
+ 51 55 64 71 79 87 95 101 111 116 120 140 174 56 58 62 66 69 73 77
+ 81 85 90 93 98 103 106 109 114 175 59 63 67 70 74 78 82 86 91 94
+ 99 104 107 110 115
+ XNOR_SRAM_16X16_ADC_MUXWBuffer $T=-92000 29600 0 0 $X=-234980 $Y=-270
X57 VDD! VSS! 4 5 7 8 11 12 14 20 23 27 233 CEN_1 61 185 186 183 83 88
+ 96 100 187 184 182 76 179 178 180 181
+ Inst_rom_g1 $T=-187200 154500 0 0 $X=-187202 $Y=154498
X58 83 88 117 100 46 21 177 28 113 32 176 50 42 43 24 34 44 39 121 33
+ 36 25 49 119 31 118 52 135 47 53 48 45 206 40 30 179 178 181 180 96
+ VSS!
+ we_reorder $T=-142200 17800 0 0 $X=-142200 $Y=17800
X59 219 213 215 211 VDD! VSS! 27 23 20 12 5 4 196 clk 8 7 11 195 14 193
+ 202 scan_en 210 214 198 resetn 421 422 423 227 scan_in 230 217 scan_out 148 173 200 172 171 221
+ 189 208 226 223 224 228 225 229 230 230
+ pc $T=-8600 -1200 0 0 $X=-8600 $Y=-1200
X60 VDD! VSS! 420 94 107 418 417 416 168 415 414 162 149 325 324 334 139 136 38 17
+ 335 56 98 109 93 326 329 165 69 330 73 58 327 110 115 114 18 41 141 103
+ 106 104 138 331 166 323 222 161 158 191 137 164 413 37 66 147 333 194 62 99
+ 171 332 26 172 201 200 328 173 170 209 126 1 213 10 129 19 3 2 13 215
+ 197 16 124 81 419 92 123 9 218 6 54 64 29 207 60 157 22 89 220 101
+ 35 55 152 102 79 96 68 343 95 120 75 87 116 100 229 140 clk 174 76 225
+ 228 423 175 264 262 230 269 433 182 189 438 183 185 268 426 186 437 67 198 61
+ 441 429 224 355 113 275 239 241 266 223 356 208 244 278 439 440 245 249 270 210
+ 227 251 272 257 24 214 273 425 226 255 253 427 233 254 258 271 276 435 117 121
+ 28 32 263 33 436 260 259 308 25 313 21 31 265 119 432 312 280 431 354 434
+ 350 177 351 49 309 267 353 311 261 352 349 277 74 78 274 428 176 430 91 36
+ 310 46 82 86 442 190 192 146 144 143 148 134 90 128 145 142 199 205 131 132
+ 85 160 159 211 212 153 130 127 77 155 163 216 154 97 125 122 219 105 167 133
+ 15 169 111 156 203 150 338 339 57 51 346 221 71 151 72 65 342 88 347 80
+ 83 84 348 204 70 108 59 63 178 179 422 180 181 421 112 193 184 195 231 196
+ 232 234 235 236 187 237 238 202 240 242 243 246 248 424 252 217 256
+ controllerLING $T=-8600 50200 0 0 $X=-8600 $Y=50200
X61 VDD! VSS! 203 336 337 338 339 340 341 342 343 344 345 346 347 247 250 348 171 173
+ 172 212 218 233 204 286 287 207 197 288 289 216 201 290 285 199 205 357 358 209
+ 215 200 211 219 221 148
+ RA1SH16x512 $T=200 134100 0 0 $X=198 $Y=134098
X62 435 455 372 344 350 319 314 358 349 315 316 285 351 317 318 357 434 446 361 336
+ 438 448 364 337 437 450 307 340 425 453 369 341 353 373 376 345 354 377 380 247
+ 428 381 384 250 352 386 389 286 436 390 393 287 432 394 397 288 265 398 401 289
+ 431 402 405 290 356 355 VSS! VDD!
+ MUX_D $T=125200 7200 0 0 $X=124680 $Y=6770
X63 478 430 VDD! VSS! MUX_Shifter_SEL_BUF $T=125200 39600 0 270 $X=124950 $Y=37820
X64 477 441 VDD! VSS! MUX_Shifter_SEL_BUF $T=131200 39600 1 270 $X=127710 $Y=37820
X65 477 452 227 478 148 VSS! VDD! VDD! MUX_Shifter_SEL_1bit $T=125200 39600 1 90 $X=124960 $Y=39170
X66 477 454 226 478 171 VSS! VSS! VDD! MUX_Shifter_SEL_1bit $T=125200 44400 1 90 $X=124960 $Y=43970
X67 477 451 224 478 173 VSS! VSS! VDD! MUX_Shifter_SEL_1bit $T=125200 49200 1 90 $X=124960 $Y=48770
X68 477 449 223 478 172 VSS! VSS! VDD! MUX_Shifter_SEL_1bit $T=125200 54000 1 90 $X=124960 $Y=53570
X69 291 223 VSS! VDD! Sign_Ext_left $T=131200 38000 0 0 $X=130960 $Y=37820
X70 292 224 VSS! VDD! Sign_Ext_left $T=131200 39600 0 0 $X=130960 $Y=39420
X71 293 226 VSS! VDD! Sign_Ext_left $T=131200 41200 0 0 $X=130960 $Y=41020
X72 294 227 VSS! VDD! Sign_Ext_left $T=131200 42800 0 0 $X=130960 $Y=42620
X73 295 228 VSS! VDD! Sign_Ext_left $T=131200 44400 0 0 $X=130960 $Y=44220
X74 296 225 VSS! VDD! Sign_Ext_left $T=131200 46000 0 0 $X=130960 $Y=45820
X75 297 229 VSS! VDD! Sign_Ext_left $T=131200 47600 0 0 $X=130960 $Y=47420
X76 298 230 VSS! VDD! Sign_Ext_left $T=131200 49200 0 0 $X=130960 $Y=49020
X77 299 479 480 VSS! VDD! Sign_Ext_right $T=131200 50800 0 0 $X=130960 $Y=50500
X78 300 479 480 VSS! VDD! Sign_Ext_right $T=131200 54800 0 0 $X=130960 $Y=54500
X79 301 479 480 VSS! VDD! Sign_Ext_right $T=131200 58800 0 0 $X=130960 $Y=58500
X80 302 479 480 VSS! VDD! Sign_Ext_right $T=131200 62800 0 0 $X=130960 $Y=62500
X81 303 479 480 VSS! VDD! Sign_Ext_right $T=131200 66800 0 0 $X=130960 $Y=66500
X82 304 479 480 VSS! VDD! Sign_Ext_right $T=131200 70800 0 0 $X=130960 $Y=70500
X83 305 479 480 VSS! VDD! Sign_Ext_right $T=131200 74800 0 0 $X=130960 $Y=74500
X84 306 479 480 VSS! VDD! Sign_Ext_right $T=131200 78800 0 0 $X=130960 $Y=78500
X85 426 440 429 427 439 433 360 363 366 368 371 375 379 383 388 392 278 396 400 404
+ 407 409 412 359 362 365 367 370 374 378 382 387 391 395 399 403 406 408 411 446
+ 448 450 453 455 373 377 381 386 390 394 398 402 315 317 319 VDD! VSS!
+ ALU $T=134400 6400 0 0 $X=134000 $Y=6100
X86 475 461 462 463 464 465 466 467 468 469 470 471 472 473 474 452 454 451 449 476
+ 361 364 307 369 372 376 380 384 389 393 397 401 405 316 318 314 VDD! VSS!
+ SHIFTER $T=134400 40800 0 0 $X=134160 $Y=40600
X87 233 481 239 251 274 235 270 243 276 237 273 248 244 231 271 246 272 234 255 252
+ 245 232 249 242 258 236 253 256 257 238 424 241 254 240 clk 268 262 280 269 263
+ 266 313 260 308 312 311 309 261 310 277 267 172 203 173 338 171 339 148 342 221
+ 343 219 346 211 347 200 348 215 204 213 207 190 197 191 216 220 201 194 199 222
+ 205 192 209 VDD! VSS! 30 52 118 53 45 44 47 48 39 40 50 135 206 43 34
+ 42
+ RF $T=134400 54800 0 0 $X=134160 $Y=54800
X88 443 444 445 447 359 362 360 291 172 363 292 173 203 456 458 338 VSS! VDD! ICV_2 $T=139600 33600 0 0 $X=139360 $Y=33440
X89 443 444 445 447 365 367 366 293 171 368 294 148 339 456 458 342 VSS! VDD! ICV_2 $T=151600 33600 0 0 $X=151360 $Y=33440
X90 443 444 445 447 370 374 371 295 221 375 296 219 343 456 458 346 VSS! VDD! ICV_2 $T=163600 33600 0 0 $X=163360 $Y=33440
X91 443 444 445 447 378 382 379 297 211 383 298 200 347 456 458 348 VSS! VDD! ICV_2 $T=175600 33600 0 0 $X=175360 $Y=33440
X92 443 444 445 447 387 391 388 299 215 392 300 213 204 456 458 207 VSS! VDD! ICV_2 $T=187600 33600 0 0 $X=187360 $Y=33440
X93 443 444 445 447 395 399 396 301 190 400 302 191 197 456 458 216 VSS! VDD! ICV_2 $T=199600 33600 0 0 $X=199360 $Y=33440
X94 443 444 445 447 403 406 404 303 220 407 304 194 201 456 458 199 VSS! VDD! ICV_2 $T=211600 33600 0 0 $X=211360 $Y=33440
X95 443 444 445 447 408 411 409 305 222 412 306 192 205 456 458 209 VSS! VDD! ICV_2 $T=223600 33600 0 0 $X=223360 $Y=33440
X96 461 475 338 457 292 459 203 291 VSS! VDD! ICV_1 $T=145600 52800 1 180 $X=139360 $Y=52500
X97 463 462 342 457 294 459 339 293 VSS! VDD! ICV_1 $T=157600 52800 1 180 $X=151360 $Y=52500
X98 465 464 346 457 296 459 343 295 VSS! VDD! ICV_1 $T=169600 52800 1 180 $X=163360 $Y=52500
X99 467 466 348 457 298 459 347 297 VSS! VDD! ICV_1 $T=181600 52800 1 180 $X=175360 $Y=52500
X100 469 468 207 457 300 459 204 299 VSS! VDD! ICV_1 $T=193600 52800 1 180 $X=187360 $Y=52500
X101 471 470 216 457 302 459 197 301 VSS! VDD! ICV_1 $T=205600 52800 1 180 $X=199360 $Y=52500
X102 473 472 199 457 304 459 201 303 VSS! VDD! ICV_1 $T=217600 52800 1 180 $X=211360 $Y=52500
X103 476 474 209 457 306 459 205 305 VSS! VDD! ICV_1 $T=229600 52800 1 180 $X=223360 $Y=52500
.ENDS
***************************************
