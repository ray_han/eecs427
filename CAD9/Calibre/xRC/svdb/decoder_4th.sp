* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT decoder_4th E SH3_BAR SH<3> IN IN1 VSS! VDD!
** N=139 EP=7 IP=0 FDC=29
M0 5 SH3_BAR IN VSS! nfet L=1.2e-07 W=3.4e-07 AD=2.448e-13 AS=1.088e-13 PD=2.12e-06 PS=1.32e-06 NRD=2.11765 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=7.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=480 $Y=450 $D=25
M1 5 SH<3> IN1 VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.156e-13 AS=2.38e-13 PD=1.36e-06 PS=2.08e-06 NRD=1 NRS=2.05882 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7e-07 sb=3.4e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=880 $Y=4810 $D=25
M2 6 5 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 AD=1.184e-13 AS=1.184e-13 PD=1.38e-06 PS=1.38e-06 NRD=0.864865 NRS=0.864865 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=1880 $Y=450 $D=25
M3 6 5 VSS! VSS! nfet L=1.2e-07 W=3.7e-07 AD=1.184e-13 AS=1.184e-13 PD=1.38e-06 PS=1.38e-06 NRD=0.864865 NRS=0.864865 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=1880 $Y=4780 $D=25
M4 1 6 VSS! VSS! nfet L=1.2e-07 W=5e-07 AD=9e-14 AS=1.6e-13 PD=8.6e-07 PS=1.64e-06 NRD=0.36 NRS=0.64 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.8e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=1.2e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=2820 $Y=450 $D=25
M5 E 1 VSS! VSS! nfet L=1.2e-07 W=6e-07 AD=1.08e-13 AS=1.92e-13 PD=9.6e-07 PS=1.84e-06 NRD=0.3 NRS=0.533333 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=1.2e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=2820 $Y=4550 $D=25
M6 VSS! 6 1 VSS! nfet L=1.2e-07 W=5e-07 AD=1.05455e-13 AS=9e-14 PD=9.09091e-07 PS=8.6e-07 NRD=0.421818 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.32e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=1.2e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=3300 $Y=450 $D=25
M7 VSS! 1 E VSS! nfet L=1.2e-07 W=6e-07 AD=1.08e-13 AS=1.08e-13 PD=9.6e-07 PS=9.6e-07 NRD=0.3 NRS=0.3 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=1.2e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=3300 $Y=4550 $D=25
M8 E 1 VSS! VSS! nfet L=1.2e-07 W=6e-07 AD=1.08e-13 AS=1.08e-13 PD=9.6e-07 PS=9.6e-07 NRD=0.3 NRS=0.3 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=1.2e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=3780 $Y=4550 $D=25
M9 E 1 VSS! VSS! nfet L=1.2e-07 W=6e-07 AD=1.08e-13 AS=1.26545e-13 PD=9.6e-07 PS=1.09091e-06 NRD=0.3 NRS=0.351515 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.15333e-06 sb=8e-07 sd=0 panw1=0 panw2=1.2e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=3820 $Y=450 $D=25
M10 VSS! 1 E VSS! nfet L=1.2e-07 W=6e-07 AD=1.08e-13 AS=1.08e-13 PD=9.6e-07 PS=9.6e-07 NRD=0.3 NRS=0.3 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=1.2e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=4260 $Y=4550 $D=25
M11 VSS! 1 E VSS! nfet L=1.2e-07 W=6e-07 AD=1.92e-13 AS=1.08e-13 PD=1.84e-06 PS=9.6e-07 NRD=0.533333 NRS=0.3 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.63333e-06 sb=3.2e-07 sd=0 panw1=0 panw2=1.2e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=4300 $Y=450 $D=25
M12 E 1 VSS! VSS! nfet L=1.2e-07 W=6e-07 AD=1.92e-13 AS=1.08e-13 PD=1.84e-06 PS=9.6e-07 NRD=0.533333 NRS=0.3 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=1.2e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.68e-14 panw9=0 panw10=0 $X=4740 $Y=4550 $D=25
M13 5 SH3_BAR IN1 VDD! pfet L=1.2e-07 W=3.4e-07 AD=2.448e-13 AS=1.088e-13 PD=2.12e-06 PS=1.32e-06 NRD=2.11765 NRS=0.941176 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=7.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=4.8e-15 panw7=3.08e-14 panw8=4.6e-14 panw9=0 panw10=9.6e-15 $X=480 $Y=3310 $D=108
M14 5 SH<3> IN VDD! pfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=2.448e-13 PD=1.32e-06 PS=2.12e-06 NRD=0.941176 NRS=2.11765 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=4.8e-15 panw7=2.4e-14 panw8=1.2e-14 panw9=4.08e-14 panw10=9.6e-15 $X=880 $Y=1950 $D=108
M15 6 5 VDD! VDD! pfet L=1.2e-07 W=3.7e-07 AD=1.184e-13 AS=1.184e-13 PD=1.38e-06 PS=1.38e-06 NRD=0.864865 NRS=0.864865 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=1.2e-14 panw9=0 panw10=9.6e-15 $X=1880 $Y=1920 $D=108
M16 6 5 VDD! VDD! pfet L=1.2e-07 W=3.7e-07 AD=1.184e-13 AS=1.184e-13 PD=1.38e-06 PS=1.38e-06 NRD=0.864865 NRS=0.864865 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=8.4e-15 panw7=2.4e-14 panw8=1.2e-14 panw9=0 panw10=9.6e-15 $X=1880 $Y=3310 $D=108
M17 1 6 VDD! VDD! pfet L=1.2e-07 W=5e-07 AD=9e-14 AS=1.6e-13 PD=8.6e-07 PS=1.64e-06 NRD=0.36 NRS=0.64 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.8e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-14 panw9=0 panw10=9.6e-15 $X=2820 $Y=1790 $D=108
M18 E 1 VDD! VDD! pfet L=1.2e-07 W=6e-07 AD=1.08e-13 AS=1.92e-13 PD=9.6e-07 PS=1.84e-06 NRD=0.3 NRS=0.533333 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-14 panw9=0 panw10=9.6e-15 $X=2820 $Y=3310 $D=108
M19 VDD! 6 1 VDD! pfet L=1.2e-07 W=5e-07 AD=1.05455e-13 AS=9e-14 PD=9.09091e-07 PS=8.6e-07 NRD=0.421818 NRS=0.36 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.32e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-14 panw9=0 panw10=9.6e-15 $X=3300 $Y=1790 $D=108
M20 VDD! 1 E VDD! pfet L=1.2e-07 W=6e-07 AD=1.08e-13 AS=1.08e-13 PD=9.6e-07 PS=9.6e-07 NRD=0.3 NRS=0.3 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-14 panw9=0 panw10=9.6e-15 $X=3300 $Y=3310 $D=108
M21 E 1 VDD! VDD! pfet L=1.2e-07 W=6e-07 AD=1.08e-13 AS=1.08e-13 PD=9.6e-07 PS=9.6e-07 NRD=0.3 NRS=0.3 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-14 panw9=0 panw10=8.16e-14 $X=3780 $Y=3310 $D=108
M22 E 1 VDD! VDD! pfet L=1.2e-07 W=6e-07 AD=1.08e-13 AS=1.26545e-13 PD=9.6e-07 PS=1.09091e-06 NRD=0.3 NRS=0.351515 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.15333e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-14 panw9=0 panw10=8.16e-14 $X=3820 $Y=1690 $D=108
M23 VDD! 1 E VDD! pfet L=1.2e-07 W=6e-07 AD=1.08e-13 AS=1.08e-13 PD=9.6e-07 PS=9.6e-07 NRD=0.3 NRS=0.3 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-14 panw9=7.2e-14 panw10=9.6e-15 $X=4260 $Y=3310 $D=108
M24 VDD! 1 E VDD! pfet L=1.2e-07 W=6e-07 AD=1.92e-13 AS=1.08e-13 PD=1.84e-06 PS=9.6e-07 NRD=0.533333 NRS=0.3 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.63333e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-14 panw9=7.2e-14 panw10=9.6e-15 $X=4300 $Y=1690 $D=108
M25 E 1 VDD! VDD! pfet L=1.2e-07 W=6e-07 AD=1.92e-13 AS=1.08e-13 PD=1.84e-06 PS=9.6e-07 NRD=0.533333 NRS=0.3 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=9.6e-14 panw8=1.2e-14 panw9=0 panw10=9.6e-15 $X=4740 $Y=3310 $D=108
D26 VSS! VDD! diodenwx AREA=1.4664e-11 perim=1.04e-05 t3well=0 $X=0 $Y=1390 $D=474
D27 11 VDD! diodenwx AREA=8.46e-13 perim=3.42e-06 t3well=0 $X=-300 $Y=1390 $D=472
D28 11 VDD! diodenwx AREA=8.46e-13 perim=3.42e-06 t3well=0 $X=5200 $Y=1390 $D=472
.ENDS
***************************************
