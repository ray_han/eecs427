* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT RWL_Driver_1bit RWLBN RWLP RWLBP RWLN RWL_N RWLB_P RWLB_N RWL_P VSS! VDD!
** N=208 EP=10 IP=0 FDC=51
M0 5 RWL_N VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=1.344e-13 PD=7.8e-07 PS=1.48e-06 NRD=0.428571 NRS=0.761905 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=560 $Y=450 $D=25
M1 RWLN 5 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=1.344e-13 PD=7.8e-07 PS=1.48e-06 NRD=0.428571 NRS=0.761905 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=560 $Y=4730 $D=25
M2 VSS! RWL_N 5 VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=1040 $Y=450 $D=25
M3 VSS! 5 RWLN VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=1040 $Y=4730 $D=25
M4 RWLBN 12 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=1520 $Y=450 $D=25
M5 RWLN 5 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=1520 $Y=4730 $D=25
M6 VSS! 12 RWLBN VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=2000 $Y=450 $D=25
M7 VSS! 5 RWLN VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=2000 $Y=4730 $D=25
M8 RWLBN 12 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=2480 $Y=450 $D=25
M9 12 RWLB_N VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=2480 $Y=4730 $D=25
M10 VSS! 12 RWLBN VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=2960 $Y=450 $D=25
M11 VSS! RWLB_N 12 VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=2960 $Y=4730 $D=25
M12 9 RWLB_P VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=3440 $Y=450 $D=25
M13 RWLBP 9 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=3440 $Y=4730 $D=25
M14 VSS! RWLB_P 9 VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=3920 $Y=450 $D=25
M15 VSS! 9 RWLBP VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=3920 $Y=4730 $D=25
M16 RWLP 10 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.76e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=4400 $Y=450 $D=25
M17 RWLBP 9 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.76e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=4400 $Y=4730 $D=25
M18 VSS! 10 RWLP VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.28e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=4880 $Y=450 $D=25
M19 VSS! 9 RWLBP VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.28e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=4880 $Y=4730 $D=25
M20 RWLP 10 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=8e-07 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=5360 $Y=450 $D=25
M21 10 RWL_P VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=7.56e-14 PD=7.8e-07 PS=7.8e-07 NRD=0.428571 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=8e-07 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=5360 $Y=4730 $D=25
M22 VSS! 10 RWLP VSS! nfet L=1.2e-07 W=4.2e-07 AD=1.344e-13 AS=7.56e-14 PD=1.48e-06 PS=7.8e-07 NRD=0.761905 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=5840 $Y=450 $D=25
M23 VSS! RWL_P 10 VSS! nfet L=1.2e-07 W=4.2e-07 AD=1.344e-13 AS=7.56e-14 PD=1.48e-06 PS=7.8e-07 NRD=0.761905 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=5840 $Y=4730 $D=25
M24 5 RWL_N VDD! VDD! pfet L=1.2e-07 W=8.1e-07 AD=1.458e-13 AS=2.592e-13 PD=1.17e-06 PS=2.26e-06 NRD=0.222222 NRS=0.395062 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.9837e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.212e-13 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=560 $Y=1480 $D=108
M25 RWLN 5 VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.2e-13 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=560 $Y=3310 $D=108
M26 VDD! RWL_N 5 VDD! pfet L=1.2e-07 W=8.1e-07 AD=1.45901e-13 AS=1.458e-13 PD=1.17727e-06 PS=1.17e-06 NRD=0.222376 NRS=0.222222 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.97778e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.104e-13 panw10=0 $X=1040 $Y=1480 $D=108
M27 VDD! 5 RWLN VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.092e-13 panw10=0 $X=1040 $Y=3310 $D=108
M28 RWLBN 12 VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44099e-13 PD=1.16e-06 PS=1.16273e-06 NRD=0.225 NRS=0.225155 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=9.6e-14 $X=1520 $Y=1490 $D=108
M29 RWLN 5 VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=9.6e-14 $X=1520 $Y=3310 $D=108
M30 VDD! 12 RWLBN VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=2000 $Y=1490 $D=108
M31 VDD! 5 RWLN VDD! pfet L=1.2e-07 W=8e-07 AD=1.44099e-13 AS=1.44e-13 PD=1.16273e-06 PS=1.16e-06 NRD=0.225155 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=2000 $Y=3310 $D=108
M32 RWLBN 12 VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=2480 $Y=1490 $D=108
M33 12 RWLB_N VDD! VDD! pfet L=1.2e-07 W=8.1e-07 AD=1.458e-13 AS=1.45901e-13 PD=1.17e-06 PS=1.17727e-06 NRD=0.222222 NRS=0.222376 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.97778e-06 sb=1.9837e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=2480 $Y=3310 $D=108
M34 VDD! 12 RWLBN VDD! pfet L=1.2e-07 W=8e-07 AD=1.44099e-13 AS=1.44e-13 PD=1.16273e-06 PS=1.16e-06 NRD=0.225155 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=2960 $Y=1490 $D=108
M35 VDD! RWLB_N 12 VDD! pfet L=1.2e-07 W=8.1e-07 AD=1.45901e-13 AS=1.458e-13 PD=1.17727e-06 PS=1.17e-06 NRD=0.222376 NRS=0.222222 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.9837e-06 sb=1.97778e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=2960 $Y=3310 $D=108
M36 9 RWLB_P VDD! VDD! pfet L=1.2e-07 W=8.1e-07 AD=1.458e-13 AS=1.45901e-13 PD=1.17e-06 PS=1.17727e-06 NRD=0.222222 NRS=0.222376 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.97778e-06 sb=1.9837e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=3440 $Y=1480 $D=108
M37 RWLBP 9 VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44099e-13 PD=1.16e-06 PS=1.16273e-06 NRD=0.225 NRS=0.225155 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=3440 $Y=3310 $D=108
M38 VDD! RWLB_P 9 VDD! pfet L=1.2e-07 W=8.1e-07 AD=1.45901e-13 AS=1.458e-13 PD=1.17727e-06 PS=1.17e-06 NRD=0.222376 NRS=0.222222 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.9837e-06 sb=1.97778e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=3920 $Y=1480 $D=108
M39 VDD! 9 RWLBP VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=3920 $Y=3310 $D=108
M40 RWLP 10 VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44099e-13 PD=1.16e-06 PS=1.16273e-06 NRD=0.225 NRS=0.225155 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.76e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=4400 $Y=1490 $D=108
M41 RWLBP 9 VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.76e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=4400 $Y=3310 $D=108
M42 VDD! 10 RWLP VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.28e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=9.6e-14 $X=4880 $Y=1490 $D=108
M43 VDD! 9 RWLBP VDD! pfet L=1.2e-07 W=8e-07 AD=1.44099e-13 AS=1.44e-13 PD=1.16273e-06 PS=1.16e-06 NRD=0.225155 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.28e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=9.6e-14 $X=4880 $Y=3310 $D=108
M44 RWLP 10 VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=8e-07 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.092e-13 panw10=0 $X=5360 $Y=1490 $D=108
M45 10 RWL_P VDD! VDD! pfet L=1.2e-07 W=8.1e-07 AD=1.458e-13 AS=1.45901e-13 PD=1.17e-06 PS=1.17727e-06 NRD=0.222222 NRS=0.222376 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.97778e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.104e-13 panw10=0 $X=5360 $Y=3310 $D=108
M46 VDD! 10 RWLP VDD! pfet L=1.2e-07 W=8e-07 AD=2.56e-13 AS=1.44e-13 PD=2.24e-06 PS=1.16e-06 NRD=0.4 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.2e-13 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=5840 $Y=1490 $D=108
M47 VDD! RWL_P 10 VDD! pfet L=1.2e-07 W=8.1e-07 AD=2.592e-13 AS=1.458e-13 PD=2.26e-06 PS=1.17e-06 NRD=0.395062 NRS=0.222222 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.9837e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.212e-13 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=5840 $Y=3310 $D=108
D48 VSS! VDD! diodenwx AREA=2.0736e-11 perim=1.28e-05 t3well=0 $X=0 $Y=1180 $D=474
D49 15 VDD! diodenwx AREA=1.944e-13 perim=3.36e-06 t3well=0 $X=-60 $Y=1180 $D=472
D50 15 VDD! diodenwx AREA=5.832e-13 perim=3.6e-06 t3well=0 $X=6400 $Y=1180 $D=472
.ENDS
***************************************
