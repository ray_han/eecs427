* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT select_buffer SH0 SH0_BAR VSS! VDD!
** N=25 EP=4 IP=0 FDC=7
M0 SH0_BAR SH0 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 AD=6.3e-14 AS=1.12e-13 PD=7.1e-07 PS=1.34e-06 NRD=0.514286 NRS=0.914286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8.7e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.4e-14 panw8=7.2e-15 panw9=0 panw10=0 $X=410 $Y=450 $D=25
M1 VSS! SH0 SH0_BAR VSS! nfet L=1.2e-07 W=3.5e-07 AD=1.365e-13 AS=6.3e-14 PD=1.48e-06 PS=7.1e-07 NRD=1.11429 NRS=0.514286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.9e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.4e-14 panw8=7.2e-15 panw9=0 panw10=0 $X=890 $Y=450 $D=25
M2 SH0_BAR SH0 VDD! VDD! pfet L=1.2e-07 W=6.4e-07 AD=1.152e-13 AS=2.048e-13 PD=1e-06 PS=1.92e-06 NRD=0.28125 NRS=0.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8.7e-07 sd=0 panw1=0 panw2=1.2e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=8.76e-14 panw8=6.48e-14 panw9=6.6e-14 panw10=5.76e-14 $X=410 $Y=1650 $D=108
M3 VDD! SH0 SH0_BAR VDD! pfet L=1.2e-07 W=6.4e-07 AD=2.496e-13 AS=1.152e-13 PD=2.06e-06 PS=1e-06 NRD=0.609375 NRS=0.28125 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.9e-07 sd=0 panw1=0 panw2=1.2e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=3e-14 panw8=1.16e-13 panw9=1.3e-13 panw10=0 $X=890 $Y=1650 $D=108
D4 VSS! VDD! diodenwx AREA=2.768e-12 perim=3.2e-06 t3well=0 $X=0 $Y=1310 $D=474
D5 5 VDD! diodenwx AREA=5.19e-13 perim=2.33e-06 t3well=0 $X=-300 $Y=1310 $D=472
D6 5 VDD! diodenwx AREA=5.19e-13 perim=2.33e-06 t3well=0 $X=1600 $Y=1310 $D=472
.ENDS
***************************************
