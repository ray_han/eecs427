* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT WBL_Driver_1bit IN WBLB WBL VSS! VDD!
** N=91 EP=5 IP=0 FDC=21
M0 VSS! 2 1 VSS! nfet L=1.2e-07 W=3.1e-07 AD=5.60862e-14 AS=9.92e-14 PD=6.67692e-07 PS=1.26e-06 NRD=0.583623 NRS=1.03226 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=4.8e-15 panw8=0 panw9=0 panw10=0 $X=750 $Y=450 $D=25
M1 VSS! 2 1 VSS! nfet L=1.2e-07 W=3.1e-07 AD=5.61153e-14 AS=9.92e-14 PD=7.04068e-07 PS=1.26e-06 NRD=0.583926 NRS=1.03226 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=7.41935e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.2e-14 panw8=0 panw9=0 panw10=0 $X=770 $Y=4840 $D=25
M2 WBLB 1 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 AD=6.12e-14 AS=6.15138e-14 PD=7e-07 PS=7.32308e-07 NRD=0.529412 NRS=0.532127 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7.47059e-07 sb=1.76e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=4.8e-15 panw8=0 panw9=0 panw10=0 $X=1230 $Y=450 $D=25
M3 2 IN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.06847e-14 PD=1.2e-06 PS=6.35932e-07 NRD=1.14286 NRS=0.646489 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=3.6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.2e-14 panw8=0 panw9=0 panw10=0 $X=1250 $Y=4870 $D=25
M4 VSS! 1 WBLB VSS! nfet L=1.2e-07 W=3.4e-07 AD=6.12e-14 AS=6.12e-14 PD=7e-07 PS=7e-07 NRD=0.529412 NRS=0.529412 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.22706e-06 sb=1.28e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=4.8e-15 panw8=0 panw9=0 panw10=0 $X=1710 $Y=450 $D=25
M5 WBLB 1 VSS! VSS! nfet L=1.2e-07 W=3.4e-07 AD=6.12e-14 AS=6.12e-14 PD=7e-07 PS=7e-07 NRD=0.529412 NRS=0.529412 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.70706e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=4.8e-15 panw8=0 panw9=0 panw10=0 $X=2190 $Y=450 $D=25
M6 VSS! IN 6 VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.13882e-14 AS=8.96e-14 PD=6.25882e-07 PS=1.2e-06 NRD=0.655462 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=3.6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.2e-14 panw8=0 panw9=0 panw10=0 $X=2190 $Y=4870 $D=25
M7 VSS! 1 WBLB VSS! nfet L=1.2e-07 W=3.4e-07 AD=1.088e-13 AS=6.12e-14 PD=1.32e-06 PS=7e-07 NRD=0.941176 NRS=0.529412 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.96824e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=4.8e-15 panw8=0 panw9=0 panw10=0 $X=2670 $Y=450 $D=25
M8 WBL 6 VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=7.34118e-14 PD=1.44e-06 PS=8.94118e-07 NRD=0.8 NRS=0.458824 m=1 par=1 nf=1 ngcon=1 psp=0 sa=6.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.2e-14 panw8=0 panw9=0 panw10=0 $X=2670 $Y=4750 $D=25
M9 VDD! 2 1 VDD! pfet L=1.2e-07 W=7.3e-07 AD=1.31976e-13 AS=2.336e-13 PD=1.10461e-06 PS=2.1e-06 NRD=0.247657 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=3.6e-15 panw6=1.2e-14 panw7=6.05e-14 panw8=7.51e-14 panw9=2.4e-14 panw10=0 $X=750 $Y=1560 $D=108
M10 VDD! 2 1 VDD! pfet L=1.2e-07 W=7.3e-07 AD=1.32135e-13 AS=2.336e-13 PD=1.14489e-06 PS=2.1e-06 NRD=0.247955 NRS=0.438356 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=7.42466e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=4.8e-15 panw5=6e-15 panw6=1.2e-14 panw7=4.59e-14 panw8=8.97e-14 panw9=1.68e-14 panw10=0 $X=770 $Y=3310 $D=108
M11 WBLB 1 VDD! VDD! pfet L=1.2e-07 W=7.9e-07 AD=1.422e-13 AS=1.42824e-13 PD=1.15e-06 PS=1.19539e-06 NRD=0.227848 NRS=0.228847 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7.5443e-07 sb=1.76e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=4.8e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.188e-13 panw10=0 $X=1230 $Y=1500 $D=108
M12 2 IN VDD! VDD! pfet L=1.2e-07 W=6.6e-07 AD=2.112e-13 AS=1.19465e-13 PD=1.96e-06 PS=1.03511e-06 NRD=0.484848 NRS=0.274253 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=2.4e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=9.6e-14 panw10=0 $X=1250 $Y=3310 $D=108
M13 VDD! 1 WBLB VDD! pfet L=1.2e-07 W=7.9e-07 AD=1.422e-13 AS=1.422e-13 PD=1.15e-06 PS=1.15e-06 NRD=0.227848 NRS=0.227848 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.23443e-06 sb=1.28e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=4.8e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=2.4e-14 panw10=1.896e-13 $X=1710 $Y=1500 $D=108
M14 WBLB 1 VDD! VDD! pfet L=1.2e-07 W=7.9e-07 AD=1.422e-13 AS=1.422e-13 PD=1.15e-06 PS=1.15e-06 NRD=0.227848 NRS=0.227848 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.71443e-06 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=4.8e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.188e-13 panw10=0 $X=2190 $Y=1500 $D=108
M15 VDD! IN 6 VDD! pfet L=1.2e-07 W=5.7e-07 AD=1.04783e-13 AS=1.824e-13 PD=9.70213e-07 PS=1.78e-06 NRD=0.322508 NRS=0.561404 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=8.52e-14 panw10=0 $X=2190 $Y=3310 $D=108
M16 VDD! 1 WBLB VDD! pfet L=1.2e-07 W=7.9e-07 AD=2.528e-13 AS=1.422e-13 PD=2.22e-06 PS=1.15e-06 NRD=0.405063 NRS=0.227848 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.97266e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=4.8e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.188e-13 panw8=2.4e-14 panw9=2.4e-14 panw10=0 $X=2670 $Y=1500 $D=108
M17 WBL 6 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 AD=2.688e-13 AS=1.54417e-13 PD=2.32e-06 PS=1.42979e-06 NRD=0.380952 NRS=0.218845 m=1 par=1 nf=1 ngcon=1 psp=0 sa=6.07143e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.248e-13 panw8=2.4e-14 panw9=1.68e-14 panw10=0 $X=2670 $Y=3310 $D=108
D18 VSS! VSS! diodenx AREA=1.12e-13 perim=1.36e-06 $X=0 $Y=5460 $D=480
D19 VSS! VDD! diodenwx AREA=1.0752e-11 perim=9.76e-06 t3well=0 $X=0 $Y=1090 $D=474
D20 9 VDD! diodenwx AREA=7.056e-13 perim=3.78e-06 t3well=0 $X=3200 $Y=1090 $D=472
.ENDS
***************************************
