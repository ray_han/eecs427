* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT ADC3 CLK Vin Vref OUT VSS! VDD!
** N=109 EP=6 IP=0 FDC=14
M0 VSS! 6 OUT VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=1.008e-13 PD=1.28e-06 PS=1.28e-06 NRD=1.28571 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.76e-14 panw10=6e-15 $X=580 $Y=490 $D=25
M1 11 Vin 6 VSS! nfet L=1.2e-07 W=2.8e-07 AD=6.16e-14 AS=1.008e-13 PD=7.2e-07 PS=1.28e-06 NRD=0.785714 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=9.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.76e-14 panw10=6e-15 $X=1660 $Y=490 $D=25
M2 5 9 11 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=6.16e-14 PD=1.28e-06 PS=7.2e-07 NRD=1.28571 NRS=0.785714 m=1 par=1 nf=1 ngcon=1 psp=0 sa=9.2e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.76e-14 panw10=6e-15 $X=2220 $Y=490 $D=25
M3 5 CLK VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=1.008e-13 PD=1.28e-06 PS=1.28e-06 NRD=1.28571 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.8e-15 panw9=3.08e-14 panw10=0 $X=3260 $Y=830 $D=25
M4 12 6 5 VSS! nfet L=1.2e-07 W=2.8e-07 AD=6.16e-14 AS=1.008e-13 PD=7.2e-07 PS=1.28e-06 NRD=0.785714 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=9.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.76e-14 panw10=6e-15 $X=4460 $Y=490 $D=25
M5 9 Vref 12 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=6.16e-14 PD=1.28e-06 PS=7.2e-07 NRD=1.28571 NRS=0.785714 m=1 par=1 nf=1 ngcon=1 psp=0 sa=9.2e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.76e-14 panw10=6e-15 $X=5020 $Y=490 $D=25
M6 10 9 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=1.008e-13 PD=1.28e-06 PS=1.28e-06 NRD=1.28571 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.76e-14 panw10=6e-15 $X=6100 $Y=490 $D=25
M7 VDD! 6 OUT VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=1.008e-13 PD=1.28e-06 PS=1.28e-06 NRD=1.28571 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=3.6e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=5.52e-14 panw9=1.2e-14 panw10=0 $X=580 $Y=2240 $D=108
M8 6 CLK VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=6.16e-14 AS=1.008e-13 PD=7.2e-07 PS=1.28e-06 NRD=0.785714 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=9.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=2.16e-14 panw9=1.2e-14 panw10=1.68e-14 $X=1660 $Y=2240 $D=108
M9 VDD! 9 6 VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=6.16e-14 PD=1.28e-06 PS=7.2e-07 NRD=1.28571 NRS=0.785714 m=1 par=1 nf=1 ngcon=1 psp=0 sa=9.2e-07 sb=3.6e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=2.16e-14 panw9=1.2e-14 panw10=0 $X=2220 $Y=2240 $D=108
M10 9 6 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=6.16e-14 AS=1.008e-13 PD=7.2e-07 PS=1.28e-06 NRD=0.785714 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=9.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=2.16e-14 panw9=1.2e-14 panw10=0 $X=4460 $Y=2240 $D=108
M11 VDD! CLK 9 VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=6.16e-14 PD=1.28e-06 PS=7.2e-07 NRD=1.28571 NRS=0.785714 m=1 par=1 nf=1 ngcon=1 psp=0 sa=9.2e-07 sb=3.6e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=2.16e-14 panw9=1.2e-14 panw10=1.68e-14 $X=5020 $Y=2240 $D=108
M12 10 9 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=1.008e-13 PD=1.28e-06 PS=1.28e-06 NRD=1.28571 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=3.6e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=5.52e-14 panw9=1.2e-14 panw10=0 $X=6100 $Y=2240 $D=108
D13 VSS! VDD! diodenwx AREA=1.0304e-11 perim=1.752e-05 t3well=0 $X=-280 $Y=1940 $D=474
.ENDS
***************************************
