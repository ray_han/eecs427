* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT select_buffer4 SH<3> SH3 VSS! SH3_BAR VDD!
** N=133 EP=5 IP=0 FDC=37
M0 SH3_BAR SH<3> VSS! VSS! nfet L=1.2e-07 W=4.4e-07 AD=7.92e-14 AS=1.408e-13 PD=8e-07 PS=1.52e-06 NRD=0.409091 NRS=0.727273 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.92e-14 panw8=0 panw9=0 panw10=0 $X=410 $Y=450 $D=25
M1 3 SH3 VSS! VSS! nfet L=1.2e-07 W=4.2e-07 AD=7.56e-14 AS=1.344e-13 PD=7.8e-07 PS=1.48e-06 NRD=0.428571 NRS=0.761905 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=410 $Y=4730 $D=25
M2 VSS! SH<3> SH3_BAR VSS! nfet L=1.2e-07 W=4.4e-07 AD=8.14e-14 AS=7.92e-14 PD=8.1e-07 PS=8e-07 NRD=0.420455 NRS=0.409091 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=2e-06 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.92e-14 panw8=0 panw9=0 panw10=0 $X=890 $Y=450 $D=25
M3 VSS! SH3 3 VSS! nfet L=1.2e-07 W=4.2e-07 AD=9.49694e-14 AS=7.56e-14 PD=8.69647e-07 PS=7.8e-07 NRD=0.538375 NRS=0.428571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=2e-06 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=890 $Y=4730 $D=25
M4 SH3_BAR SH<3> VSS! VSS! nfet L=1.2e-07 W=4.4e-07 AD=7.92e-14 AS=8.14e-14 PD=8e-07 PS=8.1e-07 NRD=0.409091 NRS=0.420455 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.29e-06 sb=2e-06 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.92e-14 panw8=0 panw9=0 panw10=0 $X=1380 $Y=450 $D=25
M5 SH<3> 3 VSS! VSS! nfet L=1.2e-07 W=4.3e-07 AD=7.74e-14 AS=9.72306e-14 PD=7.9e-07 PS=8.90353e-07 NRD=0.418605 NRS=0.525855 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.34558e-06 sb=2e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=1460 $Y=4720 $D=25
M6 VSS! SH<3> SH3_BAR VSS! nfet L=1.2e-07 W=4.4e-07 AD=7.92e-14 AS=7.92e-14 PD=8e-07 PS=8e-07 NRD=0.409091 NRS=0.409091 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.77e-06 sb=2e-06 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.92e-14 panw8=0 panw9=0 panw10=0 $X=1860 $Y=450 $D=25
M7 VSS! 3 SH<3> VSS! nfet L=1.2e-07 W=4.3e-07 AD=7.955e-14 AS=7.74e-14 PD=8e-07 PS=7.9e-07 NRD=0.430233 NRS=0.418605 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.82558e-06 sb=1.77e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=1940 $Y=4720 $D=25
M8 SH3_BAR SH<3> VSS! VSS! nfet L=1.2e-07 W=4.4e-07 AD=7.92e-14 AS=7.92e-14 PD=8e-07 PS=8e-07 NRD=0.409091 NRS=0.409091 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.92e-14 panw8=0 panw9=0 panw10=0 $X=2340 $Y=450 $D=25
M9 SH<3> 3 VSS! VSS! nfet L=1.2e-07 W=4.3e-07 AD=7.74e-14 AS=7.955e-14 PD=7.9e-07 PS=8e-07 NRD=0.418605 NRS=0.430233 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.98349e-06 sb=1.28e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=2430 $Y=4720 $D=25
M10 VSS! SH<3> SH3_BAR VSS! nfet L=1.2e-07 W=4.4e-07 AD=7.92e-14 AS=7.92e-14 PD=8e-07 PS=8e-07 NRD=0.409091 NRS=0.409091 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.92e-14 panw8=0 panw9=0 panw10=0 $X=2820 $Y=450 $D=25
M11 VSS! 3 SH<3> VSS! nfet L=1.2e-07 W=4.3e-07 AD=7.74e-14 AS=7.74e-14 PD=7.9e-07 PS=7.9e-07 NRD=0.418605 NRS=0.418605 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.99465e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=2910 $Y=4720 $D=25
M12 SH3_BAR SH<3> VSS! VSS! nfet L=1.2e-07 W=4.4e-07 AD=8.14e-14 AS=7.92e-14 PD=8.1e-07 PS=8e-07 NRD=0.420455 NRS=0.409091 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.77e-06 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.92e-14 panw8=0 panw9=0 panw10=0 $X=3300 $Y=450 $D=25
M13 SH<3> 3 VSS! VSS! nfet L=1.2e-07 W=4.3e-07 AD=1.376e-13 AS=7.74e-14 PD=1.5e-06 PS=7.9e-07 NRD=0.744186 NRS=0.418605 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=3390 $Y=4720 $D=25
M14 VSS! SH<3> SH3_BAR VSS! nfet L=1.2e-07 W=4.4e-07 AD=7.92e-14 AS=8.14e-14 PD=8e-07 PS=8.1e-07 NRD=0.409091 NRS=0.420455 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.28e-06 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.92e-14 panw8=0 panw9=0 panw10=0 $X=3790 $Y=450 $D=25
M15 SH3_BAR SH<3> VSS! VSS! nfet L=1.2e-07 W=4.4e-07 AD=7.92e-14 AS=7.92e-14 PD=8e-07 PS=8e-07 NRD=0.409091 NRS=0.409091 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=8e-07 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.92e-14 panw8=0 panw9=0 panw10=0 $X=4270 $Y=450 $D=25
M16 VSS! SH<3> SH3_BAR VSS! nfet L=1.2e-07 W=4.4e-07 AD=1.408e-13 AS=7.92e-14 PD=1.52e-06 PS=8e-07 NRD=0.727273 NRS=0.409091 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=3.6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.92e-14 panw8=0 panw9=0 panw10=0 $X=4750 $Y=450 $D=25
M17 SH3_BAR SH<3> VDD! VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.404e-13 AS=2.496e-13 PD=1.14e-06 PS=2.2e-06 NRD=0.230769 NRS=0.410256 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=9.42e-14 panw8=4.74e-14 panw9=9.6e-15 panw10=0 $X=410 $Y=1510 $D=108
M18 3 SH3 VDD! VDD! pfet L=1.2e-07 W=8.1e-07 AD=1.458e-13 AS=2.592e-13 PD=1.17e-06 PS=2.26e-06 NRD=0.222222 NRS=0.395062 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.95556e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=9.69e-14 panw8=4.83e-14 panw9=1.32e-14 panw10=0 $X=410 $Y=3310 $D=108
M19 VDD! SH<3> SH3_BAR VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.443e-13 AS=1.404e-13 PD=1.15e-06 PS=1.14e-06 NRD=0.237179 NRS=0.230769 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=2e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.032e-13 panw10=0 $X=890 $Y=1510 $D=108
M20 VDD! SH3 3 VDD! pfet L=1.2e-07 W=8.1e-07 AD=1.83702e-13 AS=1.458e-13 PD=1.28377e-06 PS=1.17e-06 NRD=0.279991 NRS=0.222222 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.93778e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.104e-13 panw10=0 $X=890 $Y=3310 $D=108
M21 SH3_BAR SH<3> VDD! VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.404e-13 AS=1.443e-13 PD=1.14e-06 PS=1.15e-06 NRD=0.230769 NRS=0.237179 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.29e-06 sb=2e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=9.6e-15 panw10=9.36e-14 $X=1380 $Y=1510 $D=108
M22 SH<3> 3 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.404e-13 AS=1.76898e-13 PD=1.14e-06 PS=1.23623e-06 NRD=0.230769 NRS=0.29076 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.37e-06 sb=2e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=9.36e-14 $X=1460 $Y=3310 $D=108
M23 VDD! SH<3> SH3_BAR VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.404e-13 AS=1.404e-13 PD=1.14e-06 PS=1.14e-06 NRD=0.230769 NRS=0.230769 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.77e-06 sb=2e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=9.6e-15 panw10=0 $X=1860 $Y=1510 $D=108
M24 VDD! 3 SH<3> VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.443e-13 AS=1.404e-13 PD=1.15e-06 PS=1.14e-06 NRD=0.237179 NRS=0.230769 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.85e-06 sb=1.77e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=1940 $Y=3310 $D=108
M25 SH3_BAR SH<3> VDD! VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.404e-13 AS=1.404e-13 PD=1.14e-06 PS=1.14e-06 NRD=0.230769 NRS=0.230769 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=9.6e-15 panw10=0 $X=2340 $Y=1510 $D=108
M26 SH<3> 3 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.404e-13 AS=1.443e-13 PD=1.14e-06 PS=1.15e-06 NRD=0.230769 NRS=0.237179 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.28e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=2430 $Y=3310 $D=108
M27 VDD! SH<3> SH3_BAR VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.404e-13 AS=1.404e-13 PD=1.14e-06 PS=1.14e-06 NRD=0.230769 NRS=0.230769 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=2e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=9.6e-15 panw10=0 $X=2820 $Y=1510 $D=108
M28 VDD! 3 SH<3> VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.404e-13 AS=1.404e-13 PD=1.14e-06 PS=1.14e-06 NRD=0.230769 NRS=0.230769 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=2910 $Y=3310 $D=108
M29 SH3_BAR SH<3> VDD! VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.443e-13 AS=1.404e-13 PD=1.15e-06 PS=1.14e-06 NRD=0.237179 NRS=0.230769 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.77e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=9.6e-15 panw10=0 $X=3300 $Y=1510 $D=108
M30 SH<3> 3 VDD! VDD! pfet L=1.2e-07 W=7.8e-07 AD=2.496e-13 AS=1.404e-13 PD=2.2e-06 PS=1.14e-06 NRD=0.410256 NRS=0.230769 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=3390 $Y=3310 $D=108
M31 VDD! SH<3> SH3_BAR VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.404e-13 AS=1.443e-13 PD=1.14e-06 PS=1.15e-06 NRD=0.230769 NRS=0.237179 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=1.28e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=9.6e-15 panw10=7.8e-15 $X=3790 $Y=1510 $D=108
M32 SH3_BAR SH<3> VDD! VDD! pfet L=1.2e-07 W=7.8e-07 AD=1.404e-13 AS=1.404e-13 PD=1.14e-06 PS=1.14e-06 NRD=0.230769 NRS=0.230769 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=9.6e-15 panw10=9.36e-14 $X=4270 $Y=1510 $D=108
M33 VDD! SH<3> SH3_BAR VDD! pfet L=1.2e-07 W=7.8e-07 AD=2.496e-13 AS=1.404e-13 PD=2.2e-06 PS=1.14e-06 NRD=0.410256 NRS=0.230769 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.032e-13 panw10=0 $X=4750 $Y=1510 $D=108
D34 VSS! VDD! diodenwx AREA=1.7976e-11 perim=1.12e-05 t3well=0 $X=0 $Y=1210 $D=474
D35 7 VDD! diodenwx AREA=9.63e-13 perim=3.81e-06 t3well=0 $X=-300 $Y=1210 $D=472
D36 7 VDD! diodenwx AREA=9.63e-13 perim=3.81e-06 t3well=0 $X=5600 $Y=1210 $D=472
.ENDS
***************************************
