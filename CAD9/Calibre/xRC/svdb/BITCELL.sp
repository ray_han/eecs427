* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT BITCELL RENB RWL_N WWL RWLB_P RWL_P RWLB_N REN RBL WBLB WBL VSS! VDD!
** N=125 EP=12 IP=0 FDC=15
M0 7 REN RBL VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=8.96e-14 PD=6.4e-07 PS=1.2e-06 NRD=0.642857 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=0 $X=700 $Y=450 $D=25
M1 RWL_N 14 7 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.04e-14 PD=1.2e-06 PS=6.4e-07 NRD=1.14286 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=0 $X=1180 $Y=450 $D=25
M2 RWLB_N 15 7 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=0 $X=1180 $Y=4870 $D=25
M3 15 14 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=8.96e-14 PD=6.4e-07 PS=1.2e-06 NRD=0.642857 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=2120 $Y=4870 $D=25
M4 14 WWL WBLB VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=8.96e-14 PD=6.4e-07 PS=1.2e-06 NRD=0.642857 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=2240 $Y=450 $D=25
M5 WBL WWL 15 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.04e-14 PD=1.2e-06 PS=6.4e-07 NRD=1.14286 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=2600 $Y=4870 $D=25
M6 VSS! 15 14 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.04e-14 PD=1.2e-06 PS=6.4e-07 NRD=1.14286 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=6e-15 panw6=1.2e-14 panw7=1.56e-14 panw8=0 panw9=0 panw10=0 $X=2720 $Y=450 $D=25
M7 10 RENB RBL VDD! pfet L=1.2e-07 W=8.7e-07 AD=1.566e-13 AS=2.784e-13 PD=1.23e-06 PS=2.38e-06 NRD=0.206897 NRS=0.367816 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.11e-13 panw8=4.14e-14 panw9=3.12e-14 panw10=0 $X=700 $Y=3310 $D=108
M8 RWLB_P 14 10 VDD! pfet L=1.2e-07 W=8.7e-07 AD=2.784e-13 AS=2.784e-13 PD=2.38e-06 PS=2.38e-06 NRD=0.367816 NRS=0.367816 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.356e-13 panw10=0 $X=1180 $Y=1420 $D=108
M9 RWL_P 15 10 VDD! pfet L=1.2e-07 W=8.7e-07 AD=2.784e-13 AS=1.566e-13 PD=2.38e-06 PS=1.23e-06 NRD=0.367816 NRS=0.206897 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.356e-13 panw10=0 $X=1180 $Y=3310 $D=108
M10 15 14 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 AD=1.728e-13 AS=1.728e-13 PD=1.72e-06 PS=1.72e-06 NRD=0.592593 NRS=0.592593 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=7.8e-14 panw10=0 $X=2120 $Y=3310 $D=108
M11 VDD! 15 14 VDD! pfet L=1.2e-07 W=5.4e-07 AD=1.728e-13 AS=1.728e-13 PD=1.72e-06 PS=1.72e-06 NRD=0.592593 NRS=0.592593 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=8.88e-14 panw8=2.4e-14 panw9=1.32e-14 panw10=0 $X=2670 $Y=1750 $D=108
D12 VSS! VSS! diodenx AREA=1.12e-13 perim=1.36e-06 $X=0 $Y=5460 $D=480
D13 VSS! VDD! diodenwx AREA=1.0998e-11 perim=1.024e-05 t3well=0 $X=0 $Y=1030 $D=474
D14 17 VDD! diodenwx AREA=6.804e-13 perim=3.66e-06 t3well=0 $X=3200 $Y=1180 $D=472
.ENDS
***************************************
