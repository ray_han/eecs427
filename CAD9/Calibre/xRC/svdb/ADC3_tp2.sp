* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT ADC3_tp2 CLK Vin Vref OUT VSS! VDD!
** N=120 EP=6 IP=0 FDC=17
M0 VSS! 5 OUT VSS! nfet L=1.2e-07 W=5.6e-07 AD=2.016e-13 AS=2.016e-13 PD=1.84e-06 PS=1.84e-06 NRD=0.642857 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=4.2e-14 $X=380 $Y=1330 $D=25
M1 8 4 2 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.59676e-14 AS=1.008e-13 PD=6.20541e-07 PS=1.28e-06 NRD=1.09653 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=1.3e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=3.36e-14 $X=1460 $Y=1610 $D=25
M2 5 Vin 8 VSS! nfet L=5e-07 W=1.2e-06 AD=4.32e-13 AS=3.68432e-13 PD=3.12e-06 PS=2.65946e-06 NRD=0.3 NRS=0.255856 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4.90667e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=1.75e-13 $X=2020 $Y=690 $D=25
M3 2 CLK VSS! VSS! nfet L=1.2e-07 W=5e-07 AD=1.2e-13 AS=1.8e-13 PD=9.8e-07 PS=1.72e-06 NRD=0.48 NRS=0.72 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=9.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=0 $X=3150 $Y=2660 $D=25
M4 VSS! CLK 2 VSS! nfet L=1.2e-07 W=5e-07 AD=1.8e-13 AS=1.2e-13 PD=1.72e-06 PS=9.8e-07 NRD=0.72 NRS=0.48 m=1 par=1 nf=1 ngcon=1 psp=0 sa=9.6e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=0 $X=3150 $Y=3260 $D=25
M5 9 Vref 4 VSS! nfet L=5e-07 W=1.2e-06 AD=3.70703e-13 AS=4.32e-13 PD=2.67568e-06 PS=3.12e-06 NRD=0.257432 NRS=0.3 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=4.86e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=1.7e-13 $X=4280 $Y=680 $D=25
M6 2 5 9 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=8.64973e-14 PD=1.28e-06 PS=6.24324e-07 NRD=1.28571 NRS=1.10328 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.26643e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=3.36e-14 $X=5220 $Y=1610 $D=25
M7 12 4 VSS! VSS! nfet L=1.2e-07 W=5.6e-07 AD=2.016e-13 AS=2.016e-13 PD=1.84e-06 PS=1.84e-06 NRD=0.642857 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=0 panw10=4.2e-14 $X=6300 $Y=1330 $D=25
M8 VDD! 5 OUT VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=1.008e-13 PD=1.28e-06 PS=1.28e-06 NRD=1.28571 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=3.6e-15 panw6=1.2e-14 panw7=6.96e-14 panw8=1.56e-14 panw9=0 panw10=0 $X=380 $Y=4010 $D=108
M9 VDD! 4 5 VDD! pfet L=1.2e-07 W=2.8e-07 AD=6.16e-14 AS=1.008e-13 PD=7.2e-07 PS=1.28e-06 NRD=0.785714 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=9.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=3.6e-15 panw6=1.2e-14 panw7=3.6e-14 panw8=1.56e-14 panw9=3.36e-14 panw10=3.36e-14 $X=1460 $Y=4010 $D=108
M10 5 CLK VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=6.16e-14 PD=1.28e-06 PS=7.2e-07 NRD=1.28571 NRS=0.785714 m=1 par=1 nf=1 ngcon=1 psp=0 sa=9.2e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=3.6e-15 panw6=1.2e-14 panw7=6.96e-14 panw8=1.56e-14 panw9=0 panw10=0 $X=2020 $Y=4010 $D=108
M11 VDD! CLK 4 VDD! pfet L=1.2e-07 W=2.8e-07 AD=6.16e-14 AS=1.008e-13 PD=7.2e-07 PS=1.28e-06 NRD=0.785714 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=9.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=3.6e-15 panw6=1.2e-14 panw7=6.96e-14 panw8=1.56e-14 panw9=0 panw10=0 $X=4660 $Y=4010 $D=108
M12 4 5 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=6.16e-14 PD=1.28e-06 PS=7.2e-07 NRD=1.28571 NRS=0.785714 m=1 par=1 nf=1 ngcon=1 psp=0 sa=9.2e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=3.6e-15 panw6=1.2e-14 panw7=3.6e-14 panw8=1.56e-14 panw9=3.36e-14 panw10=3.36e-14 $X=5220 $Y=4010 $D=108
M13 12 4 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=1.008e-13 PD=1.28e-06 PS=1.28e-06 NRD=1.28571 NRS=1.28571 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.6e-07 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=3.6e-15 panw6=1.2e-14 panw7=6.96e-14 panw8=1.56e-14 panw9=0 panw10=0 $X=6300 $Y=4010 $D=108
D14 VSS! VDD! diodenx AREA=3.36e-13 perim=2.96e-06 $X=2800 $Y=4660 $D=480
D15 VSS! VDD! diodenwx AREA=4.312e-12 perim=8.96e-06 t3well=0 $X=-280 $Y=3540 $D=474
D16 VSS! VDD! diodenwx AREA=4.312e-12 perim=8.96e-06 t3well=0 $X=4000 $Y=3540 $D=474
.ENDS
***************************************
