* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT special_inverter IN<15> OUT<15> IN<14> OUT<14> IN<13> OUT<13> IN<12> OUT<12> IN<11> OUT<11> IN<10> OUT<10> IN<9> OUT<9> IN<8> OUT<8> IN<7> OUT<7> IN<6> OUT<6>
+ IN<5> OUT<5> IN<4> OUT<4> IN<3> OUT<3> IN<2> OUT<2> IN<1> OUT<1> IN<0> VDD! VSS! OUT<0>
** N=693 EP=34 IP=0 FDC=97
M0 2 IN<15> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=1180 $Y=440 $D=25
M1 OUT<15> 2 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=2270 $Y=440 $D=25
M2 5 IN<14> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=4380 $Y=440 $D=25
M3 OUT<14> 5 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=5470 $Y=440 $D=25
M4 8 IN<13> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=7580 $Y=440 $D=25
M5 OUT<13> 8 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=8670 $Y=440 $D=25
M6 11 IN<12> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=10780 $Y=440 $D=25
M7 OUT<12> 11 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=11870 $Y=440 $D=25
M8 14 IN<11> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=13980 $Y=440 $D=25
M9 OUT<11> 14 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=15070 $Y=440 $D=25
M10 17 IN<10> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=17180 $Y=440 $D=25
M11 OUT<10> 17 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=18270 $Y=440 $D=25
M12 20 IN<9> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=20380 $Y=440 $D=25
M13 OUT<9> 20 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=21470 $Y=440 $D=25
M14 23 IN<8> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=23580 $Y=440 $D=25
M15 OUT<8> 23 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=24670 $Y=440 $D=25
M16 26 IN<7> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=26780 $Y=440 $D=25
M17 OUT<7> 26 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=27870 $Y=440 $D=25
M18 29 IN<6> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=29980 $Y=440 $D=25
M19 OUT<6> 29 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=31070 $Y=440 $D=25
M20 32 IN<5> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=33180 $Y=440 $D=25
M21 OUT<5> 32 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=34270 $Y=440 $D=25
M22 35 IN<4> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=36380 $Y=440 $D=25
M23 OUT<4> 35 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=37470 $Y=440 $D=25
M24 38 IN<3> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=39580 $Y=440 $D=25
M25 OUT<3> 38 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=40670 $Y=440 $D=25
M26 41 IN<2> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=42780 $Y=440 $D=25
M27 OUT<2> 41 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=43870 $Y=440 $D=25
M28 44 IN<1> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=45980 $Y=440 $D=25
M29 OUT<1> 44 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=47070 $Y=440 $D=25
M30 47 IN<0> VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=49180 $Y=440 $D=25
M31 OUT<0> 47 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=2.16e-14 panw10=1.2e-14 $X=50270 $Y=440 $D=25
M32 VDD! IN<15> 2 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=1.26e-13 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=810 $Y=2690 $D=108
M33 2 IN<15> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=1.884e-13 panw10=1.8e-14 $X=1290 $Y=2690 $D=108
M34 VDD! IN<15> 2 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.14e-13 $X=1770 $Y=2690 $D=108
M35 OUT<15> 2 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=2270 $Y=3210 $D=108
M36 VDD! IN<14> 5 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=4010 $Y=2690 $D=108
M37 5 IN<14> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=4490 $Y=2690 $D=108
M38 VDD! IN<14> 5 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=4970 $Y=2690 $D=108
M39 OUT<14> 5 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=5470 $Y=3210 $D=108
M40 VDD! IN<13> 8 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=7210 $Y=2690 $D=108
M41 8 IN<13> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=7690 $Y=2690 $D=108
M42 VDD! IN<13> 8 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=8170 $Y=2690 $D=108
M43 OUT<13> 8 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=8670 $Y=3210 $D=108
M44 VDD! IN<12> 11 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=10410 $Y=2690 $D=108
M45 11 IN<12> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=10890 $Y=2690 $D=108
M46 VDD! IN<12> 11 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=11370 $Y=2690 $D=108
M47 OUT<12> 11 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=11870 $Y=3210 $D=108
M48 VDD! IN<11> 14 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=13610 $Y=2690 $D=108
M49 14 IN<11> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=14090 $Y=2690 $D=108
M50 VDD! IN<11> 14 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=14570 $Y=2690 $D=108
M51 OUT<11> 14 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=15070 $Y=3210 $D=108
M52 VDD! IN<10> 17 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=16810 $Y=2690 $D=108
M53 17 IN<10> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=17290 $Y=2690 $D=108
M54 VDD! IN<10> 17 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=17770 $Y=2690 $D=108
M55 OUT<10> 17 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=18270 $Y=3210 $D=108
M56 VDD! IN<9> 20 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=20010 $Y=2690 $D=108
M57 20 IN<9> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=20490 $Y=2690 $D=108
M58 VDD! IN<9> 20 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=20970 $Y=2690 $D=108
M59 OUT<9> 20 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=21470 $Y=3210 $D=108
M60 VDD! IN<8> 23 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=23210 $Y=2690 $D=108
M61 23 IN<8> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=23690 $Y=2690 $D=108
M62 VDD! IN<8> 23 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=24170 $Y=2690 $D=108
M63 OUT<8> 23 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=24670 $Y=3210 $D=108
M64 VDD! IN<7> 26 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=26410 $Y=2690 $D=108
M65 26 IN<7> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=26890 $Y=2690 $D=108
M66 VDD! IN<7> 26 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=27370 $Y=2690 $D=108
M67 OUT<7> 26 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=27870 $Y=3210 $D=108
M68 VDD! IN<6> 29 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=29610 $Y=2690 $D=108
M69 29 IN<6> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=30090 $Y=2690 $D=108
M70 VDD! IN<6> 29 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=30570 $Y=2690 $D=108
M71 OUT<6> 29 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=31070 $Y=3210 $D=108
M72 VDD! IN<5> 32 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=32810 $Y=2690 $D=108
M73 32 IN<5> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=33290 $Y=2690 $D=108
M74 VDD! IN<5> 32 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=33770 $Y=2690 $D=108
M75 OUT<5> 32 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=34270 $Y=3210 $D=108
M76 VDD! IN<4> 35 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=36010 $Y=2690 $D=108
M77 35 IN<4> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=36490 $Y=2690 $D=108
M78 VDD! IN<4> 35 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=36970 $Y=2690 $D=108
M79 OUT<4> 35 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=37470 $Y=3210 $D=108
M80 VDD! IN<3> 38 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=39210 $Y=2690 $D=108
M81 38 IN<3> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=39690 $Y=2690 $D=108
M82 VDD! IN<3> 38 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=40170 $Y=2690 $D=108
M83 OUT<3> 38 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=40670 $Y=3210 $D=108
M84 VDD! IN<2> 41 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=42410 $Y=2690 $D=108
M85 41 IN<2> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=42890 $Y=2690 $D=108
M86 VDD! IN<2> 41 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=43370 $Y=2690 $D=108
M87 OUT<2> 41 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=43870 $Y=3210 $D=108
M88 VDD! IN<1> 44 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=45610 $Y=2690 $D=108
M89 44 IN<1> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=46090 $Y=2690 $D=108
M90 VDD! IN<1> 44 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=46570 $Y=2690 $D=108
M91 OUT<1> 44 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=2.4e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=47070 $Y=3210 $D=108
M92 VDD! IN<0> 47 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.455e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.8e-14 $X=48810 $Y=2690 $D=108
M93 47 IN<0> VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=9.75e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=9.24e-14 panw10=1.14e-13 $X=49290 $Y=2690 $D=108
M94 VDD! IN<0> 47 VDD! pfet L=1.2e-07 W=8e-07 AD=2.02074e-13 AS=1.44e-13 PD=1.74815e-06 PS=1.16e-06 NRD=0.315741 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=4.95e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=3e-14 panw8=4.8e-14 panw9=1.884e-13 panw10=1.8e-14 $X=49770 $Y=2690 $D=108
M95 OUT<0> 47 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=7.07259e-14 PD=1.2e-06 PS=6.11852e-07 NRD=1.14286 NRS=0.902116 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.78e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=3.6e-15 panw7=5.76e-14 panw8=6e-15 panw9=1.56e-14 panw10=1.8e-14 $X=50270 $Y=3210 $D=108
D96 VSS! VDD! diodenwx AREA=1.07738e-10 perim=0.00010588 t3well=0 $X=190 $Y=1940 $D=474
.ENDS
***************************************
