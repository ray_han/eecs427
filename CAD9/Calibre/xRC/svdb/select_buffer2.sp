* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT select_buffer2 SH1_BAR SH1 VSS! SH<1> VDD!
** N=65 EP=5 IP=0 FDC=15
M0 SH<1> SH1_BAR VSS! VSS! nfet L=1.2e-07 W=4.4e-07 AD=7.92e-14 AS=1.408e-13 PD=8e-07 PS=1.52e-06 NRD=0.409091 NRS=0.727273 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.77e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.68e-14 panw8=0 panw9=0 panw10=0 $X=410 $Y=450 $D=25
M1 SH1_BAR SH1 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 AD=6.3e-14 AS=1.12e-13 PD=7.1e-07 PS=1.34e-06 NRD=0.514286 NRS=0.914286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.4e-14 panw8=7.2e-15 panw9=0 panw10=0 $X=700 $Y=4800 $D=25
M2 VSS! SH1_BAR SH<1> VSS! nfet L=1.2e-07 W=4.4e-07 AD=8.14e-14 AS=7.92e-14 PD=8.1e-07 PS=8e-07 NRD=0.420455 NRS=0.409091 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.29e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.68e-14 panw8=0 panw9=0 panw10=0 $X=890 $Y=450 $D=25
M3 VSS! SH1 SH1_BAR VSS! nfet L=1.2e-07 W=3.5e-07 AD=1.12e-13 AS=6.3e-14 PD=1.34e-06 PS=7.1e-07 NRD=0.914286 NRS=0.514286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.4e-14 panw8=7.2e-15 panw9=0 panw10=0 $X=1180 $Y=4800 $D=25
M4 SH<1> SH1_BAR VSS! VSS! nfet L=1.2e-07 W=4.4e-07 AD=7.92e-14 AS=8.14e-14 PD=8e-07 PS=8.1e-07 NRD=0.409091 NRS=0.420455 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.29e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.68e-14 panw8=0 panw9=0 panw10=0 $X=1380 $Y=450 $D=25
M5 VSS! SH1_BAR SH<1> VSS! nfet L=1.2e-07 W=4.4e-07 AD=1.408e-13 AS=7.92e-14 PD=1.52e-06 PS=8e-07 NRD=0.727273 NRS=0.409091 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.77e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.68e-14 panw8=0 panw9=0 panw10=0 $X=1860 $Y=450 $D=25
M6 SH<1> SH1_BAR VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.77e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=9.6e-14 panw8=4.8e-14 panw9=1.2e-14 panw10=0 $X=410 $Y=1490 $D=108
M7 SH1_BAR SH1 VDD! VDD! pfet L=1.2e-07 W=6.8e-07 AD=1.224e-13 AS=2.176e-13 PD=1.04e-06 PS=2e-06 NRD=0.264706 NRS=0.470588 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.16e-14 panw9=8.16e-14 panw10=8.16e-14 $X=700 $Y=3310 $D=108
M8 VDD! SH1_BAR SH<1> VDD! pfet L=1.2e-07 W=8e-07 AD=1.48e-13 AS=1.44e-13 PD=1.17e-06 PS=1.16e-06 NRD=0.23125 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.29e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.08e-13 panw10=9.6e-14 $X=890 $Y=1490 $D=108
M9 VDD! SH1 SH1_BAR VDD! pfet L=1.2e-07 W=6.8e-07 AD=2.176e-13 AS=1.224e-13 PD=2e-06 PS=1.04e-06 NRD=0.470588 NRS=0.264706 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.16e-14 panw9=0 panw10=1.632e-13 $X=1180 $Y=3310 $D=108
M10 SH<1> SH1_BAR VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.48e-13 PD=1.16e-06 PS=1.17e-06 NRD=0.225 NRS=0.23125 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.29e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.08e-13 panw10=9.6e-14 $X=1380 $Y=1490 $D=108
M11 VDD! SH1_BAR SH<1> VDD! pfet L=1.2e-07 W=8e-07 AD=2.56e-13 AS=1.44e-13 PD=2.24e-06 PS=1.16e-06 NRD=0.4 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.77e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=8.8e-14 panw8=5.6e-14 panw9=1.2e-14 panw10=0 $X=1860 $Y=1490 $D=108
D12 VSS! VDD! diodenwx AREA=7.44e-12 perim=4.8e-06 t3well=0 $X=0 $Y=1190 $D=474
D13 6 VDD! diodenwx AREA=9.3e-13 perim=3.7e-06 t3well=0 $X=-300 $Y=1190 $D=472
D14 6 VDD! diodenwx AREA=9.3e-13 perim=3.7e-06 t3well=0 $X=2400 $Y=1190 $D=472
.ENDS
***************************************
