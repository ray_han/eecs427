* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT REN_Driver_1bit RENB REN IN VSS! VDD!
** N=97 EP=5 IP=0 FDC=21
M0 VSS! 4 2 VSS! nfet L=1.2e-07 W=4e-07 AD=7.2381e-14 AS=1.28e-13 PD=7.61905e-07 PS=1.44e-06 NRD=0.452381 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=1.2e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.68e-14 panw8=0 panw9=0 panw10=0 $X=750 $Y=450 $D=25
M1 VSS! 4 2 VSS! nfet L=1.2e-07 W=4e-07 AD=7.2e-14 AS=1.28e-13 PD=7.6e-07 PS=1.44e-06 NRD=0.45 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.2e-14 panw8=0 panw9=0 panw10=0 $X=770 $Y=4750 $D=25
M2 RENB 2 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 AD=7.92e-14 AS=7.9619e-14 PD=8e-07 PS=8.38095e-07 NRD=0.409091 NRS=0.411255 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7.45455e-07 sb=1.76e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.68e-14 panw8=0 panw9=0 panw10=0 $X=1230 $Y=450 $D=25
M3 4 IN VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=7.2e-14 PD=1.44e-06 PS=7.6e-07 NRD=0.8 NRS=0.45 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.2e-14 panw8=0 panw9=0 panw10=0 $X=1250 $Y=4750 $D=25
M4 VSS! 2 RENB VSS! nfet L=1.2e-07 W=4.4e-07 AD=7.92e-14 AS=7.92e-14 PD=8e-07 PS=8e-07 NRD=0.409091 NRS=0.409091 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.22545e-06 sb=1.28e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.68e-14 panw8=0 panw9=0 panw10=0 $X=1710 $Y=450 $D=25
M5 RENB 2 VSS! VSS! nfet L=1.2e-07 W=4.4e-07 AD=7.92e-14 AS=7.92e-14 PD=8e-07 PS=8e-07 NRD=0.409091 NRS=0.409091 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.70545e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.68e-14 panw8=0 panw9=0 panw10=0 $X=2190 $Y=450 $D=25
M6 VSS! IN 6 VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.13882e-14 AS=8.96e-14 PD=6.25882e-07 PS=1.2e-06 NRD=0.655462 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=0 $X=2190 $Y=4750 $D=25
M7 VSS! 2 RENB VSS! nfet L=1.2e-07 W=4.4e-07 AD=1.408e-13 AS=7.92e-14 PD=1.52e-06 PS=8e-07 NRD=0.727273 NRS=0.409091 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.96727e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.68e-14 panw8=0 panw9=0 panw10=0 $X=2670 $Y=450 $D=25
M8 REN 6 VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=7.34118e-14 PD=1.44e-06 PS=8.94118e-07 NRD=0.8 NRS=0.458824 m=1 par=1 nf=1 ngcon=1 psp=0 sa=6.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.2e-14 panw8=0 panw9=0 panw10=0 $X=2670 $Y=4750 $D=25
M9 VDD! 4 2 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=2.56e-13 PD=1.16e-06 PS=2.24e-06 NRD=0.225 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=6.4e-14 panw8=8e-14 panw9=1.2e-14 panw10=0 $X=750 $Y=1490 $D=108
M10 VDD! 4 2 VDD! pfet L=1.2e-07 W=8e-07 AD=1.44203e-13 AS=2.56e-13 PD=1.17468e-06 PS=2.24e-06 NRD=0.225316 NRS=0.4 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=7.85e-07 sd=0 panw1=0 panw2=1.2e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=4.8e-14 panw8=9.6e-14 panw9=1.68e-14 panw10=0 $X=770 $Y=3310 $D=108
M11 RENB 2 VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.76e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.08e-13 panw10=0 $X=1230 $Y=1490 $D=108
M12 4 IN VDD! VDD! pfet L=1.2e-07 W=7.8e-07 AD=2.496e-13 AS=1.40597e-13 PD=2.2e-06 PS=1.14532e-06 NRD=0.410256 NRS=0.231094 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=4.8e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.104e-13 panw10=0 $X=1250 $Y=3310 $D=108
M13 VDD! 2 RENB VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=1.28e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.2e-14 panw10=1.92e-13 $X=1710 $Y=1490 $D=108
M14 RENB 2 VDD! VDD! pfet L=1.2e-07 W=8e-07 AD=1.44e-13 AS=1.44e-13 PD=1.16e-06 PS=1.16e-06 NRD=0.225 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.08e-13 panw10=0 $X=2190 $Y=1490 $D=108
M15 VDD! IN 6 VDD! pfet L=1.2e-07 W=6.6e-07 AD=1.20384e-13 AS=2.112e-13 PD=1.056e-06 PS=1.96e-06 NRD=0.276364 NRS=0.484848 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=2.4e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=9.6e-14 panw10=0 $X=2190 $Y=3310 $D=108
M16 VDD! 2 RENB VDD! pfet L=1.2e-07 W=8e-07 AD=2.56e-13 AS=1.44e-13 PD=2.24e-06 PS=1.16e-06 NRD=0.4 NRS=0.225 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.2e-13 panw8=2.4e-14 panw9=1.2e-14 panw10=0 $X=2670 $Y=1490 $D=108
M17 REN 6 VDD! VDD! pfet L=1.2e-07 W=8.4e-07 AD=2.688e-13 AS=1.53216e-13 PD=2.32e-06 PS=1.344e-06 NRD=0.380952 NRS=0.217143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=6.71429e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.248e-13 panw8=2.4e-14 panw9=1.68e-14 panw10=0 $X=2670 $Y=3310 $D=108
D18 VSS! VSS! diodenx AREA=1.12e-13 perim=1.36e-06 $X=0 $Y=5460 $D=480
D19 VSS! VDD! diodenwx AREA=1.0432e-11 perim=9.66e-06 t3well=0 $X=0 $Y=1190 $D=474
D20 9 VDD! diodenwx AREA=6.846e-13 perim=3.68e-06 t3well=0 $X=3200 $Y=1190 $D=472
.ENDS
***************************************
