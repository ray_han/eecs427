#set sdc_version 1.3

 create_clock -name "I_clk" -period 3 -waveform {0 1.5}  find(port,"I_clk")
 set_clock_uncertainty 0.1 I_clk


