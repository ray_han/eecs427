module eecs427_core(clk, A, out, resetn);
   input clk;
   input [7:0] A;
   output [7:0] out;
   input 	resetn;
   
   wire [15:0] 	result;   
   wire [15:0] 	ram_out;
   
   supply0 VSS;
   
   RA1SH16x512 ram(.Q(ram_out), 
		   .CLK(clk),
		   .CEN(VSS),
		   .WEN(VSS),
		   .A({VSS,A[7:0]}),
		   .D(result[15:0])
		   );
		   
   mult mult(.clk(clk), 
	     .a(A[7:0]), 
	     .b(ram_out[7:0]), 
	     .result(result),
	     .resetn(resetn)
	     );
   assign 	out = result[7:0];   
   
endmodule // eecs427_core
