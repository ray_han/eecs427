// Created by ihdl
module we_reorder_functional(WE, inst15_12,inst7_4,WE_OUT);
	input [15:0]		WE;
	input [3:0] inst15_12,inst7_4;
	output reg [15:0]	WE_OUT;

always@(*)begin
	if({inst15_12,inst7_4} == {4'b0000,4'b0100} || {inst15_12,inst7_4} == {4'b0000,4'b1111})	
								WE_OUT = 16'h0000; //NO WB for STOR_SRAM, RST_SEL
	else
		WE_OUT=WE;

end





endmodule
