/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06
// Date      : Tue Dec 11 19:33:09 2018
/////////////////////////////////////////////////////////////


module mult_DW_mult_uns_0 ( a, b, product );
  input [7:0] a;
  input [7:0] b;
  output [15:0] product;
  wire   n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44,
         n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58,
         n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72,
         n73, n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86,
         n87, n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100,
         n101, n102, n103, n104, n105, n106, n107, n108, n109, n110, n111,
         n112, n113, n114, n115, n116, n117, n118, n119, n120, n121, n122,
         n123, n124, n125, n126, n127, n128, n129, n130, n131, n132, n133,
         n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146, n147, n200, n201, n202, n203, n204, n205, n206, n207,
         n208, n209, n210, n211, n212, n213, n214, n215, n216, n217, n218,
         n219, n220, n221, n222, n223, n224, n225, n226, n227, n228, n229,
         n230, n231;

  CMPR32X2TR U3 ( .A(n18), .B(n16), .C(n3), .CO(n2), .S(product[13]) );
  CMPR32X2TR U4 ( .A(n21), .B(n19), .C(n4), .CO(n3), .S(product[12]) );
  CMPR32X2TR U5 ( .A(n22), .B(n26), .C(n5), .CO(n4), .S(product[11]) );
  CMPR32X2TR U6 ( .A(n27), .B(n32), .C(n6), .CO(n5), .S(product[10]) );
  CMPR32X2TR U7 ( .A(n33), .B(n40), .C(n7), .CO(n6), .S(product[9]) );
  CMPR32X2TR U8 ( .A(n41), .B(n50), .C(n8), .CO(n7), .S(product[8]) );
  CMPR32X2TR U9 ( .A(n51), .B(n60), .C(n9), .CO(n8), .S(product[7]) );
  CMPR32X2TR U10 ( .A(n61), .B(n68), .C(n10), .CO(n9), .S(product[6]) );
  CMPR32X2TR U12 ( .A(n76), .B(n79), .C(n12), .CO(n11), .S(product[4]) );
  CMPR32X2TR U13 ( .A(n80), .B(n82), .C(n13), .CO(n12), .S(product[3]) );
  CMPR32X2TR U14 ( .A(n14), .B(n132), .C(n84), .CO(n13), .S(product[2]) );
  ADDHXLTR U15 ( .A(n140), .B(n147), .CO(n14), .S(product[1]) );
  CMPR32X2TR U16 ( .A(n86), .B(n93), .C(n17), .CO(n15), .S(n16) );
  CMPR42X1TR U17 ( .A(n101), .B(n87), .C(n94), .D(n23), .ICI(n20), .S(n19), 
        .ICO(n17), .CO(n18) );
  CMPR42X1TR U18 ( .A(n95), .B(n28), .C(n24), .D(n29), .ICI(n25), .S(n22), 
        .ICO(n20), .CO(n21) );
  CMPR32X2TR U19 ( .A(n102), .B(n109), .C(n88), .CO(n23), .S(n24) );
  CMPR42X1TR U20 ( .A(n96), .B(n34), .C(n30), .D(n35), .ICI(n31), .S(n27), 
        .ICO(n25), .CO(n26) );
  CMPR42X1TR U21 ( .A(n103), .B(n89), .C(n117), .D(n110), .ICI(n37), .S(n30), 
        .ICO(n28), .CO(n29) );
  CMPR42X1TR U22 ( .A(n38), .B(n42), .C(n36), .D(n43), .ICI(n39), .S(n33), 
        .ICO(n31), .CO(n32) );
  CMPR42X1TR U23 ( .A(n104), .B(n118), .C(n111), .D(n47), .ICI(n45), .S(n36), 
        .ICO(n34), .CO(n35) );
  CMPR32X2TR U24 ( .A(n90), .B(n125), .C(n97), .CO(n37), .S(n38) );
  CMPR42X1TR U25 ( .A(n46), .B(n48), .C(n53), .D(n44), .ICI(n49), .S(n41), 
        .ICO(n39), .CO(n40) );
  CMPR42X1TR U26 ( .A(n119), .B(n105), .C(n52), .D(n57), .ICI(n55), .S(n44), 
        .ICO(n42), .CO(n43) );
  CMPR32X2TR U27 ( .A(n133), .B(n112), .C(n126), .CO(n45), .S(n46) );
  ADDHXLTR U28 ( .A(n98), .B(n91), .CO(n47), .S(n48) );
  CMPR42X1TR U29 ( .A(n56), .B(n58), .C(n59), .D(n63), .ICI(n54), .S(n51), 
        .ICO(n49), .CO(n50) );
  CMPR42X1TR U30 ( .A(n113), .B(n134), .C(n127), .D(n62), .ICI(n65), .S(n54), 
        .ICO(n52), .CO(n53) );
  CMPR32X2TR U31 ( .A(n141), .B(n120), .C(n106), .CO(n55), .S(n56) );
  ADDHXLTR U32 ( .A(n99), .B(n92), .CO(n57), .S(n58) );
  CMPR42X1TR U33 ( .A(n135), .B(n72), .C(n66), .D(n67), .ICI(n64), .S(n61), 
        .ICO(n59), .CO(n60) );
  CMPR42X1TR U34 ( .A(n142), .B(n114), .C(n121), .D(n128), .ICI(n70), .S(n64), 
        .ICO(n62), .CO(n63) );
  ADDHXLTR U35 ( .A(n107), .B(n100), .CO(n65), .S(n66) );
  CMPR42X1TR U36 ( .A(n136), .B(n74), .C(n77), .D(n71), .ICI(n73), .S(n69), 
        .ICO(n67), .CO(n68) );
  CMPR32X2TR U37 ( .A(n143), .B(n129), .C(n122), .CO(n70), .S(n71) );
  ADDHXLTR U38 ( .A(n115), .B(n108), .CO(n72), .S(n73) );
  CMPR42X1TR U39 ( .A(n144), .B(n130), .C(n137), .D(n81), .ICI(n78), .S(n76), 
        .ICO(n74), .CO(n75) );
  ADDHXLTR U40 ( .A(n123), .B(n116), .CO(n77), .S(n78) );
  CMPR32X2TR U41 ( .A(n138), .B(n145), .C(n83), .CO(n79), .S(n80) );
  ADDHXLTR U42 ( .A(n131), .B(n124), .CO(n81), .S(n82) );
  ADDHXLTR U43 ( .A(n146), .B(n139), .CO(n83), .S(n84) );
  CMPR32X2TR U126 ( .A(n69), .B(n75), .C(n11), .CO(n10), .S(product[5]) );
  CLKINVX2TR U127 ( .A(b[7]), .Y(n200) );
  CLKINVX2TR U128 ( .A(a[7]), .Y(n201) );
  CLKINVX2TR U129 ( .A(a[5]), .Y(n202) );
  CLKINVX2TR U130 ( .A(b[5]), .Y(n203) );
  CLKINVX2TR U131 ( .A(b[6]), .Y(n204) );
  CLKINVX2TR U132 ( .A(b[4]), .Y(n205) );
  CLKINVX2TR U133 ( .A(a[4]), .Y(n206) );
  INVX2TR U134 ( .A(a[0]), .Y(n207) );
  CLKINVX2TR U135 ( .A(a[6]), .Y(n208) );
  CLKINVX2TR U136 ( .A(a[3]), .Y(n209) );
  CLKINVX2TR U137 ( .A(a[1]), .Y(n210) );
  CLKINVX2TR U138 ( .A(b[2]), .Y(n211) );
  CLKINVX2TR U139 ( .A(b[0]), .Y(n212) );
  CLKINVX2TR U140 ( .A(b[1]), .Y(n213) );
  CLKINVX1TR U141 ( .A(a[2]), .Y(n214) );
  CMPR32X2TR U142 ( .A(n15), .B(n85), .C(n2), .CO(product[15]), .S(product[14]) );
  CLKINVX1TR U143 ( .A(b[3]), .Y(n215) );
  NOR2XLTR U144 ( .A(n217), .B(n210), .Y(n134) );
  NOR2XLTR U145 ( .A(n208), .B(n205), .Y(n96) );
  NOR2XLTR U146 ( .A(n221), .B(n227), .Y(n114) );
  NOR2XLTR U147 ( .A(n221), .B(n230), .Y(n138) );
  NOR2XLTR U148 ( .A(n205), .B(n201), .Y(n88) );
  INVX2TR U149 ( .A(a[4]), .Y(n227) );
  NOR2XLTR U150 ( .A(n220), .B(n229), .Y(n129) );
  NOR2XLTR U151 ( .A(n231), .B(n220), .Y(n145) );
  NOR2XLTR U152 ( .A(n220), .B(n230), .Y(n137) );
  NOR2XLTR U153 ( .A(n220), .B(n228), .Y(n121) );
  NOR2XLTR U154 ( .A(n215), .B(n227), .Y(n113) );
  INVX2TR U155 ( .A(b[3]), .Y(n220) );
  INVX2TR U156 ( .A(a[0]), .Y(n231) );
  CLKINVX2TR U157 ( .A(a[6]), .Y(n225) );
  INVX2TR U158 ( .A(b[0]), .Y(n223) );
  INVX2TR U159 ( .A(b[1]), .Y(n222) );
  INVX2TR U160 ( .A(b[2]), .Y(n221) );
  INVX2TR U161 ( .A(b[4]), .Y(n219) );
  CLKINVX2TR U162 ( .A(b[5]), .Y(n218) );
  CLKINVX2TR U163 ( .A(b[7]), .Y(n216) );
  CLKINVX2TR U164 ( .A(b[6]), .Y(n217) );
  INVX2TR U165 ( .A(a[3]), .Y(n228) );
  INVX2TR U166 ( .A(a[2]), .Y(n229) );
  INVX2TR U167 ( .A(a[1]), .Y(n230) );
  CLKINVX2TR U168 ( .A(a[5]), .Y(n226) );
  CLKINVX2TR U169 ( .A(a[7]), .Y(n224) );
  NOR2X1TR U170 ( .A(n207), .B(n212), .Y(product[0]) );
  NOR2X1TR U171 ( .A(n225), .B(n213), .Y(n99) );
  NOR2X1TR U172 ( .A(n225), .B(n211), .Y(n98) );
  NOR2X1TR U173 ( .A(n225), .B(n215), .Y(n97) );
  NOR2X1TR U174 ( .A(n208), .B(n203), .Y(n95) );
  NOR2X1TR U175 ( .A(n208), .B(n204), .Y(n94) );
  NOR2X1TR U176 ( .A(n208), .B(n200), .Y(n93) );
  NOR2X1TR U177 ( .A(n212), .B(n224), .Y(n92) );
  NOR2X1TR U178 ( .A(n213), .B(n224), .Y(n91) );
  NOR2X1TR U179 ( .A(n211), .B(n224), .Y(n90) );
  NOR2X1TR U180 ( .A(n215), .B(n224), .Y(n89) );
  NOR2X1TR U181 ( .A(n203), .B(n201), .Y(n87) );
  NOR2X1TR U182 ( .A(n204), .B(n201), .Y(n86) );
  NOR2X1TR U183 ( .A(n200), .B(n201), .Y(n85) );
  NOR2X1TR U184 ( .A(n231), .B(n222), .Y(n147) );
  NOR2X1TR U185 ( .A(n231), .B(n221), .Y(n146) );
  NOR2X1TR U186 ( .A(n231), .B(n219), .Y(n144) );
  NOR2X1TR U187 ( .A(n207), .B(n218), .Y(n143) );
  NOR2X1TR U188 ( .A(n207), .B(n217), .Y(n142) );
  NOR2X1TR U189 ( .A(n207), .B(n216), .Y(n141) );
  NOR2X1TR U190 ( .A(n223), .B(n230), .Y(n140) );
  NOR2X1TR U191 ( .A(n222), .B(n230), .Y(n139) );
  NOR2X1TR U192 ( .A(n219), .B(n210), .Y(n136) );
  NOR2X1TR U193 ( .A(n218), .B(n210), .Y(n135) );
  NOR2X1TR U194 ( .A(n216), .B(n210), .Y(n133) );
  NOR2X1TR U195 ( .A(n212), .B(n229), .Y(n132) );
  NOR2X1TR U196 ( .A(n222), .B(n229), .Y(n131) );
  NOR2X1TR U197 ( .A(n221), .B(n229), .Y(n130) );
  NOR2X1TR U198 ( .A(n219), .B(n214), .Y(n128) );
  NOR2X1TR U199 ( .A(n218), .B(n214), .Y(n127) );
  NOR2X1TR U200 ( .A(n217), .B(n214), .Y(n126) );
  NOR2X1TR U201 ( .A(n216), .B(n214), .Y(n125) );
  NOR2X1TR U202 ( .A(n223), .B(n228), .Y(n124) );
  NOR2X1TR U203 ( .A(n222), .B(n228), .Y(n123) );
  NOR2X1TR U204 ( .A(n211), .B(n228), .Y(n122) );
  NOR2X1TR U205 ( .A(n219), .B(n209), .Y(n120) );
  NOR2X1TR U206 ( .A(n218), .B(n209), .Y(n119) );
  NOR2X1TR U207 ( .A(n217), .B(n209), .Y(n118) );
  NOR2X1TR U208 ( .A(n216), .B(n209), .Y(n117) );
  NOR2X1TR U209 ( .A(n223), .B(n227), .Y(n116) );
  NOR2X1TR U210 ( .A(n213), .B(n227), .Y(n115) );
  NOR2X1TR U211 ( .A(n205), .B(n206), .Y(n112) );
  NOR2X1TR U212 ( .A(n203), .B(n206), .Y(n111) );
  NOR2X1TR U213 ( .A(n204), .B(n206), .Y(n110) );
  NOR2X1TR U214 ( .A(n200), .B(n206), .Y(n109) );
  NOR2X1TR U215 ( .A(n223), .B(n226), .Y(n108) );
  NOR2X1TR U216 ( .A(n213), .B(n226), .Y(n107) );
  NOR2X1TR U217 ( .A(n211), .B(n226), .Y(n106) );
  NOR2X1TR U218 ( .A(n215), .B(n226), .Y(n105) );
  NOR2X1TR U219 ( .A(n205), .B(n202), .Y(n104) );
  NOR2X1TR U220 ( .A(n203), .B(n202), .Y(n103) );
  NOR2X1TR U221 ( .A(n204), .B(n202), .Y(n102) );
  NOR2X1TR U222 ( .A(n200), .B(n202), .Y(n101) );
  NOR2X1TR U223 ( .A(n212), .B(n225), .Y(n100) );
endmodule


module mult ( clk, a, b, result, resetn );
  input [7:0] a;
  input [7:0] b;
  output [15:0] result;
  input clk, resetn;
  wire   a_int_7_0, a_int_6_0, a_int_5_0, a_int_4_0, a_int_3_0, a_int_2_0,
         a_int_1_0, a_int_0_0, b_int_7_0, b_int_6_0, b_int_5_0, b_int_4_0,
         b_int_3_0, b_int_2_0, b_int_1_0, b_int_0_0, anything_15_0,
         anything_14_0, anything_13_0, anything_12_0, anything_11_0,
         anything_10_0, anything_9_0, anything_8_0, anything_7_0, anything_6_0,
         anything_5_0, anything_4_0, anything_3_0, anything_2_0, anything_1_0,
         anything_0_0;

  mult_DW_mult_uns_0 mult_27 ( .a({a_int_7_0, a_int_6_0, a_int_5_0, a_int_4_0, 
        a_int_3_0, a_int_2_0, a_int_1_0, a_int_0_0}), .b({b_int_7_0, b_int_6_0, 
        b_int_5_0, b_int_4_0, b_int_3_0, b_int_2_0, b_int_1_0, b_int_0_0}), 
        .product({anything_15_0, anything_14_0, anything_13_0, anything_12_0, 
        anything_11_0, anything_10_0, anything_9_0, anything_8_0, anything_7_0, 
        anything_6_0, anything_5_0, anything_4_0, anything_3_0, anything_2_0, 
        anything_1_0, anything_0_0}) );
  DFFRX2TR b_int_reg_7_0 ( .D(b[7]), .CK(clk), .RN(resetn), .Q(b_int_7_0) );
  DFFRX2TR b_int_reg_6_0 ( .D(b[6]), .CK(clk), .RN(resetn), .Q(b_int_6_0) );
  DFFRX2TR b_int_reg_5_0 ( .D(b[5]), .CK(clk), .RN(resetn), .Q(b_int_5_0) );
  DFFRX2TR b_int_reg_4_0 ( .D(b[4]), .CK(clk), .RN(resetn), .Q(b_int_4_0) );
  DFFRX2TR b_int_reg_3_0 ( .D(b[3]), .CK(clk), .RN(resetn), .Q(b_int_3_0) );
  DFFRX2TR a_int_reg_7_0 ( .D(a[7]), .CK(clk), .RN(resetn), .Q(a_int_7_0) );
  DFFRX2TR a_int_reg_6_0 ( .D(a[6]), .CK(clk), .RN(resetn), .Q(a_int_6_0) );
  DFFRX2TR a_int_reg_5_0 ( .D(a[5]), .CK(clk), .RN(resetn), .Q(a_int_5_0) );
  DFFRX2TR a_int_reg_4_0 ( .D(a[4]), .CK(clk), .RN(resetn), .Q(a_int_4_0) );
  EDFFX2TR result_int_reg_2_0 ( .D(anything_2_0), .E(resetn), .CK(clk), .Q(
        result[2]) );
  EDFFX2TR result_int_reg_1_0 ( .D(anything_1_0), .E(resetn), .CK(clk), .Q(
        result[1]) );
  EDFFX2TR result_int_reg_0_0 ( .D(anything_0_0), .E(resetn), .CK(clk), .Q(
        result[0]) );
  EDFFX2TR result_int_reg_13_0 ( .D(anything_13_0), .E(resetn), .CK(clk), .Q(
        result[13]) );
  EDFFX2TR result_int_reg_12_0 ( .D(anything_12_0), .E(resetn), .CK(clk), .Q(
        result[12]) );
  EDFFX2TR result_int_reg_11_0 ( .D(anything_11_0), .E(resetn), .CK(clk), .Q(
        result[11]) );
  EDFFX2TR result_int_reg_10_0 ( .D(anything_10_0), .E(resetn), .CK(clk), .Q(
        result[10]) );
  EDFFX2TR result_int_reg_7_0 ( .D(anything_7_0), .E(resetn), .CK(clk), .Q(
        result[7]) );
  EDFFX2TR result_int_reg_6_0 ( .D(anything_6_0), .E(resetn), .CK(clk), .Q(
        result[6]) );
  EDFFX2TR result_int_reg_5_0 ( .D(anything_5_0), .E(resetn), .CK(clk), .Q(
        result[5]) );
  EDFFX2TR result_int_reg_4_0 ( .D(anything_4_0), .E(resetn), .CK(clk), .Q(
        result[4]) );
  EDFFX2TR result_int_reg_3_0 ( .D(anything_3_0), .E(resetn), .CK(clk), .Q(
        result[3]) );
  EDFFX2TR result_int_reg_9_0 ( .D(anything_9_0), .E(resetn), .CK(clk), .Q(
        result[9]) );
  EDFFX2TR result_int_reg_8_0 ( .D(anything_8_0), .E(resetn), .CK(clk), .Q(
        result[8]) );
  EDFFX1TR result_int_reg_15_0 ( .D(anything_15_0), .E(resetn), .CK(clk), .Q(
        result[15]) );
  EDFFX1TR result_int_reg_14_0 ( .D(anything_14_0), .E(resetn), .CK(clk), .Q(
        result[14]) );
  DFFRX2TR a_int_reg_2_0 ( .D(a[2]), .CK(clk), .RN(resetn), .Q(a_int_2_0) );
  DFFRX2TR a_int_reg_3_0 ( .D(a[3]), .CK(clk), .RN(resetn), .Q(a_int_3_0) );
  DFFRX2TR a_int_reg_1_0 ( .D(a[1]), .CK(clk), .RN(resetn), .Q(a_int_1_0) );
  DFFRX2TR b_int_reg_1_0 ( .D(b[1]), .CK(clk), .RN(resetn), .Q(b_int_1_0) );
  DFFRX2TR b_int_reg_2_0 ( .D(b[2]), .CK(clk), .RN(resetn), .Q(b_int_2_0) );
  DFFSX2TR b_int_reg_0_0 ( .D(b[0]), .CK(clk), .SN(resetn), .Q(b_int_0_0) );
  DFFRX2TR a_int_reg_0_0 ( .D(a[0]), .CK(clk), .RN(resetn), .Q(a_int_0_0) );
endmodule

