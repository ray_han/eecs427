//EESC427 TUTORIAL # FALL 2009
//LAST EDITED SEPT 09 2009

module mult( clk,resetn,scan_in,scan_en,cen_1,Vref,scan_out);
   input         clk,resetn,scan_in,scan_en,cen_1;
   input   [31:0]Vref;
   
   output reg scan_out;
   
       
always @(posedge clk or negedge resetn)
  begin
     if(!cen_1 || scan_en) begin
	scan_out=Vref[31]+Vref[30]+Vref[29]+Vref[28]+Vref[27]+Vref[26]+Vref[25]+Vref[24]+Vref[23]+Vref[22]+Vref[21]+Vref[20]+Vref[19]+Vref[18]+Vref[17]+Vref[16]+Vref[31]+Vref[31]+Vref[31]+Vref[31]+Vref[31]+Vref[31]+Vref[31]+Vref[31]+Vref[31]+Vref[31]+Vref[31]+Vref[31]+Vref[31]+Vref[31]+Vref[31]+
     end
     else begin
	Vref  <= 0;
     end     
  end // always @ (posedge clk or negedge resetn)
   
 
endmodule // mult
