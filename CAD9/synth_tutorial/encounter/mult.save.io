######################################################
#                                                    #
#  Cadence Design Systems, Inc.                      #
#  Cadence(R) Encounter(TM) IO Assignments           #
#                                                    #
######################################################

Version: 2




Offset: 27.8000
Pin: result[15] N 2 0.2000 0.4000
Offset: 29.8000
Pin: b[6] N 2 0.2000 0.4000
Offset: 48.2000
Pin: b[7] N 2 0.2000 0.4000
Offset: 50.6000
Pin: result[14] N 2 0.2000 0.4000
Offset: 52.6000
Pin: a[4] N 2 0.2000 0.4000
Offset: 53.4000
Pin: a[7] N 2 0.2000 0.4000
Offset: 54.2000
Pin: result[13] N 2 0.2000 0.4000
Offset: 21.0000
Pin: result[5] W 3 0.2000 0.4000
Offset: 28.2000
Pin: result[6] W 3 0.2000 0.4000
Offset: 31.0000
Pin: a[2] W 3 0.2000 0.4000
Offset: 33.8000
Pin: a[3] W 3 0.2000 0.4000
Offset: 34.6000
Pin: b[3] W 3 0.2000 0.4000
Offset: 36.6000
Pin: result[8] W 3 0.2000 0.4000
Offset: 38.2000
Pin: b[4] W 3 0.2000 0.4000
Offset: 48.2000
Pin: a[6] W 3 0.2000 0.4000
Offset: 58.2000
Pin: result[11] W 3 0.2000 0.4000
Offset: 64.2000
Pin: result[12] W 3 0.2000 0.4000
Offset: 67.0000
Pin: b[5] W 3 0.2000 0.4000
Offset: 16.6000
Pin: result[4] S 2 0.2000 0.4000
Offset: 17.4000
Pin: result[3] S 2 0.2000 0.4000
Offset: 27.4000
Pin: result[2] S 2 0.2000 0.4000
Offset: 35.8000
Pin: a[0] S 2 0.2000 0.4000
Offset: 41.0000
Pin: clk S 2 0.2000 0.4000
Offset: 41.8000
Pin: resetn S 2 0.2000 0.4000
Offset: 44.6000
Pin: b[0] S 2 0.2000 0.4000
Offset: 48.6000
Pin: result[0] S 2 0.2000 0.4000
Offset: 54.6000
Pin: a[1] S 2 0.2000 0.4000
Offset: 57.4000
Pin: b[1] S 2 0.2000 0.4000
Offset: 58.2000
Pin: result[1] S 2 0.2000 0.4000
Offset: 26.6000
Pin: b[2] E 3 0.2000 0.4000
Offset: 29.4000
Pin: result[7] E 3 0.2000 0.4000
Offset: 45.4000
Pin: a[5] E 3 0.2000 0.4000
Offset: 49.8000
Pin: result[10] E 3 0.2000 0.4000
Offset: 57.0000
Pin: result[9] E 3 0.2000 0.4000
