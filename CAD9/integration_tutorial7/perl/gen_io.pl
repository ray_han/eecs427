#!/usr/bin/perl

# This scrip would generate the .io file needed by the first encounter 
# based on .raw file

$inputFile = shift;
$outputFile = shift;
$outputFile = ">".$outputFile;

$direction   = -1;
$east_count  = -1;
$west_count  = -1;
$north_count = -1;
$south_count = -1;
$dir_char    ="E";
$dir_count   =  0;
$mydirection = 0;


open(IN, $inputFile);
open(OUT, $outputFile);

@directionlist= qw(RIGHT TOP LEFT BOTTOM DONE);

$directionkey{"RIGHT"}  = "E";
$directionkey{"TOP"}    = "N";
$directionkey{"LEFT"}   = "W";    
$directionkey{"BOTTOM"} = "S";

printf OUT "##############################################\n";
printf OUT "# This .io file is generated using gen_io.pl #\n";
printf OUT "# version 0.1.0                              #\n";
printf OUT "##############################################\n";
printf OUT "\nVersion: 2\n";

$direction = "RIGHT";      # default PAD direction
$top_name  = "TOP_LEVEL";  # default top_name
$io_name   = "IOPAD_INST"; # default io_name

foreach $line(<IN>) {
    chop($line);                # remove tailing newline
    $line =~ s/^(\s+)//; #remove all space in front
    $line =~ s/(\s+)/ /; #reduce all spacing to 1 
    $line =~ s/(.*)\#(.*)/$1/; #strip all comment
    
    @line = split(/\s/,$line);
    
    
    if($#line<0){next;}         # skip this line if all blank
    if($line =~ /^\s*\#/){next;} # skip this line since comment found
    
    #check if the top name is defined.
    if($line[0] =~ /TOP_NAME/){$top_name = $line[1]; next;}
    
    #check if the io name is defined.
    if($line[0] =~ /IO_NAME/){
	$io_name = $line[1]; 
	$io_name = $io_name."0";
	next;
    }
    
    #check if the core name is defined.
    if($line[0] =~ /CORE_NAME/){$core_name = $line[1]; next;}
    
    
    #check if changing PAD direction
    if($line[0] =~ /DIRECTION/){$direction = $line[1]; next;}



    $line=~/[IOP]_(.*)/;
    @line =split(/\#/,$line);
    $pad = @line[0];
    
    $comment = "\#".@line[1];

   CASE3:{
	($direction =~ "RIGHT")&& do{
	    $east_count++;
            $dir_char="E";
	    $dir_count = $east_count;
	    last CASE3;
	};
	($direction =~ "LEFT")&& do{
	    $west_count++;
            $dir_char="W";
	    $dir_count = $west_count;
	    last CASE3;
	};
	($direction =~ "TOP")&& do{
	    $north_count++;
            $dir_char="N";
	    $dir_count = $north_count;
	    last CASE3;
	};
	($direction =~ "BOTTOM")&& do{
	    $south_count++;
            $dir_char="S";
	    $dir_count = $south_count;
	    last CASE3;
	};
    } #end of case3
      $pad =~ s/\s+//;
      $pad_name = $pad."_".$dir_char.$dir_count;

#  printf OUT "$#line: \[0\]: @line[0], \[1\]: @line[1]:$pad_name\n";
  printf OUT "Pad:  $io_name\/P$pad_name  $directionkey{$direction}  $comment\n";

} #end of foreach

  printf OUT "Pad:  PCORNER2 SE PCORNER\n";
  printf OUT "Pad:  PCORNER0 NE PCORNER\n";
  printf OUT "Pad:  PCORNER3 NW PCORNER\n";
  printf OUT "Pad:  PCORNER1 SW PCORNER\n";

  printf OUT "# TOP_LEVEL: $top_name\n";
  printf OUT "# IO_NAME: $io_name\n";




  close(IN);
  close(OUT);



