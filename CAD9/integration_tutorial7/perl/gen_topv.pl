#!/usr/bin/perl

# This scrip would generate the top level .verilog file needed 
# by the first encounter based on .raw file

$inputFile = shift;
$outputFile = shift;
$outputFile = ">".$outputFile;

open(IN, $inputFile);
open(OUT, $outputFile);

#open(IN, "padplace.raw");
#open(OUT, ">top_level.v");

@directionlist= qw(RIGHT TOP LEFT BOTTOM DONE);

$directionkey{"RIGHT"}  = "E";
$directionkey{"TOP"}    = "N";
$directionkey{"LEFT"}   = "W";    
$directionkey{"BOTTOM"} = "S";

printf OUT "\/\/\###################################################\n";
printf OUT "\/\/\# This .v file is generated using gen_topv.pl     #\n";
printf OUT "\/\/\# version 0.1.0                                   #\n";
printf OUT "\/\/\# Last Updated: Oct. 20, 2005                     #\n";
printf OUT "\/\/\###################################################\n";
printf OUT "\n\n";

$pad_in    = "padin";
$pad_out   = "padout";
$direction = "RIGHT";      # default PAD direction
$top_name  = "TOP_LEVEL";  # default top_name
$io_name   = "IOPAD_INST"; # default io_name
$core_name = "FIR";        # default core_name

$east_count = 0; $north_count = 0; $west_count = 0; $south_count = 0;
@east  = ();
@north = ();
@west  = ();
@south = ();
@array = ();
@direction = ();
$count = 0;
%vector_max = 0; %vector_min = 0;

foreach $line(<IN>){
  chop($line);                  # remove tailing newline
   $line =~ s/(\t+)//;
  $line =~ s/(.*)\#(.*)/$1/;
  print "$line,\n";
  @line = split(/ /,$line);

  if($#line<0){next; }          # skip this line if all blank

  if($line=~/^\s*#/){next;  }   # skip this line since comment found

  #check if the top name is defined.
  if(@line[0] =~ /\s*TOP_NAME/) {$top_name = @line[1]; next;}

  #check if the io name is defined.
  if(@line[0] =~ /\s*IO_NAME/) {$io_name = @line[1]; next;}

  #check if the core name is defined.
  if(@line[0] =~ /\s*CORE_NAME/){$core_name = @line[1]; next;}

  #check if changing PAD direction
  if(@line[0] =~ /\s*DIRECTION/){$direction = @line[1]; next;}

  $line=~/[IOP]_(.*)/;
  $pad_name = $line;
  $signal_name = $1;
  
  #check to see if this is a vector
  if( $signal_name =~ /(.*)__(\d*)$/){  #check to see if the signal end w/ __##
    $signal_name =$1;
    $bit_num = $2;
    

    if(!(exists($vector_min{$signal_name}))){
      $vector_min{$signal_name} = $bit_num;
      $vector_max{$signal_name} = $bit_num;      

    }elsif($bit_num>$vector_max{$signal_name}){
      $vector_max{$signal_name} = $bit_num;
    }elsif($bit_num<$vector_min{$signal_name}){
      $vector_min{$signal_name} = $bit_num
    }

 } #end if

#  printf OUT "$#line: \[0\]: @line[0], \[1\]: @line[1]:$pad_name\n";
#  printf OUT "Pad:  $io_name\/P$signal_name  $directionkey{$direction}\n";

     @array[$count] = $pad_name ;
     @direction[$count] = $direction ; 
     $count++;

 CASE:{
    ($direction =~ "RIGHT")&& do{
      @east[$east_count] = $pad_name;
      $east_count++;
      last CASE;
    };
    ($direction =~ "LEFT")&& do{
      @west[$west_count] = $pad_name;
      $west_count++;
      last CASE;
    };
    ($direction =~ "TOP")&& do{
      @north[$north_count] = $pad_name;
      $north_count++;
      last CASE;
    };
    ($direction =~ "BOTTOM")&& do{
      @south[$south_count] = $pad_name;
      $south_count++;
      last CASE;
    };
  } #end of CASE
} #end of foreach

#printf OUT "@east\n"



printf OUT "module $top_name(\n";
  printf OUT "        DVDD, DVSS,\n";
  &define_module_in_out(@array);

printf OUT ");\n";

  printf OUT "input DVDD;\n";
  printf OUT "input DVSS;\n";

printf OUT "\/\/ Input Ouput declaration\n\n";
  &define_in_out(@array);

# &define_in_out(@east);
# &define_in_out(@west);
# &define_in_out(@south);
# &define_in_out(@north);


printf OUT "\n\/\/ PAD INSTANCE INSTANTIATION\n\n";

&print_core_body();

printf OUT "\n\n";

&print_pad_body();

printf OUT "endmodule\n";

######################################################################
sub define_module_in_out{
  my @array = @_;
  my $pad;
  my $signal_name;
  my $init = 1;
  my %vector_hash =(); 
  my $mydirection = 0;
  my $count = 0;
  for($count=0;$count<=$#array;$count++){
      
      $pad = @array[$count];
      if($pad=~/^P_(.*)/){next;}  #skip power
      
      $pad=~/[IOP]_(.*)/;
      $signal_name = $1;    
      #if a bus if found
      if($signal_name =~ /(.*)__(\d*)$/){
	  $signal_name =$1;
	  $pad =~ /(.*)__(\d*)$/;
	  $pad =  $1; 
	  if(!(exists($vector_hash{$signal_name}))){
	      $vector_hash{$signal_name} = 1; #create an entry so wont declear twice
	  }else{next; }
      }
      if($init){ $init = 0;
	     }else{ printf OUT ",\n";
		}      
      $direction = @direction[$count];
#       print " $count: pad: $pad, direction: $direction\n";
      if($mydirection ne $direction){
# 	  print "change direction to $direction.\n";
	  printf OUT "\/\/  $direction \n";
	  $mydirection = $direction;
      }
      printf OUT "              $pad";
  }
}

sub define_in_out{
  my @array = @_;
  my %vector_hash = ();

  for($count=0;$count<=$#array;$count++){
      $pad = @array[$count];
      
      $pad=~/[IOP]_(.*)/;
      $signal_name = $1;
      if($signal_name =~ /(.*)__(\d*)/){
	  $signal_name =$1;
	  $pad =~ /(.*)__(\d*)/;
	  $pad =  $1; 
	  if(!(exists($vector_hash{$signal_name}))){
	      $vector = "\[$vector_max{$signal_name}:$vector_min{$signal_name}\]";
	      $vector_hash{$signal_name} = 1; #create an entry so wont declear twice
	  }else{next;}
      }else{
	  $vector= "";
    }
      
    CASE1: {
      ($pad =~ /^I_/) && do{ #input 
      printf OUT "         input  $vector $pad;\n";
      printf OUT "         wire $vector $signal_name;\n";
      last CASE1;
    };
    ($pad =~ /^O_/) && do{ #output 
      printf OUT "         output $vector $pad;\n";
      printf OUT "         wire  $vector $signal_name;\n";
      last CASE1;
    };
  }; #CASE
  } #foreach


} #define_in_out


sub print_core_body{
  my %vector_hash = ();

  $core_id = $core_name."0";
  printf OUT "  $core_name  $core_id(\n";
  &print_core_1d(@array);
  printf OUT ");\n";
} #end of print_core_body

sub print_core_1d{
  my @array = @_;
  my $init = 1;
  for($count=0;$count<=$#array;$count++){
     $pad = @array[$count];

    if($pad =~ /[IO]_(.*)/){
      $signal_name = $1;
      if($signal_name =~ /(.*)__(\d*)/){
	$signal_name = $1;
	$bit_num = $2;
	if(exists($vector_hash{$signal_name})){next;}
	else{$vector_hash{$signal_name} = 1;}
      }
      if($init){ $init = 0;}
      else{ printf OUT ",\n";}
      
      if($signal_name =~ /(.*)__(\d*)$/){
	$signal_name =$1;
	$pad =~ /(.*)__(\d*)$/;
	 $pad =  $1;
	$bit_num = $2;
      }
      printf OUT "     .$signal_name($signal_name)";    
    }
  } #foreach
} #print_core_1d
 

sub print_pad_body{
  %vector_hash = ();

  $io_id = $io_name."0";
  printf OUT "  $io_name  $io_id(\n";
  printf OUT "     .DVDD(DVDD), .DVSS(DVSS),\n";
  &print_pad_1d(@array);
  printf OUT ");\n";
} #end of print_pad_body

sub print_pad_1d{
  my @array = @_;
  my $init = 1;

  for($count=0;$count<=$#array;$count++){
     $pad = @array[$count];

#   foreach $pad (@array){
    if($pad =~ /[IO]_(.*)/){
      $signal_name = $1;

#       printf "pad: $pad, signal_name:$signal_name\n";


      #check if it is a bus
      if($signal_name =~ /(.*)__(\d*)/){
	$signal_name = $1;
	$bit_num = $2;
# 	cleanup the pad name
	$pad =~ /(.*)__(\d*)$/;
	 $pad =  $1;

	if(exists($vector_hash{$signal_name})){ next;}
	else{$vector_hash{$signal_name} = 1;}
      }
      # $init is a variable used to remove the last comma
      if($init){ $init = 0;}
      else{ printf OUT ",\n";}
      
#       if($signal_name =~ /(.*)__(\d*)$/){
# 	$signal_name =$1;
# 	$pad =~ /(.*)__(\d*)$/;
# 	 $pad =  $1;
# 	$bit_num = $2;
#       }
      printf OUT "     .$pad($pad),\n";
      printf OUT "     .$signal_name($signal_name)";    

    }
  } #foreach
} #print_pad_1d


