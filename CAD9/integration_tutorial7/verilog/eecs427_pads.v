//###################################################
//# This .v file is generated using gen_verilog.pl  #
//# version 0.1.0                                   #
//###################################################


`timescale 1 ns/1 ps
`celldefine
module eecs427_pads(
         DVDD, DVSS, 
//  RIGHT 
              I_clk ,
              clk ,
              I_scan_en ,
              scan_en ,
              I_scan_in ,
              scan_in ,
              O_scan_out ,
              scan_out ,
              I_resetn ,
              resetn ,
              I_CEN_1 ,
              CEN_1 ,
              I_Vref0 ,
              Vref0 ,
              I_Vref1 ,
              Vref1 ,
              I_Vref2 ,
              Vref2 ,
//  TOP 
              I_Vref4 ,
              Vref4 ,
              I_Vref5 ,
              Vref5 ,
              I_Vref6 ,
              Vref6 ,
              I_Vref7 ,
              Vref7 ,
              I_Vref8 ,
              Vref8 ,
              I_Vref9 ,
              Vref9 ,
              I_Vref10 ,
              Vref10 ,
              I_Vref11 ,
              Vref11 ,
              I_Vref12 ,
              Vref12 ,
              I_Vref13 ,
              Vref13 ,
//  LEFT 
              I_Vref14 ,
              Vref14 ,
              I_Vref15 ,
              Vref15 ,
              I_Vref16 ,
              Vref16 ,
              I_Vref17 ,
              Vref17 ,
              I_Vref18 ,
              Vref18 ,
              I_Vref19 ,
              Vref19 ,
              I_Vref20 ,
              Vref20 ,
              I_Vref21 ,
              Vref21 ,
              I_Vref22 ,
              Vref22 ,
//  BOTTOM 
              I_Vref24 ,
              Vref24 ,
              I_Vref25 ,
              Vref25 ,
              I_Vref26 ,
              Vref26 ,
              I_Vref27 ,
              Vref27 ,
              I_Vref28 ,
              Vref28 ,
              I_Vref29 ,
              Vref29 ,
              I_Vref30 ,
              Vref30 ,
              I_Vref31 ,
              Vref31 ,
              I_Vref3 ,
              Vref3 ,
              I_Vref23 ,
              Vref23 );
// Power supply
   supply0 VSS;
   supply1 VDD;
// Input Ouput declaration

    input DVDD;
    input DVSS;
         input   I_clk ;
         output  clk ;
         input   I_scan_en ;
         output  scan_en ;
         input   I_scan_in ;
         output  scan_in ;
         output  O_scan_out ;
         input   scan_out ;
         input   I_resetn ;
         output  resetn ;
         input   I_CEN_1 ;
         output  CEN_1 ;
         input   I_Vref0 ;
         output  Vref0 ;
         input   I_Vref1 ;
         output  Vref1 ;
         input   I_Vref2 ;
         output  Vref2 ;
         input   I_Vref4 ;
         output  Vref4 ;
         input   I_Vref5 ;
         output  Vref5 ;
         input   I_Vref6 ;
         output  Vref6 ;
         input   I_Vref7 ;
         output  Vref7 ;
         input   I_Vref8 ;
         output  Vref8 ;
         input   I_Vref9 ;
         output  Vref9 ;
         input   I_Vref10 ;
         output  Vref10 ;
         input   I_Vref11 ;
         output  Vref11 ;
         input   I_Vref12 ;
         output  Vref12 ;
         input   I_Vref13 ;
         output  Vref13 ;
         input   I_Vref14 ;
         output  Vref14 ;
         input   I_Vref15 ;
         output  Vref15 ;
         input   I_Vref16 ;
         output  Vref16 ;
         input   I_Vref17 ;
         output  Vref17 ;
         input   I_Vref18 ;
         output  Vref18 ;
         input   I_Vref19 ;
         output  Vref19 ;
         input   I_Vref20 ;
         output  Vref20 ;
         input   I_Vref21 ;
         output  Vref21 ;
         input   I_Vref22 ;
         output  Vref22 ;
         input   I_Vref24 ;
         output  Vref24 ;
         input   I_Vref25 ;
         output  Vref25 ;
         input   I_Vref26 ;
         output  Vref26 ;
         input   I_Vref27 ;
         output  Vref27 ;
         input   I_Vref28 ;
         output  Vref28 ;
         input   I_Vref29 ;
         output  Vref29 ;
         input   I_Vref30 ;
         output  Vref30 ;
         input   I_Vref31 ;
         output  Vref31 ;
         input   I_Vref3 ;
         output  Vref3 ;
         input   I_Vref23 ;
         output  Vref23 ;

// PAD INSTANCE INSTANTIATION


//  RIGHT 
     PDVSS  PP_tl_dirty_VSS_E0(.DVSS(DVSS) );
     PVSS   PP_tl_clean_VSS_E1(VSS);
     PICS   PI_clk_E2(.P(I_clk), .Y(clk), .IE(1'b1));
     PICS   PI_scan_en_E3(.P(I_scan_en), .Y(scan_en), .IE(1'b1));
     PICS   PI_scan_in_E4(.P(I_scan_in), .Y(scan_in), .IE(1'b1));
     POC24A PO_scan_out_E5(.P(O_scan_out), .A(scan_out));
     PICS   PI_resetn_E6(.P(I_resetn), .Y(resetn), .IE(1'b1));
     PICS   PI_CEN_1_E7(.P(I_CEN_1), .Y(CEN_1), .IE(1'b1));
     PICS   PI_Vref0_E8(.P(I_Vref0), .Y(Vref0), .IE(1'b1));
     PICS   PI_Vref1_E9(.P(I_Vref1), .Y(Vref1), .IE(1'b1));
     PICS   PI_Vref2_E10(.P(I_Vref2), .Y(Vref2), .IE(1'b1));
     PVDD   PP_tl_clean_VDD_E11(.VDD(VDD));
     PDVDD  PP_tl_dirty_VDD_E12(.DVDD(DVDD) );

//  TOP 
     PDVSS  PP_tl_dirty_VSS_N0(.DVSS(DVSS) );
     PVSS   PP_tl_clean_VSS_N1(VSS);
     PICS   PI_Vref4_N2(.P(I_Vref4), .Y(Vref4), .IE(1'b1));
     PICS   PI_Vref5_N3(.P(I_Vref5), .Y(Vref5), .IE(1'b1));
     PICS   PI_Vref6_N4(.P(I_Vref6), .Y(Vref6), .IE(1'b1));
     PICS   PI_Vref7_N5(.P(I_Vref7), .Y(Vref7), .IE(1'b1));
     PICS   PI_Vref8_N6(.P(I_Vref8), .Y(Vref8), .IE(1'b1));
     PICS   PI_Vref9_N7(.P(I_Vref9), .Y(Vref9), .IE(1'b1));
     PICS   PI_Vref10_N8(.P(I_Vref10), .Y(Vref10), .IE(1'b1));
     PICS   PI_Vref11_N9(.P(I_Vref11), .Y(Vref11), .IE(1'b1));
     PICS   PI_Vref12_N10(.P(I_Vref12), .Y(Vref12), .IE(1'b1));
     PICS   PI_Vref13_N11(.P(I_Vref13), .Y(Vref13), .IE(1'b1));
     PVDD   PP_tl_clean_VDD_N12(.VDD(VDD));
     PDVDD  PP_tl_dirty_VDD_N13(.DVDD(DVDD) );

//  LEFT 
     PDVSS  PP_tl_dirty_VSS_W0(.DVSS(DVSS) );
     PVSS   PP_tl_clean_VSS_W1(VSS);
     PICS   PI_Vref14_W2(.P(I_Vref14), .Y(Vref14), .IE(1'b1));
     PICS   PI_Vref15_W3(.P(I_Vref15), .Y(Vref15), .IE(1'b1));
     PICS   PI_Vref16_W4(.P(I_Vref16), .Y(Vref16), .IE(1'b1));
     PICS   PI_Vref17_W5(.P(I_Vref17), .Y(Vref17), .IE(1'b1));
     PICS   PI_Vref18_W6(.P(I_Vref18), .Y(Vref18), .IE(1'b1));
     PICS   PI_Vref19_W7(.P(I_Vref19), .Y(Vref19), .IE(1'b1));
     PICS   PI_Vref20_W8(.P(I_Vref20), .Y(Vref20), .IE(1'b1));
     PICS   PI_Vref21_W9(.P(I_Vref21), .Y(Vref21), .IE(1'b1));
     PICS   PI_Vref22_W10(.P(I_Vref22), .Y(Vref22), .IE(1'b1));
     PVDD   PP_tl_clean_VDD_W11(.VDD(VDD));
     PDVDD  PP_tl_dirty_VDD_W12(.DVDD(DVDD) );

//  BOTTOM 
     PDVSS  PP_tl_dirty_VSS_S0(.DVSS(DVSS) );
     PVSS   PP_tl_clean_VSS_S1(VSS);
     PICS   PI_Vref24_S2(.P(I_Vref24), .Y(Vref24), .IE(1'b1));
     PICS   PI_Vref25_S3(.P(I_Vref25), .Y(Vref25), .IE(1'b1));
     PICS   PI_Vref26_S4(.P(I_Vref26), .Y(Vref26), .IE(1'b1));
     PICS   PI_Vref27_S5(.P(I_Vref27), .Y(Vref27), .IE(1'b1));
     PICS   PI_Vref28_S6(.P(I_Vref28), .Y(Vref28), .IE(1'b1));
     PICS   PI_Vref29_S7(.P(I_Vref29), .Y(Vref29), .IE(1'b1));
     PICS   PI_Vref30_S8(.P(I_Vref30), .Y(Vref30), .IE(1'b1));
     PICS   PI_Vref31_S9(.P(I_Vref31), .Y(Vref31), .IE(1'b1));
     PICS   PI_Vref3_S10(.P(I_Vref3), .Y(Vref3), .IE(1'b1));
     PICS   PI_Vref23_S11(.P(I_Vref23), .Y(Vref23), .IE(1'b1));
     PVDD   PP_tl_clean_VDD_S12(.VDD(VDD));
     PDVDD  PP_tl_dirty_VDD_S13(.DVDD(DVDD) );
endmodule
