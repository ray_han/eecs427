#######################################################
#                                                     
#  Encounter Command Logging File                     
#  Created on Tue Dec 11 23:48:40 2018                
#                                                     
#######################################################

#@(#)CDS: Encounter v14.27-s035_1 (64bit) 10/07/2015 12:46 (Linux 2.6.18-194.el5)
#@(#)CDS: NanoRoute v14.27-s012 NR150928-2308/14_27-UB (database version 2.30, 267.6.1) {superthreading v1.25}
#@(#)CDS: CeltIC v14.27-s018_1 (64bit) 10/06/2015 22:57:34 (Linux 2.6.18-194.el5)
#@(#)CDS: AAE 14.27-s003 (64bit) 10/07/2015 (Linux 2.6.18-194.el5)
#@(#)CDS: CTE 14.27-s022_1 (64bit) Oct  6 2015 07:33:36 (Linux 2.6.18-194.el5)
#@(#)CDS: CPE v14.27-s021
#@(#)CDS: IQRC/TQRC 14.2.2-s217 (64bit) Wed Apr 15 23:10:24 PDT 2015 (Linux 2.6.18-194.el5)

set_global _enable_mmmc_by_default_flow      $CTE::mmmc_default
suppressMessage ENCEXT-2799
set init_verilog {../verilog/eecs427_top.v ../verilog/eecs427_core.v ../verilog/eecs427_pads.v /afs/umich.edu/class/eecs427/ibm13/parts/eecs427pads/verilog/arti_cmos8rf_verilog_i_modules.v /afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/verilog/ibm13_modules.v /afs/umich.edu/class/eecs427/ibm13/parts/eecs427memories/ra1sh16x512/verilog/RA1SH16x512_module.v }
set init_top_cell eecs427_top
set init_design_netlisttype Verilog
set init_design_settop 1
set init_io_file ../verilog/eecs427_top.save.io
set init_lef_file {/afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/lef/ibm13_8lm_2thick_tech.lef /afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/lef/ibm13rvt_macros.lef /afs/umich.edu/class/eecs427/ibm13/parts/eecs427pads/lef/arti_cmos8rf_8lm_2thick_i.lef ../lef/DATAPATH_FINAL.lef /afs/umich.edu/class/eecs427/ibm13/parts/eecs427memories/ra1sh16x512/lef/RA1SH16x512.vclef}
set init_mmmc_file viewDefinition.tcl
set init_pwr_net VDD
set init_gnd_net VSS
set init_assign_buffer 0
setDesignMode -process 130
init_design
win
