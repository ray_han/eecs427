// testfile.asm
// Assembly language with LUI command

// Clear registers
.orig 0x0000

	andi 0,r0	//00	r0=0
	andi 0,r1	//01	r1=0
	andi 0,r2	//02	r2=0
	andi 0,r3	//03	r3=0
	andi 0,r4	//04	r4=0
	andi 0,r5	//05	r5=0
	andi 0,r6	//06	r6=0
	andi 0,r7	//07	r7=0
	
	addi 1,r0	//08	r0=1
	addi 2,r1	//09	r1=2
	movi 0xa,r2	//0a	r2=a
	mov r2,r3	//0b	r3=a
	add r1,r3	//0c	r3=c
	or r0,r1	//0d	r1=3
	ori 0xa,r1	//0e	r1=b
	movi 0xf,r4	//0f	r4=f
	movi 0xa,r5	//10	r5=a
	xor r5,r4  	//11	r4=5
	and r4,r5	//12	r5=0
	andi 0x18,r3	//13	r3=8
	xori 0xc,r5	//14	r5=c
	sub r3,r5	//15	r5=4
	subi 0x5,r5	//16	r5=ffff
	
	stor r0,r6	//17	1->mem(0)
	stor r1,r0	//18	b->mem(1)
	add r0,r7	//19	r7=1
	add r0,r7	//1a	r7=2
	stor r2,r7	//1b	a->mem(2)
	add r0,r7	//1c	r7=3
	stor r3,r7	//1d	8->mem(3)
	add r0,r7	//1e	r7=4
	stor r4,r7	//1f	5->mem(4)
	add r0,r7	//20	r7=5
	stor r5,r7	//21	ffff->mem(5)
	load r6,r0	//22	mem(1)=b ->r6
	add r0,r7	//23	r7=6
	stor r6,r7	//24	b->mem(6)
	
	lsh r0,r0	//25	r0=2
	lshi 0x6,r0	//26	r0=80
	lsh r5,r0	//27	r0=40 //wrong!! r0 should be 0 so change to movi 0x40 r0 to make r0=40
	
	movi 0x3,r1	//28	r1=3
	xor r7,r7	//29	r7=0
	add r0,r7	//2a	r7=40
	add r0,r7	//2b	r7=80
	add r6,r4	//2c	r4=10
	jmp r0		//2d	jump to test1
	xor r4,r4	//2e

	.orig 0x40
test1	subi 0x1,r1	//40	mem location 40
	cmpi 0x0,r1	//41	set z
	jeq r7		//42	jump to test2 on 3rd run through
	cmpi 0x0,r1	//43	set z   ///add extra one at 43
	jne r0		//44	jump to test1 twice
	xor r4,r4	//45
	.orig 0x80
test2	add r0,r7	//80	r7=c0
	addi 0x1,r5 	//81	carry set
	jcc r0		//82	don't jump to test1
	subi 0x1,r5	//83	r5=ffff				//modified
	addi 0x1,r5     //84	r5=1				//modified
	jcs r7		//85	jump to test3
	xor r4,r4	//86
	.orig 0xc0
test3	add r0,r7	//c0	r7=100
	subi 0x1,r5	//c1	r5=ffff
			//lsh r5,r5 				//from lsh r5,r5  change to following three instruction to make r5=7fff
	andi 0xff, r5	//c2	r5=00ff				//modified
	lshi 0x7,r5	//c3	r5=7f80				//modified
	addi 0x7f,7f	//c4 	r5=7fff				//modified
	addi 0x1,r5	//c5	set overflow
	jfc r0		//c6	don't jump to test1
	subi 0x1,r5	//c7	r5=7fff				//modified
	addi 0x1,r5	//c8	set overflow			//modified
	jfs r7		//c9	jump to test4
	xor r4,r4	//ca
	.orig 0x100
test4	add r0,r7	//100	r7=140
	xor r1,r1	//101	r1=0
	subi 0x1,r1	//102	r1=ffff
	cmp r1,r7	//103	set n=0, l=1
	jgt r0		//104	don't jump to test1
	cmp r7,r1	//105	set n=1, l=1			//modified
	jge r7		//106	jump to test5			//modified no higher/lower
	xor r4,r4	//107
	.orig 0x140
test5	ble j1		//140	branch to j1
	xor r4,r4	//141
	.orig 0x151
j1	blt j2		//151	branch to j2
	xor r4,r4	//152
	.orig 0x162
j2	cmp r1,r7	//162	set l=0, n=1
	//bhs j3		//163	don't branch to j3	//modified no higher/lower
	bgt j4		//163	branch to j4
	xor r4,r4	//164
	.orig 0x174
j3	xor r4,r4	//174	
j4	lshi 0x1,r7	//175	r7=280
	jal r6,r7	//176	jump to test6, set rlink to r6 	//due to imem delay half cycle than pc r6=rlink should be 178 instead of 177
	stor r4,r7	//177	r4 -> mem(280)  r4 should equal 10 not 0
	or r0,r0	//178	r0=0040
	or r1,r1	//179	r1=ed00
	or r2,r2	//17a	r2=000a
	or r3,r3	//17b	r3=0008
	or r4,r4	//17c	r4=0010		//should be 0000 in f18
	or r5,r5	//17d	r5=8000
	or r6,r6	//17e	r6=0177		//should be 178
	or r7,r7	//17f	r7=0280
	
	.orig 0x280
test6	nop	//280	mem location 280
	lui	0xed,r1	//281	r1=ed00
	jmp r6		//282	return from jal
	xor r4,r4	//283

