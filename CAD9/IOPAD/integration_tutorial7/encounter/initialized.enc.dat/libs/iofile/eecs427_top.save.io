##############################################
# This .io file is generated using gen_io.pl #
# version 0.1.0                              #
##############################################

Version: 2
Pad:  eecs427_pads0/PP_tl_dirty_VSS_E0  E  #
Pad:  eecs427_pads0/PP_tl_clean_VSS_E1  E  #
Pad:  eecs427_pads0/PI_clk_E2  E  #
Pad:  eecs427_pads0/PI_scan_en_E3  E  #
Pad:  eecs427_pads0/PI_scan_in_E4  E  #
Pad:  eecs427_pads0/PO_scan_out_E5  E  #
Pad:  eecs427_pads0/PI_resetn_E6  E  #
Pad:  eecs427_pads0/PI_CEN_1_E7  E  #
Pad:  eecs427_pads0/PI_Vref0_E8  E  #
Pad:  eecs427_pads0/PI_Vref1_E9  E  #
Pad:  eecs427_pads0/PI_Vref2_E10  E  #
Pad:  eecs427_pads0/PP_tl_clean_VDD_E11  E  #
Pad:  eecs427_pads0/PP_tl_dirty_VDD_E12  E  #
Pad:  eecs427_pads0/PP_tl_dirty_VSS_N0  N  #
Pad:  eecs427_pads0/PP_tl_clean_VSS_N1  N  #
Pad:  eecs427_pads0/PI_Vref4_N2  N  #
Pad:  eecs427_pads0/PI_Vref5_N3  N  #
Pad:  eecs427_pads0/PI_Vref6_N4  N  #
Pad:  eecs427_pads0/PI_Vref7_N5  N  #
Pad:  eecs427_pads0/PI_Vref8_N6  N  #
Pad:  eecs427_pads0/PI_Vref9_N7  N  #
Pad:  eecs427_pads0/PI_Vref10_N8  N  #
Pad:  eecs427_pads0/PI_Vref11_N9  N  #
Pad:  eecs427_pads0/PI_Vref12_N10  N  #
Pad:  eecs427_pads0/PI_Vref13_N11  N  #
Pad:  eecs427_pads0/PP_tl_clean_VDD_N12  N  #
Pad:  eecs427_pads0/PP_tl_dirty_VDD_N13  N  #
Pad:  eecs427_pads0/PP_tl_dirty_VSS_W0  W  #
Pad:  eecs427_pads0/PP_tl_clean_VSS_W1  W  #
Pad:  eecs427_pads0/PI_Vref14_W2  W  #
Pad:  eecs427_pads0/PI_Vref15_W3  W  #
Pad:  eecs427_pads0/PI_Vref16_W4  W  #
Pad:  eecs427_pads0/PI_Vref17_W5  W  #
Pad:  eecs427_pads0/PI_Vref18_W6  W  #
Pad:  eecs427_pads0/PI_Vref19_W7  W  #
Pad:  eecs427_pads0/PI_Vref20_W8  W  #
Pad:  eecs427_pads0/PI_Vref21_W9  W  #
Pad:  eecs427_pads0/PI_Vref22_W10  W  #
Pad:  eecs427_pads0/PP_tl_clean_VDD_W11  W  #
Pad:  eecs427_pads0/PP_tl_dirty_VDD_W12  W  #
Pad:  eecs427_pads0/PP_tl_dirty_VSS_S0  S  #
Pad:  eecs427_pads0/PP_tl_clean_VSS_S1  S  #
Pad:  eecs427_pads0/PI_Vref24_S2  S  #
Pad:  eecs427_pads0/PI_Vref25_S3  S  #
Pad:  eecs427_pads0/PI_Vref26_S4  S  #
Pad:  eecs427_pads0/PI_Vref27_S5  S  #
Pad:  eecs427_pads0/PI_Vref28_S6  S  #
Pad:  eecs427_pads0/PI_Vref29_S7  S  #
Pad:  eecs427_pads0/PI_Vref30_S8  S  #
Pad:  eecs427_pads0/PI_Vref31_S9  S  #
Pad:  eecs427_pads0/PI_Vref3_S10  S  #
Pad:  eecs427_pads0/PI_Vref23_S11  S  #
Pad:  eecs427_pads0/PP_tl_clean_VDD_S12  S  #
Pad:  eecs427_pads0/PP_tl_dirty_VDD_S13  S  #
Pad:  PCORNER2 SE PCORNER
Pad:  PCORNER0 NE PCORNER
Pad:  PCORNER3 NW PCORNER
Pad:  PCORNER1 SW PCORNER
# TOP_LEVEL: eecs427_top
# IO_NAME: eecs427_pads0
