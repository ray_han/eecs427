//###################################################
//# This .v file is generated using gen_topv.pl     #
//# version 0.1.0                                   #
//# Last Updated: Oct. 20, 2005                     #
//###################################################


module eecs427_top(
        DVDD, DVSS,
//  RIGHT 
              I_clk,
              I_scan_en,
              I_scan_in,
              O_scan_out,
              I_resetn,
              I_CEN_1,
              I_Vref0,
              I_Vref1,
              I_Vref2,
//  TOP 
              I_Vref4,
              I_Vref5,
              I_Vref6,
              I_Vref7,
              I_Vref8,
              I_Vref9,
              I_Vref10,
              I_Vref11,
              I_Vref12,
              I_Vref13,
//  LEFT 
              I_Vref14,
              I_Vref15,
              I_Vref16,
              I_Vref17,
              I_Vref18,
              I_Vref19,
              I_Vref20,
              I_Vref21,
              I_Vref22,
//  BOTTOM 
              I_Vref24,
              I_Vref25,
              I_Vref26,
              I_Vref27,
              I_Vref28,
              I_Vref29,
              I_Vref30,
              I_Vref31,
              I_Vref3,
              I_Vref23);
input DVDD;
input DVSS;
// Input Ouput declaration

         input   I_clk;
         wire  clk;
         input   I_scan_en;
         wire  scan_en;
         input   I_scan_in;
         wire  scan_in;
         output  O_scan_out;
         wire   scan_out;
         input   I_resetn;
         wire  resetn;
         input   I_CEN_1;
         wire  CEN_1;
         input   I_Vref0;
         wire  Vref0;
         input   I_Vref1;
         wire  Vref1;
         input   I_Vref2;
         wire  Vref2;
         input   I_Vref4;
         wire  Vref4;
         input   I_Vref5;
         wire  Vref5;
         input   I_Vref6;
         wire  Vref6;
         input   I_Vref7;
         wire  Vref7;
         input   I_Vref8;
         wire  Vref8;
         input   I_Vref9;
         wire  Vref9;
         input   I_Vref10;
         wire  Vref10;
         input   I_Vref11;
         wire  Vref11;
         input   I_Vref12;
         wire  Vref12;
         input   I_Vref13;
         wire  Vref13;
         input   I_Vref14;
         wire  Vref14;
         input   I_Vref15;
         wire  Vref15;
         input   I_Vref16;
         wire  Vref16;
         input   I_Vref17;
         wire  Vref17;
         input   I_Vref18;
         wire  Vref18;
         input   I_Vref19;
         wire  Vref19;
         input   I_Vref20;
         wire  Vref20;
         input   I_Vref21;
         wire  Vref21;
         input   I_Vref22;
         wire  Vref22;
         input   I_Vref24;
         wire  Vref24;
         input   I_Vref25;
         wire  Vref25;
         input   I_Vref26;
         wire  Vref26;
         input   I_Vref27;
         wire  Vref27;
         input   I_Vref28;
         wire  Vref28;
         input   I_Vref29;
         wire  Vref29;
         input   I_Vref30;
         wire  Vref30;
         input   I_Vref31;
         wire  Vref31;
         input   I_Vref3;
         wire  Vref3;
         input   I_Vref23;
         wire  Vref23;

// PAD INSTANCE INSTANTIATION

  eecs427_core  eecs427_core0(
     .clk(clk),
     .scan_en(scan_en),
     .scan_in(scan_in),
     .scan_out(scan_out),
     .resetn(resetn),
     .CEN_1(CEN_1),
     .Vref0(Vref0),
     .Vref1(Vref1),
     .Vref2(Vref2),
     .Vref4(Vref4),
     .Vref5(Vref5),
     .Vref6(Vref6),
     .Vref7(Vref7),
     .Vref8(Vref8),
     .Vref9(Vref9),
     .Vref10(Vref10),
     .Vref11(Vref11),
     .Vref12(Vref12),
     .Vref13(Vref13),
     .Vref14(Vref14),
     .Vref15(Vref15),
     .Vref16(Vref16),
     .Vref17(Vref17),
     .Vref18(Vref18),
     .Vref19(Vref19),
     .Vref20(Vref20),
     .Vref21(Vref21),
     .Vref22(Vref22),
     .Vref24(Vref24),
     .Vref25(Vref25),
     .Vref26(Vref26),
     .Vref27(Vref27),
     .Vref28(Vref28),
     .Vref29(Vref29),
     .Vref30(Vref30),
     .Vref31(Vref31),
     .Vref3(Vref3),
     .Vref23(Vref23));


  eecs427_pads  eecs427_pads0(
     .DVDD(DVDD), .DVSS(DVSS),
     .I_clk(I_clk),
     .clk(clk),
     .I_scan_en(I_scan_en),
     .scan_en(scan_en),
     .I_scan_in(I_scan_in),
     .scan_in(scan_in),
     .O_scan_out(O_scan_out),
     .scan_out(scan_out),
     .I_resetn(I_resetn),
     .resetn(resetn),
     .I_CEN_1(I_CEN_1),
     .CEN_1(CEN_1),
     .I_Vref0(I_Vref0),
     .Vref0(Vref0),
     .I_Vref1(I_Vref1),
     .Vref1(Vref1),
     .I_Vref2(I_Vref2),
     .Vref2(Vref2),
     .I_Vref4(I_Vref4),
     .Vref4(Vref4),
     .I_Vref5(I_Vref5),
     .Vref5(Vref5),
     .I_Vref6(I_Vref6),
     .Vref6(Vref6),
     .I_Vref7(I_Vref7),
     .Vref7(Vref7),
     .I_Vref8(I_Vref8),
     .Vref8(Vref8),
     .I_Vref9(I_Vref9),
     .Vref9(Vref9),
     .I_Vref10(I_Vref10),
     .Vref10(Vref10),
     .I_Vref11(I_Vref11),
     .Vref11(Vref11),
     .I_Vref12(I_Vref12),
     .Vref12(Vref12),
     .I_Vref13(I_Vref13),
     .Vref13(Vref13),
     .I_Vref14(I_Vref14),
     .Vref14(Vref14),
     .I_Vref15(I_Vref15),
     .Vref15(Vref15),
     .I_Vref16(I_Vref16),
     .Vref16(Vref16),
     .I_Vref17(I_Vref17),
     .Vref17(Vref17),
     .I_Vref18(I_Vref18),
     .Vref18(Vref18),
     .I_Vref19(I_Vref19),
     .Vref19(Vref19),
     .I_Vref20(I_Vref20),
     .Vref20(Vref20),
     .I_Vref21(I_Vref21),
     .Vref21(Vref21),
     .I_Vref22(I_Vref22),
     .Vref22(Vref22),
     .I_Vref24(I_Vref24),
     .Vref24(Vref24),
     .I_Vref25(I_Vref25),
     .Vref25(Vref25),
     .I_Vref26(I_Vref26),
     .Vref26(Vref26),
     .I_Vref27(I_Vref27),
     .Vref27(Vref27),
     .I_Vref28(I_Vref28),
     .Vref28(Vref28),
     .I_Vref29(I_Vref29),
     .Vref29(Vref29),
     .I_Vref30(I_Vref30),
     .Vref30(Vref30),
     .I_Vref31(I_Vref31),
     .Vref31(Vref31),
     .I_Vref3(I_Vref3),
     .Vref3(Vref3),
     .I_Vref23(I_Vref23),
     .Vref23(Vref23));
endmodule
