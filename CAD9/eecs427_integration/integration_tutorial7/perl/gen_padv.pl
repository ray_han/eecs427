#!/usr/bin/perl 

# This scrip would generate the .verilog file needed by the first encounter 
# based on .raw file
$inputFile = shift;
$outputFile = shift;
$outputFile = ">".$outputFile;


open(IN, $inputFile);
open(OUT, $outputFile);

#open(IN, "padplace.raw");
#open(OUT, ">padplace.v");

@directionlist= qw(RIGHT TOP LEFT BOTTOM DONE);

$directionkey{"RIGHT"}  = "E";
$directionkey{"TOP"}    = "N";
$directionkey{"LEFT"}   = "W";    
$directionkey{"BOTTOM"} = "S";

printf OUT "\/\/###################################################\n";
printf OUT "\/\/# This .v file is generated using gen_verilog.pl  #\n";
printf OUT "\/\/# version 0.1.0                                   #\n";
printf OUT "\/\/###################################################\n";
printf OUT "\n\n";

$pad_in    = "padin";
$pad_out   = "padout";
$direction = "RIGHT";      # default PAD direction
$top_name  = "TOP_LEVEL";  # default top_name
$io_name   = "IOPAD_INST"; # default io_name

$east_count = 0; $north_count = 0; $west_count = 0; $south_count = 0;
@east  = ();
@north = ();
@west  = ();
@south = ();
@array = ();
@direction = ();
%vector_max = 0; %vector_min = 0;

foreach $line(<IN>) {
    chop($line);                # remove tailing newline
    $line =~ s/^(\s+)//; #remove all space in front
    $line =~ s/(\s+)/ /; #reduce all spacing to 1 
    $line =~ s/(.*)\#(.*)/$1/; #strip all comment
    
    @line = split(/\s/,$line);
#      print "$line[0]\n";
    if($#line<0){next;}         # skip this line if all blank
    if($line =~ /^\s*\#/){next;} # skip this line since comment found
    
    #check if the top name is defined.
    if($line[0] =~ /TOP_NAME/){$top_name = $line[1]; next;}
    
    #check if the io name is defined.
    if($line[0] =~ /IO_NAME/){$io_name = $line[1]; next;}

    #check if the core name is defined.
    if($line[0] =~ /CORE_NAME/){$core_name = $line[1]; next;}

    #check if changing PAD direction
    if($line[0] =~ /DIRECTION/){$direction = $line[1]; next;}

  $line=~/[IOP]_(.*)/;
  $pad_name = $line;
  $signal_name = $1;
  
  #check to see if this is a vector
  if( $signal_name =~ /(.*)__(\d*)/){  #check to see if the signal end w/ __##
    $signal_name =$1;
    $bit_num = $2;
    
    #  printf OUT "vector: $line found\n";
    if(!(exists($vector_min{$signal_name}))){
#      printf OUT "vector: $signal_name doesn't exist in record: $bit_num\n";
      $vector_min{$signal_name} = $bit_num;
      $vector_max{$signal_name} = $bit_num;      
#printf OUT "signal_name: $signal_name, vector_max: $vector_max{$signal_name}\n";
    }elsif($bit_num>$vector_max{$signal_name}){
      $vector_max{$signal_name} = $bit_num;
    }elsif($bit_num<$vector_min{$signal_name}){
      $vector_min{$signal_name} = $bit_num
    }
#printf OUT "signal_name: $signal_name, vector_max: $vector_max{$signal_name}, vector_min:$vector_min{$signal_name}\n";
  }

#  printf OUT "$#line: \[0\]: @line[0], \[1\]: @line[1]:$pad_name\n";
#  printf OUT "Pad:  $io_name\/P$signal_name  $directionkey{$direction}\n";


     @array[$count] = $pad_name ;
     @direction[$count] = $direction ; 
     $count++;

 CASE:{
    ($direction =~ "RIGHT")&& do{
      @east[$east_count] = $pad_name;
      $east_count++;
      last CASE;
    };
    ($direction =~ "LEFT")&& do{
      @west[$west_count] = $pad_name;
      $west_count++;
      last CASE;
    };
    ($direction =~ "TOP")&& do{
      @north[$north_count] = $pad_name;
      $north_count++;
      last CASE;
    };
    ($direction =~ "BOTTOM")&& do{
      @south[$south_count] = $pad_name;
      $south_count++;
      last CASE;
    };
  } #end of CASE
} #end of foreach

#printf OUT "@east\n"

#     print "top_name: $top_name\n";
#     print "core_name: $core_name\n";
#     print "io_name: $io_name\n";

printf OUT "`timescale 1 ns/1 ps\n";
printf OUT "`celldefine\n";
printf OUT "module $io_name(\n";
printf OUT "         DVDD, DVSS, \n";
  &define_module_in_out(@array);
printf OUT ");\n";

printf OUT "\/\/ Power supply\n";
printf OUT "   supply0 VSS;\n";
printf OUT "   supply1 VDD;\n";
printf OUT "\/\/ Input Ouput declaration\n\n";
printf OUT "    input DVDD;\n";
printf OUT "    input DVSS;\n";
&define_in_out(@array);


printf OUT "\n\/\/ PAD INSTANCE INSTANTIATION\n\n";

&print_body(@array);

# printf OUT "\/\/ EAST\n";
# &print_body(@east);
# printf OUT "\n\/\/ WEST\n";
# &print_body(@west);
# printf OUT "\n\/\/ SOUTh\n";
# &print_body(@south);
# printf OUT "\n\/\/ NORTH\n";
# &print_body(@north);

printf OUT "endmodule\n";

#printf OUT "%vector\n";

######################################################################
sub define_module_in_out{
  my @array = @_;
  my $pad;
  my $signal_name;
  my $init = 1;
  my %vector_hash =(); 
  my $mydirection = 0;
  my $count = 0;
  for($count=0;$count<=$#array;$count++){
      
      $pad = @array[$count];
      if($pad=~/^P_(.*)/){next;}  #skip power
      
      $pad=~/[IOP]_(.*)/;
      $signal_name = $1;    
      #if a bus if found
      if($signal_name =~ /(.*)__(\d*)/){
	  $signal_name =$1;
	  $pad =~ /(.*)__(\d*)/;
	  $pad =  $1; 
	  if(!(exists($vector_hash{$signal_name}))){
# 	      print "$signal_name found, create an entry\n";
	      $vector_hash{$signal_name} = 1; #create an entry so wont declear twice
	  }else{
# 	      print "$signal_name found once already\n";
	      next; 
	  }
      }
      if($init){ $init = 0;
	     }else{ printf OUT ",\n";
		}      
      $direction = @direction[$count];
#       print " $count: pad: $pad, direction: $direction\n";
      if($mydirection ne $direction){
# 	  print "change direction to $direction.\n";
	  printf OUT "\/\/  $direction \n";
	  $mydirection = $direction;
      }
      printf OUT "              $pad,\n";
      printf OUT "              $signal_name";

  }
}


sub define_in_out{
  my @array = @_;
  my %vector_hash = ();

  foreach $pad (@array){
  
    $pad=~/[IOP]_(.*)/;
    $signal_name = $1;

    if($signal_name =~ /(.*)__(\d*)/){
      $signal_name =$1;

#       print "vector: $signal_name found\n";

      $pad =~ /(.*)__(\d*)/;
      $pad = $1; 
      $bit = $2;
      if(!(exists($vector_hash{$signal_name}))){
	$vector = "\[$vector_max{$signal_name}:$vector_min{$signal_name}\]";
        $vector_hash{$signal_name} = 1; #create an entry so won't declear twice
      }else{
# 	  print "vector: $pad $bit already exist\n";
	  next;
      }
    }else{
      $vector= "";
    }

  CASE1: {
      ($pad =~ /^I_/) && do{ #input 
      printf OUT "         input  $vector $pad;\n";
      printf OUT "         output $vector $signal_name;\n";
      last CASE1;
    };
    ($pad =~ /^O_/) && do{ #output 
      printf OUT "         output $vector $pad;\n";
      printf OUT "         input  $vector $signal_name;\n";
      last CASE1;
    };
  }; #CASE
  } #foreach


} #define_in_out


sub print_body{
  my @array       = @_;
  my $direction   = -1;
  my $east_count  = -1;
  my $west_count  = -1;
  my $north_count = -1;
  my $south_count = -1;
  my $dir_char    ="E";
  my $dir_count   =  0;
  my $mydirection = 0;
  for($count=0;$count<=$#array;$count++){
      $pad = @array[$count];
      $direction = @direction[$count];
#print the IO direction for the following section
      if($mydirection ne $direction){
	  printf OUT "\n\/\/  $direction \n";
	  $mydirection = $direction;
      }

    CASE3:{
	($direction =~ "RIGHT")&& do{
	    $east_count++;
            $dir_char="E";
	    $dir_count = $east_count;
	    last CASE3;
	};
	($direction =~ "LEFT")&& do{
	    $west_count++;
            $dir_char="W";
	    $dir_count = $west_count;
	    last CASE3;
	};
	($direction =~ "TOP")&& do{
	    $north_count++;
            $dir_char="N";
	    $dir_count = $north_count;
	    last CASE3;
	};
	($direction =~ "BOTTOM")&& do{
	    $south_count++;
            $dir_char="S";
	    $dir_count = $south_count;
	    last CASE3;
	};
    } #end of CASE

      $pad =~ s/\s+//;
      $pad_name = $pad."_".$dir_char.$dir_count;
      
      $pad=~/[IOP]_(.*)/;
      $signal_name = $1;
      #if a bus is found
      if($signal_name =~ /(.*)__(\d*)/){
	  $signal_name =$1;	 
	  $pad =~ /(.*)__(\d*)$/;
	  $pad =  $1;
	  $bit_num = $2;
	  $pad = "$pad\[$bit_num\]";
	  $signal_name = "$signal_name\[$bit_num\]";
      }
    CASE1: {
	($pad =~ /^I_/) && do{ #input 
	    printf OUT "     PICS   P$pad_name(.P($pad), .Y($signal_name), .IE(1'b1));\n";
	    last CASE1;
	};
	($pad =~ /^O_/) && do{ #output 
	    printf OUT "     POC24A P$pad_name(.P($pad), .A($signal_name));\n";
	    last CASE1;
	};
      ($pad =~ /^P_/) && do{ #power
	  if($pad=~/clean_VDD(\w*)/){
	      $bit =$1;
	      printf OUT "     PVDD$bit   P$pad_name(.VDD$bit(VDD$bit));\n";
	  }
	  elsif($pad=~/clean_VDC(\w*)/){
	      $bit =$1;
	      printf OUT "     PVDC$bit   P$pad_name(.VDC$bit(VDC$bit));\n";
	  }

	  elsif($pad=~/dirty_VDD/){
	      printf OUT "     PDVDD  P$pad_name(.DVDD(DVDD) );\n";
	}
	  elsif($pad=~/clean_VSS(\w*)/){
	      $bit = $1;
	      printf OUT "     PVSS   P$pad_name(VSS$bit);\n";
	  }
	  elsif($pad=~/dirty_VSS/){
	      printf OUT "     PDVSS  P$pad_name(.DVSS(DVSS) );\n";
	  }
	  else{
	      printf "\/\/ERROR!! UNDEFINED SIGNAL DIRECTION FOR $pad\n";
	}
	  last CASE1;
      };
    } #CASE1
      
  } #foreach
} #end of sub print_body


