#######################################################
#                                                     
#  Encounter Command Logging File                     
#  Created on Tue Dec 11 16:31:42 2018                
#                                                     
#######################################################

#@(#)CDS: Encounter v14.27-s035_1 (64bit) 10/07/2015 12:46 (Linux 2.6.18-194.el5)
#@(#)CDS: NanoRoute v14.27-s012 NR150928-2308/14_27-UB (database version 2.30, 267.6.1) {superthreading v1.25}
#@(#)CDS: CeltIC v14.27-s018_1 (64bit) 10/06/2015 22:57:34 (Linux 2.6.18-194.el5)
#@(#)CDS: AAE 14.27-s003 (64bit) 10/07/2015 (Linux 2.6.18-194.el5)
#@(#)CDS: CTE 14.27-s022_1 (64bit) Oct  6 2015 07:33:36 (Linux 2.6.18-194.el5)
#@(#)CDS: CPE v14.27-s021
#@(#)CDS: IQRC/TQRC 14.2.2-s217 (64bit) Wed Apr 15 23:10:24 PDT 2015 (Linux 2.6.18-194.el5)

set_global _enable_mmmc_by_default_flow      $CTE::mmmc_default
suppressMessage ENCEXT-2799
set init_verilog {../verilog/eecs427_top.v ../verilog/eecs427_core.v ../verilog/eecs427_pads.v ../verilog/mult.apr.v /afs/umich.edu/class/eecs427/ibm13/parts/eecs427pads/verilog/arti_cmos8rf_verilog_i_modules.v /afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/verilog/ibm13_modules.v /afs/umich.edu/class/eecs427/ibm13/parts/eecs427memories/ra1sh16x512/verilog/RA1SH16x512_module.v }
set init_top_cell eecs427_top
set init_design_netlisttype Verilog
set init_design_settop 1
set init_io_file ../verilog/eecs427_top.save.io
set init_lef_file {/afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/lef/ibm13_8lm_2thick_tech.lef /afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/lef/ibm13rvt_macros.lef /afs/umich.edu/class/eecs427/ibm13/parts/eecs427pads/lef/arti_cmos8rf_8lm_2thick_i.lef ../lef/mult.lef /afs/umich.edu/class/eecs427/ibm13/parts/eecs427memories/ra1sh16x512/lef/RA1SH16x512.vclef}
set init_mmmc_file viewDefinition.tcl
set init_pwr_net VDD
set init_gnd_net VSS
set init_assign_buffer 0
setDesignMode -process 130
init_design
floorPlan -site IBM13SITE -d 1254.0 1424.0 10.8 10.8 10.8 10.8
setFlipping f
redraw
fit
clearGlobalNets
globalNetConnect VDD -type pgpin -pin VDD -inst *
globalNetConnect VSS -type pgpin -pin VSS -inst *
globalNetConnect VDD -type pgpin -pin IE -inst PI*
saveDesign initialized.enc
addIoFiller -cell PFILL1 -prefix PF_ -fillAnyGap
placeInstance eecs427_core0/mult 603.0 446.0 MX -fixed
placeInstance eecs427_core0/ram 457.12 598.0 R0 -fixed
addRing -nets {VDD VSS} -around each_block -center 1 -layer {top M3 bottom M3 left M4 right M4} -width {top 2 bottom 2 left 2 right 2} -spacing {top 1 bottom 1 left 1 right 1}
selectInst eecs427_core0/ram
addRing -nets VDD -type block_rings -around selected -layer {top M3 bottom M3 left M4 right M4} -width {top 10 bottom 10 left 10 right 10} -spacing {top 4 bottom 4 left 4 right 4} -offset {top 3 bottom 3 left 3 right 3} -extend_corner {tr tl}
addRing -nets VSS -type block_rings -around selected -layer {top M3 bottom M3 left M4 right M4} -width {top 10 bottom 10 left 10 right 10} -spacing {top 4 bottom 4 left 4 right 4} -offset {top 16 bottom 24 left 16 right 16} -extend_corner {br bl}
deselectAll
selectInst eecs427_core0/mult
addRing -nets VDD -type block_rings -around selected -layer {top M3 bottom M3 left M4 right M4} -width {top 10 bottom 10 left 10 right 10} -spacing {top 4 bottom 4 left 4 right 4} -offset {top 3 bottom 3 left 3 right 3} -extend_corner {br bl}
addRing -nets VSS -type block_rings -around selected -layer {top M3 bottom M3 left M4 right M4} -width {top 10 bottom 10 left 10 right 10} -spacing {top 4 bottom 4 left 4 right 4} -offset {top 24 bottom 16 left 16 right 16}
deselectAll
globalNetConnect VDD -type pgpin -pin VDD -singleInstance eecs427_core0/ram -override -verbose
globalNetConnect VSS -type pgpin -pin VSS -singleInstance eecs427_core0/ram -override -verbose
globalNetConnect VDD -type pgpin -pin VDD -singleInstance eecs427_core0/mult -override -verbose
globalNetConnect VSS -type pgpin -pin VSS -singleInstance eecs427_core0/mult -override -verbose
selectInst eecs427_core0/ram
sroute -nets { VDD VSS } -connect blockPin -blockPin all -blockPinTarget nearestTarget -inst eecs427_core0/ram -blockSides {top left right bottom} -jogControl {preferWithChanges differentLayer} -blockPinConnectRingPinCorners -verbose
deselectAll
selectInst eecs427_core0/mult
sroute -nets { VDD VSS } -connect blockPin -blockPin all -blockPinTarget nearestTarget -inst eecs427_core0/mult -blockSides {top left right bottom} -jogControl { preferWithChanges differentLayer } -blockPinConnectRingPinCorners -verbose
deselectAll
saveDesign power.enc
setPlaceMode -congHighEffort -doCongOpt -modulePlan -maxRouteLayer 3
placeDesign -inPlaceOpt -prePlaceOpt
redraw
saveDesign placed.enc
redraw
globalNetConnect VDD -type pgpin -pin VDD -override -netlistOverride
globalNetConnect VSS -type pgpin -pin VSS -override -netlistOverride
sroute -nets { VDD VSS } -connect padPin -blockPin all -padPinMinLayer 4 -corePinNoRouteEmptyRows
saveDesign power_routed.enc
trialRoute -highEffort
setNanoRouteMode -drouteUseViaOfCut 2
setNanoRouteMode -drouteUseMultiCutViaEffort medium
setNanoRouteMode -drouteFixAntenna false
setNanoRouteMode -drouteSearchAndRepair true
setNanoRouteMode -routeTopRoutingLayer 6
setNanoRouteMode -routeBottomRoutingLayer 2
globalDetailRoute
saveDesign routed.enc
lefOut ../lef/eecs427_top.lef
setStreamOutMode -SEvianames ON
streamOut ../gds2/eecs427_top.gds2 -mapFile /afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/enc2gdsLM.map -libName eecs427artisan -structureName eecs427_top -stripes 1 -units 1000 -mode ALL
saveNetlist -excludeLeafCell ../verilog/eecs427_top.apr.v
setExtractRCMode -engine postRoute -effortLevel high -relative_c_th 0.01 -total_c_th 0.01 -Reduce 0.0 -specialNet true
extractRC -outfile eecs427_top.cap
rcOut -spf eecs427_top.spf
rcOut -spef eecs427_top.spef
setDelayCalMode -engine aae -signoff true -ecsmType ecsmOnDemand
write_sdf -celltiming all ../sdf/${my_toplevel}.apr.sdf
verifyGeometry -reportAllCells -noSameNet -noOverlap -report eecs427_top.geom.rpt
fixVia -minCut
verifyConnectivity -type all -noAntenna -report eecs427_top.conn.rpt
win
fit
fit
uiSetTool ruler
uiSetTool ruler
panPage 0 1
panPage 0 1
uiSetTool ruler
uiSetTool ruler
uiSetTool ruler
panPage 0 -1
fit
uiSetTool ruler
fit
uiSetTool ruler
panPage -1 0
panPage -1 0
panPage -1 0
panPage -1 0
panPage -1 0
panPage -1 0
panPage -1 0
fit
uiSetTool ruler
panPage -1 0
panPage -1 0
panPage -1 0
panPage -1 0
panPage -1 0
panPage -1 0
panPage -1 0
panPage -1 0
panPage -1 0
fit
panPage 0 -1
panPage 0 -1
uiSetTool ruler
panPage 0 1
panPage 0 1
panPage 0 1
panPage 0 1
panPage 0 1
panPage 0 1
panPage 0 1
panPage 0 1
panPage 0 1
panPage 0 1
panPage 0 1
panPage 0 1
panPage 0 1
uiSetTool ruler
uiSetTool ruler
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
panPage 0 -1
fit
fit
panPage -1 0
panPage -1 0
panPage 0 -1
fit
fit
