* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT NAND2
** N=26 EP=0 IP=0 FDC=0
*.SEEDPROM
*.CALIBRE ISOLATED NETS: IN1 VSS! OUT IN0 VDD!
.ENDS
***************************************
.SUBCKT MUX21 VDD! IN0 VSS! SEL IN1 OUT
** N=43 EP=6 IP=42 FDC=16
M0 VSS! SEL 2 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=3.36e-14 $X=980 $Y=1670 $D=25
M1 41 2 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=2.8e-14 AS=8.96e-14 PD=4.8e-07 PS=1.2e-06 NRD=0.357143 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=6.4e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=3.36e-14 $X=500 $Y=2850 $D=25
M2 7 IN0 41 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=2.8e-14 PD=1.2e-06 PS=4.8e-07 NRD=1.14286 NRS=0.357143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=6.4e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=3.36e-14 $X=820 $Y=2850 $D=25
M3 VSS! IN1 42 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=2.8e-14 PD=1.2e-06 PS=4.8e-07 NRD=1.14286 NRS=0.357143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=6.4e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=3.36e-14 $X=2580 $Y=1670 $D=25
M4 42 SEL 1 VSS! nfet L=1.2e-07 W=2.8e-07 AD=2.8e-14 AS=8.96e-14 PD=4.8e-07 PS=1.2e-06 NRD=0.357143 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=6.4e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=3.36e-14 $X=2260 $Y=1670 $D=25
M5 43 7 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=2.8e-14 AS=8.96e-14 PD=4.8e-07 PS=1.2e-06 NRD=0.357143 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=6.4e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=3.36e-14 $X=2100 $Y=2850 $D=25
M6 OUT 1 43 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=2.8e-14 PD=1.2e-06 PS=4.8e-07 NRD=1.14286 NRS=0.357143 m=1 par=1 nf=1 ngcon=1 psp=0 sa=6.4e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=3.36e-14 $X=2420 $Y=2850 $D=25
M7 VDD! SEL 2 VDD! pfet L=1.2e-07 W=5.6e-07 AD=1.792e-13 AS=1.792e-13 PD=1.76e-06 PS=1.76e-06 NRD=0.571429 NRS=0.571429 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=9.72e-14 panw8=3.12e-14 panw9=3.72e-14 panw10=0 $X=980 $Y=510 $D=108
M8 7 2 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=8.96e-14 PD=6.4e-07 PS=1.2e-06 NRD=0.642857 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=2.4e-15 panw7=6.36e-14 panw8=3.12e-14 panw9=3.6e-15 panw10=0 $X=500 $Y=4010 $D=108
M9 VDD! IN0 7 VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.04e-14 PD=1.2e-06 PS=6.4e-07 NRD=1.14286 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=2.4e-15 panw7=3e-14 panw8=3.12e-14 panw9=3.72e-14 panw10=0 $X=980 $Y=4010 $D=108
M10 VDD! IN1 1 VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.04e-14 PD=1.2e-06 PS=6.4e-07 NRD=1.14286 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=2.4e-15 panw7=6.36e-14 panw8=3.12e-14 panw9=3.6e-15 panw10=0 $X=2580 $Y=510 $D=108
M11 1 SEL VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=8.96e-14 PD=6.4e-07 PS=1.2e-06 NRD=0.642857 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=2.4e-15 panw7=3e-14 panw8=3.12e-14 panw9=3.72e-14 panw10=3.36e-14 $X=2100 $Y=510 $D=108
M12 OUT 7 VDD! VDD! pfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=8.96e-14 PD=6.4e-07 PS=1.2e-06 NRD=0.642857 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=2.4e-15 panw7=3e-14 panw8=3.12e-14 panw9=3.72e-14 panw10=0 $X=2100 $Y=4010 $D=108
M13 VDD! 1 OUT VDD! pfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.04e-14 PD=1.2e-06 PS=6.4e-07 NRD=1.14286 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=2.4e-15 panw7=6.36e-14 panw8=3.12e-14 panw9=3.6e-15 panw10=0 $X=2580 $Y=4010 $D=108
D14 VSS! VDD! diodenwx AREA=4.7656e-12 perim=9.14e-06 t3well=0 $X=360 $Y=-240 $D=474
D15 VSS! VDD! diodenwx AREA=5.5384e-12 perim=1.01e-05 t3well=0 $X=-120 $Y=3430 $D=474
.ENDS
***************************************
