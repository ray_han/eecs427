
######################################################
#                                                    #
#  Cadence Design Systems, Inc.                      #
#  Cadence(R) Encounter(TM) IO Assignments           #
#                                                    #
######################################################

Version: 2



Offset:	10
Pin:	inst15_12[0] N 2 0.2000 0.4000
Offset:	12
Pin:	inst15_12[1] N 2 0.2000 0.4000
Offset:	14
Pin:	inst15_12[2] N 2 0.2000 0.4000
Offset:	16
Pin:	inst15_12[3] N 2 0.2000 0.4000
Offset:	18
Pin:	inst7_4[0] N 2 0.2000 0.4000
Offset:	20
Pin:	inst7_4[1] N 2 0.2000 0.4000
Offset:	22
Pin:	inst7_4[2] N 2 0.2000 0.4000
Offset:	24
Pin:	inst7_4[3] N 2 0.2000 0.4000

Offset:	10
Pin:	WE[0] E 3 0.2000 0.4000
Offset:	13
Pin:	WE[1] E 3 0.2000 0.4000
Offset:	16
Pin:	WE[2] E 3 0.2000 0.4000
Offset:	19
Pin:	WE[3] E 3 0.2000 0.4000
Offset:	22
Pin:	WE[4] E 3 0.2000 0.4000
Offset:	25
Pin:	WE[5] E 3 0.2000 0.4000
Offset:	28
Pin:	WE[6] E 3 0.2000 0.4000
Offset:	31
Pin:	WE[7] E 3 0.2000 0.4000
Offset:	34
Pin:	WE[8] E 3 0.2000 0.4000
Offset:	37
Pin:	WE[9] E 3 0.2000 0.4000
Offset:	40
Pin:	WE[10] E 3 0.2000 0.4000
Offset:	43
Pin:	WE[11] E 3 0.2000 0.4000
Offset:	46
Pin:	WE[12] E 3 0.2000 0.4000
Offset:	49
Pin:	WE[13] E 3 0.2000 0.4000
Offset:	52
Pin:	WE[14] E 3 0.2000 0.4000
Offset:	55
Pin:	WE[15] E 3 0.2000 0.4000
Offset:	58
Pin:	WE_OUT[0] E 3 0.2000 0.4000
Offset:	61
Pin:	WE_OUT[1] E 3 0.2000 0.4000
Offset:	64
Pin:	WE_OUT[2] E 3 0.2000 0.4000
Offset:	67
Pin:	WE_OUT[3] E 3 0.2000 0.4000
Offset:	70
Pin:	WE_OUT[4] E 3 0.2000 0.4000
Offset:	73
Pin:	WE_OUT[5] E 3 0.2000 0.4000
Offset:	76
Pin:	WE_OUT[6] E 3 0.2000 0.4000
Offset:	79
Pin:	WE_OUT[7] E 3 0.2000 0.4000
Offset:	82
Pin:	WE_OUT[8] E 3 0.2000 0.4000
Offset:	85
Pin:	WE_OUT[9] E 3 0.2000 0.4000
Offset:	88
Pin:	WE_OUT[10] E 3 0.2000 0.4000
Offset:	91
Pin:	WE_OUT[11] E 3 0.2000 0.4000
Offset:	94
Pin:	WE_OUT[12] E 3 0.2000 0.4000
Offset:	97
Pin:	WE_OUT[13] E 3 0.2000 0.4000
Offset:	100
Pin:	WE_OUT[14] E 3 0.2000 0.4000
Offset:	103
Pin:	WE_OUT[15] E 3 0.2000 0.4000




