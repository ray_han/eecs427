#######################################################
#                                                     
#  Encounter Command Logging File                     
#  Created on Thu Nov 15 15:17:48 2018                
#                                                     
#######################################################

#@(#)CDS: Encounter v14.27-s035_1 (64bit) 10/07/2015 12:46 (Linux 2.6.18-194.el5)
#@(#)CDS: NanoRoute v14.27-s012 NR150928-2308/14_27-UB (database version 2.30, 267.6.1) {superthreading v1.25}
#@(#)CDS: CeltIC v14.27-s018_1 (64bit) 10/06/2015 22:57:34 (Linux 2.6.18-194.el5)
#@(#)CDS: AAE 14.27-s003 (64bit) 10/07/2015 (Linux 2.6.18-194.el5)
#@(#)CDS: CTE 14.27-s022_1 (64bit) Oct  6 2015 07:33:36 (Linux 2.6.18-194.el5)
#@(#)CDS: CPE v14.27-s021
#@(#)CDS: IQRC/TQRC 14.2.2-s217 (64bit) Wed Apr 15 23:10:24 PDT 2015 (Linux 2.6.18-194.el5)

set_global _enable_mmmc_by_default_flow      $CTE::mmmc_default
suppressMessage ENCEXT-2799
suppressMessage ENCLF-201
suppressMessage ENCPP-557
suppressMessage TECHLIB-436
set init_verilog {../verilog/mult.nl.v /afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/verilog/ibm13_modules.v}
set init_top_cell mult
set init_design_netlisttype Verilog
set init_design_settop 1
set init_io_file {}
set init_lef_file {/afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/lef/ibm13_8lm_2thick_tech.lef /afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/lef/ibm13rvt_macros.lef}
set init_mmmc_file viewDefinition.tcl
set init_pwr_net VDD
set init_gnd_net VSS
set init_assign_buffer 0
setDesignMode -process 130
init_design
floorPlan -s 70.4 74 10.8 10.8 10.8 10.8
setFlipping f
redraw
fit
clearGlobalNets
globalNetConnect VDD -type pgpin -pin VDD -inst *
globalNetConnect VDD -type tiehi
globalNetConnect VSS -type pgpin -pin VSS -inst *
globalNetConnect VSS -type tielo
applyGlobalNets
saveDesign initialized.enc
addRing -nets {VDD VSS} -around each_block -center 1 -layer {top M3 bottom M3 left M2 right M2} -width {top 2 bottom 2 left 2 right 2} -spacing {top 1 bottom 1 left 1 right 1}
setAddStripeMode -break_at block_ring -stacked_via_bottom_layer M3 -stacked_via_top_layer M5
addStripe -direction vertical -nets {VDD VSS} -extend_to design_boundary -set_to_set_distance 24.3 -width 1.2 -spacing 10.8 -layer M4 -create_pins 1 -xleft_offset 0
saveDesign power.enc
loadIoFile mult.save.io
setPlaceMode -congEffort High -modulePlan true
setTrialRouteMode -maxRouteLayer 3
setOptMode -allEndPoints true
placeDesign -prePlaceOpt
optDesign -preCTS
deletePlaceBlockage -all
addTieHiLo -cell {TIEHITR TIELOTR} -prefix tie
redraw
saveDesign placed.enc
redraw
sroute -blockPinTarget nearestTarget
saveDesign power_routed.enc
setCTSMode -engine ck
createClockTreeSpec -output mult.cts
specifyClockTree -file mult.cts
ckSynthesis -rguide cts.rguide -report report.ctsrpt -macromodel report.ctsmdl -fix_added_buffers -forceReconvergent
optDesign -postCTS
trialRoute -highEffort -guide cts.rguide
addFiller -cell FILL8TR -prefix FILL -fillBoundary
addFiller -cell FILL4TR -prefix FILL -fillBoundary
addFiller -cell FILL2TR -prefix FILL -fillBoundary
addFiller -cell FILL1TR -prefix FILL -fillBoundary
globalNetConnect VDD -type tiehi
globalNetConnect VDD -type pgpin -pin VDD -override
globalNetConnect VSS -type tielo
globalNetConnect VSS -type pgpin -pin VSS -override
setNanoRouteMode -drouteUseViaOfCut 2
setNanoRouteMode -drouteFixAntenna false
setNanoRouteMode -drouteSearchAndRepair true
setNanoRouteMode -droutePostRouteSwapVia multicut
setNanoRouteMode -routeTopRoutingLayer 3
setNanoRouteMode -routeBottomRoutingLayer 1
globalDetailRoute
setNanoRouteMode -routeTopRoutingLayer 3
setNanoRouteMode -routeInsertDiodeForClockNets true
setNanoRouteMode -drouteFixAntenna true
setNanoRouteMode -routeAntennaCellName ANTENNATR
setNanoRouteMode -routeInsertAntennaDiode true
setNanoRouteMode -drouteSearchAndRepair true
globalDetailRoute
saveDesign routed.enc
lefOut ../lef/mult.lef
setStreamOutMode -SEvianames ON
streamOut ../gds2/mult.gds2 -mapFile /afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/enc2gdsLM.map -libName eecs427artisan -structureName mult -stripes 1 -units 1000 -mode ALL
saveNetlist -excludeLeafCell ../verilog/mult.apr.v
setExtractRCMode -engine postRoute -effortLevel high -relative_c_th 0.01 -total_c_th 0.01 -Reduce 0.0 -specialNet true
extractRC -outfile mult.cap
rcOut -spf mult.spf
rcOut -spef mult.spef
setDelayCalMode -engine aae -signoff true -ecsmType ecsmOnDemand
write_sdf ../sdf/${my_toplevel}.apr.sdf
verifyGeometry -reportAllCells -noOverlap -report mult.geom.rpt
fixVia -minCut
verifyConnectivity -type all -noAntenna -report mult.conn.rpt
win
