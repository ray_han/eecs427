/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06
// Date      : Mon Dec 10 02:35:30 2018
/////////////////////////////////////////////////////////////


module we_reorder ( WE, inst15_12, inst7_4, WE_OUT );
  input [15:0] WE;
  input [3:0] inst15_12;
  input [3:0] inst7_4;
  output [15:0] WE_OUT;
  wire   n1, n2, n3, n4, n5, n6, n7;

  INVX2TR U24 ( .A(inst7_4[1]), .Y(n2) );
  BUFX8TR U25 ( .A(n4), .Y(n7) );
  NAND4BX2TR U26 ( .AN(inst15_12[0]), .B(n5), .C(inst7_4[2]), .D(n6), .Y(n4)
         );
  OAI33X1TR U27 ( .A0(n1), .A1(n3), .A2(n2), .B0(inst7_4[0]), .B1(inst7_4[3]), 
        .B2(inst7_4[1]), .Y(n5) );
  NOR3X1TR U28 ( .A(inst15_12[1]), .B(inst15_12[3]), .C(inst15_12[2]), .Y(n6)
         );
  CLKAND2X2TR U29 ( .A(WE[0]), .B(n7), .Y(WE_OUT[0]) );
  CLKAND2X2TR U30 ( .A(WE[1]), .B(n7), .Y(WE_OUT[1]) );
  CLKAND2X2TR U31 ( .A(WE[2]), .B(n7), .Y(WE_OUT[2]) );
  CLKAND2X2TR U32 ( .A(WE[3]), .B(n7), .Y(WE_OUT[3]) );
  CLKAND2X2TR U33 ( .A(WE[4]), .B(n7), .Y(WE_OUT[4]) );
  CLKAND2X2TR U34 ( .A(WE[5]), .B(n7), .Y(WE_OUT[5]) );
  CLKAND2X2TR U35 ( .A(WE[6]), .B(n7), .Y(WE_OUT[6]) );
  CLKAND2X2TR U36 ( .A(WE[7]), .B(n7), .Y(WE_OUT[7]) );
  CLKAND2X2TR U37 ( .A(WE[8]), .B(n7), .Y(WE_OUT[8]) );
  CLKAND2X2TR U38 ( .A(WE[9]), .B(n7), .Y(WE_OUT[9]) );
  CLKAND2X2TR U39 ( .A(WE[10]), .B(n7), .Y(WE_OUT[10]) );
  CLKAND2X2TR U40 ( .A(WE[11]), .B(n7), .Y(WE_OUT[11]) );
  CLKAND2X2TR U41 ( .A(WE[12]), .B(n7), .Y(WE_OUT[12]) );
  CLKAND2X2TR U42 ( .A(WE[13]), .B(n7), .Y(WE_OUT[13]) );
  CLKAND2X2TR U43 ( .A(WE[14]), .B(n7), .Y(WE_OUT[14]) );
  CLKAND2X2TR U44 ( .A(WE[15]), .B(n7), .Y(WE_OUT[15]) );
  CLKINVX2TR U45 ( .A(inst7_4[3]), .Y(n1) );
  CLKINVX2TR U46 ( .A(inst7_4[0]), .Y(n3) );
endmodule

