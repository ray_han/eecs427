# common.tcl setup library files

# 0.13um IBM Artisan Library
# Set library paths

set ARTISAN "/afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells"
set SYNOPSYS [get_unix_variable SYNOPSYS]
set search_path [list "." ${SYNOPSYS}/libraries/syn "${ARTISAN}/synopsys"]
set link_library "* typical.db dw_foundation.sldb"
set target_library "typical.db"

