* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT decoder_4th 1 2 3 4 5 6 7
** N=80 EP=7 IP=0 FDC=14
M0 9 2 4 6 nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=450 $D=97
M1 9 3 5 6 nfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=4810 $D=97
M2 10 9 6 6 nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=450 $D=97
M3 10 9 6 6 nfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=4780 $D=97
M4 11 10 12 6 nfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=450 $D=97
M5 1 8 6 6 nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=4800 $D=97
M6 6 8 1 6 nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=4800 $D=97
M7 9 2 5 7 pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=3310 $D=189
M8 9 3 4 7 pfet L=1.2e-07 W=3.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=1950 $D=189
M9 10 9 7 7 pfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=1920 $D=189
M10 10 9 7 7 pfet L=1.2e-07 W=3.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=3310 $D=189
M11 11 10 13 7 pfet L=1.2e-07 W=5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=1790 $D=189
M12 1 8 7 7 pfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2820 $Y=3310 $D=189
M13 7 8 1 7 pfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3300 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8
** N=8 EP=8 IP=14 FDC=28
X0 1 3 4 5 6 7 5 decoder_4th $T=0 0 0 0 $X=-300 $Y=-240
X1 2 3 4 5 8 7 5 decoder_4th $T=0 5600 0 0 $X=-300 $Y=5360
.ENDS
***************************************
.SUBCKT decoder_3rd 1 2 3 4 5 6 7
** N=67 EP=7 IP=0 FDC=8
M0 8 1 4 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=450 $D=97
M1 8 2 5 6 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=4870 $D=97
M2 3 8 6 6 nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=450 $D=97
M3 3 8 6 6 nfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=4550 $D=97
M4 8 1 5 7 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=480 $Y=3310 $D=189
M5 8 2 4 7 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=2010 $D=189
M6 3 8 7 7 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=1690 $D=189
M7 3 8 7 7 pfet L=1.2e-07 W=6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1880 $Y=3310 $D=189
.ENDS
***************************************
.SUBCKT ICV_2 1 2 3 4 5 6 7 8 9 10 11
** N=11 EP=11 IP=14 FDC=22
X0 1 2 3 7 8 9 8 decoder_4th $T=4800 0 0 0 $X=4500 $Y=-240
X1 4 5 6 11 10 9 8 decoder_3rd $T=0 0 0 0 $X=-300 $Y=-240
.ENDS
***************************************
.SUBCKT ICV_3 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
** N=15 EP=15 IP=22 FDC=44
X0 1 6 7 3 4 5 12 9 13 10 11 ICV_2 $T=0 0 0 0 $X=-300 $Y=-240
X1 2 6 7 3 4 8 8 9 13 14 15 ICV_2 $T=0 5600 0 0 $X=-300 $Y=5360
.ENDS
***************************************
.SUBCKT decoder_2st 1 2 3 4 5 6 7
** N=37 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4590 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=5.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=1730 $D=189
.ENDS
***************************************
.SUBCKT decoder_1st 1 2 3 4 5 6 7
** N=39 EP=7 IP=0 FDC=4
M0 3 1 6 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=450 $D=97
M1 3 2 7 4 nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=4870 $D=97
M2 3 1 7 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=560 $Y=3310 $D=189
M3 3 2 6 5 pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=960 $Y=2010 $D=189
.ENDS
***************************************
.SUBCKT DECODER8 E<15> E<14> E<13> E<12> E<11> E<10> E<9> E<8> E<7> E<6> E<5> E<4> E<3> E<2> E<1> E<0> EN SH0_BAR SH<0> SH1_BAR
+ SH<1> SH2_BAR SH<2> SH3_BAR SH<3> VSS! VDD!
** N=44 EP=27 IP=134 FDC=312
X0 E<15> E<14> SH3_BAR SH<3> VDD! 30 VSS! 31 ICV_1 $T=8800 -78400 0 0 $X=8500 $Y=-78640
X1 E<13> E<12> SH3_BAR SH<3> VDD! 32 VSS! 33 ICV_1 $T=8800 -67200 0 0 $X=8500 $Y=-67440
X2 E<11> E<10> SH3_BAR SH<3> VDD! 34 VSS! 35 ICV_1 $T=8800 -56000 0 0 $X=8500 $Y=-56240
X3 E<9> E<8> SH3_BAR SH<3> VDD! 36 VSS! 37 ICV_1 $T=8800 -44800 0 0 $X=8500 $Y=-45040
X4 E<7> E<6> SH2_BAR SH<2> 30 SH3_BAR SH<3> 31 VDD! 24 VSS! 30 VSS! 25 VSS! ICV_3 $T=4000 -33600 0 0 $X=3700 $Y=-33840
X5 E<5> E<4> SH2_BAR SH<2> 32 SH3_BAR SH<3> 33 VDD! 26 VSS! 32 VSS! 27 VSS! ICV_3 $T=4000 -22400 0 0 $X=3700 $Y=-22640
X6 E<3> E<2> SH2_BAR SH<2> 34 SH3_BAR SH<3> 35 VDD! VSS! 43 34 VSS! VSS! 25 ICV_3 $T=4000 -11200 0 0 $X=3700 $Y=-11440
X7 E<1> E<0> SH2_BAR SH<2> 40 SH3_BAR SH<3> 37 VDD! VSS! 26 36 VSS! VSS! 27 ICV_3 $T=4000 0 0 0 $X=3700 $Y=-240
X8 SH1_BAR SH<1> 24 VSS! VDD! VSS! 20 decoder_2st $T=1600 -11200 0 0 $X=1300 $Y=-11440
X9 SH1_BAR SH<1> 25 VSS! VDD! VSS! 21 decoder_2st $T=1600 -5600 0 0 $X=1300 $Y=-5840
X10 SH1_BAR SH<1> 26 VSS! VDD! 20 VSS! decoder_2st $T=1600 0 0 0 $X=1300 $Y=-240
X11 SH1_BAR SH<1> 27 VSS! VDD! 21 VSS! decoder_2st $T=1600 5600 0 0 $X=1300 $Y=5360
X12 SH0_BAR SH<0> 20 VSS! VDD! VSS! EN decoder_1st $T=0 0 0 0 $X=-300 $Y=-240
X13 SH0_BAR SH<0> 21 VSS! VDD! EN VSS! decoder_1st $T=0 5600 0 0 $X=-300 $Y=5360
.ENDS
***************************************
