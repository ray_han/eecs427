######################################################
#                                                    #
#  Cadence Design Systems, Inc.                      #
#  Cadence(R) Encounter(TM) IO Assignments           #
#                                                    #
######################################################

Version: 2




Offset: 12
Pin: ReadA[15] E 5 0.2000 0.4000
Offset: 13.5
Pin: ReadA[14] E 5 0.2000 0.4000
Offset: 15
Pin: ReadA[13] E 5 0.2000 0.4000
Offset: 16.5
Pin: ReadA[12] E 5 0.2000 0.4000
Offset: 18
Pin: ReadA[11] E 5 0.2000 0.4000
Offset: 19.5
Pin: ReadA[10] E 5 0.2000 0.4000
Offset: 21
Pin: ReadA[9] E 5 0.2000 0.4000
Offset: 22.5
Pin: ReadA[8] E 5 0.2000 0.4000
Offset: 24
Pin: ReadA[7] E 5 0.2000 0.4000
Offset: 25.5
Pin: ReadA[6] E 5 0.2000 0.4000
Offset: 27
Pin: ReadA[5] E 5 0.2000 0.4000
Offset: 28.5
Pin: ReadA[4] E 5 0.2000 0.4000
Offset: 30
Pin: ReadA[3] E 5 0.2000 0.4000
Offset: 31.5
Pin: ReadA[2] E 5 0.2000 0.4000
Offset: 33
Pin: ReadA[1] E 5 0.2000 0.4000
Offset: 34.5
Pin: ReadA[0] E 5 0.2000 0.4000
Offset: 36
Pin: DSEL[1] E 3 0.2000 0.4000
Offset: 37.5
Pin: DSEL[0] E 3 0.2000 0.4000
Offset: 39
Pin: Amount[1] E 3 0.2000 0.4000
Offset: 40.5
Pin: Amount[0] E 3 0.2000 0.4000
Offset: 42
Pin: LuiSEL E 3 0.2000 0.4000
Offset: 43.5
Pin: SEL1 E 3 0.2000 0.4000
Offset: 45
Pin:  MovSEL E 3 0.2000 0.4000
Offset: 46.5
Pin:  AluSEL[1] E 3 0.2000 0.4000
Offset: 48
Pin:  AluSEL[0] E 3 0.2000 0.4000
Offset: 49.5
Pin:  FunISEL[1] E 3 0.2000 0.4000
Offset: 51
Pin:  FunISEL[0] E 3 0.2000 0.4000
Offset: 52.5
Pin:  ReadB[15] E 3 0.2000 0.4000
Offset: 54
Pin:  ReadB[14] E 3 0.2000 0.4000
Offset: 55.5
Pin:  ReadB[13] E 3 0.2000 0.4000
Offset: 57
Pin:  ReadB[12] E 3 0.2000 0.4000
Offset: 58.5
Pin:  ReadB[11] E 3 0.2000 0.4000
Offset: 60
Pin:  ReadB[10] E 3 0.2000 0.4000
Offset: 61.5
Pin:  ReadB[9] E 3 0.2000 0.4000
Offset: 63
Pin:  ReadB[8] E 3 0.2000 0.4000
Offset: 64.5
Pin:  ReadB[7] E 3 0.2000 0.4000
Offset: 66
Pin:  ReadB[6] E 3 0.2000 0.4000
Offset: 67.5
Pin:  ReadB[5] E 3 0.2000 0.4000
Offset: 69
Pin:  ReadB[4] E 3 0.2000 0.4000
Offset: 70.5
Pin:  ReadB[3] E 3 0.2000 0.4000
Offset: 72
Pin:  ReadB[2] E 3 0.2000 0.4000
Offset: 73.5
Pin:  ReadB[1] E 3 0.2000 0.4000
Offset: 75
Pin:  ReadB[0] E 3 0.2000 0.4000
Offset: 76.5
Pin: WE[15] E 3 0.2000 0.4000
Offset: 78
Pin: WE[14] E 3 0.2000 0.4000
Offset: 79.5
Pin: WE[13] E 3 0.2000 0.4000
Offset: 81
Pin: WE[12] E 3 0.2000 0.4000
Offset: 82.5
Pin: WE[11] E 3 0.2000 0.4000
Offset: 84
Pin: WE[10] E 3 0.2000 0.4000
Offset: 85.5
Pin: WE[9] E 3 0.2000 0.4000
Offset: 87
Pin: WE[8] E 3 0.2000 0.4000
Offset: 88.5
Pin: WE[7] E 3 0.2000 0.4000
Offset: 90
Pin: WE[6] E 3 0.2000 0.4000
Offset: 91.5
Pin: WE[5] E 3 0.2000 0.4000
Offset: 93
Pin: WE[4] E 3 0.2000 0.4000
Offset: 94.5
Pin: WE[3] E 3 0.2000 0.4000
Offset: 96
Pin: WE[2] E 3 0.2000 0.4000
Offset: 9.5
Pin: WE[1] E 3 0.2000 0.4000
Offset: 8.5
Pin: WE[0] E 3 0.2000 0.4000
Offset: 100.5
Pin: DOut[15] E 3 0.2000 0.4000
Offset: 102
Pin: DOut[14] E 3 0.2000 0.4000
Offset: 103.5
Pin: DOut[13] E 3 0.2000 0.4000
Offset: 105
Pin: DOut[12] E 3 0.2000 0.4000
Offset: 106.5
Pin: DOut[11] E 3 0.2000 0.4000
Offset: 108
Pin: DOut[10] E 3 0.2000 0.4000
Offset: 109.5
Pin: DOut[9] E 3 0.2000 0.4000
Offset: 111
Pin: DOut[8] E 3 0.2000 0.4000
Offset: 112.5
Pin: DOut[7] E 3 0.2000 0.4000
Offset: 114
Pin: DOut[6] E 3 0.2000 0.4000
Offset: 115
Pin: DOut[5] E 3 0.2000 0.4000
Offset: 116
Pin: DOut[4] E 3 0.2000 0.4000
Offset: 117
Pin: DOut[3] E 3 0.2000 0.4000
Offset: 118
Pin: DOut[2] E 3 0.2000 0.4000
Offset: 99 
Pin: DOut[1] E 3 0.2000 0.4000
Offset: 97.5
Pin: DOut[0] E 3 0.2000 0.4000
Offset: 10.5
Pin: WEMaster E 3 0.2000 0.4000




Offset: 14
Pin: InstruIn[15] W 3 0.2000 0.4000
Offset: 16
Pin: InstruIn[14] W 3 0.2000 0.4000
Offset: 19
Pin: InstruIn[13] W 3 0.2000 0.4000
Offset: 22
Pin: InstruIn[12] W 3 0.2000 0.4000
Offset: 25
Pin: InstruIn[11] W 3 0.2000 0.4000
Offset: 28
Pin: InstruIn[10] W 3 0.2000 0.4000
Offset: 31
Pin: InstruIn[9] W 3 0.2000 0.4000
Offset: 34
Pin: InstruIn[8] W 3 0.2000 0.4000
Offset: 37
Pin: InstruIn[7] W 3 0.2000 0.4000
Offset: 40
Pin: InstruIn[6] W 3 0.2000 0.4000
Offset: 43
Pin: InstruIn[5] W 3 0.2000 0.4000
Offset: 46
Pin: InstruIn[4] W 3 0.2000 0.4000
Offset: 49
Pin: InstruIn[3] W 3 0.2000 0.4000
Offset: 52
Pin: InstruIn[2] W 3 0.2000 0.4000
Offset: 55
Pin: InstruIn[1] W 3 0.2000 0.4000
Offset: 58
Pin: InstruIn[0] W 3 0.2000 0.4000
Offset: 61
Pin: imm_disp[7] W 3 0.2000 0.4000
Offset: 64
Pin: imm_disp[6] W 3 0.2000 0.4000
Offset: 67
Pin: imm_disp[5] W 3 0.2000 0.4000
Offset: 70
Pin: imm_disp[4] W 3 0.2000 0.4000
Offset: 73
Pin: imm_disp[3] W 3 0.2000 0.4000
Offset: 76
Pin: imm_disp[2] W 3 0.2000 0.4000
Offset: 79
Pin: imm_disp[1] W 3 0.2000 0.4000
Offset: 82
Pin: imm_disp[0] W 3 0.2000 0.4000
Offset: 85
Pin: Br W 3 0.2000 0.4000
Offset: 88
Pin: Jmp W 3 0.2000 0.4000


Offset:  20
Pin: WEN N 4 0.2000 0.4000
Offset: 30
Pin: CEN N 4 0.2000 0.4000
Offset: 40
Pin: clk N 4 0.2000 0.4000

Offset:  14
Pin: DIn[15] S 2 0.2000 0.4000
Offset:  16.5
Pin: DIn[14] S 2 0.2000 0.4000
Offset:  19
Pin: DIn[13] S 2 0.2000 0.4000
Offset:  21.5
Pin: DIn[12] S 2 0.2000 0.4000
Offset:  24
Pin: DIn[11] S 2 0.2000 0.4000
Offset:  26.5
Pin: DIn[10] S 2 0.2000 0.4000
Offset:  29
Pin: DIn[9] S 2 0.2000 0.4000
Offset:  31.5
Pin: DIn[8] S 2 0.2000 0.4000
Offset:  34
Pin: DIn[7] S 2 0.2000 0.4000
Offset:  36.5
Pin: DIn[6] S 2 0.2000 0.4000
Offset:  39
Pin: DIn[5] S 2 0.2000 0.4000
Offset:  41.5
Pin: DIn[4] S 2 0.2000 0.4000
Offset:  44
Pin: DIn[3] S 2 0.2000 0.4000
Offset:  46.5
Pin: DIn[2] S 2 0.2000 0.4000
Offset:  49
Pin: DIn[1] S 2 0.2000 0.4000
Offset:  51.5
Pin: DIn[0] S 2 0.2000 0.4000
Offset:  54
Pin: pc[9]  S 2 0.2000 0.4000
Offset:  56.5
Pin: pc[8] S 2 0.2000 0.4000
Offset:  59
Pin: pc[7] S 2 0.2000 0.4000
Offset:  61.5
Pin: pc[6]  S  2 0.2000 0.4000
Offset:  64
Pin: pc[5]  S 2 0.2000 0.4000
Offset:  66.5
Pin: pc[4]   S 2 0.2000 0.4000
Offset:  69
Pin: pc[3] S  2 0.2000 0.4000
Offset:  71.5
Pin: pc[2] S  2 0.2000 0.4000
Offset:  74
Pin: pc[1] S  2 0.2000 0.4000
Offset:  76.5
Pin: pc[0] S  2 0.2000 0.4000
Offset:  79
Pin: N S  2 0.2000 0.4000
Offset:  81.5
Pin: Z S  2 0.2000 0.4000
Offset:  83
Pin: F S  2 0.2000 0.4000
Offset:  84.5
Pin: C S  2 0.2000 0.4000











