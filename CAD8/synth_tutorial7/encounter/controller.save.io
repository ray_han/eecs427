######################################################
#                                                    #
#  Cadence Design Systems, Inc.                      #
#  Cadence(R) Encounter(TM) IO Assignments           #
#                                                    #
######################################################

Version: 2


Offset:	7.6 
Pin: READB[14] E 5 0.2000 0.4000
Offset:	8 
Pin: CLK        E 3 0.2000 0.4000
Offset:	8.4 
Pin: DIn[5]     E 3 0.2000 0.4000
Offset:	8.8 
Pin: LuiSEL    E 5 0.2000 0.4000
Offset:	9.2 
Pin: AluSEL[1] E 3 0.2000 0.4000
Offset:	9.6 
Pin: DIn[1]     E 3 0.2000 0.4000
Offset:	10 
Pin: DIn[6]     E 3 0.2000 0.4000
Offset:	10.4 
Pin: READA[14] E 5 0.2000 0.4000
Offset:	10.8 
Pin: AluSEL[0] E 3 0.2000 0.4000
Offset:	11.2 
Pin: WE[14]     E 3 0.2000 0.4000
Offset:	11.6 
Pin: DIn[2]     E 3 0.2000 0.4000
Offset:	12 
Pin: Z          E 3 0.2000 0.4000
Offset:	12.4 
Pin: READB[13] E 5 0.2000 0.4000
Offset:	12.8 
Pin: imm8_disp[7]    E 3 0.2000 0.4000
Offset:	13.2 
Pin: DIn[3]     E 3 0.2000 0.4000
Offset:	13.6 
Pin: DIn[4]     E 3 0.2000 0.4000
Offset:	14 
Pin: READA[13] E 5 0.2000 0.4000
Offset:	14.4 
Pin: F          E 3 0.2000 0.4000
Offset:	14.8 
Pin: DIn[7]     E 3 0.2000 0.4000
Offset:	15.2 
Pin: WE[13]     E 3 0.2000 0.4000
Offset:	15.6 
Pin: N          E 3 0.2000 0.4000
Offset:	16 
Pin: READB[12]  E 5 0.2000 0.4000
Offset:	16.4 
Pin: FunISEL[1] E 3 0.2000 0.4000
Offset:	16.8 
Pin: FunISEL[0] E 3 0.2000 0.4000
Offset:	17.2 
Pin: MovSEL     E 3 0.2000 0.4000
Offset:	17.6 
Pin: WE[12]     E 3 0.2000 0.4000
Offset:	18 
Pin: READA[12]  E 5 0.2000 0.4000
Offset:	18.4 
Pin: Amount[0]  E 3 0.2000 0.4000
Offset:	18.8 
Pin: Amount[1]  E 3 0.2000 0.4000
Offset:	19.2 
Pin: imm8_disp[3]    E 3 0.2000 0.4000
Offset:	19.6 
Pin: DIn[8]     E 3 0.2000 0.4000
Offset:	20 
Pin: READB[11]  E 5 0.2000 0.4000
Offset:	20.4 
Pin: C          E 3 0.2000 0.4000
Offset:	20.8 
Pin: READB[15]  E 3 0.2000 0.4000
Offset:	21.2 
Pin: READA[15]  E 3 0.2000 0.4000
Offset:	21.6 
Pin: READA[11]  E 5 0.2000 0.4000
Offset:	22 
Pin: DIn[0]     E 3 0.2000 0.4000
Offset:	22.4 
Pin: WE[11]     E 3 0.2000 0.4000
Offset:	22.8 
Pin: READB[10]  E 5 0.2000 0.4000
Offset:	23.2 
Pin: imm8_disp[5]    E 3 0.2000 0.4000
Offset:	23.6 
Pin: imm8_disp[2]    E 3 0.2000 0.4000
Offset:	24 
Pin: imm8_disp[6]    E 3 0.2000 0.4000
Offset:	24.4 
Pin: imm8_disp[1]    E 3 0.2000 0.4000
Offset:	24.8 
Pin: DIn[9]     E 3 0.2000 0.4000
Offset:	25.2 
Pin: imm8_disp[0]    E 3 0.2000 0.4000
Offset:	25.6 
Pin: WE[10]     E 3 0.2000 0.4000
Offset:	26 
Pin: READA[10] E 5 0.2000 0.4000
Offset:	26.4 
Pin: WE[15]     E 3 0.2000 0.4000
Offset: 26.8 
Pin: imm8_disp[4]	  E 3 0.2000 0.4000
Offset: 27.2 
Pin: DSEL[1]	  E 3 0.2000 0.4000
Offset: 27.6 
Pin: READB[9]  E 5 0.2000 0.4000
Offset: 28 
Pin: DSEL[0]   E 3 0.2000 0.4000
Offset: 28.8 
Pin: READA[9]  E 5 0.2000 0.4000
Offset: 29.2 
Pin: DIn[10]    E 3 0.2000 0.4000
Offset: 29.6 
Pin: WE[9]      E 3 0.2000 0.4000
Offset: 30.4 
Pin: READB[8]  E 5 0.2000 0.4000
Offset: 32
 Pin: SEL1      E 3 0.2000 0.4000
Offset: 33.2 
Pin: READA[8]  E 5 0.2000 0.4000
Offset: 34 
Pin: DIn[11]    E 3 0.2000 0.4000
Offset: 34.4
 Pin: WE[8]      E 3 0.2000 0.4000
Offset: 35.2 
Pin: READB[7]  E 5 0.2000 0.4000
Offset: 36.8 
Pin: WE[7]      E 3 0.2000 0.4000
Offset: 37.2 
Pin: READA[7]  E 5 0.2000 0.4000
Offset: 38 
Pin: READB[6]  E 5 0.2000 0.4000
Offset: 38.8 
Pin: DIn[12]    E 3 0.2000 0.4000
Offset: 40 
Pin: WE[6]      E 3 0.2000 0.4000
Offset: 40.8 
Pin: READA[6]  E 5 0.2000 0.4000
Offset: 42.8 
Pin: READB[5]  E 5 0.2000 0.4000
Offset: 43.6 
Pin: DIn[13]    E 3 0.2000 0.4000
Offset: 44 
Pin: WE[1]      E 3 0.2000 0.4000
Offset: 44.4 
Pin: READA[5]  E 5 0.2000 0.4000
Offset: 44.8 
Pin: WE[5]      E 3 0.2000 0.4000
Offset: 45.2 
Pin: READA[1]  E 3 0.2000 0.4000
Offset: 45.6 
Pin: READB[4]  E 5 0.2000 0.4000
Offset: 46 
Pin: DOut[12]      E 3 0.2000 0.4000
Offset: 46.4 
Pin: READA[2]  E 3 0.2000 0.4000
Offset: 46.8 
Pin: DOut[11]      E 3 0.2000 0.4000
Offset: 47.2 
Pin: DOut[10]      E 3 0.2000 0.4000
Offset: 47.6 
Pin: READB[0]  E 3 0.2000 0.4000
Offset: 48 
Pin: READA[4]  E 5 0.2000 0.4000
Offset: 48.4 
Pin: DIn[14]    E 3 0.2000 0.4000
Offset: 48.8 
Pin: WE[4]      E 3 0.2000 0.4000
Offset: 49.2 
Pin: DOut[9]       E 3 0.2000 0.4000
Offset: 49.6 
Pin: READA[0]  E 3 0.2000 0.4000
Offset: 50 
Pin: DOut[4]       E 3 0.2000 0.4000
Offset: 50.4 
Pin: READB[3]  E 5 0.2000 0.4000
Offset: 50.8 
Pin: DOut[8]       E 3 0.2000 0.4000
Offset: 51.2 
Pin: WE[3]      E 3 0.2000 0.4000
Offset: 51.6 
Pin: READA[3]  E 5 0.2000 0.4000
Offset: 52 
Pin: DOut[7]       E 3 0.2000 0.4000
Offset: 52.4 
Pin: WE[0]      E 3 0.2000 0.4000
Offset: 52.8 
Pin: DOut[15]      E 5 0.2000 0.4000
Offset: 53.2 
Pin: DIn[15]    E 3 0.2000 0.4000
Offset: 53.6 
Pin: READB[2]  E 3 0.2000 0.4000
Offset: 54 
Pin: DOut[6]       E 3 0.2000 0.4000
Offset: 54.4 
Pin: DOut[5]       E 3 0.2000 0.4000
Offset: 54.8 
Pin: DOut[3]       E 3 0.2000 0.4000
Offset: 55.2 
Pin: WEMaster  E 3 0.2000 0.4000
Offset: 55.6 
Pin: DOut[14]      E 5 0.2000 0.4000
Offset: 56 
Pin: WE[2]      E 3 0.2000 0.4000
Offset: 56.4 
Pin: DOut[13]      E 5 0.2000 0.4000
Offset: 56.8 
Pin: DOut[2]       E 3 0.2000 0.4000
Offset: 57.2 
Pin: DOut[1]       E 3 0.2000 0.4000
Offset: 57.6 
Pin: DOut[0]       E 3 0.2000 0.4000
Offset: 58 
Pin: READB[1]  E 3 0.2000 0.4000



Offset: 9 	  
Pin: rSrcIn[0 ]	S 2 0.2000 0.4000
Offset: 12	  
Pin: rSrcIn[1 ]	S 2 0.2000 0.4000
Offset: 15	  
Pin: rSrcIn[2 ]	S 2 0.2000 0.4000
Offset: 18	  
Pin: rSrcIn[3 ]	S 2 0.2000 0.4000
Offset: 21	  
Pin: rSrcIn[4 ]	S 2 0.2000 0.4000
Offset: 24	  
Pin: rSrcIn[5 ]	S 2 0.2000 0.4000
Offset: 27	 
 Pin: rSrcIn[6 ]	S 2 0.2000 0.4000
Offset: 30	 
 Pin: rSrcIn[7 ]	S 2 0.2000 0.4000
Offset: 33	  
Pin: rSrcIn[8 ]	S 2 0.2000 0.4000
Offset: 36	  
Pin: rSrcIn[9 ]	S 2 0.2000 0.4000
Offset: 39	  
Pin: rSrcIn[10]	S 2 0.2000 0.4000
Offset: 42	  
Pin: rSrcIn[11]	S 2 0.2000 0.4000
Offset: 45	 
 Pin: rSrcIn[12]	S 2 0.2000 0.4000
Offset: 48	  
Pin: rSrcIn[13]	S 2 0.2000 0.4000
Offset: 51	  
Pin: rSrcIn[14]	S 2 0.2000 0.4000
Offset: 54	  
Pin: rSrcIn[15]	S 2 0.2000 0.4000
Offset: 57	  
Pin: rSrcIn[16]	S 2 0.2000 0.4000
Offset: 60	  
Pin: Br         S 2 0.2000 0.4000
Offset: 63	  
Pin: Jmp        S 2 0.2000 0.4000
Offset: 66	  
Pin: Stall      S 2 0.2000 0.4000
Offset: 69	  
Pin: pc[0]      S 2 0.2000 0.4000
Offset: 72	  
Pin: pc[1]      S 2 0.2000 0.4000
Offset: 75	  
Pin: pc[2]      S 2 0.2000 0.4000
Offset: 78	  
Pin: pc[3]      S 2 0.2000 0.4000
Offset: 81	 
 Pin: pc[4]      S 2 0.2000 0.4000
Offset: 84	 
 Pin: pc[5]      S 2 0.2000 0.4000
Offset: 87	  
Pin: pc[6]      S 2 0.2000 0.4000
Offset: 90	  
Pin: pc[7]      S 2 0.2000 0.4000
Offset: 93	  
Pin: pc[8]      S 2 0.2000 0.4000
Offset: 96	  
Pin: pc[9]      S 2 0.2000 0.4000

Offset: 0  	  
Pin: rDestIn[0 ] N 2 0.2000 0.4000
Offset: 2  	 
 Pin: rDestIn[1 ] N 2 0.2000 0.4000
Offset: 4  	 
 Pin: rDestIn[2 ] N 2 0.2000 0.4000
Offset: 6  	
  Pin: rDestIn[3 ] N 2 0.2000 0.4000
Offset: 8  	
  Pin: rDestIn[4 ] N 2 0.2000 0.4000
Offset: 10 	 
 Pin: rDestIn[5 ] N 2 0.2000 0.4000
Offset: 12 	  
Pin: rDestIn[6 ] N 2 0.2000 0.4000
Offset: 14 	
  Pin: rDestIn[7 ] N 2 0.2000 0.4000
Offset: 16 	
  Pin: rDestIn[8 ] N 2 0.2000 0.4000
Offset: 18 	 
 Pin: rDestIn[9 ] N 2 0.2000 0.4000
Offset: 20 	
  Pin: rDestIn[10] N 2 0.2000 0.4000
Offset: 22 	 
 Pin: rDestIn[11] N 2 0.2000 0.4000
Offset: 24 	 
 Pin: rDestIn[12] N 2 0.2000 0.4000
Offset: 26 	
  Pin: rDestIn[13] N 2 0.2000 0.4000
Offset: 28 	 
 Pin: rDestIn[14] N 2 0.2000 0.4000
Offset: 30 	 
 Pin: rDestIn[15] N 2 0.2000 0.4000
Offset: 32 	
  Pin: WEN         N 2 0.2000 0.4000
Offset: 34 	 
 Pin: CEN         N 2 0.2000 0.4000
Offset: 36      
Pin: instruIn[0 ]  N 2 0.2000 0.4000
Offset: 38     
  Pin: instruIn[1 ]  N 2 0.2000 0.4000
Offset: 40       
Pin: instruIn[2 ]  N 2 0.2000 0.4000
Offset: 42       
Pin: instruIn[3 ]  N 2 0.2000 0.4000
Offset: 44      
 Pin: instruIn[4 ]  N 2 0.2000 0.4000
Offset: 46       
Pin: instruIn[5 ]  N 2 0.2000 0.4000
Offset: 48      
 Pin: instruIn[6 ]  N 2 0.2000 0.4000
Offset: 50   
    Pin: instruIn[7 ]  N 2 0.2000 0.4000
Offset: 52   
    Pin: instruIn[8 ]  N 2 0.2000 0.4000
Offset: 54      
 Pin: instruIn[9 ]  N 2 0.2000 0.4000
Offset: 56   
    Pin: instruIn[10]  N 2 0.2000 0.4000
Offset: 58     
  Pin: instruIn[11]  N 2 0.2000 0.4000
Offset: 60      
 Pin: instruIn[12]  N 2 0.2000 0.4000
Offset: 62   
    Pin: instruIn[13]  N 2 0.2000 0.4000
Offset: 64   
    Pin: instruIn[14]  N 2 0.2000 0.4000
Offset: 66    
   Pin: instruIn[15]  N 2 0.2000 0.4000
Offset: 68   
    Pin: WR_EN_SRAM    N 2 0.2000 0.4000
Offset: 70   
    Pin: WR_Data[0 ]   N 2 0.2000 0.4000
Offset: 72       
Pin: WR_Data[1 ]   N 2 0.2000 0.4000
Offset: 74    
   Pin: WR_Data[2 ]   N 2 0.2000 0.4000
Offset: 76       
Pin: WR_Data[3 ]   N 2 0.2000 0.4000
Offset: 78       
Pin: WR_Data[4 ]   N 2 0.2000 0.4000
Offset: 80       
Pin: WR_Data[5 ]   N 2 0.2000 0.4000
Offset: 82      
 Pin: WR_Data[6 ]   N 2 0.2000 0.4000
Offset: 84      
 Pin: WR_Data[7 ]   N 2 0.2000 0.4000
Offset: 86       
Pin: WR_Data[8 ]   N 2 0.2000 0.4000
Offset: 88       
Pin: WR_Data[9 ]   N 2 0.2000 0.4000
Offset: 90      
 Pin: WR_Data[10]   N 2 0.2000 0.4000
Offset: 92      
 Pin: WR_Data[11]   N 2 0.2000 0.4000
Offset: 94       
Pin: WR_Data[12]   N 2 0.2000 0.4000
Offset: 96     
  Pin: WR_Data[13]   N 2 0.2000 0.4000
Offset: 98     
  Pin: WR_Data[14]   N 2 0.2000 0.4000
Offset: 100     
 Pin: WR_Data[15]   N 2 0.2000 0.4000




Offset: 7.6	
Pin: Dsram[0 ]     	W 3 0.2000 0.4000
Offset: 8	
Pin: Dsram[1 ]     		W 3 0.2000 0.4000
Offset: 8.4	
Pin: Dsram[2 ]     	W 3 0.2000 0.4000
Offset: 8.8	
Pin: Dsram[3 ]     	W 3 0.2000 0.4000
Offset: 9.2	
Pin: Dsram[4 ]     	W 3 0.2000 0.4000
Offset: 9.6	
Pin: Dsram[5 ]     	W 3 0.2000 0.4000
Offset: 10	
Pin: Dsram[6 ]     	W 3 0.2000 0.4000
Offset: 10.4	
Pin: Dsram[7 ]    W 3 0.2000 0.4000
Offset: 10.8	
Pin: Dsram[8 ]    W 3 0.2000 0.4000
Offset: 11.2	
Pin: Dsram[9 ]     	W 3 0.2000 0.4000
Offset: 11.6	
Pin: Dsram[10]    W 3 0.2000 0.4000
Offset: 12	
Pin: Dsram[11]     	W 3 0.2000 0.4000
Offset: 12.4	
Pin: Dsram[12]    W 3 0.2000 0.4000
Offset: 12.8	
Pin: Dsram[13]    W 3 0.2000 0.4000
Offset: 13.2	
Pin: Dsram[14]     	W 3 0.2000 0.4000
Offset: 13.6	
Pin: Dsram[15]    W 3 0.2000 0.4000
Offset: 14	
Pin: RWL_P[0 ]    	W 3 0.2000 0.4000
Offset: 14.4	
Pin: RWL_P[1 ]    W 3 0.2000 0.4000
Offset: 14.8	
Pin: RWL_P[2 ]    W 3 0.2000 0.4000
Offset: 15.2	
Pin: RWL_P[3 ]      W 3 0.2000 0.4000
Offset: 15.6	
Pin: RWL_P[4 ]    W 3 0.2000 0.4000
Offset: 16	
Pin: RWL_P[5 ]     	W 3 0.2000 0.4000
Offset: 16.4	
Pin: RWL_P[6 ]    W 3 0.2000 0.4000
Offset: 16.8	
Pin: RWL_P[7 ]    W 3 0.2000 0.4000
Offset: 17.2	
Pin: RWL_P[8 ]     	W 3 0.2000 0.4000
Offset: 17.6	
Pin: RWL_P[9 ]    W 3 0.2000 0.4000
Offset: 18	
Pin: RWL_P[10]     	W 3 0.2000 0.4000
Offset: 18.4	
Pin: RWL_P[11]    W 3 0.2000 0.4000
Offset: 18.8	
Pin: RWL_P[12]    W 3 0.2000 0.4000
Offset: 19.2	
Pin: RWL_P[13]     	W 3 0.2000 0.4000
Offset: 19.6	
Pin: RWL_P[14]    W 3 0.2000 0.4000
Offset: 20	
Pin: RWL_P[15]     	W 3 0.2000 0.4000
Offset: 20.4	
Pin: RWL_N[0 ]    W 3 0.2000 0.4000
Offset: 20.8	
Pin: RWL_N[1 ]    W 3 0.2000 0.4000
Offset: 21.2	
Pin: RWL_N[2 ]     	W 3 0.2000 0.4000
Offset: 21.6	
Pin: RWL_N[3 ]    W 3 0.2000 0.4000
Offset: 22	
Pin: RWL_N[4 ]        W 3 0.2000 0.4000
Offset: 22.4	
Pin: RWL_N[5 ]    W 3 0.2000 0.4000
Offset: 22.8	
Pin: RWL_N[6 ]    W 3 0.2000 0.4000
Offset: 23.2	
Pin: RWL_N[7 ]      W 3 0.2000 0.4000
Offset: 23.6	
Pin: RWL_N[8 ]    W 3 0.2000 0.4000
Offset: 24	
Pin: RWL_N[9 ]     W 3 0.2000 0.4000
Offset: 24.4	
Pin: RWL_N[10]     W 3 0.2000 0.4000
Offset: 24.8	
Pin: RWL_N[11]     W 3 0.2000 0.4000
Offset: 25.2	
Pin: RWL_N[12]     W 3 0.2000 0.4000
Offset: 25.6	
Pin: RWL_N[13]     W 3 0.2000 0.4000
Offset: 26	
Pin: RWL_N[14]     W 3 0.2000 0.4000
Offset: 26.4	
Pin: RWL_N[15]     W 3 0.2000 0.4000
Offset: 26.8	
Pin: RWLB_P[0 ]    W 3 0.2000 0.4000
Offset: 27.2	
Pin: RWLB_P[1 ]    W 3 0.2000 0.4000
Offset: 27.6	
Pin: RWLB_P[2 ]    W 3 0.2000 0.4000
Offset: 28	
Pin: RWLB_P[3 ]    W 3 0.2000 0.4000
Offset: 28.8	
Pin: RWLB_P[4 ]    W 3 0.2000 0.4000
Offset: 29.2	
Pin: RWLB_P[5 ]    W 3 0.2000 0.4000
Offset: 29.6	
Pin: RWLB_P[6 ]    W 3 0.2000 0.4000
Offset: 30.4	
Pin: RWLB_P[7 ]    W 3 0.2000 0.4000
Offset: 32	
Pin: RWLB_P[8 ]    W 3 0.2000 0.4000
Offset: 33.2	
Pin: RWLB_P[9 ]    W 3 0.2000 0.4000
Offset: 34	
Pin: RWLB_P[10]    W 3 0.2000 0.4000
Offset: 34.4	
Pin: RWLB_P[11]    W 3 0.2000 0.4000
Offset: 35.2	
Pin: RWLB_P[12]    W 3 0.2000 0.4000
Offset: 36.8	
Pin: RWLB_P[13]    W 3 0.2000 0.4000
Offset: 37.2	
Pin: RWLB_P[14]    W 3 0.2000 0.4000
Offset: 38	
Pin: RWLB_P[15]    W 3 0.2000 0.4000
Offset: 38.8	
Pin: RWLB_N[0 ]    W 3 0.2000 0.4000
Offset: 40	
Pin: RWLB_N[1 ]    W 3 0.2000 0.4000
Offset: 40.8	
Pin: RWLB_N[2 ]    W 3 0.2000 0.4000
Offset: 42.8	
Pin: RWLB_N[3 ]    W 3 0.2000 0.4000
Offset: 43.6	
Pin: RWLB_N[4 ]    W 3 0.2000 0.4000
Offset: 44	
Pin: RWLB_N[5 ]    W 3 0.2000 0.4000
Offset: 44.4	
Pin: RWLB_N[6 ]    W 3 0.2000 0.4000
Offset: 44.8	
Pin: RWLB_N[7 ]    W 3 0.2000 0.4000
Offset: 45.2	
Pin: RWLB_N[8 ]    W 3 0.2000 0.4000
Offset: 45.6	
Pin: RWLB_N[9 ]    W 3 0.2000 0.4000
Offset: 46	
Pin: RWLB_N[10]    W 3 0.2000 0.4000
Offset: 46.4	
Pin: RWLB_N[11]    W 3 0.2000 0.4000
Offset: 46.8	
Pin: RWLB_N[12]    W 3 0.2000 0.4000
Offset: 47.2	
Pin: RWLB_N[13]    W 3 0.2000 0.4000
Offset: 47.6	
Pin: RWLB_N[14]    W 3 0.2000 0.4000
Offset: 48	
Pin:  RWLB_N[15]    W 3 0.2000 0.4000
Offset: 48.4	
Pin: Dxac[0]       W 3 0.2000 0.4000
Offset: 48.8	
Pin: Dxac[1]       W 3 0.2000 0.4000
Offset: 49.2	
Pin: Dxac[2]       W 3 0.2000 0.4000
Offset: 49.6	
Pin: Dxac[3]       W 3 0.2000 0.4000
Offset: 50	
Pin: Dxac[4]       W 3 0.2000 0.4000
Offset: 50.4	
Pin: Dxac[5]       W 3 0.2000 0.4000
Offset: 50.8	
Pin: REN_SRAM      W 3 0.2000 0.4000
Offset: 51.2	
Pin: RENB_SRAM     W 3 0.2000 0.4000
Offset: 51.6	
Pin: Col_Sel[0]    W 3 0.2000 0.4000
Offset: 52	
Pin: Col_Sel[1]    W 3 0.2000 0.4000
Offset: 52.4	
Pin: Col_Sel[2]    W 3 0.2000 0.4000
Offset: 52.8	
Pin: Col_Sel[3]    W 3 0.2000 0.4000
Offset: 53.2	
Pin: Col_Sel[4 ]   W 3 0.2000 0.4000
Offset: 53.6	
Pin: Col_Sel[5 ]   W 3 0.2000 0.4000
Offset: 54	
Pin: Col_Sel[6 ]   W 3 0.2000 0.4000
Offset: 54.4	
Pin: Col_Sel[7 ]   W 3 0.2000 0.4000
Offset: 54.8	
Pin: Col_Sel[8 ]   W 3 0.2000 0.4000
Offset: 55.2	
Pin: Col_Sel[9 ]   W 3 0.2000 0.4000
Offset: 55.6	
Pin: Col_Sel[10]   W 3 0.2000 0.4000
Offset: 56	
Pin: Col_Sel[11]   W 3 0.2000 0.4000
Offset: 56.4	
Pin: Col_Sel[12]   W 3 0.2000 0.4000
Offset: 56.8	
Pin: Col_Sel[13]   W 3 0.2000 0.4000
Offset: 57.2	
Pin: Col_Sel[14]   W 3 0.2000 0.4000
Offset: 57.6	
Pin: Col_Sel[15]   W 3 0.2000 0.4000



        




