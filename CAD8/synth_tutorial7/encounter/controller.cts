###############################################################
#  Generated by:      Cadence Encounter 14.27-s035_1
#  OS:                Linux x86_64(Host ID pierb507p33.engin.umich.edu)
#  Generated on:      Sun Dec  9 20:17:58 2018
#  Design:            controller
#  Command:           createClockTreeSpec -output controller.cts
###############################################################
#
# Encounter(R) Clock Synthesis Technology File Format
#

#-- MacroModel --
#MacroModel pin <pin> <maxRiseDelay> <minRiseDelay> <maxFallDelay> <minFallDelay> <inputCap>


#-- Special Route Type --
#RouteTypeName specialRoute
#TopPreferredLayer 4
#BottomPreferredLayer 3
#PreferredExtraSpace 1
#End

#-- Regular Route Type --
#RouteTypeName regularRoute
#TopPreferredLayer 4
#BottomPreferredLayer 3
#PreferredExtraSpace 1
#End

#-- Clock Group --
#ClkGroup
#+ <clockName>


#Excluded pin under clk
GlobalExcludedPin
+ U72/A

#------------------------------------------------------------
# Clock Root   : clk
# Clock Name   : clk
# Clock Period : 3ns
#------------------------------------------------------------
AutoCTSRootPin clk
Period         3ns
MaxDelay       0.01ns # sdc driven default
MinDelay       0ns # sdc driven default
MaxSkew        100ps # set_clock_uncertainty
SinkMaxTran    100ps # set_clock_transition
BufMaxTran     100ps # set_clock_transition
Buffer         CLKBUFX20TR CLKBUFX16TR CLKBUFX8TR CLKBUFX12TR CLKINVX16TR CLKINVX6TR
NoGating       NO
DetailReport   YES
#SetDPinAsSync  NO
#SetIoPinAsSync NO
#SetASyncSRPinAsSync  NO
#SetTriStEnPinAsSync NO
#SetBBoxPinAsSync NO
RouteClkNet    YES
PostOpt        YES
OptAddBuffer   YES
#RouteType      specialRoute
#LeafRouteType  regularRoute
END

