/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06
// Date      : Fri Nov 16 08:48:46 2018
/////////////////////////////////////////////////////////////


module mult ( clk, instruIn, DIn, pc, N, Z, F, C, DOut, ReadA, ReadB, WE, 
        WEMaster, imm8_disp, DSEL, AluSEL, Amount, LuiSEL, FunISEL, MovSEL, 
        SEL1, CEN, WEN, Br, Jmp );
  input [15:0] instruIn;
  input [15:0] DIn;
  input [9:0] pc;
  output [15:0] DOut;
  output [15:0] ReadA;
  output [15:0] ReadB;
  output [15:0] WE;
  output [7:0] imm8_disp;
  output [1:0] DSEL;
  output [1:0] AluSEL;
  output [1:0] Amount;
  output [1:0] FunISEL;
  input clk, N, Z, F, C;
  output WEMaster, LuiSEL, MovSEL, SEL1, CEN, WEN, Br, Jmp;
  wire   n125, rDest_3_0, rDest_2_0, rDest_1_0, rDest_0_0, opCode_3_0,
         opCode_2_0, opCode_1_0, opCode_0_0, N63, N64, N65, N66, N67, N68, N69,
         N70, N71, N72, N73, N74, N75, N76, N77, N78, NOUT, ZOUT, COUT, N135,
         N188, N189, N190, N280, n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n29,
         n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43,
         n44, n45, n46, n47, n48, n49, n630, n640, n650, n660, n670, n680,
         n700, n710, n720, n730, n740, n750, n760, n770, n780, n79, n80, n81,
         n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n94, n95,
         n96, n97, n98, n99, n100, n101, n102, n103, n104, n105, n106, n107,
         n108, n109, n110, n111, n112, n113, n114, n115, n116, n117, n118,
         n119, n120, n121, n122, n123, n124;

  DFFXLTR FOUT_reg ( .D(F), .CK(clk), .QN(N280) );
  CLKINVX2TR U22 ( .A(clk), .Y(WEMaster) );
  DFFQXLTR NOUT_reg ( .D(n630), .CK(clk), .Q(NOUT) );
  DFFXLTR instruReg_reg_14_0 ( .D(instruIn[14]), .CK(clk), .Q(opCode_2_0), 
        .QN(n93) );
  DFFXLTR instruReg_reg_13_0 ( .D(instruIn[13]), .CK(clk), .Q(opCode_1_0), 
        .QN(n94) );
  DFFXLTR COUT_reg ( .D(C), .CK(clk), .Q(COUT) );
  DFFXLTR WE_reg_15_0 ( .D(N78), .CK(clk), .Q(WE[15]) );
  DFFXLTR WE_reg_14_0 ( .D(N77), .CK(clk), .Q(WE[14]) );
  DFFXLTR WE_reg_13_0 ( .D(N76), .CK(clk), .Q(WE[13]) );
  DFFXLTR WE_reg_12_0 ( .D(N75), .CK(clk), .Q(WE[12]) );
  DFFXLTR WE_reg_11_0 ( .D(N74), .CK(clk), .Q(WE[11]) );
  DFFXLTR WE_reg_10_0 ( .D(N73), .CK(clk), .Q(WE[10]) );
  DFFXLTR WE_reg_9_0 ( .D(N72), .CK(clk), .Q(WE[9]) );
  DFFXLTR WE_reg_8_0 ( .D(N71), .CK(clk), .Q(WE[8]) );
  DFFXLTR WE_reg_7_0 ( .D(N70), .CK(clk), .Q(WE[7]) );
  DFFXLTR WE_reg_6_0 ( .D(N69), .CK(clk), .Q(WE[6]) );
  DFFXLTR WE_reg_5_0 ( .D(N68), .CK(clk), .Q(WE[5]) );
  DFFXLTR WE_reg_4_0 ( .D(N67), .CK(clk), .Q(WE[4]) );
  DFFXLTR WE_reg_3_0 ( .D(N66), .CK(clk), .Q(WE[3]) );
  DFFXLTR WE_reg_2_0 ( .D(N65), .CK(clk), .Q(WE[2]) );
  DFFXLTR WE_reg_1_0 ( .D(N64), .CK(clk), .Q(WE[1]) );
  DFFXLTR WE_reg_0_0 ( .D(N63), .CK(clk), .Q(WE[0]) );
  DFFX1TR instruReg_reg_6_0 ( .D(instruIn[6]), .CK(clk), .Q(imm8_disp[6]), 
        .QN(n102) );
  DFFX1TR instruReg_reg_4_0 ( .D(instruIn[4]), .CK(clk), .Q(imm8_disp[4]), 
        .QN(n104) );
  DFFX1TR instruReg_reg_9_0 ( .D(instruIn[9]), .CK(clk), .Q(rDest_1_0), .QN(
        n99) );
  DFFX1TR instruReg_reg_11_0 ( .D(instruIn[11]), .CK(clk), .Q(rDest_3_0), .QN(
        n97) );
  DFFX1TR instruReg_reg_8_0 ( .D(instruIn[8]), .CK(clk), .Q(rDest_0_0), .QN(
        n100) );
  DFFX1TR instruReg_reg_7_0 ( .D(instruIn[7]), .CK(clk), .Q(n125), .QN(n101)
         );
  DFFX1TR ZOUT_reg ( .D(Z), .CK(clk), .Q(ZOUT) );
  DFFX1TR instruReg_reg_12_0 ( .D(instruIn[12]), .CK(clk), .Q(opCode_0_0) );
  DFFX1TR instruReg_reg_15_0 ( .D(instruIn[15]), .CK(clk), .Q(opCode_3_0), 
        .QN(n90) );
  DFFX1TR instruReg_reg_10_0 ( .D(instruIn[10]), .CK(clk), .Q(rDest_2_0), .QN(
        n98) );
  DFFX1TR instruReg_reg_3_0 ( .D(instruIn[3]), .CK(clk), .Q(imm8_disp[3]), 
        .QN(n105) );
  DFFX1TR instruReg_reg_0_0 ( .D(instruIn[0]), .CK(clk), .Q(imm8_disp[0]), 
        .QN(n108) );
  DFFX1TR instruReg_reg_2_0 ( .D(instruIn[2]), .CK(clk), .Q(imm8_disp[2]), 
        .QN(n106) );
  DFFX1TR instruReg_reg_1_0 ( .D(instruIn[1]), .CK(clk), .Q(imm8_disp[1]), 
        .QN(n107) );
  DFFX1TR instruReg_reg_5_0 ( .D(instruIn[5]), .CK(clk), .Q(imm8_disp[5]), 
        .QN(n103) );
  INVX2TR U114 ( .A(1'b1), .Y(CEN) );
  CLKBUFX2TR U116 ( .A(N), .Y(n630) );
  OAI32XLTR U117 ( .A0(n98), .A1(n99), .A2(n6), .B0(rDest_2_0), .B1(n5), .Y(n7) );
  NOR3XLTR U118 ( .A(imm8_disp[4]), .B(imm8_disp[7]), .C(imm8_disp[5]), .Y(n44) );
  CLKBUFX2TR U119 ( .A(n98), .Y(n640) );
  CLKINVX2TR U120 ( .A(n760), .Y(n650) );
  CLKINVX2TR U121 ( .A(n93), .Y(n660) );
  CLKINVX2TR U122 ( .A(n710), .Y(n670) );
  CLKBUFX2TR U123 ( .A(n90), .Y(n680) );
  CLKINVX2TR U124 ( .A(n101), .Y(imm8_disp[7]) );
  CLKBUFX2TR U125 ( .A(opCode_0_0), .Y(n700) );
  CLKBUFX2TR U126 ( .A(rDest_0_0), .Y(n710) );
  CLKINVX2TR U127 ( .A(n94), .Y(n720) );
  CLKBUFX2TR U128 ( .A(opCode_3_0), .Y(n730) );
  CLKBUFX2TR U129 ( .A(n39), .Y(n740) );
  CLKBUFX2TR U130 ( .A(n39), .Y(n750) );
  CLKBUFX2TR U131 ( .A(rDest_3_0), .Y(n760) );
  CLKBUFX2TR U132 ( .A(rDest_1_0), .Y(n770) );
  CLKBUFX2TR U133 ( .A(n86), .Y(n89) );
  CLKINVX2TR U134 ( .A(n89), .Y(n780) );
  CLKINVX2TR U135 ( .A(n89), .Y(n79) );
  CLKINVX2TR U136 ( .A(n89), .Y(n80) );
  CLKINVX2TR U137 ( .A(n89), .Y(n81) );
  CLKINVX2TR U138 ( .A(n32), .Y(n87) );
  CLKINVX2TR U139 ( .A(n87), .Y(n82) );
  CLKINVX2TR U140 ( .A(n87), .Y(n83) );
  CLKINVX2TR U141 ( .A(n87), .Y(n84) );
  CLKINVX2TR U142 ( .A(n87), .Y(n85) );
  NAND2X1TR U143 ( .A(n770), .B(n100), .Y(n111) );
  NAND2X1TR U144 ( .A(n760), .B(n98), .Y(n115) );
  NAND2X1TR U145 ( .A(rDest_2_0), .B(n97), .Y(n113) );
  NAND2X1TR U146 ( .A(n710), .B(n99), .Y(n116) );
  NAND2X1TR U147 ( .A(imm8_disp[0]), .B(n107), .Y(n124) );
  NAND2X1TR U148 ( .A(imm8_disp[3]), .B(n106), .Y(n123) );
  NAND2X1TR U149 ( .A(imm8_disp[1]), .B(imm8_disp[0]), .Y(n120) );
  NAND2X1TR U150 ( .A(imm8_disp[2]), .B(n105), .Y(n121) );
  NAND2X1TR U151 ( .A(imm8_disp[1]), .B(n108), .Y(n119) );
  NAND2X1TR U152 ( .A(n770), .B(n710), .Y(n112) );
  NAND2X1TR U153 ( .A(imm8_disp[2]), .B(imm8_disp[3]), .Y(n117) );
  NAND2X1TR U154 ( .A(rDest_2_0), .B(n760), .Y(n109) );
  CLKINVX2TR U155 ( .A(FunISEL[0]), .Y(n88) );
  NOR2X1TR U156 ( .A(n114), .B(n110), .Y(ReadB[0]) );
  NOR2X1TR U157 ( .A(n122), .B(n118), .Y(ReadA[0]) );
  NAND2X1TR U158 ( .A(n41), .B(n90), .Y(FunISEL[0]) );
  NOR2BX1TR U159 ( .AN(N135), .B(n34), .Y(Br) );
  OAI31X1TR U160 ( .A0(n10), .A1(n97), .A2(n640), .B0(n95), .Y(N135) );
  AOI22X1TR U161 ( .A0(N188), .A1(n9), .B0(N189), .B1(n670), .Y(n10) );
  AND3X2TR U162 ( .A(n42), .B(n104), .C(n103), .Y(n37) );
  CLKAND2X2TR U163 ( .A(n43), .B(n90), .Y(n42) );
  NOR2BX1TR U164 ( .AN(ReadB[0]), .B(n82), .Y(N63) );
  NOR2BX1TR U165 ( .AN(ReadB[1]), .B(n82), .Y(N64) );
  NOR2BX1TR U166 ( .AN(ReadB[2]), .B(n82), .Y(N65) );
  NOR2BX1TR U167 ( .AN(ReadB[3]), .B(n82), .Y(N66) );
  NOR2BX1TR U168 ( .AN(ReadB[4]), .B(n85), .Y(N67) );
  NOR2BX1TR U169 ( .AN(ReadB[5]), .B(n84), .Y(N68) );
  NOR2BX1TR U170 ( .AN(ReadB[6]), .B(n83), .Y(N69) );
  NOR2BX1TR U171 ( .AN(ReadB[7]), .B(n85), .Y(N70) );
  NOR2BX1TR U172 ( .AN(ReadB[8]), .B(n84), .Y(N71) );
  NOR2BX1TR U173 ( .AN(ReadB[9]), .B(n83), .Y(N72) );
  NOR2BX1TR U174 ( .AN(ReadB[10]), .B(n85), .Y(N73) );
  NOR2BX1TR U175 ( .AN(ReadB[11]), .B(n84), .Y(N74) );
  NOR2BX1TR U176 ( .AN(ReadB[12]), .B(n83), .Y(N75) );
  NOR2BX1TR U177 ( .AN(ReadB[13]), .B(n85), .Y(N76) );
  NOR2BX1TR U178 ( .AN(ReadB[14]), .B(n84), .Y(N77) );
  NOR2BX1TR U179 ( .AN(ReadB[15]), .B(n83), .Y(N78) );
  NAND2X1TR U180 ( .A(n100), .B(n99), .Y(n114) );
  NAND2X1TR U181 ( .A(n108), .B(n107), .Y(n122) );
  NAND2X1TR U182 ( .A(n98), .B(n97), .Y(n110) );
  NAND2X1TR U183 ( .A(n106), .B(n105), .Y(n118) );
  NOR3X1TR U184 ( .A(n94), .B(n90), .C(n31), .Y(LuiSEL) );
  NOR2X1TR U185 ( .A(n114), .B(n113), .Y(ReadB[4]) );
  NOR2X1TR U186 ( .A(n114), .B(n109), .Y(ReadB[12]) );
  NOR2X1TR U187 ( .A(n111), .B(n110), .Y(ReadB[2]) );
  NOR2X1TR U188 ( .A(n111), .B(n109), .Y(ReadB[14]) );
  NOR2X1TR U189 ( .A(n116), .B(n110), .Y(ReadB[1]) );
  NOR2X1TR U190 ( .A(n116), .B(n113), .Y(ReadB[5]) );
  NOR2X1TR U191 ( .A(n116), .B(n115), .Y(ReadB[9]) );
  NOR2X1TR U192 ( .A(n116), .B(n109), .Y(ReadB[13]) );
  NOR2X1TR U193 ( .A(n115), .B(n114), .Y(ReadB[8]) );
  NOR2X1TR U194 ( .A(n115), .B(n111), .Y(ReadB[10]) );
  NOR2X1TR U195 ( .A(n115), .B(n112), .Y(ReadB[11]) );
  NOR2X1TR U196 ( .A(n113), .B(n111), .Y(ReadB[6]) );
  NOR2X1TR U197 ( .A(n113), .B(n112), .Y(ReadB[7]) );
  NOR2X1TR U198 ( .A(n112), .B(n110), .Y(ReadB[3]) );
  NOR2X1TR U199 ( .A(n112), .B(n109), .Y(ReadB[15]) );
  NOR2X1TR U200 ( .A(n124), .B(n118), .Y(ReadA[1]) );
  NOR2X1TR U201 ( .A(n124), .B(n121), .Y(ReadA[5]) );
  NOR2X1TR U202 ( .A(n124), .B(n123), .Y(ReadA[9]) );
  NOR2X1TR U203 ( .A(n124), .B(n117), .Y(ReadA[13]) );
  NOR2X1TR U204 ( .A(n123), .B(n122), .Y(ReadA[8]) );
  NOR2X1TR U205 ( .A(n123), .B(n119), .Y(ReadA[10]) );
  NOR2X1TR U206 ( .A(n123), .B(n120), .Y(ReadA[11]) );
  NOR2X1TR U207 ( .A(n122), .B(n121), .Y(ReadA[4]) );
  NOR2X1TR U208 ( .A(n122), .B(n117), .Y(ReadA[12]) );
  NOR2X1TR U209 ( .A(n119), .B(n118), .Y(ReadA[2]) );
  NOR2X1TR U210 ( .A(n120), .B(n118), .Y(ReadA[3]) );
  NOR2X1TR U211 ( .A(n121), .B(n119), .Y(ReadA[6]) );
  NOR2X1TR U212 ( .A(n121), .B(n120), .Y(ReadA[7]) );
  NOR2X1TR U213 ( .A(n119), .B(n117), .Y(ReadA[14]) );
  NOR2X1TR U214 ( .A(n120), .B(n117), .Y(ReadA[15]) );
  NAND2BX1TR U215 ( .AN(n36), .B(n33), .Y(FunISEL[1]) );
  NAND2X1TR U216 ( .A(n29), .B(n101), .Y(WEN) );
  NOR2X1TR U217 ( .A(DSEL[0]), .B(n42), .Y(DSEL[1]) );
  NOR3X1TR U218 ( .A(opCode_1_0), .B(n660), .C(opCode_0_0), .Y(n41) );
  OAI211X1TR U219 ( .A0(n33), .A1(n103), .B0(n34), .C0(n35), .Y(n32) );
  AOI21X1TR U220 ( .A0(opCode_1_0), .A1(n36), .B0(n29), .Y(n35) );
  NOR3X1TR U221 ( .A(opCode_0_0), .B(opCode_1_0), .C(n93), .Y(n43) );
  NOR2X1TR U222 ( .A(n100), .B(rDest_1_0), .Y(n9) );
  AOI22X1TR U223 ( .A0(n770), .A1(n2), .B0(ZOUT), .B1(n9), .Y(n4) );
  XNOR2X1TR U224 ( .A(rDest_0_0), .B(COUT), .Y(n2) );
  NAND4X1TR U225 ( .A(imm8_disp[4]), .B(n125), .C(n88), .D(n102), .Y(n33) );
  NOR2BX1TR U226 ( .AN(DIn[10]), .B(n81), .Y(DOut[10]) );
  NOR2BX1TR U227 ( .AN(DIn[11]), .B(n80), .Y(DOut[11]) );
  NOR2BX1TR U228 ( .AN(DIn[12]), .B(n79), .Y(DOut[12]) );
  NOR2BX1TR U229 ( .AN(DIn[13]), .B(n79), .Y(DOut[13]) );
  NOR2BX1TR U230 ( .AN(DIn[14]), .B(n81), .Y(DOut[14]) );
  NOR2BX1TR U231 ( .AN(DIn[15]), .B(n80), .Y(DOut[15]) );
  CLKBUFX2TR U232 ( .A(n39), .Y(n86) );
  NAND3X1TR U233 ( .A(n37), .B(n102), .C(n125), .Y(n39) );
  CLKINVX2TR U234 ( .A(n7), .Y(n95) );
  XNOR2X1TR U235 ( .A(n100), .B(n1), .Y(n6) );
  AOI32X1TR U236 ( .A0(rDest_3_0), .A1(n99), .A2(N280), .B0(n4), .B1(n96), .Y(
        n5) );
  CLKAND2X2TR U237 ( .A(n37), .B(imm8_disp[6]), .Y(n29) );
  NAND2X1TR U238 ( .A(n750), .B(n40), .Y(Jmp) );
  NAND3X1TR U239 ( .A(imm8_disp[7]), .B(n37), .C(N190), .Y(n40) );
  OAI31X1TR U240 ( .A0(n8), .A1(n97), .A2(n640), .B0(n95), .Y(N190) );
  AOI22X1TR U241 ( .A0(N188), .A1(n9), .B0(N189), .B1(n670), .Y(n8) );
  AO22X1TR U242 ( .A0(DIn[0]), .A1(n740), .B0(pc[0]), .B1(n780), .Y(DOut[0])
         );
  AO22X1TR U243 ( .A0(DIn[1]), .A1(n750), .B0(pc[1]), .B1(n780), .Y(DOut[1])
         );
  AO22X1TR U244 ( .A0(DIn[2]), .A1(n86), .B0(pc[2]), .B1(n780), .Y(DOut[2]) );
  AO22X1TR U245 ( .A0(DIn[3]), .A1(n740), .B0(pc[3]), .B1(n780), .Y(DOut[3])
         );
  AO22X1TR U246 ( .A0(DIn[4]), .A1(n750), .B0(pc[4]), .B1(n81), .Y(DOut[4]) );
  AO22X1TR U247 ( .A0(DIn[5]), .A1(n86), .B0(pc[5]), .B1(n80), .Y(DOut[5]) );
  AO22X1TR U248 ( .A0(DIn[6]), .A1(n740), .B0(pc[6]), .B1(n79), .Y(DOut[6]) );
  AO22X1TR U249 ( .A0(DIn[7]), .A1(n750), .B0(pc[7]), .B1(n81), .Y(DOut[7]) );
  AO22X1TR U250 ( .A0(DIn[8]), .A1(n86), .B0(pc[8]), .B1(n80), .Y(DOut[8]) );
  AO22X1TR U251 ( .A0(DIn[9]), .A1(n740), .B0(pc[9]), .B1(n79), .Y(DOut[9]) );
  CLKINVX2TR U252 ( .A(n3), .Y(n96) );
  OAI31X1TR U253 ( .A0(rDest_1_0), .A1(ZOUT), .A2(rDest_0_0), .B0(n650), .Y(n3) );
  NOR2X1TR U254 ( .A(rDest_3_0), .B(NOUT), .Y(n1) );
  OAI33XLTR U255 ( .A0(n31), .A1(n720), .A2(n680), .B0(n38), .B1(n101), .B2(
        n104), .Y(MovSEL) );
  NAND3X1TR U256 ( .A(n88), .B(n103), .C(imm8_disp[6]), .Y(n38) );
  OAI31X1TR U257 ( .A0(n91), .A1(n700), .A2(n49), .B0(n47), .Y(AluSEL[0]) );
  CLKINVX2TR U258 ( .A(n46), .Y(n91) );
  AOI21X1TR U259 ( .A0(n48), .A1(imm8_disp[5]), .B0(n720), .Y(n49) );
  AOI31X1TR U260 ( .A0(n45), .A1(n94), .A2(n46), .B0(n92), .Y(AluSEL[1]) );
  CLKINVX2TR U261 ( .A(n47), .Y(n92) );
  AO21X1TR U262 ( .A0(imm8_disp[4]), .A1(n48), .B0(n700), .Y(n45) );
  NAND2X1TR U263 ( .A(opCode_0_0), .B(opCode_2_0), .Y(n31) );
  NOR2X1TR U264 ( .A(n30), .B(n101), .Y(SEL1) );
  AOI22X1TR U265 ( .A0(n730), .A1(n31), .B0(opCode_2_0), .B1(n680), .Y(n30) );
  AO21X1TR U266 ( .A0(n730), .A1(n41), .B0(LuiSEL), .Y(DSEL[0]) );
  NOR2X1TR U267 ( .A(opCode_2_0), .B(n730), .Y(n46) );
  OR2X2TR U268 ( .A(ZOUT), .B(NOUT), .Y(N188) );
  NAND2X1TR U269 ( .A(ZOUT), .B(NOUT), .Y(N189) );
  NAND3X1TR U270 ( .A(n41), .B(n730), .C(n44), .Y(Amount[1]) );
  NAND2X1TR U271 ( .A(opCode_3_0), .B(n43), .Y(n34) );
  NOR2X1TR U272 ( .A(imm8_disp[6]), .B(n125), .Y(n48) );
  NAND3X1TR U273 ( .A(n720), .B(n700), .C(n46), .Y(n47) );
  NOR2X1TR U274 ( .A(imm8_disp[6]), .B(Amount[1]), .Y(Amount[0]) );
  AND3X2TR U275 ( .A(n700), .B(n93), .C(opCode_3_0), .Y(n36) );
endmodule

