//EESC427 Gropu2 CAD8 revised

module controller(clk,  instruIn, DIn, Dsram, Dxac, RWL_P, RWL_N, RWLB_P, RWLB_N, rDestIn, rSrcIn, pc, N, Z, F, C, DOut, ReadA, ReadB, WE, WEMaster, imm8_disp, DSEL, AluSEL, Amount, LuiSEL, FunISEL, MovSEL, SEL1, CEN, WEN, WR_Data, WR_EN_SRAM, RENB_SRAM, REN_SRAM, Br, Jmp, Stall, Col_Sel);
	input			clk, N, Z, F, C;
	input [9:0]		pc;
	input [15:0]		instruIn, DIn, rDestIn, rSrcIn, Dsram;
	input [31:0]		Dxac;
	output [7:0]		imm8_disp;
	output reg 		LuiSEL, SEL1, WEN, WR_EN_SRAM, MovSEL, WEMaster, Br, Jmp, Stall, REN_SRAM;
	output reg [1:0]	DSEL, AluSEL, Amount, FunISEL;
	output reg [15:0]	DOut, WE, WR_Data, RWL_P, RWL_N, RWLB_P, RWLB_N;
	output reg [15:0]	Col_Sel;//16bit 
	output [15:0]		ReadA, ReadB ;
	output 			CEN,RENB_SRAM;

//REG WIRE definition
	reg [5:0]	Dxac_Encoded;
	reg		NOUT, ZOUT, FOUT, COUT;
	reg [15:0] 	instruReg;
	wire[3:0]	opCode, opCodeExt, rSrc, rDest;

//Rtarget = Rsrc<9:0>
assign imm8_disp 	= instruReg[7:0];
assign rSrc 		= instruReg[3:0];
assign rDest 		= instruReg[11:8];
assign opCode 		= instruReg[15:12];
assign opCodeExt	= instruReg[7:4];
assign RENB_SRAM	= 1'b0; //NC
assign ReadA		= 16'h0001<<rSrc;
assign ReadB		= 16'h0001<<rDest;
assign CEN		= 1'b0; //(check CEN truth table ????)
//assign CIN		= FunISEL[1]; (NOTE PLEASE CHECK THIS FOR ROUTING ALU Carry-In )

always @(posedge clk)begin//instruReg assignment
	instruReg	<= instruIn;
end

always @(posedge clk)begin//WESLAVE
	if(opCode==4'b1100)					WE <= 16'h0000; //NO WB for Bcond 
	else if({opCode,opCodeExt[2:0]}=={4'b0100,3'b100})	WE <= 16'h0000; //NO WB for STOR, Jcond
	else if({opCode,opCodeExt} == {4'b0000,4'b1011} || opCode == 4'b1011)
								WE <= 16'h0000; //NO WB for CMP, CMPI
	else if({opCode,opCodeExt} == {4'b0000,4'b0100} || {opCode,opCodeExt} == {4'b0000,4'b1111})	
								WE <= 16'h0000; //NO WB for STOR_SRAM, RST_SEL
	else 							WE <= (16'h0001 << rDest); //WB for OTHERS
end

always @(*) begin//WEMaster ==clkb
	WEMaster = ~clk; //WB for OTHERS
end

always@(posedge clk)begin//FLAG REG
	NOUT <= N;
end

always@(posedge clk)begin//FLAG REG
	FOUT <= F;
end

always@(posedge clk)begin//FLAG REG
	ZOUT <= Z;
end

always@(posedge clk)begin//COUT REG
	COUT <= C;
end

always@(posedge clk)begin
	if ({opCode,opCodeExt}=={4'b0000,4'b1100})
		if (Col_Sel[15] == 1'b0) Col_Sel <= (Col_Sel << 1'b1);
		else Col_Sel <= 16'h0001;
	else if ({opCode,opCodeExt}=={4'b0000,4'b1111})
		Col_Sel <= (16'h0001 << imm8_disp[3:0]);
	else	Col_Sel <= Col_Sel;
end

//Br
always@(*)begin
	if (opCode == 4'b1100) begin
		case(rDest)
		4'b0000: begin //EQ 
			if (ZOUT == 1'b1) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b0001: begin //NE
			if (ZOUT == 1'b0) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b1101: begin //GE 
			if (ZOUT == 1'b1 || NOUT == 1'b1) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b0010: begin //CS 
			if (COUT == 1'b1) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b0011: begin //CC
			if (COUT == 1'b0) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b0110: begin //GT
			if (NOUT == 1'b1) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b0111: begin //LE
			if (NOUT == 1'b0) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b1000: begin //FS
			if (FOUT == 1'b0) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b1001: begin //FC
			if (FOUT == 1'b0) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b1100: begin //LT
			if (NOUT == 1'b0 || ZOUT == 1'b0) Br = 1'b1;
			else Br = 1'b0;
		end
		4'b1110: Br = 1'b1;
		default: Br = 1'b0;
		endcase	
	end else Br = 1'b0;
end

//JMP
always@(*)begin
	if ({opCode,opCodeExt} == {4'b0100,4'b1100}) begin
		case(rDest)
		4'b0000: begin //EQ 
			if (ZOUT == 1'b1) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b0001: begin //NE
			if (ZOUT == 1'b0) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b1101: begin //GE 
			if (ZOUT == 1'b1 || NOUT == 1'b1) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b0010: begin //CS 
			if (COUT == 1'b1) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b0011: begin //CC
			if (COUT == 1'b0) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b0110: begin //GT
			if (NOUT == 1'b1) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b0111: begin //LE
			if (NOUT == 1'b0) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b1000: begin //FS
			if (FOUT == 1'b0) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b1001: begin //FC
			if (FOUT == 1'b0) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b1100: begin //LT
			if (NOUT == 1'b0 || ZOUT == 1'b0) Jmp = 1'b1;
			else Jmp = 1'b0;
		end
		4'b1110: Jmp = 1'b1;
		default: Jmp = 1'b0;
		endcase	
	end else if ({opCode,opCodeExt} == {4'b0100,4'b1000}) Jmp = 1'b1;
	else Jmp = 1'b0;
end


//SRAM XAC ENCODER
always @(*) begin
	if (Dxac[16] == 0) begin
		if (Dxac[8] == 0) begin
			if (Dxac[4] == 0) begin
				if (Dxac[2] == 0) begin
					if (Dxac[1] == 0) begin
						if (Dxac[0] == 0) Dxac_Encoded = 6'b110000; //-16
						else Dxac_Encoded = 6'b110001; //-15
					end else Dxac_Encoded = 6'b110010;
				end else begin
					if (Dxac[3] == 0) Dxac_Encoded = 6'b110011;
					else Dxac_Encoded = 6'b110100;
				end
			end else begin
				if (Dxac[6] == 0) begin
					if (Dxac[5] == 0) Dxac_Encoded = 6'b110101;
					else Dxac_Encoded = 6'b110110;
				end else begin
					if (Dxac[7] == 0) Dxac_Encoded = 6'b110111;
					else Dxac_Encoded = 6'b111000; //-8
				end
			end
		end else begin
			if (Dxac[12] == 0) begin
				if (Dxac[10] == 0) begin
					if (Dxac[9] == 0) Dxac_Encoded = 6'b111001; //-7
					else Dxac_Encoded = 6'b111010;
				end else begin
					if (Dxac[11] == 0) Dxac_Encoded = 6'b111011;
					else Dxac_Encoded = 6'b111100;
				end
			end else begin
				if (Dxac[14] == 0) begin
					if (Dxac[13] == 0) Dxac_Encoded = 6'b111101;
					else Dxac_Encoded = 6'b111110;
				end else begin
					if (Dxac[15] == 0) Dxac_Encoded = 6'b111111; //-1
					else Dxac_Encoded = 6'b000000; //0
				end
			end			

		end
	end else begin
		if (Dxac[24] == 0) begin
			if (Dxac[20] == 0) begin
				if (Dxac[18] == 0) begin
					if (Dxac[17] == 0) Dxac_Encoded = 6'b000001; //1
					else Dxac_Encoded = 6'b000010;
				end else begin
					if (Dxac[19] == 0) Dxac_Encoded = 6'b000011;
					else Dxac_Encoded = 6'b000100;
				end
			end else begin
				if (Dxac[22] == 0) begin
					if (Dxac[21] == 0) Dxac_Encoded = 6'b000101;
					else Dxac_Encoded = 6'b000110;
				end else begin
					if (Dxac[23] == 0) Dxac_Encoded = 6'b000111;
					else Dxac_Encoded = 6'b001000; //8
				end
			end
		end else begin
			if (Dxac[28] == 0) begin
				if (Dxac[26] == 0) begin
					if (Dxac[25] == 0) Dxac_Encoded = 6'b001001; //9
					else Dxac_Encoded = 6'b001010;
				end else begin
					if (Dxac[27] == 0) Dxac_Encoded = 6'b001011;
					else Dxac_Encoded = 6'b001100;
				end
			end else begin
				if (Dxac[30] == 0) begin
					if (Dxac[29] == 0) Dxac_Encoded = 6'b001101;
					else Dxac_Encoded = 6'b001110;
				end else begin
					if (Dxac[31] == 0) Dxac_Encoded = 6'b001111; //15
					else Dxac_Encoded = 6'b010000; //16
				end
			end			

		end

	end
end

//MUX for ALU,Shifter,DMEM,MOV,SRAM_DATA, SRAM_XAC,pc results
always@(*)begin//MUX(D, pc, Dsram, Dxac_Encoded) -> D;
	if ({opCode,opCodeExt} == 8'b01001000)	DOut = {6'b000000, pc};
	else if ({opCode,opCodeExt} == {4'b0000,4'b1000}) DOut = Dsram;
	else if ({opCode,opCodeExt} == {4'b0000,4'b1100}) DOut = {{10{Dxac_Encoded[5]}}, Dxac_Encoded};
	else					DOut = DIn;//???????????
end

always@(*)begin//WEN
	if ({opCode,opCodeExt} == {4'b0100,4'b0100}) //STOR
		WEN	=1'b0;
	else 	WEN	=1'b1;
end

always@(*)begin//Stall
	if ({opCode,opCodeExt} == {4'b0000,4'b0000}) //WAIT
		Stall	=1'b1;
	else 	Stall	=1'b0;
end

always@(*)begin//DSEL
	if(opCode == 4'b0100)				DSEL = 2'b00;//DMEM	
	else if(opCode == 4'b1111 || opCode == 4'b1000)	DSEL = 2'b01;//SHIFTER
	else						DSEL = 2'b10;
end

always@(*)begin//MovSEL
	if(opCode==4'b1101)	MovSEL = 1'b1;//MOVI
	else if({opCode,opCodeExt}=={4'b0000,4'b1101})
				MovSEL = 1'b1;//MOV
	else 			MovSEL = 1'b0;
end

always@(*)begin//FunISEL
	if({opCode,opCodeExt[3:2],opCodeExt[0]}=={4'b0000,3'b101})//CMP SUB
		FunISEL = 2'b10;
	else if({opCode[3:2],opCode[0]}==3'b101)//SUBi CMPi
		FunISEL	= 2'b11;
	else if(opCode==4'b0000)//ADD AND OR XOR MOV
		FunISEL = 2'b00;
	else 	FunISEL = 2'b01;
end

always@(*)begin//LuiSEL
	if(opCode==4'b1111)	LuiSEL = 1'b1;
	else 			LuiSEL = 1'b0;
end

always@(*)begin//Amount
	if({opCode,opCodeExt}=={4'b1000,4'b0100})//LSH({opCode,opCodeExt}=={4'b0000,4'b1100}) 
		Amount = 2'b00;
	else if({opCode,opCodeExt}=={4'b1000,4'b0000})//LSHI
		Amount = 2'b01;
	else 	Amount = 2'b10;//LUI
end

always@(*)begin//AluSEL
	if     (opCode==4'b0001||{opCode,opCodeExt}=={4'b0000,4'b0001})	AluSEL = 2'b00;//AND ANDi
	else if(opCode==4'b0010||{opCode,opCodeExt}=={4'b0000,4'b0010})	AluSEL = 2'b11;//OR ORi
	else if(opCode==4'b0011||{opCode,opCodeExt}=={4'b0000,4'b0011})	AluSEL = 2'b01;//XOR XORi
	else AluSEL = 2'b10;//ADD
end

always@(*)begin//SEL1
	if ((~imm8_disp[7]) | ((~opCode[3]) & (~opCode[2])) | (opCode[3] & opCode[2] & opCode[0]) == 1'b1)
		SEL1 = 1'b0;
	else	SEL1 = 1'b1;
end

always@(*)begin//STOR to SRAM
	if({opCode,opCodeExt}=={4'b0000,4'b0100}) begin
		WR_EN_SRAM = 1'b1;
		WR_Data = rDestIn;
	end else begin
		WR_EN_SRAM = 1'b0;
		WR_Data = 16'h0000; //State Assignment For Power Saving
	end
end

always@(*)begin
	if({opCode,opCodeExt}=={4'b0000,4'b1000}) begin //LOAD from SRAM //RWL encode
		REN_SRAM = 1'b1;
		RWL_P = 16'hFFFF;
		RWL_N = 16'h0001 << rSrcIn[3:0];
		RWLB_P = ~RWL_N; //????
		RWLB_N = 16'h0000;
	end else if ({opCode,opCodeExt}=={4'b0000,4'b1100}) begin //XAC in SRAM and LOAD from it to RF
		REN_SRAM = 1'b1;
		RWL_P = rSrcIn;
		RWL_N = rSrcIn;
		RWLB_P = ~rSrcIn;
		RWLB_N = ~rSrcIn;
	end else begin
		REN_SRAM = 1'b0;
		RWL_P = 16'h0000;
		RWL_N = 16'h0000;
		RWLB_P = 16'h0000;
		RWLB_N = 16'h0000;
	end
end

endmodule

