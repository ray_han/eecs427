//EESC427 Gropu2 CAD8 Ray

module mult(clk, instruIn, DIn, RsrcIn, pc, Rtarget, disp, DOut, ReadA, ReadB, WE, WEMaster, imm8, DSEL, AluSEL, Amount, LuiSEL, FunISEL, MovSEL, SEL1, CIN, WEN, CEN);
	input		clk;
	input [9:0]	pc;
	input [15:0]	instruIn, DIn, RsrcIn;
	output [7:0]	imm8;
	output reg[9:0]	Rtarget, disp;
	output reg	LuiSEL, SEL1, CIN, WEN, CEN, MovSEL, WEMaster;
	output reg[1:0]	DSEL, AluSEL, Amount, FunISEL;
	output reg[15:0]	DOut, ReadA, ReadB, WE;
	
	reg [15:0] 	instruReg;
	reg [3:0]	WEReg;
	reg 		WEMasterReg;
	
	wire[3:0]	opCode, opCodeExt, rSrc, rDest;

//Rtarget = Rsrc<9:0>
assign Rtarget		= RsrcIn[9:0];
assign imm8 		= instruReg[7:0];
assign rSrc 		= instruReg[3:0];
assign rDest 		= instruReg[11:8];
assign opCode 		= instruReg[15:12];
assign opCodeExt	= instruReg[7:4];
assign WEMaster		= WEMasterReg;
assign CEN		= 1'b0; //(check CEN truth table ????)
//ReadA, ReadB decoder
assign ReadA		= 16'h0001<<rSrc;
assign ReadB		= 16'h0001<<rDest;
assign WE		= 16'h0001<<WEReg;

always @(posedge clk) begin
	instruReg 	<= instruIn;
	WEReg 		<= rDest; // Delay one cycle for WB;
end

always @(posedge clk) begin	
	if (opCode == 4'b0100) begin
		if (opCodeExt[2:0] == 3'b100)	WEMasterReg <= 1'b0; //STORE, Jcond
		else				WEMasterReg <= 1'b1; //OTHERS
	end else if (opCode == 4'b1100)		WEMasterReg <= 1'b0; //Bcond
	else					WEMasterReg <= 1'b1; //OTHERS
end

//MUX for ALU,Shifter,DMEM,MOV results
always @(*) begin
	//Sign Ext disp for PC
	if (imm8[7] == 1)	disp = {2'b11, imm8};
	else			disp = {2'b00, imm8};
end

always @(*) begin
	//MUX(D, pc) -> D;
	if ({opCode, opCodeExt} == 8'b01001000)	DOut = DIn;
	else					DOut = {6'b000000, pc};
end

always @(*) begin	
	//MUX(DMEM, ALU, SH) -> D & WEN;
	if (opCode == 4'b0100) begin
		DSEL = 2'b00;//LOAD DMEM
		if (opCodeExt[3:2] == 2'b01)	WEN = 1'b0; //STOR
		else				WEN = 1'b1; //LOAD
	end else begin
		WEN = 1'b1; //OTHER
		if (opCode == 4'b1111 || opCode == 4'b1000)	DSEL = 2'b01;//SHIFTER
		else						DSEL = 2'b10;//ALU
	end
end

always @(*) begin
	//MUX(Rdest, 0) -> ALUA;
	if (opCode[3:1] == 3'b110 || {opCode, opCodeExt[3:1]} == 7'b0000110)	MovSEL = 1'b1;//MOV
	else 									MovSEL = 1'b0;//non-MOV
end

always @(*) begin	
	//MUX(Rsrc, imm16) -> ALUB & Carry-In;
	if (opCode[1:0] == 2'b00) begin
		if (opCodeExt[3:2] == 2'b10) begin //SUB,CMP
			FunISEL = 2'b10;
			CIN = 1'b1;
		end else begin //ADD,AND,OR,XOR
			FunISEL = 2'b00;
			CIN = 1'b0;
		end
	end else if (opCode[3:2] == 2'b10) begin //SUBi,CMPi
		FunISEL = 2'b11;
		CIN = 1'b1;
	end else begin //ADDi,ANDi,ORi,XORi
		FunISEL = 2'b01;
		CIN = 1'b0;
	end
end

always @(*) begin	
	//MUX(Rsrc, imm8, 8) -> Shifter_SEL & MUX(Rdest, imm16) -> Shifter;
	if (opCode == 4'b1111) begin
		LuiSEL = 1'b1;
		Amount = 2'b10;		
	end else begin
		LuiSEL = 1'b0;
		if ({opCode, opCodeExt[3:2]} == 6'b100001)	Amount = 2'b00;
		else 						Amount = 2'b01;
	end
end

always @(*) begin	
	//One-Zero Ext;
	if ((~imm8[7]) | ((~opCode[3]) & (~opCode[2])) | (opCode[3] & opCode[2] & opCode[0]) == 1'b1)	SEL1 = 1'b0;
	else												SEL1 = 1'b1;
end

always @(*) begin
	//ALU-SEL;
	if ({opCode[1:0], opCodeExt[3:1]} == 5'b00000 || opCode == 4'b0001)		AluSEL = 2'b00; //AND(i)
	else if ({opCode[1:0], opCodeExt[3:0]} == 6'b000011 || opCode == 4'b0011)	AluSEL = 2'b01; //XOR(i)
	else if ({opCode[1:0], opCodeExt[3:0]} == 6'b000010 || opCode == 4'b0010)	AluSEL = 2'b11; //OR(i)
	else										AluSEL = 2'b10; //AND(i),SUB(i),CMP(i) and OTHERS
end

endmodule

