//EESC427 Gropu2 CAD8 revised

module encoder(in, out);
	input [31:0]		in;
	output reg [5:0]	out;

always @(*) begin//WEMaster
	if (in[16] == 0) begin
		if (in[8] == 0) begin
			if (in[4] == 0) begin
				if (in[2] == 0) begin
					if (in[1] == 0) begin
						if (in[0] == 0) out = 6'b110000; //-16
						else out = 6'b110001; //-15
					end else out = 6'b110010;
				end else begin
					if (in[3] == 0) out = 6'b110011;
					else out = 6'b110100;
				end
			end else begin
				if (in[6] == 0) begin
					if (in[5] == 0) out = 6'b110101;
					else out = 6'b110110;
				end else begin
					if (in[7] == 0) out = 6'b110111;
					else out = 6'b111000; //-8
				end
			end
		end else begin
			if (in[12] == 0) begin
				if (in[10] == 0) begin
					if (in[9] == 0) out = 6'b111001; //-7
					else out = 6'b111010;
				end else begin
					if (in[11] == 0) out = 6'b111011;
					else out = 6'b111100;
				end
			end else begin
				if (in[14] == 0) begin
					if (in[13] == 0) out = 6'b111101;
					else out = 6'b111110;
				end else begin
					if (in[15] == 0) out = 6'b111111; //-1
					else out = 6'b000000; //0
				end
			end			

		end
	end else begin
		if (in[24] == 0) begin
			if (in[20] == 0) begin
				if (in[18] == 0) begin
					if (in[17] == 0) out = 6'b000001; //1
					else out = 6'b000010;
				end else begin
					if (in[19] == 0) out = 6'b000011;
					else out = 6'b000100;
				end
			end else begin
				if (in[22] == 0) begin
					if (in[21] == 0) out = 6'b000101;
					else out = 6'b000110;
				end else begin
					if (in[23] == 0) out = 6'b000111;
					else out = 6'b001000; //8
				end
			end
		end else begin
			if (in[28] == 0) begin
				if (in[26] == 0) begin
					if (in[25] == 0) out = 6'b001001; //9
					else out = 6'b001010;
				end else begin
					if (in[27] == 0) out = 6'b001011;
					else out = 6'b001100;
				end
			end else begin
				if (in[30] == 0) begin
					if (in[29] == 0) out = 6'b001101;
					else out = 6'b001110;
				end else begin
					if (in[31] == 0) out = 6'b001111; //15
					else out = 6'b010000; //16
				end
			end			

		end

	end
end
endmodule
