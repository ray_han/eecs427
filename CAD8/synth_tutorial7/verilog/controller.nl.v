/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06
// Date      : Sun Dec  9 20:00:37 2018
/////////////////////////////////////////////////////////////


module controller ( clk, instruIn, DIn, Dsram, Dxac, RWL_P, RWL_N, RWLB_P, 
        RWLB_N, rDestIn, rSrcIn, pc, N, Z, F, C, DOut, ReadA, ReadB, WE, 
        WEMaster, imm8_disp, DSEL, AluSEL, Amount, LuiSEL, FunISEL, MovSEL, 
        SEL1, CEN, WEN, WR_Data, WR_EN_SRAM, RENB_SRAM, REN_SRAM, Br, Jmp, 
        Stall, Col_Sel );
  input [15:0] instruIn;
  input [15:0] DIn;
  input [15:0] Dsram;
  input [31:0] Dxac;
  output [15:0] RWL_P;
  output [15:0] RWL_N;
  output [15:0] RWLB_P;
  output [15:0] RWLB_N;
  input [15:0] rDestIn;
  input [15:0] rSrcIn;
  input [9:0] pc;
  output [15:0] DOut;
  output [15:0] ReadA;
  output [15:0] ReadB;
  output [15:0] WE;
  output [7:0] imm8_disp;
  output [1:0] DSEL;
  output [1:0] AluSEL;
  output [1:0] Amount;
  output [1:0] FunISEL;
  output [15:0] WR_Data;
  output [15:0] Col_Sel;
  input clk, N, Z, F, C;
  output WEMaster, LuiSEL, MovSEL, SEL1, CEN, WEN, WR_EN_SRAM, RENB_SRAM,
         REN_SRAM, Br, Jmp, Stall;
  wire   n500, n501, n502, n503, rDest_3_0, rDest_2_0, rDest_1_0, rDest_0_0,
         opCode_3_0, opCode_1_0, opCode_0_0, N84, N85, N86, N87, N88, N89, N90,
         N91, N92, N93, N94, N95, N96, N97, N98, N99, NOUT, ZOUT, COUT, N204,
         N257, N258, N259, N374, N375, N376, N377, N378, N379, N380, N381,
         N382, N383, N384, N385, N386, N387, N388, N389, N498, n1, n2, n3, n4,
         n5, n6, n7, n8, n9, n10, n114, n115, n117, n118, n119, n120, n121,
         n122, n123, n124, n125, n126, n127, n128, n129, n130, n131, n132,
         n133, n134, n135, n136, n137, n138, n139, n140, n141, n142, n143,
         n144, n145, n146, n147, n148, n149, n150, n151, n152, n154, n155,
         n156, n157, n158, n159, n160, n161, n162, n163, n164, n165, n166,
         n167, n168, n169, n170, n171, n172, n173, n174, n175, n176, n177,
         n178, n179, n180, n181, n182, n183, n184, n185, n186, n187, n188,
         n189, n190, n191, n192, n193, n194, n195, n196, n197, n198, n199,
         n200, n201, n202, n203, n2040, n205, n206, n207, n208, n209, n210,
         n211, n212, n213, n214, n215, n216, n217, n218, n219, n220, n221,
         n222, n223, n224, n225, n226, n227, n228, n229, n230, n231, n232,
         n233, n234, n235, n236, n237, n238, n239, n240, n241, n2570, n2580,
         n2590, n260, n261, n262, n263, n264, n265, n266, n267, n268, n269,
         n270, n271, n272, n273, n274, n275, n276, n277, n278, n279, n280,
         n281, n282, n283, n284, n285, n286, n287, n288, n289, n290, n291,
         n292, n293, n294, n297, n298, n300, n301, n302, n303, n304, n305,
         n306, n307, n308, n309, n310, n311, n312, n313, n314, n316, n317,
         n318, n319, n320, n321, n322, n323, n324, n325, n326, n327, n328,
         n329, n330, n331, n332, n333, n334, n335, n336, n337, n338, n339,
         n340, n341, n342, n343, n344, n345, n346, n347, n348, n349, n350,
         n351, n352, n353, n354, n355, n356, n357, n358, n359, n360, n361,
         n362, n363, n364, n365, n366, n367, n368, n369, n370, n371, n372,
         n373, n3740, n3750, n3760, n3770, n3780, n3790, n3800, n3810, n3820,
         n3830, n3840, n3850, n3860, n3870, n3880, n3890, n390, n391, n392,
         n393, n394, n395, n396, n397, n398, n399, n400, n401, n402, n403,
         n404, n405, n406, n407, n408, n409, n410, n411, n412, n413, n414,
         n415, n416, n417, n418, n419, n420, n421, n422, n423, n424, n425,
         n426, n427, n428, n429, n430, n431, n432, n433, n434, n435, n436,
         n437, n438, n439, n440, n441, n442, n443, n444, n445, n446, n447,
         n448, n449, n450, n451, n452, n453, n454, n455, n456, n457, n458,
         n459, n460, n461, n462, n463, n464, n465, n466, n467, n468, n469,
         n470, n471, n472, n473, n474, n475, n476, n477, n478, n479, n480,
         n481, n482, n483, n484, n485, n486, n487, n488, n489, n490, n491,
         n492, n493, n494, n495, n496, n497, n4980, n499;

  DFFXLTR FOUT_reg ( .D(F), .CK(clk), .QN(N498) );
  DFFQX1TR Col_Sel_reg_0_0 ( .D(n241), .CK(clk), .Q(Col_Sel[0]) );
  DFFQX1TR Col_Sel_reg_1_0 ( .D(n240), .CK(clk), .Q(Col_Sel[1]) );
  DFFQX1TR Col_Sel_reg_2_0 ( .D(n239), .CK(clk), .Q(Col_Sel[2]) );
  DFFQX1TR Col_Sel_reg_3_0 ( .D(n238), .CK(clk), .Q(Col_Sel[3]) );
  DFFQX1TR Col_Sel_reg_4_0 ( .D(n237), .CK(clk), .Q(Col_Sel[4]) );
  DFFQX1TR Col_Sel_reg_5_0 ( .D(n236), .CK(clk), .Q(Col_Sel[5]) );
  DFFQX1TR Col_Sel_reg_6_0 ( .D(n235), .CK(clk), .Q(Col_Sel[6]) );
  DFFQX1TR Col_Sel_reg_7_0 ( .D(n234), .CK(clk), .Q(Col_Sel[7]) );
  DFFQX1TR Col_Sel_reg_8_0 ( .D(n233), .CK(clk), .Q(Col_Sel[8]) );
  DFFQX1TR Col_Sel_reg_9_0 ( .D(n232), .CK(clk), .Q(Col_Sel[9]) );
  DFFQX1TR Col_Sel_reg_10_0 ( .D(n231), .CK(clk), .Q(Col_Sel[10]) );
  DFFQX1TR Col_Sel_reg_11_0 ( .D(n230), .CK(clk), .Q(Col_Sel[11]) );
  DFFQX1TR Col_Sel_reg_12_0 ( .D(n229), .CK(clk), .Q(Col_Sel[12]) );
  DFFQX1TR Col_Sel_reg_13_0 ( .D(n228), .CK(clk), .Q(Col_Sel[13]) );
  DFFQX1TR Col_Sel_reg_14_0 ( .D(n227), .CK(clk), .Q(Col_Sel[14]) );
  DFFQX1TR instruReg_reg_3_0 ( .D(n265), .CK(clk), .Q(imm8_disp[3]) );
  DFFQX1TR instruReg_reg_0_0 ( .D(n264), .CK(clk), .Q(imm8_disp[0]) );
  DFFQX1TR COUT_reg ( .D(n261), .CK(clk), .Q(COUT) );
  DFFQX1TR ZOUT_reg ( .D(n260), .CK(clk), .Q(ZOUT) );
  DFFQX1TR instruReg_reg_10_0 ( .D(n2590), .CK(clk), .Q(rDest_2_0) );
  DFFQX1TR instruReg_reg_12_0 ( .D(n2580), .CK(clk), .Q(opCode_0_0) );
  CLKINVX2TR U72 ( .A(clk), .Y(WEMaster) );
  DFFQX1TR Col_Sel_reg_15_0 ( .D(n226), .CK(clk), .Q(Col_Sel[15]) );
  DFFQX1TR WE_reg_15_0 ( .D(N99), .CK(clk), .Q(WE[15]) );
  DFFQX1TR WE_reg_14_0 ( .D(N98), .CK(clk), .Q(WE[14]) );
  DFFQX1TR WE_reg_13_0 ( .D(N97), .CK(clk), .Q(WE[13]) );
  DFFQX1TR WE_reg_12_0 ( .D(N96), .CK(clk), .Q(WE[12]) );
  DFFQX1TR WE_reg_11_0 ( .D(N95), .CK(clk), .Q(WE[11]) );
  DFFQX1TR WE_reg_10_0 ( .D(N94), .CK(clk), .Q(WE[10]) );
  DFFQX1TR WE_reg_9_0 ( .D(N93), .CK(clk), .Q(WE[9]) );
  DFFQX1TR WE_reg_8_0 ( .D(N92), .CK(clk), .Q(WE[8]) );
  DFFQX1TR WE_reg_7_0 ( .D(N91), .CK(clk), .Q(WE[7]) );
  DFFQX1TR WE_reg_6_0 ( .D(N90), .CK(clk), .Q(WE[6]) );
  DFFQX1TR WE_reg_5_0 ( .D(N89), .CK(clk), .Q(WE[5]) );
  DFFQX1TR WE_reg_4_0 ( .D(N88), .CK(clk), .Q(WE[4]) );
  DFFQX1TR WE_reg_3_0 ( .D(N87), .CK(clk), .Q(WE[3]) );
  DFFQX1TR WE_reg_2_0 ( .D(N86), .CK(clk), .Q(WE[2]) );
  DFFQX1TR WE_reg_1_0 ( .D(N85), .CK(clk), .Q(WE[1]) );
  DFFQX1TR WE_reg_0_0 ( .D(N84), .CK(clk), .Q(WE[0]) );
  DFFQX1TR instruReg_reg_2_0 ( .D(n270), .CK(clk), .Q(imm8_disp[2]) );
  DFFQX1TR instruReg_reg_1_0 ( .D(n269), .CK(clk), .Q(imm8_disp[1]) );
  DFFQX1TR instruReg_reg_11_0 ( .D(n268), .CK(clk), .Q(rDest_3_0) );
  DFFQX1TR instruReg_reg_8_0 ( .D(n267), .CK(clk), .Q(rDest_0_0) );
  DFFQX1TR instruReg_reg_9_0 ( .D(n266), .CK(clk), .Q(rDest_1_0) );
  DFFQX1TR instruReg_reg_13_0 ( .D(n274), .CK(clk), .Q(opCode_1_0) );
  DFFQX1TR instruReg_reg_4_0 ( .D(n271), .CK(clk), .Q(imm8_disp[4]) );
  DFFQX1TR instruReg_reg_15_0 ( .D(n2570), .CK(clk), .Q(opCode_3_0) );
  DFFQX1TR instruReg_reg_6_0 ( .D(n273), .CK(clk), .Q(n501) );
  DFFQX1TR instruReg_reg_7_0 ( .D(n272), .CK(clk), .Q(n500) );
  DFFQX1TR instruReg_reg_5_0 ( .D(n262), .CK(clk), .Q(n502) );
  DFFQXLTR NOUT_reg ( .D(n263), .CK(clk), .Q(NOUT) );
  DFFXLTR instruReg_reg_14_0 ( .D(instruIn[14]), .CK(clk), .Q(n323), .QN(n442)
         );
  INVX2TR U389 ( .A(1'b1), .Y(RENB_SRAM) );
  INVX2TR U391 ( .A(1'b1), .Y(CEN) );
  CLKBUFX2TR U393 ( .A(instruIn[15]), .Y(n2570) );
  CLKBUFX2TR U394 ( .A(instruIn[12]), .Y(n2580) );
  CLKBUFX2TR U395 ( .A(instruIn[10]), .Y(n2590) );
  CLKBUFX2TR U396 ( .A(Z), .Y(n260) );
  CLKBUFX2TR U397 ( .A(C), .Y(n261) );
  CLKBUFX2TR U398 ( .A(instruIn[5]), .Y(n262) );
  CLKBUFX2TR U399 ( .A(N), .Y(n263) );
  CLKBUFX2TR U400 ( .A(instruIn[0]), .Y(n264) );
  CLKBUFX2TR U401 ( .A(instruIn[3]), .Y(n265) );
  CLKBUFX2TR U402 ( .A(instruIn[9]), .Y(n266) );
  CLKBUFX2TR U403 ( .A(instruIn[8]), .Y(n267) );
  CLKBUFX2TR U404 ( .A(instruIn[11]), .Y(n268) );
  CLKBUFX2TR U405 ( .A(instruIn[1]), .Y(n269) );
  CLKBUFX2TR U406 ( .A(instruIn[2]), .Y(n270) );
  CLKBUFX2TR U407 ( .A(instruIn[4]), .Y(n271) );
  CLKBUFX2TR U408 ( .A(instruIn[7]), .Y(n272) );
  CLKBUFX2TR U409 ( .A(instruIn[6]), .Y(n273) );
  CLKBUFX2TR U410 ( .A(instruIn[13]), .Y(n274) );
  NAND4X1TR U411 ( .A(n158), .B(n151), .C(n159), .D(n160), .Y(DOut[4]) );
  OAI211X4TR U412 ( .A0(n169), .A1(n347), .B0(n170), .C0(n171), .Y(DOut[2]) );
  OAI211X4TR U413 ( .A0(n361), .A1(n3770), .B0(n151), .C0(n157), .Y(DOut[5])
         );
  OAI211X4TR U414 ( .A0(n361), .A1(n3740), .B0(n298), .C0(n154), .Y(DOut[8])
         );
  OAI211X4TR U415 ( .A0(n360), .A1(n373), .B0(n298), .C0(n152), .Y(DOut[9]) );
  NOR2BX2TR U416 ( .AN(n322), .B(n444), .Y(n145) );
  NOR3X2TR U417 ( .A(n452), .B(n501), .C(n135), .Y(n138) );
  NAND2BX4TR U418 ( .AN(FunISEL[0]), .B(n144), .Y(n135) );
  CLKINVX2TR U419 ( .A(n275), .Y(n282) );
  NOR2X1TR U420 ( .A(n441), .B(REN_SRAM), .Y(n275) );
  INVX2TR U421 ( .A(n282), .Y(n284) );
  INVX6TR U422 ( .A(n282), .Y(n283) );
  CLKINVX2TR U423 ( .A(n282), .Y(n286) );
  CLKINVX2TR U424 ( .A(n282), .Y(n285) );
  CLKBUFX2TR U425 ( .A(n438), .Y(n346) );
  INVX2TR U426 ( .A(n132), .Y(n438) );
  CLKINVX2TR U427 ( .A(n323), .Y(n276) );
  CLKINVX2TR U428 ( .A(n276), .Y(n277) );
  CLKINVX2TR U429 ( .A(Dxac[20]), .Y(n278) );
  CLKINVX2TR U430 ( .A(n278), .Y(n279) );
  CLKINVX2TR U431 ( .A(Dxac[16]), .Y(n280) );
  CLKINVX2TR U432 ( .A(n280), .Y(n281) );
  CLKBUFX2TR U433 ( .A(n449), .Y(n287) );
  CLKINVX2TR U434 ( .A(n305), .Y(n288) );
  CLKINVX2TR U435 ( .A(imm8_disp[7]), .Y(n289) );
  CLKBUFX2TR U436 ( .A(n144), .Y(n290) );
  NOR2X1TR U437 ( .A(n502), .B(imm8_disp[4]), .Y(n144) );
  CLKINVX2TR U438 ( .A(n443), .Y(n291) );
  NOR2X2TR U439 ( .A(opCode_1_0), .B(opCode_0_0), .Y(n150) );
  CLKINVX2TR U440 ( .A(n348), .Y(n292) );
  CLKINVX2TR U441 ( .A(n325), .Y(n293) );
  CLKINVX2TR U442 ( .A(n297), .Y(n294) );
  CLKBUFX2TR U443 ( .A(n502), .Y(imm8_disp[5]) );
  CLKBUFX2TR U444 ( .A(n500), .Y(imm8_disp[7]) );
  CLKBUFX2TR U445 ( .A(n451), .Y(n297) );
  CLKINVX2TR U446 ( .A(n306), .Y(n298) );
  NAND2X1TR U447 ( .A(n2040), .B(n132), .Y(n151) );
  CLKBUFX2TR U448 ( .A(n501), .Y(imm8_disp[6]) );
  CLKBUFX2TR U449 ( .A(n139), .Y(n300) );
  CLKBUFX2TR U450 ( .A(n139), .Y(n301) );
  CLKBUFX2TR U451 ( .A(n114), .Y(n302) );
  CLKBUFX2TR U452 ( .A(n114), .Y(n303) );
  NAND2X1TR U453 ( .A(n346), .B(n133), .Y(n114) );
  CLKINVX2TR U454 ( .A(opCode_3_0), .Y(n304) );
  CLKBUFX2TR U455 ( .A(rDest_3_0), .Y(n305) );
  CLKBUFX2TR U456 ( .A(n392), .Y(n306) );
  CLKBUFX2TR U457 ( .A(rDest_1_0), .Y(n307) );
  CLKINVX1TR U458 ( .A(n147), .Y(n308) );
  CLKINVX1TR U459 ( .A(n147), .Y(n309) );
  CLKINVX2TR U460 ( .A(n147), .Y(n441) );
  NAND4X1TR U461 ( .A(n145), .B(n144), .C(n500), .D(n454), .Y(n147) );
  CLKBUFX2TR U462 ( .A(n133), .Y(n440) );
  CLKINVX2TR U463 ( .A(n440), .Y(n310) );
  CLKINVX2TR U464 ( .A(n440), .Y(n311) );
  CLKINVX2TR U465 ( .A(n440), .Y(n312) );
  CLKINVX2TR U466 ( .A(n440), .Y(n313) );
  CLKINVX2TR U467 ( .A(n503), .Y(n326) );
  CLKINVX2TR U468 ( .A(n326), .Y(n314) );
  CLKINVX2TR U469 ( .A(n326), .Y(WR_EN_SRAM) );
  CLKINVX2TR U470 ( .A(n326), .Y(n316) );
  CLKINVX2TR U471 ( .A(n326), .Y(n317) );
  CLKBUFX2TR U472 ( .A(opCode_3_0), .Y(n318) );
  NAND2X2TR U473 ( .A(n356), .B(n346), .Y(REN_SRAM) );
  INVX2TR U474 ( .A(RWLB_N[8]), .Y(n418) );
  NOR2X1TR U475 ( .A(n319), .B(n320), .Y(n163) );
  NOR2X1TR U476 ( .A(n318), .B(n442), .Y(n322) );
  AND2XLTR U477 ( .A(DIn[3]), .B(n283), .Y(n319) );
  AND2XLTR U478 ( .A(pc[3]), .B(n441), .Y(n320) );
  NOR3XLTR U479 ( .A(n442), .B(n444), .C(n435), .Y(n143) );
  NAND2X2TR U480 ( .A(n209), .B(n150), .Y(FunISEL[0]) );
  NAND2X1TR U481 ( .A(n321), .B(n163), .Y(DOut[3]) );
  OAI31X1TR U482 ( .A0(n164), .A1(n3810), .A2(n165), .B0(n132), .Y(n162) );
  OA21XLTR U483 ( .A0(n361), .A1(n3780), .B0(n162), .Y(n321) );
  CLKINVX1TR U484 ( .A(n150), .Y(n444) );
  OR2X2TR U485 ( .A(n454), .B(n289), .Y(n324) );
  OR2X2TR U486 ( .A(n337), .B(Col_Sel[15]), .Y(n325) );
  NOR2X2TR U487 ( .A(n323), .B(opCode_3_0), .Y(n209) );
  OAI2BB2XLTR U488 ( .B0(n342), .B1(n433), .A0N(n365), .A1N(N374), .Y(RWL_N[0]) );
  NOR2X1TR U489 ( .A(n338), .B(rSrcIn[8]), .Y(RWLB_N[8]) );
  INVX2TR U490 ( .A(n500), .Y(n452) );
  INVX2TR U491 ( .A(n138), .Y(n360) );
  INVX2TR U492 ( .A(n363), .Y(n352) );
  INVX2TR U493 ( .A(n362), .Y(n353) );
  INVX1TR U494 ( .A(n362), .Y(n354) );
  INVX1TR U495 ( .A(n362), .Y(n355) );
  CLKINVX1TR U496 ( .A(n371), .Y(n359) );
  INVX1TR U497 ( .A(n371), .Y(n361) );
  INVX2TR U498 ( .A(n146), .Y(n439) );
  INVX2TR U499 ( .A(RWLB_N[0]), .Y(n434) );
  INVX2TR U500 ( .A(RWLB_N[1]), .Y(n432) );
  INVX2TR U501 ( .A(RWLB_N[2]), .Y(n430) );
  INVX2TR U502 ( .A(RWLB_N[3]), .Y(n428) );
  INVX2TR U503 ( .A(RWLB_N[4]), .Y(n426) );
  INVX2TR U504 ( .A(RWLB_N[5]), .Y(n424) );
  INVX2TR U505 ( .A(RWLB_N[10]), .Y(n414) );
  INVX2TR U506 ( .A(RWLB_N[11]), .Y(n412) );
  INVX2TR U507 ( .A(RWLB_N[12]), .Y(n410) );
  INVX2TR U508 ( .A(RWLB_N[13]), .Y(n408) );
  INVX2TR U509 ( .A(RWLB_N[14]), .Y(n406) );
  INVX2TR U510 ( .A(RWLB_N[15]), .Y(n404) );
  INVX2TR U511 ( .A(RWLB_N[6]), .Y(n422) );
  INVX2TR U512 ( .A(RWLB_N[7]), .Y(n420) );
  INVX2TR U513 ( .A(RWLB_N[9]), .Y(n416) );
  OAI21XLTR U514 ( .A0(n343), .A1(n409), .B0(n356), .Y(RWL_P[12]) );
  OAI21XLTR U515 ( .A0(n343), .A1(n405), .B0(n356), .Y(RWL_P[14]) );
  OAI21XLTR U516 ( .A0(n342), .A1(n433), .B0(n356), .Y(RWL_P[0]) );
  OAI21XLTR U517 ( .A0(n342), .A1(n413), .B0(n357), .Y(RWL_P[10]) );
  OAI21XLTR U518 ( .A0(n342), .A1(n411), .B0(n357), .Y(RWL_P[11]) );
  OAI21XLTR U519 ( .A0(n346), .A1(n417), .B0(n360), .Y(RWL_P[8]) );
  OR2XLTR U520 ( .A(n141), .B(n142), .Y(FunISEL[1]) );
  AOI21XLTR U521 ( .A0(Dsram[10]), .A1(n363), .B0(n392), .Y(n203) );
  AOI21XLTR U522 ( .A0(Dsram[11]), .A1(n363), .B0(n392), .Y(n202) );
  AOI21XLTR U523 ( .A0(Dsram[12]), .A1(n363), .B0(n392), .Y(n201) );
  NAND2XLTR U524 ( .A(Dsram[1]), .B(n365), .Y(n185) );
  NAND2XLTR U525 ( .A(Dsram[2]), .B(n365), .Y(n170) );
  NOR3XLTR U526 ( .A(n445), .B(n277), .C(n304), .Y(n142) );
  NAND3XLTR U527 ( .A(n502), .B(n146), .C(n501), .Y(n133) );
  NAND2XLTR U528 ( .A(n147), .B(n148), .Y(Jmp) );
  NAND4XLTR U529 ( .A(N259), .B(n145), .C(n290), .D(imm8_disp[7]), .Y(n148) );
  CLKINVX1TR U530 ( .A(opCode_3_0), .Y(n435) );
  NAND3XLTR U531 ( .A(n290), .B(imm8_disp[6]), .C(n145), .Y(n134) );
  INVXLTR U532 ( .A(imm8_disp[4]), .Y(n455) );
  AND2XLTR U533 ( .A(rDestIn[0]), .B(n314), .Y(WR_Data[0]) );
  AND2XLTR U534 ( .A(rDestIn[1]), .B(n314), .Y(WR_Data[1]) );
  AND2XLTR U535 ( .A(rDestIn[2]), .B(n314), .Y(WR_Data[2]) );
  AND2XLTR U536 ( .A(rDestIn[3]), .B(n316), .Y(WR_Data[3]) );
  AND2XLTR U537 ( .A(rDestIn[4]), .B(WR_EN_SRAM), .Y(WR_Data[4]) );
  AND2XLTR U538 ( .A(rDestIn[5]), .B(WR_EN_SRAM), .Y(WR_Data[5]) );
  AND2XLTR U539 ( .A(rDestIn[6]), .B(WR_EN_SRAM), .Y(WR_Data[6]) );
  AND2XLTR U540 ( .A(rDestIn[7]), .B(n317), .Y(WR_Data[7]) );
  AND2XLTR U541 ( .A(rDestIn[8]), .B(n316), .Y(WR_Data[8]) );
  AND2XLTR U542 ( .A(rDestIn[9]), .B(WR_EN_SRAM), .Y(WR_Data[9]) );
  AND2XLTR U543 ( .A(rDestIn[10]), .B(n317), .Y(WR_Data[10]) );
  AND2XLTR U544 ( .A(rDestIn[11]), .B(n316), .Y(WR_Data[11]) );
  AND2XLTR U545 ( .A(rDestIn[12]), .B(n317), .Y(WR_Data[12]) );
  AND2XLTR U546 ( .A(rDestIn[13]), .B(n316), .Y(WR_Data[13]) );
  AND2XLTR U547 ( .A(rDestIn[14]), .B(n317), .Y(WR_Data[14]) );
  AND2XLTR U548 ( .A(rDestIn[15]), .B(n503), .Y(WR_Data[15]) );
  AOI21XLTR U549 ( .A0(n224), .A1(imm8_disp[5]), .B0(opCode_1_0), .Y(n225) );
  AOI22XLTR U550 ( .A0(n318), .A1(n137), .B0(n277), .B1(n435), .Y(n136) );
  NAND2XLTR U551 ( .A(opCode_0_0), .B(n277), .Y(n137) );
  NAND2X1TR U552 ( .A(n307), .B(n297), .Y(n486) );
  NAND2X1TR U553 ( .A(n305), .B(n449), .Y(n490) );
  NAND2X1TR U554 ( .A(imm8_disp[1]), .B(imm8_disp[0]), .Y(n479) );
  NAND2X1TR U555 ( .A(rDest_2_0), .B(n448), .Y(n488) );
  NAND2X1TR U556 ( .A(imm8_disp[2]), .B(imm8_disp[3]), .Y(n476) );
  NAND2X1TR U557 ( .A(imm8_disp[0]), .B(n458), .Y(n483) );
  NAND2X1TR U558 ( .A(imm8_disp[3]), .B(n457), .Y(n482) );
  NAND2X1TR U559 ( .A(rDest_0_0), .B(n450), .Y(n491) );
  NAND2X1TR U560 ( .A(rSrcIn[0]), .B(n431), .Y(n499) );
  NAND2X1TR U561 ( .A(rSrcIn[3]), .B(n429), .Y(n4980) );
  NAND2X1TR U562 ( .A(imm8_disp[2]), .B(n456), .Y(n480) );
  NAND2X1TR U563 ( .A(imm8_disp[1]), .B(n459), .Y(n478) );
  NAND2X1TR U564 ( .A(rSrcIn[2]), .B(n427), .Y(n496) );
  NAND2X1TR U565 ( .A(rSrcIn[1]), .B(n433), .Y(n494) );
  NAND2X1TR U566 ( .A(n307), .B(rDest_0_0), .Y(n487) );
  NAND2X1TR U567 ( .A(rSrcIn[1]), .B(rSrcIn[0]), .Y(n495) );
  NAND2X1TR U568 ( .A(rDest_2_0), .B(n305), .Y(n484) );
  NAND2X1TR U569 ( .A(rSrcIn[2]), .B(rSrcIn[3]), .Y(n492) );
  NOR2XLTR U570 ( .A(n277), .B(imm8_disp[7]), .Y(n221) );
  CLKINVX2TR U571 ( .A(n501), .Y(n454) );
  NOR2X2TR U572 ( .A(n324), .B(n135), .Y(n132) );
  NAND4XLTR U573 ( .A(Dxac[31]), .B(Dxac[30]), .C(n161), .D(n132), .Y(n158) );
  AOI22XLTR U574 ( .A0(Col_Sel[15]), .A1(n292), .B0(ReadA[0]), .B1(n310), .Y(
        n131) );
  NAND4XLTR U575 ( .A(n318), .B(n290), .C(n221), .D(n150), .Y(Amount[1]) );
  AOI31XLTR U576 ( .A0(n318), .A1(n442), .A2(n150), .B0(LuiSEL), .Y(n149) );
  CLKBUFX2TR U577 ( .A(n350), .Y(n336) );
  CLKBUFX2TR U578 ( .A(n438), .Y(n337) );
  CLKBUFX2TR U579 ( .A(n351), .Y(n335) );
  CLKBUFX2TR U580 ( .A(n350), .Y(n334) );
  CLKBUFX2TR U581 ( .A(n350), .Y(n338) );
  CLKBUFX2TR U582 ( .A(n350), .Y(n339) );
  CLKBUFX2TR U583 ( .A(n349), .Y(n341) );
  CLKBUFX2TR U584 ( .A(n349), .Y(n340) );
  CLKBUFX2TR U585 ( .A(n349), .Y(n342) );
  CLKBUFX2TR U586 ( .A(n348), .Y(n345) );
  CLKBUFX2TR U587 ( .A(n351), .Y(n343) );
  CLKBUFX2TR U588 ( .A(n348), .Y(n344) );
  CLKINVX2TR U589 ( .A(n138), .Y(n356) );
  CLKINVX2TR U590 ( .A(n369), .Y(n357) );
  CLKINVX2TR U591 ( .A(n370), .Y(n358) );
  CLKBUFX2TR U592 ( .A(n351), .Y(n349) );
  CLKBUFX2TR U593 ( .A(n351), .Y(n348) );
  CLKBUFX2TR U594 ( .A(n348), .Y(n347) );
  CLKBUFX2TR U595 ( .A(n371), .Y(n362) );
  CLKBUFX2TR U596 ( .A(n371), .Y(n363) );
  CLKBUFX2TR U597 ( .A(n370), .Y(n364) );
  CLKBUFX2TR U598 ( .A(n369), .Y(n365) );
  CLKBUFX2TR U599 ( .A(n438), .Y(n350) );
  CLKBUFX2TR U600 ( .A(n438), .Y(n351) );
  CLKBUFX2TR U601 ( .A(n369), .Y(n368) );
  CLKBUFX2TR U602 ( .A(n370), .Y(n366) );
  CLKBUFX2TR U603 ( .A(n370), .Y(n367) );
  CLKBUFX2TR U604 ( .A(n114), .Y(n329) );
  CLKBUFX2TR U605 ( .A(n114), .Y(n330) );
  NOR2BX1TR U606 ( .AN(ReadB[0]), .B(n327), .Y(N84) );
  NOR2BX1TR U607 ( .AN(ReadB[4]), .B(n328), .Y(N88) );
  NOR2BX1TR U608 ( .AN(ReadB[12]), .B(n300), .Y(N96) );
  NOR2BX1TR U609 ( .AN(ReadB[2]), .B(n301), .Y(N86) );
  NOR2BX1TR U610 ( .AN(ReadB[14]), .B(n327), .Y(N98) );
  NOR2BX1TR U611 ( .AN(ReadB[1]), .B(n327), .Y(N85) );
  NOR2BX1TR U612 ( .AN(ReadB[5]), .B(n328), .Y(N89) );
  NOR2BX1TR U613 ( .AN(ReadB[8]), .B(n328), .Y(N92) );
  NOR2BX1TR U614 ( .AN(ReadB[9]), .B(n300), .Y(N93) );
  NOR2BX1TR U615 ( .AN(ReadB[10]), .B(n300), .Y(N94) );
  NOR2BX1TR U616 ( .AN(ReadB[11]), .B(n301), .Y(N95) );
  NOR2BX1TR U617 ( .AN(ReadB[13]), .B(n327), .Y(N97) );
  NOR2BX1TR U618 ( .AN(ReadB[3]), .B(n328), .Y(N87) );
  NOR2BX1TR U619 ( .AN(ReadB[6]), .B(n301), .Y(N90) );
  NOR2BX1TR U620 ( .AN(ReadB[7]), .B(n300), .Y(N91) );
  NOR2BX1TR U621 ( .AN(ReadB[15]), .B(n301), .Y(N99) );
  CLKBUFX2TR U622 ( .A(n372), .Y(n371) );
  CLKBUFX2TR U623 ( .A(n372), .Y(n369) );
  CLKBUFX2TR U624 ( .A(n372), .Y(n370) );
  NOR2X1TR U625 ( .A(n481), .B(n477), .Y(ReadA[0]) );
  NOR2X1TR U626 ( .A(n489), .B(n485), .Y(ReadB[0]) );
  NOR2X1TR U627 ( .A(n497), .B(n493), .Y(N374) );
  NOR2X1TR U628 ( .A(n135), .B(n453), .Y(Stall) );
  CLKINVX2TR U629 ( .A(n151), .Y(n392) );
  NOR3X1TR U630 ( .A(n455), .B(FunISEL[0]), .C(n452), .Y(n146) );
  OAI2BB2XLTR U631 ( .B0(n340), .B1(n431), .A0N(n367), .A1N(N375), .Y(RWL_N[1]) );
  OAI2BB2XLTR U632 ( .B0(n339), .B1(n429), .A0N(n367), .A1N(N376), .Y(RWL_N[2]) );
  OAI2BB2XLTR U633 ( .B0(n339), .B1(n427), .A0N(n368), .A1N(N377), .Y(RWL_N[3]) );
  OAI2BB2XLTR U634 ( .B0(n339), .B1(n425), .A0N(n368), .A1N(N378), .Y(RWL_N[4]) );
  OAI2BB2XLTR U635 ( .B0(n339), .B1(n423), .A0N(n368), .A1N(N379), .Y(RWL_N[5]) );
  OAI2BB2XLTR U636 ( .B0(n340), .B1(n421), .A0N(n368), .A1N(N380), .Y(RWL_N[6]) );
  OAI2BB2XLTR U637 ( .B0(n338), .B1(n419), .A0N(n369), .A1N(N381), .Y(RWL_N[7]) );
  OAI2BB2XLTR U638 ( .B0(n338), .B1(n417), .A0N(n362), .A1N(N382), .Y(RWL_N[8]) );
  OAI2BB2XLTR U639 ( .B0(n338), .B1(n415), .A0N(n372), .A1N(N383), .Y(RWL_N[9]) );
  OAI2BB2XLTR U640 ( .B0(n341), .B1(n413), .A0N(n366), .A1N(N384), .Y(
        RWL_N[10]) );
  OAI2BB2XLTR U641 ( .B0(n341), .B1(n411), .A0N(n366), .A1N(N385), .Y(
        RWL_N[11]) );
  OAI2BB2XLTR U642 ( .B0(n341), .B1(n409), .A0N(n366), .A1N(N386), .Y(
        RWL_N[12]) );
  OAI2BB2XLTR U643 ( .B0(n341), .B1(n407), .A0N(n366), .A1N(N387), .Y(
        RWL_N[13]) );
  OAI2BB2XLTR U644 ( .B0(n340), .B1(n405), .A0N(n367), .A1N(N388), .Y(
        RWL_N[14]) );
  OAI2BB2XLTR U645 ( .B0(n340), .B1(n403), .A0N(n367), .A1N(N389), .Y(
        RWL_N[15]) );
  OAI21X1TR U646 ( .A0(n343), .A1(n431), .B0(n357), .Y(RWL_P[1]) );
  OAI21X1TR U647 ( .A0(n344), .A1(n429), .B0(n358), .Y(RWL_P[2]) );
  OAI21X1TR U648 ( .A0(n344), .A1(n427), .B0(n359), .Y(RWL_P[3]) );
  OAI21X1TR U649 ( .A0(N382), .A1(n352), .B0(n418), .Y(RWLB_P[8]) );
  OAI21X1TR U650 ( .A0(N383), .A1(n352), .B0(n416), .Y(RWLB_P[9]) );
  OAI21X1TR U651 ( .A0(N374), .A1(n353), .B0(n434), .Y(RWLB_P[0]) );
  OAI21X1TR U652 ( .A0(N375), .A1(n355), .B0(n432), .Y(RWLB_P[1]) );
  OAI21X1TR U653 ( .A0(N376), .A1(n355), .B0(n430), .Y(RWLB_P[2]) );
  OAI21X1TR U654 ( .A0(N377), .A1(n353), .B0(n428), .Y(RWLB_P[3]) );
  OAI21X1TR U655 ( .A0(N378), .A1(n353), .B0(n426), .Y(RWLB_P[4]) );
  OAI21X1TR U656 ( .A0(N379), .A1(n353), .B0(n424), .Y(RWLB_P[5]) );
  OAI21X1TR U657 ( .A0(N380), .A1(n352), .B0(n422), .Y(RWLB_P[6]) );
  OAI21X1TR U658 ( .A0(N381), .A1(n352), .B0(n420), .Y(RWLB_P[7]) );
  OAI21X1TR U659 ( .A0(N384), .A1(n354), .B0(n414), .Y(RWLB_P[10]) );
  OAI21X1TR U660 ( .A0(N385), .A1(n354), .B0(n412), .Y(RWLB_P[11]) );
  OAI21X1TR U661 ( .A0(N386), .A1(n354), .B0(n410), .Y(RWLB_P[12]) );
  OAI21X1TR U662 ( .A0(N387), .A1(n354), .B0(n408), .Y(RWLB_P[13]) );
  OAI21X1TR U663 ( .A0(N388), .A1(n355), .B0(n406), .Y(RWLB_P[14]) );
  OAI21X1TR U664 ( .A0(N389), .A1(n355), .B0(n404), .Y(RWLB_P[15]) );
  OAI21X1TR U665 ( .A0(n345), .A1(n425), .B0(n359), .Y(RWL_P[4]) );
  OAI21X1TR U666 ( .A0(n345), .A1(n423), .B0(n358), .Y(RWL_P[5]) );
  OAI21X1TR U667 ( .A0(n344), .A1(n421), .B0(n359), .Y(RWL_P[6]) );
  OAI21X1TR U668 ( .A0(n345), .A1(n419), .B0(n359), .Y(RWL_P[7]) );
  OAI21X1TR U669 ( .A0(n345), .A1(n415), .B0(n358), .Y(RWL_P[9]) );
  OAI21X1TR U670 ( .A0(n343), .A1(n407), .B0(n357), .Y(RWL_P[13]) );
  OAI21X1TR U671 ( .A0(n344), .A1(n403), .B0(n358), .Y(RWL_P[15]) );
  NOR2BX1TR U672 ( .AN(n192), .B(n205), .Y(n175) );
  CLKINVX2TR U673 ( .A(n178), .Y(n3810) );
  NAND2BX1TR U674 ( .AN(n134), .B(n452), .Y(WEN) );
  CLKINVX2TR U675 ( .A(n168), .Y(n395) );
  CLKBUFX2TR U676 ( .A(n139), .Y(n327) );
  CLKBUFX2TR U677 ( .A(n139), .Y(n328) );
  CLKINVX2TR U678 ( .A(n172), .Y(n3850) );
  CLKINVX2TR U679 ( .A(n197), .Y(n398) );
  CLKAND2X2TR U680 ( .A(N204), .B(n143), .Y(Br) );
  OAI31X1TR U681 ( .A0(n10), .A1(n448), .A2(n287), .B0(n446), .Y(N204) );
  AOI22X1TR U682 ( .A0(N257), .A1(n9), .B0(N258), .B1(n297), .Y(n10) );
  CLKINVX2TR U683 ( .A(n325), .Y(n333) );
  CLKINVX2TR U684 ( .A(n325), .Y(n332) );
  CLKINVX2TR U685 ( .A(n325), .Y(n331) );
  CLKBUFX2TR U686 ( .A(n138), .Y(n372) );
  AOI31X1TR U687 ( .A0(n222), .A1(n443), .A2(n209), .B0(n223), .Y(AluSEL[1])
         );
  OAI21X1TR U688 ( .A0(n455), .A1(n453), .B0(n445), .Y(n222) );
  NAND2X1TR U689 ( .A(n433), .B(n431), .Y(n497) );
  NAND2X1TR U690 ( .A(n451), .B(n450), .Y(n489) );
  NAND2X1TR U691 ( .A(n459), .B(n458), .Y(n481) );
  NAND2X1TR U692 ( .A(n429), .B(n427), .Y(n493) );
  NAND2X1TR U693 ( .A(n449), .B(n448), .Y(n485) );
  NAND2X1TR U694 ( .A(n457), .B(n456), .Y(n477) );
  NOR3X1TR U695 ( .A(n443), .B(n435), .C(n137), .Y(LuiSEL) );
  NOR4XLTR U696 ( .A(n3850), .B(n3890), .C(n3870), .D(n3860), .Y(n165) );
  NOR3X1TR U697 ( .A(n445), .B(n437), .C(n443), .Y(n223) );
  NOR2X1TR U698 ( .A(n483), .B(n477), .Y(ReadA[1]) );
  NOR2X1TR U699 ( .A(n483), .B(n480), .Y(ReadA[5]) );
  NOR2X1TR U700 ( .A(n483), .B(n482), .Y(ReadA[9]) );
  NOR2X1TR U701 ( .A(n483), .B(n476), .Y(ReadA[13]) );
  NOR2X1TR U702 ( .A(n482), .B(n481), .Y(ReadA[8]) );
  NOR2X1TR U703 ( .A(n482), .B(n478), .Y(ReadA[10]) );
  NOR2X1TR U704 ( .A(n482), .B(n479), .Y(ReadA[11]) );
  NOR2X1TR U705 ( .A(n481), .B(n480), .Y(ReadA[4]) );
  NOR2X1TR U706 ( .A(n481), .B(n476), .Y(ReadA[12]) );
  NOR2X1TR U707 ( .A(n478), .B(n477), .Y(ReadA[2]) );
  NOR2X1TR U708 ( .A(n479), .B(n477), .Y(ReadA[3]) );
  NOR2X1TR U709 ( .A(n480), .B(n478), .Y(ReadA[6]) );
  NOR2X1TR U710 ( .A(n480), .B(n479), .Y(ReadA[7]) );
  NOR2X1TR U711 ( .A(n478), .B(n476), .Y(ReadA[14]) );
  NOR2X1TR U712 ( .A(n479), .B(n476), .Y(ReadA[15]) );
  NOR2X1TR U713 ( .A(n489), .B(n488), .Y(ReadB[4]) );
  NOR2X1TR U714 ( .A(n489), .B(n484), .Y(ReadB[12]) );
  NOR2X1TR U715 ( .A(n486), .B(n485), .Y(ReadB[2]) );
  NOR2X1TR U716 ( .A(n486), .B(n484), .Y(ReadB[14]) );
  NOR2X1TR U717 ( .A(n491), .B(n485), .Y(ReadB[1]) );
  NOR2X1TR U718 ( .A(n491), .B(n488), .Y(ReadB[5]) );
  NOR2X1TR U719 ( .A(n491), .B(n490), .Y(ReadB[9]) );
  NOR2X1TR U720 ( .A(n491), .B(n484), .Y(ReadB[13]) );
  NOR2X1TR U721 ( .A(n490), .B(n489), .Y(ReadB[8]) );
  NOR2X1TR U722 ( .A(n490), .B(n486), .Y(ReadB[10]) );
  NOR2X1TR U723 ( .A(n490), .B(n487), .Y(ReadB[11]) );
  NOR2X1TR U724 ( .A(n488), .B(n486), .Y(ReadB[6]) );
  NOR2X1TR U725 ( .A(n488), .B(n487), .Y(ReadB[7]) );
  NOR2X1TR U726 ( .A(n487), .B(n485), .Y(ReadB[3]) );
  NOR2X1TR U727 ( .A(n487), .B(n484), .Y(ReadB[15]) );
  CLKINVX2TR U728 ( .A(n149), .Y(DSEL[0]) );
  NOR2X1TR U729 ( .A(n499), .B(n493), .Y(N375) );
  NOR2X1TR U730 ( .A(n499), .B(n496), .Y(N379) );
  NOR2X1TR U731 ( .A(n499), .B(n4980), .Y(N383) );
  NOR2X1TR U732 ( .A(n499), .B(n492), .Y(N387) );
  NOR2X1TR U733 ( .A(n497), .B(n496), .Y(N378) );
  NOR2X1TR U734 ( .A(n4980), .B(n497), .Y(N382) );
  NOR2X1TR U735 ( .A(n4980), .B(n494), .Y(N384) );
  NOR2X1TR U736 ( .A(n4980), .B(n495), .Y(N385) );
  NOR2X1TR U737 ( .A(n497), .B(n492), .Y(N386) );
  NOR2X1TR U738 ( .A(n494), .B(n493), .Y(N376) );
  NOR2X1TR U739 ( .A(n496), .B(n494), .Y(N380) );
  NOR2X1TR U740 ( .A(n496), .B(n495), .Y(N381) );
  NOR2X1TR U741 ( .A(n494), .B(n492), .Y(N388) );
  NOR2X1TR U742 ( .A(n495), .B(n493), .Y(N377) );
  NOR2X1TR U743 ( .A(n495), .B(n492), .Y(N389) );
  NOR2BX1TR U744 ( .AN(n149), .B(n145), .Y(DSEL[1]) );
  CLKINVX2TR U745 ( .A(n224), .Y(n453) );
  CLKINVX2TR U746 ( .A(n209), .Y(n437) );
  OAI33X1TR U747 ( .A0(n454), .A1(imm8_disp[5]), .A2(n439), .B0(n137), .B1(
        n291), .B2(n435), .Y(MovSEL) );
  OAI211X1TR U748 ( .A0(Dxac[16]), .A1(n175), .B0(n176), .C0(n177), .Y(n167)
         );
  AOI211X1TR U749 ( .A0(n196), .A1(n219), .B0(n205), .C0(n220), .Y(n193) );
  NOR2X1TR U750 ( .A(n402), .B(Dxac[3]), .Y(n219) );
  OAI33XLTR U751 ( .A0(n197), .A1(Dxac[7]), .A2(n400), .B0(n395), .B1(Dxac[11]), .B2(n396), .Y(n220) );
  OAI211X1TR U752 ( .A0(n212), .A1(n3850), .B0(n213), .C0(n176), .Y(n189) );
  NAND3X1TR U753 ( .A(n3810), .B(n3820), .C(Dxac[26]), .Y(n213) );
  AOI33X1TR U754 ( .A0(n390), .A1(n3890), .A2(Dxac[18]), .B0(n279), .B1(n3860), 
        .B2(Dxac[22]), .Y(n212) );
  CLKINVX2TR U755 ( .A(Dxac[19]), .Y(n390) );
  OAI211X1TR U756 ( .A0(n206), .A1(n347), .B0(n207), .C0(n208), .Y(DOut[0]) );
  NAND2X1TR U757 ( .A(Dsram[0]), .B(n364), .Y(n207) );
  AOI211X1TR U758 ( .A0(n210), .A1(n280), .B0(n211), .C0(n189), .Y(n206) );
  AOI22X1TR U759 ( .A0(DIn[0]), .A1(n284), .B0(pc[0]), .B1(n309), .Y(n208) );
  OAI211X1TR U760 ( .A0(n184), .A1(n346), .B0(n185), .C0(n186), .Y(DOut[1]) );
  AOI211X1TR U761 ( .A0(n187), .A1(n280), .B0(n188), .C0(n189), .Y(n184) );
  AOI22X1TR U762 ( .A0(DIn[1]), .A1(n283), .B0(pc[1]), .B1(n308), .Y(n186) );
  AOI211X1TR U763 ( .A0(n172), .A1(n173), .B0(n174), .C0(n167), .Y(n169) );
  AOI22X1TR U764 ( .A0(DIn[2]), .A1(n286), .B0(pc[2]), .B1(n309), .Y(n171) );
  CLKINVX2TR U765 ( .A(Dsram[3]), .Y(n3780) );
  CLKINVX2TR U766 ( .A(Dsram[5]), .Y(n3770) );
  AOI22X1TR U767 ( .A0(DIn[5]), .A1(n285), .B0(pc[5]), .B1(n308), .Y(n157) );
  OAI211X1TR U768 ( .A0(n360), .A1(n3760), .B0(n151), .C0(n156), .Y(DOut[6])
         );
  CLKINVX2TR U769 ( .A(Dsram[6]), .Y(n3760) );
  AOI22X1TR U770 ( .A0(DIn[6]), .A1(n284), .B0(pc[6]), .B1(n309), .Y(n156) );
  OAI211X1TR U771 ( .A0(n360), .A1(n3750), .B0(n298), .C0(n155), .Y(DOut[7])
         );
  CLKINVX2TR U772 ( .A(Dsram[7]), .Y(n3750) );
  AOI22X1TR U773 ( .A0(DIn[7]), .A1(n283), .B0(pc[7]), .B1(n308), .Y(n155) );
  CLKINVX2TR U774 ( .A(Dsram[8]), .Y(n3740) );
  AOI22X1TR U775 ( .A0(DIn[8]), .A1(n286), .B0(pc[8]), .B1(n309), .Y(n154) );
  CLKINVX2TR U776 ( .A(Dsram[9]), .Y(n373) );
  AOI22X1TR U777 ( .A0(DIn[9]), .A1(n285), .B0(pc[9]), .B1(n308), .Y(n152) );
  OAI31X1TR U778 ( .A0(n8), .A1(n448), .A2(n287), .B0(n446), .Y(N259) );
  AOI22X1TR U779 ( .A0(N257), .A1(n9), .B0(N258), .B1(n297), .Y(n8) );
  AOI31X1TR U780 ( .A0(Dxac[8]), .A1(n395), .A2(n175), .B0(n281), .Y(n2040) );
  NOR4BBX1TR U781 ( .AN(Dxac[14]), .BN(Dxac[12]), .C(Dxac[15]), .D(n399), .Y(
        n205) );
  OAI32X1TR U782 ( .A0(n3820), .A1(n178), .A2(n3830), .B0(n281), .B1(n179), 
        .Y(n174) );
  AOI32X1TR U783 ( .A0(Dxac[10]), .A1(n168), .A2(Dxac[11]), .B0(n180), .B1(
        n399), .Y(n179) );
  OAI31X1TR U784 ( .A0(n401), .A1(Dxac[4]), .A2(n402), .B0(n181), .Y(n180) );
  OAI2BB1X1TR U785 ( .A0N(Dxac[6]), .A1N(Dxac[7]), .B0(Dxac[4]), .Y(n181) );
  AOI33X1TR U786 ( .A0(Dxac[5]), .A1(n400), .A2(n398), .B0(Dxac[1]), .B1(n402), 
        .B2(n196), .Y(n195) );
  CLKINVX2TR U787 ( .A(rDest_0_0), .Y(n451) );
  OAI211X1TR U788 ( .A0(Dxac[13]), .A1(n192), .B0(n193), .C0(n216), .Y(n210)
         );
  AOI31X1TR U789 ( .A0(n396), .A1(n397), .A2(n168), .B0(n217), .Y(n216) );
  OAI31X1TR U790 ( .A0(n197), .A1(Dxac[6]), .A2(Dxac[5]), .B0(n218), .Y(n217)
         );
  NAND4BX1TR U791 ( .AN(Dxac[1]), .B(Dxac[0]), .C(n196), .D(n402), .Y(n218) );
  OAI211X1TR U792 ( .A0(n192), .A1(n393), .B0(n193), .C0(n394), .Y(n187) );
  CLKINVX2TR U793 ( .A(Dxac[13]), .Y(n393) );
  CLKINVX2TR U794 ( .A(n194), .Y(n394) );
  OAI31X1TR U795 ( .A0(n395), .A1(Dxac[10]), .A2(n397), .B0(n195), .Y(n194) );
  NAND4BX1TR U796 ( .AN(n314), .B(n133), .C(n134), .D(n140), .Y(n139) );
  AOI221X1TR U797 ( .A0(n141), .A1(n502), .B0(opCode_1_0), .B1(n142), .C0(n143), .Y(n140) );
  NAND3BX1TR U798 ( .AN(Dxac[14]), .B(Dxac[8]), .C(Dxac[12]), .Y(n192) );
  NAND2BX1TR U799 ( .AN(Dxac[30]), .B(n161), .Y(n177) );
  NAND2X1TR U800 ( .A(Dsram[4]), .B(n365), .Y(n159) );
  AOI22X1TR U801 ( .A0(DIn[4]), .A1(n284), .B0(pc[4]), .B1(n441), .Y(n160) );
  NOR2X1TR U802 ( .A(n280), .B(Dxac[24]), .Y(n172) );
  NOR2X1TR U803 ( .A(n399), .B(Dxac[12]), .Y(n168) );
  NOR2X1TR U804 ( .A(n451), .B(rDest_1_0), .Y(n9) );
  NAND2X1TR U805 ( .A(Dxac[4]), .B(n399), .Y(n197) );
  CLKINVX2TR U806 ( .A(rDest_2_0), .Y(n449) );
  CLKINVX2TR U807 ( .A(rDest_3_0), .Y(n448) );
  CLKINVX2TR U808 ( .A(Dxac[8]), .Y(n399) );
  NAND3BX1TR U809 ( .AN(Dxac[28]), .B(Dxac[24]), .C(Dxac[16]), .Y(n178) );
  CLKINVX2TR U810 ( .A(Dxac[2]), .Y(n402) );
  NAND2X1TR U811 ( .A(n279), .B(n3870), .Y(n183) );
  AOI22X1TR U812 ( .A0(rDest_1_0), .A1(n2), .B0(ZOUT), .B1(n9), .Y(n4) );
  XNOR2X1TR U813 ( .A(n294), .B(COUT), .Y(n2) );
  NOR2X1TR U814 ( .A(Dxac[4]), .B(Dxac[8]), .Y(n196) );
  CLKINVX2TR U815 ( .A(opCode_0_0), .Y(n445) );
  NOR3X1TR U816 ( .A(n454), .B(n500), .C(n135), .Y(n503) );
  NOR2X1TR U817 ( .A(n334), .B(rSrcIn[0]), .Y(RWLB_N[0]) );
  NOR2X1TR U818 ( .A(n336), .B(rSrcIn[1]), .Y(RWLB_N[1]) );
  NOR2X1TR U819 ( .A(n336), .B(rSrcIn[2]), .Y(RWLB_N[2]) );
  NOR2X1TR U820 ( .A(n336), .B(rSrcIn[3]), .Y(RWLB_N[3]) );
  NOR2X1TR U821 ( .A(n334), .B(rSrcIn[4]), .Y(RWLB_N[4]) );
  NOR2X1TR U822 ( .A(n336), .B(rSrcIn[5]), .Y(RWLB_N[5]) );
  NOR2X1TR U823 ( .A(n337), .B(rSrcIn[6]), .Y(RWLB_N[6]) );
  NOR2X1TR U824 ( .A(n337), .B(rSrcIn[7]), .Y(RWLB_N[7]) );
  NOR2X1TR U825 ( .A(n337), .B(rSrcIn[9]), .Y(RWLB_N[9]) );
  NOR2X1TR U826 ( .A(n334), .B(rSrcIn[10]), .Y(RWLB_N[10]) );
  NOR2X1TR U827 ( .A(n335), .B(rSrcIn[11]), .Y(RWLB_N[11]) );
  NOR2X1TR U828 ( .A(n335), .B(rSrcIn[12]), .Y(RWLB_N[12]) );
  NOR2X1TR U829 ( .A(n335), .B(rSrcIn[13]), .Y(RWLB_N[13]) );
  NOR2X1TR U830 ( .A(n335), .B(rSrcIn[14]), .Y(RWLB_N[14]) );
  NOR2X1TR U831 ( .A(n334), .B(rSrcIn[15]), .Y(RWLB_N[15]) );
  CLKINVX2TR U832 ( .A(rDest_1_0), .Y(n450) );
  OAI21X1TR U833 ( .A0(Dxac[16]), .A1(n166), .B0(n3790), .Y(n164) );
  AOI31X1TR U834 ( .A0(Dxac[6]), .A1(n398), .A2(Dxac[7]), .B0(n168), .Y(n166)
         );
  CLKINVX2TR U835 ( .A(n167), .Y(n3790) );
  OAI21X1TR U836 ( .A0(n177), .A1(n3800), .B0(n190), .Y(n188) );
  CLKINVX2TR U837 ( .A(Dxac[29]), .Y(n3800) );
  AOI32X1TR U838 ( .A0(n3810), .A1(n3830), .A2(Dxac[25]), .B0(n172), .B1(n191), 
        .Y(n190) );
  OAI32X1TR U839 ( .A0(n391), .A1(n279), .A2(Dxac[18]), .B0(n183), .B1(n3880), 
        .Y(n191) );
  OAI21X1TR U840 ( .A0(Dxac[29]), .A1(n177), .B0(n214), .Y(n211) );
  AOI32X1TR U841 ( .A0(n3840), .A1(n3830), .A2(n3810), .B0(n172), .B1(n215), 
        .Y(n214) );
  CLKINVX2TR U842 ( .A(Dxac[25]), .Y(n3840) );
  OAI32X1TR U843 ( .A0(Dxac[17]), .A1(n279), .A2(Dxac[18]), .B0(Dxac[21]), 
        .B1(n183), .Y(n215) );
  CLKINVX2TR U844 ( .A(n7), .Y(n446) );
  OAI32X1TR U845 ( .A0(n449), .A1(n450), .A2(n6), .B0(rDest_2_0), .B1(n5), .Y(
        n7) );
  XNOR2X1TR U846 ( .A(n451), .B(n1), .Y(n6) );
  AOI32X1TR U847 ( .A0(rDest_3_0), .A1(n450), .A2(N498), .B0(n4), .B1(n447), 
        .Y(n5) );
  AND3X2TR U848 ( .A(Dxac[24]), .B(n281), .C(Dxac[28]), .Y(n161) );
  OAI2BB1X1TR U849 ( .A0N(DIn[10]), .A1N(n286), .B0(n203), .Y(DOut[10]) );
  OAI2BB1X1TR U850 ( .A0N(DIn[11]), .A1N(n285), .B0(n202), .Y(DOut[11]) );
  OAI2BB1X1TR U851 ( .A0N(DIn[12]), .A1N(n284), .B0(n201), .Y(DOut[12]) );
  OAI2BB1X1TR U852 ( .A0N(DIn[13]), .A1N(n283), .B0(n200), .Y(DOut[13]) );
  AOI21X1TR U853 ( .A0(Dsram[13]), .A1(n364), .B0(n306), .Y(n200) );
  OAI2BB1X1TR U854 ( .A0N(DIn[14]), .A1N(n286), .B0(n199), .Y(DOut[14]) );
  AOI21X1TR U855 ( .A0(Dsram[14]), .A1(n364), .B0(n306), .Y(n199) );
  OAI2BB1X1TR U856 ( .A0N(DIn[15]), .A1N(n285), .B0(n198), .Y(DOut[15]) );
  AOI21X1TR U857 ( .A0(Dsram[15]), .A1(n364), .B0(n306), .Y(n198) );
  OAI21X1TR U858 ( .A0(n474), .A1(n302), .B0(n117), .Y(n227) );
  CLKINVX2TR U859 ( .A(Col_Sel[14]), .Y(n474) );
  AOI22X1TR U860 ( .A0(ReadA[14]), .A1(n310), .B0(Col_Sel[13]), .B1(n332), .Y(
        n117) );
  OAI21X1TR U861 ( .A0(n330), .A1(n475), .B0(n115), .Y(n226) );
  CLKINVX2TR U862 ( .A(Col_Sel[15]), .Y(n475) );
  AOI22X1TR U863 ( .A0(ReadA[15]), .A1(n310), .B0(Col_Sel[14]), .B1(n333), .Y(
        n115) );
  OAI21X1TR U864 ( .A0(n330), .A1(n473), .B0(n118), .Y(n228) );
  CLKINVX2TR U865 ( .A(Col_Sel[13]), .Y(n473) );
  AOI22X1TR U866 ( .A0(ReadA[13]), .A1(n310), .B0(Col_Sel[12]), .B1(n331), .Y(
        n118) );
  OAI21X1TR U867 ( .A0(n330), .A1(n472), .B0(n119), .Y(n229) );
  CLKINVX2TR U868 ( .A(Col_Sel[12]), .Y(n472) );
  AOI22X1TR U869 ( .A0(ReadA[12]), .A1(n313), .B0(Col_Sel[11]), .B1(n293), .Y(
        n119) );
  OAI21X1TR U870 ( .A0(n330), .A1(n471), .B0(n120), .Y(n230) );
  CLKINVX2TR U871 ( .A(Col_Sel[11]), .Y(n471) );
  AOI22X1TR U872 ( .A0(ReadA[11]), .A1(n312), .B0(Col_Sel[10]), .B1(n331), .Y(
        n120) );
  OAI21X1TR U873 ( .A0(n302), .A1(n470), .B0(n121), .Y(n231) );
  CLKINVX2TR U874 ( .A(Col_Sel[10]), .Y(n470) );
  AOI22X1TR U875 ( .A0(ReadA[10]), .A1(n311), .B0(Col_Sel[9]), .B1(n333), .Y(
        n121) );
  OAI21X1TR U876 ( .A0(n329), .A1(n469), .B0(n122), .Y(n232) );
  CLKINVX2TR U877 ( .A(Col_Sel[9]), .Y(n469) );
  AOI22X1TR U878 ( .A0(ReadA[9]), .A1(n313), .B0(Col_Sel[8]), .B1(n332), .Y(
        n122) );
  OAI21X1TR U879 ( .A0(n303), .A1(n468), .B0(n123), .Y(n233) );
  CLKINVX2TR U880 ( .A(Col_Sel[8]), .Y(n468) );
  AOI22X1TR U881 ( .A0(ReadA[8]), .A1(n312), .B0(Col_Sel[7]), .B1(n333), .Y(
        n123) );
  OAI21X1TR U882 ( .A0(n302), .A1(n467), .B0(n124), .Y(n234) );
  CLKINVX2TR U883 ( .A(Col_Sel[7]), .Y(n467) );
  AOI22X1TR U884 ( .A0(ReadA[7]), .A1(n311), .B0(Col_Sel[6]), .B1(n293), .Y(
        n124) );
  OAI21X1TR U885 ( .A0(n329), .A1(n466), .B0(n125), .Y(n235) );
  CLKINVX2TR U886 ( .A(Col_Sel[6]), .Y(n466) );
  AOI22X1TR U887 ( .A0(ReadA[6]), .A1(n313), .B0(Col_Sel[5]), .B1(n331), .Y(
        n125) );
  OAI21X1TR U888 ( .A0(n303), .A1(n465), .B0(n126), .Y(n236) );
  CLKINVX2TR U889 ( .A(Col_Sel[5]), .Y(n465) );
  AOI22X1TR U890 ( .A0(ReadA[5]), .A1(n312), .B0(Col_Sel[4]), .B1(n293), .Y(
        n126) );
  OAI21X1TR U891 ( .A0(n302), .A1(n464), .B0(n127), .Y(n237) );
  CLKINVX2TR U892 ( .A(Col_Sel[4]), .Y(n464) );
  AOI22X1TR U893 ( .A0(ReadA[4]), .A1(n311), .B0(Col_Sel[3]), .B1(n332), .Y(
        n127) );
  OAI21X1TR U894 ( .A0(n329), .A1(n463), .B0(n128), .Y(n238) );
  CLKINVX2TR U895 ( .A(Col_Sel[3]), .Y(n463) );
  AOI22X1TR U896 ( .A0(ReadA[3]), .A1(n313), .B0(Col_Sel[2]), .B1(n333), .Y(
        n128) );
  OAI21X1TR U897 ( .A0(n303), .A1(n462), .B0(n129), .Y(n239) );
  CLKINVX2TR U898 ( .A(Col_Sel[2]), .Y(n462) );
  AOI22X1TR U899 ( .A0(ReadA[2]), .A1(n312), .B0(Col_Sel[1]), .B1(n332), .Y(
        n129) );
  OAI21X1TR U900 ( .A0(n303), .A1(n461), .B0(n130), .Y(n240) );
  CLKINVX2TR U901 ( .A(Col_Sel[1]), .Y(n461) );
  AOI22X1TR U902 ( .A0(ReadA[1]), .A1(n311), .B0(Col_Sel[0]), .B1(n331), .Y(
        n130) );
  OAI21X1TR U903 ( .A0(n329), .A1(n460), .B0(n131), .Y(n241) );
  CLKINVX2TR U904 ( .A(Col_Sel[0]), .Y(n460) );
  NOR2X1TR U905 ( .A(n439), .B(imm8_disp[6]), .Y(n141) );
  CLKINVX2TR U906 ( .A(Dxac[22]), .Y(n3870) );
  CLKINVX2TR U907 ( .A(n3), .Y(n447) );
  OAI31X1TR U908 ( .A0(n307), .A1(ZOUT), .A2(rDest_0_0), .B0(n288), .Y(n3) );
  NOR2X1TR U909 ( .A(rDest_3_0), .B(NOUT), .Y(n1) );
  CLKINVX2TR U910 ( .A(Dxac[10]), .Y(n396) );
  CLKINVX2TR U911 ( .A(Dxac[6]), .Y(n400) );
  OAI31X1TR U912 ( .A0(n437), .A1(opCode_0_0), .A2(n225), .B0(n436), .Y(
        AluSEL[0]) );
  CLKINVX2TR U913 ( .A(n223), .Y(n436) );
  OAI211X1TR U914 ( .A0(Dxac[23]), .A1(n3890), .B0(n182), .C0(n183), .Y(n173)
         );
  NAND3X1TR U915 ( .A(Dxac[18]), .B(n3890), .C(Dxac[19]), .Y(n182) );
  CLKINVX2TR U916 ( .A(Dxac[20]), .Y(n3890) );
  CLKINVX2TR U917 ( .A(rSrcIn[2]), .Y(n429) );
  CLKINVX2TR U918 ( .A(rSrcIn[0]), .Y(n433) );
  CLKINVX2TR U919 ( .A(rSrcIn[3]), .Y(n427) );
  CLKINVX2TR U920 ( .A(rSrcIn[1]), .Y(n431) );
  OR2X2TR U921 ( .A(ZOUT), .B(NOUT), .Y(N257) );
  NAND3BX1TR U922 ( .AN(Dxac[31]), .B(n161), .C(Dxac[30]), .Y(n176) );
  NAND2X1TR U923 ( .A(ZOUT), .B(NOUT), .Y(N258) );
  CLKINVX2TR U924 ( .A(opCode_1_0), .Y(n443) );
  NOR2X1TR U925 ( .A(imm8_disp[6]), .B(imm8_disp[7]), .Y(n224) );
  NOR2X1TR U926 ( .A(n136), .B(n452), .Y(SEL1) );
  NOR2X1TR U927 ( .A(imm8_disp[6]), .B(Amount[1]), .Y(Amount[0]) );
  CLKINVX2TR U928 ( .A(Dxac[26]), .Y(n3830) );
  CLKINVX2TR U929 ( .A(Dxac[23]), .Y(n3860) );
  CLKINVX2TR U930 ( .A(rSrcIn[4]), .Y(n425) );
  CLKINVX2TR U931 ( .A(rSrcIn[5]), .Y(n423) );
  CLKINVX2TR U932 ( .A(rSrcIn[6]), .Y(n421) );
  CLKINVX2TR U933 ( .A(rSrcIn[7]), .Y(n419) );
  CLKINVX2TR U934 ( .A(rSrcIn[8]), .Y(n417) );
  CLKINVX2TR U935 ( .A(rSrcIn[9]), .Y(n415) );
  CLKINVX2TR U936 ( .A(rSrcIn[10]), .Y(n413) );
  CLKINVX2TR U937 ( .A(rSrcIn[11]), .Y(n411) );
  CLKINVX2TR U938 ( .A(rSrcIn[12]), .Y(n409) );
  CLKINVX2TR U939 ( .A(rSrcIn[13]), .Y(n407) );
  CLKINVX2TR U940 ( .A(rSrcIn[14]), .Y(n405) );
  CLKINVX2TR U941 ( .A(rSrcIn[15]), .Y(n403) );
  CLKINVX2TR U942 ( .A(Dxac[27]), .Y(n3820) );
  CLKINVX2TR U943 ( .A(imm8_disp[2]), .Y(n457) );
  CLKINVX2TR U944 ( .A(imm8_disp[0]), .Y(n459) );
  CLKINVX2TR U945 ( .A(imm8_disp[1]), .Y(n458) );
  CLKINVX2TR U946 ( .A(imm8_disp[3]), .Y(n456) );
  CLKINVX2TR U947 ( .A(Dxac[9]), .Y(n397) );
  CLKINVX2TR U948 ( .A(Dxac[17]), .Y(n391) );
  CLKINVX2TR U949 ( .A(Dxac[3]), .Y(n401) );
  CLKINVX2TR U950 ( .A(Dxac[21]), .Y(n3880) );
endmodule

