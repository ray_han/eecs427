* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT INV_RAY SEL SEL_BAR VSS! IN<1> VDD! OUT IN<0>
** N=47 EP=7 IP=0 FDC=10
M0 SEL_BAR SEL VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=320 $Y=450 $D=97
M1 3 SEL IN<1> VSS! nfet L=1.4e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=5270 $D=97
M2 VSS! SEL SEL_BAR VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=800 $Y=450 $D=97
M3 OUT 3 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1320 $Y=450 $D=97
M4 IN<0> SEL_BAR 3 VSS! nfet L=1.4e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1330 $Y=5270 $D=97
M5 SEL_BAR SEL VDD! VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=320 $Y=2020 $D=189
M6 3 SEL_BAR IN<1> VDD! pfet L=1.4e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=530 $Y=3510 $D=189
M7 VDD! SEL SEL_BAR VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=800 $Y=2020 $D=189
M8 OUT 3 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1320 $Y=1950 $D=189
M9 IN<0> SEL 3 VDD! pfet L=1.4e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1330 $Y=3510 $D=189
.ENDS
***************************************
