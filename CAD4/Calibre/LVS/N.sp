* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT N P14 VSS! N VDD! COUT
** N=74 EP=5 IP=0 FDC=18
M0 10 7 N VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=470 $D=97
M1 VSS! 1 10 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=950 $D=97
M2 11 2 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1430 $D=97
M3 7 3 11 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1910 $D=97
M4 12 3 1 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=470 $D=97
M5 VSS! P14 12 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=950 $D=97
M6 13 P14 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=1430 $D=97
M7 3 2 13 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5010 $Y=1910 $D=97
M8 VSS! COUT 2 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=6550 $Y=1490 $D=97
M9 N 7 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1900 $Y=470 $D=189
M10 VDD! 1 N VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1900 $Y=950 $D=189
M11 7 2 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1900 $Y=1430 $D=189
M12 VDD! 3 7 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1900 $Y=1910 $D=189
M13 1 3 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3560 $Y=470 $D=189
M14 VDD! P14 1 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3560 $Y=950 $D=189
M15 3 P14 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3560 $Y=1430 $D=189
M16 VDD! 2 3 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3560 $Y=1910 $D=189
M17 VDD! COUT 2 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=7910 $Y=1490 $D=189
.ENDS
***************************************
