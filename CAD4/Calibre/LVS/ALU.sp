* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT MUX21_BAR SEL VSS! IN<1> VDD! OUT IN<0>
** N=41 EP=6 IP=0 FDC=8
M0 8 SEL IN<1> VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=600 $Y=5270 $D=97
M1 VSS! SEL 7 VSS! nfet L=1.2e-07 W=5.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=640 $Y=450 $D=97
M2 OUT 8 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1120 $Y=450 $D=97
M3 IN<0> 7 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1280 $Y=5270 $D=97
M4 8 7 IN<1> VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=600 $Y=3510 $D=189
M5 VDD! SEL 7 VDD! pfet L=1.2e-07 W=9.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=640 $Y=1580 $D=189
M6 OUT 8 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1120 $Y=1950 $D=189
M7 IN<0> SEL 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1280 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT MUX41 SEL1 SEL0 OUT VSS! IN3 IN2 VDD! IN0 IN1
** N=127 EP=9 IP=0 FDC=28
M0 VSS! SEL1 12 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1090 $D=97
M1 13 SEL0 IN3 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2080 $D=97
M2 IN2 10 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2880 $D=97
M3 10 SEL0 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3880 $D=97
M4 OUT 11 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-1350 $D=97
M5 VSS! 11 OUT VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-870 $D=97
M6 OUT 11 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-390 $D=97
M7 VSS! 11 OUT VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=90 $D=97
M8 VSS! 14 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=-650 $D=97
M9 14 SEL1 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=940 $D=97
M10 15 12 14 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=2090 $D=97
M11 15 10 IN0 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=3340 $D=97
M12 IN1 SEL0 15 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=4140 $D=97
M13 OUT 11 VDD! VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-1350 $D=189
M14 VDD! 11 OUT VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-870 $D=189
M15 OUT 11 VDD! VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-390 $D=189
M16 VDD! 11 OUT VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=90 $D=189
M17 12 SEL1 VDD! VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=610 $D=189
M18 VDD! SEL1 12 VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1090 $D=189
M19 10 SEL0 VDD! VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=3880 $D=189
M20 VDD! SEL0 10 VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=4360 $D=189
M21 13 10 IN3 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=2080 $D=189
M22 IN2 SEL0 13 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=2880 $D=189
M23 VDD! 14 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=-650 $D=189
M24 14 12 13 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=940 $D=189
M25 15 SEL1 14 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2140 $D=189
M26 15 SEL0 IN0 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3340 $D=189
M27 IN1 10 15 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=4140 $D=189
.ENDS
***************************************
.SUBCKT SETUP_BIT A B XOR AND OR VSS! VDD!
** N=98 EP=7 IP=0 FDC=26
M0 13 A 11 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=5030 $D=97
M1 VSS! A 10 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=520 $Y=430 $D=97
M2 VSS! B 13 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=5030 $D=97
M3 8 B VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1000 $Y=430 $D=97
M4 AND 11 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=5290 $D=97
M5 14 8 OR VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=430 $D=97
M6 15 A 12 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=5030 $D=97
M7 VSS! 10 14 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=430 $D=97
M8 VSS! 8 15 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=5030 $D=97
M9 16 10 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=430 $D=97
M10 17 12 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=5030 $D=97
M11 9 B 16 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=430 $D=97
M12 XOR 9 17 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=5030 $D=97
M13 11 A VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=500 $Y=3510 $D=189
M14 VDD! A 10 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=520 $Y=1950 $D=189
M15 VDD! B 11 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=980 $Y=3510 $D=189
M16 8 B VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1000 $Y=1950 $D=189
M17 AND 11 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1460 $Y=3510 $D=189
M18 OR 8 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=1950 $D=189
M19 12 A VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2410 $Y=3510 $D=189
M20 VDD! 10 OR VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=1950 $D=189
M21 VDD! 8 12 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2890 $Y=3510 $D=189
M22 9 10 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=1950 $D=189
M23 XOR 12 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3370 $Y=3510 $D=189
M24 VDD! B 9 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=1950 $D=189
M25 VDD! 9 XOR VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3850 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT ICV_1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
** N=18 EP=18 IP=32 FDC=108
X0 1 2 5 15 10 16 17 9 8 MUX41 $T=0 0 0 0 $X=-240 $Y=-2300
X1 1 2 6 15 14 18 17 13 12 MUX41 $T=6000 0 0 0 $X=5760 $Y=-2300
X2 7 3 8 9 10 15 17 SETUP_BIT $T=0 9600 0 270 $X=-240 $Y=4490
X3 11 4 12 13 14 15 17 SETUP_BIT $T=6000 9600 0 270 $X=5760 $Y=4490
.ENDS
***************************************
.SUBCKT RIPPLE_BIT_BAR B CIN A VSS! SUM VDD! COUT
** N=115 EP=7 IP=0 FDC=24
M0 10 8 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=450 $D=97
M1 VSS! B 12 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=5070 $D=97
M2 13 A VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1360 $Y=5190 $D=97
M3 9 10 CIN VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1920 $Y=450 $D=97
M4 8 B 13 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1980 $Y=5270 $D=97
M5 14 8 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2600 $Y=450 $D=97
M6 A 12 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2660 $Y=5270 $D=97
M7 VSS! CIN 14 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3220 $Y=450 $D=97
M8 SUM 9 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3700 $Y=450 $D=97
M9 11 10 B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3740 $Y=5270 $D=97
M10 CIN 8 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4420 $Y=5270 $D=97
M11 COUT 11 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4930 $Y=450 $D=97
M12 10 8 VDD! VDD! pfet L=1.2e-07 W=9.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=1540 $D=189
M13 VDD! B 12 VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=880 $Y=3510 $D=189
M14 13 A VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1360 $Y=3510 $D=189
M15 9 8 CIN VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1920 $Y=2210 $D=189
M16 8 12 13 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1980 $Y=3510 $D=189
M17 14 10 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2600 $Y=2210 $D=189
M18 A B 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2660 $Y=3510 $D=189
M19 VDD! CIN 14 VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3220 $Y=1630 $D=189
M20 SUM 9 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3700 $Y=1630 $D=189
M21 11 8 B VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3740 $Y=3510 $D=189
M22 CIN 10 11 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4420 $Y=3510 $D=189
M23 COUT 11 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4930 $Y=1630 $D=189
.ENDS
***************************************
.SUBCKT RIPPLE_BIT B CIN A SUM VSS! VDD! COUT
** N=124 EP=7 IP=0 FDC=26
M0 VSS! B 13 VSS! nfet L=1.2e-07 W=4.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=470 $Y=5070 $D=97
M1 10 8 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=450 $D=97
M2 14 A VSS! VSS! nfet L=1.2e-07 W=3.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=950 $Y=5190 $D=97
M3 8 B 14 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=5270 $D=97
M4 9 10 CIN VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1650 $Y=450 $D=97
M5 A 13 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2250 $Y=5270 $D=97
M6 15 8 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2330 $Y=450 $D=97
M7 VSS! CIN 15 VSS! nfet L=1.2e-07 W=4.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2950 $Y=450 $D=97
M8 11 10 B VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3330 $Y=5270 $D=97
M9 SUM 9 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3430 $Y=450 $D=97
M10 CIN 8 11 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4010 $Y=5270 $D=97
M11 COUT 12 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4850 $Y=450 $D=97
M12 12 11 VSS! VSS! nfet L=1.2e-07 W=4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4990 $Y=5150 $D=97
M13 VDD! B 13 VDD! pfet L=1.2e-07 W=9.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=470 $Y=3510 $D=189
M14 10 8 VDD! VDD! pfet L=1.2e-07 W=9.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=1540 $D=189
M15 14 A VDD! VDD! pfet L=1.2e-07 W=7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=950 $Y=3510 $D=189
M16 8 13 14 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1570 $Y=3510 $D=189
M17 9 8 CIN VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1650 $Y=2210 $D=189
M18 A B 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2250 $Y=3510 $D=189
M19 15 10 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2330 $Y=2210 $D=189
M20 VDD! CIN 15 VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2950 $Y=1630 $D=189
M21 11 8 B VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3330 $Y=3510 $D=189
M22 SUM 9 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3430 $Y=1630 $D=189
M23 CIN 10 11 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4010 $Y=3510 $D=189
M24 COUT 12 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4850 $Y=1630 $D=189
M25 12 11 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4990 $Y=3510 $D=189
.ENDS
***************************************
.SUBCKT RIPPLE_4BIT_LAST_4BIT C3 CIN B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! SUM<3> COUT SUM<2> SUM<1> SUM<0>
** N=19 EP=17 IP=28 FDC=102
X0 B<3> C3 A<3> VSS! SUM<3> VDD! COUT RIPPLE_BIT_BAR $T=0 0 0 0 $X=-200 $Y=-240
X1 B<2> 18 A<2> SUM<2> VSS! VDD! C3 RIPPLE_BIT $T=0 6000 0 0 $X=-200 $Y=5760
X2 B<1> 19 A<1> SUM<1> VSS! VDD! 18 RIPPLE_BIT $T=0 12000 0 0 $X=-200 $Y=11760
X3 B<0> CIN A<0> SUM<0> VSS! VDD! 19 RIPPLE_BIT $T=0 18000 0 0 $X=-200 $Y=17760
.ENDS
***************************************
.SUBCKT MUX84 SEL VDD! VSS! S3<1> S<3> S3<0> S2<1> S<2> S2<0> S1<1> S<1> S1<0> S0<1> S<0> S0<0>
** N=15 EP=15 IP=24 FDC=32
X0 SEL VSS! S3<1> VDD! S<3> S3<0> MUX21_BAR $T=6000 2000 1 270 $X=-240 $Y=-200
X1 SEL VSS! S2<1> VDD! S<2> S2<0> MUX21_BAR $T=12000 2000 1 270 $X=5760 $Y=-200
X2 SEL VSS! S1<1> VDD! S<1> S1<0> MUX21_BAR $T=18000 2000 1 270 $X=11760 $Y=-200
X3 SEL VSS! S0<1> VDD! S<0> S0<0> MUX21_BAR $T=24000 2000 1 270 $X=17760 $Y=-200
.ENDS
***************************************
.SUBCKT Z_cell4 VSS! P<1> P<3> P<0> P<2> VDD! OUT
** N=50 EP=7 IP=0 FDC=10
M0 9 P<0> VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=440 $D=97
M1 10 P<1> 9 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=920 $D=97
M2 11 P<2> 10 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=1400 $D=97
M3 8 P<3> 11 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=460 $Y=1880 $D=97
M4 VSS! 8 OUT VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5100 $Y=1070 $D=97
M5 8 P<0> VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=440 $D=189
M6 VDD! P<1> 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=920 $D=189
M7 8 P<2> VDD! VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1400 $D=189
M8 VDD! P<3> 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1880 $D=189
M9 VDD! 8 OUT VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3560 $Y=1070 $D=189
.ENDS
***************************************
.SUBCKT RIPPLE_4BIT CIN B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! SUM<3> COUT SUM<2> SUM<1> SUM<0>
** N=19 EP=16 IP=28 FDC=102
X0 B<3> 17 A<3> VSS! SUM<3> VDD! COUT RIPPLE_BIT_BAR $T=0 0 0 0 $X=-200 $Y=-240
X1 B<2> 18 A<2> SUM<2> VSS! VDD! 17 RIPPLE_BIT $T=0 6000 0 0 $X=-200 $Y=5760
X2 B<1> 19 A<1> SUM<1> VSS! VDD! 18 RIPPLE_BIT $T=0 12000 0 0 $X=-200 $Y=11760
X3 B<0> CIN A<0> SUM<0> VSS! VDD! 19 RIPPLE_BIT $T=0 18000 0 0 $X=-200 $Y=17760
.ENDS
***************************************
.SUBCKT BUFFER_SEL IN VSS! VDD! OUT
** N=85 EP=4 IP=0 FDC=14
M0 VSS! IN 5 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=730 $Y=450 $D=97
M1 6 5 VSS! VSS! nfet L=1.2e-07 W=6.2e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=450 $D=97
M2 7 6 VSS! VSS! nfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=450 $D=97
M3 VSS! 6 7 VSS! nfet L=1.2e-07 W=6.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=450 $D=97
M4 OUT 7 VSS! VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=450 $D=97
M5 VSS! 7 OUT VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=450 $D=97
M6 OUT 7 VSS! VSS! nfet L=1.2e-07 W=1e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4110 $Y=450 $D=97
M7 VDD! IN 5 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=730 $Y=4150 $D=189
M8 6 5 VDD! VDD! pfet L=1.2e-07 W=1.19e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1250 $Y=3500 $D=189
M9 7 6 VDD! VDD! pfet L=1.2e-07 W=1.3e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2190 $Y=3390 $D=189
M10 VDD! 6 7 VDD! pfet L=1.2e-07 W=1.3e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2670 $Y=3390 $D=189
M11 OUT 7 VDD! VDD! pfet L=1.2e-07 W=1.92e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3150 $Y=2770 $D=189
M12 VDD! 7 OUT VDD! pfet L=1.2e-07 W=1.92e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3630 $Y=2770 $D=189
M13 OUT 7 VDD! VDD! pfet L=1.2e-07 W=1.92e-06 m=1 par=1 nf=1 ngcon=1 psp=0 $X=4110 $Y=2770 $D=189
.ENDS
***************************************
.SUBCKT ALU SEL<1> F N SEL<0> Z CIN B<0> B<1> B<2> B<3> B<4> B<5> B<6> B<7> B<8> B<9> COUT B<10> B<11> B<12>
+ B<13> B<14> B<15> A<0> A<1> A<2> A<3> A<4> A<5> A<6> A<7> A<8> A<9> A<10> A<11> A<12> A<13> A<14> A<15> OUT<0>
+ OUT<1> OUT<2> OUT<3> OUT<4> OUT<5> OUT<6> OUT<7> OUT<8> OUT<9> OUT<10> OUT<11> OUT<12> OUT<13> OUT<14> OUT<15> VDD! VSS!
** N=502 EP=57 IP=401 FDC=1972
M0 178 CIN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2740 $Y=14450 $D=97
M1 7 178 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2740 $Y=15410 $D=97
M2 188 45 181 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40190 $Y=14960 $D=97
M3 VSS! 7 188 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40190 $Y=15440 $D=97
M4 VSS! 181 Z VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=40450 $Y=14010 $D=97
M5 VSS! COUT 70 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=64370 $Y=15090 $D=97
M6 189 75 74 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=14070 $D=97
M7 VSS! 71 189 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=14550 $D=97
M8 190 71 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=15030 $D=97
M9 75 70 190 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=65640 $Y=15510 $D=97
M10 191 183 N VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=14070 $D=97
M11 VSS! 74 191 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=14550 $D=97
M12 192 70 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=15030 $D=97
M13 183 75 192 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=70210 $Y=15510 $D=97
M14 193 94 92 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=14070 $D=97
M15 VSS! 93 193 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=14550 $D=97
M16 194 93 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=15030 $D=97
M17 94 COUT 194 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=83640 $Y=15510 $D=97
M18 195 185 F VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=14070 $D=97
M19 VSS! 92 195 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=14550 $D=97
M20 196 COUT VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=15030 $D=97
M21 185 94 196 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=88220 $Y=15510 $D=97
M22 VSS! 80 100 VSS! nfet L=1.2e-07 W=5.3e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=89650 $Y=14040 $D=97
M23 186 99 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=89650 $Y=14520 $D=97
M24 93 186 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=89650 $Y=15460 $D=97
M25 99 80 108 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=94470 $Y=14200 $D=97
M26 103 100 99 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=94470 $Y=14880 $D=97
M27 178 CIN VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1600 $Y=14450 $D=189
M28 7 178 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1600 $Y=15410 $D=189
M29 VDD! 181 Z VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38750 $Y=14010 $D=189
M30 181 45 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38750 $Y=14960 $D=189
M31 VDD! 7 181 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=38750 $Y=15440 $D=189
M32 VDD! COUT 70 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=62750 $Y=15090 $D=189
M33 74 75 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=14070 $D=189
M34 VDD! 71 74 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=14550 $D=189
M35 75 71 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=15030 $D=189
M36 VDD! 70 75 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=67100 $Y=15510 $D=189
M37 N 183 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=14070 $D=189
M38 VDD! 74 N VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=14550 $D=189
M39 183 70 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=15030 $D=189
M40 VDD! 75 183 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=68760 $Y=15510 $D=189
M41 92 94 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=14070 $D=189
M42 VDD! 93 92 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=14550 $D=189
M43 94 93 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=15030 $D=189
M44 VDD! COUT 94 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=85100 $Y=15510 $D=189
M45 F 185 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=14070 $D=189
M46 VDD! 92 F VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=14550 $D=189
M47 185 COUT VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=15030 $D=189
M48 VDD! 94 185 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=86760 $Y=15510 $D=189
M49 VDD! 80 100 VDD! pfet L=1.2e-07 W=9.1e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=90780 $Y=14040 $D=189
M50 186 99 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=91150 $Y=14520 $D=189
M51 93 186 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=91150 $Y=15460 $D=189
M52 99 100 108 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=92710 $Y=14200 $D=189
M53 103 80 99 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=92710 $Y=14880 $D=189
X54 7 VSS! 180 VDD! 29 121 MUX21_BAR $T=23200 16000 0 270 $X=22960 $Y=13800
X55 29 VSS! 182 VDD! 54 134 MUX21_BAR $T=47200 16000 0 270 $X=46960 $Y=13800
X56 54 VSS! 184 VDD! 80 147 MUX21_BAR $T=71200 16000 0 270 $X=70960 $Y=13800
X57 80 VSS! 187 VDD! COUT 160 MUX21_BAR $T=95200 16000 0 270 $X=94960 $Y=13800
X58 8 9 B<0> B<1> OUT<0> OUT<1> A<0> 13 15 11 A<1> 16 22 20 VSS! 109 VDD! 112 ICV_1 $T=5200 2000 0 0 $X=4960 $Y=-300
X59 8 9 B<2> B<3> OUT<2> OUT<3> A<2> 18 26 24 A<3> 17 31 28 VSS! 115 VDD! 118 ICV_1 $T=17200 2000 0 0 $X=16960 $Y=-300
X60 8 9 B<4> B<5> OUT<4> OUT<5> A<4> 35 37 33 A<5> 38 44 42 VSS! 122 VDD! 125 ICV_1 $T=29200 2000 0 0 $X=28960 $Y=-300
X61 8 9 B<6> B<7> OUT<6> OUT<7> A<6> 40 49 47 A<7> 39 56 53 VSS! 128 VDD! 131 ICV_1 $T=41200 2000 0 0 $X=40960 $Y=-300
X62 8 9 B<8> B<9> OUT<8> OUT<9> A<8> 59 61 58 A<9> 62 68 66 VSS! 135 VDD! 138 ICV_1 $T=53200 2000 0 0 $X=52960 $Y=-300
X63 8 9 B<10> B<11> OUT<10> OUT<11> A<10> 64 77 73 A<11> 63 82 79 VSS! 141 VDD! 144 ICV_1 $T=65200 2000 0 0 $X=64960 $Y=-300
X64 8 9 B<12> B<13> OUT<12> OUT<13> A<12> 85 87 84 A<13> 88 96 91 VSS! 148 VDD! 151 ICV_1 $T=77200 2000 0 0 $X=76960 $Y=-300
X65 8 9 B<14> B<15> OUT<14> OUT<15> A<14> 89 102 98 A<15> 71 107 105 VSS! 154 VDD! 157 ICV_1 $T=89200 2000 0 0 $X=88960 $Y=-300
X66 108 VDD! B<15> A<15> B<14> A<14> B<13> A<13> B<12> A<12> VDD! VSS! 159 187 156 153 150 RIPPLE_4BIT_LAST_4BIT $T=101200 21600 1 270 $X=76960 $Y=15800
X67 103 VSS! B<15> A<15> B<14> A<14> B<13> A<13> B<12> A<12> VDD! VSS! 106 160 101 95 86 RIPPLE_4BIT_LAST_4BIT $T=101200 27200 1 270 $X=76960 $Y=21400
X68 7 VDD! VSS! 120 118 30 117 115 25 114 112 21 111 109 14 MUX84 $T=29200 11600 1 180 $X=4960 $Y=11400
X69 29 VDD! VSS! 133 131 55 130 128 48 127 125 43 124 122 36 MUX84 $T=53200 11600 1 180 $X=28960 $Y=11400
X70 54 VDD! VSS! 146 144 81 143 141 76 140 138 67 137 135 60 MUX84 $T=77200 11600 1 180 $X=52960 $Y=11400
X71 80 VDD! VSS! 159 157 106 156 154 101 153 151 95 150 148 86 MUX84 $T=101200 11600 1 180 $X=76960 $Y=11400
X72 VSS! 16 17 13 18 VDD! 12 Z_cell4 $T=11200 13600 1 180 $X=4930 $Y=13350
X73 VSS! 38 39 35 40 VDD! 34 Z_cell4 $T=35200 13600 1 180 $X=28930 $Y=13350
X74 VSS! 34 50 12 51 VDD! 45 Z_cell4 $T=47200 13600 1 180 $X=40930 $Y=13350
X75 VSS! 62 63 59 64 VDD! 51 Z_cell4 $T=59200 13600 1 180 $X=52930 $Y=13350
X76 VSS! 88 71 85 89 VDD! 50 Z_cell4 $T=83200 13600 1 180 $X=76930 $Y=13350
X77 VDD! B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! 120 180 117 114 111 RIPPLE_4BIT $T=29200 21600 1 270 $X=4960 $Y=15800
X78 VSS! B<3> A<3> B<2> A<2> B<1> A<1> B<0> A<0> VDD! VSS! 30 121 25 21 14 RIPPLE_4BIT $T=29200 27200 1 270 $X=4960 $Y=21400
X79 VDD! B<7> A<7> B<6> A<6> B<5> A<5> B<4> A<4> VDD! VSS! 133 182 130 127 124 RIPPLE_4BIT $T=53200 21600 1 270 $X=28960 $Y=15800
X80 VSS! B<7> A<7> B<6> A<6> B<5> A<5> B<4> A<4> VDD! VSS! 55 134 48 43 36 RIPPLE_4BIT $T=53200 27200 1 270 $X=28960 $Y=21400
X81 VDD! B<11> A<11> B<10> A<10> B<9> A<9> B<8> A<8> VDD! VSS! 146 184 143 140 137 RIPPLE_4BIT $T=77200 21600 1 270 $X=52960 $Y=15800
X82 VSS! B<11> A<11> B<10> A<10> B<9> A<9> B<8> A<8> VDD! VSS! 81 147 76 67 60 RIPPLE_4BIT $T=77200 27200 1 270 $X=52960 $Y=21400
X83 SEL<1> VSS! VDD! 8 BUFFER_SEL $T=5200 4800 1 270 $X=-240 $Y=-100
X84 SEL<0> VSS! VDD! 9 BUFFER_SEL $T=5200 9600 1 270 $X=-240 $Y=4700
.ENDS
***************************************
