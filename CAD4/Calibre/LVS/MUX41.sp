* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT nfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw G S D B PI sx
.ENDS
***************************************
.SUBCKT nfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT dgnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT hvtnfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lvtnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT nfet33tw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT lpnfettw_rf G S D B PI sx
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT MUX41 SEL1 SEL0 OUT VSS! IN3 IN2 VDD! IN0 IN1
** N=128 EP=9 IP=0 FDC=28
M0 VSS! SEL1 4 VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=1090 $D=97
M1 6 SEL0 IN3 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2080 $D=97
M2 IN2 1 6 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=2880 $D=97
M3 1 SEL0 VSS! VSS! nfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=450 $Y=3880 $D=97
M4 OUT 2 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-1350 $D=97
M5 VSS! 2 OUT VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-870 $D=97
M6 OUT 2 VSS! VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=-390 $D=97
M7 VSS! 2 OUT VSS! nfet L=1.2e-07 W=3.5e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=610 $Y=90 $D=97
M8 VSS! 8 2 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5110 $Y=-650 $D=97
M9 8 SEL1 6 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=940 $D=97
M10 9 4 8 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=2090 $D=97
M11 9 1 IN0 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=3340 $D=97
M12 IN1 SEL0 9 VSS! nfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=5270 $Y=4140 $D=97
M13 OUT 2 VDD! VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-1350 $D=189
M14 VDD! 2 OUT VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-870 $D=189
M15 OUT 2 VDD! VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=-390 $D=189
M16 VDD! 2 OUT VDD! pfet L=1.2e-07 W=6.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=1820 $Y=90 $D=189
M17 4 SEL1 VDD! VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=610 $D=189
M18 VDD! SEL1 4 VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=1090 $D=189
M19 1 SEL0 VDD! VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=3880 $D=189
M20 VDD! SEL0 1 VDD! pfet L=1.2e-07 W=4.7e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2020 $Y=4360 $D=189
M21 6 1 IN3 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=2080 $D=189
M22 IN2 SEL0 6 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=2210 $Y=2880 $D=189
M23 VDD! 8 2 VDD! pfet L=1.2e-07 W=5.4e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=-650 $D=189
M24 8 4 6 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=940 $D=189
M25 9 SEL1 8 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=2140 $D=189
M26 9 SEL0 IN0 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=3340 $D=189
M27 IN1 1 9 VDD! pfet L=1.2e-07 W=2.8e-07 m=1 par=1 nf=1 ngcon=1 psp=0 $X=3510 $Y=4140 $D=189
.ENDS
***************************************
