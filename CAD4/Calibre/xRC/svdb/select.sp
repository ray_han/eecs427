* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT select SH0 SH<0> SH0_BAR VSS! VDD!
** N=76 EP=5 IP=0 FDC=11
M0 VSS! SH0 2 VSS! nfet L=1.2e-07 W=4e-07 AD=9.48966e-14 AS=1.28e-13 PD=8e-07 PS=1.44e-06 NRD=0.593103 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=0 panw9=3.12e-14 panw10=1.68e-14 $X=320 $Y=490 $D=25
M1 SH<0> 2 VSS! VSS! nfet L=1.2e-07 W=7.6e-07 AD=1.368e-13 AS=1.80303e-13 PD=1.12e-06 PS=1.52e-06 NRD=0.236842 NRS=0.31216 m=1 par=1 nf=1 ngcon=1 psp=0 sa=5.93684e-07 sb=1.8e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-15 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=840 $Y=490 $D=25
M2 VSS! 2 SH<0> VSS! nfet L=1.2e-07 W=7.6e-07 AD=1.72832e-13 AS=1.368e-13 PD=1.24223e-06 PS=1.12e-06 NRD=0.299225 NRS=0.236842 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.07368e-06 sb=1.32e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=2.4e-15 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=1320 $Y=490 $D=25
M3 SH0_BAR SH<0> VSS! VSS! nfet L=1.2e-07 W=1.21e-06 AD=2.178e-13 AS=2.75168e-13 PD=1.57e-06 PS=1.97777e-06 NRD=0.14876 NRS=0.187943 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.12e-06 sb=8e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=1840 $Y=490 $D=25
M4 VSS! SH<0> SH0_BAR VSS! nfet L=1.2e-07 W=1.21e-06 AD=3.872e-13 AS=2.178e-13 PD=3.06e-06 PS=1.57e-06 NRD=0.264463 NRS=0.14876 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.49421e-06 sb=3.2e-07 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=4.8e-14 panw10=1.68e-14 $X=2320 $Y=490 $D=25
M5 VDD! SH0 2 VDD! pfet L=1.2e-07 W=7.6e-07 AD=1.84461e-13 AS=2.432e-13 PD=1.28814e-06 PS=2.16e-06 NRD=0.319358 NRS=0.421053 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=1.152e-13 panw9=4.8e-14 panw10=3e-14 $X=320 $Y=3910 $D=108
M6 SH<0> 2 VDD! VDD! pfet L=1.2e-07 W=1.6e-06 AD=2.88e-13 AS=3.88339e-13 PD=1.96e-06 PS=2.71186e-06 NRD=0.1125 NRS=0.151695 m=1 par=1 nf=1 ngcon=1 psp=0 sa=5.67e-07 sb=1.8e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=2.4e-14 panw9=1.872e-13 panw10=2.4e-13 $X=840 $Y=3070 $D=108
M7 VDD! 2 SH<0> VDD! pfet L=1.2e-07 W=1.6e-06 AD=3.56061e-13 AS=2.88e-13 PD=2.22538e-06 PS=1.96e-06 NRD=0.139086 NRS=0.1125 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.047e-06 sb=1.32e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=3.6e-15 panw8=2.4e-14 panw9=9.12e-14 panw10=5.28e-13 $X=1320 $Y=3070 $D=108
M8 SH0_BAR SH<0> VDD! VDD! pfet L=1.2e-07 W=2.34e-06 AD=4.212e-13 AS=5.20739e-13 PD=2.7e-06 PS=3.25462e-06 NRD=0.0769231 NRS=0.0951017 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.17265e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.76e-14 panw8=4.8e-14 panw9=3.768e-13 panw10=1.44e-13 $X=1840 $Y=2330 $D=108
M9 VDD! SH<0> SH0_BAR VDD! pfet L=1.2e-07 W=2.34e-06 AD=7.488e-13 AS=4.212e-13 PD=5.32e-06 PS=2.7e-06 NRD=0.136752 NRS=0.0769231 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.54872e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=3.084e-13 panw8=4.8e-14 panw9=9.6e-14 panw10=1.44e-13 $X=2320 $Y=2330 $D=108
D10 VSS! VDD! diodenwx AREA=1.21396e-11 perim=1.394e-05 t3well=0 $X=-500 $Y=2030 $D=474
.ENDS
***************************************
