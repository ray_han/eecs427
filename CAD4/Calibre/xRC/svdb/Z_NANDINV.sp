* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT Z_NANDINV VSS! CIN P Z VDD!
** N=44 EP=5 IP=0 FDC=7
M0 VSS! 4 Z VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=8.96e-14 PD=1.2e-06 PS=1.2e-06 NRD=1.14286 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=1.8e-14 panw9=1.56e-14 panw10=0 $X=470 $Y=410 $D=25
M1 7 P 4 VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=8.96e-14 PD=6.4e-07 PS=1.2e-06 NRD=0.642857 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=1.8e-14 panw9=1.56e-14 panw10=0 $X=470 $Y=1360 $D=25
M2 VSS! CIN 7 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.04e-14 PD=1.2e-06 PS=6.4e-07 NRD=1.14286 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=1.8e-14 panw9=1.56e-14 panw10=0 $X=470 $Y=1840 $D=25
M3 VDD! 4 Z VDD! pfet L=1.2e-07 W=5.4e-07 AD=1.728e-13 AS=1.728e-13 PD=1.72e-06 PS=1.72e-06 NRD=0.592593 NRS=0.592593 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=9e-14 panw8=3e-14 panw9=3.96e-14 panw10=0 $X=1910 $Y=410 $D=108
M4 4 P VDD! VDD! pfet L=1.2e-07 W=5.4e-07 AD=9.72e-14 AS=1.728e-13 PD=9e-07 PS=1.72e-06 NRD=0.333333 NRS=0.592593 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=8e-07 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.52e-14 panw8=3e-14 panw9=1.044e-13 panw10=6.48e-14 $X=1910 $Y=1360 $D=108
M5 VDD! CIN 4 VDD! pfet L=1.2e-07 W=5.4e-07 AD=1.728e-13 AS=9.72e-14 PD=1.72e-06 PS=9e-07 NRD=0.592593 NRS=0.333333 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=3.2e-07 sd=0 panw1=0 panw2=4.8e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=9e-14 panw8=3e-14 panw9=3.96e-14 panw10=0 $X=1910 $Y=1840 $D=108
D6 VSS! VDD! diodenwx AREA=4.6412e-12 perim=8.94e-06 t3well=0 $X=1600 $Y=-210 $D=474
.ENDS
***************************************
