* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT F COUT Cn-1 VSS! F VDD!
** N=91 EP=5 IP=0 FDC=17
M0 9 7 F VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=8.96e-14 PD=6.4e-07 PS=1.2e-06 NRD=0.642857 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.76e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=1.56e-14 panw9=1.8e-14 panw10=0 $X=450 $Y=470 $D=25
M1 VSS! 1 9 VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=5.04e-14 PD=6.4e-07 PS=6.4e-07 NRD=0.642857 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.28e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=1.56e-14 panw9=1.8e-14 panw10=0 $X=450 $Y=950 $D=25
M2 10 COUT VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=5.04e-14 PD=6.4e-07 PS=6.4e-07 NRD=0.642857 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=1.56e-14 panw9=1.8e-14 panw10=0 $X=450 $Y=1430 $D=25
M3 7 3 10 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.04e-14 PD=1.2e-06 PS=6.4e-07 NRD=1.14286 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=1.56e-14 panw9=1.8e-14 panw10=0 $X=450 $Y=1910 $D=25
M4 11 3 1 VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=8.96e-14 PD=6.4e-07 PS=1.2e-06 NRD=0.642857 NRS=1.14286 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.76e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.28e-14 panw9=1.08e-14 panw10=0 $X=5270 $Y=470 $D=25
M5 VSS! Cn-1 11 VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=5.04e-14 PD=6.4e-07 PS=6.4e-07 NRD=0.642857 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.28e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.28e-14 panw9=1.08e-14 panw10=0 $X=5270 $Y=950 $D=25
M6 12 Cn-1 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=5.04e-14 AS=5.04e-14 PD=6.4e-07 PS=6.4e-07 NRD=0.642857 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.28e-14 panw9=1.08e-14 panw10=0 $X=5270 $Y=1430 $D=25
M7 3 COUT 12 VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.04e-14 PD=1.2e-06 PS=6.4e-07 NRD=1.14286 NRS=0.642857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.28e-14 panw9=1.08e-14 panw10=0 $X=5270 $Y=1910 $D=25
M8 F 7 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 AD=9.72e-14 AS=1.728e-13 PD=9e-07 PS=1.72e-06 NRD=0.333333 NRS=0.592593 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.76e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=8.88e-14 panw8=4.8e-15 panw9=0 panw10=0 $X=1900 $Y=470 $D=108
M9 VDD! 1 F VDD! pfet L=1.2e-07 W=5.4e-07 AD=9.72e-14 AS=9.72e-14 PD=9e-07 PS=9e-07 NRD=0.333333 NRS=0.333333 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.28e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=4.8e-15 panw9=6.48e-14 panw10=6.48e-14 $X=1900 $Y=950 $D=108
M10 7 COUT VDD! VDD! pfet L=1.2e-07 W=5.4e-07 AD=9.72e-14 AS=9.72e-14 PD=9e-07 PS=9e-07 NRD=0.333333 NRS=0.333333 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=8e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=4.8e-15 panw9=6.48e-14 panw10=6.48e-14 $X=1900 $Y=1430 $D=108
M11 VDD! 3 7 VDD! pfet L=1.2e-07 W=5.4e-07 AD=1.728e-13 AS=9.72e-14 PD=1.72e-06 PS=9e-07 NRD=0.592593 NRS=0.333333 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=8.88e-14 panw8=4.8e-15 panw9=0 panw10=0 $X=1900 $Y=1910 $D=108
M12 1 3 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 AD=9.72e-14 AS=1.728e-13 PD=9e-07 PS=1.72e-06 NRD=0.333333 NRS=0.592593 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.76e-06 sd=0 panw1=0 panw2=0 panw3=4.8e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=8.88e-14 panw8=1.2e-14 panw9=0 panw10=4.8e-15 $X=3560 $Y=470 $D=108
M13 VDD! Cn-1 1 VDD! pfet L=1.2e-07 W=5.4e-07 AD=9.72e-14 AS=9.72e-14 PD=9e-07 PS=9e-07 NRD=0.333333 NRS=0.333333 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.28e-06 sd=0 panw1=0 panw2=0 panw3=4.8e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-14 panw9=6.48e-14 panw10=6.96e-14 $X=3560 $Y=950 $D=108
M14 3 Cn-1 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 AD=9.72e-14 AS=9.72e-14 PD=9e-07 PS=9e-07 NRD=0.333333 NRS=0.333333 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.28e-06 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=4.8e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.2e-14 panw9=6.48e-14 panw10=6.96e-14 $X=3560 $Y=1430 $D=108
M15 VDD! COUT 3 VDD! pfet L=1.2e-07 W=5.4e-07 AD=1.728e-13 AS=9.72e-14 PD=1.72e-06 PS=9e-07 NRD=0.592593 NRS=0.333333 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.76e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=4.8e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=8.88e-14 panw8=1.2e-14 panw9=0 panw10=4.8e-15 $X=3560 $Y=1910 $D=108
D16 VSS! VDD! diodenwx AREA=8.008e-12 perim=1.132e-05 t3well=0 $X=1600 $Y=-150 $D=474
.ENDS
***************************************
