* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT BUFFER_CIN VDD! OUT IN VSS!
** N=33 EP=4 IP=0 FDC=5
M0 4 IN VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=9.24e-14 AS=9.24e-14 PD=1.22e-06 PS=1.22e-06 NRD=1.17857 NRS=1.17857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.3e-07 sb=3.3e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=0 $X=2740 $Y=450 $D=25
M1 OUT 4 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=9.24e-14 AS=9.24e-14 PD=1.22e-06 PS=1.22e-06 NRD=1.17857 NRS=1.17857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.3e-07 sb=3.3e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=9.6e-15 panw7=0 panw8=0 panw9=0 panw10=0 $X=2740 $Y=1410 $D=25
M2 4 IN VDD! VDD! pfet L=1.2e-07 W=5.4e-07 AD=1.782e-13 AS=1.782e-13 PD=1.74e-06 PS=1.74e-06 NRD=0.611111 NRS=0.611111 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.3e-07 sb=3.3e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=8.88e-14 panw8=4.8e-15 panw9=0 panw10=6.48e-14 $X=1600 $Y=450 $D=108
M3 OUT 4 VDD! VDD! pfet L=1.2e-07 W=5.4e-07 AD=1.782e-13 AS=1.782e-13 PD=1.74e-06 PS=1.74e-06 NRD=0.611111 NRS=0.611111 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.3e-07 sb=3.3e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=8.88e-14 panw8=4.8e-15 panw9=0 panw10=6.48e-14 $X=1600 $Y=1410 $D=108
D4 VSS! VDD! diodenwx AREA=6.6456e-12 perim=1.036e-05 t3well=0 $X=-400 $Y=-180 $D=474
.ENDS
***************************************
