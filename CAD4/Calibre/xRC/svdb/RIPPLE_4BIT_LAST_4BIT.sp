* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT RIPPLE_BIT B CIN A SUM VSS! VDD! COUT 40 41 42 43 44 45 46 47
** N=197 EP=15 IP=0 FDC=14
*.SEEDPROM
M0 VDD! B 45 VDD! pfet L=1.2e-07 W=9.3e-07 AD=1.77898e-13 AS=2.976e-13 PD=1.47202e-06 PS=2.5e-06 NRD=0.205686 NRS=0.344086 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.00065e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.356e-13 panw8=2.4e-14 panw9=3.12e-14 panw10=0 $X=470 $Y=3510 $D=108
M1 42 40 VDD! VDD! pfet L=1.2e-07 W=9.5e-07 AD=3.04e-13 AS=3.04e-13 PD=2.54e-06 PS=2.54e-06 NRD=0.336842 NRS=0.336842 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.38e-13 panw9=3e-14 panw10=0 $X=610 $Y=1540 $D=108
M2 46 A VDD! VDD! pfet L=1.2e-07 W=7e-07 AD=1.78e-13 AS=1.33902e-13 PD=1.71429e-06 PS=1.10798e-06 NRD=0.363265 NRS=0.273269 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=4.8e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=1.152e-13 panw10=0 $X=950 $Y=3510 $D=108
M3 40 45 46 VDD! pfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=7.12e-14 PD=8.4e-07 PS=6.85714e-07 NRD=1 NRS=0.908163 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.42e-06 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.4e-15 panw9=3.12e-14 panw10=3.36e-14 $X=1570 $Y=3510 $D=108
M4 41 40 CIN VDD! pfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.176e-13 PD=8.4e-07 PS=1.4e-06 NRD=1 NRS=1.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=3.6e-15 panw9=3e-14 panw10=3.36e-14 $X=1650 $Y=2210 $D=108
M5 A B 40 VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.4e-15 panw9=3.12e-14 panw10=0 $X=2250 $Y=3510 $D=108
M6 47 42 41 VDD! pfet L=1.2e-07 W=2.8e-07 AD=7.99719e-14 AS=7.84e-14 PD=6.6807e-07 PS=8.4e-07 NRD=1.02005 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1e-06 sb=1.42e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=3.6e-15 panw9=3e-14 panw10=0 $X=2330 $Y=2210 $D=108
M7 VDD! CIN 47 VDD! pfet L=1.2e-07 W=8.6e-07 AD=1.548e-13 AS=2.45628e-13 PD=1.22e-06 PS=2.05193e-06 NRD=0.209302 NRS=0.332109 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7.75814e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3e-14 panw10=0 $X=2950 $Y=1630 $D=108
M8 43 40 B VDD! pfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.176e-13 PD=8.4e-07 PS=1.4e-06 NRD=1 NRS=1.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4.2e-07 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=8.4e-15 panw9=2.52e-14 panw10=0 $X=3330 $Y=3510 $D=108
M9 SUM 41 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 AD=2.752e-13 AS=1.548e-13 PD=2.36e-06 PS=1.22e-06 NRD=0.372093 NRS=0.209302 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1907e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3e-14 panw10=0 $X=3430 $Y=1630 $D=108
M10 CIN 42 43 VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=8.4e-15 panw9=2.52e-14 panw10=3.36e-14 $X=4010 $Y=3510 $D=108
M11 COUT 44 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 AD=2.752e-13 AS=2.752e-13 PD=2.36e-06 PS=2.36e-06 NRD=0.372093 NRS=0.372093 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=1.272e-13 panw9=3e-14 panw10=0 $X=4850 $Y=1630 $D=108
M12 44 43 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 AD=2.752e-13 AS=2.752e-13 PD=2.36e-06 PS=2.36e-06 NRD=0.372093 NRS=0.372093 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=1.186e-13 panw8=3.26e-14 panw9=2.52e-14 panw10=0 $X=4990 $Y=3510 $D=108
D13 VSS! VDD! diodenwx AREA=2.102e-11 perim=1.906e-05 t3well=0 $X=-200 $Y=1240 $D=474
.ENDS
***************************************
.SUBCKT RIPPLE_4BIT_LAST_4BIT B<3> C3 CIN A<3> B<2> A<2> B<1> A<1> B<0> A<0> VSS! SUM<3> VDD! COUT SUM<2> SUM<1> SUM<0>
** N=275 EP=17 IP=141 FDC=106
M0 4 1 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 AD=1.568e-13 AS=1.568e-13 PD=1.62e-06 PS=1.62e-06 NRD=0.653061 NRS=0.653061 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=0 $X=880 $Y=450 $D=25
M1 VSS! B<3> 11 VSS! nfet L=1.2e-07 W=4.8e-07 AD=8.77714e-14 AS=1.536e-13 PD=9.6e-07 PS=1.6e-06 NRD=0.380952 NRS=0.666667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.34e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=3.72e-14 $X=880 $Y=5070 $D=25
M2 19 A<3> VSS! VSS! nfet L=1.2e-07 W=3.6e-07 AD=9.045e-14 AS=6.58286e-14 PD=9.675e-07 PS=7.2e-07 NRD=0.697917 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.34889e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=3.6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=3.72e-14 $X=1360 $Y=5190 $D=25
M3 3 4 C3 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.176e-13 PD=8.4e-07 PS=1.4e-06 NRD=1 NRS=1.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=0 $X=1920 $Y=450 $D=25
M4 1 B<3> 19 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=7.035e-14 PD=8.4e-07 PS=7.525e-07 NRD=1 NRS=0.897321 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.42e-06 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=3.36e-14 $X=1980 $Y=5270 $D=25
M5 20 1 3 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.06222e-14 AS=7.84e-14 PD=7.31111e-07 PS=8.4e-07 NRD=0.900794 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1e-06 sb=1.42e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=0 $X=2600 $Y=450 $D=25
M6 A<3> 11 1 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=6e-15 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=3.36e-14 $X=2660 $Y=5270 $D=25
M7 VSS! C3 20 VSS! nfet L=1.2e-07 W=4.4e-07 AD=8.70222e-14 AS=1.10978e-13 PD=9.77778e-07 PS=1.14889e-06 NRD=0.449495 NRS=0.573232 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.18909e-06 sb=6.03636e-07 sd=0 panw1=0 panw2=0 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=0 $X=3220 $Y=450 $D=25
M8 SUM<3> 3 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.53778e-14 PD=1.2e-06 PS=6.22222e-07 NRD=1.14286 NRS=0.706349 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=0 $X=3700 $Y=450 $D=25
M9 8 4 B<3> VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.12e-13 PD=8.4e-07 PS=1.36e-06 NRD=1 NRS=1.42857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4e-07 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=6e-15 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=3.36e-14 $X=3740 $Y=5270 $D=25
M10 C3 1 8 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.08e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=6e-15 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=3.36e-14 $X=4420 $Y=5270 $D=25
M11 COUT 8 VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=1.28e-13 PD=1.44e-06 PS=1.44e-06 NRD=0.8 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=0 $X=4930 $Y=450 $D=25
M12 VSS! B<2> 257 VSS! nfet L=1.2e-07 W=4.8e-07 AD=8.77714e-14 AS=1.536e-13 PD=9.6e-07 PS=1.6e-06 NRD=0.380952 NRS=0.666667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.34e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=3.72e-14 $X=470 $Y=11070 $D=25
M13 254 252 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 AD=1.568e-13 AS=1.568e-13 PD=1.62e-06 PS=1.62e-06 NRD=0.653061 NRS=0.653061 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.84e-14 $X=610 $Y=6450 $D=25
M14 258 A<2> VSS! VSS! nfet L=1.2e-07 W=3.6e-07 AD=9.045e-14 AS=6.58286e-14 PD=9.675e-07 PS=7.2e-07 NRD=0.697917 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.34889e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=3.6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=3.72e-14 $X=950 $Y=11190 $D=25
M15 252 B<2> 258 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=7.035e-14 PD=8.4e-07 PS=7.525e-07 NRD=1 NRS=0.897321 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.42e-06 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=3.36e-14 $X=1570 $Y=11270 $D=25
M16 253 254 6 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.176e-13 PD=8.4e-07 PS=1.4e-06 NRD=1 NRS=1.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.36e-14 $X=1650 $Y=6450 $D=25
M17 A<2> 257 252 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=3.36e-14 $X=2250 $Y=11270 $D=25
M18 259 252 253 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.06222e-14 AS=7.84e-14 PD=7.31111e-07 PS=8.4e-07 NRD=0.900794 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1e-06 sb=1.42e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.36e-14 $X=2330 $Y=6450 $D=25
M19 VSS! 6 259 VSS! nfet L=1.2e-07 W=4.4e-07 AD=8.70222e-14 AS=1.10978e-13 PD=9.77778e-07 PS=1.14889e-06 NRD=0.449495 NRS=0.573232 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.18909e-06 sb=6.03636e-07 sd=0 panw1=0 panw2=0 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.24e-14 $X=2950 $Y=6450 $D=25
M20 255 254 B<2> VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.12e-13 PD=8.4e-07 PS=1.36e-06 NRD=1 NRS=1.42857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4e-07 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=6e-15 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=3.36e-14 $X=3330 $Y=11270 $D=25
M21 SUM<2> 253 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.53778e-14 PD=1.2e-06 PS=6.22222e-07 NRD=1.14286 NRS=0.706349 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.24e-14 $X=3430 $Y=6450 $D=25
M22 6 252 255 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.08e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=6e-15 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=3.36e-14 $X=4010 $Y=11270 $D=25
M23 C3 256 VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=1.28e-13 PD=1.44e-06 PS=1.44e-06 NRD=0.8 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.24e-14 $X=4850 $Y=6450 $D=25
M24 256 255 VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=1.28e-13 PD=1.44e-06 PS=1.44e-06 NRD=0.8 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=2.4e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=3.72e-14 $X=4990 $Y=11150 $D=25
M25 VSS! B<1> 265 VSS! nfet L=1.2e-07 W=4.8e-07 AD=8.77714e-14 AS=1.536e-13 PD=9.6e-07 PS=1.6e-06 NRD=0.380952 NRS=0.666667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.34e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=3.72e-14 $X=470 $Y=17070 $D=25
M26 262 260 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 AD=1.568e-13 AS=1.568e-13 PD=1.62e-06 PS=1.62e-06 NRD=0.653061 NRS=0.653061 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.84e-14 $X=610 $Y=12450 $D=25
M27 266 A<1> VSS! VSS! nfet L=1.2e-07 W=3.6e-07 AD=9.045e-14 AS=6.58286e-14 PD=9.675e-07 PS=7.2e-07 NRD=0.697917 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.34889e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=3.6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=3.72e-14 $X=950 $Y=17190 $D=25
M28 260 B<1> 266 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=7.035e-14 PD=8.4e-07 PS=7.525e-07 NRD=1 NRS=0.897321 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.42e-06 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=3.36e-14 $X=1570 $Y=17270 $D=25
M29 261 262 7 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.176e-13 PD=8.4e-07 PS=1.4e-06 NRD=1 NRS=1.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.36e-14 $X=1650 $Y=12450 $D=25
M30 A<1> 265 260 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=3.36e-14 $X=2250 $Y=17270 $D=25
M31 267 260 261 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.06222e-14 AS=7.84e-14 PD=7.31111e-07 PS=8.4e-07 NRD=0.900794 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1e-06 sb=1.42e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.36e-14 $X=2330 $Y=12450 $D=25
M32 VSS! 7 267 VSS! nfet L=1.2e-07 W=4.4e-07 AD=8.70222e-14 AS=1.10978e-13 PD=9.77778e-07 PS=1.14889e-06 NRD=0.449495 NRS=0.573232 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.18909e-06 sb=6.03636e-07 sd=0 panw1=0 panw2=0 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.24e-14 $X=2950 $Y=12450 $D=25
M33 263 262 B<1> VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.12e-13 PD=8.4e-07 PS=1.36e-06 NRD=1 NRS=1.42857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4e-07 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=6e-15 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=3.36e-14 $X=3330 $Y=17270 $D=25
M34 SUM<1> 261 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.53778e-14 PD=1.2e-06 PS=6.22222e-07 NRD=1.14286 NRS=0.706349 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.24e-14 $X=3430 $Y=12450 $D=25
M35 7 260 263 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.08e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=6e-15 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=3.36e-14 $X=4010 $Y=17270 $D=25
M36 6 264 VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=1.28e-13 PD=1.44e-06 PS=1.44e-06 NRD=0.8 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.24e-14 $X=4850 $Y=12450 $D=25
M37 264 263 VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=1.28e-13 PD=1.44e-06 PS=1.44e-06 NRD=0.8 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=2.4e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=3.72e-14 $X=4990 $Y=17150 $D=25
M38 VSS! B<0> 273 VSS! nfet L=1.2e-07 W=4.8e-07 AD=8.77714e-14 AS=1.536e-13 PD=9.6e-07 PS=1.6e-06 NRD=0.380952 NRS=0.666667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.34e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=0 $X=470 $Y=23070 $D=25
M39 270 268 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 AD=1.568e-13 AS=1.568e-13 PD=1.62e-06 PS=1.62e-06 NRD=0.653061 NRS=0.653061 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.84e-14 $X=610 $Y=18450 $D=25
M40 274 A<0> VSS! VSS! nfet L=1.2e-07 W=3.6e-07 AD=9.045e-14 AS=6.58286e-14 PD=9.675e-07 PS=7.2e-07 NRD=0.697917 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.34889e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=3.6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=0 $X=950 $Y=23190 $D=25
M41 268 B<0> 274 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=7.035e-14 PD=8.4e-07 PS=7.525e-07 NRD=1 NRS=0.897321 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.42e-06 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=0 $X=1570 $Y=23270 $D=25
M42 269 270 CIN VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.176e-13 PD=8.4e-07 PS=1.4e-06 NRD=1 NRS=1.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.36e-14 $X=1650 $Y=18450 $D=25
M43 A<0> 273 268 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=0 $X=2250 $Y=23270 $D=25
M44 275 268 269 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.06222e-14 AS=7.84e-14 PD=7.31111e-07 PS=8.4e-07 NRD=0.900794 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1e-06 sb=1.42e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.36e-14 $X=2330 $Y=18450 $D=25
M45 VSS! CIN 275 VSS! nfet L=1.2e-07 W=4.4e-07 AD=8.70222e-14 AS=1.10978e-13 PD=9.77778e-07 PS=1.14889e-06 NRD=0.449495 NRS=0.573232 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.18909e-06 sb=6.03636e-07 sd=0 panw1=0 panw2=0 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.24e-14 $X=2950 $Y=18450 $D=25
M46 271 270 B<0> VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.12e-13 PD=8.4e-07 PS=1.36e-06 NRD=1 NRS=1.42857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4e-07 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=6e-15 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=0 $X=3330 $Y=23270 $D=25
M47 SUM<0> 269 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.53778e-14 PD=1.2e-06 PS=6.22222e-07 NRD=1.14286 NRS=0.706349 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.24e-14 $X=3430 $Y=18450 $D=25
M48 CIN 268 271 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.08e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=6e-15 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=0 $X=4010 $Y=23270 $D=25
M49 7 272 VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=1.28e-13 PD=1.44e-06 PS=1.44e-06 NRD=0.8 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=3.24e-14 $X=4850 $Y=18450 $D=25
M50 272 271 VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=1.28e-13 PD=1.44e-06 PS=1.44e-06 NRD=0.8 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=2.4e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=0 $X=4990 $Y=23150 $D=25
M51 4 1 VDD! VDD! pfet L=1.2e-07 W=9.5e-07 AD=3.04e-13 AS=3.04e-13 PD=2.54e-06 PS=2.54e-06 NRD=0.336842 NRS=0.336842 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.44e-13 panw10=0 $X=880 $Y=1540 $D=108
M52 VDD! B<3> 11 VDD! pfet L=1.2e-07 W=9.3e-07 AD=1.77898e-13 AS=2.976e-13 PD=1.47202e-06 PS=2.5e-06 NRD=0.205686 NRS=0.344086 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.00065e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.428e-13 panw10=0 $X=880 $Y=3510 $D=108
M53 19 A<3> VDD! VDD! pfet L=1.2e-07 W=7e-07 AD=1.78e-13 AS=1.33902e-13 PD=1.71429e-06 PS=1.10798e-06 NRD=0.363265 NRS=0.273269 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=4.8e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.12e-14 panw10=8.4e-14 $X=1360 $Y=3510 $D=108
M54 3 1 C3 VDD! pfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.176e-13 PD=8.4e-07 PS=1.4e-06 NRD=1 NRS=1.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=3.6e-15 panw9=3e-14 panw10=0 $X=1920 $Y=2210 $D=108
M55 1 11 19 VDD! pfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=7.12e-14 PD=8.4e-07 PS=6.85714e-07 NRD=1 NRS=0.908163 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.42e-06 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.4e-15 panw9=3.12e-14 panw10=0 $X=1980 $Y=3510 $D=108
M56 20 4 3 VDD! pfet L=1.2e-07 W=2.8e-07 AD=7.99719e-14 AS=7.84e-14 PD=6.6807e-07 PS=8.4e-07 NRD=1.02005 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1e-06 sb=1.42e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=3.6e-15 panw9=3e-14 panw10=0 $X=2600 $Y=2210 $D=108
M57 A<3> B<3> 1 VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=8.4e-15 panw9=2.52e-14 panw10=0 $X=2660 $Y=3510 $D=108
M58 VDD! C3 20 VDD! pfet L=1.2e-07 W=8.6e-07 AD=1.548e-13 AS=2.45628e-13 PD=1.22e-06 PS=2.05193e-06 NRD=0.209302 NRS=0.332109 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7.75814e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3e-14 panw10=0 $X=3220 $Y=1630 $D=108
M59 SUM<3> 3 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 AD=2.752e-13 AS=1.548e-13 PD=2.36e-06 PS=1.22e-06 NRD=0.372093 NRS=0.209302 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1907e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3e-14 panw10=1.72e-14 $X=3700 $Y=1630 $D=108
M60 8 1 B<3> VDD! pfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.176e-13 PD=8.4e-07 PS=1.4e-06 NRD=1 NRS=1.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4.2e-07 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=8.4e-15 panw9=2.52e-14 panw10=1.68e-14 $X=3740 $Y=3510 $D=108
M61 C3 4 8 VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=8.4e-15 panw9=5.88e-14 panw10=0 $X=4420 $Y=3510 $D=108
M62 COUT 8 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 AD=2.752e-13 AS=2.752e-13 PD=2.36e-06 PS=2.36e-06 NRD=0.372093 NRS=0.372093 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=6.7e-14 panw8=8.42e-14 panw9=3e-14 panw10=0 $X=4930 $Y=1630 $D=108
D63 VSS! VDD! diodenwx AREA=2.102e-11 perim=1.906e-05 t3well=0 $X=-200 $Y=1240 $D=474
X64 B<2> 6 A<2> SUM<2> VSS! VDD! C3 252 253 254 255 256 257 258 259 RIPPLE_BIT $T=0 6000 0 0 $X=-200 $Y=5760
X65 B<1> 7 A<1> SUM<1> VSS! VDD! 6 260 261 262 263 264 265 266 267 RIPPLE_BIT $T=0 12000 0 0 $X=-200 $Y=11760
X66 B<0> CIN A<0> SUM<0> VSS! VDD! 7 268 269 270 271 272 273 274 275 RIPPLE_BIT $T=0 18000 0 0 $X=-200 $Y=17760
.ENDS
***************************************
