* SPICE NETLIST
***************************************

.SUBCKT esdscr A K PD SX
.ENDS
***************************************
.SUBCKT subc SUBCON SUB
.ENDS
***************************************
.SUBCKT sblkndres D S G
.ENDS
***************************************
.SUBCKT ncap G S B D
.ENDS
***************************************
.SUBCKT dgncap G S B D
.ENDS
***************************************
.SUBCKT diffhavar ANODE1 ANODE2 CATHODE BULK
.ENDS
***************************************
.SUBCKT diffncap GA GB NW SX
.ENDS
***************************************
.SUBCKT esdnsh_base d g s b
.ENDS
***************************************
.SUBCKT esdpsh_base d g s b
.ENDS
***************************************
.SUBCKT bondpad in gp sub
.ENDS
***************************************
.SUBCKT devicepad pad
.ENDS
***************************************
.SUBCKT efuse IN OUT
.ENDS
***************************************
.SUBCKT indp out in bulk
.ENDS
***************************************
.SUBCKT ind out in bulk
.ENDS
***************************************
.SUBCKT inds out in bulk
.ENDS
***************************************
.SUBCKT symindp outpr outse ct BULK
.ENDS
***************************************
.SUBCKT symind outpr outse ct BULK
.ENDS
***************************************
.SUBCKT rfline in out bulk
.ENDS
***************************************
.SUBCKT singlewire VA VB VSHIELD
.ENDS
***************************************
.SUBCKT coupledwires VA1 VA2 VB1 VB2 VSHIELD
.ENDS
***************************************
.SUBCKT singlecpw va vb vshield
.ENDS
***************************************
.SUBCKT coupledcpw va1 va2 vb1 vb2 vshield
.ENDS
***************************************
.SUBCKT corrPoint cp
.ENDS
***************************************
.SUBCKT RIPPLE_BIT_BAR B CIN A VSS! SUM VDD! COUT
** N=183 EP=7 IP=0 FDC=25
M0 4 1 VSS! VSS! nfet L=1.2e-07 W=4.9e-07 AD=1.568e-13 AS=1.568e-13 PD=1.62e-06 PS=1.62e-06 NRD=0.653061 NRS=0.653061 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=0 $X=880 $Y=450 $D=25
M1 VSS! B 8 VSS! nfet L=1.2e-07 W=4.8e-07 AD=8.77714e-14 AS=1.536e-13 PD=9.6e-07 PS=1.6e-06 NRD=0.380952 NRS=0.666667 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.34e-06 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=0 $X=880 $Y=5070 $D=25
M2 10 A VSS! VSS! nfet L=1.2e-07 W=3.6e-07 AD=9.045e-14 AS=6.58286e-14 PD=9.675e-07 PS=7.2e-07 NRD=0.697917 NRS=0.507937 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=1.34889e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=3.6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=0 $X=1360 $Y=5190 $D=25
M3 3 4 CIN VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.176e-13 PD=8.4e-07 PS=1.4e-06 NRD=1 NRS=1.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=0 $X=1920 $Y=450 $D=25
M4 1 B 10 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=7.035e-14 PD=8.4e-07 PS=7.525e-07 NRD=1 NRS=0.897321 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.42e-06 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.2e-14 panw7=2.16e-14 panw8=0 panw9=0 panw10=0 $X=1980 $Y=5270 $D=25
M5 11 1 3 VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.06222e-14 AS=7.84e-14 PD=7.31111e-07 PS=8.4e-07 NRD=0.900794 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1e-06 sb=1.42e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=0 $X=2600 $Y=450 $D=25
M6 A 8 1 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=6e-15 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=0 $X=2660 $Y=5270 $D=25
M7 VSS! CIN 11 VSS! nfet L=1.2e-07 W=4.4e-07 AD=8.70222e-14 AS=1.10978e-13 PD=9.77778e-07 PS=1.14889e-06 NRD=0.449495 NRS=0.573232 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.18909e-06 sb=6.03636e-07 sd=0 panw1=0 panw2=0 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=0 $X=3220 $Y=450 $D=25
M8 SUM 3 VSS! VSS! nfet L=1.2e-07 W=2.8e-07 AD=8.96e-14 AS=5.53778e-14 PD=1.2e-06 PS=6.22222e-07 NRD=1.14286 NRS=0.706349 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=1.08e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=0 $X=3700 $Y=450 $D=25
M9 6 4 B VSS! nfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.12e-13 PD=8.4e-07 PS=1.36e-06 NRD=1 NRS=1.42857 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4e-07 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=6e-15 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=0 $X=3740 $Y=5270 $D=25
M10 CIN 1 6 VSS! nfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.08e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=6e-15 panw7=2.4e-14 panw8=3.6e-15 panw9=0 panw10=0 $X=4420 $Y=5270 $D=25
M11 COUT 6 VSS! VSS! nfet L=1.2e-07 W=4e-07 AD=1.28e-13 AS=1.28e-13 PD=1.44e-06 PS=1.44e-06 NRD=0.8 NRS=0.8 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.28e-14 panw8=0 panw9=0 panw10=0 $X=4930 $Y=450 $D=25
M12 4 1 VDD! VDD! pfet L=1.2e-07 W=9.5e-07 AD=3.04e-13 AS=3.04e-13 PD=2.54e-06 PS=2.54e-06 NRD=0.336842 NRS=0.336842 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=6e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.44e-13 panw10=0 $X=880 $Y=1540 $D=108
M13 VDD! B 8 VDD! pfet L=1.2e-07 W=9.3e-07 AD=1.77898e-13 AS=2.976e-13 PD=1.47202e-06 PS=2.5e-06 NRD=0.205686 NRS=0.344086 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=1.00065e-06 sd=0 panw1=0 panw2=2.4e-15 panw3=6e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=1.428e-13 panw10=0 $X=880 $Y=3510 $D=108
M14 10 A VDD! VDD! pfet L=1.2e-07 W=7e-07 AD=1.78e-13 AS=1.33902e-13 PD=1.71429e-06 PS=1.10798e-06 NRD=0.363265 NRS=0.273269 m=1 par=1 nf=1 ngcon=1 psp=0 sa=8e-07 sb=8.2e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=4.8e-15 panw7=2.4e-14 panw8=2.4e-14 panw9=3.12e-14 panw10=8.4e-14 $X=1360 $Y=3510 $D=108
M15 3 1 CIN VDD! pfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.176e-13 PD=8.4e-07 PS=1.4e-06 NRD=1 NRS=1.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4.2e-07 sb=2e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=3.6e-15 panw9=3e-14 panw10=0 $X=1920 $Y=2210 $D=108
M16 1 8 10 VDD! pfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=7.12e-14 PD=8.4e-07 PS=6.85714e-07 NRD=1 NRS=0.908163 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.42e-06 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=2.4e-15 panw9=3.12e-14 panw10=0 $X=1980 $Y=3510 $D=108
M17 11 4 3 VDD! pfet L=1.2e-07 W=2.8e-07 AD=7.99719e-14 AS=7.84e-14 PD=6.6807e-07 PS=8.4e-07 NRD=1.02005 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1e-06 sb=1.42e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=3.6e-15 panw9=3e-14 panw10=0 $X=2600 $Y=2210 $D=108
M18 A B 1 VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=2e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=8.4e-15 panw9=2.52e-14 panw10=0 $X=2660 $Y=3510 $D=108
M19 VDD! CIN 11 VDD! pfet L=1.2e-07 W=8.6e-07 AD=1.548e-13 AS=2.45628e-13 PD=1.22e-06 PS=2.05193e-06 NRD=0.209302 NRS=0.332109 m=1 par=1 nf=1 ngcon=1 psp=0 sa=7.75814e-07 sb=8e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3e-14 panw10=0 $X=3220 $Y=1630 $D=108
M20 SUM 3 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 AD=2.752e-13 AS=1.548e-13 PD=2.36e-06 PS=1.22e-06 NRD=0.372093 NRS=0.209302 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1907e-06 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=2.4e-14 panw8=2.4e-14 panw9=3e-14 panw10=1.72e-14 $X=3700 $Y=1630 $D=108
M21 6 1 B VDD! pfet L=1.2e-07 W=2.8e-07 AD=7.84e-14 AS=1.176e-13 PD=8.4e-07 PS=1.4e-06 NRD=1 NRS=1.5 m=1 par=1 nf=1 ngcon=1 psp=0 sa=4.2e-07 sb=1.04e-06 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=8.4e-15 panw9=2.52e-14 panw10=1.68e-14 $X=3740 $Y=3510 $D=108
M22 CIN 4 6 VDD! pfet L=1.2e-07 W=2.8e-07 AD=1.008e-13 AS=7.84e-14 PD=1.28e-06 PS=8.4e-07 NRD=1.28571 NRS=1 m=1 par=1 nf=1 ngcon=1 psp=0 sa=1.1e-06 sb=3.6e-07 sd=0 panw1=0 panw2=0 panw3=0 panw4=0 panw5=0 panw6=0 panw7=0 panw8=8.4e-15 panw9=5.88e-14 panw10=0 $X=4420 $Y=3510 $D=108
M23 COUT 6 VDD! VDD! pfet L=1.2e-07 W=8.6e-07 AD=2.752e-13 AS=2.752e-13 PD=2.36e-06 PS=2.36e-06 NRD=0.372093 NRS=0.372093 m=1 par=1 nf=1 ngcon=1 psp=0 sa=3.2e-07 sb=3.2e-07 sd=0 panw1=0 panw2=0 panw3=1.2e-15 panw4=6e-15 panw5=6e-15 panw6=1.2e-14 panw7=6.7e-14 panw8=8.42e-14 panw9=3e-14 panw10=0 $X=4930 $Y=1630 $D=108
D24 VSS! VDD! diodenwx AREA=2.102e-11 perim=1.906e-05 t3well=0 $X=-200 $Y=1240 $D=474
.ENDS
***************************************
