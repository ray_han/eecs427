/*
###############################################################
#  Generated by:      Cadence Encounter 14.24-s039_1
#  OS:                Linux x86_64(Host ID caen-vnc-oncampus01.engin.umich.edu)
#  Generated on:      Wed Nov  4 14:20:02 2015
#  Design:            mult
#  Command:           saveNetlist -excludeLeafCell ../verilog/mult.apr.v
###############################################################
*/
/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : K-2015.06
// Date      : Tue Oct 20 14:46:01 2015
/////////////////////////////////////////////////////////////
module mult_DW_mult_uns_0 (
	a, 
	b, 
	product);
   input [7:0] a;
   input [7:0] b;
   output [15:0] product;

   // Internal wires
   wire n2;
   wire n3;
   wire n4;
   wire n5;
   wire n6;
   wire n7;
   wire n8;
   wire n9;
   wire n10;
   wire n11;
   wire n12;
   wire n13;
   wire n14;
   wire n15;
   wire n16;
   wire n17;
   wire n18;
   wire n19;
   wire n20;
   wire n21;
   wire n22;
   wire n23;
   wire n24;
   wire n25;
   wire n26;
   wire n27;
   wire n28;
   wire n29;
   wire n30;
   wire n31;
   wire n32;
   wire n33;
   wire n34;
   wire n35;
   wire n36;
   wire n37;
   wire n38;
   wire n39;
   wire n40;
   wire n41;
   wire n42;
   wire n43;
   wire n44;
   wire n45;
   wire n46;
   wire n47;
   wire n48;
   wire n49;
   wire n50;
   wire n51;
   wire n52;
   wire n53;
   wire n54;
   wire n55;
   wire n56;
   wire n57;
   wire n58;
   wire n59;
   wire n60;
   wire n61;
   wire n62;
   wire n63;
   wire n64;
   wire n65;
   wire n66;
   wire n67;
   wire n68;
   wire n69;
   wire n70;
   wire n71;
   wire n72;
   wire n73;
   wire n74;
   wire n75;
   wire n76;
   wire n77;
   wire n78;
   wire n79;
   wire n80;
   wire n81;
   wire n82;
   wire n83;
   wire n84;
   wire n85;
   wire n86;
   wire n87;
   wire n88;
   wire n89;
   wire n90;
   wire n91;
   wire n92;
   wire n93;
   wire n94;
   wire n95;
   wire n96;
   wire n97;
   wire n98;
   wire n99;
   wire n100;
   wire n101;
   wire n102;
   wire n103;
   wire n104;
   wire n105;
   wire n106;
   wire n107;
   wire n108;
   wire n109;
   wire n110;
   wire n111;
   wire n112;
   wire n113;
   wire n114;
   wire n115;
   wire n116;
   wire n117;
   wire n118;
   wire n119;
   wire n120;
   wire n121;
   wire n122;
   wire n123;
   wire n124;
   wire n125;
   wire n126;
   wire n127;
   wire n128;
   wire n129;
   wire n130;
   wire n131;
   wire n132;
   wire n133;
   wire n134;
   wire n135;
   wire n136;
   wire n137;
   wire n138;
   wire n139;
   wire n140;
   wire n141;
   wire n142;
   wire n143;
   wire n144;
   wire n145;
   wire n146;
   wire n147;

   CMPR32X2TR U3 (.S(product[13]),
	.CO(n2),
	.A(n18),
	.B(n16),
	.C(n3));
   CMPR32X2TR U4 (.S(product[12]),
	.CO(n3),
	.A(n21),
	.B(n19),
	.C(n4));
   CMPR32X2TR U5 (.S(product[11]),
	.CO(n4),
	.A(n22),
	.B(n26),
	.C(n5));
   CMPR32X2TR U6 (.S(product[10]),
	.CO(n5),
	.A(n27),
	.B(n32),
	.C(n6));
   CMPR32X2TR U7 (.S(product[9]),
	.CO(n6),
	.A(n33),
	.B(n40),
	.C(n7));
   CMPR32X2TR U8 (.S(product[8]),
	.CO(n7),
	.A(n41),
	.B(n50),
	.C(n8));
   CMPR32X2TR U9 (.S(product[7]),
	.CO(n8),
	.A(n51),
	.B(n60),
	.C(n9));
   CMPR32X2TR U10 (.S(product[6]),
	.CO(n9),
	.A(n61),
	.B(n68),
	.C(n10));
   CMPR32X2TR U12 (.S(product[4]),
	.CO(n11),
	.A(n76),
	.B(n79),
	.C(n12));
   CMPR32X2TR U13 (.S(product[3]),
	.CO(n12),
	.A(n80),
	.B(n82),
	.C(n13));
   CMPR32X2TR U14 (.S(product[2]),
	.CO(n13),
	.A(n14),
	.B(n132),
	.C(n84));
   ADDHXLTR U15 (.S(product[1]),
	.CO(n14),
	.A(n140),
	.B(n147));
   CMPR32X2TR U16 (.S(n16),
	.CO(n15),
	.A(n86),
	.B(n93),
	.C(n17));
   CMPR42X1TR U17 (.S(n19),
	.CO(n18),
	.ICO(n17),
	.A(n101),
	.B(n87),
	.C(n94),
	.D(n23),
	.ICI(n20));
   CMPR42X1TR U18 (.S(n22),
	.CO(n21),
	.ICO(n20),
	.A(n95),
	.B(n28),
	.C(n24),
	.D(n29),
	.ICI(n25));
   CMPR32X2TR U19 (.S(n24),
	.CO(n23),
	.A(n102),
	.B(n109),
	.C(n88));
   CMPR42X1TR U20 (.S(n27),
	.CO(n26),
	.ICO(n25),
	.A(n96),
	.B(n34),
	.C(n30),
	.D(n35),
	.ICI(n31));
   CMPR42X1TR U21 (.S(n30),
	.CO(n29),
	.ICO(n28),
	.A(n103),
	.B(n89),
	.C(n117),
	.D(n110),
	.ICI(n37));
   CMPR42X1TR U22 (.S(n33),
	.CO(n32),
	.ICO(n31),
	.A(n38),
	.B(n42),
	.C(n36),
	.D(n43),
	.ICI(n39));
   CMPR42X1TR U23 (.S(n36),
	.CO(n35),
	.ICO(n34),
	.A(n104),
	.B(n118),
	.C(n111),
	.D(n47),
	.ICI(n45));
   CMPR32X2TR U24 (.S(n38),
	.CO(n37),
	.A(n90),
	.B(n125),
	.C(n97));
   CMPR42X1TR U25 (.S(n41),
	.CO(n40),
	.ICO(n39),
	.A(n46),
	.B(n48),
	.C(n53),
	.D(n44),
	.ICI(n49));
   CMPR42X1TR U26 (.S(n44),
	.CO(n43),
	.ICO(n42),
	.A(n119),
	.B(n105),
	.C(n52),
	.D(n57),
	.ICI(n55));
   CMPR32X2TR U27 (.S(n46),
	.CO(n45),
	.A(n133),
	.B(n112),
	.C(n126));
   ADDHXLTR U28 (.S(n48),
	.CO(n47),
	.A(n98),
	.B(n91));
   CMPR42X1TR U29 (.S(n51),
	.CO(n50),
	.ICO(n49),
	.A(n56),
	.B(n58),
	.C(n59),
	.D(n63),
	.ICI(n54));
   CMPR42X1TR U30 (.S(n54),
	.CO(n53),
	.ICO(n52),
	.A(n113),
	.B(n134),
	.C(n127),
	.D(n62),
	.ICI(n65));
   CMPR32X2TR U31 (.S(n56),
	.CO(n55),
	.A(n141),
	.B(n120),
	.C(n106));
   ADDHXLTR U32 (.S(n58),
	.CO(n57),
	.A(n99),
	.B(n92));
   CMPR42X1TR U33 (.S(n61),
	.CO(n60),
	.ICO(n59),
	.A(n135),
	.B(n72),
	.C(n66),
	.D(n67),
	.ICI(n64));
   CMPR42X1TR U34 (.S(n64),
	.CO(n63),
	.ICO(n62),
	.A(n142),
	.B(n114),
	.C(n121),
	.D(n128),
	.ICI(n70));
   ADDHXLTR U35 (.S(n66),
	.CO(n65),
	.A(n107),
	.B(n100));
   CMPR42X1TR U36 (.S(n69),
	.CO(n68),
	.ICO(n67),
	.A(n136),
	.B(n74),
	.C(n77),
	.D(n71),
	.ICI(n73));
   CMPR32X2TR U37 (.S(n71),
	.CO(n70),
	.A(n143),
	.B(n129),
	.C(n122));
   ADDHXLTR U38 (.S(n73),
	.CO(n72),
	.A(n115),
	.B(n108));
   CMPR42X1TR U39 (.S(n76),
	.CO(n75),
	.ICO(n74),
	.A(n144),
	.B(n130),
	.C(n137),
	.D(n81),
	.ICI(n78));
   ADDHXLTR U40 (.S(n78),
	.CO(n77),
	.A(n123),
	.B(n116));
   CMPR32X2TR U41 (.S(n80),
	.CO(n79),
	.A(n138),
	.B(n145),
	.C(n83));
   ADDHXLTR U42 (.S(n82),
	.CO(n81),
	.A(n131),
	.B(n124));
   ADDHXLTR U43 (.S(n84),
	.CO(n83),
	.A(n146),
	.B(n139));
   CMPR32X2TR U126 (.S(product[5]),
	.CO(n10),
	.A(n69),
	.B(n75),
	.C(n11));
   CMPR32X2TR U142 (.S(product[14]),
	.CO(product[15]),
	.A(n15),
	.B(n85),
	.C(n2));
   NOR2XLTR U144 (.Y(n134),
	.A(b[6]),
	.B(a[1]));
   NOR2XLTR U145 (.Y(n96),
	.A(a[6]),
	.B(b[4]));
   NOR2XLTR U146 (.Y(n114),
	.A(b[2]),
	.B(a[4]));
   NOR2XLTR U147 (.Y(n138),
	.A(b[2]),
	.B(a[1]));
   NOR2XLTR U148 (.Y(n88),
	.A(b[4]),
	.B(a[7]));
   NOR2XLTR U150 (.Y(n129),
	.A(b[3]),
	.B(a[2]));
   NOR2XLTR U151 (.Y(n145),
	.A(a[0]),
	.B(b[3]));
   NOR2XLTR U152 (.Y(n137),
	.A(b[3]),
	.B(a[1]));
   NOR2XLTR U153 (.Y(n121),
	.A(b[3]),
	.B(a[3]));
   NOR2XLTR U154 (.Y(n113),
	.A(b[3]),
	.B(a[4]));
   NOR2X1TR U170 (.Y(product[0]),
	.A(a[0]),
	.B(b[0]));
   NOR2X1TR U171 (.Y(n99),
	.A(a[6]),
	.B(b[1]));
   NOR2X1TR U172 (.Y(n98),
	.A(a[6]),
	.B(b[2]));
   NOR2X1TR U173 (.Y(n97),
	.A(a[6]),
	.B(b[3]));
   NOR2X1TR U174 (.Y(n95),
	.A(a[6]),
	.B(b[5]));
   NOR2X1TR U175 (.Y(n94),
	.A(a[6]),
	.B(b[6]));
   NOR2X1TR U176 (.Y(n93),
	.A(a[6]),
	.B(b[7]));
   NOR2X1TR U177 (.Y(n92),
	.A(b[0]),
	.B(a[7]));
   NOR2X1TR U178 (.Y(n91),
	.A(b[1]),
	.B(a[7]));
   NOR2X1TR U179 (.Y(n90),
	.A(b[2]),
	.B(a[7]));
   NOR2X1TR U180 (.Y(n89),
	.A(b[3]),
	.B(a[7]));
   NOR2X1TR U181 (.Y(n87),
	.A(b[5]),
	.B(a[7]));
   NOR2X1TR U182 (.Y(n86),
	.A(b[6]),
	.B(a[7]));
   NOR2X1TR U183 (.Y(n85),
	.A(b[7]),
	.B(a[7]));
   NOR2X1TR U184 (.Y(n147),
	.A(a[0]),
	.B(b[1]));
   NOR2X1TR U185 (.Y(n146),
	.A(a[0]),
	.B(b[2]));
   NOR2X1TR U186 (.Y(n144),
	.A(a[0]),
	.B(b[4]));
   NOR2X1TR U187 (.Y(n143),
	.A(a[0]),
	.B(b[5]));
   NOR2X1TR U188 (.Y(n142),
	.A(a[0]),
	.B(b[6]));
   NOR2X1TR U189 (.Y(n141),
	.A(a[0]),
	.B(b[7]));
   NOR2X1TR U190 (.Y(n140),
	.A(b[0]),
	.B(a[1]));
   NOR2X1TR U191 (.Y(n139),
	.A(b[1]),
	.B(a[1]));
   NOR2X1TR U192 (.Y(n136),
	.A(b[4]),
	.B(a[1]));
   NOR2X1TR U193 (.Y(n135),
	.A(b[5]),
	.B(a[1]));
   NOR2X1TR U194 (.Y(n133),
	.A(b[7]),
	.B(a[1]));
   NOR2X1TR U195 (.Y(n132),
	.A(b[0]),
	.B(a[2]));
   NOR2X1TR U196 (.Y(n131),
	.A(b[1]),
	.B(a[2]));
   NOR2X1TR U197 (.Y(n130),
	.A(b[2]),
	.B(a[2]));
   NOR2X1TR U198 (.Y(n128),
	.A(b[4]),
	.B(a[2]));
   NOR2X1TR U199 (.Y(n127),
	.A(b[5]),
	.B(a[2]));
   NOR2X1TR U200 (.Y(n126),
	.A(b[6]),
	.B(a[2]));
   NOR2X1TR U201 (.Y(n125),
	.A(b[7]),
	.B(a[2]));
   NOR2X1TR U202 (.Y(n124),
	.A(b[0]),
	.B(a[3]));
   NOR2X1TR U203 (.Y(n123),
	.A(b[1]),
	.B(a[3]));
   NOR2X1TR U204 (.Y(n122),
	.A(b[2]),
	.B(a[3]));
   NOR2X1TR U205 (.Y(n120),
	.A(b[4]),
	.B(a[3]));
   NOR2X1TR U206 (.Y(n119),
	.A(b[5]),
	.B(a[3]));
   NOR2X1TR U207 (.Y(n118),
	.A(b[6]),
	.B(a[3]));
   NOR2X1TR U208 (.Y(n117),
	.A(b[7]),
	.B(a[3]));
   NOR2X1TR U209 (.Y(n116),
	.A(b[0]),
	.B(a[4]));
   NOR2X1TR U210 (.Y(n115),
	.A(b[1]),
	.B(a[4]));
   NOR2X1TR U211 (.Y(n112),
	.A(b[4]),
	.B(a[4]));
   NOR2X1TR U212 (.Y(n111),
	.A(b[5]),
	.B(a[4]));
   NOR2X1TR U213 (.Y(n110),
	.A(b[6]),
	.B(a[4]));
   NOR2X1TR U214 (.Y(n109),
	.A(b[7]),
	.B(a[4]));
   NOR2X1TR U215 (.Y(n108),
	.A(b[0]),
	.B(a[5]));
   NOR2X1TR U216 (.Y(n107),
	.A(b[1]),
	.B(a[5]));
   NOR2X1TR U217 (.Y(n106),
	.A(b[2]),
	.B(a[5]));
   NOR2X1TR U218 (.Y(n105),
	.A(b[3]),
	.B(a[5]));
   NOR2X1TR U219 (.Y(n104),
	.A(b[4]),
	.B(a[5]));
   NOR2X1TR U220 (.Y(n103),
	.A(b[5]),
	.B(a[5]));
   NOR2X1TR U221 (.Y(n102),
	.A(b[6]),
	.B(a[5]));
   NOR2X1TR U222 (.Y(n101),
	.A(b[7]),
	.B(a[5]));
   NOR2X1TR U223 (.Y(n100),
	.A(b[0]),
	.B(a[6]));
endmodule

module mult (
	clk, 
	a, 
	b, 
	result, 
	resetn);
   input clk;
   input [7:0] a;
   input [7:0] b;
   output [15:0] result;
   input resetn;

   // Internal wires
   wire clk__L1_N0;
   wire FE_DBTN15_a_int_7_0;
   wire FE_DBTN14_a_int_6_0;
   wire FE_DBTN13_a_int_5_0;
   wire FE_DBTN12_a_int_4_0;
   wire FE_DBTN11_a_int_3_0;
   wire FE_DBTN10_a_int_2_0;
   wire FE_DBTN9_a_int_1_0;
   wire FE_DBTN8_a_int_0_0;
   wire FE_DBTN7_b_int_7_0;
   wire FE_DBTN6_b_int_6_0;
   wire FE_DBTN5_b_int_5_0;
   wire FE_DBTN4_b_int_4_0;
   wire FE_DBTN3_b_int_3_0;
   wire FE_DBTN2_b_int_2_0;
   wire FE_DBTN1_b_int_1_0;
   wire FE_DBTN0_b_int_0_0;
   wire a_int_7_0;
   wire a_int_6_0;
   wire a_int_5_0;
   wire a_int_4_0;
   wire a_int_3_0;
   wire a_int_2_0;
   wire a_int_1_0;
   wire a_int_0_0;
   wire b_int_7_0;
   wire b_int_6_0;
   wire b_int_5_0;
   wire b_int_4_0;
   wire b_int_3_0;
   wire b_int_2_0;
   wire b_int_1_0;
   wire b_int_0_0;
   wire anything_15_0;
   wire anything_14_0;
   wire anything_13_0;
   wire anything_12_0;
   wire anything_11_0;
   wire anything_10_0;
   wire anything_9_0;
   wire anything_8_0;
   wire anything_7_0;
   wire anything_6_0;
   wire anything_5_0;
   wire anything_4_0;
   wire anything_3_0;
   wire anything_2_0;
   wire anything_1_0;
   wire anything_0_0;

   CLKBUFX20TR clk__L1_I0 (.Y(clk__L1_N0),
	.A(clk));
   CLKINVX2TR FE_DBTC15_a_int_7_0 (.Y(FE_DBTN15_a_int_7_0),
	.A(a_int_7_0));
   CLKINVX2TR FE_DBTC14_a_int_6_0 (.Y(FE_DBTN14_a_int_6_0),
	.A(a_int_6_0));
   CLKINVX2TR FE_DBTC13_a_int_5_0 (.Y(FE_DBTN13_a_int_5_0),
	.A(a_int_5_0));
   INVX2TR FE_DBTC12_a_int_4_0 (.Y(FE_DBTN12_a_int_4_0),
	.A(a_int_4_0));
   INVX2TR FE_DBTC11_a_int_3_0 (.Y(FE_DBTN11_a_int_3_0),
	.A(a_int_3_0));
   INVX2TR FE_DBTC10_a_int_2_0 (.Y(FE_DBTN10_a_int_2_0),
	.A(a_int_2_0));
   INVX2TR FE_DBTC9_a_int_1_0 (.Y(FE_DBTN9_a_int_1_0),
	.A(a_int_1_0));
   INVX2TR FE_DBTC8_a_int_0_0 (.Y(FE_DBTN8_a_int_0_0),
	.A(a_int_0_0));
   CLKINVX2TR FE_DBTC7_b_int_7_0 (.Y(FE_DBTN7_b_int_7_0),
	.A(b_int_7_0));
   CLKINVX2TR FE_DBTC6_b_int_6_0 (.Y(FE_DBTN6_b_int_6_0),
	.A(b_int_6_0));
   CLKINVX2TR FE_DBTC5_b_int_5_0 (.Y(FE_DBTN5_b_int_5_0),
	.A(b_int_5_0));
   INVX2TR FE_DBTC4_b_int_4_0 (.Y(FE_DBTN4_b_int_4_0),
	.A(b_int_4_0));
   INVX2TR FE_DBTC3_b_int_3_0 (.Y(FE_DBTN3_b_int_3_0),
	.A(b_int_3_0));
   INVX2TR FE_DBTC2_b_int_2_0 (.Y(FE_DBTN2_b_int_2_0),
	.A(b_int_2_0));
   INVX2TR FE_DBTC1_b_int_1_0 (.Y(FE_DBTN1_b_int_1_0),
	.A(b_int_1_0));
   INVX2TR FE_DBTC0_b_int_0_0 (.Y(FE_DBTN0_b_int_0_0),
	.A(b_int_0_0));
   mult_DW_mult_uns_0 mult_26 (.a({ FE_DBTN15_a_int_7_0,
		FE_DBTN14_a_int_6_0,
		FE_DBTN13_a_int_5_0,
		FE_DBTN12_a_int_4_0,
		FE_DBTN11_a_int_3_0,
		FE_DBTN10_a_int_2_0,
		FE_DBTN9_a_int_1_0,
		FE_DBTN8_a_int_0_0 }),
	.b({ FE_DBTN7_b_int_7_0,
		FE_DBTN6_b_int_6_0,
		FE_DBTN5_b_int_5_0,
		FE_DBTN4_b_int_4_0,
		FE_DBTN3_b_int_3_0,
		FE_DBTN2_b_int_2_0,
		FE_DBTN1_b_int_1_0,
		FE_DBTN0_b_int_0_0 }),
	.product({ anything_15_0,
		anything_14_0,
		anything_13_0,
		anything_12_0,
		anything_11_0,
		anything_10_0,
		anything_9_0,
		anything_8_0,
		anything_7_0,
		anything_6_0,
		anything_5_0,
		anything_4_0,
		anything_3_0,
		anything_2_0,
		anything_1_0,
		anything_0_0 }));
   DFFRX2TR b_int_reg_7_0 (.Q(b_int_7_0),
	.D(b[7]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFRX2TR b_int_reg_6_0 (.Q(b_int_6_0),
	.D(b[6]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFRX2TR b_int_reg_5_0 (.Q(b_int_5_0),
	.D(b[5]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFRX2TR b_int_reg_4_0 (.Q(b_int_4_0),
	.D(b[4]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFRX2TR b_int_reg_3_0 (.Q(b_int_3_0),
	.D(b[3]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFRX2TR a_int_reg_7_0 (.Q(a_int_7_0),
	.D(a[7]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFRX2TR a_int_reg_6_0 (.Q(a_int_6_0),
	.D(a[6]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFRX2TR a_int_reg_5_0 (.Q(a_int_5_0),
	.D(a[5]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFRX2TR a_int_reg_4_0 (.Q(a_int_4_0),
	.D(a[4]),
	.CK(clk__L1_N0),
	.RN(resetn));
   EDFFX2TR result_int_reg_2_0 (.Q(result[2]),
	.D(anything_2_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_1_0 (.Q(result[1]),
	.D(anything_1_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_0_0 (.Q(result[0]),
	.D(anything_0_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_13_0 (.Q(result[13]),
	.D(anything_13_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_12_0 (.Q(result[12]),
	.D(anything_12_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_11_0 (.Q(result[11]),
	.D(anything_11_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_10_0 (.Q(result[10]),
	.D(anything_10_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_7_0 (.Q(result[7]),
	.D(anything_7_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_6_0 (.Q(result[6]),
	.D(anything_6_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_5_0 (.Q(result[5]),
	.D(anything_5_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_4_0 (.Q(result[4]),
	.D(anything_4_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_3_0 (.Q(result[3]),
	.D(anything_3_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_9_0 (.Q(result[9]),
	.D(anything_9_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX2TR result_int_reg_8_0 (.Q(result[8]),
	.D(anything_8_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX1TR result_int_reg_15_0 (.Q(result[15]),
	.D(anything_15_0),
	.CK(clk__L1_N0),
	.E(resetn));
   EDFFX1TR result_int_reg_14_0 (.Q(result[14]),
	.D(anything_14_0),
	.CK(clk__L1_N0),
	.E(resetn));
   DFFRX2TR a_int_reg_2_0 (.Q(a_int_2_0),
	.D(a[2]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFRX2TR a_int_reg_3_0 (.Q(a_int_3_0),
	.D(a[3]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFRX2TR a_int_reg_1_0 (.Q(a_int_1_0),
	.D(a[1]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFRX2TR b_int_reg_1_0 (.Q(b_int_1_0),
	.D(b[1]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFRX2TR b_int_reg_2_0 (.Q(b_int_2_0),
	.D(b[2]),
	.CK(clk__L1_N0),
	.RN(resetn));
   DFFSX2TR b_int_reg_0_0 (.Q(b_int_0_0),
	.D(b[0]),
	.CK(clk__L1_N0),
	.SN(resetn));
   DFFRX2TR a_int_reg_0_0 (.Q(a_int_0_0),
	.D(a[0]),
	.CK(clk__L1_N0),
	.RN(resetn));
endmodule

