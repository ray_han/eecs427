###################################
# EECS 427 F15
# Changed by Jaeyoung Kim for SOC 14.2 compatibility
# Changed on November 9th 2015
# Encounter basic script
###################################
set my_toplevel eecs427_top

# Setup design and create floorplan
source eecs427_top.globals
setDesignMode -process 130
init_design

# Initialize Floorplan
# floorPlan -r 1.0 0.70 10.8 10.8 10.8 10.8
floorPlan -site IBM13SITE -d 1254.0 1424.0 10.8 10.8 10.8 10.8
setFlipping f
redraw
fit

# Declare global VDD, VSS nets
clearGlobalNets
globalNetConnect VDD -type pgpin -pin VDD -inst *
#globalNetConnect VDD -type tiehi
globalNetConnect VSS -type pgpin -pin VSS -inst * 
#globalNetConnect VSS -type tielo

globalNetConnect VDD -type pgpin -pin IE  -inst PI*

saveDesign initialized.enc

#add io pad fill
addIoFiller -cell PFILL1 -prefix PF_ -fillAnyGap
#place the instances
placeInstance eecs427_core0/mult  603.0  446.0  MX -fixed
placeInstance eecs427_core0/ram   457.12 598.0  R0 -fixed

# Create Power Rings
addRing -nets {VDD VSS} -around each_block -center 1 -layer {top M3 bottom M3 left M4 right M4} -width {top 2 bottom 2 left 2 right 2} -spacing {top 1 bottom 1 left 1 right 1}
    

# Add power ring around placed instance
  selectInst eecs427_core0/ram
  addRing -nets {VDD} -type block_rings -around selected -layer {top M3 bottom M3 left M4 right M4} -width {top 10 bottom 10 left 10 right 10} -spacing {top 4 bottom 4 left 4 right 4} -offset {top 3 bottom 3 left 3 right 3} -extend_corner {tr tl}
  addRing -nets {VSS} -type block_rings -around selected -layer {top M3 bottom M3 left M4 right M4} -width {top 10 bottom 10 left 10 right 10} -spacing {top 4 bottom 4 left 4 right 4} -offset {top 16 bottom 24 left 16 right 16} -extend_corner {br bl}
  deselectAll


  selectInst eecs427_core0/mult
  addRing -nets {VDD} -type block_rings -around selected -layer {top M3 bottom M3 left M4 right M4} -width {top 10 bottom 10 left 10 right 10} -spacing {top 4 bottom 4 left 4 right 4} -offset {top 3 bottom 3 left 3 right 3} -extend_corner {br bl}
  addRing -nets {VSS} -type block_rings -around selected -layer {top M3 bottom M3 left M4 right M4} -width {top 10 bottom 10 left 10 right 10} -spacing {top 4 bottom 4 left 4 right 4} -offset {top 24 bottom 16 left 16 right 16} 
  deselectAll

# Add Stripes (optional but always very recommended)
# Should add more stripes in other metal layers
# addStripe -direction vertical -nets {VDD VSS}  -break_stripes_at_block_rings 1 -extend_to design_boundary -set_to_set_distance 24.0 -width 1.2 -spacing 10.8 -layer M4 -stacked_via_bottom_layer M3 -stacked_via_top_layer M5 -create_pins 1 -xleft_offset 0
globalNetConnect VDD -type pgpin -pin {VDD} -singleInstance {eecs427_core0/ram} -override -verbose
globalNetConnect VSS -type pgpin -pin {VSS} -singleInstance {eecs427_core0/ram} -override -verbose
globalNetConnect VDD -type pgpin -pin {VDD} -singleInstance {eecs427_core0/mult} -override -verbose
globalNetConnect VSS -type pgpin -pin {VSS} -singleInstance {eecs427_core0/mult} -override -verbose

# Connect power for each instance
  selectInst eecs427_core0/ram
sroute -nets { VDD VSS } -connect {blockPin} -blockPin {all} -blockPinTarget {nearestTarget} -inst {eecs427_core0/ram} -blockSides {top left right bottom} -jogControl {preferWithChanges differentLayer} -blockPinConnectRingPinCorners -verbose
deselectAll

  selectInst eecs427_core0/mult
sroute -nets { VDD VSS } -connect {blockPin} -blockPin {all} -blockPinTarget {nearestTarget} -inst {eecs427_core0/mult} -blockSides {top left right bottom} -jogControl { preferWithChanges differentLayer } -blockPinConnectRingPinCorners -verbose
deselectAll

saveDesign power.enc

# Then preplace the scan cells as jtag cells
#specifyJtag -hinst scan_chain0
#placeJtag -nrRow 1 -nrRowTop 3 -nrRowBottom 3 -nrRowLeft 6 -nrRowRight 3 -contour

# Place standard cells but count ONLY m1 and m2 as obstructions
setPlaceMode -congHighEffort -doCongOpt -modulePlan -maxRouteLayer 3
placeDesign -inPlaceOpt -prePlaceOpt
deleteObstruction -all
#addTieHiLo -cell "TIEHITR TIELOTR" -prefix tie 

redraw
saveDesign placed.enc
redraw

# Route power nets
globalNetConnect VDD    -type pgpin -pin VDD    -override  -netlistOverride
globalNetConnect VSS    -type pgpin -pin VSS    -override  -netlistOverride

# Route VDD and VSS between pad and power ring
sroute -nets { VDD VSS } -connect {padPin} -blockPin {all} -padPinMinLayer 4 -corePinNoRouteEmptyRows

saveDesign power_routed.enc

# TrialRoute 
trialRoute -highEffort

# Timing driven routing or timing optimization
setNanoRouteMode -drouteUseViaOfCut 2
setNanoRouteMode -drouteUseMultiCutViaEffort medium
setNanoRouteMode -drouteFixAntenna false
setNanoRouteMode -drouteSearchAndRepair true 
setNanoRouteMode -routeTopRoutingLayer 6
setNanoRouteMode -routeBottomRoutingLayer 2

globalDetailRoute

# Fix Antenna errors
# Set the top metal lower than the maximum level to avoid adding diodes
# setNanoRouteMode routeTopRoutingLayer 3
# setNanoRouteMode -routeInsertDiodeForClockNets true
# setNanoRouteMode -drouteFixAntenna true
# setNanoRouteMode -routeAntennaCellName "ANTENNATR"
# setNanoRouteMode -routeInsertAntennaDiodeForClockNet true
# setNanoRouteMode -routeInsertAntennaDiode true
# setNanoRouteMode drouteSearchAndRepair true 

# globalDetailRoute

# Save Desing
saveDesign routed.enc

# Output DEF, LEF and GDSII
set dbgLefDefOutVersion 5.7
#defout ../def/${my_toplevel}.def  -placement -routing 
lefout ../lef/${my_toplevel}.lef
setStreamOutMode -SEvianames ON
streamOut ../gds2/${my_toplevel}.gds2 -mapFile /afs/umich.edu/class/eecs427/ibm13/parts/artisan_cells/enc2gdsLM.map -libName eecs427artisan -structureName ${my_toplevel} -stripes 1 -units 1000 -mode ALL

#editSelect -nets clk
#deleteSelectedFromFPlan
saveNetlist -excludeLeafCell ../verilog/${my_toplevel}.apr.v

# Generate SDF
setExtractRCMode -engine postRoute -effortLevel high -relative_c_th 0.01 -total_c_th 0.01 -Reduce 0.0 -specialNet true
extractRC -outfile ${my_toplevel}.cap
rcOut -spf ${my_toplevel}.spf
rcOut -spef ${my_toplevel}.spef

setUseDefaultDelayLimit 10000
setDelayCalMode -engine aae -signoff true -ecsmType ecsmOnDemand
write_sdf -celltiming all ../sdf/${my_toplevel}.apr.sdf

# Run Geometry and Connection checks
verifyGeometry -reportAllCells -noSameNet -noOverlap -report ${my_toplevel}.geom.rpt
fixVia -minCut

# Meant for power vias that are just too small
verifyConnectivity -type all -noAntenna -report ${my_toplevel}.conn.rpt

puts "**************************************"
puts "*                                    *"
puts "* Encounter script finished          *"
puts "*                                    *"
puts "**************************************"
